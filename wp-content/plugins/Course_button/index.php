<?php
/*
Plugin Name:Custom Course button
Plugin URI: http://
Description: register students in course
Version: 1.0.0
Author: Sirushik
Author URI: http://
*/

include('add_to_cart_product.php');


/*
 * Function name event_reg
 *
 *  Description : Function calls on /events/  page , and when user clicks on button it is checking if the course is paid goes to checkout page otherwise it is continue the process
 *
 *  @params $course_id, $product_id
 *
 *
 * **/


function event_reg(){
    global $woocommerce, $wpdb;


    if (isset($_POST['elearning_event_id']) && $_POST['elearning_event_id']!='' && isset($_POST['elearning_product_id']) && $_POST['elearning_product_id']!=''){
        $course_id = $_POST['elearning_event_id'];
        $product_id = $_POST['elearning_product_id'];
        $current_product = wc_get_product($product_id);

        do_action('event_reg');

        if (isset($current_product->regular_price) && $current_product->regular_price != '' && $current_product->regular_price > 0 &&  isset($current_product->sale_price) && $current_product->sale_price != '' && $current_product->sale_price > 0) {
            $woocommerce->cart->add_to_cart($product_id);
                wp_redirect($woocommerce->cart->get_checkout_url());
                exit();
            }else{
            $current_user = wp_get_current_user();
            $order_data = array(
                'status'        => 'wc-completed',
                'customer_id'   => $current_user->ID,
                'customer_note' => '',
                'total'         => '',
            );
/*
           $address = array(
                'first_name' => $current_user->display_name,
                'email'      =>  $current_user->user_email,
            );
*/
            $order = wc_create_order($order_data);
            $order->add_product( get_product( $product_id ), 1 );


//            $order->set_address( $address, 'billing' );
            $order->calculate_totals();
            $order->update_status( "wc-completed", 'Imported order', TRUE );

            wp_redirect(event_reg_orders_check($order->id));

        }
        }
    }


add_shortcode('event_reg','event_reg');

if($_SERVER['REQUEST_URI'] == '/events/') {
    add_action('wpcf7_before_send_mail', 'event_reg', 1);
}

/*
 * Function name  event_reg_orders_check
 *
 * Description: Hooks to event_reg function and checking if the current order is completed the register user to the course
 *
 * **/



function event_reg_orders_check($order_id){
    require_once( $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/elearning/eLearning-addons/onstreamAPI/functions.php' );
    global $woocommerce;
    $order = new WC_Order( $order_id );
    $customer_id = (int)$order->user_id;
    $items = $order->get_items();
    foreach ( $items as $item ) {
        $product_name = $item['name'];
        $product_id = $item['product_id'];
        $post_meta = get_post_meta($product_id);
        $event_id =  $post_meta['eLearning_associated_product'];
        $webinarPostMeta = get_post_meta($event_id[0]);
        $webinarId = $webinarPostMeta['eLearning_webinar_id'][0];
        $thank_you_page_id = $webinarPostMeta['thank_you_page'][0];

        $thank_you_page = get_site_url().'/'.get_post($thank_you_page_id)->post_name;

        $inner_page_id = $webinarPostMeta['inner_page'][0];
        $inner_page = get_site_url().'/'.get_post($inner_page_id)->post_name;

        $courseStart = $webinarPostMeta['eLearning_start_date'][0];
        $current_date = strtotime("now");
        $start_date = str_replace(array('/', ' ',':','.'), '-', $courseStart);
        $start_date = strtotime($start_date);
        $diff = $start_date - $current_date;

/*        echo "<pre>";
        print_r($inner_page);die;*/
        SetStudentAPI($webinarId, $customer_id);


        if ($diff > 0){
            return $thank_you_page;
        }else{
            return $inner_page;
        }

    }


}

add_action( 'woocommerce_order_status_completed', 'event_reg_orders_check', 10, 1 );






function register_users($atts)
{

    $a = shortcode_atts(
        array
        (
            'course_id'=>'',
            'value'=>''
        ), $atts
    );

        if ( is_user_logged_in() ) {
            $user_id = get_current_user_id();
            $course_id =$a['course_id'];
            $values=$a['value'];
            echo '
    <form action="" method="post" name="myForm" onsubmit="">
    <input type="submit" value="' . $values . '" name="Submitname" id="buttonSubmit" autocomplete="off" style="width:20%; height:80px; border: none !important;"/>
    <input type="hidden" name="meta_id" value="' . $user_id . '" id="userId"/>
    <input type="hidden" name="post_id" value="' . $course_id . '" id="courseId"/>
</form>
';
            if (isset($_POST['Submitname']) && $_POST['Submitname'] == '' . $values . ''){
        if (empty($args)) {
            $args = array();
        }
        $seats = bp_course_get_max_students($course_id, $user_id);
        $students = bp_course_count_students_pursuing($course_id);

                get_course_associated_product($course_id);
        if (!empty($seats) && $seats < 9999 && empty($force)) {
            if ($seats < $students) {
                return false;
            }
        }
        $total_duration = 0;
        if (empty($duration)) {
            $total_duration = bp_course_get_course_duration($course_id, $user_id);
        } else {
            $total_duration = $duration;
        }

        $time = 0;
        $existing = get_user_meta($user_id, $course_id, true);

        if (empty($existing)) {
            $start_date = bp_course_get_start_date($course_id, $user_id);
            if (isset($start_date) && $start_date) {
                $time = strtotime($start_date);
            }
        } else {
            $time = $existing;
        }

        if ($time < time())
            $time = time();

        if (empty($total_duration)) {
            $total_duration = 0;
        }

        $t = $time + $total_duration;

        update_post_meta($course_id, $user_id, 0);

        if (empty($existing)) {
            update_user_meta($user_id, 'course_status' . $course_id, 1);
        } else {
            update_user_meta($user_id, 'course_status' . $course_id, 2);
            $students = get_post_meta($course_id, 'eLearning_students', true);
            $students++;
            $j = update_post_meta($course_id, 'eLearning_students', $students);
        }

        update_user_meta($user_id, $course_id, $t);
        echo "you are enrolled in the course succesfully";
    }
    }

}

add_shortcode('create_enroll_button', 'register_users');


?>