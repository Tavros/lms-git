<?php
/*
 * Get course by id
 *
 * @params $course_id
 * @return
 *
 */
error_reporting(0);
function get_course_associated_product( $course_id ) {
    global $wpdb , $woocommerce;
    $meta_key = "'eLearning_product'";

    // Connect to database to select the product id by course id

    $sql_get_product_id = 'SELECT meta_value FROM wp_postmeta WHERE post_id = '.$course_id.' AND meta_key = '.$meta_key;
    $results = $wpdb->get_row( $sql_get_product_id );


    $course_product_id = $results->meta_value;



    // Add product to cart by course product id
    $woocommerce->cart->add_to_cart( $course_product_id );

    // Check if user is registered for course then get the current course , get the course price,
    // if it is not free go to checkout page else make an order and submit to user

        if( isset( $_REQUEST['post_id'] ) && !empty( $_REQUEST['post_id'] ) ) {
            $_product = wc_get_product( $course_product_id );
            $reg_price = $_product->get_regular_price();

            if( isset( $reg_price ) &&  $reg_price > 0 ) {

                wp_redirect($woocommerce->cart->get_checkout_url());
                exit();
            } else {
                $order = wc_create_order();
                $order->add_product( get_product( $course_product_id ), 1 );
                $order->calculate_totals();
                $order->update_status( "wc-completed", 'Imported order', TRUE );
                wp_redirect(home_url());
            }
        }

    }

    /*
     * Remove billing address fields from checkout
     *
     * @params $fields
     *
     *
     */

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    $billing_fields = array(
        'billing_first_name',
        'billing_last_name',
        'billing_company',
        'billing_address_1',
        'billing_address_2',
        'billing_city',
        'billing_postcode',
        'billing_country',
        'billing_state',
        'billing_phone',
        'order_comments',
        'billing_address_2',
        'billing_postcode',
        'billing_company',
        'billing_last_name',
        'billing_email',
        'billing_city',
    );

    foreach( $billing_fields  as $data )
        unset( $fields['billing'][ $data ] );
}

/*
 * Add product to cart
 *
 * @param $product_id
 *
 */

add_action( 'init', 'add_product_to_cart' );

function add_product_to_cart( $product_id ) {
    if ( ! is_admin() ) {
        global $woocommerce;

        $found = false;
        $cart_total = 0;

        if( $woocommerce->cart->total >= $cart_total ) {
            //check if product already in cart
            if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
                foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
                    $_product = $values['data'];

                    if ( $_product->id == $product_id )
                        $found = true;
                }
                // if product not found, add it
                if ( ! $found )
                    $woocommerce->cart->add_to_cart( $product_id );
            } else {
                // if no products in cart, add it
                $woocommerce->cart->add_to_cart( $product_id );
            }
        }
    }
}
?>