<?php
	if($file_used=="sql_table")
	{
		
		$request 			= array();
		$start				= 0;
		
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		
		$el_id_order_status 	= $this->el_get_woo_requests('el_id_order_status',NULL,true);				
		$el_paid_customer		= $this->el_get_woo_requests('el_customers_paid',NULL,true);				
		$txtProduct 		= $this->el_get_woo_requests('txtProduct',NULL,true);
		$el_product_id			= $this->el_get_woo_requests('el_product_id',"-1",true);
		$category_id 		= $this->el_get_woo_requests('el_category_id','-1',true);
		
		$limit 				= $this->el_get_woo_requests('limit',15,true);
		$p 					= $this->el_get_woo_requests('p',1,true);
	
		$page 				= $this->el_get_woo_requests('page',NULL,true);
		$order_id 			= $this->el_get_woo_requests('el_id_order',NULL,true);
		$el_from_date 		= $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date 			= $this->el_get_woo_requests('el_to_date',NULL,true);
		
		$el_txt_email 			= $this->el_get_woo_requests('el_email_text',NULL,true);
		
		$el_txt_first_name		= $this->el_get_woo_requests('el_first_name_text',NULL,true);
		
		$el_detail_view		= $this->el_get_woo_requests('el_view_details',"no",true);				
		$el_country_code		= $this->el_get_woo_requests('el_countries_code',NULL,true);
		$state_code			= $this->el_get_woo_requests('el_states_code','-1',true);
		$el_payment_method		= $this->el_get_woo_requests('payment_method',NULL,true);
		$el_order_item_name	= $this->el_get_woo_requests('order_item_name',NULL,true);//for coupon
		$el_coupon_code		= $this->el_get_woo_requests('coupon_code',NULL,true);//for coupon
		$el_publish_order		= $this->el_get_woo_requests('publish_order','no',true);//if publish display publish order only, no or null display all order
		$el_coupon_used		= $this->el_get_woo_requests('el_use_coupon','no',true);				
		$el_order_meta_key		= $this->el_get_woo_requests('order_meta_key','-1',true);
		$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
		$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		$el_coupon_code		= $this->el_get_woo_requests('coupon_code','-1',true);
		$el_coupon_codes		= $this->el_get_woo_requests('el_codes_of_coupon','-1',true);
		
		$el_max_amount			= $this->el_get_woo_requests('max_amount','-1',true);
		$el_min_amount			= $this->el_get_woo_requests('min_amount','-1',true);
		
		$el_billing_post_code		= $this->el_get_woo_requests('el_bill_post_code','-1',true);
		$el_variation_id		= $this->el_get_woo_requests('variation_id','-1',true);
		$el_variation_only		= $this->el_get_woo_requests('variation_only','-1',true);
		$el_hide_os		= $this->el_get_woo_requests('el_hide_os','"trash"',true);
		
		$el_show_cog		= $this->el_get_woo_requests('el_show_cog','no',true);
		
		///////////HIDDEN FIELDS////////////
		$el_hide_os='trash';
		$el_publish_order='no';
		$el_order_item_name='';
		$el_coupon_code='';
		$el_coupon_codes='';
		$el_payment_method='';
		
		$el_variation_only=$this->el_get_woo_requests('variation_only','-1',true);
		$el_order_meta_key='';
		
		$data_format=$this->el_get_woo_requests('date_format',get_option('date_format'),true);
		
		
		$el_variation_id='-1';
		$amont_zero='';
		//////////////////////
		
		
		
		/////////////////CUSTOM FIELDS & TAXONOMY/////////////////
		
		/////////////////////////////////
		
		
		
		
		
		
		
		

		$el_variations_formated='';
		
		if(strlen($el_max_amount)<=0) $_REQUEST['max_amount']	= 	$el_max_amount = '-1';
		if(strlen($el_min_amount)<=0) $_REQUEST['min_amount']	=	$el_min_amount = '-1';
		
		if($el_max_amount != '-1' || $el_min_amount != '-1'){
			if($el_order_meta_key == '-1'){
				$_REQUEST['order_meta_key']	= "_order_total";
			}					
		}
		
		$last_days_orders 		= "0";
		if(is_array($el_id_order_status)){		$el_id_order_status 	= implode(",", $el_id_order_status);}
		if(is_array($category_id)){ 		$category_id		= implode(",", $category_id);}
		
		if(!$el_from_date){	$el_from_date = date_i18n('Y-m-d');}
		if(!$el_to_date){
			$last_days_orders 		= apply_filters($page.'_back_day', $last_days_orders);//-1,-2,-3,-4,-5
			$el_to_date = date('Y-m-d', strtotime($last_days_orders.' day', strtotime(date_i18n("Y-m-d"))));}
		
		$el_sort_by 			= $this->el_get_woo_requests('sort_by','order_id',true);
		$el_order_by 			= $this->el_get_woo_requests('order_by','DESC',true);
		///
		
		if($p > 1){	$start = ($p - 1) * $limit;}
		
		if($el_detail_view == "yes"){
			$el_variations_value		= $this->el_get_woo_requests('variations_value',"-1",true);
			$el_variations_formated = '-1';
			if($el_variations_value != "-1" and strlen($el_variations_value)>0){
				$el_variations_value = explode(",",$el_variations_value);				
				$var = array();
				foreach($el_variations_value as $key => $value):
					$var[] .=  $value;
				endforeach;
				$result = array_unique ($var);
				//$this->print_array($var);
				$el_variations_formated = implode("', '",$result);
			}
			$_REQUEST['variations_formated'] = $el_variations_formated;
		}
		
		
		//el_first_name_text
		$el_txt_first_name_cols='';
		$el_txt_first_name_join = '';
		$el_txt_first_name_condition_1 = '';
		$el_txt_first_name_condition_2 = '';
		
		//el_email_text
		$el_txt_email_cols ='';
		$el_txt_email_join = '';
		$el_txt_email_condition_1 = '';
		$el_txt_email_condition_2 = '';
		
		//SORT BY
		$el_sort_by_cols ='';
		
		//CATEGORY
		$category_id_join ='';
		$category_id_condition = '';
		
		//ORDER ID
		$el_id_order_status_join ='';
		$el_id_order_status_condition = '';
		
		//COUNTRY
		$el_country_code_join = '';
		$el_country_code_condition_1 = '';
		$el_country_code_condition_2 = '';
		
		//STATE
		$state_code_join= '';
		$state_code_condition_1 = '';
		$state_code_condition_2 = '';
		
		//PAYMENT METHOD
		$el_payment_method_join= '';
		$el_payment_method_condition_1 = '';
		$el_payment_method_condition_2 = '';
		
		//POSTCODE
		$el_billing_post_code_join = '';
		$el_billing_post_code_condition= '';
		
		//COUPON USED
		$el_coupon_used_join = '';
		$el_coupon_used_condition = '';
		
		//VARIATION ID
		$el_variation_id_join = '';
		$el_variation_id_condition = '';
		
		//VARIATION ONLY
		$el_variation_only_join = '';
		$el_variation_only_condition = '';
		
		//VARIATION FORMAT
		$el_variations_formated_join = '';
		$el_variations_formated_condition = '';
		
		//ORDER META KEY
		$el_order_meta_key_join = '';
		$el_order_meta_key_condition = '';
		
		//COUPON CODES
		$el_coupon_codes_join = '';
		$el_coupon_codes_condition = '';
		
		//COUPON CODE
		$el_coupon_code_condition = '';
		
		//DATA CONDITION
		$date_condition = '';
		
		//ORDER ID
		$order_id_condition = '';
		
		//PAID CUSTOMER
		$el_paid_customer_condition = '';
		
		//PUBLISH ORDER
		$el_publish_order_condition_1 = '';
		$el_publish_order_condition_2 = '';
		
		//ORDER ITEM NAME
		$el_order_item_name_condition = '';
		
		//txt PRODUCT
		$txtProduct_condition = '';
		
		//PRODUCT ID
		$el_product_id_condition = '';
		
		//CATEGORY ID
		$category_id_condition = '';
		
		//ORDER STATUS ID
		$el_id_order_status_condition = '';
		
		//ORDER STATUS
		$el_order_status_condition = '';
		
		//HIDE ORDER STATUS
		$el_hide_os_condition = '';
		
		
		if($el_sort_by == "status"){
			$el_sort_by_cols = " terms2.name as status, ";
		}
		$sql_columns = " $el_txt_first_name_cols $el_txt_email_cols $el_sort_by_cols";
		$sql_columns .= "
		DATE_FORMAT(el_posts.post_date,'%m/%d/%Y') 													AS order_date,
		el_woocommerce_order_items.order_id 															AS order_id,					
		el_woocommerce_order_items.order_item_name 													AS product_name,					
		el_woocommerce_order_items.order_item_id														AS order_item_id,
		woocommerce_order_itemmeta.meta_value 														AS woocommerce_order_itemmeta_meta_value,					
		(el_woocommerce_order_itemmeta2.meta_value/el_woocommerce_order_itemmeta3.meta_value) 			AS sold_rate,
		(el_woocommerce_order_itemmeta4.meta_value/el_woocommerce_order_itemmeta3.meta_value) 			AS product_rate,
		(el_woocommerce_order_itemmeta4.meta_value) 													AS item_amount,
		(el_woocommerce_order_itemmeta2.meta_value) 													AS item_net_amount,
		(el_woocommerce_order_itemmeta4.meta_value - el_woocommerce_order_itemmeta2.meta_value) 			AS item_discount,					
		el_woocommerce_order_itemmeta2.meta_value 														AS total_price,
		count(el_woocommerce_order_items.order_item_id) 												AS product_quentity,
		woocommerce_order_itemmeta.meta_value 														AS product_id
		
		,woocommerce_order_itemmeta22.meta_value AS variation_id 
		
		,el_woocommerce_order_itemmeta3.meta_value 													AS 'product_quantity'					
		,el_posts.post_status 																			AS post_status
		,el_posts.post_status 																			AS order_status
		
		";
			
		$sql_joins ="{$wpdb->prefix}woocommerce_order_items as el_woocommerce_order_items 
		
		LEFT JOIN  {$wpdb->prefix}posts as el_posts ON el_posts.ID=el_woocommerce_order_items.order_id				
		
		LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta 	as woocommerce_order_itemmeta 	ON woocommerce_order_itemmeta.order_item_id		=	el_woocommerce_order_items.order_item_id
		
		LEFT JOIN wp_woocommerce_order_itemmeta as woocommerce_order_itemmeta22 ON woocommerce_order_itemmeta22.order_item_id	=	el_woocommerce_order_items.order_item_id
		
		LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta 	as el_woocommerce_order_itemmeta2 	ON el_woocommerce_order_itemmeta2.order_item_id	=	el_woocommerce_order_items.order_item_id
		LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta 	as el_woocommerce_order_itemmeta3 	ON el_woocommerce_order_itemmeta3.order_item_id	=	el_woocommerce_order_items.order_item_id
		LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta 	as el_woocommerce_order_itemmeta4 	ON el_woocommerce_order_itemmeta4.order_item_id	=	el_woocommerce_order_items.order_item_id AND el_woocommerce_order_itemmeta4.meta_key='_line_subtotal'";	
			
		
		
		
		if($category_id  && $category_id != "-1") {
			$category_id_join = "
				LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 			ON el_term_relationships.object_id		=	woocommerce_order_itemmeta.meta_value
				LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 				ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id";
				//LEFT JOIN  {$wpdb->prefix}terms 				as el_terms 						ON el_terms.term_id					=	term_taxonomy.term_id";
		}
		
		if(($el_id_order_status  && $el_id_order_status != '-1') || $el_sort_by == "status"){
			$el_id_order_status_join= "
				LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships2			ON el_term_relationships2.object_id	= el_woocommerce_order_items.order_id
				LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as el_term_taxonomy2				ON el_term_taxonomy2.term_taxonomy_id	= el_term_relationships2.term_taxonomy_id";
				if($el_sort_by == "status"){
					$el_id_order_status_join .= " LEFT JOIN  {$wpdb->prefix}terms 	as terms2 						ON terms2.term_id					=	el_term_taxonomy2.term_id";
				}
		}
				
		if($el_country_code and $el_country_code != '-1')
			$el_country_code_join = " LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta4 ON el_postmeta4.post_id=el_woocommerce_order_items.order_id";
		
		if($state_code && $state_code != '-1')
			$state_code_join= " LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta_billing_state ON el_postmeta_billing_state.post_id=el_posts.ID";
		
		if($el_payment_method)
			$el_payment_method_join= " LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta5 ON el_postmeta5.post_id=el_woocommerce_order_items.order_id";
		
	
		$post_type_condition="el_posts.post_type = 'shop_order'";
		
		$other_condition_1 = "
		AND woocommerce_order_itemmeta.meta_key = '_product_id'
		
		AND woocommerce_order_itemmeta22.meta_key = '_variation_id' 
		
		AND el_woocommerce_order_itemmeta2.meta_key='_line_total'
		AND el_woocommerce_order_itemmeta3.meta_key='_qty' ";
		
		
		
		if($el_country_code and $el_country_code != '-1')
			$el_country_code_condition_1 = " AND el_postmeta4.meta_key='_billing_country'";
		
		if($state_code && $state_code != '-1')
			$state_code_condition_1 = " AND el_postmeta_billing_state.meta_key='_billing_state'";
		
		if ($el_from_date != NULL &&  $el_to_date !=NULL){
			$date_condition = " AND DATE(el_posts.post_date) BETWEEN '".$el_from_date."' AND '". $el_to_date ."'";
		}
		
		if($order_id)
			$order_id_condition = " AND el_woocommerce_order_items.order_id = ".$order_id;
		
		if($el_publish_order == 'yes')
			$el_publish_order_condition_1 = " AND el_posts.post_status = 'publish'";
			
		if($el_publish_order == 'publish' || $el_publish_order == 'trash')
			$el_publish_order_condition_2 = " AND el_posts.post_status = '".$el_publish_order."'";
	
		if($el_country_code and $el_country_code != '-1')
			$el_country_code_condition_2 = " AND el_postmeta4.meta_value IN ('".$el_country_code."')";
			
		if($state_code && $state_code != '-1')
			$state_code_condition_2 = " AND el_postmeta_billing_state.meta_value IN ('".$state_code."')";
	
		if($el_product_id  && $el_product_id != "-1")
			$el_product_id_condition = " AND woocommerce_order_itemmeta.meta_value IN (".$el_product_id .")";	
		
		if($category_id  && $category_id != "-1")
			$category_id_condition = " AND term_taxonomy.taxonomy LIKE('product_cat') AND term_taxonomy.term_id IN (".$category_id .")";
			
		
		if($el_id_order_status  && $el_id_order_status != "-1")
			$el_id_order_status_condition = " AND el_term_taxonomy2.taxonomy LIKE('shop_order_status') AND el_term_taxonomy2.term_id IN (".$el_id_order_status .")";
		
		if($el_order_status  && $el_order_status != '-1' and $el_order_status != "'-1'")
			$el_order_status_condition = " AND el_posts.post_status IN (".$el_order_status.")";
		
		if($el_hide_os  && $el_hide_os != '-1' and $el_hide_os != "'-1'")
			$el_hide_os_condition = " AND el_posts.post_status NOT IN ('".$el_hide_os."')";
		
		
		
		$sql ="SELECT $sql_columns FROM $sql_joins";
		
		$sql .="$category_id_join $el_id_order_status_join $el_txt_email_join $el_txt_first_name_join
				$el_country_code_join $state_code_join $el_payment_method_join $el_billing_post_code_join
				$el_coupon_used_join $el_variation_id_join $el_variation_only_join $el_variations_formated_join
				$el_order_meta_key_join $el_coupon_codes_join";
		
		$sql .= " Where $post_type_condition $el_txt_email_condition_1 $el_txt_first_name_condition_1
						$other_condition_1 $el_country_code_condition_1 $state_code_condition_1
						$el_billing_post_code_condition $el_payment_method_condition_1 $date_condition
						$order_id_condition $el_txt_email_condition_2 $el_paid_customer_condition
						$el_txt_first_name_condition_2 $el_publish_order_condition_1 $el_publish_order_condition_2
						$el_country_code_condition_2 $state_code_condition_2 $el_payment_method_condition_2
						$el_order_meta_key_condition $el_order_item_name_condition $txtProduct_condition
						$el_product_id_condition $category_id_condition $el_id_order_status_condition
						$el_coupon_used_condition $el_coupon_code_condition $el_coupon_codes_condition
						$el_variation_id_condition $el_variation_only_condition $el_variations_formated_condition
						$el_order_status_condition $el_hide_os_condition ";
		
		$sql_group_by = " GROUP BY el_woocommerce_order_items.order_item_id ";
		
		$sql .=$sql_group_by;
		
		//print_r($search_fields);
		//echo $sql;
		
		
		/*global $wpdb;
		$query_tax_query=array('relation' => 'AND');
		$query_tax_query[]=array(
			'taxonomy' => 'product_cat',
			'field'    => 'id',
			'terms'    => array('9'),
			'operator' => 'IN',
		);
		$query_tax_query[]=array(
			'taxonomy' => 'product_brand',
			'field'    => 'id',
			'terms'    => array('19'),
			'operator' => 'IN',
		);
		
		
		$args = array(
			'post_type'   => array('product'),
			'post_status' =>'publish',
			'posts_per_page'	=> -1,
			'tax_query'=>$query_tax_query
		);
		
		$quer=new WP_Query($args);
		echo $quer->request;*/
		
		
		
		$columns=array(
			array('lable'=>__('Order ID',__ELREPORT_TEXTDOMAIN__),'status'=>'show'),
			array('lable'=>__('Status',__ELREPORT_TEXTDOMAIN__),'status'=>'show'),
			array('lable'=>__('Net Amt.',__ELREPORT_TEXTDOMAIN__),'status'=>'currency'),
			array('lable'=>__('Cog',__ELREPORT_TEXTDOMAIN__),'status'=>'currency'),
			array('lable'=>__('Profit',__ELREPORT_TEXTDOMAIN__),'status'=>'currency'),
			//array('lable'=>__('Avg Profit/Order',__ELREPORT_TEXTDOMAIN__),'status'=>'currency'),
		);
	
		$this->table_cols = $columns;
		
	}
	elseif($file_used=="data_table"){

		$first_order_id='';
		
		$order_items=$this->results;
		$categories = array();
		$order_meta = array();
		if(count($order_items)>0)
		
		foreach ( $order_items as $key => $order_item ) {
			
				$order_id								= $order_item->order_id;
				$order_items[$key]->billing_first_name  = '';//Default, some time it missing
				$order_items[$key]->billing_last_name  	= '';//Default, some time it missing
				$order_items[$key]->billing_email  		= '';//Default, some time it missing
				
				if(!isset($order_meta[$order_id])){
					$order_meta[$order_id]					= $this->el_get_full_post_meta($order_id);
				}
				
				foreach($order_meta[$order_id] as $k => $v){
					$order_items[$key]->$k			= $v;
				}
				
				
				$order_items[$key]->order_total			= isset($order_item->order_total)		? $order_item->order_total 		: 0;
				$order_items[$key]->order_shipping		= isset($order_item->order_shipping)	? $order_item->order_shipping 	: 0;
				
				
				$order_items[$key]->cart_discount		= isset($order_item->cart_discount)		? $order_item->cart_discount 	: 0;
				$order_items[$key]->order_discount		= isset($order_item->order_discount)	? $order_item->order_discount 	: 0;
				$order_items[$key]->total_discount 		= isset($order_item->total_discount)	? $order_item->total_discount 	: ($order_items[$key]->cart_discount + $order_items[$key]->order_discount);
				
				
				$order_items[$key]->order_tax 			= isset($order_item->order_tax)			? $order_item->order_tax : 0;
				$order_items[$key]->order_shipping_tax 	= isset($order_item->order_shipping_tax)? $order_item->order_shipping_tax : 0;
				$order_items[$key]->total_tax 			= isset($order_item->total_tax)			? $order_item->total_tax 	: ($order_items[$key]->order_tax + $order_items[$key]->order_shipping_tax);
				
				$transaction_id = "ransaction ID";
				$order_items[$key]->transaction_id		= isset($order_item->$transaction_id) 	? $order_item->$transaction_id		: (isset($order_item->transaction_id) ? $order_item->transaction_id : '');
				$order_items[$key]->gross_amount 		= ($order_items[$key]->order_total + $order_items[$key]->total_discount) - ($order_items[$key]->order_shipping +  $order_items[$key]->order_shipping_tax + $order_items[$key]->order_tax );
				
				
				$order_items[$key]->billing_first_name	= isset($order_item->billing_first_name)? $order_item->billing_first_name 	: '';
				$order_items[$key]->billing_last_name	= isset($order_item->billing_last_name)	? $order_item->billing_last_name 	: '';
				$order_items[$key]->billing_name		= $order_items[$key]->billing_first_name.' '.$order_items[$key]->billing_last_name;
				
			
		}
		
		
		$this->results=$order_items;
		
		
		//print_r($this->results);
		
		$items_render=array();
		
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
			
			$order_id= $items->order_id;
			$fetch_other_data='';				
							
			if(!isset($this->order_meta[$order_id])){
				$fetch_other_data= $this->el_get_full_post_meta($order_id);
			}
			
			$new_order=false;
			if($first_order_id=='')
			{
				$first_order_id=$items->order_id;
				$new_order=true;
			}else if($first_order_id!=$items->order_id)
			{
				$first_order_id=$items->order_id;
				$new_order=true;	
			}
			
			
			if(in_array($items->order_id,$items_render))		
				continue;
			else
				$items_render[]=$items->order_id;	
			
			$datatable_value.=("<tr>");
				
				//order ID
				$display_class='';
				if($this->table_cols[0]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->order_id;
				$datatable_value.=("</td>");
				
				//Status
				$el_table_value = isset($items->order_status) ? $items->order_status : '';
				
				if($el_table_value=='wc-completed')
					$el_table_value = '<span class="awr-order-status awr-order-status-'.sanitize_title($el_table_value).'" >'.ucwords(__($el_table_value, __ELREPORT_TEXTDOMAIN__)).'</span>';
				else if($el_table_value=='wc-refunded')
					$el_table_value = '<span class="awr-order-status awr-order-status-'.sanitize_title($el_table_value).'" >'.ucwords(__($el_table_value, __ELREPORT_TEXTDOMAIN__)).'</span>';
				else
					$el_table_value = '<span class="awr-order-status awr-order-status-'.sanitize_title($el_table_value).'" >'.ucwords(__($el_table_value, __ELREPORT_TEXTDOMAIN__)).'</span>';	
				
				$display_class='';
				if($this->table_cols[4]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= str_replace("Wc-","",$el_table_value);
				$datatable_value.=("</td>");
				
				//Order Total
				$display_class='';
				$el_table_value = isset($items -> order_total) ? ($items -> order_total)-$part_refund : 0;
				$el_table_value = $el_table_value == 0 ? $el_null_val : $el_table_value;
				if($this->table_cols[8]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($el_table_value,array("currency" => $fetch_other_data['order_currency']),'multi_currency');
				$datatable_value.=("</td>");
				
				$display_class='';
				$all_cog='';
				
				$order = new WC_Order( $order_id );
				$items_order = $order->get_items();
				
				foreach ( $items_order as $item ) {
					$product_qty = $item['qty'];
					$product_id = $item['product_id'];
					$product_variation_id = $item['variation_id'];
					
					$product_id_cog=$product_id;
					if($product_variation_id!=''){
						$product_id_cog=$product_variation_id;
					}
					$cog=get_post_meta($product_id_cog,__PW_COG__,true);
					$cog*=$product_qty;
					$all_cog+=$cog;
				}
				
				
				
				if($this->table_cols[5]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $all_cog == 0 ? 0 : $this->price($all_cog);
				$datatable_value.=("</td>");
				
				$display_class='';
				if($this->table_cols[4]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $all_cog == 0 ? 0 : $this->price($el_table_value-$all_cog);
				$datatable_value.=("</td>");
				
				
			$datatable_value.=("</tr>");	
		
			
			
		}
	}elseif($file_used=="search_form"){
	?>
		<form class='alldetails search_form_report' action='' method='post'>
            <input type='hidden' name='action' value='submit-form' />
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Date From',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_from_date" id="pwr_from_date" type="text" readonly='true' class="datepick"/>
                </div>
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Date To',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_to_date" id="pwr_to_date" type="text" readonly='true' class="datepick"/>
                </div>
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Order ID',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_id_order" type="text"  class=""/>
                </div>
                
               
                <?php
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_category_id');
                	if($this->get_form_element_permission('el_category_id') ||  $permission_value!=''){
						
						if(!$this->get_form_element_permission('el_category_id') &&  $permission_value!='')
							$col_style='display:none';
				?>
                <div class="col-md-6" style=" <?php echo $col_style;?>">
                    <div class="awr-form-title">
                        <?php _e('Category',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-tags"></i></span>
                    <?php
                        $args = array(
                            'orderby'                  => 'name',
                            'order'                    => 'ASC',
                            'hide_empty'               => 1,
                            'hierarchical'             => 0,
                            'exclude'                  => '',
                            'include'                  => '',
                            'child_of'          		 => 0,
                            'number'                   => '',
                            'pad_counts'               => false 
                        
                        ); 
        
                        //$categories = get_categories($args); 
                        $current_category=$this->el_get_woo_requests_links('el_category_id','',true);
                        
                        $categories = get_terms('product_cat',$args);
                        $option='';
                        foreach ($categories as $category) {
							
							$selected='';
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($category->term_id,$permission_value))
								continue;
								
							if(!$this->get_form_element_permission('el_category_id') &&  $permission_value!='')
								$selected="selected";
							
                            if($current_category==$category->term_id)
                                $selected="selected";
                            
                            $option .= '<option value="'.$category->term_id.'" '.$selected.'>';
                            $option .= $category->name;
                            $option .= ' ('.$category->count.')';
                            $option .= '</option>';
                        }
                    ?>
                    <select name="el_category_id[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <?php
                        	if($this->get_form_element_permission('el_category_id') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>
                <?php
					}
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_product_id');
				?>	
                 
                 	
                 
                 <?php
				 	$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_orders_status');
					if($this->get_form_element_permission('el_orders_status') ||  $permission_value!=''){
						
						if(!$this->get_form_element_permission('el_orders_status') &&  $permission_value!='')
							$col_style='display:none';
				?>
                 
                 <div class="col-md-6" style=" <?php echo $col_style;?>">
                    <div class="awr-form-title">
                        <?php _e('Status',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-check"></i></span>
                    <?php
                        $el_order_status=$this->el_get_woo_orders_statuses();

                        $option='';
                        foreach($el_order_status as $key => $value){
							
							$selected="selected";
							if(is_array($permission_value) && !in_array($key,$permission_value))
								continue;
								
							if(!$this->get_form_element_permission('el_orders_status') &&  $permission_value!='')
								$selected="selected";	
							
                            $option.="<option value='".$key."' >".$value."</option>";
                        }
                    ?>
                
                    <select name="el_orders_status[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <?php
                        	if($this->get_form_element_permission('el_orders_status') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    <input type="hidden" name="el_id_order_status[]" id="el_id_order_status" value="-1">
                </div>	
                <?php
					}
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_countries_code');
                	if($this->get_form_element_permission('el_countries_code') ||  $permission_value!=''){
						if(!$this->get_form_element_permission('el_countries_code') &&  $permission_value!='')
							$col_style='display:none'
						
				?> 
                 <div class="col-md-6" style=" <?php echo $col_style;?>">
                    <div class="awr-form-title">
                        <?php _e('Country',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-globe"></i></span>
					<?php
                        $country_data = $this->el_get_paying_woo_state('billing_country');
                        $country      	= $this->el_get_woo_countries();
                        $option='';
                        foreach($country_data as $countries){
                            $selected='';
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($countries->id,$permission_value))
								continue;
							
							if(!$this->get_form_element_permission('el_countries_code') &&  $permission_value!='')
								$selected="selected";	
							                                                    
                            $el_table_value = $country->countries[$countries->id];
                            $option.="<option value='".$countries->id."' $selected >".$el_table_value."</option>";
                        }
                        
                        $country_states = $this->el_get_woo_country_of_state();
                        $json_country_states = json_encode($country_states);
                        //print_r($json_country_states);
                    ?>
                    <select id="el_adr_country" name="el_countries_code[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <?php
                        	if($this->get_form_element_permission('el_countries_code') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    
                    <script type="text/javascript">
                        "use strict";
                        jQuery( document ).ready(function( $ ) {
                            
                            var country_state='';
                            country_state=<?php echo $json_country_states?>;	
                            
                            $("#el_adr_country").change(function(){
                                var country_val=$(this).val();

								if(country_val==null){
									return false;
								}
								
								var option_data = Array();
								var optionss = '<option value="-1">Select All</option>';
								var i = 1;
								$.each(country_state, function(key,val){

									if(country_val.indexOf(val.parent_id) >= 0 || country_val=="-1"){
										optionss += '<option value="' + val.id + '">' + val.label + '</option>';
										option_data[val.id] = val.label;
									}
									i++;
								});

								$('#el_adr_state').empty(); //remove all child nodes
								$("#el_adr_state").html(optionss);
								$('#el_adr_state').trigger("chosen:updated");
                            });
                            
                            
                            
                        });
                        
                    </script>
                    
                </div>	
                
                <?php
					}
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_states_code');
                	if($this->get_form_element_permission('el_states_code') ||  $permission_value!=''){
						if(!$this->get_form_element_permission('el_states_code') &&  $permission_value!='')
							$col_style='display:none';
				?> 

                 <div class="col-md-6" style=" <?php echo $col_style;?>">
                    <div class="awr-form-title">
                        <?php _e('State',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-map"></i></span>
					<?php
                        //$state_codes = $this->el_get_paying_woo_state('shipping_state','shipping_country');
                        //$this->el_get_woo_country_of_state();
                        //$this->el_get_woo_bsn($items->billing_country,$items->billing_state_code);
                        $state_codes = $this->el_get_paying_woo_state('billing_state','billing_country');
                        $option='';
                        foreach($state_codes as $state){
                            $selected="";	
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($state->id,$permission_value))
								continue;
							
							if(!$this->get_form_element_permission('el_states_code') &&  $permission_value!='')
								$selected="selected";	
							                                                    
                            $el_table_value = $this->el_get_woo_bsn($state->billing_country,$state->id);
                            $option.="<option $selected value='".$state->id."' >".$el_table_value." ($state->billing_country)</option>";
                        }
                    ?>
                
                    <select id="el_adr_state" name="el_states_code[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <?php
                        	if($this->get_form_element_permission('el_states_code') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                        <?php
                             echo $option;
                        ?>
                    </select>  
                    
                </div>	
           		<?php
					}
				?>
                
                 
           	 	<div class="col-md-12">
				<?php
                    $el_hide_os='trash';
                    $el_publish_order='no';
                    $el_order_item_name='';
                    $el_coupon_code='';
                    $el_coupon_codes='';
                    $el_payment_method='';
                    
                    $el_variation_only=$this->el_get_woo_requests_links('variation_only','-1',true);
                    $el_order_meta_key='';
                    
                    $data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
                    
                   
                    $el_variation_id='-1';
                    $amont_zero='';
                    
                ?>
            
                <input type="hidden" name="el_hide_os" value="<?php echo $el_hide_os;?>" />
                <input type="hidden" name="publish_order" value="<?php echo $el_publish_order;?>" />
                <input type="hidden" name="order_item_name" value="<?php echo $el_order_item_name;?>" />
                <input type="hidden" name="coupon_code" value="<?php echo $el_coupon_code;?>" />
                <input type="hidden" name="el_codes_of_coupon" value="<?php echo $el_coupon_codes;?>" />
                <input type="hidden" name="payment_method" value="<?php echo $el_payment_method;?>" />
                <input type="hidden" name="variation_id" value="<?php echo $el_variation_id; ?>" />
                <input type="hidden" name="variation_only" value="<?php echo $el_variation_only; ?>" />
                <input type="hidden" name="date_format" value="<?php echo $data_format; ?>" />
                
                <input type="hidden" name="table_names" value="<?php echo $table_name;?>"/>
                <div class="fetch_form_loading search-form-loading"></div>	
                <input type="submit" value="Search" class="button-primary"/>
                <input type="button" value="Reset" class="button-secondary form_reset_btn"/>
            </div>  
                                
        </form>
    <?php
	}
	
?>