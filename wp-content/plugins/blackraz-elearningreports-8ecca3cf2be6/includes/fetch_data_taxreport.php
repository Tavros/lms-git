<?php
	
	if($file_used=="sql_table")
	{
		
		
		//GET POSTED PARAMETERS
		$request 			= array();
		$start				= 0;
		
		
		$el_order_status		= $this->el_get_woo_requests('el_orders_status',"-1",true);
		$category_id		= $this->el_get_woo_requests('el_category_id','-1',true);
		$el_product_id			= $this->el_get_woo_requests('el_product_id','-1',true);
		$el_id_order_status	= $this->el_get_woo_requests('el_id_order_status','-1',true);	
		$el_country_code		= $this->el_get_woo_requests('el_countries_code','-1',true);
		$state_code 		= $this->el_get_woo_requests('el_states_code','-1',true);
		$el_sort_by 			= $this->el_get_woo_requests('sort_by','-1',true);
		$el_order_by 			= $this->el_get_woo_requests('order_by','DESC',true);
		
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		$el_id_order_status 	= $this->el_get_woo_requests('el_id_order_status',NULL,true);
		$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
		$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		///////////HIDDEN FIELDS////////////
		//$el_hide_os	= $this->el_get_woo_sm_requests('el_hide_os',$el_hide_os, "-1");
		$el_hide_os='"trash"';
		$el_publish_order='no';
		
		$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
		//////////////////////
		
		//DATE
		$el_from_date_condition='';
		
		//ORDER SATTUS
		$el_id_order_status_join='';
		$el_order_status_condition='';
		
		//COUTNRY
		$el_country_code_join='';
		$el_country_code_condition_1='';
		$el_country_code_condition_2='';
		
		//STATE
		$state_code_join='';
		$state_code_condition_1='';
		$state_code_condition_2='';
		
		//ORDER STATUS
		$el_id_order_status_condition='';
		
		//DATE
		$el_from_date_condition='';
		
		//PUBLISH ORDER
		$el_publish_order_condition='';
		
		//HIDE ORDER STATUS
		$el_hide_os_condition ='';
		
		$sql_columns = "
		SUM(el_woocommerce_order_itemmeta_tax_amount.meta_value)  AS _order_tax,
		SUM(el_woocommerce_order_itemmeta_shipping_tax_amount.meta_value)  AS _shipping_tax_amount,
		
		SUM(el_postmeta1.meta_value)  AS _order_shipping_amount,
		SUM(el_postmeta2.meta_value)  AS _order_total_amount,
		COUNT(el_posts.ID)  AS _order_count,
		
		el_woocommerce_order_items.order_item_name as tax_rate_code, 
		el_woocommerce_tax_rates.tax_rate_name as tax_rate_name, 
		el_woocommerce_tax_rates.tax_rate as order_tax_rate, 
		
		el_woocommerce_order_itemmeta_tax_amount.meta_value AS order_tax,
		el_woocommerce_order_itemmeta_shipping_tax_amount.meta_value AS shipping_tax_amount,
		el_postmeta1.meta_value as order_shipping_amount,
		el_postmeta2.meta_value as order_total_amount,
		el_postmeta3.meta_value 		as billing_state,
		el_postmeta4.meta_value 		as billing_country
		";
		
		$sql_columns .= ", CONCAT(el_woocommerce_order_items.order_item_name,'-',el_woocommerce_tax_rates.tax_rate_name,'-',el_woocommerce_tax_rates.tax_rate,'-',el_postmeta4.meta_value,'',el_postmeta3.meta_value) as group_column";
		
		
		$sql_joins = "{$wpdb->prefix}woocommerce_order_items as el_woocommerce_order_items";
				
		if(($el_id_order_status  && $el_id_order_status != '-1') || $el_sort_by == "status"){
			$el_id_order_status_join = " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_posts.ID
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id";
			
			if($el_sort_by == "status"){
				$el_id_order_status_join .= " LEFT JOIN  {$wpdb->prefix}terms 				as el_terms 				ON el_terms.term_id					=	term_taxonomy.term_id";
			}
		}
			
		$sql_joins .= "$el_id_order_status_join LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta1 ON el_postmeta1.post_id=el_woocommerce_order_items.order_id
		LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta2 ON el_postmeta2.post_id=el_woocommerce_order_items.order_id
		LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta_tax ON el_woocommerce_order_itemmeta_tax.order_item_id=el_woocommerce_order_items.order_item_id
		LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta_tax_amount ON el_woocommerce_order_itemmeta_tax_amount.order_item_id=el_woocommerce_order_items.order_item_id
		LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta_shipping_tax_amount ON el_woocommerce_order_itemmeta_shipping_tax_amount.order_item_id=el_woocommerce_order_items.order_item_id
		LEFT JOIN  {$wpdb->prefix}woocommerce_tax_rates as el_woocommerce_tax_rates ON el_woocommerce_tax_rates.tax_rate_id=el_woocommerce_order_itemmeta_tax.meta_value
		LEFT JOIN  {$wpdb->prefix}posts as el_posts ON el_posts.ID=	el_woocommerce_order_items.order_id
		LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta3 ON el_postmeta3.post_id=el_woocommerce_order_items.order_id
		LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta4 ON el_postmeta4.post_id=el_woocommerce_order_items.order_id";
		
		if($el_country_code and $el_country_code != '-1')	
			$el_country_code_join = " LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta5 ON el_postmeta5.post_id=el_posts.ID";
		
		if($state_code and $state_code != '-1')	
			$state_code_join = " LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta_billing_state ON el_postmeta_billing_state.post_id=el_posts.ID";
		
		$sql_joins.="$el_country_code_join $state_code_join";
		
		$sql_condition = " el_postmeta1.meta_key = '_order_shipping' AND el_woocommerce_order_items.order_item_type = 'tax'
		AND el_posts.post_type='shop_order' 
		AND el_postmeta2.meta_key='_order_total'
		AND el_woocommerce_order_itemmeta_tax.meta_key='rate_id'
		AND el_woocommerce_order_itemmeta_tax_amount.meta_key='tax_amount'
		AND el_woocommerce_order_itemmeta_shipping_tax_amount.meta_key='shipping_tax_amount'
		AND el_postmeta3.meta_key='_billing_state'
		AND el_postmeta4.meta_key='_billing_country'";
		
		if($el_id_order_status  && $el_id_order_status != '-1') 
			$el_id_order_status_condition = " AND term_taxonomy.term_id IN (".$el_id_order_status .")";
		
		if($el_country_code and $el_country_code != '-1')	
			$el_country_code_condition_1 = " AND el_postmeta5.meta_key='_billing_country'";
		
		if($state_code and $state_code != '-1')		
			$state_code_condition_1 = " AND el_postmeta_billing_state.meta_key='_billing_state'";
		
		if($el_country_code and $el_country_code != '-1')	
			$el_country_code_condition_2 = " AND el_postmeta5.meta_value IN (".$el_country_code.")";
		
		if($state_code and $state_code != '-1')	
			$state_code_condition_2 = " AND el_postmeta_billing_state.meta_value IN (".$state_code.")";
		
		if($el_order_status  && $el_order_status != '-1' and $el_order_status != "'-1'")
			$el_order_status_condition = " AND el_posts.post_status IN (".$el_order_status.")";
			
		if ($el_from_date != NULL &&  $el_to_date !=NULL){
			$el_from_date_condition = " AND (DATE(el_posts.post_date) BETWEEN '".$el_from_date."' AND '". $el_to_date ."')";
		}	

		if($el_hide_os  && $el_hide_os != '-1' and $el_hide_os != "'-1'")
			$el_hide_os_condition = " AND el_posts.post_status NOT IN (".$el_hide_os.")";
		
		$sql_group_by = "  group by group_column";
	
		$sql_order_by = "  ORDER BY (el_woocommerce_tax_rates.tax_rate + 0)  ASC";
		
		$sql = "SELECT $sql_columns
				FROM $sql_joins $el_id_order_status_join
				WHERE $sql_condition
				$el_id_order_status_condition $el_country_code_condition_1 $state_code_condition_1
				$el_country_code_condition_2 $state_code_condition_2
				$el_from_date_condition $el_publish_order_condition
				$el_order_status_condition $el_hide_os_condition  $el_from_date_condition
				$sql_group_by $sql_order_by";
		
		//echo $sql;	
		
	}
	elseif($file_used=="data_table"){
		
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
			$datatable_value.=("<tr>");
				//Tax Name
				$display_class='';
				if($this->table_cols[0]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->tax_rate_name;
				$datatable_value.=("</td>");
	
				//Tax Rate
				$el_table_value=$items->order_tax_rate;
				
				$display_class='';
				if($this->table_cols[1]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $el_table_value = sprintf("%.2f%%",$el_table_value);
				$datatable_value.=("</td>");
				
				//Order Count
				$display_class='';
				if($this->table_cols[2]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->_order_count;
				$datatable_value.=("</td>");
				
				//Shipping Amt.
				$display_class='';
				if($this->table_cols[3]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($items->_order_shipping_amount);
				$datatable_value.=("</td>");
				
				//Gross Amt.
				$display_class='';
				if($this->table_cols[4]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($this->el_get_number_percentage($items->_order_tax,$items->order_tax_rate));
				$datatable_value.=("</td>");
								
				//Net Amt.
				$display_class='';
				if($this->table_cols[5]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($items->_order_total_amount);
				$datatable_value.=("</td>");
				
				//Shipping Tax
				$display_class='';
				if($this->table_cols[6]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($items->shipping_tax_amount);
				$datatable_value.=("</td>");
				
				//Order Tax
				$display_class='';
				if($this->table_cols[7]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($items->_order_tax);
				$datatable_value.=("</td>");
				       
				//Total Tax
				$display_class='';
				if($this->table_cols[8]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($items->_shipping_tax_amount + $items->_order_tax);
				$datatable_value.=("</td>");
										
			$datatable_value.=("</tr>");
		}
	}elseif($file_used=="search_form"){
	?>
		<form class='alldetails search_form_report' action='' method='post'>
            <input type='hidden' name='action' value='submit-form' />
            <div class="row">
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('From Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_from_date" id="pwr_from_date" type="text" readonly='true' class="datepick"/>
                </div>
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('To Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_to_date" id="pwr_to_date" type="text" readonly='true' class="datepick"/>
                    
                    <input type="hidden" name="el_id_order_status[]" id="el_id_order_status" value="-1">
                    <input type="hidden" name="el_orders_status[]" id="order_status" value="<?php echo $this->el_shop_status; ?>">
                </div>
                
            </div>
            
            <div class="col-md-12">
                    <?php
                    	$el_hide_os='trash';
						$el_publish_order='no';
						
						$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
					?>
                    <input type="hidden" name="list_parent_category" value="">
                    <input type="hidden" name="el_category_id" value="-1">
                    
                	<input type="hidden" name="el_hide_os" id="el_hide_os" value="<?php echo $el_hide_os;?>" />
                    
                    <input type="hidden" name="date_format" id="date_format" value="<?php echo $data_format;?>" />
                
                	<input type="hidden" name="table_names" value="<?php echo $table_name;?>"/>
                    <div class="fetch_form_loading search-form-loading"></div>	
                    <input type="submit" value="Search" class="button-primary"/>
					<input type="button" value="Reset" class="button-secondary form_reset_btn"/>	
            </div>  
                                
        </form>
    <?php
	}
	
?>