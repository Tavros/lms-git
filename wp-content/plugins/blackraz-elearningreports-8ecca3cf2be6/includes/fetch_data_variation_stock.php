<?php
	
	if($file_used=="sql_table")
	{
		$el_product_id		= $this->el_get_woo_requests('el_product_id','-1',true);
		$category_id	= $this->el_get_woo_requests('el_category_id','-1',true);
		$el_product_subtype= $this->el_get_woo_requests('el_sub_product_type','-1',true);
		$el_sku_number= $this->el_get_woo_requests('el_sku_no','-1',true);
		$el_product_sku= $this->el_get_woo_requests('el_sku_products','-1',true);
		$el_manage_stock= $this->el_get_woo_requests('el_stock_manage','-1',true);
		$el_stock_status= $this->el_get_woo_requests('el_status_of_stock','-1',true);
		$el_product_stock = $this->el_get_woo_requests('el_stock_product','-1',true);
		$el_txt_min_stock = $this->el_get_woo_requests('el_stock_min','-1',true);
		$el_txt_max_stock  = $this->el_get_woo_requests('el_stock_max','-1',true);
		
		$el_zero_stock = $this->el_get_woo_requests('el_stock_zero','no',true);
		$el_product_type= $this->el_get_woo_requests('el_products_type','-1',true);
		$el_zero_sold = $this->el_get_woo_requests('zero_sold','-1',true);
		$el_product_name = $this->el_get_woo_requests('el_name_of_product','-1',true);
		$el_basic_column = $this->el_get_woo_requests('el_general_cols','no',true);
		$el_zero_stock = $this->el_get_woo_requests('el_stock_zero','no',true);
		
		$el_show_cog		= $this->el_get_woo_requests('el_show_cog','no',true);

		$el_variations			= $this->el_get_woo_requests('el_variations','-1',true);
		$el_variation_sku 		= $this->el_get_woo_requests('el_sku_variations','-1',true);	
		if($el_variation_sku != NULL  && $el_variation_sku != '-1'){
			$el_variation_sku  		= "'".str_replace(",","','",$el_variation_sku)."'";
		}


		$el_product_sku		= $this->el_get_woo_sm_requests('el_sku_products',$el_product_sku, "-1");
		
		$el_manage_stock		= $this->el_get_woo_sm_requests('el_stock_manage',$el_manage_stock, "-1");
		
		$el_stock_status		= $this->el_get_woo_sm_requests('el_status_of_stock',$el_stock_status, "-1");
		
		
			
		//GET POSTED PARAMETERS
			
		$el_product_sku 		= $this->el_get_woo_requests('el_sku_products','-1',true);	
		if($el_product_sku != NULL  && $el_product_sku != '-1'){
			$el_product_sku  		= "'".str_replace(",","','",$el_product_sku)."'";
		}
		
		
		$page				= $this->el_get_woo_requests('page',NULL);	
		
		$report_name 		= apply_filters($page.'_default_report_name', 'product_page');
		$optionsid			= "per_row_variation_page";

		$report_name 		= $this->el_get_woo_requests('report_name',$report_name,true);
		$admin_page			= $this->el_get_woo_requests('admin_page',$page,true);
		$category_id		= $this->el_get_woo_requests('el_category_id','-1',true);
		$el_id_order_status 	= $this->el_get_woo_requests('el_id_order_status',NULL,true);
		$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
		$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		$el_hide_os='"trash"';
		$el_hide_os	= $this->el_get_woo_requests('el_hide_os',$el_hide_os,true);
		$el_product_id			= $this->el_get_woo_requests('el_product_id','-1',true);
		
		//GET POSTED PARAMETERS
		$start				= 0;
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		$el_product_id			= $this->el_get_woo_requests('el_product_id',"-1",true);
		$category_id 		= $this->el_get_woo_requests('el_category_id','-1',true);
		if($el_variations && $el_variations!='-1')
		{
			$el_variations = explode(",",$el_variations);
			//$this->print_array($el_variations);
			$var = array();
			foreach($el_variations as $key => $value):
				$var[] .=  "attribute_pa_".$value;
				$var[] .=  "attribute_".$value;	
			endforeach;
			$el_variations =  implode("', '",$var);
		}
		
		//$el_item_meta_key =  implode("', '",$item_att);
		//print_r($el_variations);
		$el_variation_attributes='';
		if($el_variations && $el_variations!='-1')
			$el_variation_attributes= $el_variations;
		
		
		///////////HIDDEN FIELDS////////////
		//$el_hide_os	= $this->el_get_woo_sm_requests('el_hide_os',$el_hide_os, "-1");
		$el_hide_os='"trash"';
		$el_publish_order='no';
		
		$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
		//////////////////////	
		
		
		//PRODUCT SUBTYPE
		$el_product_subtype_join='';
		$el_product_subtype_condition_1='';
		$el_product_subtype_condition_2='';
		
		//SKU NUMBER
		$el_sku_number_join='';
		$el_sku_number_condition_1='';
		$el_sku_number_condition_2='';
		
		//PRODUCT NAME
		$el_product_name_condition='';
		
		//PRODUCT ID
		$el_product_id_condition='';
		
		//PRODUCT SKU
		$el_product_sku_join='';
		$el_product_sku_condition_1='';
		$el_product_sku_condition_2='';
		
		//VARIATION SKU
		$el_variation_sku_join='';
		$el_variation_sku_condition_1='';
		$el_variation_sku_condition_2='';
		
		//PRODUCT STOCK
		$el_product_stock_join='';
		$el_product_stock_condition_1='';
		$el_product_stock_condition_2='';

		//CATEGORY ID
		$category_id_join='';
		$category_id_condition='';
		
		//STOCK STATUS
		$el_stock_status_join='';
		$el_stock_status_condition='';
		
		//MANAGE STOCK
		$el_manage_stock_join='';
		$el_manage_stock_condition='';
		
		//VARIATION ATTRIBUTE
		$el_variation_attributes_join='';
		$el_variation_attributes_condition='';
		
		//MAX,MIN,ZERO STOCK
		$el_txt_min_stock_condition='';
		$el_txt_max_stock_condition ='';
		$el_zero_stock_condition='';
		
		//SOLD
		$sold_variation_ids_condition='';
		
		
		$sql_columns = " 
		el_posts.ID as id
		,el_posts.post_title as variation_name						
		,el_posts.ID as variation_id
		,el_posts.post_date as product_date
		,el_posts.post_modified as modified_date												
		,el_products.ID as product_id
		,el_products.post_title as product_name
		,el_posts.post_parent AS variation_parent_id";
						
		$sql_joins = " {$wpdb->prefix}posts as el_posts LEFT JOIN {$wpdb->prefix}posts as el_products ON el_products.ID = el_posts.post_parent";
		
		if($el_product_subtype=="virtual") 						
			$el_product_subtype_join = " LEFT JOIN  {$wpdb->prefix}postmeta as el_virtual 			ON el_virtual.post_id			=el_posts.ID";
		
		if($el_product_subtype=="downloadable") 					
			$el_product_subtype_join= " LEFT JOIN  {$wpdb->prefix}postmeta as el_downloadable		ON el_downloadable.post_id		=el_posts.ID";
		
		if($el_sku_number){
			$el_sku_number_join= " LEFT JOIN  {$wpdb->prefix}postmeta as el_product_sku 				ON el_product_sku.post_id					=	el_posts.post_parent";
			$el_sku_number_join .= " LEFT JOIN  {$wpdb->prefix}postmeta as el_variation_sku 				ON el_variation_sku.post_id				=	el_posts.ID";
		}else{
			if($el_product_sku and $el_product_sku != '-1'){
				$el_product_sku_join= " LEFT JOIN  {$wpdb->prefix}postmeta as el_product_sku 				ON el_product_sku.post_id				=	el_posts.post_parent";
			}
			
			if($el_variation_sku and $el_variation_sku != '-1'){
				$el_variation_sku_join= " LEFT JOIN  {$wpdb->prefix}postmeta as el_variation_sku 				ON el_variation_sku.post_id			=	el_posts.ID";
			}
		}
		
		if($el_product_stock || $el_txt_min_stock || $el_txt_max_stock || strlen($el_product_stock) >0 || $el_zero_stock) 		$el_product_stock_join = " LEFT JOIN  {$wpdb->prefix}postmeta as el_stock 				ON el_stock.post_id			=el_posts.ID";
		
		if($category_id and $category_id != "-1"){
			$category_id_join = " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 		as el_term_relationships 	ON el_term_relationships.object_id			=el_posts.post_parent
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 			as term_taxonomy 		ON term_taxonomy.term_taxonomy_id		=el_term_relationships.term_taxonomy_id
			LEFT JOIN  {$wpdb->prefix}terms 					as el_terms 				ON el_terms.term_id						=term_taxonomy.term_id";
		}
		
		if($el_stock_status and $el_stock_status != '-1') 
			$el_stock_status_join = " LEFT JOIN  {$wpdb->prefix}postmeta as el_stock_status 			ON el_stock_status.post_id			=el_posts.ID";
			
		if($el_manage_stock and $el_manage_stock != '-1') 
			$el_manage_stock_join= " LEFT JOIN  {$wpdb->prefix}postmeta as el_manage_stock 			ON el_manage_stock.post_id			=el_posts.ID";
		
		if($el_variation_attributes != "-1" and strlen($el_variation_attributes)>2)
			$el_variation_attributes_join = " LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta_variation ON el_postmeta_variation.post_id = el_posts.ID";
		
		$sql_condition= "el_posts.post_type='product_variation' AND el_posts.post_status = 'publish' AND el_products.post_type='product' AND el_posts.post_parent > 0";
		
		if($el_product_stock || $el_txt_min_stock || $el_txt_max_stock || strlen($el_product_stock) >0 || $el_zero_stock == "no") 		$el_product_stock_condition_1 = " AND el_stock.meta_key ='_stock'";
		
		if($el_sku_number){
			$el_sku_number_condition_1 = " AND el_product_sku.meta_key ='_sku' AND el_variation_sku.meta_key ='_sku'";
		}else{
			if($el_product_sku and $el_product_sku != '-1'){
				$el_product_sku_condition_1 = " AND el_product_sku.meta_key ='_sku'";
			}
			
			if($el_variation_sku and $el_variation_sku != '-1'){
				$el_variation_sku_condition_1 = " AND el_variation_sku.meta_key ='_sku'";
			}
		}
		
		if($el_product_subtype=="downloadable") 		
			$el_product_subtype_condition_1 = " AND el_downloadable.meta_key ='_downloadable'";					
		
		if($el_product_subtype=="virtual") 			
			$el_product_subtype_condition_1 = " AND el_virtual.meta_key ='_virtual'";					
		
		if($el_product_name) 							
			$el_product_name_condition = " AND el_posts.post_title like '%{$el_product_name}%'";
		
		if($el_product_id and $el_product_id >0) 			
			$el_product_id_condition = " AND el_posts.post_parent IN ({$el_product_id})";
		
		if(strlen($el_product_stock) >0){
			if($el_product_stock == 0){
				$el_product_stock_condition_2 = " AND el_stock.meta_value = '{$el_product_stock}'";
			}elseif($el_product_stock >= 1){
				$el_product_stock_condition_2 = " AND el_stock.meta_value = '{$el_product_stock}'";
			}
		}
		
		if($el_txt_min_stock) 							
			$el_txt_min_stock_condition = " AND el_stock.meta_value >= {$el_txt_min_stock}";
			
		if($el_txt_max_stock) 							
			$el_txt_max_stock_condition = " AND el_stock.meta_value <= {$el_txt_max_stock}";									
			
		if($el_product_subtype=="downloadable") 		
			$el_product_subtype_condition_2 = " AND el_downloadable.meta_value = 'yes'";					
			
		if($el_product_subtype=="virtual") 			
			$el_product_subtype_condition_2= " AND el_virtual.meta_value = 'yes'";					
			
		if($category_id and $category_id != "-1") 	
			$category_id_condition = " AND el_terms.term_id = {$category_id}";
		
		
		
		if($el_sku_number){
			$el_sku_number_condition_2 = " AND (  ( el_product_sku.meta_value like '%{$el_sku_number}%'  OR el_variation_sku.meta_value like '%{$el_sku_number}%' )";
			
			if(($el_product_sku and $el_product_sku != '-1') and ($el_variation_sku and $el_variation_sku != '-1')){
				$el_product_sku_condition_2= " AND ( el_product_sku.meta_value IN ($el_product_sku) AND el_variation_sku.meta_value IN ($el_variation_sku) )";
			}else{
				
				if($el_product_sku and $el_product_sku != '-1'){
					$el_product_sku_condition_2 = " AND el_product_sku.meta_value IN ($el_product_sku)";
				}
				
				if($el_variation_sku and $el_variation_sku != '-1'){
					$el_variation_sku_condition_2 = " AND el_variation_sku.meta_value IN ($el_variation_sku)";
				}
			}
			
			
			$el_sku_number_condition_2 .= $el_product_sku_condition_2.$el_variation_sku_condition_2. " )";
			
		}else{
			
			if(($el_product_sku and $el_product_sku != '-1') and ($el_variation_sku and $el_variation_sku != '-1')){
				$el_product_sku_condition_2= " AND ( el_product_sku.meta_value IN ($el_product_sku) AND el_variation_sku.meta_value IN ($el_variation_sku) )";
			}else{
				if($el_product_sku and $el_product_sku != '-1'){
					$el_product_sku_condition_2 = " AND el_product_sku.meta_value IN ($el_product_sku)";
				}
				
				if($el_variation_sku and $el_variation_sku != '-1'){
					$el_variation_sku_condition_2 = " AND el_variation_sku.meta_value IN ($el_variation_sku)";
				}
			}
			
			$el_sku_number_condition_2 .= $el_product_sku_condition_2.$el_variation_sku_condition_2;
		}
		
		
		if($el_zero_stock == "no")	
			$el_zero_stock_condition = " AND (el_stock.meta_value > 0 OR LENGTH(TRIM(el_stock.meta_value)) > 0)";
	
		if($el_stock_status and $el_stock_status != '-1')		
			$el_stock_status_condition = " AND el_stock_status.meta_key ='_stock_status' AND el_stock_status.meta_value IN ({$el_stock_status})";
			
		if($el_manage_stock and $el_manage_stock != '-1')		
			$el_manage_stock_condition = " AND el_manage_stock.meta_key ='_manage_stock' AND el_manage_stock.meta_value IN ({$el_manage_stock})";
		
		if($el_zero_sold=="yes"){
			if(strlen($sold_variation_ids)>0){
				$sold_variation_ids_condition = " AND el_posts.ID NOT IN ($sold_variation_ids)";
			}
		}
		
		if($el_variation_attributes != "-1" and strlen($el_variation_attributes)>2){
			$el_variation_attributes_condition = " AND el_postmeta_variation.meta_key IN ('{$el_variation_attributes}')";
		}
		
		$sql_group_by = " GROUP BY el_posts.ID";
		
		$sql_order_by = " ORDER BY el_posts.post_parent ASC, el_posts.post_title ASC";
		
		
		$sql="SELECT $sql_columns 
			FROM 
			$sql_joins $el_product_subtype_join $el_sku_number_join $el_product_sku_join
			$el_variation_sku_join $el_product_stock_join $category_id_join $el_stock_status_join
			$el_manage_stock_join $el_variation_attributes_join 
			WHERE $sql_condition
			$el_product_stock_condition_1 $el_sku_number_condition_1 $el_product_sku_condition_1
			$el_variation_sku_condition_1 $el_product_subtype_condition_1 $el_product_name_condition
			$el_product_id_condition $el_product_stock_condition_2 $el_txt_min_stock_condition
			$el_txt_max_stock_condition $el_product_subtype_condition_2 $category_id_condition
			$el_sku_number_condition_2 $el_zero_stock_condition $el_stock_status_condition
			$el_manage_stock_condition $sold_variation_ids_condition $el_variation_attributes_condition
			$sql_group_by $sql_order_by";
	
		//echo $sql;
		
		///////////////////
		//EXTRA COLUMNS
		$this->table_cols =$this->table_columns($table_name);
		
		$variation_cols_arr='';
		if($el_basic_column=='yes'){
			$variation_cols_arr[] = array('lable'=>'Variation ID','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'SKU','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Variation SKU','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Product Name','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Category','status'=>'show');
			
			//variation columns
			$data_variation='';
			
			$attributes_available=$this->el_get_woo_pv_atts('yes');
			$el_variation_attributes 	= $this->el_get_woo_requests('el_variations',NULL,true);
			
			$variation_sel_arr	= '';
			if($el_variation_attributes != NULL  && $el_variation_attributes != '-1')
			{
				$el_variation_attributes = str_replace("','", ",",$el_variation_attributes);
				$variation_sel_arr = explode(",",$el_variation_attributes);
			}
			
			foreach($attributes_available as $key => $value){
				$new_key = str_replace("wcv_","",$key);
				if($el_variation_attributes=='-1' || is_array($variation_sel_arr) && in_array($new_key,$variation_sel_arr))
				{
					$data_variation[]=$new_key;
					$variation_cols_arr[] = array('lable'=>$value,'status'=>'show');
				}
			}
			$this->data_variation=$data_variation;
			
			$variation_cols_arr[] = array('lable'=>'Stock','status'=>'show');
			//$variation_cols_arr[] = array('lable'=>'Edit','status'=>'show');
			
		}else if($el_basic_column!='yes'){
			
			$variation_cols_arr[] = array('lable'=>'Variation ID','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'SKU','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Variation SKU','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Product Name','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Category','status'=>'show');
			
			$variation_cols_arr[] = array('lable'=>'Created Date','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Modified Date','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Downloadable','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Virtual','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Manage Stock','status'=>'show');
			$variation_cols_arr[] = array('lable'=>'Backorders','status'=>'show');
			
			$variation_cols_arr[] = array('lable'=>'Regular Price','status'=>'currency');
			$variation_cols_arr[] = array('lable'=>'Sale Price','status'=>'currency');
			
			//CHECK IF COG IS ENABLE
			if($el_show_cog=='yes'){
				$variation_cols_arr[] = array('lable'=>'Cog','status'=>'currency');
			}
			
			//variation columns
			$data_variation='';
			
			$attributes_available=$this->el_get_woo_pv_atts('yes');
			$el_variation_attributes 	= $this->el_get_woo_requests('el_variations',NULL,true);
			
			$variation_sel_arr	= '';
			if($el_variation_attributes != NULL  && $el_variation_attributes != '-1')
			{
				$el_variation_attributes = str_replace("','", ",",$el_variation_attributes);
				$variation_sel_arr = explode(",",$el_variation_attributes);
			}
			
			foreach($attributes_available as $key => $value){
				$new_key = str_replace("wcv_","",$key);
				if($el_variation_attributes=='-1' || is_array($variation_sel_arr) && in_array($new_key,$variation_sel_arr))
				{
					$data_variation[]=$new_key;
					$variation_cols_arr[] = array('lable'=>$value,'status'=>'show');
				}
			}
			$this->data_variation=$data_variation;
			
			$variation_cols_arr[] = array('lable'=>'Stock','status'=>'show');
			//$variation_cols_arr[] = array('lable'=>'Edit','status'=>'show');
		}
		
		$this->table_cols = $variation_cols_arr;
		
		
		
	}elseif($file_used=="data_table"){
		
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
			$datatable_value.=("<tr>");
				
				$el_basic_column = $this->el_get_woo_requests('el_general_cols','-1',true);
				$product_details=$this->el_get_full_post_meta($items->variation_id);
				$product_details_prod=$this->el_get_full_post_meta($items->product_id);
				
				/*if($items->variation_id=='134')
					print_r($product_details_prod);
					
					*/
				
				if($el_basic_column=='yes'){
					
					//Variation ID
					$display_class='';
					if($this->table_cols[0]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $items->variation_id;
					$datatable_value.=("</td>");
					
					//SKU
					$display_class='';
					if($this->table_cols[1]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $product_details_prod['sku'];
					$datatable_value.=("</td>");
					
					//Variation SKU
					$display_class='';
					if($this->table_cols[2]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.=  $product_details['sku'];
					$datatable_value.=("</td>");
					
					//Product Name
					$display_class='';
					if($this->table_cols[3]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $items->product_name;
					$datatable_value.=("</td>");
					
					//Category
					$display_class='';
					if($this->table_cols[4]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $this->el_get_cn_product_id($items->product_id,"product_cat");
					$datatable_value.=("</td>");
					
					//Variation Columns
					$j=5;
					$variation_arr='';
					$variation = $this->el_get_woo_prod_var($items->variation_id);
					foreach($variation as $var){
						
						$variation_arr=$var;
					}
					
					
					foreach($this->data_variation as $variation_name){
						$el_table_value=(!empty($variation_arr[$variation_name]) ? $variation_arr[$variation_name] : "-");
						$display_class='';
						if($this->table_cols[$j++]['status']=='hide') $display_class='display:none';
						$datatable_value.=("<td style='".$display_class."'>");
							$datatable_value.= urldecode($el_table_value); //$this->el_get_woo_variation($items->order_item_id);
						$datatable_value.=("</td>");	
					}
					
					
					//Stock
					$display_class='';
					if($this->table_cols[$j++]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= isset($product_details['stock']) && $product_details['stock']!='' ? round($product_details['stock']) : "0";
					$datatable_value.=("</td>");
					
					//Edit
					/*$display_class='';
					if($this->table_cols[$j++]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= 'Edit';
					$datatable_value.=("</td>");*/
					
				}else if($el_basic_column!='yes'){
					
					//Variation ID
					$display_class='';
					if($this->table_cols[0]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $items->variation_id;
					$datatable_value.=("</td>");
					
					//SKU
					$display_class='';
					if($this->table_cols[1]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.=  $product_details_prod['sku'];
					$datatable_value.=("</td>");
					
					//Variation SKU
					$display_class='';
					if($this->table_cols[2]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.=  $product_details['sku'];
					$datatable_value.=("</td>");
					
					//Product Name
					$display_class='';
					if($this->table_cols[3]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $items->product_name;
					$datatable_value.=("</td>");
					
					//Category
					$display_class='';
					if($this->table_cols[4]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $this->el_get_cn_product_id($items->product_id,"product_cat");
					$datatable_value.=("</td>");
					
					//Create Date
					$date_format	= get_option( 'date_format' );
					$display_class='';
					if($this->table_cols[5]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= date($date_format,strtotime($items->product_date));
					$datatable_value.=("</td>");
					
					//Modified Date
					$display_class='';
					if($this->table_cols[6]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= date($date_format,strtotime($items->modified_date));
					$datatable_value.=("</td>");
					
					//Downloadable
					$downloadable=ucwords($product_details['downloadable']);
					if($downloadable=='')
						$downloadable='NO';
					
					$display_class='';
					if($this->table_cols[7]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $downloadable;
					$datatable_value.=("</td>");
					
					//Virtual
					$virtual=ucwords($product_details['virtual']);
					if($virtual=='')
						$virtual='NO';
						
					$display_class='';
					if($this->table_cols[8]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $virtual;
					$datatable_value.=("</td>");
					
					//Manage Stock
					$el_manage_stock=isset($product_details['manage_stock']) ? $product_details['manage_stock'] :'';
					$el_manage_stock=ucwords($el_manage_stock);
					if($el_manage_stock=='')
						$el_manage_stock='NO';
						
					$display_class='';
					if($this->table_cols[9]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $el_manage_stock;
					$datatable_value.=("</td>");
					
					//Backorders
					$display_class='';
					if($this->table_cols[10]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$backorders=isset($product_details['backorders']) ? $product_details['backorders'] :"";
						$datatable_value.= ((isset($product_details['backorders']) && $product_details['backorders']=='no') || (isset($product_details['backorders']) && $product_details['backorders']=='')) ? "Do not allow" : $backorders;
					$datatable_value.=("</td>");
					
					
					$regular_price='';
					$sale_price='';
					
					$regular_price=$product_details['regular_price'];
					$sale_price=$product_details['sale_price'];
					//$cost_price=$product_details[substr(__PW_COG__,1)];
					$cost_price=get_post_meta($items->variation_id,__PW_COG__,true);
					
					//Regualr Price
					$display_class='';
					if($this->table_cols[11]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $this->price($regular_price);
					$datatable_value.=("</td>");
					
					//Sale Price
					$display_class='';
					if($this->table_cols[11]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $this->price($sale_price);
					$datatable_value.=("</td>");
					
					
					//COST Price
					$el_show_cog= $this->el_get_woo_requests('el_show_cog',"no",true);	
					if($el_show_cog=='yes'){
						$display_class='';
						if($this->table_cols[11]['status']=='hide') $display_class='display:none';
						$datatable_value.=("<td style='".$display_class."'>");
							$datatable_value.= $this->price($cost_price);
						$datatable_value.=("</td>");
					}
					
					//Variation Columns
					$j=12;
					$variation_arr='';
					$variation = $this->el_get_woo_prod_var($items->variation_id);
					foreach($variation as $var){
						
						$variation_arr=$var;
					}
					
					
					foreach($this->data_variation as $variation_name){
						$el_table_value=(!empty($variation_arr[$variation_name]) ? $variation_arr[$variation_name] : "-");
						$display_class='';
						if($this->table_cols[$j++]['status']=='hide') $display_class='display:none';
						$datatable_value.=("<td style='".$display_class."'>");
							$datatable_value.= urldecode($el_table_value); //$this->el_get_woo_variation($items->order_item_id);
						$datatable_value.=("</td>");	
					}
					
					
					//Stock
					$display_class='';
					if($this->table_cols[$j++]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= (isset($product_details['stock']) && $product_details['stock']!='') ? round($product_details['stock']) : "0";
					$datatable_value.=("</td>");
					
					//Edit
					/*$display_class='';
					if($this->table_cols[$j++]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= 'Edit';
					$datatable_value.=("</td>");*/
					
				}
				
			$datatable_value.=("</tr>");
		}
	}elseif($file_used=="search_form"){
	?>
		<form class='alldetails search_form_report' action='' method='post'>
            <input type='hidden' name='action' value='submit-form' />
            <div class="row">
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('SKU No',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-check"></i></span>
                    <input name="el_sku_no" type="text" class="sku_no"/>
                </div>

                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Product Name',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-cog"></i></span>
                    <input name="el_name_of_product" type="text" class="el_name_of_product"/>
                </div>

                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Min Stock',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-battery-0"></i></span>
                    <input name="el_stock_min" type="text" class="el_stock_min"/>
                </div>

                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Max Stock',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-battery-4"></i></span>
                    <input name="el_stock_max" type="text" class="el_stock_max"/>
                </div>

                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Product Stock',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-cog"></i></span>
                    <input name="el_stock_product" type="text" class="el_stock_product"/>
                </div>

                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Show all sub-types',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-check"></i></span>
                    <select name="el_sub_product_type" id="el_sub_product_type" >
                        <option value="">Show all sub-types</option>
                        <option value="downloadable">Downloadable</option>
                        <option value="virtual">Virtual</option>
                    </select>
                </div>

                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Stock Status',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-check"></i></span>
                    <select name="el_status_of_stock" id="el_status_of_stock" class="el_status_of_stock">
                        <option value="-1">All</option>
                        <option value="instock">In stock</option>
                        <option value="outofstock">Out of stock</option>
                    </select>
                </div>

                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Manage Stock',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-crop"></i></span>
                    <select name="el_stock_manage" id="el_stock_manage" class="el_stock_manage">
                        <option value="-1">All</option>
                        <option value="yes">Include items whose stock is mannaged</option>
                        <option value="no">Include items whose stock is not mannaged</option>
                    </select>
                </div>
                
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Product',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-cog"></i></span>
					<?php
                        //$product_data = $this->el_get_product_woo_data('variable');
                       // $products=$this->el_get_product_woo_data('0');
					   $products = $this->el_get_var_product_dropdown('product','publish');
                        $option='';
                        
                        
                        foreach($products as $product){
                            
                            $option.="<option value='".$product -> id."' >".$product -> label." </option>";
                        }
                        
                        
                    ?>
                    <select id="el_adr_product" name="el_product_id[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                 
                 <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Category',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-tags"></i></span>
					<?php
                        $args = array(
                            'orderby'                  => 'name',
                            'order'                    => 'ASC',
                            'hide_empty'               => 1,
                            'hierarchical'             => 0,
                            'exclude'                  => '',
                            'include'                  => '',
                            'child_of'          		 => 0,
                            'number'                   => '',
                            'pad_counts'               => false 
                        
                        ); 
						
						
                        //$categories = get_categories($args); 
                        $current_category=$this->el_get_woo_requests_links('el_category_id','',true);
                        
                        $categories = get_terms('product_cat',$args);
                        $option='';
                        foreach ($categories as $category) {
                            $selected='';
                            if($current_category==$category->term_id)
                                $selected="selected";
                            
                            $option .= '<option value="'.$category->term_id.'" '.$selected.'>';
                            $option .= $category->name;
                            $option .= ' ('.$category->count.')';
                            $option .= '</option>';
                        }
						
						
						$categories = $this->el_get_var_category_dropdown('product_cat','no',false);
						$option='';
                        foreach($categories as $category){
                            
                            $option.="<option value='".$category -> id."' >".$category -> label." </option>";
                        }
						
                    ?>
                    <select name="el_category_id[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                 
                <?php
					$el_variation_sku_data = $this->el_get_woo_var_sku();									
					if($el_variation_sku_data){
				?>
                
                <div class="col-md-6">
                	<div class="awr-form-title">
						<?php _e('Variation SKU',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-bolt"></i></span>
					<?php
                        $option='';
                        foreach($el_variation_sku_data as $sku){
                            $option.="<option value='".$sku->id."' >".$sku->label."</option>";
                        }
                    ?>
                    <select name="el_sku_variations[]" multiple="multiple" size="5"  data-size="5" class="variation_elements chosen-select-search">
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                <?php 
					}
				?>
                
                
                <?php
                	$el_product_sku_data = $this->el_get_woo_var_prod_sku();
					if($el_product_sku_data){
				?>
                
                <div class="col-md-6">
                	<div class="awr-form-title">
						<?php _e('Product SKU',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-cog"></i></span>
					<?php
                        $option='';
                        foreach($el_product_sku_data as $sku){
                            $option.="<option value='".$sku->id."' >".$sku->label."</option>";
                        }
                    ?>
                
                    <select name="el_sku_products[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                <?php 
					}
				?>
                
                
                <div class="col-md-6">
                	<div class="awr-form-title">
						<?php _e('Variations',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-bolt"></i></span>
                    <?php
                        $option='';
                        $el_variations=$this->el_get_woo_pv_atts('yes');

            
                        foreach($el_variations as $key=>$value){
                            $new_key = str_replace("wcv_","",$key);
                            $option.="<option $selected value='".$new_key."' >".$value." </option>";
                        }
                    ?>
                
                    <select name="el_variations[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Basic Column',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					
                    <input type="checkbox" name="el_general_cols" class="el_general_cols" value="yes">
                </div>
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Zero Stock',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
                    <input type="checkbox" name="el_stock_zero" class="el_stock_zero" value="yes" >
                    <label>Include items having 0 stock</label>
                </div>
                
                <?php
            	if(__PW_COG__!=''){
				?>
				
					<div class="col-md-6">
						<div class="awr-form-title">
							<?php _e('Show Cog & Profit',__ELREPORT_TEXTDOMAIN__);?>
						</div>
						
						<input name="el_show_cog" type="checkbox" value="yes"/>
						
					</div>	
				<?php
					}
				?>
                
            </div>
            
            <div class="col-md-12">
                    <?php
                    	$el_hide_os='trash';
						$el_publish_order='no';
						
						$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
					?>
                    <input type="hidden" name="list_parent_category" value="">
                    <input type="hidden" name="group_by_parent_cat" value="0">
                    
                	<input type="hidden" name="el_hide_os" id="el_hide_os" value="<?php echo $el_hide_os;?>" />
                    
                    <input type="hidden" name="date_format" id="date_format" value="<?php echo $data_format;?>" />
                
                	<input type="hidden" name="table_names" value="<?php echo $table_name;?>"/>
                    <div class="fetch_form_loading search-form-loading"></div>	
                    <input type="submit" value="Search" class="button-primary"/>
					<input type="button" value="Reset" class="button-secondary form_reset_btn"/>	
            </div>  
                                
        </form>
    <?php
	}
	
?>