<?php
if($file_used=="sql_table")
{
	
	//show_seleted_order_status
	global $wpdb;
	
	$el_from_date=$this->el_from_date_dashboard;
	$el_to_date=$this->el_to_date_dashboard;
	
	$el_hide_os=$this->otder_status_hide;
	$el_shop_order_status=$this->el_shop_status;
	
	if(isset($_POST['el_from_date']))
	{
		//parse_str($_REQUEST, $my_array_of_vars);
		$this->search_form_fields=$_POST;

		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		$el_hide_os	= $this->el_get_woo_requests('el_hide_os',$el_hide_os,true);
		$el_shop_order_status	= $this->el_get_woo_requests('shop_order_status',$el_shop_order_status,true);

	}
	
	$el_create_date =  date("Y-m-d");
	$el_url_shop_order_status	= "";
	$el_in_shop_os	= "";
	$el_in_post_os	= "";
	
	//$el_hide_os='trash';
	//$el_hide_os	= $this->el_get_woo_requests('el_hide_os',$el_hide_os,true);
	$el_hide_os=explode(',',$el_hide_os);		
	//$el_shop_order_status="wc-completed,wc-on-hold,wc-processing";
	//$el_shop_order_status	= $this->el_get_woo_requests('shop_order_status',$el_shop_order_status,true);
	if(strlen($el_shop_order_status)>0 and $el_shop_order_status != "-1") 
		$el_shop_order_status = explode(",",$el_shop_order_status); 
	else $el_shop_order_status = array();
	
	if(count($el_shop_order_status)>0){
		$el_in_post_os	= implode("', '",$el_shop_order_status);	
	}
	
	$in_el_hide_os = "";
	if(count($el_hide_os)>0){
		$in_el_hide_os		= implode("', '",$el_hide_os);				
	}
	
	$per_page=get_option(__ELREPORT_FIELDS_PERFIX__.'top_country_post_per_page',5);
	
	$el_shop_order_status_condition ='';
	$el_from_date_condition ='';
	$el_hide_os_condition ='';
	
	$sql_columns = " SUM(el_postmeta1.meta_value) AS 'Total' 
	,el_postmeta2.meta_value AS 'BillingCountry'
	,Count(*) AS 'OrderCount'";
	
	$sql_joins= " {$wpdb->prefix}posts as el_posts
	LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta1 ON el_postmeta1.post_id=el_posts.ID
	LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta2 ON el_postmeta2.post_id=el_posts.ID";
	
	
	$sql_condition = " el_posts.post_type			=	'shop_order'  
	AND el_postmeta1.meta_key	=	'_order_total' 
	AND el_postmeta2.meta_key	=	'_billing_country'";
	
	if(count($el_shop_order_status)>0){
		$el_in_shop_os		= implode("', '",$el_shop_order_status);
		$el_shop_order_status_condition = " AND  el_posts.post_status IN ('{$el_in_shop_os}')";
	}
		
	if ($el_from_date != NULL &&  $el_to_date !=NULL){
		$el_from_date_condition = " AND DATE(el_posts.post_date) BETWEEN '{$el_from_date}' AND '{$el_to_date}'";
	}
	
	if(count($el_hide_os)>0){
		$in_el_hide_os		= implode("', '",$el_hide_os);
		$el_hide_os_condition = " AND  el_posts.post_status NOT IN ('{$in_el_hide_os}')";
	}
	
	$sql_group_by = " GROUP BY  el_postmeta2.meta_value ";
	$sql_order_by = " Order By Total DESC";		
	$sql_limit =" LIMIT {$per_page}";

	$sql = "SELECT  $sql_columns FROM $sql_joins WHERE $sql_condition
			$el_shop_order_status_condition $el_from_date_condition $el_hide_os_condition 
			$sql_group_by $sql_order_by $sql_limit";
	
	//echo $sql;
	
}elseif($file_used=="data_table"){
	
	foreach($this->results as $items){
	//for($i=1; $i<=20 ; $i++){
						
		$datatable_value.=("<tr>");
								
			//COUNTRY
			$country      	= $this->el_get_woo_countries();														
			$el_table_value = isset($country->countries[$items->BillingCountry]) ? $country->countries[$items->BillingCountry]: $items->BillingCountry;
			$display_class='';
			if($this->table_cols[0]['status']=='hide') $display_class='display:none';
			$datatable_value.=("<td style='".$display_class."'>");
				$datatable_value.= $el_table_value;
			$datatable_value.=("</td>");
			
			//Target Sales
			$display_class='';
			if($this->table_cols[1]['status']=='hide') $display_class='display:none';
			$datatable_value.=("<td style='".$display_class."'>");
				$datatable_value.= $items->OrderCount;
			$datatable_value.=("</td>");
			
			//Actual Sales
			$display_class='';
			if($this->table_cols[2]['status']=='hide') $display_class='display:none';
			$datatable_value.=("<td style='".$display_class."'>");
				$datatable_value.= $this->price($items->Total);
			$datatable_value.=("</td>");
			
		$datatable_value.=("</tr>");
	}
}elseif($file_used=="search_form"){}

?>