<?php
if($file_used=="sql_table")
{
	
	//show_seleted_order_status
	global $wpdb;
	
	$el_from_date=$this->el_from_date_dashboard;
	$el_to_date=$this->el_to_date_dashboard;
	
	$el_hide_os=$this->otder_status_hide;
	$el_shop_order_status=$this->el_shop_status;
	
	if(isset($_POST['el_from_date']))
	{
		//parse_str($_REQUEST, $my_array_of_vars);
		$this->search_form_fields=$_POST;

		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		$el_hide_os	= $this->el_get_woo_requests('el_hide_os',$el_hide_os,true);
		$el_shop_order_status	= $this->el_get_woo_requests('shop_order_status',$el_shop_order_status,true);

	}
	
	$el_create_date =  date("Y-m-d");
	$el_url_shop_order_status	= "";
	$el_in_shop_os	= "";
	$el_in_post_os	= "";
	
	
//	$el_hide_os='trash';
//	$el_hide_os	= $this->el_get_woo_requests('el_hide_os',$el_hide_os,true);
	$el_hide_os=explode(',',$el_hide_os);		
	//$el_shop_order_status="wc-completed,wc-on-hold,wc-processing";
	//$el_shop_order_status	= $this->el_get_woo_requests('shop_order_status',$el_shop_order_status,true);
	if(strlen($el_shop_order_status)>0 and $el_shop_order_status != "-1") 
		$el_shop_order_status = explode(",",$el_shop_order_status); 
	else $el_shop_order_status = array();
	
	if(count($el_shop_order_status)>0){
		$el_in_post_os	= implode("', '",$el_shop_order_status);	
	}
	
	$in_el_hide_os = "";
	if(count($el_hide_os)>0){
		$in_el_hide_os		= implode("', '",$el_hide_os);				
	}
	
	$per_page=get_option(__ELREPORT_FIELDS_PERFIX__.'top_category_post_per_page',5);
	
	$el_shop_order_status_condition ='';
	$el_from_date_condition ='';
	$el_hide_os_condition ='';
	
	$sql_columns = " SUM(el_woocommerce_order_itemmeta_product_qty.meta_value) AS quantity 
	,SUM(el_woocommerce_order_itemmeta_product_line_total.meta_value) AS total_amount
	,el_terms_product_id.term_id AS category_id 
	,el_terms_product_id.name AS category_name 
	,el_term_taxonomy_product_id.parent AS parent_category_id
	,el_terms_parent_product_id.name AS parent_category_name";
	
	$sql_joins = " {$wpdb->prefix}woocommerce_order_items as el_woocommerce_order_items
	LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta_product_id ON el_woocommerce_order_itemmeta_product_id.order_item_id=el_woocommerce_order_items.order_item_id
	LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta_product_qty ON el_woocommerce_order_itemmeta_product_qty.order_item_id=el_woocommerce_order_items.order_item_id
	LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta_product_line_total ON el_woocommerce_order_itemmeta_product_line_total.order_item_id=el_woocommerce_order_items.order_item_id
	LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships_product_id 	ON el_term_relationships_product_id.object_id		=	el_woocommerce_order_itemmeta_product_id.meta_value 
	LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as el_term_taxonomy_product_id 		ON el_term_taxonomy_product_id.term_taxonomy_id	=	el_term_relationships_product_id.term_taxonomy_id
	LEFT JOIN  {$wpdb->prefix}terms 				as el_terms_product_id 				ON el_terms_product_id.term_id						=	el_term_taxonomy_product_id.term_id
	LEFT JOIN  {$wpdb->prefix}terms 				as el_terms_parent_product_id 				ON el_terms_parent_product_id.term_id						=	el_term_taxonomy_product_id.parent
	LEFT JOIN  {$wpdb->prefix}posts as el_posts ON el_posts.id=el_woocommerce_order_items.order_id";
		
	$sql_condition = " 1*1 
	AND el_woocommerce_order_items.order_item_type = 'line_item'
	AND el_woocommerce_order_itemmeta_product_id.meta_key 	= '_product_id'
	AND el_woocommerce_order_itemmeta_product_qty.meta_key = '_qty'
	AND el_woocommerce_order_itemmeta_product_line_total.meta_key 	= '_line_total'
	AND el_term_taxonomy_product_id.taxonomy = 'product_cat'
	AND el_posts.post_type = 'shop_order'";				
	
	if(count($el_shop_order_status)>0){
		$el_in_shop_os		= implode("', '",$el_shop_order_status);
		$el_shop_order_status_condition = " AND  el_posts.post_status IN ('{$el_in_shop_os}')";
	}
	
	if ($el_from_date != NULL &&  $el_to_date !=NULL){
		$el_from_date_condition = " AND DATE(el_posts.post_date) BETWEEN '{$el_from_date}' AND '{$el_to_date}'";
	}
	
	if(count($el_hide_os)>0){
		$in_el_hide_os		= implode("', '",$el_hide_os);
		$el_hide_os_condition = " AND  el_posts.post_status NOT IN ('{$in_el_hide_os}')";
	}
	
	$sql_group_by = " GROUP BY category_id";
	$sql_order_by = " Order By total_amount DESC";
	$sql_limit = " LIMIT {$per_page}";
	
	$sql = "SELECT  $sql_columns FROM $sql_joins WHERE $sql_condition
			$el_shop_order_status_condition $el_from_date_condition $el_hide_os_condition 
			$sql_group_by $sql_order_by $sql_limit";
	
	//echo $sql;
	
}elseif($file_used=="data_table"){
	
	foreach($this->results as $items){
	//for($i=1; $i<=20 ; $i++){
						
		$datatable_value.=("<tr>");
								
			//Status
			$display_class='';
			if($this->table_cols[0]['status']=='hide') $display_class='display:none';
			$datatable_value.=("<td style='".$display_class."'>");
				$datatable_value.= $items->category_name;
			$datatable_value.=("</td>");
			
			//Target Sales
			$display_class='';
			if($this->table_cols[1]['status']=='hide') $display_class='display:none';
			$datatable_value.=("<td style='".$display_class."'>");
				$datatable_value.= $items->quantity;
			$datatable_value.=("</td>");
			
			//Actual Sales
			$display_class='';
			if($this->table_cols[2]['status']=='hide') $display_class='display:none';
			$datatable_value.=("<td style='".$display_class."'>");
				$datatable_value.= $this->price($items->total_amount);
			$datatable_value.=("</td>");
			
		$datatable_value.=("</tr>");
	}
}elseif($file_used=="search_form"){}

?>