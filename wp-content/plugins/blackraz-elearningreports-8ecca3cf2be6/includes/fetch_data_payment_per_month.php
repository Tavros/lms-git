<?php
	
	if($file_used=="sql_table")
	{
		
		//GET POSTED PARAMETERS
		$start				= 0;
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		
		$el_from_date=substr($el_from_date,0,strlen($el_from_date)-3);
		$el_to_date=substr($el_to_date,0,strlen($el_to_date)-3);

		$el_product_id			= $this->el_get_woo_requests('el_product_id',"-1",true);
		$category_id 		= $this->el_get_woo_requests('el_category_id','-1',true);
		$el_cat_prod_id_string = $this->el_get_woo_pli_category($category_id,$el_product_id);
		$category_id 				= "-1";
		
		$el_sort_by 			= $this->el_get_woo_requests('sort_by','item_name',true);
		$el_order_by 			= $this->el_get_woo_requests('order_by','ASC',true);
		
		$el_id_order_status 	= $this->el_get_woo_requests('el_id_order_status',NULL,true);
		$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
		//$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		$el_payment_gatway		= $this->el_get_woo_requests('el_pay_gatway','-1',true);
		
		if($el_payment_gatway != NULL  && $el_payment_gatway != '-1')
		{
			$el_payment_gatway = str_replace(",", "','",$el_payment_gatway);
		}
		
		///////////HIDDEN FIELDS////////////
		//$el_hide_os	= $this->el_get_woo_sm_requests('el_hide_os',$el_hide_os, "-1");
		$el_hide_os='"trash"';
		$el_publish_order='no';
		$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
		
		/////////////////////////
		//APPLY PERMISSION TERMS
		$key=$this->el_get_woo_requests('table_names','',true);

		$permission_value=$this->get_form_element_value_permission('el_orders_status',$key);
		$permission_enable=$this->get_form_element_permission('el_orders_status',$key);
		
		if($permission_enable && $el_order_status=='-1' && $permission_value!=1){
			$el_order_status=implode(",",$permission_value);
		}
		if($el_order_status != NULL  && $el_order_status != '-1')
			$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		///////////////////////////

		
		//PAYMENT GETWAY
		$el_payment_gatway_condition="";
		
		//ORDER STATUS
		$el_id_order_status_joins ="";
		
		//Order Status id
		$el_id_order_status_join="";
		
		//ORDER Status
		$el_id_order_status_condition ="";
		
		//Start Date
		$el_from_date_condition ="";
		
		//ORDER Status Condition
		$el_order_status_condition ="";
		
		//Hide ORDER Status
		$el_hide_os_condition ="";
		
		
		$sql_columns = " 
		el_postmeta1.meta_value 							as id	
		,el_postmeta3.meta_value						 	as payment_method
		,el_postmeta3.meta_value						 	as item_name
		,SUM(el_postmeta2.meta_value)						as total
		,COUNT(shop_order.ID) 							as quantity
		,MONTH(shop_order.post_date) 					as month_number
		,DATE_FORMAT(shop_order.post_date, '%Y-%m')		as month_key";
		$sql_joins ="{$wpdb->prefix}posts as shop_order 
		LEFT JOIN	{$wpdb->prefix}postmeta as el_postmeta1 on el_postmeta1.post_id = shop_order.ID
		LEFT JOIN	{$wpdb->prefix}postmeta as el_postmeta2 on el_postmeta2.post_id = shop_order.ID
		LEFT JOIN	{$wpdb->prefix}postmeta as el_postmeta3 on el_postmeta3.post_id = shop_order.ID
		";
			
		if($el_id_order_status != NULL  && $el_id_order_status != '-1'){
			$el_id_order_status_joins = " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships2 	ON el_term_relationships2.object_id	=	shop_order.ID
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as el_term_taxonomy2 		ON el_term_taxonomy2.term_taxonomy_id	=	el_term_relationships2.term_taxonomy_id
			LEFT JOIN  {$wpdb->prefix}terms 				as terms2 				ON terms2.term_id					=	el_term_taxonomy2.term_id";
		}
		
		$sql_condition = " 
		shop_order.post_type	= 'shop_order'
		AND el_postmeta1.meta_key 		= '_payment_method'
		AND	el_postmeta2.meta_key 		= '_order_total'
		AND	el_postmeta3.meta_key 		= '_payment_method_title'";
		/*
		if($id != NULL  && $id != '-1'){
			$sql .= " AND el_postmeta1.meta_value IN ('{$id}') ";
			//$sql .= " AND el_postmeta1.meta_value IN ('cheque','paypal') ";
		}
		*/
		if($el_id_order_status != NULL  && $el_id_order_status != '-1'){
			$el_id_order_status_condition = "
			AND el_term_taxonomy2.taxonomy LIKE('shop_order_status')
			AND terms2.term_id IN (".$el_id_order_status .") ";
		}
	
		if ($el_from_date != NULL &&  $el_to_date !=NULL)
			$el_from_date_condition = " AND DATE_FORMAT(shop_order.post_date, '%Y-%m') BETWEEN '".$el_from_date."' AND '". $el_to_date ."'";
		
		
		if(isset($el_payment_gatway) && $el_payment_gatway != NULL  && $el_payment_gatway != '-1')
			$el_payment_gatway_condition = "	
				AND	el_postmeta1.meta_value IN ('{$el_payment_gatway}')";
		
		if($el_order_status  && $el_order_status != '-1' and $el_order_status != "'-1'")
			$el_order_status_condition = " AND shop_order.post_status IN (".$el_order_status.")";
			
		if($el_hide_os  && $el_hide_os != '-1' and $el_hide_os != "'-1'")
			$el_hide_os_condition = " AND shop_order.post_status NOT IN (".$el_hide_os.")";
		

		$sql_group_by = " group by item_name";
		$sql_order_by ="ORDER BY item_name ASC";

		$sql = "SELECT $sql_columns FROM $sql_joins $el_id_order_status_joins WHERE $sql_condition
			$el_id_order_status_condition $el_from_date_condition $el_payment_gatway_condition
			$el_order_status_condition $el_hide_os_condition 
			$sql_group_by $sql_order_by";			
				
		//echo $sql;		
				
		///////////////////
		//MONTHS
		
		$array_index=2;
		$this->table_cols =$this->table_columns($table_name);
		
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);

		$time1  = strtotime($el_from_date); 
	   	$time2  = strtotime($el_to_date); 
	   	$my     = date('mY', $time2); 
		$this->month_start=date('m', $time1);
		$months=array();	
		
		$month_count=0;
		
		$data_month='';
		
		if($my!=date('mY', $time1))
		{	
			$year=date('Y', $time1);
			$months = array(array('lable'=>$this->el_translate_function(__ELREPORT_FIELDS_PERFIX__.date('M', $time1).'_translate',date('M', $time1))."-".$year,'status'=>'currency')); 
			$month_count=1;
			$data_month[]=$year."-".date('m', $time1);
				
			while($time1 < $time2) { 
				
				$time1 = strtotime(date('Y-m-d', $time1).' +1 month'); 
			  
				if(date('mY', $time1) != $my && ($time1 < $time2)) 
				{
					if($year!=date('Y', $time1))
					{
						$year=date('Y', $time1);
						$label = $this->el_translate_function(__ELREPORT_FIELDS_PERFIX__.date('M', $time1).'_translate',date('M', $time1))."-".$year; 
					}else
						$label = $this->el_translate_function(__ELREPORT_FIELDS_PERFIX__.date('M', $time1).'_translate',date('M', $time1)); 
				
					$month_count++;
					$months[] = array('lable'=>$label,'status'=>'currency');
					$data_month[]=$year."-".date('m', $time1);
				}
			} 
		
			if($year!=date('Y', $time2)){
				$year=date('Y', $time2);
				$label = $this->el_translate_function(__ELREPORT_FIELDS_PERFIX__.date('M', $time2).'_translate',date('M', $time2))."-".$year; 
			}else
				$label = $this->el_translate_function(__ELREPORT_FIELDS_PERFIX__.date('M', $time2).'_translate',date('M', $time2)); 
			$months[] = array('lable'=>$label,'status'=>'currency');	
			$data_month[]=$year."-".date('m', $time2);
		}else
		{
			$year=date('Y', $time1);
			$months = array(array('lable'=>$this->el_translate_function(__ELREPORT_FIELDS_PERFIX__.date('M', $time1).'_translate',date('M', $time1))."-".$year,'status'=>'currency')); 
			$data_month[]=$year."-".date('m', $time1);
			$month_count=1;
		}	
	  	//print_r( $months); 
		

		$value=array(array('lable'=>__('Total',__ELREPORT_TEXTDOMAIN__),'status'=>'currency'));
		$value=array_merge($months,$value);			
		
		array_splice($this->table_cols, $array_index, count($this->table_cols), $value);
		
		//print_r($this->table_cols);


		$this->month_count=$month_count;
		$this->data_month=$data_month;
		
		
	}elseif($file_used=="data_table"){
		
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
			$datatable_value.=("<tr>");
				
											
				
				//Payment Gateway
				$display_class='';
				if($this->table_cols[0]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items -> payment_method;
				$datatable_value.=("</td>");
				
				$type = 'limit_row';$items_only = true; $id = $items->id;

				$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
				$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
				$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
				$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
				$el_from_date=substr($el_from_date,0,strlen($el_from_date)-3);
				$el_to_date=substr($el_to_date,0,strlen($el_to_date)-3);
				
				$params=array(
					"el_from_date"=>$el_from_date,
					"el_to_date"=>$el_to_date,
					"order_status"=>$el_order_status,
					"el_hide_os"=>'"trash"'
				);
				$items_product=$this->el_get_woo_pg_items($type , $items_only, $id,$params);
				
				$month_arr='';
				$month_arr='';
				//print_r($items_product);
				foreach($items_product as $item_product){
					$month_arr[$item_product->month_key]['total']=$item_product->total;
					$month_arr[$item_product->month_key]['qty']=$item_product->quantity;
				}
				
				
				$j=1;
				$total=0;
				$qty=0;
				
								
				foreach($this->data_month as $month_name){
					
					$el_table_value=$this->price(0);
					if(isset($month_arr[$month_name]['total'])){
						$el_table_value=$this->price($month_arr[$month_name]['total']);
						$total+=$month_arr[$month_name]['total'];
						$qty+=$month_arr[$month_name]['qty'];
					}
					
					
					$display_class='';
					if($this->table_cols[$j++]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $el_table_value;
					$datatable_value.=("</td>");
				}	
			
				$display_class='';
				if($this->table_cols[$j]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($total) .' #'.$qty;
				$datatable_value.=("</td>");
				
				
			$datatable_value.=("</tr>");
		}
	}elseif($file_used=="search_form"){
			$now_date= date("Y-m-d");
			$cur_year=substr($now_date,0,4);
			$el_from_date= $cur_year."-01-01";
			$el_to_date= $cur_year."-12-31";
		?>
		<form class='alldetails search_form_report' action='' method='post'>
			<input type='hidden' name='action' value='submit-form' />
			<div class="row">
				
				<div class="col-md-6">
					<div>
						<?php _e('From Date',__ELREPORT_TEXTDOMAIN__);?>
					</div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
					<input name="el_from_date" id="pwr_from_date" type="text" readonly='true' class="datepick" value="<?php echo $el_from_date;?>"/>                
				</div>
				<div class="col-md-6">
					<div class="awr-form-title">
						<?php _e('To Date',__ELREPORT_TEXTDOMAIN__);?>
					</div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
					<input name="el_to_date" id="pwr_to_date" type="text" readonly='true' class="datepick"  value="<?php echo $el_to_date;?>"/>
				</div>
                
                <div class="col-md-6">
                	<div class="awr-form-title">
						<?php _e('Payment Gatway',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-credit-card"></i></span>
					<?php

                        $gateway_data = $this->el_get_woo_pay_gateways();
                        //print_r($state_data);
                        $option='';
                        //$current_product=$this->el_get_woo_requests_links('el_product_id','',true);
                        //echo $current_product;
                        
                        foreach($gateway_data as $gateway){
                            $selected='';
                            /*if($current_product==$country->id)
                                $selected="selected";*/
                            $option.="<option $selected value='".$gateway -> id."' >".$gateway -> label." </option>";
                        }
                    ?>
                
                    <select name="el_pay_gatway[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                       <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                
                <?php
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_orders_status');
					if($this->get_form_element_permission('el_orders_status') ||  $permission_value!=''){
						if(!$this->get_form_element_permission('el_orders_status') &&  $permission_value!='')
							$col_style='display:none';
				?>
                
                <div class="col-md-6" style=" <?php echo $col_style;?>">
                    <div class="awr-form-title">
                        <?php _e('Status',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-check"></i></span>
					<?php
                        $el_order_status=$this->el_get_woo_orders_statuses();

                        $option='';
                        foreach($el_order_status as $key => $value){
							
							$selected='';
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($key,$permission_value))
								continue;
							
							/*if(!$this->get_form_element_permission('el_orders_status') &&  $permission_value!='')
								$selected="selected";*/
							
                            $option.="<option value='".$key."' $selected>".$value."</option>";
                        }
                    ?>
                
                    <select name="el_orders_status[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <?php
                        	if($this->get_form_element_permission('el_orders_status') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    <input type="hidden" name="el_id_order_status[]" id="el_id_order_status" value="-1">
                </div>	
              	
                <?php
					}
				?>
                  
            </div>
            
            <div class="col-md-12">
                    <?php
                    	$el_hide_os='trash';
						$el_publish_order='no';
						$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
					?>
                    <input type="hidden" name="list_parent_category" value="">
                    <input type="hidden" name="el_category_id" value="-1">
                    <input type="hidden" name="group_by_parent_cat" value="0">
                    
                	<input type="hidden" name="el_hide_os" id="el_hide_os" value="<?php echo $el_hide_os;?>" />
                   
                    <input type="hidden" name="date_format" id="date_format" value="<?php echo $data_format;?>" />
                
                	<input type="hidden" name="table_names" value="<?php echo $table_name;?>"/>
                    <div class="fetch_form_loading search-form-loading"></div>	
                    <input type="submit" value="Search" class="button-primary"/>
					<input type="button" value="Reset" class="button-secondary form_reset_btn"/>
            </div>  
                                
        </form>
    <?php
	}
	
?>