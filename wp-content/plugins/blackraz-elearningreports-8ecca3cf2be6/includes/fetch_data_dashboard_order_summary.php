<?php
	
	if($file_used=="sql_table")
	{
		$el_create_date =  date("Y-m-d");
		$el_from_date=$this->el_from_date_dashboard;
		$el_to_date=$this->el_to_date_dashboard;
		
		$el_hide_os=$this->otder_status_hide;
		$el_shop_order_status=$this->el_shop_status;
		
		if(isset($_POST['el_from_date']))
		{
			//parse_str($_REQUEST, $my_array_of_vars);
			$this->search_form_fields=$_POST;
	
			$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
			$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
			$el_hide_os	= $this->el_get_woo_requests('el_hide_os',$el_hide_os,true);
			$el_shop_order_status	= $this->el_get_woo_requests('shop_order_status',$el_shop_order_status,true);
			$el_create_date =  $el_to_date;
		}
		
		
		$el_url_shop_order_status	= "";
		$el_in_shop_os	= "";
		$el_in_post_os	= "";
		
		
		//$el_hide_os='trash';
		//$el_hide_os	= $this->el_get_woo_requests('el_hide_os',$el_hide_os,true);
		$el_hide_os=explode(',',$el_hide_os);		
		//$el_shop_order_status="wc-completed,wc-on-hold,wc-processing";
		//$el_shop_order_status	= $this->el_get_woo_requests('shop_order_status',$el_shop_order_status,true);
		if(strlen($el_shop_order_status)>0 and $el_shop_order_status != "-1") 
			$el_shop_order_status = explode(",",$el_shop_order_status); 
		else $el_shop_order_status = array();
		
		if(count($el_shop_order_status)>0){
			$el_in_post_os	= implode("', '",$el_shop_order_status);	
		}
		
		$in_el_hide_os = "";
		if(count($el_hide_os)>0){
			$in_el_hide_os		= implode("', '",$el_hide_os);				
		}	

		/*Today*/
		$sql_columns = " SUM(postmeta.meta_value)AS 'OrderTotal' 
		,COUNT(*) AS 'OrderCount'
		,'Today' AS 'SalesOrder'";
		
		$sql_joins = " {$wpdb->prefix}postmeta as postmeta 
		LEFT JOIN  {$wpdb->prefix}posts as el_posts ON el_posts.ID=postmeta.post_id";
		
		if(strlen($el_in_shop_os)>0){
			$el_in_shop_os_join = " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_posts.ID
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id";
		}
		
		$sql_condition = " meta_key='_order_total' 
		AND DATE(el_posts.post_date) = '".$el_create_date."' AND el_posts.post_type IN ('shop_order')";
		
		if(strlen($el_in_shop_os)>0){
			$sql_condition .= " AND  term_taxonomy.term_id IN ({$el_in_shop_os})";
		}
		
		if(strlen($el_in_post_os)>0){
			$sql_condition .= " AND  el_posts.post_status IN ('{$el_in_post_os}')";
		}
		
		if(strlen($in_el_hide_os)>0){
			$sql_condition .= " AND  el_posts.post_status NOT IN ('{$in_el_hide_os}')";
		}
		
		$el_today_sql = "SELECT  $sql_columns FROM $sql_joins WHERE $sql_condition";
		
			 
		/*Yesterday*/
		$sql_columns = " SUM(postmeta.meta_value)AS 'OrderTotal' 
		,COUNT(*) AS 'OrderCount'
		,'Yesterday' AS 'Sales Order'";
		
		$sql_joins=" {$wpdb->prefix}postmeta as postmeta LEFT JOIN  {$wpdb->prefix}posts as el_posts ON el_posts.ID=postmeta.post_id";
		
		if(strlen($el_in_shop_os)>0){
			$sql_joins .= " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_posts.ID
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id";
		}
		
		$sql_condition = " meta_key='_order_total' AND  DATE(el_posts.post_date)= DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))
		 AND el_posts.post_type IN ('shop_order')";
		
		if(strlen($el_in_shop_os)>0){
			$sql_condition .= " AND  term_taxonomy.term_id IN ({$el_in_shop_os})";
		}
		
		if(strlen($el_in_post_os)>0){
			$sql_condition .= " AND  el_posts.post_status IN ('{$el_in_post_os}')";
		}
		
		if(strlen($in_el_hide_os)>0){
			$sql_condition .= " AND  el_posts.post_status NOT IN ('{$in_el_hide_os}')";
		}
					
		$el_yesterday_sql = "SELECT  $sql_columns FROM $sql_joins WHERE $sql_condition";
			
		/*Week*/	
		$sql_columns = " SUM(postmeta.meta_value)AS 'OrderTotal' 
		,COUNT(*) AS 'OrderCount'
		,'Week' AS 'Sales Order'";
		
		$sql_joins = " {$wpdb->prefix}postmeta as postmeta 
		LEFT JOIN  {$wpdb->prefix}posts as el_posts ON el_posts.ID=postmeta.post_id";
		
		if(strlen($el_in_shop_os)>0){
			$sql_joins .= " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_posts.ID
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id";
		}
		
		$sql_condition = " meta_key='_order_total' AND WEEK(CURDATE()) = WEEK(DATE(el_posts.post_date)) AND YEAR(CURDATE()) = YEAR(el_posts.post_date) AND el_posts.post_type IN ('shop_order')";
		
		if(strlen($el_in_shop_os)>0){
			$sql_condition .= " AND  term_taxonomy.term_id IN ({$el_in_shop_os})";
		}
		
		if(strlen($el_in_post_os)>0){
			$sql_condition .= " AND  el_posts.post_status IN ('{$el_in_post_os}')";
		}
		
		
		if(strlen($in_el_hide_os)>0){
			$sql_condition .= " AND  el_posts.post_status NOT IN ('{$in_el_hide_os}')";
		}
				
		$el_week_sql = "SELECT  $sql_columns FROM $sql_joins WHERE $sql_condition";
		
		/*Month*/
		$sql_columns = " SUM(postmeta.meta_value)AS 'OrderTotal' ,COUNT(*) AS 'OrderCount','Month' AS 'Sales Order'";
		
		$sql_joins = " {$wpdb->prefix}postmeta as postmeta LEFT JOIN  {$wpdb->prefix}posts as el_posts ON el_posts.ID=postmeta.post_id";
		
		if(strlen($el_in_shop_os)>0){
			$sql_joins .= " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_posts.ID
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id";
		}
		
		$sql_condition = " meta_key='_order_total' 
		AND MONTH(DATE(CURDATE())) = MONTH( DATE(el_posts.post_date))					
		AND YEAR(DATE(CURDATE())) = YEAR( DATE(el_posts.post_date))
		AND el_posts.post_type IN ('shop_order')";
		
		if(strlen($el_in_shop_os)>0){
			$sql_condition .= " AND  term_taxonomy.term_id IN ({$el_in_shop_os})";
		}
		
		if(strlen($el_in_post_os)>0){
			$sql_condition .= " AND  el_posts.post_status IN ('{$el_in_post_os}')";
		}
		
		if(strlen($in_el_hide_os)>0){
			$sql_condition .= " AND  el_posts.post_status NOT IN ('{$in_el_hide_os}')";
		}
		
		$el_month_sql = "SELECT  $sql_columns FROM $sql_joins WHERE $sql_condition";
				
		/*Year*/
		$sql_columns = " SUM(postmeta.meta_value)AS 'OrderTotal' ,COUNT(*) AS 'OrderCount','Year' AS 'Sales Order'";
		
		$sql_joins = " {$wpdb->prefix}postmeta as postmeta LEFT JOIN  {$wpdb->prefix}posts as el_posts ON el_posts.ID=postmeta.post_id";
		
		if(strlen($el_in_shop_os)>0){
			$sql_joins .= " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_posts.ID
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id";
		}
		
		$sql_condition = " meta_key='_order_total' AND YEAR(DATE(CURDATE())) = YEAR( DATE(el_posts.post_date)) AND el_posts.post_type IN ('shop_order')";
		
		if(strlen($el_in_shop_os)>0){
			$sql_condition .= " AND  term_taxonomy.term_id IN ({$el_in_shop_os})";
		}
		
		if(strlen($el_in_post_os)>0){
			$sql_condition .= " AND  el_posts.post_status IN ('{$el_in_post_os}')";
		}
			
		if(strlen($in_el_hide_os)>0){
			$sql_condition .= " AND  el_posts.post_status NOT IN ('{$in_el_hide_os}')";
		}
		
		$el_year_sql = "SELECT  $sql_columns FROM $sql_joins WHERE $sql_condition";
		
		$sql = '';				
		$sql .= $el_today_sql;
		$sql .= " UNION ";
		$sql .= $el_yesterday_sql;
		$sql .= " UNION ";
		$sql .= $el_week_sql;
		$sql .= " UNION ";
		$sql .= $el_month_sql;
		$sql .= " UNION ";
		$sql .= $el_year_sql;
		
		//echo $sql;
		
	}elseif($file_used=="data_table"){
		
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
							
			$datatable_value.=("<tr>");
									
				//Month
				$display_class='';
				if($this->table_cols[0]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->SalesOrder;
				$datatable_value.=("</td>");
				
				//Target Sales
				$display_class='';
				if($this->table_cols[1]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->OrderCount;
				$datatable_value.=("</td>");
				
				//Actual Sales
				$display_class='';
				if($this->table_cols[2]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($items->OrderTotal);
				$datatable_value.=("</td>");
				
			$datatable_value.=("</tr>");
		}
	}elseif($file_used=="search_form"){}
	
?>