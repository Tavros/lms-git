<?php
	if($file_used=="sql_table")
	{
		
		//GET POSTED PARAMETERS
		$request 			= array();
		$start				= 0;
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		$el_parent_brand_id		= $this->el_get_woo_requests('el_parent_brand_id','-1',true);
		/*if($el_parent_brand_id!='-1')
			$el_parent_brand_id  		= "'".str_replace(",","','",$el_parent_brand_id)."'";*/
		$el_child_brand_id	= $this->el_get_woo_requests('child_brand_id','-1',true);
		$el_id_order_status 	= $this->el_get_woo_requests('el_id_order_status',NULL,true);
		$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
		$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		$el_list_parent_brand			= $this->el_get_woo_requests('list_parent_brand',NULL,false);
		$brand_id			= $this->el_get_woo_requests('el_brand_id','-1',true);
		$el_group_by_parent_brand			= $this->el_get_woo_requests('group_by_parent_brand','-1',true);
		///////////HIDDEN FIELDS////////////
		//$el_hide_os	= $this->el_get_woo_sm_requests('el_hide_os',$el_hide_os, "-1");
		$el_hide_os='"trash"';
		$el_publish_order='no';
		$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
		
		/////////////////////////
		//APPLY PERMISSION TERMS
		$key='brand_tax_field';
		
		$permission_value=$this->get_form_element_value_permission('el_parent_brand_id',$key);
		$permission_enable=$this->get_form_element_permission('el_parent_brand_id',$key);
				
		if($permission_enable && $el_parent_brand_id=='-1' && $permission_value!=1){
			$el_parent_brand_id=implode(",",$permission_value);
		}
		///////////////////////////
		
		//////////////////////
		 
				 
		//DATE
		$el_from_date_condition='';
		
		//ORDER STATUS
		$el_order_status_condition='';
		$el_id_order_status_join='';
		$el_id_order_status_condition='';
		
		//CATEGORY
		$brand_id_condition='';
		
		//ORDER STATUS
		$el_order_status_condition='';
		
		//PARENT CATEGORY
		$el_parent_brand_id_condition='';
		
		//CHILD CATEGORY
		$el_child_brand_id_condition='';
		
		//LIST PARENT CATEGORY
		$el_list_parent_brand_condition='';
		
		//PUBLISH STATUS
		$el_publish_order_condition='';
		
		//HIDE ORDER STATUS
		$el_hide_os_condition='';
		
		$sql_columns = " 
		SUM(el_woocommerce_order_itemmeta_product_qty.meta_value) AS quantity
		,SUM(el_woocommerce_order_itemmeta_product_line_total.meta_value) AS total_amount
		,el_terms_product_id.term_id AS brand_id
		,el_terms_product_id.name AS brand_name
		,el_term_taxonomy_product_id.parent AS parent_brand_id
		,el_terms_parent_product_id.name AS parent_brand_name";
		
		$sql_joins= "{$wpdb->prefix}woocommerce_order_items as el_woocommerce_order_items
		
		 LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta_product_id ON el_woocommerce_order_itemmeta_product_id.order_item_id=el_woocommerce_order_items.order_item_id
		 LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta_product_qty ON el_woocommerce_order_itemmeta_product_qty.order_item_id=el_woocommerce_order_items.order_item_id
		 LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta_product_line_total ON el_woocommerce_order_itemmeta_product_line_total.order_item_id=el_woocommerce_order_items.order_item_id";
		
		
		$sql_joins .= " 	LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships_product_id 	ON el_term_relationships_product_id.object_id		=	el_woocommerce_order_itemmeta_product_id.meta_value 
					LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as el_term_taxonomy_product_id 		ON el_term_taxonomy_product_id.term_taxonomy_id	=	el_term_relationships_product_id.term_taxonomy_id
					LEFT JOIN  {$wpdb->prefix}terms 				as el_terms_product_id 				ON el_terms_product_id.term_id						=	el_term_taxonomy_product_id.term_id
		
		 LEFT JOIN  {$wpdb->prefix}terms 				as el_terms_parent_product_id 				ON el_terms_parent_product_id.term_id						=	el_term_taxonomy_product_id.parent
		
		 LEFT JOIN  {$wpdb->prefix}posts as el_posts ON el_posts.id=el_woocommerce_order_items.order_id";
		
		if(strlen($el_id_order_status)>0 && $el_id_order_status != "-1" && $el_id_order_status != "no" && $el_id_order_status != "all"){
				$el_id_order_status_join= " 
				LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_posts.ID
				LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id";
		}
		
		$sql_condition = " 1*1 
		AND el_woocommerce_order_items.order_item_type 					= 'line_item'
		AND el_woocommerce_order_itemmeta_product_id.meta_key 			= '_product_id'
		AND el_woocommerce_order_itemmeta_product_qty.meta_key 			= '_qty'
		AND el_woocommerce_order_itemmeta_product_line_total.meta_key 	= '_line_total'
		AND el_term_taxonomy_product_id.taxonomy 						= 'product_brand'
		AND el_posts.post_type 											= 'shop_order'";				
		
		if(strlen($el_id_order_status)>0 && $el_id_order_status != "-1" && $el_id_order_status != "no" && $el_id_order_status != "all"){
			$el_id_order_status_condition= " AND  term_taxonomy.term_id IN ({$el_id_order_status})";
		}
		
		if($el_parent_brand_id != NULL and $el_parent_brand_id != "-1"){
			$el_parent_brand_id_condition= " AND el_term_taxonomy_product_id.parent IN ($el_parent_brand_id)";
		}
		
		if($el_child_brand_id != NULL and $el_child_brand_id != "-1"){
			$el_child_brand_id_condition= " AND el_terms_product_id.term_id IN ($el_child_brand_id)";
		}
		
		if($el_list_parent_brand != NULL and $el_list_parent_brand > 0){
			$el_list_parent_brand_condition= " AND el_term_taxonomy_product_id.parent > 0";
		}
		if ($el_from_date != NULL &&  $el_to_date !=NULL){
			$el_from_date_condition= " AND DATE(el_posts.post_date) BETWEEN '".$el_from_date."' AND '". $el_to_date ."'";
		}
		
		if(strlen($el_publish_order)>0 && $el_publish_order != "-1" && $el_publish_order != "no" && $el_publish_order != "all"){
			$in_post_status		= str_replace(",","','",$el_publish_order);
			$el_publish_order_condition= " AND  el_posts.post_status IN ('{$in_post_status}')";
		}
		
		if($el_order_status  && $el_order_status != '-1' and $el_order_status != "'-1'")
			$el_order_status_condition= " AND el_posts.post_status IN (".$el_order_status.")";
		
		if($el_hide_os  && $el_hide_os != '-1' and $el_hide_os != "'-1'")
			$el_hide_os_condition= " AND el_posts.post_status NOT IN (".$el_hide_os.")";
		
		
		if($brand_id  && $brand_id != "-1") {
			$brand_id_condition= " AND el_terms_product_id.term_id IN ($brand_id)";
		}
		
		
		$sql_group_by='';
		
		if($el_group_by_parent_brand == 1){
			$sql_group_by= " GROUP BY parent_brand_id";
		}else{
			$sql_group_by= " GROUP BY brand_id";
		};
		
		$sql_order_by= "  Order By total_amount DESC";
		
		$sql = "SELECT $sql_columns FROM $sql_joins $el_id_order_status_join WHERE $sql_condition
				$el_id_order_status_condition $el_parent_brand_id_condition $el_child_brand_id_condition
				$el_list_parent_brand_condition $el_from_date_condition $el_publish_order_condition
				$el_order_status_condition $el_hide_os_condition $brand_id_condition
				$sql_group_by $sql_order_by
				";
		
		//echo $sql;
		
	}elseif($file_used=="data_table"){
		
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
			$datatable_value.=("<tr>");
				
									
						
				//Category Name
				$display_class='';
				if($this->table_cols[0]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->brand_name;
				$datatable_value.=("</td>");
				
				//Quantity
				$display_class='';
				if($this->table_cols[1]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->quantity;
				$datatable_value.=("</td>");
				
				//Amount
				$display_class='';
				if($this->table_cols[2]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->total_amount == 0 ? 0 : $this->price($items->total_amount);
				$datatable_value.=("</td>");
				
			$datatable_value.=("</tr>");
		}
	}elseif($file_used=="search_form"){
		
	?>
		<form class='alldetails search_form_report' action='' method='post'>
            <input type='hidden' name='action' value='submit-form' />
            <div class="row">
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('From Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_from_date" id="pwr_from_date" type="text" readonly='true' class="datepick"/>

                </div>
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('To Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_to_date" id="pwr_to_date" type="text" readonly='true' class="datepick"/>
                    
                    <input type="hidden" name="el_id_order_status[]" id="el_id_order_status" value="-1">
                    <input type="hidden" name="el_orders_status[]" id="order_status" value="<?php echo $this->el_shop_status; ?>">

                </div>
                
                <?php
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_parent_brand_id');
                	if($this->get_form_element_permission('el_parent_brand_id') ||  $permission_value!=''){
						
						if(!$this->get_form_element_permission('el_parent_brand_id') &&  $permission_value!='')
							$col_style='display:none';
				?> 
                
                <div class="col-md-6"  style=" <?php echo $col_style;?>">
                    <div class="awr-form-title">
                        <?php _e('Parent Brand',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-tags"></i></span>
					<?php
                        $p_brands = $this->el_get_woo_sppc_data("product_brand");
                        $option='';
                        //echo $current_product;
                        
                        foreach($p_brands as $brand){
							
							$selected='';
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($brand->id,$permission_value))
								continue;
								
							/*if(!$this->get_form_element_permission('el_parent_brand_id') &&  $permission_value!='')
								$selected="selected";	*/
							
                            $option.="<option $selected value='".$brand -> id."' >".$brand -> label." </option>";
                        }
                        
                    ?>
                    <select name="el_parent_brand_id[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                    	
                        <?php
                        	if($this->get_form_element_permission('el_parent_brand_id') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                        
                        <?php
                            echo $option;
                        ?>
                    </select>  
             </div>
                
                <?php
					}
				?>
                
            </div>
            
            <div class="col-md-12">
                
                    <?php
                    	$el_hide_os='trash';
						$el_publish_order='no';
						$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
					?>
                    <input type="hidden" name="list_parent_brand" value="">
                    <input type="hidden" name="el_brand_id" value="-1">
                    <input type="hidden" name="group_by_parent_brand" value="0">
                    
                	<input type="hidden" name="el_hide_os" id="el_hide_os" value="<?php echo $el_hide_os;?>" />
                    
                    <input type="hidden" name="date_format" id="date_format" value="<?php echo $data_format;?>" />
                
                	<input type="hidden" name="table_names" value="<?php echo $table_name;?>"/>
                    <div class="fetch_form_loading search-form-loading"></div>	
                    <input type="submit" value="Search" class="button-primary"/>
					<input type="button" value="Reset" class="button-secondary form_reset_btn"/>							
                
                
                
            </div>  
                                
        </form>
    <?php
	}
	
?>