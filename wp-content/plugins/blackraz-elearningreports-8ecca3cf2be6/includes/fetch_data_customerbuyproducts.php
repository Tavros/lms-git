<?php
	
	if($file_used=="sql_table")
	{
		$limit 				= $this->el_get_woo_requests('limit',3,true);
		$p 					= $this->el_get_woo_requests('p',1,true);				
		$page				= $this->el_get_woo_requests('page',NULL);				
		$el_order_status		= $this->el_get_woo_requests('el_orders_status',"-1",true);
		$category_id		= $this->el_get_woo_requests('el_category_id','-1',true);
		$el_product_id			= $this->el_get_woo_requests('el_product_id','-1',true);
		$el_id_order_status	= $this->el_get_woo_requests('el_id_order_status','-1',true);	

		$el_paid_customer		= $this->el_get_woo_requests('el_customers_paid','-1',true);

		$el_sort_by 			= $this->el_get_woo_requests('sort_by','-1',true);
		$el_order_by 			= $this->el_get_woo_requests('order_by','DESC',true);
		
		$el_paid_customer		= $this->el_get_woo_sm_requests('el_customers_paid',$el_paid_customer, "-1");
		$el_order_status		= $this->el_get_woo_sm_requests('el_orders_status',$el_order_status, "-1");
		$el_hide_os='"trash"';
		$el_hide_os	= $this->el_get_woo_sm_requests('el_hide_os',$el_hide_os, "-1");				
		$el_cat_prod_id_string = $this->el_get_woo_pli_category($category_id,$el_product_id);
		$category_id 				= "-1";
		
		//GET POSTED PARAMETERS
		$request 			= array();
		$start				= 0;
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		$el_id_order_status 	= $this->el_get_woo_requests('el_id_order_status',NULL,true);
		$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
		$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		///////////HIDDEN FIELDS////////////
		//$el_hide_os	= $this->el_get_woo_sm_requests('el_hide_os',$el_hide_os, "-1");
		$el_hide_os='"trash"';
		$el_publish_order='no';
		$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
		//////////////////////
		
		//PAID CUSTOMERS 
		$el_paid_customer_condition='';	
		
		//PRODUCT ID
		$el_product_id_condition='';
		$el_cat_prod_id_string_condition='';
		
		//ORDER SATTUS
		$el_id_order_status_join='';
		$el_order_status_condition='';
		

		//CATEGORY ID
		$category_id_condition='';
		$category_id_join='';
		
		//ORDER STATUS
		$el_id_order_status_condition='';
		
		//DATE
		$el_from_date_condition='';
		
		//PUBLISH ORDER
		$el_publish_order_condition='';
		
		//HIDE ORDER STATUS
		$el_hide_os_condition ='';
		
		
		$sql_columns = "el_woocommerce_order_items.order_item_name				AS 'product_name'
					,el_woocommerce_order_items.order_item_id				AS order_item_id
					,SUM(woocommerce_order_itemmeta.meta_value)			AS 'quantity'
					,SUM(el_woocommerce_order_itemmeta6.meta_value)		AS 'total_amount'
					,el_woocommerce_order_itemmeta7.meta_value				AS product_id
					,el_postmeta_customer_user.meta_value					AS customer_id
					,DATE(shop_order.post_date) 						AS post_date 
					,el_postmeta_billing_billing_email.meta_value			AS billing_email
					,CONCAT(el_postmeta_billing_billing_email.meta_value,' ',el_woocommerce_order_itemmeta7.meta_value,' ',el_postmeta_customer_user.meta_value)			AS group_column
					,CONCAT(el_postmeta_billing_first_name.meta_value,' ',postmeta_billing_last_name.meta_value)		AS billing_name							
					";
					
		$sql_joins = "{$wpdb->prefix}woocommerce_order_items as el_woocommerce_order_items						
					LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as woocommerce_order_itemmeta ON woocommerce_order_itemmeta.order_item_id=el_woocommerce_order_items.order_item_id
					LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta6 ON el_woocommerce_order_itemmeta6.order_item_id=el_woocommerce_order_items.order_item_id
					LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as el_woocommerce_order_itemmeta7 ON el_woocommerce_order_itemmeta7.order_item_id=el_woocommerce_order_items.order_item_id						
					";
		
		if($category_id  && $category_id != "-1") {
				$category_id_join = " 	
					LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_woocommerce_order_itemmeta7.meta_value 
					LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id
					LEFT JOIN  {$wpdb->prefix}terms 				as el_terms 				ON el_terms.term_id					=	term_taxonomy.term_id";
		}
		
		if($el_id_order_status  && $el_id_order_status != "-1") {
				$el_id_order_status_join = " 	
					LEFT JOIN  {$wpdb->prefix}term_relationships	as el_term_relationships2 	ON el_term_relationships2.object_id	=	el_woocommerce_order_items.order_id
					LEFT JOIN  {$wpdb->prefix}term_taxonomy			as el_term_taxonomy2 		ON el_term_taxonomy2.term_taxonomy_id	=	el_term_relationships2.term_taxonomy_id
					LEFT JOIN  {$wpdb->prefix}terms					as terms2 				ON terms2.term_id					=	el_term_taxonomy2.term_id";
		}
		
		
		$sql_joins.="$category_id_join $el_id_order_status_join ";
		
		$sql_joins .= "
		LEFT JOIN  {$wpdb->prefix}posts as shop_order ON shop_order.id=el_woocommerce_order_items.order_id
		LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta_billing_first_name ON el_postmeta_billing_first_name.post_id		=	el_woocommerce_order_items.order_id
		LEFT JOIN  {$wpdb->prefix}postmeta as postmeta_billing_last_name ON postmeta_billing_last_name.post_id			=	el_woocommerce_order_items.order_id
		LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta_billing_billing_email ON el_postmeta_billing_billing_email.post_id	=	el_woocommerce_order_items.order_id
		LEFT JOIN  {$wpdb->prefix}postmeta as el_postmeta_customer_user ON el_postmeta_customer_user.post_id	=	el_woocommerce_order_items.order_id";
					
		$sql_condition = "
					woocommerce_order_itemmeta.meta_key	= '_qty'
					AND el_woocommerce_order_itemmeta6.meta_key	= '_line_total' 
					AND el_woocommerce_order_itemmeta7.meta_key 	= '_product_id'
					AND el_woocommerce_order_itemmeta7.meta_key 	= '_product_id'
					AND el_postmeta_billing_first_name.meta_key	= '_billing_first_name'
					AND postmeta_billing_last_name.meta_key		= '_billing_last_name'
					AND el_postmeta_billing_billing_email.meta_key	= '_billing_email'
					AND el_postmeta_customer_user.meta_key			= '_customer_user'
					";
					
		
		
		if ($el_from_date != NULL &&  $el_to_date !=NULL){
			$el_from_date_condition = " 
					AND (DATE(shop_order.post_date) BETWEEN '".$el_from_date."' AND '". $el_to_date ."')";
		}
		
		if($el_product_id  && $el_product_id != "-1") 
			$el_product_id_condition = "
					AND el_woocommerce_order_itemmeta7.meta_value IN (".$el_product_id .")";	
		
		if($category_id  && $category_id != "-1") 
			$category_id_condition = "
					AND el_terms.term_id IN (".$category_id .")";	
		
		
		if($el_cat_prod_id_string  && $el_cat_prod_id_string != "-1") 
			$el_cat_prod_id_string_condition = " AND el_woocommerce_order_itemmeta7.meta_value IN (".$el_cat_prod_id_string .")";
		
		if($el_id_order_status  && $el_id_order_status != "-1") 
			$el_id_order_status_condition = " 
					AND terms2.term_id IN (".$el_id_order_status .")";
					
		
		if(strlen($el_publish_order)>0 && $el_publish_order != "-1" && $el_publish_order != "no" && $el_publish_order != "all"){
			$in_post_status		= str_replace(",","','",$el_publish_order);
			$el_publish_order_condition = " AND  shop_order.post_status IN ('{$in_post_status}')";
		}
		
		//echo $el_order_status;
		if($el_order_status  && $el_order_status != '-1' and $el_order_status != "'-1'")
			$el_order_status_condition = " AND shop_order.post_status IN (".$el_order_status.")";
			
		if($el_hide_os  && $el_hide_os != '-1' and $el_hide_os != "'-1'")
			$el_hide_os_condition = " AND shop_order.post_status NOT IN (".$el_hide_os.")";
				
		if($el_paid_customer  && $el_paid_customer != '-1' and $el_paid_customer != "'-1'")
			$el_paid_customer_condition = " AND el_postmeta_billing_billing_email.meta_value IN (".$el_paid_customer.")";
		
		$sql_group_by = " GROUP BY  group_column";
		
		$sql_order_by = " ORDER BY billing_name ASC, product_name ASC, total_amount DESC";
		
		$sql = "SELECT $sql_columns
				FROM $sql_joins
				WHERE $sql_condition $el_from_date_condition $el_product_id_condition $category_id_condition
				$el_cat_prod_id_string_condition $el_id_order_status_condition 		
				$el_publish_order_condition $el_order_status_condition $el_hide_os_condition 
				$el_paid_customer_condition $sql_group_by $sql_order_by";
		
		//echo $sql;
		
		/*
			SELECT el_woocommerce_order_items.order_item_name	AS 'product_name' ,el_woocommerce_order_items.order_item_id	AS order_item_id ,SUM(woocommerce_order_itemmeta.meta_value)	AS 'quantity' ,SUM(el_woocommerce_order_itemmeta6.meta_value)	AS 'total_amount' ,el_woocommerce_order_itemmeta7.meta_value	AS product_id ,el_postmeta_customer_user.meta_value	AS customer_id ,DATE(shop_order.post_date) AS post_date ,el_postmeta_billing_billing_email.meta_value	AS billing_email ,CONCAT(el_postmeta_billing_billing_email.meta_value,' ',el_woocommerce_order_itemmeta7.meta_value,' ',el_postmeta_customer_user.meta_value)	AS group_column ,CONCAT(el_postmeta_billing_first_name.meta_value,' ',postmeta_billing_last_name.meta_value)	AS billing_name,CONCAT(el_postmeta_shipping_first_name.meta_value,' ',postmeta_shipping_last_name.meta_value)	AS shipping_name	FROM wp_woocommerce_order_items as el_woocommerce_order_items	LEFT JOIN wp_woocommerce_order_itemmeta as woocommerce_order_itemmeta ON woocommerce_order_itemmeta.order_item_id=el_woocommerce_order_items.order_item_id LEFT JOIN wp_woocommerce_order_itemmeta as el_woocommerce_order_itemmeta6 ON el_woocommerce_order_itemmeta6.order_item_id=el_woocommerce_order_items.order_item_id LEFT JOIN wp_woocommerce_order_itemmeta as el_woocommerce_order_itemmeta7 ON el_woocommerce_order_itemmeta7.order_item_id=el_woocommerce_order_items.order_item_id	LEFT JOIN wp_posts as shop_order ON shop_order.id=el_woocommerce_order_items.order_id LEFT JOIN wp_postmeta as el_postmeta_billing_first_name ON el_postmeta_billing_first_name.post_id	=	el_woocommerce_order_items.order_id LEFT JOIN wp_postmeta as postmeta_billing_last_name ON postmeta_billing_last_name.post_id	=	el_woocommerce_order_items.order_id LEFT JOIN wp_postmeta as el_postmeta_shipping_first_name ON el_postmeta_shipping_first_name.post_id	=	el_woocommerce_order_items.order_id LEFT JOIN wp_postmeta as postmeta_shipping_last_name ON postmeta_shipping_last_name.post_id	=	el_woocommerce_order_items.order_id LEFT JOIN wp_postmeta as el_postmeta_billing_billing_email ON el_postmeta_billing_billing_email.post_id	=	el_woocommerce_order_items.order_id LEFT JOIN wp_postmeta as el_postmeta_customer_user ON el_postmeta_customer_user.post_id	=	el_woocommerce_order_items.order_id WHERE woocommerce_order_itemmeta.meta_key	= '_qty' AND el_woocommerce_order_itemmeta6.meta_key	= '_line_total' AND el_woocommerce_order_itemmeta7.meta_key = '_product_id' AND el_woocommerce_order_itemmeta7.meta_key = '_product_id' AND el_postmeta_billing_first_name.meta_key	= '_billing_first_name' AND postmeta_billing_last_name.meta_key	= '_billing_last_name' AND el_postmeta_shipping_first_name.meta_key	= '_shipping_first_name' AND postmeta_shipping_last_name.meta_key	= '_shipping_last_name' AND el_postmeta_billing_billing_email.meta_key	= '_billing_email' AND el_postmeta_customer_user.meta_key	= '_customer_user' AND (DATE(shop_order.post_date) BETWEEN '2015-01-01' AND '2016-01-19') AND shop_order.post_status IN ('wc-completed','wc-on-hold','wc-processing') AND shop_order.post_status NOT IN ("trash") GROUP BY group_column ORDER BY billing_name ASC, product_name ASC, total_amount DESC

		*/
		
	}
	elseif($file_used=="data_table"){
							
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
			$datatable_value.=("<tr>");
				
				//Product SKU
				$display_class='';
				if($this->table_cols[0]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->el_get_prod_sku($items->order_item_id, $items->product_id);
				$datatable_value.=("</td>");
	
				//Customer Name
				$display_class='';
				if($this->table_cols[1]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->billing_name;
				$datatable_value.=("</td>");
				
				//Customer Email
				$display_class='';
				if($this->table_cols[2]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->billing_email;
				$datatable_value.=("</td>");
				
				//Product Name
				$display_class='';
				if($this->table_cols[3]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->product_name;
				$datatable_value.=("</td>");
				
				//Sales Qty.
				$display_class='';
				if($this->table_cols[4]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->quantity;
				$datatable_value.=("</td>");
				
				//Current Stock
				$el_table_value = $this->el_get_prod_stock_($items->order_item_id, $items->product_id);
				$display_class='';
				if($this->table_cols[5]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $el_table_value;
				$datatable_value.=("</td>");
								
				//Amount
				$display_class='';
				if($this->table_cols[6]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->total_amount == 0 ? 0 : $this->price($items->total_amount);
				$datatable_value.=("</td>");
				
			$datatable_value.=("</tr>");
		}
	}elseif($file_used=="search_form"){
	?>
		<form class='alldetails search_form_report' action='' method='post'>
            <input type='hidden' name='action' value='submit-form' />
            <div class="row">
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('From Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_from_date" id="pwr_from_date" type="text" readonly='true' class="datepick"/>
                </div>

                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('To Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_to_date" id="pwr_to_date" type="text" readonly='true' class="datepick"/>
                    
                    <input type="hidden" name="el_id_order_status[]" id="el_id_order_status" value="-1">
                    <input type="hidden" name="el_orders_status[]" id="order_status" value="<?php echo $this->el_shop_status; ?>">
                </div>
                
            </div>
            
            <div class="col-md-12">
                
                	
                    <?php
                    	$el_hide_os='trash';
						$el_publish_order='no';
						$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
					?>
                    <input type="hidden" name="list_parent_category" value="">
                    <input type="hidden" name="el_category_id" value="-1">
                    
                	<input type="hidden" name="el_hide_os" id="el_hide_os" value="<?php echo $el_hide_os;?>" />
                    
                    <input type="hidden" name="date_format" id="date_format" value="<?php echo $data_format;?>" />
                
                	<input type="hidden" name="table_names" value="<?php echo $table_name;?>"/>
					<div class="fetch_form_loading search-form-loading"></div>	
                    <input type="submit" value="Search" class="button-primary"/>
					<input type="button" value="Reset" class="button-secondary form_reset_btn"/>
                
              
            </div>  
                                
        </form>
    <?php
	}
	
?>