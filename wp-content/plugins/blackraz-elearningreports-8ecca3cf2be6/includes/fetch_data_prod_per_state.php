<?php
	
	if($file_used=="sql_table")
	{
		//GET POSTED PARAMETERS
		$start				= 0;
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		
		$el_from_date=substr($el_from_date,0,strlen($el_from_date)-3);
		$el_to_date=substr($el_to_date,0,strlen($el_to_date)-3);

		$el_product_id			= $this->el_get_woo_requests('el_product_id',"-1",true);
		$category_id 		= $this->el_get_woo_requests('el_category_id','-1',true);
		$el_cat_prod_id_string = $this->el_get_woo_pli_category($category_id,$el_product_id);
		
		$el_sort_by 			= $this->el_get_woo_requests('sort_by','item_name',true);
		$el_order_by 			= $this->el_get_woo_requests('order_by','ASC',true);
		
		$el_id_order_status 	= $this->el_get_woo_requests('el_id_order_status',NULL,true);
		$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
		//$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		$el_country_code		= $this->el_get_woo_requests('el_countries_code','-1',true);
		
		/*if($el_country_code != NULL  && $el_country_code != '-1')
		{
			$el_country_code = str_replace(",", "','",$el_country_code);
		}*/
		
		$state_code		= $this->el_get_woo_requests('el_states_code','-1',true);
		
		/*if($state_code != NULL  && $state_code != '-1')
		{
			$state_code = str_replace(",", "','",$state_code);
		}*/
		
		$el_region_code="-1";

		///////////HIDDEN FIELDS////////////
		//$el_hide_os	= $this->el_get_woo_sm_requests('el_hide_os',$el_hide_os, "-1");
		$el_hide_os='"trash"';
		$el_publish_order='no';
		
		/////////////////////////
		//APPLY PERMISSION TERMS
		$key=$this->el_get_woo_requests('table_names','',true);
		
		$permission_value=$this->get_form_element_value_permission('el_category_id',$key);
		$permission_enable=$this->get_form_element_permission('el_category_id',$key);
		
		if($permission_enable && $category_id=='-1' && $permission_value!=1){
			$category_id=implode(",",$permission_value);
		}
		
		$permission_value=$this->get_form_element_value_permission('el_product_id',$key);
		$permission_enable=$this->get_form_element_permission('el_product_id',$key);
		
		if($permission_enable && $el_product_id=='-1' && $permission_value!=1){
			$el_product_id=implode(",",$permission_value);
		}
		
		$permission_value=$this->get_form_element_value_permission('el_countries_code',$key);
		$permission_enable=$this->get_form_element_permission('el_countries_code',$key);
		
		if($permission_enable && $el_country_code=='-1' && $permission_value!=1){
			$el_country_code=implode(",",$permission_value);
		}
		if($el_country_code != NULL  && $el_country_code != '-1')
			$el_country_code  		= "'".str_replace(",","','",$el_country_code)."'";
		
		$permission_value=$this->get_form_element_value_permission('el_states_code',$key);
		$permission_enable=$this->get_form_element_permission('el_states_code',$key);
		
		if($permission_enable && $state_code=='-1' && $permission_value!=1){
			$state_code=implode(",",$permission_value);
		}
		if($state_code != NULL  && $state_code != '-1')
			$state_code  		= "'".str_replace(",","','",$state_code)."'";
		
		$permission_value=$this->get_form_element_value_permission('el_orders_status',$key);
		$permission_enable=$this->get_form_element_permission('el_orders_status',$key);
		
		if($permission_enable && $el_order_status=='-1' && $permission_value!=1){
			$el_order_status=implode(",",$permission_value);
		}
		if($el_order_status != NULL  && $el_order_status != '-1')
			$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		///////////////////////////
		
		$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
		//////////////////////
				
		/////////CUSTOM TAXONOMY//////////
		$key=$this->el_get_woo_requests('table_names','',true);
		$visible_custom_taxonomy='';
		$post_name='product';
		
		$all_tax_cols=$all_tax_joins=$all_tax_conditions='';
		$custom_tax_cols='';
		$all_tax=$this->fetch_product_taxonomies( $post_name );
		$current_value=array();
		if(defined("__PW_TAX_FIELD_ADD_ON__") && is_array($all_tax) && count($all_tax)>0){
			//FETCH TAXONOMY
			$i=10;
			foreach ( $all_tax as $tax ) {
				$tax_status=get_option(__ELREPORT_FIELDS_PERFIX__.'set_default_search_'.$key.'_'.$tax);
				if($tax_status=='on'){
					
					$taxonomy=get_taxonomy($tax);	
					$values=$tax;
					$label=$taxonomy->label;
					
					$translate=get_option($key.'_'.$tax."_translate");
					$show_column=get_option($key.'_'.$tax."_column");
					if($translate!='')
					{
						$label=$translate;
					}
		
					if($show_column=="on")
						$custom_tax_cols[]=array('lable'=>__($label,__ELREPORT_TEXTDOMAIN__),'status'=>'show');	
					
					
					$visible_custom_taxonomy[]=$tax;
					
					${$tax} 		= $this->el_get_woo_requests('el_custom_taxonomy_in_'.$tax,'-1',true);
					
					/////////////////////////
					//APPLY PERMISSION TERMS
					$permission_value=$this->get_form_element_value_permission($tax,$key);
					$permission_enable=$this->get_form_element_permission($tax,$key);
					
					if($permission_enable && ${$tax}=='-1' && $permission_value!=1){
						${$tax}=implode(",",$permission_value);
					}
					/////////////////////////
					
					//echo(${$tax});
					
					if(is_array(${$tax})){ 		${$tax}		= implode(",", ${$tax});}
					
					$lbl_join=$tax."_join";
					$lbl_con=$tax."_condition";
					
					${$lbl_join} ='';
					${$lbl_con} = '';
					
					if(${$tax}  && ${$tax} != "-1") {
						${$lbl_join} = "
							LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships$i 	ON el_term_relationships$i.object_id		=	el_woocommerce_order_itemmeta_product.meta_value 
					LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy$i 		ON term_taxonomy$i.term_taxonomy_id	=	el_term_relationships$i.term_taxonomy_id
					";
					}
													
					$all_tax_joins.=" ".${$lbl_join}." ";
					
					if(${$tax}  && ${$tax} != "-1")
						${$lbl_con} = "AND term_taxonomy$i.taxonomy LIKE('$tax') AND term_taxonomy$i.term_id IN (".${$tax} .")";
					
					$all_tax_conditions.=" ".${$lbl_con}." ";	
					
					$i++;
				}
			}
		}
		//////////////////		
		
		//Start Date
		$el_from_date_condition ="";
		$category_id_condition="";
		$category_id_join=""; 
		
		//CATEGORY PRODUCT ID STRING
		$el_cat_prod_id_string_condition ="";
		
		//ORDER Status
		$el_id_order_status_condition ="";
		$el_id_order_status_join="";
		
		//PRODUCT ID
		$el_product_id_condition ="";
		
		//COUNTRY CODE
		$el_country_code_condition ="";
		
		//State CODE
		$state_code_condition ="";
		$el_order_status_condition ="";
		$el_hide_os_condition="";
			
		$sql_columns = "
			el_woocommerce_order_itemmeta_product.meta_value 			as id
			,el_woocommerce_order_items.order_item_name 				as product_name
			,el_woocommerce_order_itemmeta_product.meta_value 			as product_id
			,el_woocommerce_order_items.order_item_id 					as order_item_id
			,el_woocommerce_order_items.order_item_name 				as item_name
			
			
			,SUM(el_woocommerce_order_itemmeta_product_total.meta_value) 	as total
			,SUM(el_woocommerce_order_itemmeta_product_qty.meta_value) 	as quantity";
			
		
		$sql_columns .= "
			,billing_state.meta_value as billing_state
			,CONCAT(billing_country.meta_value,'-',billing_state.meta_value) as month_key
			,CONCAT(billing_country.meta_value,'-',billing_state.meta_value) as state_code
			,billing_state.meta_value as month_number ";
			
		$sql_columns .= " 	
			,billing_country.meta_value as billing_country";
		
		$sql_joins  = " {$wpdb->prefix}woocommerce_order_items 			as el_woocommerce_order_items
			LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta 	as el_woocommerce_order_itemmeta_product 			ON el_woocommerce_order_itemmeta_product.order_item_id=el_woocommerce_order_items.order_item_id
			LEFT JOIN  {$wpdb->prefix}posts 						as shop_order 									ON shop_order.id								=	el_woocommerce_order_items.order_id
			
			LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta 	as el_woocommerce_order_itemmeta_product_total 	ON el_woocommerce_order_itemmeta_product_total.order_item_id=el_woocommerce_order_items.order_item_id
			LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta 	as el_woocommerce_order_itemmeta_product_qty		ON el_woocommerce_order_itemmeta_product_qty.order_item_id		=	el_woocommerce_order_items.order_item_id
			LEFT JOIN  {$wpdb->prefix}postmeta 						as billing_country 								ON billing_country.post_id									=	shop_order.ID 
			LEFT JOIN  {$wpdb->prefix}postmeta 						as billing_state 								ON billing_state.post_id									=	shop_order.ID
			
			";
		
		
		if($category_id != NULL  && $category_id != "-1"){
			$category_id_join = " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_woocommerce_order_itemmeta_product.meta_value
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id
			LEFT JOIN  {$wpdb->prefix}terms 				as el_terms 				ON el_terms.term_id					=	term_taxonomy.term_id";
		}
		
		if($el_id_order_status != NULL  && $el_id_order_status != '-1'){
			$el_id_order_status_join = " 
			LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships2 	ON el_term_relationships2.object_id	=	el_woocommerce_order_items.order_id
			LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as el_term_taxonomy2 		ON el_term_taxonomy2.term_taxonomy_id	=	el_term_relationships2.term_taxonomy_id
			LEFT JOIN  {$wpdb->prefix}terms 				as terms2 				ON terms2.term_id					=	el_term_taxonomy2.term_id";
		}
		
				
		
		$sql_condition  = "
		el_woocommerce_order_itemmeta_product.meta_key		=	'_product_id'
		AND el_woocommerce_order_items.order_item_type		=	'line_item'
		AND shop_order.post_type						=	'shop_order'
		
		AND billing_country.meta_key							=	'_billing_country'
		AND el_woocommerce_order_itemmeta_product_total.meta_key		='_line_total'
		AND el_woocommerce_order_itemmeta_product_qty.meta_key			=	'_qty'
		AND billing_state.meta_key							=	'_billing_state'";
		
		if ($el_from_date != NULL &&  $el_to_date !=NULL)
			$el_from_date_condition = " AND DATE_FORMAT(shop_order.post_date, '%Y-%m') BETWEEN '".$el_from_date."' AND '". $el_to_date ."'";
		
		
		if($category_id  != NULL && $category_id != "-1"){
			
			$category_id_condition = " 
			AND term_taxonomy.taxonomy LIKE('product_cat')
			AND el_terms.term_id IN (".$category_id .")";
		}
		
		if($el_cat_prod_id_string  && $el_cat_prod_id_string != "-1") 
			$el_cat_prod_id_string_condition = " AND el_woocommerce_order_itemmeta_product.meta_value IN (".$el_cat_prod_id_string .")";
		
		if($el_id_order_status != NULL  && $el_id_order_status != '-1'){
			$el_id_order_status_condition .= "
			AND el_term_taxonomy2.taxonomy LIKE('shop_order_status')
			AND terms2.term_id IN (".$el_id_order_status .")";
		}
		
		if($el_product_id != NULL  && $el_product_id != '-1'){
			$el_product_id_condition  = "
			AND el_woocommerce_order_itemmeta_product.meta_value IN ($el_product_id)";
		}
		
		if($el_country_code != NULL  && $el_country_code != '-1')
			$el_country_code_condition = " 
				AND	billing_country.meta_value	IN ('{$el_country_code}')";
			
		if($state_code != NULL  && $state_code != '-1')
			$state_code_condition = " 
				AND	billing_state.meta_value	IN ('{$state_code}')";
				
		if($el_order_status  && $el_order_status != '-1' and $el_order_status != "'-1'")
			$el_order_status_condition = " AND shop_order.post_status IN (".$el_order_status.")";
		if($el_hide_os  && $el_hide_os != '-1' and $el_hide_os != "'-1'")
			$el_hide_os_condition = " AND shop_order.post_status NOT IN (".$el_hide_os.")";
		
		$sql_group_by = " group by el_woocommerce_order_itemmeta_product.meta_value ";
		$sql_order_by ="ORDER BY {$el_sort_by} {$el_order_by}";

		$sql = "SELECT $sql_columns 
			FROM $sql_joins $category_id_join $all_tax_joins $el_id_order_status_join 
			WHERE $sql_condition  $el_from_date_condition $category_id_condition $all_tax_conditions
			$el_cat_prod_id_string_condition $el_id_order_status_condition $el_product_id_condition $el_country_code_condition $state_code_condition $el_order_status_condition $el_hide_os_condition
			$sql_group_by $sql_order_by";
		
		//echo $sql;	
			
		$array_index=3;
		$this->table_cols =$this->table_columns($table_name);
		
		if(is_array($custom_tax_cols))
		{
			array_splice($this->table_cols,$array_index,0,$custom_tax_cols);
			$array_index+=count($custom_tax_cols);
		}
			
		$data_state='';
		//$country_data = $this->el_get_paying_woo_country();
		$state_codes = $this->el_get_paying_woo_state('billing_state','billing_country');
		
		
		//IF STATE SELETED
		$state_sel_arr	= '';
		if($state_code != NULL  && $state_code != '-1')
		{
			$state_code = str_replace("','", ",",$state_code);
			$state_sel_arr = explode(",",$state_code);
			
			foreach($state_codes as $state){
				if($state_code=='-1' || is_array($state_sel_arr) && in_array($state -> id,$state_sel_arr))
				{
					$data_state[]=$state -> id;
					$value=array(array('lable'=>$state -> label,'status'=>'currency'));
					array_splice($this->table_cols, $array_index++, 0, $value );
				}
			}
						
		}else if($el_country_code && $el_country_code!='-1') //IF STATE NOT SELECTED AND COUNTRY SELETED
		{
			$country_states = $this->el_get_woo_country_of_state();
			$state_codes_per_country='';
			$el_country_code		= explode(",",$this->el_get_woo_requests('el_countries_code','-1',true));
			if($el_country_code && $el_country_code!='-1')
			{		
				$index=0;
				foreach($country_states as $country_states_val){
					if(in_array($country_states_val->parent_id,$el_country_code)){
						$state_codes_per_country[$index]["id"]=$country_states_val->id;
						$state_codes_per_country[$index]["label"]=$country_states_val->label;
						$index++;
					}
				}
			}
			
			//print_r($state_codes_per_country);
			
			$state_codes=$state_codes_per_country;
			foreach($state_codes as $state){
				$data_state[]=$state ["id"];
				$value=array(array('lable'=>$state ["label"],'status'=>'currency'));
				array_splice($this->table_cols, $array_index++, 0, $value );
			}
		}else{
			foreach($state_codes as $state){
				if($state_code=='-1' || is_array($state_sel_arr) && in_array($state -> id,$state_sel_arr))
				{
					$data_state[]=$state -> id;
					$value=array(array('lable'=>$state -> label,'status'=>'currency'));
					array_splice($this->table_cols, $array_index++, 0, $value );
				}
			}
		}
		
		$value=array(array('lable'=>__('Total',__ELREPORT_TEXTDOMAIN__),'status'=>'currency'));
		array_splice($this->table_cols, $array_index, 0, $value );
		$this->data_state=$data_state;
			
		
	}elseif($file_used=="data_table"){
		
		/////////CUSTOM TAXONOMY////////
		$key=$this->el_get_woo_requests('table_names','',true);
		$visible_custom_taxonomy='';
		$post_name='product';
		$all_tax=$this->fetch_product_taxonomies( $post_name );
		$current_value=array();
		if(is_array($all_tax) && count($all_tax)>0){
			//FETCH TAXONOMY
			foreach ( $all_tax as $tax ) {
				$tax_status=get_option(__ELREPORT_FIELDS_PERFIX__.'set_default_search_'.$key.'_'.$tax);
				$show_column=get_option($key.'_'.$tax.'_column');
				if($show_column=='on'){
				//if($tax_status=='on'){
					$visible_custom_taxonomy[]=$tax;
				}
			}
		}
		///////////////////////////
		
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
			$datatable_value.=("<tr>");
				
				//Product SKU
				$display_class='';
				if($this->table_cols[0]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->el_get_prod_sku($items->order_item_id, $items->product_id);
				$datatable_value.=("</td>");
				
				//Product NAME
				$display_class='';
				if($this->table_cols[1]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->product_name;
				$datatable_value.=("</td>");
				
				//Categories
				$display_class='';
				if($this->table_cols[2]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->el_get_cn_product_id($items->product_id,"product_cat");
				$datatable_value.=("</td>");
				
				if(is_array($visible_custom_taxonomy)){
					foreach((array)$visible_custom_taxonomy as $tax){
						//Category
						$display_class='';
						if($this->table_cols[3]['status']=='hide') $display_class='display:none';
						$datatable_value.=("<td style='".$display_class."'>");
							$datatable_value.= $this->el_get_cn_product_id($items->product_id,$tax);
						$datatable_value.=("</td>");
					}
				}
				
				$type = 'total_row';$items_only = true; $id = $items->id;
				
				$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
				$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
				$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
				$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
				$el_from_date=substr($el_from_date,0,strlen($el_from_date)-3);
				$el_to_date=substr($el_to_date,0,strlen($el_to_date)-3);
				
				$params=array(
					"el_from_date"=>$el_from_date,
					"el_to_date"=>$el_to_date,
					"order_status"=>$el_order_status,
					"el_hide_os"=>'"trash"'
				);
				//print_r($arr);
				$items_product=$this->el_get_woo_cp_items($type , $items_only, $id,$params,'_billing_state');
								
				$state_arr='';
				foreach($items_product as $item_product){
					$state_arr[$item_product->billing_state]['total']=$item_product->total;
					$state_arr[$item_product->billing_state]['qty']=$item_product->quantity;
				}
				
				$j=2;
				$total=0;
				$qty=0;
				foreach($this->data_state as $state_name){
					$el_table_value=$this->price(0);
					if(isset($state_arr[$state_name]['total'])){
						$el_table_value=$this->price($state_arr[$state_name]['total']) .' #'.$state_arr[$state_name]['qty'];
						$total+=$state_arr[$state_name]['total'];
						$qty+=$state_arr[$state_name]['qty'];
					}
					
					
					$display_class='';
					if($this->table_cols[$j++]['status']=='hide') $display_class='display:none';
					$datatable_value.=("<td style='".$display_class."'>");
						$datatable_value.= $el_table_value;
					$datatable_value.=("</td>");
				}
				
				
				//Total
				$display_class='';
				if($this->table_cols[$j]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $this->price($total) .' #'.$qty;
				$datatable_value.=("</td>");
				
				
				
			$datatable_value.=("</tr>");
		}
	}elseif($file_used=="search_form"){
			$now_date= date("Y-m-d");
			$cur_year=substr($now_date,0,4);
			$el_from_date= $cur_year."-01-01";
			$el_to_date= $cur_year."-12-31";
		?>
		<form class='alldetails search_form_report' action='' method='post'>
			<input type='hidden' name='action' value='submit-form' />
			<div class="row">
				
				<div class="col-md-6">
					<div>
						<?php _e('From Date',__ELREPORT_TEXTDOMAIN__);?>
					</div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
					<input name="el_from_date" id="pwr_from_date" type="text" readonly='true' class="datepick" value="<?php echo $el_from_date;?>"/>                
				</div>
				<div class="col-md-6">
					<div class="awr-form-title">
						<?php _e('To Date',__ELREPORT_TEXTDOMAIN__);?>
					</div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
					<input name="el_to_date" id="pwr_to_date" type="text" readonly='true' class="datepick"  value="<?php echo $el_to_date;?>"/>
				</div>
                
                <?php
					/////////////////
					//CUSTOM TAXONOMY
					$key=isset($_GET['smenu']) ? $_GET['smenu']:$_GET['parent'];
					$args_f=array("page"=>$key);
					echo $this->make_custom_taxonomy($args_f);
				?>
                
                <?php
                	$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_category_id');
					if($this->get_form_element_permission('el_category_id') ||  $permission_value!=''){
						
						if(!$this->get_form_element_permission('el_category_id') &&  $permission_value!='')
							$col_style='display:none';
				?> 
                
                <div class="col-md-6"  style=" <?php echo $col_style;?>">
                    <div class="awr-form-title">
                        <?php _e('Category',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-tags"></i></span>
					<?php
                        $args = array(
                            'orderby'                  => 'name',
                            'order'                    => 'ASC',
                            'hide_empty'               => 1,
                            'hierarchical'             => 0,
                            'exclude'                  => '',
                            'include'                  => '',
                            'child_of'          		 => 0,
                            'number'                   => '',
                            'pad_counts'               => false 
                        
                        ); 
        
                        //$categories = get_categories($args); 
                        $current_category=$this->el_get_woo_requests_links('el_category_id','',true);
                        
                        $categories = get_terms('product_cat',$args);
                        $option='';
                        foreach ($categories as $category) {
							$selected='';
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($category -> id,$permission_value))
								continue;
							/*if(!$this->get_form_element_permission('el_category_id') &&  $permission_value!='')
								$selected="selected";
                            
                            if($current_category==$category->term_id)
                                $selected="selected";*/
                            
                            $option .= '<option value="'.$category->term_id.'" '.$selected.'>';
                            $option .= $category->name;
                            $option .= ' ('.$category->count.')';
                            $option .= '</option>';
                        }
                    ?>
                    <select name="el_category_id[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <?php
                        	if($this->get_form_element_permission('el_category_id') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                
                <?php
					}
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_orders_status');
					if($this->get_form_element_permission('el_orders_status') ||  $permission_value!=''){
						if(!$this->get_form_element_permission('el_orders_status') &&  $permission_value!='')
							$col_style='display:none';
				?> 
                
                <div class="col-md-6"  style=" <?php echo $col_style;?>">
                    <div class="awr-form-title">
                        <?php _e('Status',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-check"></i></span>
					<?php
                        $el_order_status=$this->el_get_woo_orders_statuses();

                        $option='';
                        foreach($el_order_status as $key => $value){
							$selected='';
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($key,$permission_value))
								continue;
							
							/*if(!$this->get_form_element_permission('el_orders_status') &&  $permission_value!='')
								$selected="selected";*/	
							
                            $option.="<option value='".$key."' $selected>".$value."</option>";
                        }
                    ?>
                
                    <select name="el_orders_status[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <?php
                        	if($this->get_form_element_permission('el_orders_status') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                        <?php
                            echo $option;
                        ?>
                    </select>  
                    <input type="hidden" name="el_id_order_status[]" id="el_id_order_status" value="-1">
                </div>	
                 
                <?php
					}
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_product_id');
					if($this->get_form_element_permission('el_product_id') ||  $permission_value!=''){
						
						if(!$this->get_form_element_permission('el_product_id') &&  $permission_value!='')
								$col_style='display:none';
					?> 
					
					<div class="col-md-6"  style=" <?php echo $col_style;?>">
                    <div class="awr-form-title">
                        <?php _e('Product',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-cog"></i></span>
					<?php
                        $products=$this->el_get_product_woo_data('all');
                        $option='';
                        $current_product=$this->el_get_woo_requests_links('el_product_id','',true);
                        //echo $current_product;
                        
                        foreach($products as $product){
							$selected='';
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($product->id,$permission_value))
								continue;
							/*if(!$this->get_form_element_permission('el_product_id') &&  $permission_value!='')
								$selected="selected";
                            
                            if($current_product==$product->id)
                                $selected="selected";*/
                            $option.="<option $selected value='".$product -> id."' >".$product -> label." </option>";
                        }
                        
                        
                    ?>
                    <select name="el_product_id[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
						<?php
                            if($this->get_form_element_permission('el_product_id') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
                            {
                        ?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
                            }
                        ?>

                        <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                 
                <?php
					}
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_countries_code');
					if($this->get_form_element_permission('el_countries_code') ||  $permission_value!=''){
						if(!$this->get_form_element_permission('el_countries_code') &&  $permission_value!='')
							$col_style='display:none';
				?> 
                
                <div class="col-md-6"  style=" <?php echo $col_style;?>">
                	<div class="awr-form-title">
						<?php _e('Country',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-globe"></i></span>
					<?php
                        $country_data = $this->el_get_paying_woo_country();
                        
                        $option='';
                        //$current_product=$this->el_get_woo_requests_links('el_product_id','',true);
                        //echo $current_product;
                        
                        foreach($country_data as $country){
							$selected='';
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($country->id,$permission_value))
								continue;
							/*if(!$this->get_form_element_permission('el_countries_code') &&  $permission_value!='')
								$selected="selected";*/
                            
                            /*if($current_product==$country->id)
                                $selected="selected";*/
                            $option.="<option $selected value='".$country -> id."' >".$country -> label." </option>";
                        }
                    ?>
                
                    <select name="el_countries_code[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <?php
                        	if($this->get_form_element_permission('el_countries_code') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                       <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                
                <?php
					}
					$col_style='';
					$permission_value=$this->get_form_element_value_permission('el_states_code');
					if($this->get_form_element_permission('el_states_code') ||  $permission_value!=''){
						
						if(!$this->get_form_element_permission('el_states_code') &&  $permission_value!='')
							$col_style='display:none';
				?> 
                
                <div class="col-md-6"  style=" <?php echo $col_style;?>">
                	<div class="awr-form-title">
						<?php _e('State',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-map"></i></span>
					<?php
                        $state_code = '-1';
                        $state_data = $this->el_get_paying_woo_state('billing_state','billing_country');
                        //print_r($state_data);
                        $option='';
                        //$current_product=$this->el_get_woo_requests_links('el_product_id','',true);
                        //echo $current_product;
                        
                        foreach($state_data as $state){
							$selected='';
							//CHECK IF IS IN PERMISSION
							if(is_array($permission_value) && !in_array($state->id,$permission_value))
								continue;
							/*if(!$this->get_form_element_permission('el_states_code') &&  $permission_value!='')
								$selected="selected";*/	
                            
                            /*if($current_product==$country->id)
                                $selected="selected";*/
                            $option.="<option $selected value='".$state -> id."' >".$state -> label." </option>";
                        }
                    ?>
                
                    <select name="el_states_code[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <?php
                        	if($this->get_form_element_permission('el_states_code') && ((!is_array($permission_value)) || (is_array($permission_value) && in_array('all',$permission_value))))
							{
						?>
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
							}
						?>
                       <?php
                            echo $option;
                        ?>
                    </select>  
                    
                </div>	
                
                <?php
					}
				?>
                
            </div>
            
            <div class="col-md-12">
                    <?php
                    	$el_hide_os='trash';
						$el_publish_order='no';
						
						$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
					?>
                    <input type="hidden" name="list_parent_category" value="">
                    <input type="hidden" name="group_by_parent_cat" value="0">
                    
                	<input type="hidden" name="el_hide_os" id="el_hide_os" value="<?php echo $el_hide_os;?>" />
                    
                    <input type="hidden" name="date_format" id="date_format" value="<?php echo $data_format;?>" />
                
                	<input type="hidden" name="table_names" value="<?php echo $table_name;?>"/>
                    <div class="fetch_form_loading search-form-loading"></div>	
                    <input type="submit" value="Search" class="button-primary"/>
					<input type="button" value="Reset" class="button-secondary form_reset_btn"/>
            </div>  
                                
        </form>
    <?php
	}
	
?>