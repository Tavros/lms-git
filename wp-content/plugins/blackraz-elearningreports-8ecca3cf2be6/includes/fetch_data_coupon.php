<?php
	
	if($file_used=="sql_table")
	{
		
		//GET POSTED PARAMETERS
		$start				= 0;
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		
		$el_coupon_code		= $this->el_get_woo_requests('coupon_code','-1',true);	
		$el_coupon_codes	= $this->el_get_woo_requests('el_codes_of_coupon','-1',true);	
		if($el_coupon_codes!="-1")
			$el_coupon_codes  		= "'".str_replace(",","','",$el_coupon_codes)."'";
		$coupon_discount_types		= $this->el_get_woo_requests('el_coupon_discount_types','-1',true);	
		if($coupon_discount_types!="-1")
			$coupon_discount_types  		= "'".str_replace(",","','",$coupon_discount_types)."'";
		$el_country_code		= $this->el_get_woo_requests('el_countries_code','-1',true);	
		
		$el_sort_by 			= $this->el_get_woo_requests('sort_by','-1',true);
		$el_order_by 			= $this->el_get_woo_requests('order_by','DESC',true);
		
		$el_id_order_status 	= $this->el_get_woo_requests('el_id_order_status',NULL,true);
		$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
		if($el_order_status!="-1")
			$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		
		///////////HIDDEN FIELDS////////////
		//$el_hide_os	= $this->el_get_woo_sm_requests('el_hide_os',$el_hide_os, "-1");
		$el_hide_os='"trash"';
		$el_publish_order='no';
		$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
		//////////////////////
		
		
		//COPUN DISCOUNT
		$coupon_discount_types_join='';
		$coupon_discount_types_condition_1='';
		$coupon_discount_types_condition_2='';
		
		//DATE
		$el_from_date_condition='';
		
		//ORDER STATUS ID
		$el_id_order_status_join='';
		$el_id_order_status_condition='';
		$el_order_status_condition='';
		
		//PUBLISH
		$el_publish_order_condition='';
		
		//COUPON COED
		$el_coupon_code_condition='';
		$el_coupon_codes_condition ='';
		
		//COUNTRY
		$el_country_code_condition='';
		
		//HIDE ORDER
		$el_hide_os_condition ='';
		
		$sql_columns = "
		el_woocommerce_order_items.order_item_name				AS		'order_item_name',
		el_woocommerce_order_items.order_item_name				AS		'coupon_code', 
		SUM(woocommerce_order_itemmeta.meta_value) 			AS		'total_amount', 
		SUM(woocommerce_order_itemmeta.meta_value) 				AS 		'coupon_amount' , 
		Count(*) 											AS 		'coupon_count'";
		
		$sql_joins = "
		{$wpdb->prefix}woocommerce_order_items as el_woocommerce_order_items 
		LEFT JOIN	{$wpdb->prefix}posts	as el_posts ON el_posts.ID = el_woocommerce_order_items.order_id
		LEFT JOIN  {$wpdb->prefix}woocommerce_order_itemmeta as woocommerce_order_itemmeta ON woocommerce_order_itemmeta.order_item_id=el_woocommerce_order_items.order_item_id
		";
		
		
		if($coupon_discount_types && $coupon_discount_types != "-1"){
			$coupon_discount_types_join = " LEFT JOIN	{$wpdb->prefix}posts	as coupons ON coupons.post_title = el_woocommerce_order_items.order_item_name";
			$coupon_discount_types_join .= " LEFT JOIN	{$wpdb->prefix}postmeta	as el_coupon_discount_type ON el_coupon_discount_type.post_id = coupons.ID";
		}
		
		if(strlen($el_id_order_status)>0 && $el_id_order_status != "-1" && $el_id_order_status != "no" && $el_id_order_status != "all"){
				$el_id_order_status_join = " 
				LEFT JOIN  {$wpdb->prefix}term_relationships 	as el_term_relationships 	ON el_term_relationships.object_id		=	el_posts.ID
				LEFT JOIN  {$wpdb->prefix}term_taxonomy 		as term_taxonomy 		ON term_taxonomy.term_taxonomy_id	=	el_term_relationships.term_taxonomy_id";
		}
		
		$sql_condition = "
		el_posts.post_type 								=	'shop_order'
		AND el_woocommerce_order_items.order_item_type		=	'coupon' 
		AND woocommerce_order_itemmeta.meta_key			=	'discount_amount'";
		
		if($coupon_discount_types && $coupon_discount_types != "-1"){
			$coupon_discount_types_condition_1 = " AND coupons.post_type 				=	'shop_coupon'";
			$coupon_discount_types_condition_1 .= " AND el_coupon_discount_type.meta_key		=	'discount_type'";
		}
		if ($el_from_date != NULL &&  $el_to_date !=NULL){
			$el_from_date_condition = " AND DATE(el_posts.post_date) BETWEEN '".$el_from_date."' AND '". $el_to_date ."'";
		}
		
		if(strlen($el_id_order_status)>0 && $el_id_order_status != "-1" && $el_id_order_status != "no" && $el_id_order_status != "all"){
			$el_id_order_status_condition = " AND  term_taxonomy.term_id IN ({$el_id_order_status})";
		}
		
		if(strlen($el_publish_order)>0 && $el_publish_order != "-1" && $el_publish_order != "no" && $el_publish_order != "all"){
			$in_post_status		= str_replace(",","','",$el_publish_order);
			$el_publish_order_condition= " AND  el_posts.post_status IN ('{$in_post_status}')";
		}
		
		if($el_coupon_code && $el_coupon_code != "-1"){
			$el_coupon_code_condition = " AND (el_woocommerce_order_items.order_item_name IN ('{$el_coupon_code}') OR el_woocommerce_order_items.order_item_name LIKE '%{$el_coupon_code}%')";
		}
		
		if($el_coupon_codes && $el_coupon_codes != "-1"){
			$el_coupon_codes_condition = " AND el_woocommerce_order_items.order_item_name IN ({$el_coupon_codes})";
		}
		
		if($coupon_discount_types && $coupon_discount_types != "-1"){
			$coupon_discount_types_condition_2 = " AND el_coupon_discount_type.meta_value IN ({$coupon_discount_types})";
		}
		
		if($el_country_code && $el_country_code != "-1"){
			$el_country_code_condition= " AND billing_country.meta_value IN ({$el_country_code})";
		}
		
		if($el_order_status  && $el_order_status != '-1' and $el_order_status != "'-1'")
			$el_order_status_condition = " AND el_posts.post_status IN (".$el_order_status.")";
		
		if($el_hide_os  && $el_hide_os != '-1' and $el_hide_os != "'-1'")
			$el_hide_os_condition = " AND el_posts.post_status NOT IN (".$el_hide_os.")";
		
		/*if($request['report_name'] == "coupon_couontry_page"){
			$sql_group_by = " Group BY billing_country.meta_value, el_woocommerce_order_items.order_item_name";
			//$sql .= " Group BY   el_woocommerce_order_items.order_id";
			$sql_order_by = " ORDER BY {$el_sort_by} {$el_order_by}";
		}else{
			$sql_group_by = " Group BY el_woocommerce_order_items.order_item_name";
			$sql_order_by = " ORDER BY total_amount DESC";
		}	*/
		$sql_group_by = " Group BY el_woocommerce_order_items.order_item_name";
		$sql_order_by = " ORDER BY total_amount DESC";
			
		$sql = "SELECT $sql_columns
				FROM $sql_joins $coupon_discount_types_join $el_id_order_status_join
				WHERE $sql_condition $coupon_discount_types_condition_1 $el_from_date_condition 
				$el_id_order_status_condition $el_publish_order_condition $el_coupon_code_condition
				$el_coupon_codes_condition $coupon_discount_types_condition_2
				$el_country_code_condition $el_order_status_condition $el_hide_os_condition 
				$sql_group_by $sql_order_by";			
		
		//echo $sql;
	}
	elseif($file_used=="data_table"){
		
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
			$datatable_value.=("<tr>");
										
				//Coupon Code
				$display_class='';
				if($this->table_cols[0]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->coupon_code;
				$datatable_value.=("</td>");
	
				//Coupon Count
				$display_class='';
				if($this->table_cols[1]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->coupon_count;
				$datatable_value.=("</td>");
				
				//Coupon Amount
				$display_class='';
				if($this->table_cols[2]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->coupon_amount == 0 ? 0 : $this->price($items->coupon_amount);
				$datatable_value.=("</td>");
				
			$datatable_value.=("</tr>");
		}
	}elseif($file_used=="search_form"){
	?>
		<form class='alldetails search_form_report' action='' method='post'>
            <input type='hidden' name='action' value='submit-form' />
            <div class="row">
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('From Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_from_date" id="pwr_from_date" type="text" readonly='true' class="datepick"/>
                </div>
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('To Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_to_date" id="pwr_to_date" type="text" readonly='true' class="datepick"/>
                    <input type="hidden" name="el_id_order_status[]" id="el_id_order_status" value="-1">
                    <input type="hidden" name="el_orders_status[]" id="order_status" value="<?php echo $this->el_shop_status; ?>">
                </div>
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                    	<?php
                        	$el_coupon_codes=$this->el_get_woo_coupons_codes();
							$option='';
							foreach($el_coupon_codes as $coupon){
								$selected='';
								/*if($current_product==$product->id)
									$selected="selected";*/
								$option.="<option $selected value='".$coupon -> id."' >".$coupon -> label." </option>";
							}
						?>
                        <?php _e('Coupon Codes',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-key"></i></span>
                    <select name="el_codes_of_coupon[]" multiple="multiple" size="5"  data-size="5" class="chosen-select-search">
                        <option value="-1"><?php _e('Select All',__ELREPORT_TEXTDOMAIN__);?></option>
                        <?php
                            echo $option;
                        ?>
                    </select>  	
                </div>
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('Discount Type',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-money"></i></span>
                    <select name="el_coupon_discount_types" >
                        <option value="-1">Select One</option>
                        <option value="fixed_cart">Cart Discount</option>
                        <option value="percent">Cart % Discount</option>
                        <option value="fixed_product">Product Discount</option>
                        <option value="percent_product">Product % Discount</option>
                    </select>
                </div>
                
            </div>  
             
            <div class="col-md-12">
               
                    <?php
                    	$el_hide_os='trash';
						$el_publish_order='no';
						$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
					?>
                    <input type="hidden" name="list_parent_category" value="">
                    <input type="hidden" name="el_category_id" value="-1">
                    <input type="hidden" name="group_by_parent_cat" value="0">
                    
                	<input type="hidden" name="el_hide_os" id="el_hide_os" value="<?php echo $el_hide_os;?>" />
                    
                    <input type="hidden" name="date_format" id="date_format" value="<?php echo $data_format;?>" />
                
                	<input type="hidden" name="table_names" value="<?php echo $table_name;?>"/>
                    <div class="fetch_form_loading search-form-loading"></div>	
                    <input type="submit" value="Search" class="button-primary"/>
					<input type="button" value="Reset" class="button-secondary form_reset_btn"/>							
            </div>
                                
        </form>
    <?php
	}
	
?>