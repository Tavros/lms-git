<?php
	
	if($file_used=="sql_table")
	{
		
		//GET POSTED PARAMETERS
		$request 			= array();
		$start				= 0;
		$el_from_date		  = $this->el_get_woo_requests('el_from_date',NULL,true);
		$el_to_date			= $this->el_get_woo_requests('el_to_date',NULL,true);
		$el_id_order_status 	= $this->el_get_woo_requests('el_id_order_status',NULL,true);
		$el_order_status		= $this->el_get_woo_requests('el_orders_status','-1',true);
		$el_order_status  		= "'".str_replace(",","','",$el_order_status)."'";
		
		///////////HIDDEN FIELDS////////////
		//$el_hide_os	= $this->el_get_woo_sm_requests('el_hide_os',$el_hide_os, "-1");
		$el_hide_os='"trash"';
		$el_publish_order='no';
		
		$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
		//////////////////////
		
		//ORDER SATTUS
		$el_id_order_status_join='';
		$el_order_status_condition='';
		
		//ORDER STATUS
		$el_id_order_status_condition='';
		
		//DATE
		$el_from_date_condition='';
		
		//PUBLISH ORDER
		$el_publish_order_condition='';
		
		//HIDE ORDER STATUS
		$el_hide_os_condition ='';
		
		$this->constants['post_order_status_found']	= 1;
		
		$sql_columns= "COUNT(postmeta.meta_value) AS 'order_count'
		,SUM(postmeta.meta_value) AS 'total_amount'";
		
		if($this->constants['post_order_status_found'] == 0 ){
			$sql_columns .= "  ,el_terms.name As 'order_status', term_taxonomy.term_id AS 'StatusID'";
		
			$sql_joins  = " {$wpdb->prefix}posts as el_posts";
			
			$sql_joins .= "
			LEFT JOIN  {$wpdb->prefix}term_relationships as el_term_relationships ON el_term_relationships.object_id=el_posts.ID
			LEFT JOIN  {$wpdb->prefix}term_taxonomy as term_taxonomy ON term_taxonomy.term_taxonomy_id=el_term_relationships.term_taxonomy_id
			LEFT JOIN  {$wpdb->prefix}terms as el_terms ON el_terms.term_id=term_taxonomy.term_id";
		}else{
			$sql_columns .= "  , el_posts.post_status As 'order_status' ,el_posts.post_status As 'StatusID'";
			$sql_joins = " {$wpdb->prefix}posts as el_posts";
		}
		
		$sql_joins .= "  
		LEFT JOIN  {$wpdb->prefix}postmeta as postmeta ON postmeta.post_id=el_posts.ID";
		$sql_condition="postmeta.meta_key = '_order_total'
		AND el_posts.post_type='shop_order'";	
		if ($el_from_date != NULL &&  $el_to_date !=NULL){
			$el_from_date_condition  = " AND DATE(el_posts.post_date) BETWEEN '".$el_from_date."' AND '". $el_to_date ."'";
		}
		
		if(strlen($el_publish_order)>0 && $el_publish_order != "-1" && $el_publish_order != "no" && $el_publish_order != "all"){
			$in_post_status		= str_replace(",","','",$el_publish_order);
			$el_publish_order_condition  = " AND  el_posts.post_status IN ('{$in_post_status}')";
		}
		
		//if($el_order_status  && $el_order_status != '-1' and $el_order_status != "'-1'")$sql .= " AND el_posts.post_status IN (".$el_order_status.")";
		if($el_hide_os  && $el_hide_os != '-1' and $el_hide_os != "'-1'")
			$el_hide_os_condition = " AND el_posts.post_status NOT IN (".$el_hide_os.")";
		
		$sql_group_by  = " Group BY order_status ORDER BY total_amount DESC";
		
		$sql = "SELECT  $sql_columns FROM $sql_joins WHERE $sql_condition
				 $el_from_date_condition $el_publish_order_condition
				$el_order_status_condition $el_hide_os_condition 
				$sql_group_by";
		
		//echo $sql;
		
	}elseif($file_used=="data_table"){
		
		foreach($this->results as $items){
		//for($i=1; $i<=20 ; $i++){
			$datatable_value.=("<tr>");
				
				//Order Status
				$el_table_value = isset($items->order_status) ? $items->order_status : '';
					
				if($el_table_value=='wc-completed')
					$el_table_value = '<span class="awr-order-status awr-order-status-'.sanitize_title($el_table_value).'" >'.ucwords(__($el_table_value, __ELREPORT_TEXTDOMAIN__)).'</span>';
				else if($el_table_value=='wc-refunded')
					$el_table_value = '<span class="awr-order-status awr-order-status-'.sanitize_title($el_table_value).'" >'.ucwords(__($el_table_value, __ELREPORT_TEXTDOMAIN__)).'</span>';
				else if($el_table_value=='auto-draft')
					$el_table_value = '';	
				else
					$el_table_value = '<span class="awr-order-status awr-order-status-'.sanitize_title($el_table_value).'" >'.ucwords(__($el_table_value, __ELREPORT_TEXTDOMAIN__)).'</span>';	
				
				$display_class='';
				if($this->table_cols[0]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.=  str_replace("Wc-","",$el_table_value);
				$datatable_value.=("</td>");
				
				//Order Count
				$display_class='';
				if($this->table_cols[1]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items -> order_count;
				$datatable_value.=("</td>");
				
				//Amount
				$display_class='';
				if($this->table_cols[2]['status']=='hide') $display_class='display:none';
				$datatable_value.=("<td style='".$display_class."'>");
					$datatable_value.= $items->total_amount == 0 ? 0 : $this->price($items->total_amount);
				$datatable_value.=("</td>");
				
			$datatable_value.=("</tr>");
		}
	}elseif($file_used=="search_form"){
	?>
		<form class='alldetails search_form_report' action='' method='post'>
            <input type='hidden' name='action' value='submit-form' />
            <div class="row">
                
                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('From Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_from_date" id="pwr_from_date" type="text" readonly='true' class="datepick"/>
                </div>

                <div class="col-md-6">
                    <div class="awr-form-title">
                        <?php _e('To Date',__ELREPORT_TEXTDOMAIN__);?>
                    </div>
					<span class="awr-form-icon"><i class="fa fa-calendar"></i></span>
                    <input name="el_to_date" id="pwr_to_date" type="text" readonly='true' class="datepick"/>
                    
                    <input type="hidden" name="el_id_order_status[]" id="el_id_order_status" value="-1">
                    <input type="hidden" name="el_orders_status[]" id="order_status" value="<?php echo $this->el_shop_status; ?>">
                </div>
                
            </div>
            
            <div class="col-md-12">
                    <?php
                    	$el_hide_os='trash';
						$el_publish_order='no';
						
						$data_format=$this->el_get_woo_requests_links('date_format',get_option('date_format'),true);
					?>
                    <input type="hidden" name="list_parent_category" value="">
                    <input type="hidden" name="el_category_id" value="-1">
                    <input type="hidden" name="group_by_parent_cat" value="0">
                    
                	<input type="hidden" name="el_hide_os" id="el_hide_os" value="<?php echo $el_hide_os;?>" />
                    
                    <input type="hidden" name="date_format" id="date_format" value="<?php echo $data_format;?>" />
                
                	<input type="hidden" name="table_names" value="<?php echo $table_name;?>"/>
                    <div class="fetch_form_loading search-form-loading"></div>	
                    <input type="submit" value="Search" class="button-primary"/>
					<input type="button" value="Reset" class="button-secondary form_reset_btn"/>
            </div>  
                                
        </form>
    <?php
	}
	
?>