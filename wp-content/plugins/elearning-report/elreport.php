<?php
/*
Plugin Name: eLearning Report
Plugin URI: #
Description: eLearning Report for front end
Version: 1.0.0
Author: R. Hovhannisyan
Author URI: #
*/

define('ELREPORT_DIR', rtrim( plugin_dir_path( __FILE__ ), '/' ));
define('ELREPORT_URL', rtrim( plugins_url( '', __FILE__ ), '/' ));

/**
* 
*/
class elReport
{
	function __construct(){
		$this->init_admin();
		$this->enqueue_scripts();
		$this->init_shortcodes();
	}

	private function init_admin(){
		add_action('admin_enqueue_scripts', array($this,'admin_enqueue'));
		add_action('admin_footer', array($this, 'add_dialog'));
		add_action('init', array($this, 'add_buttons_and_ext_plugin'));
	}

	private function enqueue_scripts(){
		add_action('wp_enqueue_scripts', array($this, 'frontend_enqueue'));
	}

	private function init_shortcodes(){
		add_shortcode('elreport-dashboard-report', array($this, 'show_dashboard_report'));
		add_shortcode('elreport-details', array($this, 'show_details'));
		add_shortcode('elreport-customerbuyproducts', array($this, 'show_customerbuyproducts'));
		add_shortcode('elreport-paymentgateway', array($this, 'show_paymentgateway'));
		add_shortcode('elreport-refunddetails', array($this, 'show_refunddetails'));
		add_shortcode('elreport-coupon', array($this, 'show_coupon'));
		add_shortcode('elreport-tax-reports', array($this, 'show_tax_reports'));
		
		//Crosstab Reports
		add_shortcode('elreport-variation', array($this, 'show_variation'));
		add_shortcode('elreport-stock-list', array($this, 'show_stock_list'));
		add_shortcode('elreport-variation-stock', array($this, 'show_variation_stock'));
		add_shortcode('elreport-projected-actual-sale', array($this, 'show_projected_actual_sale'));
	}

	/**
     * register editor plugin javascript if current user can edit posts      
     */
    function add_buttons_and_ext_plugin() {
        if (get_user_option('rich_editing') == 'true') {
            add_filter('mce_buttons', array(&$this, 'add_mce_buttons'), -1);
            add_filter('mce_external_plugins', array(&$this, 'add_mce_external_plugins'), -1);
        }
    }

	/**
     * add button on wordpress post editor panel
     */
    function add_mce_buttons($buttons) {
        global $wp_version;
        if (version_compare($wp_version, '3.9', '>=')) {
            array_push($buttons, 'dialog');
        } else {
            array_push($buttons, '|', 'dialog_button');
        }
        return $buttons;
    }

    /**
     * register editor plugin
     * @return registered plugins array
     */
    function add_mce_external_plugins() {
        global $wp_version;
        if (version_compare($wp_version, '3.5', '>=') && version_compare($wp_version, '3.9', '<')) {
            $plugin_array['ACPPlugin'] = ELREPORT_URL . '/assets/js/tinymce/acp-tinymce_3.8.3.js';
        } elseif (version_compare($wp_version, '3.9', '>=') && version_compare($wp_version, '4.4', '<')) {
            $plugin_array['ACPPlugin'] = ELREPORT_URL . '/assets/js/tinymce/acp-tinymce_3.9.js';
        } elseif (version_compare($wp_version, '4.4', '>=')) {
            $plugin_array['ACPPlugin'] = ELREPORT_URL . '/assets/js/tinymce/acp-tinymce_4.4.js';
        }
        return $plugin_array;
    }

    /**
     * the dialog html to add shortcodes
     */
    function add_dialog() {
        include(ELREPORT_DIR . '/assets/dialog.php');
    }

	function admin_enqueue(){
		$jsvars = array(
			'button_img' => ELREPORT_URL . "/assets/imgs/button.png",
			'dialog_title' => 'eLearning Report'
		);
		wp_register_script('acp-options-js', ELREPORT_URL . '/assets/js/options.js', array('jquery'));
        wp_localize_script('acp-options-js', 'acpjs', array('options' =>$jsvars));
        wp_enqueue_script('acp-options-js');
	}

	function frontend_enqueue(){
		include (ELREPORT_DIR . "/includes/front-embed.php");
	}

	function show_dashboard_report(){
		include(ELREPORT_DIR . "/class/dashboard_report.php");
	}

	function show_details(){
		include(ELREPORT_DIR . "/class/details.php");
	}

	function show_customerbuyproducts(){
		include(ELREPORT_DIR . "/class/customerbuyproducts.php");
	}

	function show_paymentgateway(){
		include(ELREPORT_DIR . "/class/paymentgateway.php");
	}

	function show_refunddetails(){
		include(ELREPORT_DIR . "/class/refunddetails.php");
	}

	function show_coupon(){
		include(ELREPORT_DIR . "/class/coupon.php");
	}

	function show_tax_reports(){
		include(ELREPORT_DIR . "/class/tax_reports.php");
	}

	function show_variation(){
		include(ELREPORT_DIR . "/class/variation.php");
	}

	function show_stock_list(){
		include(ELREPORT_DIR . "/class/stock_list.php");
	}

	function show_variation_stock(){
		include(ELREPORT_DIR . "/class/variation_stock.php");
	}

	function show_projected_actual_sale(){
		include(ELREPORT_DIR . "/class/projected_actual_sale.php");
	}
}

if( class_exists('pw_report_wcreport_class') ) new elReport;