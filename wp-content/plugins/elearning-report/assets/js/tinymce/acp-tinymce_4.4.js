(function ($) {
    tinymce.PluginManager.add('ACPPlugin', function (editor, url) {
        editor.addButton('dialog', {
            text: '',
            tooltip: 'elReport',
            image: acpjs.options.button_img,
            onclick: function () {
                var w = $(window).width();
                var h = $(window).height();
                var dialogWidth = 600;
                var dialogHeight = 400;
                var H = (dialogHeight < h) ? dialogHeight : h;
                var W = (dialogWidth < w) ? dialogWidth : w;
                $('#shortcode_title').val('');
                $('.acp_button_layout .acp_button_title').text('');
                $('.shortcode_dialog input[type=text],.shortcode_dialog textarea').removeClass('has_error');
                tb_show(acpjs.options.dialog_title, '#TB_inline?width=' + W + '&height=' + H + '&inlineId=acp_dialog');
            }
        });
    });

    $('#elreport-dashboard-report').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-dashboard-report]');
        tb_remove();
    });
    $('#elreport-details').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-details]');
        tb_remove();
    });
    $('#elreport-customerbuyproducts').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-customerbuyproducts]');
        tb_remove();
    });
    $('#elreport-paymentgateway').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-paymentgateway]');
        tb_remove();
    });
    $('#elreport-refunddetails').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-refunddetails]');
        tb_remove();
    });
    $('#elreport-coupon').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-coupon]');
        tb_remove();
    });
    $('#elreport-tax-reports').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-tax-reports]');
        tb_remove();
    });
    $('#elreport-variation').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-variation]');
        tb_remove();
    });
    $('#elreport-stock-list').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-stock-list]');
        tb_remove();
    });
    $('#elreport-variation-stock').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-variation-stock]');
        tb_remove();
    });
    $('#elreport-projected-actual-sale').click(function () {
        tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[elreport-projected-actual-sale]');
        tb_remove();
    });
    
    function htmlEntities(str) {
        return String(str).replace(/"/g, '&quot;');
    }
    
    $('#shortcode_title').change(function () {
        $('.acp_button_layout .acp_button_title').text($(this).val());
    });
})(jQuery);