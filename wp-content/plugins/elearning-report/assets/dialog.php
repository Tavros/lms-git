<style>#TB_window{ background:#F0F0F0}</style>
<div id="acp_dialog" style="display:none;">
    <div class="shortcode_dialog">    
        
        <fieldset style="border: 1px solid black; margin: 15px;">
            <legend style="margin-left: 5px;">Reports</legend>

            <button id="elreport-dashboard-report">Dashboard Report</button><br><br>
            <button id="elreport-details">All Orders</button><br><br>
            <button id="elreport-customerbuyproducts">CUSTOM MEMBER BUY PRODUCTS</button><br><br>
            <button id="elreport-paymentgateway">Payment Gateways</button><br><br>
            <button id="elreport-refunddetails">REFUND DETAILS</button><br><br>
            <button id="elreport-coupon">COUPON</button><br><br>
            <button id="elreport-tax-reports">TAX REPORTS</button><br><br>
        </fieldset>

        <fieldset style="border: 1px solid black; margin: 15px;">
            <legend style="margin-left: 5px;">Crosstab Reports</legend>
        
            <button id="elreport-variation">variation</button><br><br>
            <button id="elreport-stock-list">stock-list</button><br><br>
            <button id="elreport-variation-stock">variation-stock</button><br><br>
            <button id="elreport-projected-actual-sale">projected-actual-sale</button>
        </fieldset>

    </div>
</div>