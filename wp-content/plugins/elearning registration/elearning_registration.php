<?php
/*
Plugin Name: Elearning Registration
Description: Elearning Registration
Version:  1.0
Author: LMS

*/

/**

 * When registering, add the member to a specific membership level

 * @param integer $user_id

 **/
//error_reporting(0);
//Disables the pmpro redirect to levels page when user tries to register

add_filter( "pmpro_login_redirect", "__return_false" );

function my_pmpro_default_registration_level( $user_id ) {

//Give all members who register membership level 1
    global $wpdb ;

    $table_member_level = $wpdb->prefix."pmpro_membership_levels";

    $membership_level_pay  = $wpdb->get_row("SELECT  initial_payment FROM $table_member_level" );
    $membership_level_id   = $wpdb->get_row("SELECT  id FROM $table_member_level WHERE 'initial_payment' = 0" );

    if(isset($membership_level_pay->initial_payment) && $membership_level_pay->initial_payment != '' && $membership_level_pay->initial_payment<=0){
        if(get_membership_level_from_submiting($cfdata) && get_membership_level_from_submiting($cfdata)!='') {
            pmpro_changeMembershipLevel(get_membership_level_from_submiting($cfdata), $user_id);
        }else{
            pmpro_changeMembershipLevel($membership_level_id, $user_id);
        }
    }

}

add_action( 'user_register', 'my_pmpro_default_registration_level' );

/*
 * Get the membership level from submition form
 *
 *  @return membership level
 * **/

function get_membership_level_from_submiting($cfdata)
{

    if (!isset($cfdata->posted_data) && class_exists('WPCF7_Submission')) {
        $submission = WPCF7_Submission::get_instance();

        if ($submission) {
            $formdata = $submission->get_posted_data();
        }
    } elseif (isset($cfdata->posted_data)) {
        $formdata = $cfdata->posted_data;
    } else {
        return $cfdata;
    }

        if(isset($formdata['membershiplevel']) && !empty($formdata['membershiplevel'])){
            $membershiplevel = $formdata['membershiplevel'];
            return $membershiplevel;

        }

}
add_action('wpcf7_before_send_mail', 'get_membership_level_from_submiting',1);



/*
 *  Function name:  Create user from registration
 *  Description:    Creating wp-user from home page by registering with contact form 7
 *
 */



function create_user_from_registration( $cfdata ) {
global $woocommerce, $pmprowoo_product_levels;



    if ( !isset( $cfdata->posted_data ) && class_exists( 'WPCF7_Submission' ) ) {
        $submission = WPCF7_Submission::get_instance();

        if ( $submission ) {
            $formdata = $submission->get_posted_data();
        }
    } elseif ( isset( $cfdata->posted_data ) ) {
        $formdata = $cfdata->posted_data;
    } else {
        return $cfdata;
    }

    // Check this is the user registration form

    if ( $cfdata->title() == 'Registrants' ) {

        if(isset($formdata['regsitrationpassword']) && !empty($formdata['regsitrationpassword'])){
            $password = $formdata['regsitrationpassword'];
        }else {
            $password = wp_generate_password(12, false);
        }

        $email = $formdata['registrationemail'];
        $name = $formdata['your-name'];
        if(isset($formdata['membershiplevel']) && !empty($formdata['membershiplevel'])){
            $membershiplevel = $formdata['membershiplevel'];
        }

        if(isset($formdata['registrationuser']) && !empty($formdata['registrationuser'])){
            $membership_username = $formdata['registrationuser'];
        }


        // Construct a username from the user's name
        $username = strtolower( str_replace( ' ', '', $name ) );
        $name_parts = explode( ' ',$name );

        if ( !email_exists( $email ) ) {
            // Find an unused username
            $username_tocheck = $username;

            $i = 1;
            while ( username_exists( $username_tocheck ) ) {
                $username_tocheck = $username . $i++;
            }
            $username = $username_tocheck;
            // Create the user
            $userdata = array(
                'user_login' => $username,
                'user_pass' => $password,
                'user_email' => $email,
                'nickname' => reset($name_parts),
                'display_name' => $name,
                'first_name' => reset($name_parts),
                'last_name' => end($name_parts),
                'role' => 'student'
            );

            global $wpdb ;

            $table_member_level = $wpdb->prefix."pmpro_membership_levels";

            if(get_membership_level_from_submiting($cfdata) && get_membership_level_from_submiting($cfdata)!='' && get_membership_level_from_submiting($cfdata)!= NULL) {
                $membership_level_pay = $wpdb->get_row("SELECT initial_payment FROM $table_member_level WHERE id =" . get_membership_level_from_submiting($cfdata));

            } else {

                $membership_level_id   = $wpdb->get_row("SELECT  id  FROM $table_member_level WHERE 'initial_payment' = 0" );
                $membership_level_pay = $wpdb->get_row("SELECT initial_payment FROM $table_member_level WHERE id =" . $membership_level_id->id);

            }



             if(isset($membership_level_pay->initial_payment) && $membership_level_pay->initial_payment != '' && $membership_level_pay->initial_payment<=0){

                 $user_id = wp_insert_user( $userdata );

                 wp_set_auth_cookie($user_id);
                 $product_id = array_search(get_membership_level_from_submiting($cfdata), $pmprowoo_product_levels);
                 $order_data = array(
                     'status'        => 'wc-completed',
                     'customer_id'   => $user_id,
                     'customer_note' => '',
                     'total'         => '',
                 );


                 $woocommerce->cart->add_to_cart($product_id);
                 $order = wc_create_order($order_data);
                 $order->add_product( get_product( $product_id ));
                 $order->calculate_totals();
                 $order->update_status( "wc-completed", 'Imported order', TRUE );
                 $message_for_free_membership = "You have been regitered to a free membership";
                 wp_mail( $email, 'Free membership level', $message_for_free_membership);



             }
            if ( !is_wp_error( $user_id ) ) {
                // Email login details to user
                $blogname = wp_specialchars_decode( get_option( 'blogname' ) );
                $message = "Welcome! Your login details are as follows:" . "\r\n";
                $message .= sprintf( __('Username: %s'), $username ) . "\r\n";
                $message .= sprintf( __('Password: %s'), $password ) . "\r\n";
                $message .= wp_login_url() . "\r\n";
                wp_mail( $email, sprintf( __('[%s] Your username and password'), $blogname ), $message );
            }

        }

    }
    if(isset($membership_level_pay->initial_payment) && $membership_level_pay->initial_payment != '' && $membership_level_pay->initial_payment>0){
        $user_id = wp_insert_user( $userdata );
        wp_set_auth_cookie($user_id);

//        print_r($woocommerce->cart->get_checkout_url());die();


        $product_id = array_search(get_membership_level_from_submiting($cfdata), $pmprowoo_product_levels);
        $order_data = array(

            'customer_id'   => $user_id,

        );
        $woocommerce->cart->add_to_cart($product_id);
        $order = wc_create_order($order_data);
        $order->add_product( get_product( $product_id ));
//        $order->calculate_totals();
//        $order->update_status( "Completed", 'Imported order', TRUE );
        $contact_form =$cfdata;
        do_action( 'wpcf7_before_send_mail', $contact_form );
        wp_redirect($woocommerce->cart->get_checkout_url());


        exit;
    }
        else {
//            print_r(wp_authenticate($membership_username,$password));die;
            return $cfdata;
        }
}
add_action( 'wpcf7_before_send_mail', 'create_user_from_registration', 1 );




//
//function add_membership_level_to_cart() {
//    global $woocommerce, $pmprowoo_product_levels;
//
//    if ( ! is_admin() ) {
//
//        $product_id = array_search(get_membership_level_from_submiting(), $pmprowoo_product_levels);
//
//
//        $found = false;
//        //check if product already in cart
//        if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
//            foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
//                $_product = $values['data'];
//                if ( $_product->id == $product_id )
//                    $found = true;
//            }
//            // if product not found, add it
//            if ( ! $found )
//                $woocommerce->cart->add_to_cart( $product_id );
//        } else {
//            // if no products in cart, add it
//            $woocommerce->cart->add_to_cart( $product_id );
//        }
//
////        $order = wc_create_order();
//
//
//    }
//}
////add_action( 'wpcf7_before_send_mail', 'add_membership_level_to_cart' );

/*  Check if user with same email exists in Contact form database , then disallow the registration
 *
 * Function name: is_already_submitted
 * @param $formName
 * @param $fieldName
 * @param $fieldValue
 *
 * */


function is_already_submitted($formName, $fieldName, $fieldValue) {
    require_once(ABSPATH . 'wp-content/plugins/contact-form-7-to-database-extension/CFDBFormIterator.php');
    $exp = new CFDBFormIterator();
    $atts = array();
    $atts['show'] = $fieldName;
    $atts['filter'] = "$fieldName=$fieldValue";
    $atts['unbuffered'] = 'true';
    $exp->export($formName, $atts);
    $found = false;
    while ($row = $exp->nextRow()) {
        $found = true;
    }
    return $found;

}


function email_login_admin() {
$url = get_site_url();
echo'<form id="vbp-login-form" class="standard-form" action="';echo $url;echo'/wp-login.php" method="post" name="login-form">
            <div class="inside_login_form"><label>Username
            <input id="side-user-login" class="input" tabindex="1" name="log" type="text" value="" /></label>
            <div class="checkbox small"><input id="sidebar-rememberme" name="sidebar-rememberme" type="checkbox" value="forever" /><label for="sidebar-rememberme">Remember Me</label></div>
                <input id="sidebar-wp-submit" tabindex="100" name="user-submit" type="submit" value="Log In" data-security="2fb60a29db" />
                <input name="user-cookie" type="hidden" value="1" />
            <a class="vbpregister" tabindex="5" title="Create an account" href=';echo $url;echo'/register/">Sign Up</a>
            </div>
        </form>';
	

}
add_shortcode('email_login_admin','email_login_admin');

function email_login_student(){
//$url = get_site_url();
	     echo'<form id="vbp-login-form" class="standard-form" action="';echo $url;echo'/wp-login.php" method="post" name="login-form">
		 <div class="inside_login_form">

		 <label>Email
		 <input id="side-user-login" class="input" tabindex="1" name="log" type="text" value="" /></label><label>Password
		 <input id="sidebar-user-pass" class="input" tabindex="2" name="pwd" type="password" value="" /></label>

		 </div>
		 <div class="inside_login_form">
		 <div class="checkbox small"><input id="sidebar-wp-submit" tabindex="100" name="user-submit" type="submit" value="Log In" data-security="2fb60a29db" />
		 <input name="user-cookie" type="hidden" value="1" />
		 <a class="vbpregister" tabindex="5" title="Create an account"href=';echo $url;echo'/register/">Sign Up</a></div>
		 </div>
		 </form>';
}
add_shortcode('email_login_student','email_login_student');




/*
 * Function name lms_membership
 *
 * @param  url , membership level id
 *
 * adding shortcode by name lms_membership
 *
 * */

function lms_membership_level ($args) {
    //Get membership level ID
    if(isset($args['mem_id']) && $args['mem_id'] !='' && $args['mem_id'] != NULL) {
        $mem_id = $args['mem_id'];
    }
    //Get url to redirect
    if(isset($args['url']) && $args['url'] !='' && $args['url'] != NULL) {
        $url_to_redirect = $args['url'].'?mem_id='.$mem_id;
    }
    if(isset($args['text']) && $args['text'] !='' && $args['text'] != NULL) {
        $button_name = $args['text'];
    }else{
        $button_name = "Membership level";
    }
    //Creating button
    return '<a class="btn btn-primary" href="'.$url_to_redirect.'">'.$button_name.'</a>';

}

add_shortcode('lms_membership_level', 'lms_membership_level');





/**
 * @param $result WPCF7_Validation
 * @param $tag array
 * @return WPCF7_Validation
 */
function my_validate_email($result, $tag) {
    $formName = 'Registrants'; // Change to name of the form containing this field
    $fieldName = 'registrationemail'; // Change to your form's unique field name
    $errorMessage = 'Email has already been submitted'; // Change to your error message
    $name = $tag['name'];
        if ($name == $fieldName) {
            if (is_already_submitted($formName, $fieldName, $_POST[$name])) {
                $username = $_POST[$name];
                $user = get_user_by( 'email', $username );
                $username = $user->user_login;
                $password = $user->user_pass;
                wp_set_auth_cookie($user->ID);
                wp_redirect( '/events/' );
                exit;
            }
    }
    return $result;
}

// use the next line if your field is a **required email** field on your form
add_filter('wpcf7_validate_email*', 'my_validate_email', 10, 2);



/*NEW USER AUTOMATIC APPROVE*/

/*Customize metaboxes in admin panel new-user-approve plugin*/
remove_action( 'admin_init','add_meta_boxes');

add_action('admin_init','custom_meta_box');

/*
 * Add checkbox for auto approve
 *
 * **/

function custom_meta_box() {
        add_meta_box( 'nua-auto-approve', __( 'Approve users automaticly', 'new-user-auto-approve' ), 'metabox_auto_approve' , 'users_page_new-user-approve-admin', 'side', 'default' );
}


function metabox_auto_approve(){
    ?>
        <form method="POST">
            <label for="auto_approve_checkox">Automaticly approve new users:&nbsp; </label><input type="checkbox" name="auto_approve_checkbox" value="Y"  <?php if(get_auto_update_option() == "Y"): ?> checked="checked"<?php endif;?> >
            <input type="submit" name="set_auto_approve" value="SET">
        </form>
    <?php
    if (get_auto_update_option() != NULL && !empty(get_auto_update_option()) && get_auto_update_option() =='') {
        update_auto_update_option();
    }else{
        add_auto_update_option();
    }
        if (get_auto_update_option() != NULL &&  get_auto_update_option() == "Y") {
            change_user_status_to_approved();
        }
}
/*
 * Add input checkbox to DB
 *
 * **/
function add_auto_update_option() {

    if (isset($_POST['auto_approve_checkbox']) && !empty($_POST['auto_approve_checkbox']) && $_POST['auto_approve_checkbox'] == 'Y'  && isset($_POST['set_auto_approve']) && !empty($_POST['set_auto_approve']) &&  $_POST['set_auto_approve'] == "SET") {
        $opt_value = $_POST['auto_approve_checkbox'];
        add_option( 'user_auto_approve', $opt_value);
        header("Refresh:0");

    }elseif(isset($_POST['set_auto_approve']) && !empty($_POST['set_auto_approve']) &&  $_POST['set_auto_approve'] == "SET"){
        $opt_value = "N";
        update_option( 'user_auto_approve', $opt_value );
        header("Refresh:0");

    }
    if(get_auto_update_option() == 'N' &&  $_POST['auto_approve_checkbox'] == 'Y'){
        $opt_value = "Y";
        update_option( 'user_auto_approve', $opt_value );
        header("Refresh:0");

    }
}

function update_auto_update_option() {
    $opt_value = $_POST['auto_approve_checkbox'];
    update_option( 'user_auto_approve', $opt_value );
    header("Refresh:0");
}

/*
 * Get input checkbox value from DB
 *
 * */

function get_auto_update_option() {
    return get_option( 'user_auto_approve' , 'N');
}


function change_user_status_to_approved() {
    global  $wpdb;
    $table = $wpdb->prefix."usermeta";
    $wpdb->show_errors();
    $sql = "SELECT meta_value FROM ".$table." WHERE  meta_key = 'pw_user_status'";
    $result = $wpdb->get_results($sql);

    for ($i = 0; $i <= count($result); $i++) {
        if($result[$i]->meta_value == 'pending'){
            $data = array(
                'meta_value' => 'approved',
            );
            $where = array(
                'meta_key' => 'pw_user_status'
            );
            $wpdb->update($table,$data,$where);
        }
    }
}


function create_contact_form_on_homepage() {
    global $wpdb;
//Create Contact form by name Contact FOrm 1
    $args = array(
        'id'                => -1,
        'title'             => 'Registrants',
        'locale'            => 'en_US',
        'form'              =>  '<label>Firstname (required)</label>
                                   [text* your-name]
                                    <label>Lastname (required)</label>
                                        [text* your-lastname]
                                    <label>Your Email (required)</label>
                                        [email* registrationemail]
                                    <label>Password</label>
                                        [text* regsitrationpassword]
                                
                                    [submit "Send"]'
    );

    wpcf7_save_contact_form( $args, $context = 'save' );

    $table_name_for_contact_form = $wpdb->prefix."posts";
    $sql_cont_id = "SELECT ID FROM ".$table_name_for_contact_form." WHERE post_name = 'contact-form-1' AND post_status = 'publish' AND post_type = 'wpcf7_contact_form'";
    $contactform_id = $wpdb->get_row($sql_cont_id);



//Insert shortcode to Home Page

    $guid = "https://".$_SERVER['HTTP_HOST']."/home/";

    $table_name_post = $wpdb->prefix."posts";

    $sql = "SELECT ID FROM ".$table_name_post." WHERE post_name = 'home-page' AND post_type='page'  AND post_status = 'publish'";

    $post_id = $wpdb->get_row($sql);

    $post  = array(
        'ID' => $post_id->ID,
        'post_author' => 1,
        'post_content' => '[vc_row full_width="stretch_row" css=".vc_custom_1491566452039{margin-top: 10px !important;margin-bottom: 0px !important;padding-bottom: 40px !important;background-image: url(https://presentation.first.am/wp-content/uploads/2017/04/bam-bg-2.png?id=130) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}"][vc_column][vc_row_inner css=".vc_custom_1491563302672{margin-bottom: 0px !important;padding-bottom: 0px !important;}"][vc_column_inner width="1/3"][/vc_column_inner][vc_column_inner width="1/3" css=".vc_custom_1491566164609{background-color: rgba(16,38,76,0.75) !important;*background-color: rgb(16,38,76) !important;}"][vc_custom_heading text="Q3 ATLAS FUNDAMENTAL TRADING" font_container="tag:h2|text_align:center|color:%23ffffff" google_fonts="font_family:Open%20Sans%3A300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic|font_style:400%20regular%3A400%3Anormal"][vc_column_text][contact-form-7 id="'.$contactform_id->ID.'" title="Registrants"][/vc_column_text][/vc_column_inner][vc_column_inner width="1/3"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]',
        'post_content_filtered' => '',
        'post_title' => 'Home Page',
        'post_excerpt' => '',
        'post_status' => 'publish',
        'post_type' => 'page',
        'comment_status' => 'closed',
        'ping_status' => 'closed',
        'post_password' => '',
        'to_ping' =>  '',
        'pinged' => '',
        'post_parent' => 0,
        'menu_order' => 0,
        'guid' => $guid,


    );
    if($contactform_id->ID &&$contactform_id->ID != '' && $contactform_id->ID != NULL ) {
        wp_update_post($post);
    }

}






register_activation_hook( __FILE__, 'create_contact_form_on_homepage' );
//register_activation_hook( __FILE__,  );
?>