<?php /*
Plugin Name:gadwp Custom report
Plugin URI: http://
Description:
Version: 1.0.0
Author:
Author URI: http://
*/
define('GADWPC_DIR', rtrim( plugin_dir_path( __FILE__ ), '/' ));
define('GADWPC_URL', rtrim( plugins_url( '', __FILE__ ), '/' ));
require_once(GADWPC_DIR."/includes/dpc_ajax.php");

class Gadwp_custom{

    public function __construct(){
        $this->enqueue_css_js();
        $this->init_shortcodes();
       
        add_action("init",array($this, 'init_ajax'));
    }

    public function init_ajax(){
        new DPC_Backend_Ajax;
    }

    private function enqueue_css_js() {
        wp_enqueue_script( 'gadwp_custom', GADWPC_URL . '/assets/js/reports.js', array( 'gadwp-nprogress', 'googlecharts', 'jquery', 'jquery-ui-dialog','gadwp-frontend-item-reports' ),false,true);
        wp_enqueue_style( 'gadwp-custom', GADWPC_URL . '/assets/css/report.css',  array(), '20120208', 'all');
    }

    private function init_shortcodes(){
        add_shortcode('elga-button', array($this, 'show_button'));
        // add_shortcode('dp-custom-form', array($this, 'show_custom_form'));
    }

    public function show_button($atts){
        $atts = shortcode_atts( array( 'post_id'=> 1 ), $atts );
        $post_id = $atts['post_id'];
        echo '<div id="gadwp_custom"></div><input type="hidden" id="dp_list" value="' . $post_id . '" />';
    }

    /*public function show_custom_form(){ ?>
        <h2>Google Analytics Settings</h2>
        <form name="input" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>" method="post">
            <table class="gadwp-settings-options">
                <tr>
                    <td colspan="2" class="gadwp-settings-info">
                        <?php echo __( "Use this link to get your access code:", 'google-analytics-dashboard-for-wp' ) . ' <a href="" id="gapi-access-code" target="_blank">' . __ ( "Get Access Code", 'google-analytics-dashboard-for-wp' ) . '</a>.'; ?>
                    </td>
                </tr>
                <tr>
                    <td class="gadwp-settings-title">
                        <label for="ga_dash_code" title="<?php _e("Use the red link to get your access code!",'google-analytics-dashboard-for-wp')?>"><?php echo _e( "Access Code:", 'google-analytics-dashboard-for-wp' ); ?></label>
                    </td>
                    <td>
                        <input type="text" id="ga_dash_code" name="ga_dash_code" value="" size="61" required="required" title="<?php _e("Use the red link to get your access code!",'google-analytics-dashboard-for-wp')?>">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" class="button button-secondary" name="ga_dash_authorize" value="<?php _e( "Save Access Code", 'google-analytics-dashboard-for-wp' ); ?>" />
                    </td>
                </tr>
            </table>
        </form>
        <?php 
    }*/
}

function elga_init(){
    if( class_exists('GADWP_Manager') ) new Gadwp_custom;
}
add_action( 'plugins_loaded', 'elga_init', 10 );