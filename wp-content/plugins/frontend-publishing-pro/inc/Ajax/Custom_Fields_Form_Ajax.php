<?php
namespace WPFEPP\Ajax;

if (!defined('WPINC')) die;

class Custom_Fields_Form_Ajax extends \WPFEPP\Ajax
{
	const ACTION = 'wpfepp_create_custom_field';

	function __construct()
	{
		parent::__construct();
		$this->register_action(self::ACTION_PREFIX . self::ACTION, array($this, 'create_custom_field'));
	}

	function create_custom_field()
	{
		$post_type = $_POST[ \WPFEPP\Element_Containers\Custom_Fields_Container::ELEM_KEY_POST_TYPE ];
		$form = new \WPFEPP\Forms\Custom_Fields_Form($post_type);

		$success = $form->handle_submission();

		$result = array(
			'success' => $success
		);

		if ($success) {
			$result['field_html'] = $form->get_custom_field_html();
		} else {
			$result['errors'] = $form->get_printable_errors();
		}

		die(json_encode($result));
	}
}