<?php
namespace WPFEPP\Constants;

if (!defined('WPINC')) die;

abstract class Data_Settings
{
	const SETTINGS_DELETE_DATA = 'delete_on_uninstall';
	const SETTINGS_FORCE_DELETE_ON_NETWORK = 'force_delete_on_network';
}