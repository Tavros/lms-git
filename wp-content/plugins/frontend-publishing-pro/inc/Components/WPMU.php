<?php
namespace WPFEPP\Components;

use WP_Site;
use WPFEPP\Constants\Data_Settings;
use WPFEPP\Constants\Option_Ids;
use WPFEPP\Element_Containers\Data_Settings_Container;
use WPGurus\Components\Component;

if (!defined('WPINC')) die;

class WPMU extends Component
{
	public function __construct()
	{
		parent::__construct();

		$this->register_activation_action(array($this, 'create_tables_for_existing_sites'));
		$this->register_action('wpmu_new_blog', array($this, 'create_tables_for_new_site'));
		$this->register_action('wpfepp_uninstall', array($this, 'delete_data'), 9);
	}

	public function delete_data()
	{
		if (!is_multisite()) {
			return;
		}

		$force_delete = $this->force_delete_data();
		$sites = $this->get_sites();
		foreach ($sites as $site) {
			$this->delete_data_for_sub_site($site->blog_id, $force_delete);
		}
	}

	public function create_tables_for_existing_sites($network_wide)
	{
		if (!is_multisite() || !$network_wide) {
			return;
		}

		$sites = $this->get_sites();
		foreach ($sites as $site) {
			$this->create_tables_for_sub_site($site->blog_id);
		}
	}

	/**
	 * @param $blog_id int The ID of the new blog
	 */
	public function create_tables_for_new_site($blog_id)
	{
		if (!is_multisite() || !$this->plugin_active_for_network()) {
			return;
		}

		$this->create_tables_for_sub_site($blog_id);
	}

	/**
	 * @param $blog_id int ID of the site.
	 */
	private function create_tables_for_sub_site($blog_id)
	{
		if (is_main_site($blog_id)) {
			return;
		}

		switch_to_blog($blog_id);

		$db_setup = new DB_Setup();
		$db_setup->create_tables();

		restore_current_blog();
	}

	/**
	 * @param $blog_id int ID of the site.
	 * @param $force_delete boolean
	 */
	private function delete_data_for_sub_site($blog_id, $force_delete)
	{
		if (is_main_site($blog_id)) {
			return;
		}

		switch_to_blog($blog_id);

		$data_deleter = new Data_Deleter();
		$data_deleter->delete_data($force_delete);

		restore_current_blog();
	}

	/**
	 * @return bool
	 */
	private function plugin_active_for_network()
	{
		if (!function_exists('is_plugin_active_for_network')) {
			require_once(ABSPATH . '/wp-admin/includes/plugin.php');
		}

		return is_plugin_active_for_network('frontend-publishing-pro/frontend-publishing-pro.php');
	}

	/**
	 * @return WP_Site[]
	 */
	private function get_sites()
	{
		return get_sites(
			array(
				'number' => false
			)
		);
	}

	/**
	 * @return mixed
	 */
	private function force_delete_data()
	{
		$data_settings_container = new Data_Settings_Container();
		$data_settings = $data_settings_container->parse_values(get_option(Option_Ids::OPTION_DATA_SETTINGS));
		$force_delete = $data_settings[ Data_Settings::SETTINGS_FORCE_DELETE_ON_NETWORK ];
		return $force_delete;
	}
}