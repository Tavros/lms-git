<?php
namespace WPFEPP\Components;

if (!defined('WPINC')) die;

use WPFEPP\Constants\Data_Settings;
use WPFEPP\Constants\Option_Ids;
use WPFEPP\DB_Tables\Form_Meta;
use WPFEPP\DB_Tables\Forms;
use WPFEPP\Element_Containers\Data_Settings_Container;
use WPGurus\Components\Component;

class Data_Deleter extends Component
{
	function __construct()
	{
		parent::__construct();

		$this->register_action('wpfepp_uninstall', array($this, 'delete_data'));
	}

	function delete_data($force_delete = false)
	{
		$data_settings_container = new Data_Settings_Container();
		$data_settings = $data_settings_container->parse_values(get_option(Option_Ids::OPTION_DATA_SETTINGS));

		if (!$data_settings[ Data_Settings::SETTINGS_DELETE_DATA ] && !$force_delete)
			return;

		$form_table = new Forms();
		$form_table->delete_table();
		delete_option($form_table->get_version_option_name());

		$form_meta_table = new Form_Meta();
		$form_meta_table->delete_table();
		delete_option($form_meta_table->get_version_option_name());

		delete_option(Option_Ids::OPTION_DATA_SETTINGS);
		delete_option(Option_Ids::OPTION_EMAIL_SETTINGS);
		delete_option(Option_Ids::OPTION_GENERAL_FORM_SETTINGS);
		delete_option(Option_Ids::OPTION_MEDIA_SETTINGS);
		delete_option(Option_Ids::OPTION_MESSAGES);
		delete_option(Option_Ids::OPTION_POST_LIST_SETTINGS);
		delete_option(Option_Ids::OPTION_RECAPTCHA_SETTINGS);
		delete_option(Option_Ids::OPTION_REWRITE_RULES_FLUSHED);
		delete_option(Option_Ids::OPTION_NAG_DISMISSED);
		delete_option(Option_Ids::OPTION_HAS_INCOMPATIBLE_CHANGES);
		delete_option(Option_Ids::OPTION_UPDATE_SETTINGS);

		delete_option('wpfepp_version');
	}
}