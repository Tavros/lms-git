<?php
namespace WPGurus\Forms\Elements;

if (!defined('WPINC')) die;

use WPGurus\Config\Config_Loader;

/**
 * An extension of the Media class, this element is used when a multiple media ids needs to be captured.
 *
 * Class Media_IDs
 * @package WPGurus\Forms\Elements
 */
class Media_IDs extends Media
{
	function __construct($args)
	{
		$text_domain = Config_Loader::get_config('text_domain');
		$args[ Media::FRAME_TITLE ] = __('Select Items', $text_domain);
		$args[ Media::FRAME_BUTTON_TEXT ] = __('Select', $text_domain);
		$args[ Media::MULTIPLE ] = true;

		parent::__construct($args);
	}

	/**
	 * Takes multiple media IDs and returns preview HTML for them.
	 *
	 * @param $ids
	 * @return string
	 */
	protected function render_preview_html($ids)
	{
		$html = '';

		if ($ids) {
			foreach (explode(',', $ids) as $id) {
				$html .= wp_get_attachment_image(trim($id), 'thumbnail');
			}
		}

		echo $html;
	}
}