<?php 
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<footer>
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="footertop">
                <?php 
                            if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar('topfootersidebar') ) : ?>
                <?php endif; ?>
            </div>
        </div>
    </div> 
    <div id="scrolltop">
        <a><i class="icon-arrow-1-up"></i><span><?php _e('top','eLearning'); ?></span></a>
    </div>
</footer>
<div id="footerbottom">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-3">
                <h2 id="footerlogo"><a href="<?php echo eLearning_site_url('','logo'); ?>"><img src="<?php  echo apply_filters('eLearning_logo_url',ELEARNING_URL.'/assets/images/logo.png','footer'); ?>" alt="<?php echo get_bloginfo('name'); ?>" /></a></h2>
                <?php $copyright=eLearning_get_option('copyright'); echo (isset($copyright)?do_shortcode($copyright):'&copy; 2013, All rights reserved.'); ?>
            </div>
            <div class="col-md-9">
                <?php
                    $footerbottom_right = eLearning_get_option('footerbottom_right');
                    if(isset($footerbottom_right) && $footerbottom_right){
                        echo '<div id="footer_social_icons">';
                        echo eLearning_socialicons();
                        echo '</div>';
                    }else{
                        ?>
                        <div id="footermenu">
                            <?php
                                    $args = array(
                                        'theme_location'  => 'footer-menu',
                                        'container'       => '',
                                        'menu_class'      => 'footermenu',
                                        'fallback_cb'     => 'eLearning_set_menu',
                                    );
                                    wp_nav_menu( $args );
                            ?>
                        </div> 
                        <?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>
</div><!-- END PUSHER -->
</div><!-- END MAIN -->
	<!-- SCRIPTS -->
<?php
wp_footer();
?> 
</body>
</html>