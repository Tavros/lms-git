<?php 

if ( !defined( 'ABSPATH' ) ) exit;
$register_id = eLearning_get_bp_page_id('register');

$site_lock = eLearning_get_option('site_lock');
if(!empty($site_lock)){
	include_once(get_template_directory()."/login-page.php");  
	exit;
}

get_header( eLearning_get_header() ); 
?>
<section id="title">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="pagetitle">
                    <?php
                        $breadcrumbs=get_post_meta($register_id,'eLearning_breadcrumbs',true);
                        if(eLearning_validate($breadcrumbs)){
                         eLearning_breadcrumbs();
                        }
                    ?>
                    <h1><?php the_title(); ?></h1>
                    <?php the_sub_title($register_id); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
	<div class="<?php echo eLearning_get_container(); ?>">
		<div class="col-md-9 col-sm-8">
		
		<div class="content padder">

			<div id="buddypress">

				<?php

				/**
				 * Fires at the top of the BuddyPress member registration page template.
				 *
				 * @since 1.1.0
				 */
			//	do_action( 'bp_before_register_page' ); ?>

				<div class="page" id="register-page">

					<form action="" name="signup_form" id="signup_form" class="standard-form" method="post" enctype="multipart/form-data">

					<?php if ( 'registration-disabled' == bp_get_current_signup_step() ) : ?>
						<?php

						/** This action is documented in bp-templates/bp-legacy/buddypress/activity/index.php */
						do_action( 'template_notices' ); ?>
						<?php

						/**
						 * Fires before the display of the registration disabled message.
						 *
						 * @since 1.5.0
						 */
						do_action( 'bp_before_registration_disabled' ); ?>

							<p><?php _e( 'User registration is currently not allowed.', 'eLearning' ); ?></p>

						<?php

						/**
						 * Fires after the display of the registration disabled message.
						 *
						 * @since 1.5.0
						 */
						do_action( 'bp_after_registration_disabled' ); ?>
					<?php endif; // registration-disabled signup step ?>

					<?php if ( 'request-details' == bp_get_current_signup_step() ) : ?>

						<?php

						/** This action is documented in bp-templates/bp-legacy/buddypress/activity/index.php */
						do_action( 'template_notices' ); 

						$content ='';
						if(!empty($register_id)){
							$content = get_post_field('post_content',$register_id);
						}
						if(!empty($content)){
							echo apply_filters('the_content',$content);
						}else{
							echo '<p>'.__( 'Registering for this site is easy. Just fill in the fields below, and we\'ll get a new account set up for you in no time.', 'eLearning' ).'</p>';
						}

						/**
						 * Fires before the display of member registration account details fields.
						 *
						 * @since 1.1.0
						 */
						do_action( 'bp_before_account_details_fields' ); ?>
							<div<?php bp_field_css_class( 'editfield' ); ?>>
							<?php

							/**
							 * Fires and displays any extra member registration details fields.
							 *
							 * @since 1.9.0
							 */
							do_action( 'bp_account_details_fields' ); ?>

						</div><!-- #basic-details-section -->

						<?php

						/**
						 * Fires after the display of member registration account details fields.
						 *
						 * @since 1.1.0
						 */
						do_action( 'bp_after_account_details_fields' ); ?>

						<?php /***** Extra Profile Details ******/ ?>

						<?php if ( bp_is_active( 'xprofile' ) ) : ?>

							<?php

							/**
							 * Fires before the display of member registration xprofile fields.
							 *
							 * @since 1.2.4
							 */
							do_action( 'bp_before_signup_profile_fields' ); ?>


							<?php

							/**
							 * Fires after the display of member registration xprofile fields.
							 *
							 * @since 1.1.0
							 */
							do_action( 'bp_after_signup_profile_fields' ); ?>

						<?php endif; ?>

						<?php if ( bp_get_blog_signup_allowed() ) : ?>

							<?php

							/**
							 * Fires before the display of member registration blog details fields.
							 *
							 * @since 1.1.0
							 */
							do_action( 'bp_before_blog_details_fields' ); ?>

							<?php /***** Blog Creation Details ******/ ?>

							<div class="register-section" id="blog-details-section">

								<h4><?php _e( 'Blog Details', 'eLearning' ); ?></h4>

								<p><label for="signup_with_blog"><input type="checkbox" name="signup_with_blog" id="signup_with_blog" value="1"<?php if ( (int) bp_get_signup_with_blog_value() ) : ?> checked="checked"<?php endif; ?> /> <?php _e( 'Yes, I\'d like to create a new site', 'eLearning' ); ?></label></p>

								<div id="blog-details"<?php if ( (int) bp_get_signup_with_blog_value() ) : ?>class="show"<?php endif; ?>>

									<label for="signup_blog_url"><?php _e( 'Blog URL', 'eLearning' ); ?> <?php _e( '(required)', 'eLearning' ); ?></label>
									<?php

									/**
									 * Fires and displays any member registration blog URL errors.
									 *
									 * @since 1.1.0
									 */
									do_action( 'bp_signup_blog_url_errors' ); ?>

									<?php if ( is_subdomain_install() ) : ?>
										http:// <input type="text" name="signup_blog_url" id="signup_blog_url" value="<?php bp_signup_blog_url_value(); ?>" /> .<?php bp_signup_subdomain_base(); ?>
									<?php else : ?>
										<?php echo home_url( '/' ); ?> <input type="text" name="signup_blog_url" id="signup_blog_url" value="<?php bp_signup_blog_url_value(); ?>" />
									<?php endif; ?>

									<label for="signup_blog_title"><?php _e( 'Site Title', 'eLearning' ); ?> <?php _e( '(required)', 'eLearning' ); ?></label>
									<?php

									/**
									 * Fires and displays any member registration blog title errors.
									 *
									 * @since 1.1.0
									 */
									do_action( 'bp_signup_blog_title_errors' ); ?>
									<input type="text" name="signup_blog_title" id="signup_blog_title" value="<?php bp_signup_blog_title_value(); ?>" />

									<span class="label"><?php _e( 'I would like my site to appear in search engines, and in public listings around this network.', 'eLearning' ); ?></span>
									<?php

									/**
									 * Fires and displays any member registration blog privacy errors.
									 *
									 * @since 1.1.0
									 */
									do_action( 'bp_signup_blog_privacy_errors' ); ?>

									<label for="signup_blog_privacy_public"><input type="radio" name="signup_blog_privacy" id="signup_blog_privacy_public" value="public"<?php if ( 'public' == bp_get_signup_blog_privacy_value() || !bp_get_signup_blog_privacy_value() ) : ?> checked="checked"<?php endif; ?> /> <?php _e( 'Yes', 'eLearning' ); ?></label>
									<label for="signup_blog_privacy_private"><input type="radio" name="signup_blog_privacy" id="signup_blog_privacy_private" value="private"<?php if ( 'private' == bp_get_signup_blog_privacy_value() ) : ?> checked="checked"<?php endif; ?> /> <?php _e( 'No', 'eLearning' ); ?></label>

									<?php

									/**
									 * Fires and displays any extra member registration blog details fields.
									 *
									 * @since 1.9.0
									 */
									do_action( 'bp_blog_details_fields' ); ?>

								</div>

							</div><!-- #blog-details-section -->

							<?php

							/**
							 * Fires after the display of member registration blog details fields.
							 *
							 * @since 1.1.0
							 */
							do_action( 'bp_after_blog_details_fields' ); ?>

						<?php endif; ?>

						<?php

						/**
						 * Fires before the display of the registration submit buttons.
						 *
						 * @since 1.1.0
						 */
						do_action( 'bp_before_registration_submit_buttons' ); ?>

						<?php

						/**
						 * Fires after the display of the registration submit buttons.
						 *
						 * @since 1.1.0
						 */
						do_action( 'bp_after_registration_submit_buttons' ); ?>

						<?php wp_nonce_field( 'bp_new_signup' ); ?>

					<?php endif; // request-details signup step ?>

					<?php if ( 'completed-confirmation' == bp_get_current_signup_step() ) : ?>

						<?php

						/** This action is documented in bp-templates/bp-legacy/buddypress/activity/index.php */
						do_action( 'template_notices' ); ?>
						<?php

						/**
						 * Fires before the display of the registration confirmed messages.
						 *
						 * @since 1.5.0
						 */
						do_action( 'bp_before_registration_confirmed' ); ?>

						<?php if ( bp_registration_needs_activation() ) : ?>
							<h3 class="heading"><span><?php _e('Activate your account','eLearning'); ?></span></h3>
							<p><?php _e( 'You have successfully created your account! To begin using this site you will need to activate your account via the email we have just sent to your address.', 'eLearning' ); ?></p>
						<?php else : ?>
							<h3 class="heading"><span><?php _e('Account creatd, login to your account','eLearning'); ?></span></h3>
							<p><?php _e( 'You have successfully created your account! Please log in using the username and password you have just created.', 'eLearning' ); ?></p>
						<?php endif; ?>

						<?php

						/**
						 * Fires after the display of the registration confirmed messages.
						 *
						 * @since 1.5.0
						 */
						do_action( 'bp_after_registration_confirmed' ); ?>

					<?php endif; // completed-confirmation signup step ?>

					<?php

					/**
					 * Fires and displays any custom signup steps.
					 *
					 * @since 1.1.0
					 */
					do_action( 'bp_custom_signup_steps' ); ?>

					</form>

				</div>

				<?php

				/**
				 * Fires at the bottom of the BuddyPress member registration page template.
				 *
				 * @since 1.1.0
				 */
			//	do_action( 'bp_after_register_page' ); ?>

			</div><!-- #buddypress -->

		</div><!-- .padder -->
		</div>
		<div class="col-md-3 col-sm-4">
			<div class="sidebar">
			<?php
		 		$sidebar = apply_filters('eLearning_sidebar','buddypress',$register_id);
                if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar($sidebar) ) : ?>
           	<?php endif; ?>
			</div>
		</div>
	</div>	
</section><!-- #content -->
	<script type="text/javascript">
		jQuery(document).ready( function() {
			if ( jQuery('div#blog-details').length && !jQuery('div#blog-details').hasClass('show') )
				jQuery('div#blog-details').toggle();

			jQuery( 'input#signup_with_blog' ).click( function() {
				jQuery('div#blog-details').fadeOut().toggle();
			});
		});
	</script>

<?php get_footer( eLearning_get_footer() );  