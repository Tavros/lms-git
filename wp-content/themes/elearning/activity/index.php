<?php

/**
 * Template Name: BuddyPress - Activity Directory
 *
 * @package BuddyPress
 * @subpackage Theme
 */
if ( !defined( 'ABSPATH' ) ) exit;

do_action('eLearning_before_activity_directory');

get_header( eLearning_get_header() ); 

$directory_layout = eLearning_get_customizer('directory_layout');

eLearning_include_template("directory/activity/index$directory_layout.php");  

get_footer( eLearning_get_footer() );  

