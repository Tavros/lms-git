<?php
/**
* Template Name: FullWidth Page
*/
get_header(eLearning_get_header());

if ( have_posts() ) : while ( have_posts() ) : the_post();

    $v_add_content = get_post_meta( $post->ID, '_add_content', true );

$title=get_post_meta(get_the_ID(),'eLearning_title',true);
if(eLearning_validate($title) || empty($title)){
?> 
<section id="title">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="pagetitle">
                    <?php
                        $breadcrumbs=get_post_meta(get_the_ID(),'eLearning_breadcrumbs',true);
                        if(eLearning_validate($breadcrumbs) || empty($breadcrumbs))
                            eLearning_breadcrumbs(); 
                    ?>
                    <h1><?php the_title(); ?></h1>
                    <?php the_sub_title(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
}
?>
<section id="content">
    <div class="<?php echo $v_add_content;?> content">
        <?php
            the_content();
            $page_comments = eLearning_get_option('page_comments');
            if(!empty($page_comments))
                comments_template();
         ?>
    </div>
           
</section>
<?php
endwhile;
endif; 
?>
<?php
get_footer( eLearning_get_footer() );