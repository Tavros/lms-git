<?php

if ( is_user_logged_in() ) :
	do_action( 'bp_before_sidebar_me' ); ?>
	<div id="sidebar-me">
		<div id="bpavatar">
			<?php bp_loggedin_user_avatar( 'type=full' ); ?>
		</div>
		<ul>
			<li id="username"><a href="<?php bp_loggedin_user_link(); ?>"><?php bp_loggedin_user_fullname(); ?></a></li>
			<?php do_action('eLearning_header_top_login'); ?>
			<li><a href="<?php echo bp_loggedin_user_domain() . BP_XPROFILE_SLUG ?>/" title="<?php _e('View profile','eLearning'); ?>"><?php _e('View profile','eLearning'); ?></a></li>
			<li id="vbplogout"><a href="<?php echo wp_logout_url( get_permalink() ); ?>" id="destroy-sessions" rel="nofollow" class="logout" title="<?php _e( 'Log Out','eLearning' ); ?>"><i class="icon-close-off-2"></i> <?php _e('LOGOUT','eLearning'); ?></a></li>
			<li id="admin_panel_icon"><?php if (current_user_can("edit_posts"))
		       echo '<a href="'.eLearning_site_url() .'wp-admin/" title="'.__('Access admin panel','eLearning').'"><i class="icon-settings-1"></i></a>'; ?>
		  </li>
		</ul>	
		<ul>
<?php
$loggedin_menu = array(
  'courses'=>array(
              'icon' => 'icon-book-open-1',
              'label' => __('Courses','eLearning'),
              'link' => bp_loggedin_user_domain().BP_COURSE_SLUG
              ),
  'stats'=>array(
              'icon' => 'icon-analytics-chart-graph',
              'label' => __('Stats','eLearning'),
              'link' => bp_loggedin_user_domain().BP_COURSE_SLUG.'/'.BP_COURSE_STATS_SLUG
              )
  );
if ( bp_is_active( 'messages' ) ){
  $loggedin_menu['messages']=array(
              'icon' => 'icon-letter-mail-1',
              'label' => __('Inbox','eLearning').(messages_get_unread_count()?' <span>' . messages_get_unread_count() . '</span>':''),
              'link' => bp_loggedin_user_domain().BP_MESSAGES_SLUG
              );
}
if ( bp_is_active( 'notifications' ) ){  
  $n=vbp_current_user_notification_count();
  $loggedin_menu['notifications']=array(
              'icon' => 'icon-exclamation',
              'label' => __('Notifications','eLearning').(($n)?' <span>'.$n.'</span>':''),
              'link' => bp_loggedin_user_domain().BP_NOTIFICATIONS_SLUG
              );
}
if ( bp_is_active( 'groups' ) ){
  $loggedin_menu['groups']=array(
              'icon' => 'icon-myspace-alt',
              'label' => __('Groups','eLearning'),
              'link' => bp_loggedin_user_domain().BP_GROUPS_SLUG 
              );
}

$loggedin_menu['settings']=array(
              'icon' => 'icon-settings',
              'label' => __('Settings','eLearning'),
              'link' => bp_loggedin_user_domain().BP_SETTINGS_SLUG
              );
$loggedin_menu = apply_filters('eLearning_logged_in_top_menu',$loggedin_menu);
foreach($loggedin_menu as $item){
  echo '<li><a href="'.$item['link'].'"><i class="'.$item['icon'].'"></i>'.$item['label'].'</a></li>';
}
?>
		</ul>
	
	<?php
	do_action( 'bp_sidebar_me' ); ?>
	</div>
	<?php do_action( 'bp_after_sidebar_me' );

/***** If the user is not logged in, show the log form and account creation link *****/

else :
	if(!isset($user_login))$user_login='';
	do_action( 'bp_before_sidebar_login_form' ); ?>
	
	<div class="fullscreen_login">
		<a id="close_full_popup"></a>				
		<form name="login-form" id="vbp-login-form" class="standard-form" action="<?php echo apply_filters('eLearning_login_widget_action',site_url( 'wp-login.php', 'login_post' )); ?>" method="post">
			<a href="<?php echo eLearning_site_url(); ?>" class="login_logo"><img src="<?php  echo apply_filters('eLearning_logo_url',ELEARNING_URL.'/assets/images/logo.png'); ?>" alt="<?php echo get_bloginfo('name'); ?>" /></a>
			<div class="inside_login_form">
				<label><?php _e( 'Username', 'eLearning' ); ?><br />
				<input type="text" name="log" id="side-user-login" class="input" tabindex="1" value="<?php echo esc_attr( stripslashes( $user_login ) ); ?>" /></label>
				
				<label><?php _e( 'Password', 'eLearning' ); ?> <a href="<?php echo wp_lostpassword_url(); ?>" tabindex="5" class="tip vbpforgot" title="<?php _e('Forgot Password','eLearning'); ?>"><i class="icon-question"></i></a><br />
				<input type="password" tabindex="2" name="pwd" id="sidebar-user-pass" class="input" value="" /></label>
				
			    <div class="checkbox small">
			    	<input type="checkbox" name="sidebar-rememberme" id="sidebar-rememberme" value="forever" /><label for="sidebar-rememberme"><?php _e( 'Remember Me', 'eLearning' ); ?></label>
			    </div>
				
				<?php do_action( 'bp_sidebar_login_form' ); ?>
				<input type="submit" name="user-submit" id="sidebar-wp-submit" data-security="<?php echo wp_create_nonce('eLearning_signon'); ?>" value="<?php _e( 'Log In','eLearning' ); ?>" tabindex="100" />
				<input type="hidden" name="user-cookie" value="1" />

				<?php 
				$enable_signup = apply_filters('eLearning_enable_signup',0);
	            if ( $enable_signup ) : 
	    			$registration_link = apply_filters('eLearning_buddypress_registration_link',site_url( BP_REGISTER_SLUG . '/' ));
					printf(  '<a href="%s" class="vbpregister" title="'.__('Create an account','eLearning').'" tabindex="5" >'.__( 'Sign Up','eLearning' ).'</a> ', $registration_link );
				endif; ?>
  			<?php do_action( 'login_form' ); //BruteProtect FIX 
  			?>
  			</div>
  			<?php
  			do_action( 'bp_after_sidebar_login_form' );
  			?>
		</form>
	</div>
	<?php
endif;
?>
