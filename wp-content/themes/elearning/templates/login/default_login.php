<?php
if (is_user_logged_in() && function_exists('bp_loggedin_user_avatar')) :
    do_action('bp_before_sidebar_me');
    ?>
    <div id="sidebar-me">
        <div id="bpavatar">
            <?php bp_loggedin_user_avatar('type=full'); ?>
        </div>
        <ul>
            <li id="username"><a href="<?php bp_loggedin_user_link(); ?>"><?php bp_loggedin_user_fullname(); ?></a></li>
            <?php do_action('eLearning_header_top_login'); ?>
<!--            <li><a href="<?php echo bp_loggedin_user_domain() . BP_XPROFILE_SLUG ?>/" title="<?php _e('View profile', 'eLearning'); ?>"><?php _e('View profile', 'eLearning'); ?></a></li>
            <li id="vbplogout"><a href="<?php echo wp_logout_url(get_permalink()); ?>" id="destroy-sessions" rel="nofollow" class="logout" title="<?php _e('Log Out', 'eLearning'); ?>"><i class="icon-close-off-2"></i> <?php _e('LOGOUT', 'eLearning'); ?></a></li>
            <li id="admin_panel_icon"><?php
                if (current_user_can("edit_posts"))
                    echo '<a href="' . eLearning_site_url() . 'wp-admin/" title="' . __('Access admin panel', 'eLearning') . '"><i class="icon-settings-1"></i></a>';
                ?>
            </li>-->
        </ul> 
        <ul>
            <?php
            $loggedin_menu = array(
                'dashboard' => array(
                    'icon' => 'icon-book-open-1',
                    'label' => __('Dashboard', 'eLearning'),
                    'position' => 1,
                    'link' => bp_loggedin_user_domain() . 'account-dashboard'
                ),
                'my-profile' => array(
                    'icon' => 'icon-analytics-chart-graph',
                    'label' => __('My Profile', 'eLearning'),
                    'link' => bp_loggedin_user_domain()
                ),
                'account' => array(
                    'icon' => 'icon-settings',
                    'label' => __('Account', 'eLearning'),
                    'link' => bp_loggedin_user_domain() . BP_SETTINGS_SLUG
                ),
                'setting' => array(
                    'icon' => 'fa fa-sliders',
                    'label' => __('Settings', 'eLearning'),
                    'link' => bp_loggedin_user_domain() . 'setting'
                ),
                'logout' => array(
                    'icon' => 'icon-close-off-2',
                    'label' => __('Log out', 'eLearning'),
                    'link' => wp_logout_url()
                )
            );


//            $loggedin_menu['settings'] = array(
//                'icon' => 'icon-settings',
//                'label' => __('Settings', 'eLearning'),
//                'link' => bp_loggedin_user_domain() . BP_SETTINGS_SLUG
//            );
            //    $loggedin_menu = apply_filters('eLearning_logged_in_top_menu', $loggedin_menu);
            foreach ($loggedin_menu as $item) {
                echo '<li><a href="' . $item['link'] . '"><i class="' . $item['icon'] . '"></i>' . $item['label'] . '</a></li>';
            }
            ?>
        </ul>

        <?php do_action('bp_sidebar_me'); ?>
    </div>
    <?php
    do_action('bp_after_sidebar_me');

/* * *** If the user is not logged in, show the log form and account creation link **** */

else :
    if (!isset($user_login))
        $user_login = '';
    do_action('bp_before_sidebar_login_form');
    ?>


    <form name="login-form" id="vbp-login-form" class="standard-form" action="<?php echo apply_filters('eLearning_login_widget_action', site_url('wp-login.php', 'login_post')); ?>" method="post">
        <div class="inside_login_form">
            <label><?php _e('Username', 'eLearning'); ?><br />
                <input type="text" name="log" id="side-user-login" class="input" tabindex="1" value="<?php echo esc_attr(stripslashes($user_login)); ?>" /></label>

            <label><?php _e('Password', 'eLearning'); ?> <a href="<?php echo wp_lostpassword_url(); ?>" tabindex="5" class="tip vbpforgot" title="<?php _e('Forgot Password', 'eLearning'); ?>"><i class="icon-question"></i></a><br />
                <input type="password" tabindex="2" name="pwd" id="sidebar-user-pass" class="input" value="" /></label>

            <div class="checkbox small">
                <input type="checkbox" name="sidebar-rememberme" id="sidebar-rememberme" value="forever" /><label for="sidebar-rememberme"><?php _e('Remember Me', 'eLearning'); ?></label>
            </div>

            <?php do_action('bp_sidebar_login_form'); ?>

            <input type="submit" name="user-submit" id="sidebar-wp-submit" data-security="<?php echo wp_create_nonce('eLearning_signon'); ?>" value="<?php _e('Log In', 'eLearning'); ?>" tabindex="100" />
            <input type="hidden" name="user-cookie" value="1" />
            <?php
            $enable_signup = apply_filters('eLearning_enable_signup', 0);
            if ($enable_signup) :
                $registration_link = apply_filters('eLearning_buddypress_registration_link', site_url(BP_REGISTER_SLUG . '/'));
                printf('<a href="%s" class="vbpregister" title="' . __('Create an account', 'eLearning') . '" tabindex="5" >' . __('Sign Up', 'eLearning') . '</a> ', $registration_link);
            endif;
            do_action('login_form'); //BruteProtect FIX 
            ?>
        </div>    
    </form>

    <?php
    do_action('bp_after_sidebar_login_form');
  endif;
