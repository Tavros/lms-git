<?php

// COURSE STATUS : 
// 0 : NOT STARTED 
// 1: STARTED 
// 2 : SUBMITTED
// > 2 : EVALUATED

// VERSION 1.8.4 NEW COURSE STATUSES
// 1 : START COURSE
// 2 : CONTINUE COURSE
// 3 : FINISH COURSE : COURSE UNDER EVALUATION
// 4 : COURSE EVALUATED

if ( !defined( 'ABSPATH' ) ) exit;

/**
* eLearning_before_start_course hook.
*
* @hooked 
*/
do_action('eLearning_before_start_course');

get_header(eLearning_get_header());

/**
* eLearning_before_course_main_content hook.
*
* @hooked 
*/
do_action('eLearning_before_course_main_content');

?>
<section id="content">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-9">
            <?php

                if ( have_posts() ) : while ( have_posts() ) : the_post();
                /**
                * eLearning_unit_content hook.
                *
                * @hooked 
                */
                do_action('eLearning_unit_content');
                endwhile;
                endif;
                
                /**
                * eLearning_unit_controls hook.
                *
                * @hooked 
                */
                do_action('eLearning_unit_controls');
            ?>
            </div>
            <div class="col-md-3">
                <?php 
                    /**
                    * eLearning_course_action_points hook.
                    *
                    * @hooked 
                    */
                    do_action('eLearning_course_action_points');
                ?>
            </div>
        </div>
    </div>
</section>
<?php
    /**
    * eLearning_after_course_content hook.
    *
    * @hooked 
    */
    do_action('eLearning_after_start_course');
?>
<?php

get_footer(eLearning_get_footer());