<?php
$current_user_login = wp_get_current_user()->user_login;
//Get header style
$header_style = eLearning_get_customizer( 'header_style' );

//Check if header style transparent or generic show section title
if ( $header_style == 'transparent' || $header_style == 'generic' ) {
    echo '<section id="title"></section>';
}
?>

<section id="content"><!--Open content section-->
    <div id="buddypress"><!--Open buddypress div-->
        <div class="<?php echo eLearning_get_container(); ?>">
            <div class="row heigh_correct">
                <div class="col-md-2 col-sm-4 left_nav_menu"><!--Open left nav menu div-->
                    <?php do_action( 'bp_before_member_home_content' ); ?><!--Include home bp_before_member_home_content-->
                    <div class="pagetitle">
                        <div id="item-header" role="complementary">
                            <?php locate_template( array( 'members/single/member-header.php' ), true ); ?>

                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav" class="">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <!--div class="create_event hs-elearning">
                                <a href="<?php echo site_url().'/'.$current_user_login.'/';?>edit-event" class="btn btn-primary top-button">Create Event</a>
                            </div-->
                            <ul>
                                <?php getCustomBodypressMenu_child(); ?>
                                <?php do_action( 'bp_member_options_nav' ); ?>
                            </ul>
                        </div><!-- #object-nav -->
                    </div><!-- #item-nav -->
                </div><!--Close left nav menu div-->	
                <div class="col-md-10 col-sm-8">
                    <div class="padder">
