<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ELEARNING_Admin_Welcome {

	private $plugin;
	public $major_version='2.6';
	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		// Bail if user cannot moderate
		if ( ! current_user_can( 'manage_options' ) )
			return;
		add_action( 'admin_menu', array( $this, 'admin_menus') );
		add_action( 'admin_head', array( $this, 'admin_head' ) );
		add_action( 'admin_init', array( $this, 'welcome'    ) );
	}


	/**
	 * Add admin menus/screens
	 *
	 * @access public
	 * @return void
	 */
	public function admin_menus() {	

		$welcome_page_name  = __( 'Install ELEARNING', 'eLearning' );
		$welcome_page_title = __( 'Welcome to ELEARNING', 'eLearning' );
		if(!$this->check_installed()){
			$page = add_dashboard_page( $welcome_page_title, $welcome_page_name, 'manage_options', 'eLearning-install', array( $this, 'install_screen' ) );
			add_action( 'admin_print_styles-'. $page, array( $this, 'admin_css' ) );
		}else{
//			$about_page_name = __( 'About ELEARNING', 'eLearning' );
//			add_dashboard_page( $welcome_page_title, $about_page_name, 'manage_options', 'eLearning-about', array( $this, 'about_screen' ) );
		}
		if ( empty( $_GET['page'] ) ) {
			return;
		}

		$welcome_page_name ='';
	//	$welcome_page_name  = __( 'About ELEARNING', 'eLearning' );
		$welcome_page_title = __( 'Welcome to ELEARNING', 'eLearning' );
		switch ( $_GET['page'] ) {
			case 'eLearning-about' :
				$page = add_dashboard_page( $welcome_page_title, $welcome_page_name, 'manage_options', 'eLearning-about', array( $this, 'about_screen' ) );
				add_action( 'admin_print_styles-'. $page, array( $this, 'admin_css' ) );
			break;
			case 'eLearning-system' :
				$page = add_dashboard_page( $welcome_page_title, $welcome_page_name, 'manage_options', 'eLearning-system', array( $this, 'system_screen' ) );
				add_action( 'admin_print_styles-'. $page, array( $this, 'admin_css' ) );
			break;
			case 'eLearning-changelog' :
				$page = add_dashboard_page( $welcome_page_title, $welcome_page_name, 'manage_options', 'eLearning-changelog', array( $this, 'changelog_screen' ) );
				add_action( 'admin_print_styles-'. $page, array( $this, 'admin_css' ) );
			break;
		}
	}

	/**
	 * admin_css function.
	 *
	 * @access public
	 * @return void
	 */
	public function admin_css() {
		wp_enqueue_style( 'eLearning-activation', ELEARNING_URL.'/assets/css/old_files/activation.css');
	}

	/**
	 * Add styles just for this page, and remove dashboard page links.
	 *
	 * @access public
	 * @return void
	 */
	public function admin_head() {
		if(isset($_REQUEST['page']) && $_REQUEST['page'] == 'eLearning-about'){
			remove_submenu_page( 'index.php', 'eLearning-about' );		
		}
		
		remove_submenu_page( 'index.php', 'eLearning-system' );
		remove_submenu_page( 'index.php', 'eLearning-changelog' );

		?>
		<style type="text/css">
			/*<![CDATA[*/
			.eLearning-wrap .eLearning-badge {
				<?php echo is_rtl() ? 'left' : 'right'; ?>: 0;
			}
			.eLearning-wrap .feature-rest div {
				float:<?php echo is_rtl() ? 'right':'left' ; ?>;
			}
			.eLearning-wrap .feature-rest div.last-feature {
				padding-<?php echo is_rtl() ? 'right' : 'left'; ?>: 50px !important;
				padding-<?php echo is_rtl() ? 'left' : 'right'; ?>: 0;
			}
			.three-col > div{
				float:<?php echo is_rtl() ? 'right':'left' ; ?>;
			}
			/*]]>*/
		</style>
		<?php
	}

	/**
	 * Into text/links shown on all about pages.
	 *
	 * @access private
	 * @return void
	 */
	private function intro() {

		// Flush after upgrades
		if ( ! empty( $_GET['eLearning-updated'] ) || ! empty( $_GET['eLearning-installed'] ) )
			flush_rewrite_rules();
		?>
		<h1><?php printf( __( 'Welcome to ELEARNING %s', 'eLearning' ), $this->major_version ); ?></h1>

		<div class="about-text eLearning-about-text">
			<?php
				if ( ! empty( $_GET['eLearning-installed'] ) )
					$message = __( 'Thanks, all done!', 'eLearning' );
				elseif ( ! empty( $_GET['eLearning-updated'] ) )
					$message = __( 'Thank you for updating to the latest version!', 'eLearning' );
				else
					$message = __( 'Thanks for installing!', 'eLearning' );

				printf( __( '%s ELEARNING is the best Learning Management platform for WordPress. The latest version %s now contains following features.', 'eLearning' ), $message, $this->major_version );
			?>
		</div>

		<div class="eLearning-badge"><img src="<?php echo 'https://0.s3.envato.com/files/80339740/themeforest_thumbnail.png'; ?>" /></div>

		<p class="eLearning-actions">
			<a href="<?php echo admin_url('admin.php?page=eLearning_options'); ?>" class="button button-primary"><?php _e( 'Settings', 'eLearning' ); ?></a>
			<a href="<?php echo esc_url( 'http://eLearningthemes.com/documentation/eLearning/article-categories/tips-tricks/'); ?>" class="docs button"><?php _e( 'FAQs', 'eLearning' ); ?></a>
			<a href="<?php echo esc_url( 'http://eLearningthemes.com/envato/eLearning/documentation/'); ?>" class="docs button"><?php _e( 'Docs', 'eLearning' ); ?></a>
			<a href="<?php echo esc_url( 'http://eLearningthemes.com/documentation/eLearning/knowledge-base/'); ?>" class="button"><?php _e( 'Customization Tips', 'eLearning' ); ?></a>
			<a href="<?php echo esc_url( 'http://eLearningthemes.com/documentation/eLearning/'); ?>" class="button"><?php _e( 'Support system', 'eLearning' ); ?></a>
		</p>

		<h2 class="nav-tab-wrapper">
			<?php
				if(!$this->check_installed()){
					?>
					<a class="nav-tab <?php if ( $_GET['page'] == 'eLearning-install' ) echo 'nav-tab-active'; ?>" href="<?php echo esc_url( admin_url( add_query_arg( array( 'page' => 'eLearning-install' ), 'index.php' ) ) ); ?>">
						<?php _e( "Installation and Setup", 'eLearning' ); ?>
					</a>
					<?php
				}
			?>
			
			<a class="nav-tab <?php if ( $_GET['page'] == 'eLearning-about' ) echo 'nav-tab-active'; ?>" href="<?php echo esc_url( admin_url( add_query_arg( array( 'page' => 'eLearning-about' ), 'index.php' ) ) ); ?>">
				<?php _e( "What's New", 'eLearning' ); ?>
			</a>
			<a class="nav-tab <?php if ( $_GET['page'] == 'eLearning-system' ) echo 'nav-tab-active'; ?>" href="<?php echo esc_url( admin_url( add_query_arg( array( 'page' => 'eLearning-system' ), 'index.php' ) ) ); ?>">
			<?php
			 _e( 'System Status', 'eLearning' ); 
				?>
			</a><a class="nav-tab <?php if ( $_GET['page'] == 'eLearning-changelog' ) echo 'nav-tab-active'; ?>" href="<?php echo esc_url( admin_url( add_query_arg( array( 'page' => 'eLearning-changelog' ), 'index.php' ) ) ); ?>">
				<?php _e( 'Changelog', 'eLearning' ); ?>
			</a>
		</h2>
		<?php
	}

	/**
	 * Output the install screen.
	 */
	public function install_screen() {
		?>
		<div class="wrap eLearning-wrap about-wrap">

			<?php $this->intro(); ?>

			<div class="changelog">
				<div class="eLearning-feature feature-rest feature-section col two-col">
					<div class="col-1">
						<h4><?php _e( 'One Click installation and Setup', 'eLearning' ); ?></h4>
						<p><?php _e( 'You can now install ELEARNING in one single click, select the setup procedure from the options given in the right. You just need to Click the button once and everything would be automatically installed and setup, after which your ELEARNING Site would be ready to use and configure.', 'eLearning' ); ?></p>
					</div>
					<div class="col-2"><?php
						if ( in_array( 'wordpress-importer/wordpress-importer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) || (function_exists('is_plugin_active_for_network') && is_plugin_active_for_network( 'wordpress-importer/wordpress-importer.php'))) {
							echo '<p style="padding:15px;background-color: #FFF6BF;border: 1px solid #ffd324;border-radius: 2px;color: #817134;">'.__('Please deactivate WordPress importer plugin to avoid conflicts with ELearning one click installer.','eLearning').'</p>';
						}else{
							if (is_plugin_active('buddypress/bp-loader.php') && is_plugin_active('eLearning-course-module/loader.php') && is_plugin_active('customtypes/eLearning-customtypes.php')) { 
						?>
						<strong class="install_buttons">
							<a class="button button-primary button-hero" href="<?php echo admin_url( 'themes.php?page=eLearning-setup' ); ?>" ><?php _e('Theme Setup Wizard','eLearning'); ?></a>
							<span><?php _e('OR','eLearning'); ?> </span>
							<a class="button button-primary button-hero sample_data_install" data-file="theme_data"><?php _e('Setup Theme without Sample Data','eLearning'); ?></a>
							<span><?php _e('OR','eLearning'); ?> </span>
							<?php
								$disabled=0;
								$plugin_flag=1;
								if(is_plugin_active('LayerSlider/layerslider.php') && is_plugin_active('eLearning-assignments/eLearning-assignments.php') && is_plugin_active('eLearning-front-end/eLearning-front-end.php') && is_plugin_active('woocommerce/woocommerce.php')){
									$plugin_flag=0;
								}
								$plugin_flag=apply_filters('eLearning_setup_plugins',$plugin_flag);
								if (!$plugin_flag) { 
									?>
									<a class="button button-primary button-hero sample_data_install <?php echo (($disabled)?'disabled':''); ?>" data-file="sampledata"><?php _e('Setup Theme with Sample Data','eLearning'); ?><?php echo (($disabled)?'<span>'.__('Please enable all the plugins','eLearning').'</span>':''); ?></a>
								<?php }else{
									echo '<p style="padding:15px;background-color: #FFF6BF;border: 1px solid #ffd324;border-radius: 2px;color: #817134;display: inline-block;">'.sprintf(__('Install all plugins for Full Sample Data import % slink %s','eLearning'),'<a href="'.admin_url('themes.php?page=install-required-plugins').'">','</a>').'</p>';
								 }
							?>
						</strong>
						<?php
						}else{
							echo '<p style="padding:15px;background-color: #FFF6BF;border: 1px solid #ffd324;border-radius: 2px;color: #817134;">'.__('Please activate all/required plugins bundled with the theme.','eLearning').'</p><a href="'.admin_url('themes.php?page=install-required-plugins').'" class="button button-primary">'.__('Install & Activate plugins','eLearning').'</a>';
						}
						echo '<span id="loading"><i class="sphere"></i></span>';
					}
					?>
					</div>
				</div>
			</div>
			<div class="changelog about-integrations">
				<h3><?php _e( 'Video Tutorial of Installing theme', 'eLearning' ); ?></h3>

				<iframe width="100%" height="480" src="http://www.youtube.com/embed/<?php echo apply_filters('eLearning_one_click_setup_video','HURLXtbfKVY');?>" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="changelog">
				<div class="feature-section col three-col">
					<div>
						<h4><?php _e( 'Setup Issues', 'eLearning' ); ?></h4>
						<p><?php _e( 'Facing issues while setting up? Try out suggested links below.', 'eLearning' ); ?></p>
						<p><a href="http://eLearningthemes.com/envato/eLearning/documentation/quick-installation-guide.html#pre-setup"><?php _e( 'ELEARNING Pre-Setup settings', 'eLearning' ); ?></a><br />
						<a href="http://eLearningthemes.com/envato/eLearning/documentation/quick-installation-guide.html"><?php _e( 'ELEARNING manual installation & setup', 'eLearning' ); ?></a><br />
						<a href="<?php echo admin_url( 'index.php?page=eLearning-system' ); ?>">
						<?php _e( 'ELEARNING System Status', 'eLearning' ); ?>
						</a><br /> 
						<a href="http://eLearningthemes.com/envato/eLearning/documentation/quick-installation-guide.html#setup-issues"><?php _e( 'ELEARNING FAQs', 'eLearning' ); ?></a></p>
					</div>
					<div>
						<h4><?php _e( 'Popular Setup issues', 'eLearning' ); ?></h4>
						<p><?php _e( 'Following is the list of most frequent setup issues faced by users', 'eLearning' ); ?></p>
						<p><a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/server-500-error-blank-white-page-after-activating-the-theme-or-one-of-its-plugins/"><?php _e( 'White screen when installed', 'eLearning' ); ?></a><br />
						<a href="http://eLearningthemes.com/documentation/eLearning/article-categories/faqs/"><?php _e( 'Course pages not opening', 'eLearning' ); ?></a><br />
						<a href="http://eLearningthemes.com/documentation/eLearning/article-categories/faqs/"><?php _e( 'Getting 404 pages', 'eLearning' ); ?></a><br />
						<a href="http://eLearningthemes.com/documentation/eLearning/article-categories/faqs/"><?php _e( 'Start Course/Take this Course not working', 'eLearning' ); ?></a><br />
						</p>
					</div>
					<div class="last-feature">
						<h4><?php _e( 'Other popular issues', 'eLearning' ); ?></h4>
						<p><?php _e( 'Following is the list of most frequent issues faced by users', 'eLearning' ); ?></p>
						<p><a href="http://eLearningthemes.com/documentation/eLearning/article-categories/faqs/"><?php _e( 'Quizzes not saving answers', 'eLearning' ); ?></a><br />
						<a href="http://eLearningthemes.com/documentation/eLearning/article-categories/faqs/"><?php _e( 'Visual composer not updating', 'eLearning' ); ?></a><br />
						<a href="http://eLearningthemes.com/documentation/eLearning/article-categories/faqs/"><?php _e( 'Getting 404 page on editing course', 'eLearning' ); ?></a><br />
						<a href="http://eLearningthemes.com/documentation/eLearning/article-categories/faqs/"><?php _e( 'Theme customizer not working', 'eLearning' ); ?></a><br />
						</p>
					</div>
				</div>
			</div>
			<div class="return-to-dashboard">
				<a href="http://eLearningthemes.com/documentation/eLearning/forums/"><?php _e( 'Unable to setup ? Get help on our Support forums.', 'eLearning' ); ?></a>
			</div>
		</div>
		<?php
	}
	/**
	 * Output the about screen.
	 */
	public function about_screen() {
		?>
		<div class="wrap eLearning-wrap about-wrap">

			<?php $this->intro(); ?>

			<div class="changelog">
				<div class="eLearning-feature feature-rest feature-section col two-col">
					<div class="col-1">
						<h4><?php _e( 'Bug fixes and maintenance', 'eLearning' ); ?></h4>
						<p><?php _e( 'We\'ve added 3 major features.Course tabs style layout,profile menu drop down settings,emails to incative users and ', 'eLearning' ); ?></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/2-6/" class="button"><?php _e( 'Check update log ', 'eLearning' ); ?></a>
					</div>
					<div class="col-2">
						<h4><?php _e( 'Course Tabs style layout', 'eLearning' ); ?></h4>
						<p><?php _e( 'We\'ve added course tabs style layout in 2.6.Enabling this feature will change the behaviour of course menu.Check tutorial for more.', 'eLearning' ); ?></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/2-6/" class="button"><?php _e( 'Check update log ', 'eLearning' ); ?></a>
					</div>
				</div>
			</div>
			<div class="changelog about-integrations">
				<h3><?php _e( 'What\'s New in ELEARNING', 'eLearning' ); ?><span style="float:right;"><a href="https://www.youtube.com/playlist?list=PL8n4TGA_rwD_5jqsgXIxXOk1H6ar-SVCV" class="button button-primary" target="_blank"><?php _e('ELEARNING Video Playlist','eLearning'); ?></a></span></h3>
				
				<div class="eLearning-feature feature-section col three-col">
					<div>
						<h4><?php _e( 'Course Tabs style layout', 'eLearning' ); ?></h4>
						<p><img src="https://i.gyazo.com/7aafa00a80885b047cc9c27c0f763533.gif" /></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/tabs-style-in-course-menu-with-custom-sections" target="_blank" class="button">Tutorial</a>
					</div>
					<div>
						<h4><?php _e( 'Email to in-active users', 'eLearning' ); ?></h4>
						<p><img src="http://image.prntscr.com/image/d412c4358d0b427da8b3e9192aa5e188.png" /></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/schedule-email-for-inactive-users/" target="_blank" class="button">Tutorial</a>
					</div>
					<div class="last-feature">
						<h4><?php _e( 'Custom profile logged in menu.', 'eLearning' ); ?></h4>
						<p><img src="https://i.gyazo.com/1a8c48ab135077e10bf9658ef32d2bad.gif" /></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/customise-profile-menu-dropdown-eLearning-2-6/" target="_blank" class="button">Tutorial</a>
					</div>
				</div>
			</div>
			<div class="changelog">
				<div class="feature-section col three-col">
					<div>
						<h4><?php _e( 'Migrate From Sensei to ELEARNING', 'eLearning' ); ?></h4>
						<p><?php _e( 'Migrate all courses, units, sections, quizzes and content from WooThemes Sensei to ELEARNING in 1 single click.', 'eLearning' ); ?></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/woothemes-sensei-to-eLearning-migration/" class="button">Tutorial</a>
					</div>
					<div>
						<h4><?php _e( 'Migration from LearnDash to ELEARNING', 'eLearning' ); ?></h4>
						<p><?php _e( 'Migrate all courses, units, sections, quizzes and content from LearnDash to ELEARNING in 1 single click', 'eLearning' ); ?></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/migrate-from-learndash-to-eLearning/" class="button">Tutorial</a>
					</div>
					<div class="last-feature">
						<h4><?php _e( 'Migration from CleverCourse to ELEARNING', 'eLearning' ); ?></h4>
						<p><?php _e( 'Migrate all courses, units, sections, quizzes and content from Clever course/Goodlayers LMS to ELEARNING in 1 single click.', 'eLearning' ); ?></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/clevercourse-good-lms-migration-to-eLearning" class="button">Tutorial</a>
					</div>
				</div>
				<div class="feature-section col three-col">
					<div>
						<h4><?php _e( 'Migrate From WP Courseware to ELEARNING', 'eLearning' ); ?></h4>
						<p><?php _e( 'Migrate all courses, units, sections, quizzes and content from WP Courseware to ELEARNING in 1 single click.', 'eLearning' ); ?></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/wp-courseware-eLearning-migrate-plugin/" class="button">Tutorial</a>
					</div>
					<div>
						<h4><?php _e( 'Updated Documentation ', 'eLearning' ); ?></h4>
						<p><?php _e( 'Documentation has been updated. We\'ve added new functions & shortcodes list and developer documentation. For most updated documentation we recommend checking the online documentation doc.', 'eLearning' ); ?></p>
						<a href="http://eLearningthemes.com/envato/eLearning/documentation/"><?php _e( 'Check documentation','eLearning'); ?></a>
					</div>
					<div class="last-feature">
						<h4><?php _e( 'Translation Collaboration', 'eLearning' ); ?></h4>
						<p><?php _e( 'It is really difficult to maintain translations in a versatible project as ELEARNING. Therefore we ask our users to email us translation files at eLearningthemes@gmail.com with subject "ELEARNING - Translation Files".', 'eLearning' ); ?></p>
						<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/2-5/"><?php _e( 'Check Translation files status','eLearning'); ?></a>
					</div>
				</div>
			</div>
			<div class="return-to-dashboard">
				<a href="<?php echo esc_url( admin_url( add_query_arg( array( 'page' => 'eLearning_options' ), 'admin.php' ) ) ); ?>"><?php _e( 'Go to ELEARNING Options panel', 'eLearning' ); ?></a>
			</div>
		</div>
		<?php
	}

	/**
	 * Output the system.
	 */
	public function system_screen() {
		?>
		<div class="wrap eLearning-wrap about-wrap">

			<?php $this->intro(); ?>
			<table class="eLearning_status_table widefat" cellspacing="0" id="status">
				<thead>
					<tr>
						<th colspan="2"><h4><?php _e( 'Environment', 'eLearning' ); ?></h4></th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td><?php _e( 'Home URL', 'eLearning' ); ?>:</td>
						<td><?php echo home_url(); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'Site URL', 'eLearning' ); ?>:</td>
						<td><?php echo site_url(); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Version', 'eLearning' ); ?>:</td>
						<td><?php bloginfo('version'); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Multisite Enabled', 'eLearning' ); ?>:</td>
						<td><?php if ( is_multisite() ) echo __( 'Yes', 'eLearning' ); else echo __( 'No', 'eLearning' ); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'Web Server Info', 'eLearning' ); ?>:</td>
						<td><?php echo esc_html( $_SERVER['SERVER_SOFTWARE'] ); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'PHP Version', 'eLearning' ); ?>:</td>
						<td><?php if ( function_exists( 'phpversion' ) ) echo esc_html( phpversion() ); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'MySQL Version', 'eLearning' ); ?>:</td>
						<td>
							<?php
							/** @global wpdb $wpdb */
							global $wpdb;
							echo $wpdb->db_version();
							?>
						</td>
					</tr>
					<tr>
						<td><?php _e( 'WP Active Plugins', 'eLearning' ); ?>:</td>
						<td><?php echo count( (array) get_option( 'active_plugins' ) ); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Memory Limit', 'eLearning' ); ?>:</td>
						<td><?php
							$memory = $this->eLearning_let_to_num( WP_MEMORY_LIMIT );
							if ( $memory < 134217728 ) {
								echo '<mark class="error">' . sprintf( __( '%s - We recommend setting memory to at least 128MB. See: <a href="%s">Increasing memory allocated to PHP</a>', 'eLearning' ), size_format( $memory ), 'http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP' ) . '</mark>';
							} else {
								echo '<mark class="yes">' . size_format( $memory ) . '</mark>';
							}
						?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Debug Mode', 'eLearning' ); ?>:</td>
						<td><?php if ( defined('WP_DEBUG') && WP_DEBUG ) echo '<mark class="yes">' . __( 'Yes', 'eLearning' ) . '</mark>'; else echo '<mark class="no">' . __( 'No', 'eLearning' ) . '</mark>'; ?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Language', 'eLearning' ); ?>:</td>
						<td><?php echo get_locale(); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Max Upload Size', 'eLearning' ); ?>:</td>
						<td><?php echo size_format( wp_max_upload_size() ); ?></td>
					</tr>
					<?php if ( function_exists( 'ini_get' ) ) : ?>
						<tr>
							<td><?php _e('PHP Post Max Size', 'eLearning' ); ?>:</td>
							<td><?php echo size_format($this->eLearning_let_to_num( ini_get('post_max_size') ) ); ?></td>
						</tr>
						<tr>
							<td><?php _e('PHP Time Limit', 'eLearning' ); ?>:</td>
							<td><?php echo ini_get('max_execution_time'); ?></td>
						</tr>
						<tr>
							<td><?php _e( 'PHP Max Input Vars', 'eLearning' ); ?>:</td>
							<td><?php echo ini_get('max_input_vars'); ?></td>
						</tr>
						<tr>
							<td><?php _e( 'SUHOSIN Installed', 'eLearning' ); ?>:</td>
							<td><?php echo extension_loaded( 'suhosin' ) ? __( 'Yes', 'eLearning' ) : __( 'No', 'eLearning' ); ?></td>
						</tr>
					<?php endif; ?>
					<tr>
						<td><?php _e( 'Default Timezone', 'eLearning' ); ?>:</td>
						<td><?php
							$default_timezone = date_default_timezone_get();
							if ( 'UTC' !== $default_timezone ) {
								echo '<mark class="error">' . sprintf( __( 'Default timezone is %s - it should be UTC', 'eLearning' ), $default_timezone ) . '</mark>';
							} else {
								echo '<mark class="yes">' . sprintf( __( 'Default timezone is %s', 'eLearning' ), $default_timezone ) . '</mark>';
							} ?>
						</td>
					</tr>
				</tbody>


				<thead>
					<tr>
						<th colspan="2"><h4><?php _e( 'Settings', 'eLearning' ); ?></h4></th>
					</tr>
				</thead>

				<thead>
					<tr>
						<th colspan="2"><?php _e( 'ELEARNING Pages', 'eLearning' ); ?></th>
					</tr>
				</thead>

				<tbody>
					<?php
						$check_pages = array(
							_x( 'All Course page', 'Page setting', 'eLearning' ) => array(
									'option' => 'bp-pages.course'
								),
							_x( 'Take this course page', 'Page setting', 'eLearning' ) => array(
									'option' => 'take_course_page',
									'shortcode' => '[' . apply_filters( 'eLearning_cart_shortcode_tag', 'eLearning_cart' ) . ']'
								),
							_x( 'Create Course Page', 'Page setting', 'eLearning' ) => array(
									'option' => 'create_course',
								),
							_x( 'Notes & Discussion Page', 'Page setting', 'eLearning' ) => array(
									'option' => 'unit_comments',
								),
							_x( 'Default Certificate Page', 'Page setting', 'eLearning' ) => array(
									'option' => 'certificate_page',
								)
						);

						$alt = 1;

						foreach ( $check_pages as $page_name => $values ) {

							if ( $alt == 1 ) echo '<tr>'; else echo '<tr>';

							echo '<td>' . esc_html( $page_name ) . ':</td><td>';

							$error = false;

							switch($values['option']){
								case 'bp-pages.course':
									$pages=get_option('bp-pages');
									if(isset($pages) && is_array($pages) && isset($pages['course']))
										$page_id=$pages['course'];
								break;
								default:
									$page_id = eLearning_get_option($values['option']);
								break;
							}
							// Page ID check
							if ( ! isset($page_id ) ){
								echo '<mark class="error">' . __( 'Page not set', 'eLearning' ) . '</mark>';
								$error = true;
							} else {
								$error = false;
							}

							if ( ! $error ) echo '<mark class="yes">#' . absint( $page_id ) . ' - ' . str_replace( home_url(), '', get_permalink( $page_id ) ) . '</mark>';

							echo '</td></tr>';

							$alt = $alt * -1;
						}
					?>
				</tbody>

				<thead>
					<tr>
						<th colspan="2"><h4><?php _e( 'Templates', 'eLearning' ); ?></h4></th>
					</tr>
				</thead>

				<tbody>
					<?php

						$template_paths = apply_filters( 'bp_course_load_template_filter', array( 'eLearning' => BP_COURSE_MOD_PLUGIN_DIR . '/includes/templates/' ) );
						$scanned_files  = array();
						$found_files    = array();

						
						foreach ( $template_paths as $plugin_name => $template_path ) {
							$scanned_files[ $plugin_name ] = $this->scan_template_files( $template_path );
						}

						

						foreach ( $scanned_files as $plugin_name => $files ) {
							foreach ( $files as $file ) {
								if ( file_exists( get_stylesheet_directory() . '/' . $file ) ) {
									$theme_file = get_stylesheet_directory() . '/' . $file;
								} elseif ( file_exists( get_stylesheet_directory() . '/eLearning/' . $file ) ) {
									$theme_file = get_stylesheet_directory() . '/eLearning/' . $file;
								} elseif ( file_exists( get_template_directory() . '/' . $file ) ) {
									$theme_file = get_template_directory() . '/' . $file;
								} elseif( file_exists( get_template_directory() . '/eLearning/' . $file ) ) {
									$theme_file = get_template_directory() . '/eLearning/' . $file;
								} else {
									$theme_file = false;
								}

								if ( $theme_file ) {
									$core_version  = $this->get_file_version( BP_COURSE_MOD_PLUGIN_DIR . '/includes/templates/' . $file );
									$theme_version = $this->get_file_version( $theme_file );

									if ( $core_version && ( empty( $theme_version ) || version_compare( $theme_version, $core_version, '<' ) ) ) {
										$found_files[ $plugin_name ][] = sprintf( __( '<code>%s</code> version <strong style="color:red">%s</strong> is out of date. The core version is %s', 'eLearning' ), str_replace( WP_CONTENT_DIR . '/themes/', '', $theme_file ), $theme_version ? $theme_version : '-', $core_version );
									} else {
										$found_files[ $plugin_name ][] = sprintf( '<code>%s</code>'.$core_version.' - '.$theme_version, str_replace( WP_CONTENT_DIR . '/themes/', '', $theme_file ) );
									}
								}
							}
						}

						if ( $found_files ) {
							foreach ( $found_files as $plugin_name => $found_plugin_files ) {
								?>
								<tr>
									<td><?php _e( 'Template Overrides', 'eLearning' ); ?> (<?php echo $plugin_name; ?>):</td>
									<td><?php echo implode( ', <br/>', $found_plugin_files ); ?></td>
								</tr>
								<?php
							}
						} else {
							?>
							<tr>
								<td><?php _e( 'Template Overrides', 'eLearning' ); ?>:</td>
								<td><?php _e( 'No overrides present in theme.', 'eLearning' ); ?></td>
							</tr>
							<?php
						}
					?>
				</tbody>

			</table>
		</div>
		<?php
	}

	/**
	 * Output the changelog screen
	 */
	public function changelog_screen() {
		?>
		<div class="wrap eLearning-wrap about-wrap">

			<?php $this->intro(); ?>
			<div class="changelog-description">
			<p><?php printf( __( 'Full Changelog of ELEARNING Theme', 'eLearning' ), 'eLearning' ); ?></p>

			<?php
				$file = ELEARNING_PATH.'/changelog.txt';
				$myfile = fopen($file, "r") or die("Unable to open file!".$file);
				while(!feof($myfile)) {
					$string = fgets($myfile);
					if(strpos($string, '* version') === 0){
						echo '<br />---------------------- * * * ----------------------<br /><br />';
					}
				  echo $string . "<br>";
				}
				fclose($myfile);
			?>
			</div>
		</div>
		<?php
	}

	function scan_template_files( $template_path ) {
		
		$files         = scandir( $template_path );
		$result        = array();
		if ( $files ) {
			foreach ( $files as $key => $value ) {
				if ( ! in_array( $value, array( ".",".." ) ) ) {
					if ( is_dir( $template_path . DIRECTORY_SEPARATOR . $value ) ) {
						$sub_files = self::scan_template_files( $template_path . DIRECTORY_SEPARATOR . $value );
						foreach ( $sub_files as $sub_file ) {
							$result[] = $value . DIRECTORY_SEPARATOR . $sub_file;
						}
					} else {
						$result[] = $value;
					}
				}
			}
		}
		return $result;
	}
	function get_file_version( $file ) {
		// We don't need to write to the file, so just open for reading.
		$fp = fopen( $file, 'r' );

		// Pull only the first 8kiB of the file in.
		$file_data = fread( $fp, 8192 );

		// PHP will close file handle, but we are good citizens.
		fclose( $fp );

		// Make sure we catch CR-only line endings.
		$file_data = str_replace( "\r", "\n", $file_data );
		$version   = '';

		if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( '@version', '/' ) . '(.*)$/mi', $file_data, $match ) && $match[1] )
			$version = _cleanup_header_comment( $match[1] );

		return $version ;
	}
	/**
	 * Sends user to the welcome page on first activation
	 */
	public function welcome() {
		// Bail if no activation redirect transient is set
	    if ( ! get_transient( '_eLearning_activation_redirect' ) ) {
			return;
	    }

		// Delete the redirect transient
		delete_transient( '_eLearning_activation_redirect' );
		// Bail if activating from network, or bulk, or within an iFrame
		if ( is_network_admin() || defined( 'IFRAME_REQUEST' ) ) {
			return;
		}

		if(!$this->check_installed()){
			wp_redirect( admin_url( 'themes.php?page=eLearning-setup' ) );
		}else{
			
			wp_redirect( admin_url( 'index.php?page=eLearning-about' ) );
				
		}
		exit;
	}
	function eLearning_let_to_num( $size ) {
		$l   = substr( $size, -1 );
		$ret = substr( $size, 0, -1 );
		switch ( strtoupper( $l ) ) {
			case 'P':
				$ret *= 1024;
			case 'T':
				$ret *= 1024;
			case 'G':
				$ret *= 1024;
			case 'M':
				$ret *= 1024;
			case 'K':
				$ret *= 1024;
		}
		return $ret;
	}
	function check_installed(){
		$check_options_panel = get_option(THEME_SHORT_NAME);
		if(!isset($check_options_panel) || empty($check_options_panel))
			return false;

		$take_course_page = eLearning_get_option('take_course_page');
		if(!isset($take_course_page) || !is_numeric($take_course_page) || empty($take_course_page))
			return false;

		return true;
	}
}




add_action('init','eLearning_welcome_user');
function eLearning_welcome_user(){
	new ELEARNING_Admin_Welcome();	
}

