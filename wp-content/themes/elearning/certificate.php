<?php
/**
 * Template Name: Certificate
 */
if ( ! defined( 'ABSPATH' ) ) exit;
$code = $_REQUEST['certificate_code'];

if(isset($code)){

  $user_id=apply_filters('eLearning_certificate_code_user_id',$code);
  $course_id=apply_filters('eLearning_certificate_code_course_id',$code);
  $template =apply_filters('eLearning_certificate_code_template_id',$code);

  if(strlen($code)<2 || empty($user_id) || empty($course_id) || empty($template)){
    wp_die(__('INVALID CERTIFICATE CODE','eLearning'));
  }
  if($template !== get_the_ID()){
    $location=get_permalink($template).'?c='.$course_id.'&u='.$user_id;
    wp_redirect($location);
    exit();
  }
}else{
  $user_id=$_REQUEST['u'];
  $course_id=$_REQUEST['c'];  
}

do_action('eLearning_validate_certificate',$user_id,$course_id);

get_header(eLearning_get_header());
if ( have_posts() ) : while ( have_posts() ) : the_post();

$certificate_class= apply_filters('eLearning_certificate_class','');
do_action('eLearning_certificate_before_full_content');

?>
<section id="certificate" <?php echo (empty($certificate_class)?'':'class="'.$certificate_class.'"'); ?>>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php do_action('eLearning_certificate_before_content'); ?>
                <div class="extra_buttons">
                    <?php do_action('eLearning_certificate_extra_buttons');
                        echo '<a href="#" class="certificate_close"><i class="fa fa-times"></i></a>';
                        echo '<a href="#" class="certificate_print"><i class="fa fa-print"></i></a>';
                        echo '<a href="#" class="certificate_pdf"><i class="fa fa-file-pdf-o"></i></a>';
                        echo '<a href="#" class="certificate_download"><i class="fa fa-download"></i></a>';
                    ?>    
                </div>
                <div class="certificate certificate_content" data-width="800" data-height="640">
                  <br /><br />
                   	<h1><?php echo apply_filters('eLearning_certificate_heading',__('CERTIFICATE OF COMPLETION','eLearning')); ?></h1>

                   	<h6><?php echo apply_filters('eLearning_certificate_sub_heading',__('Awarded to','eLearning')); ?></h6>

                    <?php do_action('eLearning_certificate_before_name'); ?>

                   	<h2><?php echo bp_core_get_user_displayname($user_id); ?> 

                    <?php do_action('eLearning_certificate_after_name'); ?>

                   	<span><?php echo xprofile_get_field_data( 'Location', $user_id ); ?></span></h2>

                   	<span><?php echo apply_filters('eLearning_certificate_before_course_title',__('for successful completion of course','eLearning')); ?></span>
                   	<h3><?php echo get_the_title($course_id); ?></h3>
                    <?php do_action('eLearning_certificate_after_content'); ?>
                    <br /><br />
                </div>
            </div>
        </div>
    </div>
</section>
<?php

do_action('eLearning_certificate_after_full_content');

endwhile;
endif;

get_footer(eLearning_get_footer());
?>