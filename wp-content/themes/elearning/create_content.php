<?php
/**
 * Template Name: Create Content
 */
if ( ! defined( 'ABSPATH' ) ) exit;
do_action('eLearning_before_create_course_header');

get_header(eLearning_get_header());

do_action('eLearning_before_create_course_page');

?>
<section id="title"></section>
<section id="content">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="<?php echo $v_add_content;?> content">
                <?php
                    echo do_shortcode('[edit_course]');
                 ?>
            </div>
        </div>
    </div>
</section>
<?php

do_action('eLearning_after_create_course_page');

get_footer(eLearning_get_footer());
