<?php
//Header File

if (!defined('ABSPATH'))
    exit;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <?php
        wp_head();
        ?>
    </head>
    <body <?php body_class(); ?>>
        <div id="global" class="global">
            <div class="pagesidebar">
                <div class="sidebarcontent">
                    <h2 id="sidelogo">
                        <a href="<?php echo eLearning_site_url('', 'sidelogo'); ?>"><img src="<?php echo apply_filters('eLearning_logo_url', ELEARNING_URL . '/assets/images/logo.png', 'pagesidebar'); ?>" alt="<?php echo get_bloginfo('name'); ?>" /></a>
                    </h2>
                    <?php
                    $args = apply_filters('eLearning-mobile-menu', array(
                        'theme_location' => 'mobile-menu',
                        'container' => '',
                        'menu_class' => 'sidemenu',
                        'items_wrap' => '<div class="mobile_icons"><a id="mobile_searchicon"><i class="fa fa-search"></i></a>' . ( (function_exists('WC')) ? '<a href="' . WC()->cart->get_cart_url() . '"><span class="fa fa-shopping-basket"><em>' . WC()->cart->cart_contents_count . '</em></span></a>' : '') . '</div><ul id="%1$s" class="%2$s">%3$s</ul>',
                        'fallback_cb' => 'eLearning_set_menu',
                    ));

                    wp_nav_menu($args);
                    ?>
                </div>
                <a class="sidebarclose"><span></span></a>
            </div>
            <div class="pusher">
                <?php
                $fix = eLearning_get_option('header_fix');
                ?>
                <div id="headertop" class="<?php
                if (isset($fix) && $fix) {
                    echo 'fix';
                }
                ?>">
                    <div class="<?php echo eLearning_get_container(); ?>">
                        <div class="row">
                            <div class="col-md-4 col-sm-3 col-xs-4">
                                <a href="<?php echo eLearning_site_url('', 'logo'); ?>" class="homeicon"><img src="<?php echo apply_filters('eLearning_logo_url', ELEARNING_URL . '/assets/images/logo.png', 'headertop'); ?>" width="100" height="48" alt="<?php echo get_bloginfo('name'); ?>" /></a>
                            </div>
                            <div class="col-md-8 col-sm-9 col-xs-8">
                                <?php
                                if (function_exists('bp_loggedin_user_link') && is_user_logged_in()) :
                                    ?>
                                    <ul class="topmenu">
                                        <li class="faIcon">
                                            <a href="#">
                                                <i class="fa fa-globe" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li class="faIcon">
                                            <a href="#">
                                                <i class="fa fa-comments" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li class="faIcon">
                                            <a href="#">
                                                <i class="fa fa-signal" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li class="menu-bars">
                                            <a href="<?php bp_loggedin_user_link(); ?>" class="smalname vbplogin">
                                                <?php
                                                $user_info = wp_get_current_user();
                                                $roles = wp_get_current_user()->roles;
                                                echo $roles[0];
                                                ?>
                                            </a>
                                            <a href="<?php bp_loggedin_user_link(); ?>" class="smallimg vbplogin">
                                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                            </a>
                                            <a href="<?php bp_loggedin_user_link(); ?>" class="smallimg vbplogin">
                                                <?php bp_loggedin_user_avatar('type=full'); ?>
                                            </a>
                                        </li>
                                        <?php do_action('eLearning_header_top_login'); ?>
                                    </ul>
                                    <?php
                                else :
                                    ?>
                                    <ul class="topmenu">
                                        <li><a href="#login" class="smallimg vbplogin"><?php _e('Login', 'eLearning'); ?></a></li>
                                        <li><?php
                                            $enable_signup = apply_filters('eLearning_enable_signup', 0);
                                            if ($enable_signup) :
                                                $registration_link = apply_filters('eLearning_buddypress_registration_link', site_url(BP_REGISTER_SLUG . '/'));
                                                printf(__('<a href="%s" class="vbpregister" title="' . __('Create an account', 'eLearning') . '">' . __('Sign Up', 'eLearning') . '</a> ', 'eLearning'), $registration_link);
                                            endif;
                                            ?>
                                        </li>
                                    </ul>
                                <?php
                                endif;
                                $args = apply_filters('eLearning-top-menu', array(
                                    'theme_location' => 'top-menu',
                                    'container' => '',
                                    'menu_class' => 'topmenu',
                                    'fallback_cb' => 'eLearning_set_menu',
                                ));

                                wp_nav_menu($args);
                                ?>
                            </div>
                            <?php
                            $style = eLearning_get_login_style();
                            if (empty($style)) {
                                $style = 'default_login';
                            }
                            ?>
                            <div id="eLearning_bp_login" class="<?php echo $style; ?>">
                                <?php
                                eLearning_include_template("login/$style.php");
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <header>
                    <div class="<?php echo eLearning_get_container(); ?>" style="display:none;">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-4">
                                <?php
                                if (is_front_page()) {
                                    echo '<h1 id="logo">';
                                } else {
                                    echo '<h2 id="logo">';
                                }
                                ?>
                                <a href="<?php echo eLearning_site_url(); ?>"><img src="<?php echo apply_filters('eLearning_logo_url', ELEARNING_URL . '/assets/images/logo.png', 'header'); ?>" width="100" height="48" alt="<?php echo get_bloginfo('name'); ?>" /></a>
                                <?php
                                if (is_front_page()) {
                                    echo '</h1>';
                                } else {
                                    echo '</h2>';
                                }
                                ?>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-8">
                                <?php
                                $args = apply_filters('eLearning-main-menu', array(
                                    'theme_location' => 'main-menu',
                                    'container' => 'nav',
                                    'menu_class' => 'menu',
                                    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s<li><a id="new_searchicon"><i class="fa fa-search"></i></a></li></ul>',
                                    'walker' => new eLearning_walker,
                                    'fallback_cb' => 'eLearning_set_menu'
                                ));
                                wp_nav_menu($args);
                                ?>
                                <a id="trigger">
                                    <span class="lines"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </header>
