<?php 
/**
 * The template for displaying course directory.
 *
 * Override this template by copying it to yourtheme/course/index.php
 *
 * @author 		ELearningThemes
 * @package 	eLearning-course-module/templates
 * @version     2.1
 */
 
if ( !defined( 'ABSPATH' ) ) exit;
do_action('eLearning_before_course_directory');

get_header( eLearning_get_header() ); 

$directory_layout = eLearning_get_customizer('directory_layout');

eLearning_include_template("directory/course/index$directory_layout.php");  

do_action('eLearning_after_course_directory');

get_footer( eLearning_get_footer() );  