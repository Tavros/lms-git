/*
JS Snippets for Child theme
*/


/*Start Ready Function*/
jQuery(document).ready(function () {

/*Remove Billing Fields From Checkout Page*/
    jQuery('.woocommerce-billing-fields').remove();



/*Changing the title of pages*/
    var url      = window.location.href;
    var res = url.split("/");

    var title = res[4];

    if( typeof title !== 'undefined' ) {

        if (title == 'account-dashboard') {
            title = 'dashboard'
        }
        if (title == 'users') {
            title = 'registrants'
        }
        if (title == '?page=blog-page') {
            title = 'Blog'
        }
        if (title == '') {
            title = 'profile'
            jQuery('#profile-personal-li_profile').addClass('current selected');
        }
        jQuery("title").html(title.toUpperCase() + " - BAM OnstreamLMS");
    }

/*Check contact form input names*/
    jQuery(".wpcf7-form").each(function(){
        var inputs = jQuery(this).find(':input');
        inputs.each(function (index , checkname) {
            if (checkname.name == "membershiplevel") {
                var pars_mem_id ={};location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){pars_mem_id[k]=v});
                console.log("input[name ='"+checkname.name+"']");
                jQuery("input[name ='"+checkname.name+"']").val(pars_mem_id.mem_id) ;
            }


        })
    });


    /*Hidding page title when go to add page in visual composer*/

    jQuery('.container > .row > .col-md-12 > .pagetitle').attr('style', 'display:none');

});/*End Ready Function*/


