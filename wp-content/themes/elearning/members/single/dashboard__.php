<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
if (!defined('ABSPATH'))
    exit;

if (!is_user_logged_in()) {
    wp_redirect(home_url(), '302');
}
get_header(eLearning_get_header());
$profile_layout = eLearning_get_customizer('profile_layout');
eLearning_include_template("profile/top$profile_layout.php");
?>
<div class="eLearning-dashboard row">
    <?php do_action('bp_before_dashboard_body'); ?>
    <img src="<?php echo get_bloginfo('template_directory') . '/assets/images/dashboard.jpg'; ?>">
    <?php do_action('bp_after_dashboard_body'); ?>
</div>
<?php
eLearning_include_template("profile/bottom.php");

get_footer(eLearning_get_footer());
