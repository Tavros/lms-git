
<?php  $current_user_login = wp_get_current_user()->user_login;?>
<div class="eLearning-website row hs-elearning">
    <div class="container-fluid website-menu">
        <ul class="nav navbar-nav list-group">
            <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/website"?>">PAGES</a></li>
            <li class="list-group-item current selected "><a href="<?php echo site_url()."/".$current_user_login."/registration-form"?>">REGISTRATION FORM</a> <span class="asadadada" ></span></li>
            <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/analytics"?>">ANALYTICS</a></li>
            <li class="list-group-item "><a href="<?php echo site_url()."/".$current_user_login."/site-settings"?>">SITE SETTINGS</a></li>
        </ul>
    </div><!--End container-fluid website-menu-->
    
</div><!--End eLearning-website row hs-elearning-->
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>