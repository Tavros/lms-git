<?php $current_user_login = wp_get_current_user()->user_login; ?>
<div class="eLearning-entries row hs-elearning">
    <div class="container-fluid website-menu fm_right_grid entries_top">
        <ul class="nav navbar-nav list-group">
           <li class="list-group-item "><a href="<?php echo site_url()."/".$current_user_login."/website"?>">PAGES</a></li>
           <li class="list-group-item current selected"><a href="<?php echo site_url()."/".$current_user_login."/registration-form"?>">REGISTRATION FORM</a></li>
           <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/Analitics"?>">ANALYTICS</a></li>
           <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/Analytics"?>">SITE  SETTINGS</a></li>
        </ul>
    </div>
    <div class="Form_Customization_menu">
        <ul class="Form_Customization_nav">
           <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/entries"?>"> ENTRIES</a><i class="fa fa-caret-up" aria-hidden="true"></i></li>
           <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_builder"?>"><i class="fa fa-caret-up  Customization_top_i" aria-hidden="true"></i>FORM BUILDER</a></li>
           <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form-messages"?>">MESSAGES</a></li>
           <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_responses"?>">RESPONSES</a></li>
           <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/Form_Customization"?>">CUSTOMIZATION</a> </li>
        </ul>
    </div>
    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 right_side">
        <div class="container-fluid">
            <div class="row right_row">
                <div class="col-md-4 col-sm-2 col-xs-2  "><h1>Entries</h1></div>                    
            </div>
            <!--div class="entries_search">
                <div class="search_bar_background">
                    <div class="col-md-10 col-sm-8 col-xs-8 search_bar">
                        <div id="imaginary_container">
                            <div class="input-group stylish-input-group">
                                <span class="input-group-addon ">
                                    <button type="submit" class="search_icon">
                                        <img src="https://lms.first.am/wp-content/themes/elearning/images/icons/search.png" >
                                    </button>
                                </span>
                                <input type="text" class="form-control search_button"  placeholder="Search Users..." >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-2 search_bar">
                        <div class="dropdown">
                            <button class="btn_custom dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Filter <i class="fa fa-sort-desc" aria-hidden="true"></i>
                            </button>                            
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-2">
                        <div class="dropdown">
                            <button class="btn_custom dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Sort <i class="fa fa-sort-desc" aria-hidden="true"></i>
                            </button>                            
                        </div>
                    </div>
                </div>
            </div-->
            <div class="row table_wrapper">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <table id="entries_data" class="table table-hover info_table" border="0">
                        <thead>
                            <tr>                        
                                <th><input type="checkbox" class="check"> DATE</th>                        
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th>ROLE</th>
                                <th>USER ID</th>
                                                                   
                            </tr>
                        </thead>
                        <tbody>
                            <tr> 
                                <td class="col-md-2"><input type="checkbox" class="check" style="vertical-align:middle;"> 3/14/17</td>
                                <td class="col-md-1"><a href="#">William Jameison</a></td>
                                <td class="col-md-1">williamdjameison@gmail.com</td>
                                <td class="col-md-1">STUDENT</td>
                                <td class="col-md-1">222</td> 

                            </tr>
                            <tr>                                               
                                <td><input type="checkbox" class="check" style="vertical-align:middle;"> 3/14/17</td>
                                <td><a href="#">Todd Fabacher</a></td>
                                <td>tfabacher@gmail.com</td>
                                <td>INSTRUCTOR</td>
                                <td>222</td>
                            </tr>
                            <tr>                                              
                                <td><input type="checkbox" class="check" style="vertical-align:middle;"> 3/14/17</td>
                                <td><a href="#">William Jameison</a></td>
                                <td>williamdjameison@gmail.com</td>
                                <td>STUDENT</td>
                                <td>222</td>
                            </tr>
                            <td><input type="checkbox" class="check" style="vertical-align:middle;">3/14/17</td>
                            <td><a href="#">Jimmy Jack</a></td>
                            <td>williamdjameison@gmail.com</td>
                            <td>STUDENT</td>
                            <td>222</td>
                        </tbody>
                    </table>
                    <div class="select-action">
                        <input type="checkbox" class="check" style="vertical-align:middle;"> 
                        <select>
                            <option>Bulk actions</option>
                        </select>
                        <div class="entries-paging">
                            <span><< Page 1 of 2 >></span>
                        </div>
                    </div><!--End select-action-->
                </div><!--End col-md-12 col-sm-12 col-xs-12-->             
            </div><!--End row table_wrapper-->
        </div><!--End container-fluid-->
    </div><!--End col-xs-12 col-md-12 col-sm-12 col-lg-12 right_side-->
</div><!--End eLearning-entries row hs-elearning-->
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>
<script type="text/javascript">
    jQuery(document).ready(function (){

        jQuery('#entries_data').DataTable({
            bInfo : false,
            language: {
                search: "<img class='search_icon' src='<?php echo  site_url(); ?>/wp-content/themes/elearning/images/icons/search.png'>",
                searchPlaceholder: "Search Users"
            }
        });
        jQuery('.dataTables_filter').addClass('fm-pull-left');
        jQuery('.dataTables_length').addClass('fm-pull-right');
        jQuery(".dataTable").on("draw.dt", function (e) {                    
             setCustomPagingSigns.call(jQuery(this));
         }).each(function () {
             setCustomPagingSigns.call(jQuery(this)); // initialize
         });
         function setCustomPagingSigns() {
             var wrapper = this.parent();
             wrapper.find("a.previous").text("<");
             wrapper.find("a.next").text(">");           
         }
    });
    jQuery(window).load(function (){

        var paging = jQuery('.dataTables_paginate');
        jQuery('#pages_data_wrapper .dataTables_paginate').hide();
        jQuery('.entries-paging span').hide();
        jQuery('.entries-paging').addClass('dataTables_wrapper');
        jQuery('.entries-paging').append(paging); 

    });
</script>