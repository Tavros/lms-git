<?php

/**
 * BuddyPress - Users Profile
 *
 * @package BuddyPress
 * @subpackage bp-default
 */
//
//error_reporting(E_ALL);
//$event_id = @$_REQUEST['id'];
$url = $_SERVER['REQUEST_URI'];
$current_user_login = wp_get_current_user()->user_login;
//add_post_meta(703,'eLearning_associated_event',700);
if(isset($url) && $url != '' ) {

    $file_name_array = explode('/', $url);

    $event_id = (int)$file_name_array[3];
}
$thankYou = '';
$InnerPge = '';
/*check if exists id then call edit action*/
if( !empty($event_id) && is_numeric ($event_id))
{
    //Get All Students
    $args = array(
        'meta_key'  => $event_id
    );
    $studentsData = get_users($args);

    $current_event = get_post($event_id);
    $current_event_custom_fields = get_post_meta($event_id);

    if(isset($current_event_custom_fields['eLearning_pmpro_membership'][0]) && !empty($current_event_custom_fields['eLearning_pmpro_membership'][0])){
        $currentPostMemberLevel = $current_event_custom_fields['eLearning_pmpro_membership'][0];
    }
    else{
        $currentPostMemberLevel = '';
    }
    if(isset($current_event_custom_fields['eLearning_instructor'][0]) && !empty($current_event_custom_fields['eLearning_instructor'][0])){
        $currentEventInstructor = $current_event_custom_fields['eLearning_instructor'][0];
    }
    else{
        $currentEventInstructor = '';
    }

    $event_img_url = wp_get_attachment_url( get_post_thumbnail_id($event_id), 'thumbnail' );

    $currentPostUnitId = unserialize($current_event_custom_fields['eLearning_course_curriculum'][0])[0];

    $units = get_post_meta( $currentPostUnitId );

    $unitType = $current_event_custom_fields['eLearning_type'][0];


    /*Get Thank You nad Inner Pages Links*/
    $thankYou = $current_event_custom_fields['thank_you_page'][0];
    $InnerPge = $current_event_custom_fields['inner_page'][0];

}
else{
    $current_user = wp_get_current_user();
    $create_new_event = array(
        'post_title' => 'No name',
        'post_type'  => 'course',
        'author'     => $current_user->data->ID
    );
    wp_insert_post($create_new_event);

    $args = array(
        'posts_per_page'   => 1,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'course',
        'post_status'      => 'draft',
        'author'           =>  $current_user->data->ID
    );
    $addedEvent = get_posts( $args );

    /*Create associated thank You Page*/
    $create_new_thank_you_page = array(
        'post_title'    => 'Thank You',
        'post_type'     => 'page',
        'post_status'   => 'publish',
        'post_content'  => 'Thank You For Subscribing',
        'author'        => $current_created_event->post_author
    );
    wp_insert_post($create_new_thank_you_page);

    $args = array(
        'posts_per_page'   => 1,
        'post_title'    => 'Thank You',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'page',
        'post_status'      => 'publish',
        'author'           =>  $current_created_event->post_author
    );

    $addedthankYouPage = get_posts( $args );
    add_post_meta($addedEvent[0]->ID,'thank_you_page',$addedthankYouPage[0]->ID);

    $data = array(
        'object_id' => $addedthankYouPage[0]->ID,
        'term_taxonomy_id' => '',
        'term_order' => 0,
    );
    $wpdb->insert('wp_term_relationships', $data);

    /*Create associated Inner Page*/
    $create_new_inner_page = array(
        'post_title'    => $current_created_event->post_title,
        'post_type'     => 'page',
        'post_status'   => 'publish',
        'author'        => $current_created_event->post_author
    );
    wp_insert_post($create_new_inner_page);

    $args = array(
        'posts_per_page'   => 1,
        'post_title'       => $current_created_event->post_title,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'page',
        'post_status'      => 'publish',
        'author'           =>  $current_created_event->post_author
    );

    $addedInnerPage = get_posts( $args );
    add_post_meta($addedEvent[0]->ID,'inner_page',$addedInnerPage[0]->ID);
    $data = array(
        'object_id' => $addedInnerPage[0]->ID,
        'term_taxonomy_id' => '',
        'term_order' => 0,
    );
    $wpdb->insert('wp_term_relationships', $data);

    /*Get Thank You nad Inner Pages Links*/
    $thankYou = $addedthankYouPage[0]->ID;
    $InnerPge = $addedInnerPage[0]->ID;
}

$args = array(
    'post_type'   => 'page'
);
$redirectPages = get_posts( $args );

$levels = $wpdb->get_results("SELECT id, name FROM $wpdb->pmpro_membership_levels ORDER BY name");

$unit_types = apply_filters('eLearning_unit_types', array(

    array('label' => __('ONSTREAM WEBINAR', 'eLearning-customtypes'), 'value' => 'webinar','image_url' => '/wp-content/themes/elearning/images/webinar.png','text' => 'Create a webinar to host multiple moderators in a live stream event.', 'key' => 'onstream_webinar'),
    array('label' => __('RECORDED WEBINAR', 'eLearning-customtypes'), 'value' => 'play','image_url' => '/wp-content/themes/elearning/images/recorded-webinar.png','text' => 'Capture video and broadcast to your audience.', 'key' => 'recorded_webinar'),
    array('label' => __('ONSTREAM WEBCAST', 'eLearning-customtypes'), 'value' => 'music-file-1','image_url' => '/wp-content/themes/elearning/images/onstream-webcast.png','text' => 'Display your recorded Onstream session at a selected time.', 'key' => 'onstream_webcast'),
    //array('label' => __('Podcast', 'eLearning-customtypes'), 'value' => 'podcast'),
    array('label' => __('OTHER TYPES', 'eLearning-customtypes'), 'value' => 'text-document','image_url' => '/wp-content/themes/elearning/images/other-types.png','text' => 'Create your own custom content or use one of the templates to create a custom event page.', 'key' => 'onstream_customtypes'),

));
//Get All Instructors
$args = array(
    'role'         => 'instructor'
);
$instructorsData = get_users($args);

?>

<div class="eLearning-website row hs-elearning edit_event">
<!--    <div class="head">-->
<!--        <div class="row">-->
<!--            <div class="col-md-8">-->
<!--                <h3>Event Name</h3>-->
<!--            </div>-->
<!--            <div class="col-md-2">-->
<!--                <a id="event_edit_save" class="btn btn-lg btn-primary pull-right top-button"  type="button" >SAVE</a>-->
<!--            </div>-->
<!--            <div class="col-md-2">-->
<!--                <a id="event_edit_publish" class="btn btn-lg btn-primar pull-righty top-button" type="button" >PUBLISH</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div><!--End head-->
<!--    <div class="event_menu_links">-->
<!--        <ul class="Form_Customization_nav">-->
<!--            <li class="Form_Customization_item"><a href="--><?php //echo  site_url(); ?><!--/--><?php //echo $current_user_login; ?><!--/entries"><i class="fa fa-caret-up" aria-hidden="true"></i>GENERAL</a></li>-->
<!--            <li class="Form_Customization_item"><a href="--><?php //echo  site_url(); ?><!--/--><?php //echo $current_user_login; ?><!--/event-messages">MESSAGES</a></li>-->
<!--            <li class="Form_Customization_item"><a href="--><?php //echo  site_url(); ?><!--/--><?php //echo $current_user_login; ?><!--/event-pricing">PRICING</a></li>-->
<!--            <li class="Form_Customization_item"><a href="--><?php //echo  site_url(); ?><!--/--><?php //echo $current_user_login; ?><!--/event-website">WEBSITE</a></li>-->
<!--        </ul>-->
<!--    </div>-->
<style type="text/css">
    .required_field{
        border:solid 1px red !important;
    }
</style>
    <div class="container event_edit_content">
        <form method="POST" id="edit_event_form" enctype="multipart/form-data">
            <input class="checker" type="hidden" name="updated" value="0">
            <!-- STEP 1    -->
            <h3></h3>
               <section>
                <div class="first-step">
                    <h2>STEP 1: SELECT AN EVENT TYPE</h2>
                    <div class="all-blocks">
                    <?php foreach ($unit_types as $unit_type):
                        $key =   $unit_type['key'];
                        
                        $active = '';
                        //echo $key . '<hr>';
                        if( $key == 'onstream_webinar' && $unitType = 'webinar' ) 
                        {
                            $active = 'active';
                        } 
                        
                        
                        $allow_webinar_ids = array('onstream_webinar' => 'Webinar ID', 'recorded_webinar' => 'Recorded ID');
                        $eLearning_webinar_id = isset($current_event_custom_fields['eLearning_webinar_id']) ? $current_event_custom_fields['eLearning_webinar_id'][0] : '';
                        
                        
                        /* check if webinar already recorded then set $eLearning_webinar_id - 0*/
                         if($current_event_custom_fields['event_recorded_switch'][0] == 'y') $eLearning_webinar_id = '';
                        
                        ?>
                        <div id="current_event_<?php echo $key; ?>" class="first-step-blocks lms_settings_event_types <?php echo $active; ?>" <?php if($unitType == $unit_type['value']) { echo'selected="selected"';} ?> ><h4><?php echo $unit_type['label']; ?></h4>
                            <img class="steps-images <?php echo $key; ?>" src="<?php echo $unit_type['image_url']; ?>">
                            <span> <?php echo $unit_type['text']; ?> </span>
                            <input type="radio" class="event-type-radio" name="eLearning_type" id="radio_<?php echo $key ?>" value="<?php echo $unit_type['value']; ?>">
                           
                           <?php if ( isset( $allow_webinar_ids[ $key ] ) ) : ?>
                                <div class="webinar-id" style="display: <?php if($active) echo 'block'; else echo 'none'; ?>">
                                    <p class="wb-id"><?php echo $allow_webinar_ids[$key]; ?></p>
                                    <input type="text" name="eLearning_webinar_id" value="<?php echo $eLearning_webinar_id;  ?>">
                                </div>
                            <?php endif; ?>
                        </div>

                    <?php endforeach; ?>
                    </div>

                    <div class="active-images" style="display:none;">

                        <img src="/wp-content/themes/elearning/images/webinar-checked.png">
                        <img src="/wp-content/themes/elearning/images/recorded-webinar-checked.png">
                        <img src="/wp-content/themes/elearning/images/onstream-webcast-checked.png">
                        <img src="/wp-content/themes/elearning/images/other-types-checked.png">

                    </div>


                </div>
            </section>
            <?php
            /*Basic Info > Title
            Basic Info > Start Date
            Basic Info > Start Time
            Basic Info > Duration
            Basic Info > Timezone
            Either the default should be located from their IP or the default should be empty and let them select
            Basic Info > Instructor
            Basic Info > Category
            Please make the default empty
            Pricing > Free Event
            If “Yes” then hide all the fields below
            If “No” then show the fields
            Set the default values of the fields to
            Price: $0
            Sale Price: ‘empty’
            Membership level: ‘unselected’
            On demand price: $0
            Publish > Publish at a certain time
            If this box is checked, two fields will appear. If the fields are showing, we need to make them required as well.
            */
            ?>
            <!-- STEP 2    -->
            <h3></h3>
            <section style="display: none;">
                <div class="second-step">

                    <h2>STEP 2: ENTER BASIC INFO FOR YOUR EVENT</h2>

                    <div class="evantTitle col-lg-10">
                        <h6>TITLE</h6>
                        <input class="step1Iput editEvent " data-required="2" type="text" id="event_title" name="post_title" value="<?php  if($event_id)echo $current_event->post_title;?>"/>
                        <div class="startDate basic-inputs">
                            <h6>START DATE</h6>
                            <input id="datepicker" class="eventDesc" data-required="2" type="text" name="eLearning_start_date" value="<?php if($event_id)echo $current_event_custom_fields['eLearning_start_date'][0];?>">
                        </div>
                        <div class="startTime elearning3Step1 basic-inputs">
                            <h6>START TIME</h6>
                            <input class="eventDesc" data-required="2" id="eLearning_start_time" type="text" name="eLearning_start_time" value="<?php if($event_id) echo $current_event_custom_fields['eLearning_start_time'][0];?>">
                        </div>
                        <div class="expDuration basic-inputs">
                            <h6>DURATION</h6>
                            <input class="eventDesc elearning3Step1" data-required="2" type="text" name="eLearning_duration" id="eLearning_duration" value="<?php if($event_id) echo $current_event_custom_fields['eLearning_duration'][0]?>" />
                        </div>
                        <div class="timezone basic-inputs">
                            <h6>TIME ZONE</h6>
                            <input type="text" name="timezone" id="timezone" data-required="2" value="<?php  if($event_id && isset($current_event_custom_fields['eLearning_timezone']) ) echo $current_event_custom_fields['eLearning_timezone'][0]; else echo date_default_timezone_get(); ?>">
                        </div>

                        <div class="basic-select">
                            <h6>INSTRUCTOR</h6>  
                            <select class="step1Select" id="eLearning_instructor" name="eLearning_instructor" data-required="2">
                                <option value="default">Select a Instructor</option>
                                <?php
                                foreach ($instructorsData as $instructorData): ?>
                                    <option value="<?php echo $instructorData->data->ID; ?>" <?php if($currentEventInstructor == $instructorData->data->ID){echo 'selected="selected"';} ?>><?php echo $instructorData->data->display_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>


                        <div class="basic-select">
                            <?php
                            $get_all_categories = get_terms( array(
                                'taxonomy' => 'course-cat',
                                'hide_empty' => false,
                            ) );
                            if(isset($event_id) && !empty($event_id)){
                                
                                $get_the_post_category = wp_get_object_terms($event_id,'course-cat');  
                            }
                            else {
                                
                                $get_the_post_category = '';

                            }

                            isset($get_the_post_category[0]) && !empty($get_the_post_category[0])? $currentPostCategory = $get_the_post_category[0] : $currentPostCategory = '';
                            ?>
                            <h6>EVENT CATEGORY</h6>
                            <select class="step1Select" id="eLearning_event_category" name="eLearning_event_category" data-required="2">

                                <?php foreach ($get_all_categories as $cat):?>
                                    <?php if($get_the_post_category[0]->term_id ==  $cat->term_id) { ?>
                                        <option value="<?php  echo $get_the_post_category[0]->term_id; ?>" selected="selected"> <?php  echo $get_the_post_category[0]->name;?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo $cat->term_id; ?>"> <?php  echo $cat->name;?></option>
                                    <?} ?>
                                <?php endforeach;?>

                            </select>
                        </div>


                        <!--div class="WebinarID">
                    <h6 class="h6color">WEBINAR ID</h6>
                    <input class="eventDesc" type="text" name="eLearning_webinar_id" value="<?php // echo $current_event_custom_fields['eLearning_webinar_id'][0] ?>">
                </div-->
                        <div class="max-cap">
                            <h6>MAXIMUM CAPACITY</h6>
                            <?php if(isset($current_event_custom_fields['eLearning_maximum_students'][0]) && !empty($current_event_custom_fields['eLearning_maximum_students'][0])){$maxStudents = $current_event_custom_fields['eLearning_maximum_students'][0];} ?>
                            <input class="maxcap" type="text" value="<?php echo $maxStudents; ?>" name="eLearning_maximum_students" >
                        </div>

                        <span style="font-style:italic;float: left;">Leave blank to use your default maximum allowed by your Onstream Account</span>

                        <div class="col-lg-2"></div>
                        <div class="col-lg-12 select">
                            <div class="col-lg-6 elearning3Step1 select">

                                <div class="webinarId_webinarRecorder" style="display: none;">
                                    <label for="eLearning_webinar_id">Webinar Id</label>
                                    <input type="text" id="eLearning_webinar_id" name="eLearning_webinar_id" value="<?php echo $current_event_custom_fields['eLearning_webinar_id'][0] ?>">
                                    </br>
                                    <label for="eLearning_webinar_recorder">Webinar Recorder</label>
                                    <input type="text" id="eLearning_webinar_recorder" name="eLearning_webinar_recorder" value="<?php echo $current_event_custom_fields['eLearning_webinar_recorder'][0] ?>">
                                </div>

                                <!--Get and Set categories for event-->
                            </div><!--End col-lg-6 elearning3Step1 select-->
                            <div class="col-lg-6"></div>
                            <div class="col-lg-12 description">
                                <div class="col-lg-3 watch">
                                    <h6>IMAGE</h6>
                                    <?php if(!empty($event_img_url)): ?>
                                        <img class="elearning3Step1" id="change_img" src="<?php if($event_id)echo $event_img_url;?>">
                                    <?php else: ?>
                                        <img class="elearning3Step1" id="change_img" src="/wp-content/themes/elearning/images/book-11433-300x300.jpg">
                                    <?php endif; ?>
                                </div>

                                <div class="col-lg-9 elearning3Step1">
                                    <h6>SHORT DESCRIPTION </h6>
                                    <textarea  rows="9" cols="50" name="eLearning_event_sort_description"><?php if($event_id)echo $current_event_custom_fields['eLearning_course_instructions'][0];?></textarea>
                                   
                                </div>
                            </div><!--End col-lg-12 description-->
                        </div><!--End col-lg-12 select-->

                        <!--   <input type="submit" id="edit_event_form_submit" class="btn" value="SAVE EVENT +" />-->
                        <!--
                        <div class="bottom-buttons">
                        <input type="button" class="back-btn" value="BACK >">
                        <input type="button" class="next-btn" value="NEXT >">
                        </div>
                        -->
                    </div><!--End evantTitle col-lg-10-->
                </div><!--End elearning3Step1-->


            </section>
            <!-- STEP 3    -->
            <h3></h3>
            <section style="display: none;">
                <div class="third-step">
                    <h2>STEP 3: DETERMINE YOUR PRICING</h2>
                    <div class="col-lg-12 elearning3Price">
                        <div class="applyForCourse ">
                            <div class="col-lg-12">
                                <h6><b>Free Event</b></h6>
                            </div>
                            <div class="col-lg-12">
                                <div class="switch">
                                    <input type="radio" class="switch-input" name="eLearning_course_free" value="H" id="no-pricing" checked>
                                    <label for="no-pricing" class="switch-label switch-label-off">No</label>
                                    <input type="radio" class="switch-input" name="eLearning_course_free" value="S" id="yes-pricing">
                                    <label for="yes-pricing" class="switch-label switch-label-on">Yes</label>
                                    <span class="switch-selection"></span>
                                </div>
                                <span>Setting this event to free will allow access to all members and guests and skip all payment processes</span>
                            </div>
                        </div>

                        <div class="inputs">
                            <div class= "col-lg-3">
                                <p><b>PRICE</b></p>
                                <?php
                                $regularPrice = ''; if(isset($current_event_custom_fields['_regular_price'][0]) && !empty($current_event_custom_fields['_regular_price'][0])){$regularPrice = $current_event_custom_fields['_regular_price'][0];} ?>
                                <input class="step2input" type="text" data-required="3" value="<?= $regularPrice; ?>" name="_regular_price" />
                            </div>
                            <div class="col-lg-3">
                                <p><b>Sale Price</b></p>
                                <?php $salePrice = ''; if(isset($current_event_custom_fields['_sale_price'][0]) && !empty($current_event_custom_fields['_sale_price'][0])){$salePrice = $current_event_custom_fields['_sale_price'][0];} ?>
                                  <input class="step2input step2inputright" type="text" value="<?= $salePrice; ?>" name="_sale_price" />
                            
                                
                            </div>
                        </div>

                        <div class= "col-lg-6 ondemand">
                            <p><b>ON DEMAND PRICE</b></p>
                            <input class="on-demand" name="eLearning_recorded_event_price" value="" type="text">
                            <span>When the webinar is finished, the price of the event will change to this amount.</span>
                        </div>
                    <div style="clear: both;"></div>
                        <div class= "col-lg-3">
                            <p><b>MEMBERSHIP ACCESS</b></p>
          <!--                  <select class="select2-select " id="eLearning_pmpro_membership" multiple="multiple" name="eLearning_pmpro_membership[]">
                                <option value="default">Select a Event Level</option>
                                <?php /*foreach ($levels as $level): */?>
                                    <option value="<?php /*echo $level->id */?>" <?php /*if($currentPostMemberLevel == $level->id){echo 'selected="selected"';} */?>><?php /*echo $level->name; */?></option>
                                <?php /*endforeach; */?>
                            </select>-->
                            <div class="selectBox" onclick="showCheckboxes()">
                                <select>
                                    <option>Select a Event Level</option>
                                </select>
                                <div class="overSelect"></div>
                            </div>
                            <div id="checkboxes">
                                <?php $check = 0; foreach ($levels as $level): ?>
                                <label for="one_<?= $check; ?>">
                                    <input type="checkbox" name="eLearning_pmpro_membership" value="<?= $level->id ?>" id="one_<?= $check; ?>" /><?= $level->name; ?></label>
                                    <?php $check++; endforeach; ?>
                            </div>

                            <input type="hidden" name="eLearning_course_curriculum" value="<?php echo serialize($level); ?>" />
                        </div>
                    </div>
                </div>
            </section>
            <!-- STEP 4    -->
            <h3></h3>
            <section style="display: none;">
                <div class="four-step">
                    <h2>STEP 4:CONFIRMATION EMAIL</h2>
                    <div class="col-lg-12 confirmationEmail">


                        <div class=" ">
                            <div class="col-lg-12">
                                <h6><b>SEND CONFIRMATION EMAIL</b></h6>
                            </div>
                            <div class="col-lg-12">
                                <div class="switch">
                                    <input type="radio" class="switch-input" name="email" value="no-email" id="no-email" checked>
                                    <label for="no-email" class="switch-label switch-label-off">No</label>
                                    <input type="radio" class="switch-input" name="email" value="yes-email" id="yes-email">
                                    <label for="yes-email" class="switch-label switch-label-on">Yes</label>
                                    <span class="switch-selection"></span>
                                </div>
                                <span style="font-style: italic;"> After a student registers for the event, a customized email may be sent to thrm confirming their registration.</span>
                            </div>

                        </div>

                        <div class="form inputs">
                            <div class='row'>
                                <div class='col-lg-2 step4'>
                                    <p><b> Form</b></p>
                                </div>
                                <div class='col-lg-10 step4'>
                                    <input class="step4input step4inputright"  type="text" id="your_name"  name="your_name" >
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col-lg-2 step4'>
                                    <p><b> Subject</b></p>
                                </div>
                                <div class='col-lg-10 step4'>
                                    <input class="step4input step4inputright"  type="text" id="your_Subject"  name="your_Subject" >
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col-lg-2 step4'>
                                    <p><b>Additional headers</b></P>
                                </div>
                                <div class='col-lg-10 step4'>
	                	<textarea class='step4textarea' rows="4" cols="50">

		                </textarea>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-lg-2 step4'>
                                    <p><b>Message Body</b></P>
                                </div>
                                <div class='col-lg-10 step4'>
	                	<textarea class='step4textarea' rows="4" cols="50">

	                	</textarea>
                                </div>
                            </div>
                            <?php
                        


                        
                             ?>
            </section>
            <!-- STEP 5    -->
            <h3></h3>
            <section style="display: none;">
                <div class="fourth-step">
                    <div class="edit_events_ando col-md-9">
                        <div class="row">
                            <h2>STEP 5: DESIGN YOUR EVENT PAGES</h2>
                            <div class="col-md-12 step5-blocks">
                                <div class="col-md-3">
                                    <img class="elearning3Step1" id="change_img123" src="/wp-content/themes/elearning/images/Rounded-Rectangle-28.png">
                                </div>
                                <div class="col-md-9">
                                    <p class="font_h2">SALES / REGISTRATION PAGE</p>
                                    <p>Create a custom landing page for your event. The landing page is a public page that can be used to advertise your event, capture leads, and sell tickets.</p>
                                    <div style="display: none;" class="switch_connect">
                                        <span class=" col-md-6">ACTIVATE SALES PAGE</span>
                                        <label class="switch col-md-6  ">
                                            <input type="checkbox">
                                            <div class="slider round"></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="switch_w">
                                        <span>TITLE</span>
                                        <input type="text" name="">
                                        <button>EDIT <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 step5-blocks">
                                <div class="col-md-3">
                                    <img class="elearning3Step1" id="change_img456" src="/wp-content/themes/elearning/images/Rounded-Rectangle-30.png">
                                </div>
                                <div class="col-md-9">
                                    <p class="font_h2">EVENT PAGE</p>
                                    <p>By default, webinars and streaming media are opened in a new window. If you would like to create a page and embed the webinar, check here.</p>
                                    <div style="display: none;" class="switch_connect">
                                        <span class=" col-md-6">OPEN WEBINAR OR WEBCAST IN NEW WINDOW</span>
                                        <label class="switch col-md-6  ">
                                            <input type="checkbox">
                                            <div class="slider round"></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="switch_w">
                                        <a href="/wp-admin/post.php?vc_action=vc_inline&post_id=<?php echo $thankYou; ?>"  target="_blank">
                                            <button type="button"  class="btn btn-default prev-edit">EDIT <i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 step5-blocks">
                                <div class="col-md-3">
                                    <img class="elearning3Step1" id="change_img789" src="/wp-content/themes/elearning/images/Rounded-Rectangle-31.png">
                                </div>
                                <div class="col-md-9">
                                    <p class="font_h2">THANK YOU / INSTRUCTION PAGE</p>
                                    <p>Upon successful registration, direct your users to a custom thank you page instead of the event page. This could be very useful when pre-registering users.</p>
                                    <div style="display: none;" class="switch_connect">
                                        <span class=" col-md-6">SHOW THANK YOU PAGE AFTER REGISTRATION</span>
                                        <label class="switch col-md-6  ">
                                            <input type="checkbox">
                                            <div class="slider round"></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="switch_w">
                                        <span>TITLE</span>
                                        <input type="text" name="">
                                        <a href="/wp-admin/post.php?vc_action=vc_inline&post_id=<?php echo $InnerPge; ?>"  target="_blank">
                                            <button type="button"  class="btn btn-default prev-edit">EDIT <i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        </a>
                                    </div>
                                </div>                            
                               
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- STEP 6    -->
            <h3></h3>
            <section style="display: none;">
                <div class="sixth-step">
                    <h2>STEP 5: PUBLISH YOUR EVENT</h2>

                    <h3>Congratulations your event is ready to be published!</h3>

                    <span class="checkbox-text">Would you like to add an event countdown to the event page to show to your users before the event begins?</span>

                    <div class="switch">
                        <input type="radio" class="switch-input" name="event-publish" value="publish-event" id="publish-event" checked>
                        <label for="publish-event" class="switch-label switch-label-off">No</label>
                        <input type="radio" class="switch-input" name="event-publish" value="publish1" id="publish1">
                        <label for="publish1" class="switch-label switch-label-on">Yes</label>
                        <span class="switch-selection"></span>
                    </div>
                    <div class="col-lg-12 checkbox-buttons">
                        <input type="button" name="eLearning_event_status" value="Inactive" class="save-us-draft">
                        <input type="button" name="eLearning_event_status" value="Active" class="publish-btn">
                    </div>
                    <div class="publish-checkbox">
                        <input type="checkbox" name="publish-checkbox" class="check-to-publish">
                        <h5>Check this box to publish the event a specific time.</h5>
                        <span style="font-style: italic;">Your users will not be able to see the event, but you will still see the event appear in the admin dashboard.</span>
                    </div>
                    <div class="date-time" style="display: none;">
                        <div class="startDate basic-inputs">
                            <label>DATE</label>
                            <input id="datepicker" data-date-inline-picker="true" class="eventDesc" type="date" placeholder="" name="eLearning_start_date" value="<?php if($event_id)echo $current_event_custom_fields['eLearning_start_date'][0]?>">
                        </div>
                        <div class="startTime elearning3Step1 basic-inputs">
                            <label>TIME</label>
                            <input class="eventDesc" type="text" name="eLearning_start_time" value="<?php if($event_id) echo $current_event_custom_fields['eLearning_start_time'][0]?>">
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
    </div>
</div>
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>


<script>
    var expanded = false;

    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
    jQuery(document).ready(function(){


        /**** Next/back Button Click ****/


/*        jQuery("#edit_event_form").steps({
            transitionEffect: "slideLeft",
            autoFocus: true
        });*/
        var form = jQuery("#edit_event_form");
        form.steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onFinished: function (event, currentIndex)
            {
               // alert("Submitted!");
                form.submit();
            },
            onStepChanging : function(event, currentIndex){
                /*if($('')){
                    //return false;
                }*/
                var is_checked = true;
                if(currentIndex == 0){
                    //jQuery('').validate();
                }else if(currentIndex == 1){
                    rcount = 0;
                    jQuery('[data-required="2"]').each(function( key, obj ) {

                            if(jQuery(this).val() == '' ||  jQuery(this).val().replace(' ','') == ''){
                                    jQuery(this).addClass('required_field');
                                   rcount++;
                            }
                            is_checked = rcount <= 0;
                        }
                    );
                }else if(currentIndex == 2){
                    rcount = 0;
                    jQuery('[data-required="3"]').each(function( key, obj ) {

                            if(jQuery('[name="eLearning_course_free"]:checked').val() == "H") {
                                if (jQuery(this).val() == '' || jQuery(this).val().replace(' ', '') == '') {
                                    jQuery(this).addClass('required_field');
                                    rcount++;
                                }
                                is_checked = rcount <= 0;
                            }
                        }
                    );
                }else if(currentIndex == 3){

                }else if(currentIndex == 4){

                }else if(currentIndex == 5){

                }else if(currentIndex == 6){

                }
                console.log(currentIndex);
                return is_checked;
            }
        });

        /*** end button click ***/

//     jQuery('.form-control').timezones();


        /*** Event publish checkbox ***/

        jQuery('.check-to-publish').click(function(){
            if (this.checked) {
                jQuery('.date-time').show();
            }else{
                jQuery('.date-time').hide();
            }
        })


        /*** end event publish checkbox ***/

        /*** Show webinar id ***/
        jQuery(document).on('click', '.lms_settings_event_types', function(){
            jQuery('.webinar-id').hide();
            jQuery(this).find('.webinar-id').show();

        });




        /*** end webinar id ***/

        /* Event types checkbox sections setup*/
        jQuery('.lms_settings_event_types').click(function(){
            var current_id = jQuery(this).attr('id');

            /*delete previous active class*/
            jQuery('.lms_settings_event_types').each(function(){ jQuery(this).removeClass('active'); })

            jQuery( this ).addClass('active');

            var _key = current_id.replace('current_event_', '');

            current_radio_id = 'radio_'  + _key;

            jQuery('#'+current_radio_id ).prop('checked', true);
        });





        jQuery("#back").click(function(){
            jQuery("#elearning3Step2").addClass("elearning3Step2");
            jQuery("#elearning3Step1").removeClass("elearning3Step");
        });

        jQuery("#imgButton").click(function(){
            jQuery("#elearning3Step1").addClass("elearning3Step");
            jQuery("#elearning3Step2").removeClass("elearning3Step2");

        });
        jQuery( "#datepicker" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });

        var unitType = jQuery('#eLearning_type option:selected').val();
        if(unitType == 'webinar'){
            jQuery('.webinarId_webinarRecorder').css('display', 'block');
        }
        jQuery('#eLearning_type').change( function(){
            if(jQuery(this).val() == 'webinar'){
                jQuery('.webinarId_webinarRecorder').css('display', 'block');
            }
            else{
                jQuery('.webinarId_webinarRecorder').css('display', 'none');
            }
        });

    });

    jQuery(document).ready(function() {
        jQuery("#event_edit_save").click(function() {
            jQuery('.checker').val(1);
            jQuery("#edit_event_form").submit();


        });
        jQuery('#edit_event_form').click(function (){
            jQuery('.checker').val(1);
        });

//        jQuery("#edit_event_form").submit(function(){
//            var test = jQuery('#edit_event_form').serialize();
//            console.log(test);
//            jQuery.post({url: window.location.href, data: test, success: function(result){
//                location.reload();
//            }});
//        });
    });

</script>
<script>
    /*    var mediaUploader;

     jQuery('#change_img').click(function(e) {
     e.preventDefault();
     // If the uploader object has already been created, reopen the dialog
     if (mediaUploader) {
     mediaUploader.open();
     return;
     }
     // Extend the wp.media object
     mediaUploader = wp.media.frames.file_frame = wp.media({
     title: 'Choose Image',
     button: {
     text: 'Choose Image'
     }, multiple: false });

     // When a file is selected, grab the URL and set it as the text field's value
     mediaUploader.on('select', function() {
     var attachment = mediaUploader.state().get('selection').first().toJSON();
     jQuery('#image-url').val(attachment.url);
     });
     // Open the uploader dialog
     mediaUploader.open();
     }); */

jQuery(document).ready(function(){
    
    


    // Uploading files
    var media_uploader;
    jQuery('#change_img').click( function (event) {  

        var button = jQuery(this);
         
        if (media_uploader) {
            media_uploader.open();
            return;
        }
      
        // Create the media uploader.
        media_uploader = wp.media.frames.media_uploader = wp.media({
            title: button.data('uploader-title'),
            // Tell the modal to show only images.
            library: {
                type: 'image',
                query: false
            },
            button: {
                text: button.data('uploader-button-text'),
            },
            multiple: button.data('uploader-allow-multiple')
        });

        // Create a callback when the uploader is called
        media_uploader.on('select', function () {
            var selection = media_uploader.state().get('selection'),
                input_name = "change_event_image";
            selection.map(function (attachment) {
                attachment = attachment.toJSON();
                console.log(attachment);
                var url_image = '';
                if (attachment.sizes) {
                    if (attachment.sizes.thumbnail !== undefined)
                        url_image = attachment.sizes.thumbnail.url;
                    else if (attachment.sizes.medium !== undefined)
                        url_image = attachment.sizes.medium.url;
                    else
                        url_image = attachment.sizes.full.url;
                }

                button.html('<img src="' + url_image + '" class="submission_thumb thumbnail" /><input id="' + input_name + '" class="post_field" data-type="featured_image" data-id="' + input_name + '" name="' + input_name + '" type="hidden" value="' + attachment.id + '" />');
            });

        });
        // Open the uploader
        media_uploader.open();
    });
    
});
</script>

<?php
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------UPDATE EVENT---------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------*/
isset($_POST['post_title']) && !empty($_POST['post_title']) ? $postTitle = $_POST['post_title'] : $postTitle = '';
$atts_event_post = array(
    'ID'=> $event_id,
    'post_title' => $postTitle,

);


if(isset($_REQUEST['updated']) && $_REQUEST['updated'] == 1 && $event_id){
/*    echo "<pre>";
    print_r($_POST);die;*/
    isset($_POST['post_title']) && !empty($_POST['post_title']) ? $postTitle = $_POST['post_title'] : $postTitle = '';
    if(isset($_POST['eLearning_event_category']) && !empty($_POST['eLearning_event_category'])){
        $i = trim($_POST['eLearning_event_category']);
        wp_set_object_terms($event_id,array('tag_ID' => (int)$i), 'course-cat');
    }
    wp_update_post($atts_event_post);
    /* Update Custom fields*/

    if(isset($_POST['change_event_image']) && $_POST['change_event_image'] != '' ) {
        update_post_meta($event_id, '_thumbnail_id', $_POST['change_event_image']);
    }

    update_post_meta($event_id,'eLearning_start_date',$_POST['eLearning_start_date']);
    update_post_meta($event_id,'eLearning_start_time',$_POST['eLearning_start_time']);
    update_post_meta($event_id,'eLearning_duration',$_POST['eLearning_duration']);
    update_post_meta($event_id,'elearning_events_status',$_POST['eLearning_event_status']);
    update_post_meta($event_id,'elearning_events_status',$_POST['eLearning_event_status']);
    update_post_meta($event_id,'eLearning_course_instructions',$_POST['eLearning_event_sort_description']);
    update_post_meta($event_id,'eLearning_course_instructions',$_POST['eLearning_event_sort_description']);
    update_post_meta($event_id,'eLearning_pmpro_membership',$_POST['eLearning_pmpro_membership']);
    update_post_meta($event_id,'eLearning_maximum_students',$_POST['eLearning_maximum_students']);
    update_post_meta($event_id,'eLearning_webinar_recorder',$_POST['eLearning_webinar_recorder']);
    update_post_meta($event_id,'eLearning_webinar_id',$_POST['eLearning_webinar_id']);
    update_post_meta($event_id,'eLearning_course_curriculum',$_POST['eLearning_course_curriculum']);
    update_post_meta($event_id,'event_recorded_switch',$_POST['event_recorded_switch']);
    update_post_meta($currentPostUnitId,'eLearning_type',$_POST['eLearning_type']);
    update_post_meta($event_id,'eLearning_type',$_POST['eLearning_type']);
    update_post_meta($event_id,'_regular_price',$_POST['_regular_price']);
    update_post_meta($event_id,'_sale_price',$_POST['_sale_price']);
    update_post_meta($event_id,'eLearning_recorded_event_price',$_POST['eLearning_recorded_event_price']);
    if(($_POST['_regular_price'] == 0 || empty($_POST['_regular_price'])) && ($_POST['_sale_price'] == 0) || empty($_POST['_sale_price'])){
        update_post_meta($event_id,'eLearning_course_free','S');
    }
    update_post_meta($event_id,'eLearning_instructor',$_POST['eLearning_instructor']);


    //Check If webinar id is present, then go and Create new Webinar in Onstream
    if(isset($_POST['eLearning_type']) && !empty($_POST['eLearning_type'])){
        require_once( $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/elearning/eLearning-addons/onstreamAPI/functions.php' );

        $post_settings = array();
        $post_metas = '';
        $webinarId = '';
        $instructorId = '';
        $isWebinarChecker = false;
        //Check If event type is webinar or not
        if(isset($_POST['eLearning_webinar_id']) && !empty($_POST['eLearning_webinar_id'])){
            $webinarId = $_POST['eLearning_webinar_id'];
            $isWebinarChecker = true;
        }
        //Check If event instructor isset
        if(isset($_POST['eLearning_instructor']) && !empty($_POST['eLearning_instructor'])){
            $instructorId = $_POST['eLearning_instructor'];
        }

        $post_settings['post_title'] = $postTitle;
        $post_settings['eLearning_expected_duration'] = $_POST['eLearning_duration'];
        $post_settings['eLearning_start_date'] = $_POST['eLearning_start_time'];
        //Check if event type webinar and webinar id isset or not
        if ($isWebinarChecker == true){
            //Set Webinar too tegistered user
            foreach ($studentsData as $studentData){
                SetStudentAPI( $webinarId, $studentData->data->ID);
            }
        }
        else{
            //Create webinar and Set Webinar too tegistered user
            $data = createNewWebinarInOnstream($post_settings, $post_metas, $instructorId, $webinarId);
            $webinarId = $data['webinarId'];
            $addedInstructorId = $data['addedInstructorId'];
            add_post_meta($event_id,'eLearning_webinar_id',$webinarId);
            foreach ($studentsData as $studentData){
                SetStudentAPI( $webinarId, $studentData->data->ID);
            }
        }

    }

    if(!get_post_meta($event_id, 'eLearning_redirect_url')){
        add_post_meta($event_id,'eLearning_redirect_url',$_POST['eLearning_redirect_url']);
    }
    else{
        update_post_meta($event_id,'eLearning_redirect_url',$_POST['eLearning_redirect_url']);
    }

    if(isset($_POST['eLearning_redirect_url']) && !empty($_POST['eLearning_redirect_url'])){
        header("Location: ".$_POST['eLearning_redirect_url']."");
    }
    else{
        $url = get_site_url().'/'.$current_user_login."/events";
        header("Location: $url");
    }


}
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------CREATE EVENT---------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------*/
if(isset($addedEvent)){

    if(isset($_REQUEST['updated']) && $_REQUEST['updated'] == 1 && $addedEvent){

        //Get New Added Post Id
        $newEventId = $addedEvent[0]->ID;

        //Add Event
        wp_publish_post($newEventId);

        //Update Added Event default data
        $update_event_post = array(
            'ID'            => $newEventId,
            'post_title'    => $postTitle
        );
        wp_update_post($update_event_post);

        //Update Added Event Post Metas
        add_post_meta($newEventId,'eLearning_maximum_students',$_POST['eLearning_maximum_students']);
        add_post_meta($newEventId,'eLearning_start_date',$_POST['eLearning_start_date']);
        add_post_meta($newEventId,'eLearning_start_time',$_POST['eLearning_start_time']);
        add_post_meta($newEventId,'eLearning_duration',$_POST['eLearning_duration']);
        add_post_meta($newEventId,'elearning_events_status',$_POST['eLearning_event_status']);
        add_post_meta($newEventId,'elearning_events_status',$_POST['eLearning_event_status']);
        add_post_meta($newEventId,'eLearning_course_instructions',$_POST['eLearning_event_sort_description']);
        add_post_meta($newEventId,'eLearning_course_instructions',$_POST['eLearning_event_sort_description']);
        add_post_meta($newEventId,'eLearning_redirect_url',$_POST['eLearning_redirect_url']);
        add_post_meta($newEventId,'eLearning_pmpro_membership',serialize($_POST['eLearning_pmpro_membership']));
        add_post_meta($newEventId,'eLearning_webinar_recorder',$_POST['eLearning_webinar_recorder']);
        add_post_meta($newEventId,'event_recorded_switch',$_POST['event_recorded_switch']);
        add_post_meta($newEventId, '_thumbnail_id', $_POST['change_event_image']);
        add_post_meta($newEventId,'eLearning_course_curriculum',serialize($_POST['eLearning_course_curriculum']));
        $category = trim($_POST['eLearning_event_category']);
        wp_set_object_terms($newEventId,array('tag_ID' => (int)$category), 'course-cat');
        add_post_meta($newEventId,'_regular_price',$_POST['_regular_price']);
        add_post_meta($newEventId,'_sale_price',$_POST['_sale_price']);
        if(($_POST['_regular_price'] == 0 || empty($_POST['_regular_price'])) && ($_POST['_sale_price'] == 0) || empty($_POST['_sale_price'])){
            update_post_meta($newEventId,'eLearning_course_free','S');
        }
        add_post_meta($newEventId,'is_event','S');
        add_post_meta($newEventId,'eLearning_instructor',$_POST['eLearning_instructor']);
        add_post_meta($newEventId,'eLearning_recorded_event_price',$_POST['eLearning_recorded_event_price']);
        //Check If webinar id is present, then go and Create new Webinar in Onstream
        if(isset($_POST['eLearning_type']) && !empty($_POST['eLearning_type']) && $_POST['eLearning_type'] == 'webinar'){
            require_once( $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/elearning/eLearning-addons/onstreamAPI/functions.php' );

            $post_settings = array();
            $post_metas = '';
            $webinarId = '';
            $instructorId = '';
            //Check If event type is webinar or not
            if(isset($_POST['eLearning_webinar_id']) && !empty($_POST['eLearning_webinar_id'])){
                $webinarId = $_POST['eLearning_webinar_id'];
            }
            if(isset($_POST['eLearning_instructor']) && !empty($_POST['eLearning_instructor'])){
                $instructorId = $_POST['eLearning_instructor'];
            }
            $post_settings['post_title'] = $postTitle;
            $post_settings['eLearning_expected_duration'] = $_POST['eLearning_duration'];
            $post_settings['eLearning_start_date'] = $_POST['eLearning_start_time'];
            $data = createNewWebinarInOnstream($post_settings, $post_metas, $webinarId, $instructorId);

            $webinarId = $data['webinarId'];
            $addedInstructorId = $data['addedInstructorId'];
            add_post_meta($newEventId,'eLearning_webinar_id',$webinarId);

            /*  if (!empty($addedInstructorId)){
                    foreach ($studentsData as $studentData){
                        SetStudentAPI( $webinarId, $studentData->data->ID);
                    }
               } */
        }

        //Get New Created And Updated Event Data
        $current_created_event = get_post($newEventId);
        //Check if event unit type is isset, then create new unit and set him unit type same event
        if(isset($_POST['eLearning_type']) && !empty($_POST['eLearning_type'])){
            //Create New Unit
            $create_new_unit = array(
                'post_title'    => $current_created_event->post_title,
                'post_type'     => 'unit',
                'post_status'   => 'publish',
                'author'        => $current_created_event->post_author
            );
            wp_insert_post($create_new_unit);
            $args = array(
                'posts_per_page'   => 1,
                'orderby'          => 'date',
                'order'            => 'DESC',
                'post_type'        => 'unit',
                'post_status'      => 'publish',
                'author'           =>  $current_created_event->post_author
            );
            $addedUnit = get_posts( $args );
            //Set Unit Type To Created Unit
            add_post_meta($addedUnit[0]->ID,'eLearning_type',$_POST['eLearning_type']);
        }


        //Create new product, wich connect to this event
        $create_new_product = array(
            'post_title'    => $current_created_event->post_title,
            'post_type'     => 'product',
            'post_status'   => 'publish',
            'author'        => $current_created_event->post_author
        );
        wp_insert_post($create_new_product);

        $args = array(
            'posts_per_page'   => 1,
            'orderby'          => 'date',
            'order'            => 'DESC',
            'post_type'        => 'product',
            'post_status'      => 'publish',
            'author'           =>  $current_created_event->post_author
        );

        //Add post metas (product price, sale price and add post meta wich connect this product to this event)
        $addedProduct = get_posts( $args );
        add_post_meta($newEventId,'eLearning_product',$addedProduct[0]->ID);
        add_post_meta($addedProduct[0]->ID,'eLearning_associated_product',$newEventId);
        add_post_meta($addedProduct[0]->ID,'_regular_price',$_POST['_regular_price']);
        add_post_meta($addedProduct[0]->ID,'_sale_price',$_POST['_sale_price']);





        /*Create associated thank You Page*/
        $create_new_thank_you_page = array(
            'post_title'    => 'Thank You',
            'post_type'     => 'page',
            'post_status'   => 'publish',
            'post_content'  => 'Thank You For Subscribing',
            'author'        => $current_created_event->post_author
        );
        wp_insert_post($create_new_thank_you_page);

        $args = array(
            'posts_per_page'   => 1,
            'post_title'    => 'Thank You',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'post_type'        => 'page',
            'post_status'      => 'publish',
            'author'           =>  $current_created_event->post_author
        );

        $addedthankYouPage = get_posts( $args );
        add_post_meta($newEventId,'thank_you_page',$addedthankYouPage[0]->ID);

        $data = array(
            'object_id' => $addedthankYouPage[0]->ID,
            'term_taxonomy_id' => '',
            'term_order' => 0,
        );
        $wpdb->insert('wp_term_relationships', $data);

        /*Create associated Inner Page*/
        $create_new_inner_page = array(
            'post_title'    => $current_created_event->post_title,
            'post_type'     => 'page',
            'post_status'   => 'publish',
            'author'        => $current_created_event->post_author
        );
        wp_insert_post($create_new_inner_page);

        $args = array(
            'posts_per_page'   => 1,
            'post_title'       => $current_created_event->post_title,
            'orderby'          => 'date',
            'order'            => 'DESC',
            'post_type'        => 'page',
            'post_status'      => 'publish',
            'author'           =>  $current_created_event->post_author
        );

        $addedInnerPage = get_posts( $args );
        add_post_meta($newEventId,'inner_page',$addedInnerPage[0]->ID);
        $data = array(
            'object_id' => $addedInnerPage[0]->ID,
            'term_taxonomy_id' => '',
            'term_order' => 0,
        );
        $wpdb->insert('wp_term_relationships', $data);

        if(isset($_POST['eLearning_redirect_url']) && !empty($_POST['eLearning_redirect_url'])){
            header("Location: ".$_POST['eLearning_redirect_url']."");
        }
        else{
            $url = get_site_url().'/'.$current_user_login."/events";
            header("Location: $url");
        }
    }
}

?>