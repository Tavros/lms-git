<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
if (!defined('ABSPATH'))
    exit;

if (!is_user_logged_in()) {
    wp_redirect(home_url(), '302');
}

get_header(eLearning_get_header());

$profile_layout = eLearning_get_customizer('profile_layout');

eLearning_include_template("profile/top$profile_layout.php");
?>
<div class="eLearning-referrals row">
    <?php
    if (current_user_can('administrator')) {
        get_admin_referrals_content();
    }else if (current_user_can('edit_posts')) {
        $sidebar = apply_filters('eLearning_instructor_sidebar', 'instructor_sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($sidebar)) : endif;
    }else {
        $sidebar = apply_filters('eLearning_student_sidebar', 'student_sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($sidebar)) : endif;
    }
    ?>
    <?php do_action('bp_after_dashboard_body'); ?>
</div>	<!-- .eLearning-dashbaord -->
<?php
eLearning_include_template("profile/bottom.php");

get_footer(eLearning_get_footer());
