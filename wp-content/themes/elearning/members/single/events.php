<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
if ( !defined('ABSPATH') )
    exit;
$current_user_login = wp_get_current_user()->user_login;
if ( !is_user_logged_in() ) {
    wp_redirect(home_url(), '302');
}

get_header(eLearning_get_header());
$profile_layout = eLearning_get_customizer( 'profile_layout' );
eLearning_include_template( "profile/top$profile_layout.php" );
?>
<div class="eLearning-events row hs-elearning">
    <?php
    function page_children($parent_id, $limit = -1) {
        return get_posts(array(
            'post_type' => 'page',
            'post_parent' => $parent_id,
            'posts_per_page' => $limit
        ));
    }
    //Delete Event
    if(isset($_REQUEST['delete']) && !empty($_REQUEST['delete']) && $_REQUEST['delete'] == 'true'){
        $id = $_REQUEST['id'];
        $delete = wp_delete_post($id); //True force deletes the post and doesn't send it to the Trash
        return;
    }

    //Get All Post wich are event  
    $event_data = get_posts(array(
      'post_type' => 'course',
      'meta_query' => array(
        array(
          'key' => 'is_event',
          'value' => 'S',
        )
      )
    ));
 
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 right_side">
                <div class="container-fluid">
                    <div class="row right_row">
                        <div class="col-md-4 col-sm-2 col-xs-2"><h1>Events</h1></div>
                        <div class="col-md-8 col-sm-10 col-sm-10 button_part">
                            <a href="<?php echo site_url().'/'.$current_user_login.'/';?>edit-event-new" target="_blank" class="btn btn-primary btn-lg pull-right top-button"><span class="plus">+</span> NEW EVENT</a>
                        </div>
                    </div>                   
                    <div class="row table_wrapper">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <table id="events_data" class="table table-hover info_table" border="0">


                                <thead>
                                    <tr>                        
                                        <th>TITLE</th>
                                        <th>TYPE</th>
                                        <th>STATUS</th>
                                        <th>START DATE</th>
                                        <th>START TIME</th>
                                        <th>REGISTERED</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($event_data as $key=>$event){ 

                                           
                                                $custom_fields = get_post_meta($event->ID);
                                                if (!empty(get_post($custom_fields['inner_page'][0]))){
                                                    $getInnerLink = get_post($custom_fields['inner_page'][0]);
                                                    $getInnerLink = $getInnerLink->guid;

                                                }
                                                else{
                                                    $getInnerLink = $event->slug;
                                                }

                                                if (isset($custom_fields['eLearning_start_date'][0]) && !empty($custom_fields['eLearning_start_date'][0])) {
                                                    $courseStart = $custom_fields['eLearning_start_date'][0];
                                                    $current_date = strtotime("now");
                                                    $start_date = str_replace(array('/', ' ',':','.'), '-', $courseStart);
                                                    $start_date = strtotime($start_date);
                                                    $diff = $start_date - $current_date;
                                                }
                                                else{
                                                    $courseStart = '';
                                                    $current_date = '';
                                                    $start_date = '';
                                                    $diff = '';
                                                }

                                            
                                            ?>

                                    <tr>
                                        <td class="col-md-3"><a href="/<?php echo $current_user_login; ?>/edit-event-new/<?php echo $event->ID ?>" ><?php echo $event->post_title; ?></a></td>
                                        <td class="col-md-1"><?=($diff >= 0) ? "UPCOMING" : "PAST ";?></td>
                                        <td class="col-md-1 hs-active"><?php echo $custom_fields['elearning_events_status'][0]; ?></td>
                                        <td class="col-md-1"><?php echo $courseStart; ?></td>
                                        <td class="col-md-2"><?php echo $custom_fields['eLearning_start_time'][0]; ?></td>
                                        <td class="col-md-1"><?php echo $custom_fields['eLearning_students'][0]; ?></td>
                                        <td class="col-md-1">
                                            <button type="button" class="btn btn-default prev-edit">
                                                <a class="preview_url" target="_blank" href="<?php echo $getInnerLink; ?>">PREVIEW</a>
                                            </button></td>
                                        <td class="col-md-1">
                                            <a href="/wp-admin/post.php?vc_action=vc_inline&post_id=<?php echo $custom_fields['inner_page'][0]; ?>"  target="_blank">
                                                <button type="button"  class="btn btn-default prev-edit">DESIGN</button>
                                            </a></td>
                                        <td class="col-md-1">
                                            <span id="<?php echo $event->ID; ?>" style="cursor:pointer" class="event_inactive inactive">DELETE</span>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div><!--End col-md-12 col-sm-12 col-xs-12-->
                    </div><!--End row table_wrapper-->
                </div><!--End container-fluid-->
             </div><!--End col-xs-12 col-md-12 col-sm-12 col-lg-12 right_side-->
        </div><!--End row-->
    </div><!--End container-fluid-->
</div><!--End eLearning-events row hs-elearning-->
<?php
    eLearning_include_template("profile/bottom.php");
    get_footer(eLearning_get_footer());
?>
<script type="text/javascript">
    jQuery(document).ready(function (){
        
        jQuery('#events_data').DataTable({
            searching: false,
            lengthChange: false,
            paging: false,
            bInfo : false
        });
        jQuery(".dataTable").on("draw.dt", function (e) {                    
            setCustomPagingSigns.call(jQuery(this));
        }).each(function () {
            setCustomPagingSigns.call(jQuery(this)); // initialize
        });
        function setCustomPagingSigns() {
            var wrapper = this.parent();
            wrapper.find("a.previous").text("<");
            wrapper.find("a.next").text(">");           
        }
        //Delete Event
        jQuery(".event_inactive").click(function(){
            var id = jQuery(this).attr('id');
            if(confirm("Are you sure you want to delete the event?")) {
                jQuery.ajax({url: "/<?php echo $current_user_login; ?>/events",data: "id="+id+"&delete=true", success: function(result){
                location.reload();
            }});
            }else{
                location.reload();
            };

        });
    });
</script>