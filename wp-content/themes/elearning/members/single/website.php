<?php

$current_user_login = wp_get_current_user()->user_login;
//Get current user id
$user_id = get_current_user_id();
//Get current user display name by user id
$page_author = get_userdata( $user_id )->display_name;




global $wpdb;

$get_elearninglms_tag_id = "SELECT term_id FROM wp_terms WHERE name = 'elearninglms' AND slug = 'elearninglms'";

$elearninglms_tag_id = $wpdb->get_row($get_elearninglms_tag_id);

$elearninglms_tag_id = $elearninglms_tag_id->term_id;


$pages = new WP_Query( array( 'tag__in' => $elearninglms_tag_id ));


//Delete Page
if(isset($_REQUEST['delete']) && !empty($_REQUEST['delete']) && $_REQUEST['delete'] == 'true'){
    $id = $_REQUEST['id'];
    $delete = wp_delete_post($id); //True force deletes the post and doesn't send it to the Trash
    return "Ok";
}
?>
<div class="eLearning-website row hs-elearning">
    <div class="container-fluid website-menu">
        <ul class="nav navbar-nav list-group">
          <li class="list-group-item current selected"><a href="<?php echo site_url()."/".$current_user_login."/website"?>">PAGES</a> <span class="fm_carret_up"></span></li>
          <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/entries"?>">REGISTRATION FORM</a></li>
<!--          <li class="list-group-item"><a href="--><?php //echo site_url()."/".$current_user_login."/analytics"?><!--">ANALYTICS</a></li>-->
<!--    	  <li class="list-group-item"><a href="--><?php //echo site_url()."/".$current_user_login."/site-settings"?><!--">SITE SETTINGS</a></li>-->
        </ul>
    </div><!--Close container-fluid website-menu-->
    <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 right_side">
        <div class="container-fluid">
            <div class="row right_row">
                <div class="col-md-4 col-sm-2 col-xs-2  "><h1>Pages</h1></div>
                <div class="col-md-8 col-sm-10 col-sm-10 button_part">

                    <?php echo do_shortcode('[create_page_from_front]');?>

                </div>
            </div><!--Close row right_row-->                   
            <div class="row table_wrapper">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <table id="pages_data" class="table table-hover info_table" border="0">
                        <thead>
                            <tr>                        
                              <!--  <th><input type="checkbox"> TITLE</th>      -->
                                <th>Last Updated</th>
                                <th></th>
                                <th></th>
                                <th class="paging"></th> 
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                   			<?php

                            foreach ($pages->posts as $page) {


                                //Edit with visual composer url
                   				$url = admin_url()."post.php?vc_action=vc_inline&post_id=".$page->ID ."&post_type=page"
                   			?>
	                            <tr>                                              
	                                <td class="col-md-2"><!--<input type="checkbox" class="check" style="vertical-align:middle;">--><a href="<?php echo $page->guid ?>"> <?php echo $page->post_title; ?> </a></td>
	                                <td class="col-md-1"><?php echo $page->post_modified; ?></td>                                                
	                                <td class="col-md-1">
	                                <button type="button" class="btn btn-default prev-edit">
	                                	<a class="page_preview" href="<?php echo $page->guid ?>">PREVIEW</a>
	                                </button></td>
	                                <td class="col-md-1">
	                                	<button type="button" class="btn btn-default prev-edit">
	                                	  <a class="page_edit_url" href="<?php echo $url ?>">EDIT</a>
	                                	</button>
	                                </td>
	                                <td class="col-md-1">
                                        <span id="<?php echo $page->ID; ?>" class="page_inactive inactive">DELETE</span>
                                    </td>                        
	                            </tr>
							<?php } ?>
                        </tbody>
                    </table>
                </div><!--Close col-md-12 col-sm-12 col-xs-12-->
            </div><!--Close row table_wrapper-->
        </div><!--Close container-fluid-->
    </div><!--Close right_side-->
</div><!--Close eLearning-website row hs-elearning-->
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>
<script type="text/javascript">
    jQuery(document).ready(function (){
    	console.log(window.location.href);
        jQuery('#pages_data').DataTable({
            searching: false,
            lengthChange: false,
            sDom: '<"top"i>rt<"bottom"flp><"clear">',
            bInfo : false
        });
        jQuery(".dataTable").on("draw.dt", function (e) {                    
            setCustomPagingSigns.call(jQuery(this));
        }).each(function () {
            setCustomPagingSigns.call(jQuery(this)); // initialize
        });

        function setCustomPagingSigns() {
            var wrapper = this.parent();
            wrapper.find("a.previous").text("<");
            wrapper.find("a.next").text(">");           
        }

        //Delete Page
        jQuery(".page_inactive").click(function(){
            var id = jQuery(this).attr('id');
            jQuery.ajax({url: window.location.href ,data: "&id="+id+"&delete=true", success: function(result){
            	if(result && result != ''){
            		setTimeout(location.reload(), 1200);
            	}
            }});
        });
    });
/*
    jQuery(window).load(function (){

         var paging = jQuery('#pages_data_wrapper .top');
         jQuery('#pages_data_wrapper .top').hide();
         jQuery('.paging').append(paging);
         jQuery('#pages_data .top').show();

    }); */
</script>
