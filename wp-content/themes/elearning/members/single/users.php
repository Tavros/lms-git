<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */

if ( !defined('ABSPATH') )
    exit;

if ( !is_user_logged_in() ) {
    wp_redirect( home_url(), '302' );
}
get_header( eLearning_get_header() );
$profile_layout = eLearning_get_customizer( 'profile_layout' );
eLearning_include_template( "profile/top$profile_layout.php" );
$current_user_login = wp_get_current_user()->user_login;
?>
<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12  hs-elearning">
    <div class="container-fluid">
        <div class="row right_row">
            <div class="col-md-6 col-sm-2 col-xs-4"><h1>Registrants</h1></div>

<!--            <div class="col-md-3 col-sm-8 col-xs-4 button_part">-->
<!--                <button class="btn btn-primary btn-lg pull-right top-button">-->
<!--                    <span class="plus">+</span>   -->
<!--                    NEW REGISTRANTS-->
<!--                </button>-->
<!--            </div>-->
            <!--End col-md-3 col-sm-8 col-xs-4 button_part-->
        </div><!--End row right_row-->
        <!--div class="search_bar_background">
            <div class="col-md-10 col-sm-8 col-xs-8 search_bar">
                <div id="imaginary_container">
                    <div class="input-group stylish-input-group">
                        <span class="input-group-addon dataTables_filter">
                            <button type="submit" class="search_icon">
                                <img src="https://lms.first.am/wp-content/themes/elearning/images/icons/search.png" >
                            </button>
                        </span>
                        <input type="text" class="form-control search_button"  placeholder="Search Users..." >
                    </div>
                </div>
            </div>
            <div class="col-md-1 col-sm-2 col-xs-2 search_bar">
                <div class="dropdown">
                    <button class="btn_custom dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filter <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <div class="col-md-1 col-sm-2 col-xs-2">
                <div class="dropdown">
                    <button class="btn_custom dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Sort <i class="fa fa-sort-desc" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div--><!--End search_bar_background-->
        <div class="row table_wrapper">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php
                    echo  do_shortcode('[cfdb-table form="Registrants" hide="Submitted,registrationpassword,membershiplevel,Submitted Login" headers="your-name=First Name,your-lastname=Last Name,registrationemail=Email,your-company=Company,your-title=Co Title" id="registrants_table"  class="table table-hover info_table"]');
//                    echo  do_shortcode('[cfdb-table form="Contact form 11" show="your-name,your-lastname,your-company,your-email,your-subject,Submitted" hide="Page Title,Page URL,Submitted from Page URL,Submitted From,Submitted Login,submit_time" headers="your-name=FirstName,your-lastname=LastName,your-company=Company,your-email=Email,your-subject=Telephone,your-Id,Submitted=Registration Date" id="registrants_table" class="table table-hover info_table"]');
                ?>
            </div><!--End col-md-12 col-sm-12 col-xs-12-->
        </div><!--End row table_wrapper-->
        <div class ="col-md-2 col-sm-2 col-xs-2 export_users_button">
            <form name="exportcsv" action="">
                <input type="hidden" name="unbuffered" value="true">
                <input size="1" name="enc" id="xlsx" value="xlsx" type="hidden"/>
                <input id="exportButton" name="exportButton" type="button" class="btn btn-primary btn-lg" value="Export Table" onclick="exportData(this.form.elements['enc'])">
                <span id="csvdelim_span" style="display:none">
                        <br>
                        <label for="csv_delim">CSV Delimiter</label>
                        <input id="csv_delim" type="text" size="2" value="">
                    </span>
            </form>
        </div><!--End col-md-3 col-sm-2 col-xs-4-->
    </div><!--End container-fluid-->
</div><!--End col-xs-12 col-md-12 col-sm-12 col-lg-12  hs-elearning-->
<div class="eLearning-sessions row">

<?php
    if ( current_user_can('administrator') ) {

    } else if (current_user_can( 'edit_posts') ) {
        $sidebar = apply_filters( 'eLearning_instructor_sidebar', 'instructor_sidebar' );
        if ( !function_exists('dynamic_sidebar' ) || !dynamic_sidebar( $sidebar ) ) : endif;
    } else {
        $sidebar = apply_filters( 'eLearning_student_sidebar', 'student_sidebar' );
        if (!function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar( $sidebar ) ) : endif;
    }

    do_action( 'bp_after_dashboard_body' );
?>

</div>	<!-- eLearning-sessions row -->

<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>

<script type="text/javascript" language="Javascript">

    var showHideExportLinkDelimiter = function() {
        var enc = jQuery('#enc_cntl').val();
        if ( ['CSVUTF8BOM', 'CSVUTF8', 'CSVSJIS'].indexOf(enc) > -1 ) {
            jQuery( '#csvdelim_span' ).show();
        }
        else {
            jQuery( '#csvdelim_span' ).hide();
        }
    };

    function getDelimiterValue() {
        return jQuery( '#csv_delim' ).val();
    }

    function changeDbPage( page ) {
        var newdiv = document.createElement( 'div' );
        newdiv.innerHTML = "<input id='dbpage' name='dbpage' type='hidden' value='" + page + "'>";
        var dispForm = document.forms['displayform'];
        dispForm.appendChild( newdiv );
        dispForm.submit();
    }

    function getSearchFieldValue() {
        var searchVal = '';
        if ( typeof jQuery == 'function' ) {
            try {
                searchVal = jQuery( '#cf2dbtable_filter input' ).val();
            }
            catch (e) {
            }
        }
        return searchVal;
    }

    function exportData( encSelect ) {
        var enc = jQuery('#xlsx').value;
        var checkedValues = [];
        jQuery( 'input[id^="delete_"]:checked' ).each(function() {
            checkedValues.push( this.name );
        });
        checkedValues = checkedValues.join( ',' );

        var url;
        if ( enc == 'GLD' ) {
            alert("You will now be navigated to the builder page where it will generate a function to place in your Google Spreadsheet");
            url = '<?php echo site_url();?>/wp-admin/admin.php?page=CF7DBPluginShortCodeBuilder&form=Contact+form+1&enc=' + enc;
            if (checkedValues) {
                url += "&filter=submit_time[in]" + checkedValues;
            }
            location.href = url;
        }
        else {
            url = '<?php echo site_url();?>/wp-admin/admin-ajax.php?action=cfdb-export&form=Registrants&enc=' + enc;
            var delimiter = getDelimiterValue();
            if ( delimiter ) {
                url += "&delimiter=" + encodeURIComponent( delimiter );
            } else {
                url += "&regionaldelimiter=true";
            }
            var searchVal = getSearchFieldValue();
            if ( searchVal ) {
                url += '&search=' + encodeURIComponent( searchVal );
            }
            if ( checkedValues ) {
                url += "&filter=submit_time[in]" + checkedValues;
            }
            location.href = url;
        }
    }

    jQuery( document ).ready(function() {
        showHideExportLinkDelimiter();
        jQuery('#enc_cntl').change( showHideExportLinkDelimiter );
        jQuery('#registrants_table thead tr, #registrants_table tbody tr ').prepend('<th><input type="checkbox" id="registrants_table_checkbox"  /></th>');

        jQuery('#registrants_table').DataTable({
            language: {
                search: "<img class='search_icon' src='<?php echo site_url();?>/wp-content/themes/elearning/images/icons/search.png'>",
                searchPlaceholder: "Search Users"
            }
        });
        jQuery('.dataTables_filter').addClass('fm-pull-left');
        jQuery('.dataTables_length').addClass('fm-pull-right');
        jQuery('#registrants_table tbody tr').addClass('cursor'); 

        jQuery(".dataTable").on("draw.dt", function (e) {                    
            setCustomPagingSigns.call(jQuery(this));
        }).each(function () {
            setCustomPagingSigns.call(jQuery(this)); // initialize
        });

        function setCustomPagingSigns() {
            var wrapper = this.parent();
            wrapper.find("a.previous").text("<");
            wrapper.find("a.next").text(">");           
        }
        jQuery(document).on("click","#registrants_table tbody tr",function (){
            var temp = jQuery(this).children('td[title=your-email]');
            var email = temp.children('div').html();
            var current_url =  window.location.href;
            var MD5 = function(s){function L(k,d){return(k<<d)|(k>>>(32-d))}function K(G,k){var I,d,F,H,x;F=(G&2147483648);H=(k&2147483648);I=(G&1073741824);d=(k&1073741824);x=(G&1073741823)+(k&1073741823);if(I&d){return(x^2147483648^F^H)}if(I|d){if(x&1073741824){return(x^3221225472^F^H)}else{return(x^1073741824^F^H)}}else{return(x^F^H)}}function r(d,F,k){return(d&F)|((~d)&k)}function q(d,F,k){return(d&k)|(F&(~k))}function p(d,F,k){return(d^F^k)}function n(d,F,k){return(F^(d|(~k)))}function u(G,F,aa,Z,k,H,I){G=K(G,K(K(r(F,aa,Z),k),I));return K(L(G,H),F)}function f(G,F,aa,Z,k,H,I){G=K(G,K(K(q(F,aa,Z),k),I));return K(L(G,H),F)}function D(G,F,aa,Z,k,H,I){G=K(G,K(K(p(F,aa,Z),k),I));return K(L(G,H),F)}function t(G,F,aa,Z,k,H,I){G=K(G,K(K(n(F,aa,Z),k),I));return K(L(G,H),F)}function e(G){var Z;var F=G.length;var x=F+8;var k=(x-(x%64))/64;var I=(k+1)*16;var aa=Array(I-1);var d=0;var H=0;while(H<F){Z=(H-(H%4))/4;d=(H%4)*8;aa[Z]=(aa[Z]| (G.charCodeAt(H)<<d));H++}Z=(H-(H%4))/4;d=(H%4)*8;aa[Z]=aa[Z]|(128<<d);aa[I-2]=F<<3;aa[I-1]=F>>>29;return aa}function B(x){var k="",F="",G,d;for(d=0;d<=3;d++){G=(x>>>(d*8))&255;F="0"+G.toString(16);k=k+F.substr(F.length-2,2)}return k}function J(k){k=k.replace(/rn/g,"n");var d="";for(var F=0;F<k.length;F++){var x=k.charCodeAt(F);if(x<128){d+=String.fromCharCode(x)}else{if((x>127)&&(x<2048)){d+=String.fromCharCode((x>>6)|192);d+=String.fromCharCode((x&63)|128)}else{d+=String.fromCharCode((x>>12)|224);d+=String.fromCharCode(((x>>6)&63)|128);d+=String.fromCharCode((x&63)|128)}}}return d}var C=Array();var P,h,E,v,g,Y,X,W,V;var S=7,Q=12,N=17,M=22;var A=5,z=9,y=14,w=20;var o=4,m=11,l=16,j=23;var U=6,T=10,R=15,O=21;s=J(s);C=e(s);Y=1732584193;X=4023233417;W=2562383102;V=271733878;for(P=0;P<C.length;P+=16){h=Y;E=X;v=W;g=V;Y=u(Y,X,W,V,C[P+0],S,3614090360);V=u(V,Y,X,W,C[P+1],Q,3905402710);W=u(W,V,Y,X,C[P+2],N,606105819);X=u(X,W,V,Y,C[P+3],M,3250441966);Y=u(Y,X,W,V,C[P+4],S,4118548399);V=u(V,Y,X,W,C[P+5],Q,1200080426);W=u(W,V,Y,X,C[P+6],N,2821735955);X=u(X,W,V,Y,C[P+7],M,4249261313);Y=u(Y,X,W,V,C[P+8],S,1770035416);V=u(V,Y,X,W,C[P+9],Q,2336552879);W=u(W,V,Y,X,C[P+10],N,4294925233);X=u(X,W,V,Y,C[P+11],M,2304563134);Y=u(Y,X,W,V,C[P+12],S,1804603682);V=u(V,Y,X,W,C[P+13],Q,4254626195);W=u(W,V,Y,X,C[P+14],N,2792965006);X=u(X,W,V,Y,C[P+15],M,1236535329);Y=f(Y,X,W,V,C[P+1],A,4129170786);V=f(V,Y,X,W,C[P+6],z,3225465664);W=f(W,V,Y,X,C[P+11],y,643717713);X=f(X,W,V,Y,C[P+0],w,3921069994);Y=f(Y,X,W,V,C[P+5],A,3593408605);V=f(V,Y,X,W,C[P+10],z,38016083);W=f(W,V,Y,X,C[P+15],y,3634488961);X=f(X,W,V,Y,C[P+4],w,3889429448);Y=f(Y,X,W,V,C[P+9],A,568446438);V=f(V,Y,X,W,C[P+14],z,3275163606);W=f(W,V,Y,X,C[P+3],y,4107603335);X=f(X,W,V,Y,C[P+8],w,1163531501);Y=f(Y,X,W,V,C[P+13],A,2850285829);V=f(V,Y,X,W,C[P+2],z,4243563512);W=f(W,V,Y,X,C[P+7],y,1735328473);X=f(X,W,V,Y,C[P+12],w,2368359562);Y=D(Y,X,W,V,C[P+5],o,4294588738);V=D(V,Y,X,W,C[P+8],m,2272392833);W=D(W,V,Y,X,C[P+11],l,1839030562);X=D(X,W,V,Y,C[P+14],j,4259657740);Y=D(Y,X,W,V,C[P+1],o,2763975236);V=D(V,Y,X,W,C[P+4],m,1272893353);W=D(W,V,Y,X,C[P+7],l,4139469664);X=D(X,W,V,Y,C[P+10],j,3200236656);Y=D(Y,X,W,V,C[P+13],o,681279174);V=D(V,Y,X,W,C[P+0],m,3936430074);W=D(W,V,Y,X,C[P+3],l,3572445317);X=D(X,W,V,Y,C[P+6],j,76029189);Y=D(Y,X,W,V,C[P+9],o,3654602809);V=D(V,Y,X,W,C[P+12],m,3873151461);W=D(W,V,Y,X,C[P+15],l,530742520);X=D(X,W,V,Y,C[P+2],j,3299628645);Y=t(Y,X,W,V,C[P+0],U,4096336452);V=t(V,Y,X,W,C[P+7],T,1126891415);W=t(W,V,Y,X,C[P+14],R,2878612391);X=t(X,W,V,Y,C[P+5],O,4237533241);Y=t(Y,X,W,V,C[P+12],U,1700485571);V=t(V,Y,X,W,C[P+3],T,2399980690);W=t(W,V,Y,X,C[P+10],R,4293915773);X=t(X,W,V,Y,C[P+1],O,2240044497);Y=t(Y,X,W,V,C[P+8],U,1873313359);V=t(V,Y,X,W,C[P+15],T,4264355552);W=t(W,V,Y,X,C[P+6],R,2734768916);X=t(X,W,V,Y,C[P+13],O,1309151649);Y=t(Y,X,W,V,C[P+4],U,4149444226);V=t(V,Y,X,W,C[P+11],T,3174756917);W=t(W,V,Y,X,C[P+2],R,718787259);X=t(X,W,V,Y,C[P+9],O,3951481745);Y=K(Y,h);X=K(X,E);W=K(W,v);V=K(V,g)}var i=B(Y)+B(X)+B(W)+B(V);return i.toLowerCase()};
            var value = 'test'; 
            email = (MD5(email));
            new_url = current_url.replace('users/','users');
            new_url = new_url.replace('users','/new__registrants/umail='+email);
            location.replace(new_url);
            
           
  
        })
 
    });



</script>