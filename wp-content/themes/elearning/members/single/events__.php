<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
if (!defined('ABSPATH'))
    exit;

if (!is_user_logged_in()) {
    wp_redirect(home_url(), '302');
}

get_header(eLearning_get_header());

$profile_layout = eLearning_get_customizer('profile_layout');

eLearning_include_template("profile/top$profile_layout.php");
?>
<div class="eLearning-events row">
    <?php
    /**
     * BuddyPress - Users Groups
     *
     * @package BuddyPress
     * @subpackage bp-default
     */
    if (!defined('ABSPATH'))
        exit;
    ?>

    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
        <div class="title col-sm-3">Events</div>
        <ul>
            <?php
            //if (bp_is_my_profile()) {
            bp_get_options_event_nav();
            // }

            do_action('bp_course_get_options_sub_nav');
            ?>
        </ul>
    </div><!-- .item-list-tabs -->

    <?php
    do_action('eLearning_after_single_item_list_tabs');
    do_action('bp_before_member_course_content');

    if (bp_is_current_action(BP_EVENT_RESULTS_SLUG)) :

        locate_template(array('members/single/events/results.php'), true);

    else:
        if (bp_is_current_action(BP_EVENT_STATS_SLUG)) :

            locate_template(array('members/single/events/stats.php'), true);

        else:
            ?>
            <div class="course mycourse">
                <?php
                if (bp_is_current_action('instructor-events')):
                    locate_template(array('events/instructor-events.php'), true);
                else:
                    ?>
                    <?php
                    locate_template(array('events/my-events.php'), true);
                endif;
                ?>
            </div>
        <?php
        endif;
    endif;
    ?>
    <?php do_action('bp_after_member_course_content'); ?>

</div>	<!-- .eLearning-dashbaord -->
<?php
eLearning_include_template("profile/bottom.php");

get_footer(eLearning_get_footer());
