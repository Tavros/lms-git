<?php $current_user_login = wp_get_current_user()->user_login; ?>
<div class="eLearning-website row hs-elearning">
    <div class="container-fluid website-menu">
        <ul class="nav navbar-nav list-group">
            <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/website"?>">PAGES</a></li>
            <li class="list-group-item current selected "><a href="<?php echo site_url()."/".$current_user_login."/registration-form"?>">REGISTRATION FORM</a> <span class="asadadada" ></span></li>
            <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/analytics"?>">ANALYTICS</a></li>
            <li class="list-group-item "><a href="<?php echo site_url()."/".$current_user_login."/site-settings"?>">SITE SETTINGS</a></li>
        </ul>
    </div><!--End container-fluid website-menu-->
    <div class="Form_Customization_menu">
        <ul class="Form_Customization_nav">
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/entries"?>">ENTRIES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_builder"?>">FORM BOILDER</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form-messages"?>">MESSAGES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_responses"?>"><i class="fa fa-caret-up" aria-hidden="true"></i>RESPONSES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/Form_Customization"?>">COSTOMIZATION</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/Analitics"?>">CONDITIONAL LOGIC</a></li>
        </ul>
    </div><!--End Form_Customization_menu-->
    <div class="formResponses">
        <form>
    	   <div id= "responses" class="col-lg-10">
                <h1>Responses</h1>
                <div class="input">
                    <p>Serder's message was sent successfully</p>
                    <input type="text" placeholder="Thank you for your message. it has been sent.">
                </div>
            	<div class="input">
                    <p>Serder's message failed to send </p>
                    <input type="text" placeholder="There was an error trying to send your message. please try again later. ">
                </div>
            	<div class="input">
                    <p>Validation errors occurred</p>
                    <input type="text" placeholder="One or more fields have en error. please check and try again">
                </div>
            	<div class="input">
                    <p>Submission was referred to as spam</p>
                    <input type="text" placeholder="There was an error trying to send your message please try again later">
                </div>
            	<div class="input">
                    <p>There are terms that the sender must accept </p>
                    <input type="text" placeholder="You must accept the terms and conditions before sending your message. ">
                </div>
            	<div class="input">
                    <p>There is a field that the sender must fill in </p>
                    <input type="text" placeholder="The field is required.  ">
                </div>
            	<div class="input">
                    <p>There is a field with input is longer than the maximum allowed lenght </p>
                    <input type="text" placeholder="The field is too long. ">
                </div>
            	<div class="input">
                    <p>There is a field with input is shorter than the minimum allowed lenght</p>
                    <input type="text" placeholder="The field is too short. ">
                </div>
            	<div class="input">
                    <p>Date format that the sender entered is invalid </p>
                    <input type="text" placeholder="The date format is incorrect.  ">
                </div>
            	<div class="input">
                    <p>Date is earlier than minimum limit </p>
                    <input type="text" placeholder="The date is before the earliest one allowed.">
                </div>
            	<div class="input">
                    <p>Date is later than maximum limit </p>
                    <input type="text" placeholder="The date is after the latest one allowed. ">
                </div>
            	<div class="input">
                    <p>Uploaded file is not allowed for file type</p>
                    <input type="text" placeholder="You are not allowed to upload files of this type.">
                </div>
            	<div class="input">
                    <p>Uploaded file is too large </p>
                    <input type="text" placeholder="The file is too big. ">
                </div>
            	<div class="input">
                    <p>Uploading a file fails for PHP error </p>
                    <input type="text" placeholder="There was an error uploading the file. ">
                </div>
            	<div class="input">
                    <p>Number format that the sender entered is invalid</p>
                    <input type="text" placeholder="The number format is invalid ">
                </div>
            	<div class="input">
                    <p>Number is smaller than minimum limit </p>
                    <input type="text" placeholder="The number is smaller than the minimum allowed.">
                </div>  

                <div class="input">
                    <p>Number is larger than maximum limit </p>
                    <input type="text" placeholder="The number is larger than the maximum allowed.">
                </div>

              	<div class="input">
                    <p>Sender doesn't enter the correct answer to the quiz</p>
                    <input type="text" placeholder="The answer to the quiz is incoorect. ">
                </div>
            	<div class="input">
                    <p>Email address that the sender entered is invalid</p>
                    <input type="text" placeholder="The e-mail address entered is invalid.">
                </div>
            	<div class="input">
                    <p>URL that the sender entered is invalid</p>
                    <input type="text" placeholder="The URL is invalid. ">
                </div>
            	<div class="input">
                    <p>The telephone number that the sender entered is invalid</p>
                    <input type="text" placeholder="The telephone number is invalid">
                </div>
             
                <button class="hButton">SAVE</button> 
            </div>
            <div class="col-lg-2"></div>
        </form>
    </div><!--End formResponses-->
</div><!--End eLearning-website row hs-elearning-->
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>