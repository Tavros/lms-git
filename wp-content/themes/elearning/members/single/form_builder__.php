<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

 require_once $_SERVER['DOCUMENT_ROOT'] . 'wp-content/plugins/contact-form-7/admin/admin.php';
 die;
 $current_user_login = wp_get_current_user()->user_login;?>
<div class="container_Form_Customization">
	<div class="container-fluid website-menu fm_right_grid Form_Customization_top">
		<ul class="nav navbar-nav list-group">
			<li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/website"?>">PAGES</a></li>
			<li class="list-group-item current selected"><a href="<?php echo site_url()."/".$current_user_login."/form_builder"?>">REGISTRATION</a> <span class="fm_carret_up" ></span></li>
			<li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/Analitics"?>">ANALYTICS</a></li>
			<li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/Analytics"?>">SITE  SETTINGS</a></li>
		</ul>
	</div><!--End container-fluid website-menu fm_right_grid Form_Customization_top-->
	<div class="Form_Customization_menu">
		<ul class="Form_Customization_nav">
			<li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/entries"?>">ENTRIES</a></li>
			<li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_builder"?>">FORM BUILDER</a> <i class="fa fa-caret-up" aria-hidden="true"></i><li>
			<li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form-messages"?>">MESSAGES</a></li>
			<li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_responses"?>">RESPONSES</a></li>
			<li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/Analitics"?>">CUSTOMIZATION</a></li>
		</ul>
	</div><!--End Form_Customization_menu-->
</div><!--End container_Form_Customization-->
<div class="contact-form-editor-panel ui-tabs-panel ui-widget-content ui-corner-bottom" id="form-panel" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
	<div class="config-error"></div>
	<h2 style="color:#5d5d5d">Form Builder</h2>
	<span id="tag-generator-list" class="form_builder_contact" >
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-text" class="contact_button" title="Form-tag Generator: text">text</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-email" class="contact_button" title="Form-tag Generator: email">email</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-url" class="contact_button" title="Form-tag Generator: URL">URL</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-tel" class="contact_button" title="Form-tag Generator: tel">tel</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-number" class="contact_button" title="Form-tag Generator: number">number</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-date" class="contact_button" title="Form-tag Generator: date">date</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-textarea" class="contact_button" title="Form-tag Generator: text area">text area</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-menu" class="contact_button" title="Form-tag Generator: drop-down menu">drop-down menu</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-checkbox" class="contact_button" title="Form-tag Generator: checkboxes">checkboxes</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-radio" class="contact_button" title="Form-tag Generator: radio buttons">radio buttons</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-acceptance" class="contact_button" title="Form-tag Generator: acceptance">acceptance</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-quiz" class="contact_button" title="Form-tag Generator: quiz">quiz</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-recaptcha" class="contact_button" title="Form-tag Generator: reCAPTCHA">reCAPTCHA</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-file" class="contact_button" title="Form-tag Generator: file">file</a>
		<a href="#TB_inline?width=900&amp;height=500&amp;inlineId=tag-generator-panel-submit" class="contact_button" title="Form-tag Generator: submit">submit</a>
	</span><!--End form_builder_contact-->
	<div class="fm_add_tab">
		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="#contact_01" data-toggle="tab">Tab 1</a><span>x</span>
			</li>
			<li><a href="#contact_02" data-toggle="tab">Tab 2</a>  <span> x </span>
			</li>
			<li><a href="#contact_03" data-toggle="tab">Tab 3</a>  <span> x </span>
			</li>
			<li><a href="#" class="add-contact fm_add_contact">+</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="contact_01">
				<textarea id="wpcf7-form" name="wpcf7-form" cols="100" rows="24" class="large-text code" data-config-field="form.body">
					&lt;label&gt; First Name (required)
					[text* your-name] &lt;/label&gt;

					&lt;label&gt; Last Name (required)
					[text* your-name] &lt;/label&gt;

					&lt;label&gt; Email (required)
					[email* your-email] &lt;/label&gt;

					&lt;label&gt; Company
					[text your-subject] &lt;/label&gt;

					&lt;label&gt; Home Phone
					[text your-subject] &lt;/label&gt;

					[submit "Register"]
				</textarea>
			</div><!--End tab-pane active-->
			<div class="tab-pane" id="contact_02">Tab 2</div>
			<div class="tab-pane" id="contact_03">Tab 3</div>
		</div><!--End tab-content-->
	</div><!--End fm_add_tab-->
</div><!--End contact-form-editor-panel ui-tabs-panel ui-widget-content ui-corner-bottom-->
<div class="fm_multi_options">
	<span class="multi_options">Multi options</span>
	<div class='multi_options_left'>
		<form action="" method="">
			<input type="checkbox" name="" value="" checked>Show navigation links<br>
			<input type="checkbox" name="" value="" checked>Show progress bar<br>
			<input type="checkbox" name="" value="" checked>Show pagination<br>
			<input type="submit" value="SAVE">
		</form>
	</div><!--End multi_options_left-->
	<div class='multi_options_right'>
		<form action="" method="">
			<label for="">Start Tab Text</label><Br>
			<input type="text" placeholder="Start text here..."><Br>
			<label for="">End Tab Text</label><Br>
			<input type="text" placeholder="End text here..."><Br>
			<label for="">Thank You Tab</label><Br>
			<textarea   placeholder="Thank you Contact 7">Thank you Contact 7	</textarea>
		</form>
	</div><!--End multi_options_right-->
</div><!--End fm_multi_options-->
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>
<script>
	jQuery(".nav-tabs").on("click", "a", function (e) {
		e.preventDefault();
		if (!jQuery(this).hasClass('add-contact')) {
			jQuery(this).tab('show');
		}
	})
	.on("click", "span", function () {
		var anchor = jQuery(this).siblings('a');
		jQuery(anchor.attr('href')).remove();
		jQuery(this).parent().remove();
		jQuery(".nav-tabs li").children('a').first().click();
	});

	jQuery('.add-contact').click(function (e) {
		e.preventDefault();
	    var id = jQuery(".nav-tabs").children().length; //think about it ;)
		var tabId = 'contact_' + id;
		jQuery(this).closest('li').before('<li><a href="#contact_' + id + '">Tab '+ id + '</a> <span> x </span></li>');
		jQuery('.tab-content').append('<div class="tab-pane add_tabe_pane" id="' + tabId + '">TAB' + id + '</div>');
		jQuery('.nav-tabs li:nth-child(' + id + ') a').click();
		jQuery(this).closest(li).removeClass('active');
		jQuery(this).css('background','#73a54e');
	});
</script>