
<?php $current_user_login = wp_get_current_user()->user_login; ?><div class="container_Form_Customization">
  <div class="container-fluid website-menu fm_right_grid Form_Customization_top">
   <ul class="nav navbar-nav list-group">
     <li class="list-group-item "><a href="<?php echo site_url()."/".$current_user_login."/website"?>">PAGES</a></li>
     <li class="list-group-item current selected"><a href="<?php echo site_url()."/".$current_user_login."/registration-form"?>">REGISTRATION FORM</a></li>
     <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/Analitics"?>">ANALYTICS</a></li>
     <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/Analytics"?>">SITE  SETTINGS</a></li>
   </ul>
 </div><!--End container-fluid website-menu fm_right_grid Form_Customization_top-->
 <div class="Form_Customization_menu">
   <ul class="Form_Customization_nav">
     <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/entries"?>"> ENTRIES</a></li>
     <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_builder"?>"><i class="fa fa-caret-up  Customization_top_i" aria-hidden="true"></i>FORM BUILDER</a></li>
     <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form-messages"?>">MESSAGES</a></li>
     <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_responses"?>">RESPONSES</a></li>
     <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/Form_Customization"?>">CUSTOMIZATION</a> <i class="fa fa-caret-up" aria-hidden="true"></i></li>
   </ul>
 </div><!--End Form_Customization_menu-->
 <div  class="container">
   <h1>Customized Settings</h1>
   <h3>Additional  CSS</h3>
   <p>Write additional CSS to be applied to this form</p>
   <textarea rows="9" cols="70"></textarea>
   <h3>Additional  Javascript</h3>
   <p>Write additional Javascript to be applied to this form</p>
   <textarea rows="9" cols="70"></textarea>
   <button class="btn btn-primary">SAVE</button>
 </div><!--End container-->
</div><!--End container_Form_Customization-->
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>