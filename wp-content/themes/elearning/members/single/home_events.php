<?php
/**
 * BuddyPress - Users Home
 *
 * @package BuddyPress
 * @subpackage bp-default
 */
if (!defined('ABSPATH'))
    exit;

do_action('eLearning_before_member_profile');

get_header(eLearning_get_header());

$profile_layout = eLearning_get_customizer('profile_layout');
eLearning_include_template("profile/top$profile_layout.php");
?>
<div id="item-body">
    <?php do_action('template_notices'); ?>

    <?php
    do_action('bp_before_member_body');


    if (bp_is_current_component(BP_EVENT_SLUG)) :
        locate_template(array('members/single/events.php'), true);
    endif;

    do_action('bp_after_member_body');
    ?>

</div><!-- #item-body -->

<?php do_action('bp_after_member_home_content'); ?>
<?php
eLearning_include_template("profile/bottom.php");

get_footer(eLearning_get_footer());

do_action('eLearning_after_member_profile');
