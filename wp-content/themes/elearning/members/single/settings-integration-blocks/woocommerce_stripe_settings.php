<table class="form-table">		<tbody><tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_stripe_enabled">Enable/Disable</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_stripe_enabled">
                    <input class="" type="checkbox" name="woocommerce_stripe_enabled" id="woocommerce_stripe_enabled" style=""  <?php if($enabled == "yes"):?>checked="checked"<?endif;?>> Enable Stripe</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_title">Title</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_stripe_title" id="woocommerce_stripe_title" style="" value="<?php echo $title ;?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_description">Description</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_stripe_description" id="woocommerce_stripe_description" style="" value="<?php echo $description;?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_testmode">Test mode</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_stripe_testmode">
                    <input class="" type="checkbox" name="woocommerce_stripe_testmode" id="woocommerce_stripe_testmode" style="" checked="checked"> Enable Test Mode</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_test_secret_key">Test Secret Key</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_stripe_test_secret_key" id="woocommerce_stripe_test_secret_key" style="" value="<? echo $test_secret_key;?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_test_publishable_key">Test Publishable Key</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_stripe_test_publishable_key" id="woocommerce_stripe_test_publishable_key" style="" value="<?php echo $test_publishable_key;?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top" style="display: none;">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_secret_key">Live Secret Key</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_stripe_secret_key" id="woocommerce_stripe_secret_key" style="" value="<?php echo $secret_key?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top" style="display: none;">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_publishable_key">Live Publishable Key</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_stripe_publishable_key" id="woocommerce_stripe_publishable_key" style="" value="<?php echo $publishable_key?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_statement_descriptor">Statement Descriptor</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_stripe_statement_descriptor" id="woocommerce_stripe_statement_descriptor" style="" value="<?php echo $statement_descriptor;?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_capture">Capture</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_stripe_capture">
                    <input class="" type="checkbox" name="woocommerce_stripe_capture" id="woocommerce_stripe_capture" style=""  <?php if($capture == "yes"):?>checked="checked"<?endif;?>> Capture charge immediately</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_stripe_checkout">Stripe Checkout</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_stripe_stripe_checkout">
                    <input class="" type="checkbox" name="woocommerce_stripe_stripe_checkout" id="woocommerce_stripe_stripe_checkout" style=""  <?php if($stripe_checkout == "yes"):?>checked="checked"<?endif;?>> Enable Stripe Checkout</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top" style="display: none;">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_stripe_checkout_locale">Stripe Checkout locale</label>
        </th>
        <td class="forminp">
            <fieldset>

                <select class="select wc-enhanced-select select2-hidden-accessible enhanced" name="woocommerce_stripe_stripe_checkout_locale" id="woocommerce_stripe_stripe_checkout_locale" style="" tabindex="-1" aria-hidden="true">
                    <option value="auto">Auto</option>
                    <option value="zh">Simplified Chinese</option>
                    <option value="da">Danish</option>
                    <option value="nl">Dutch</option>
                    <option value="en" selected="selected">English</option>
                    <option value="fi">Finnish</option>
                    <option value="fr">French</option>
                    <option value="de">German</option>
                    <option value="it">Italian</option>
                    <option value="ja">Japanese</option>
                    <option value="no">Norwegian</option>
                    <option value="es">Spanish</option>
                    <option value="sv">Swedish</option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 142px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-woocommerce_stripe_stripe_checkout_locale-container"><span class="select2-selection__rendered" id="select2-woocommerce_stripe_stripe_checkout_locale-container" title="English">English</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
            </fieldset>
        </td>
    </tr>
    <tr valign="top" style="display: none;">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_stripe_bitcoin">Bitcoin Currency</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_stripe_stripe_bitcoin">
                    <input class="" type="checkbox" name="woocommerce_stripe_stripe_bitcoin" id="woocommerce_stripe_stripe_bitcoin" style="" > Enable Bitcoin Currency</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top" style="display: none;">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_stripe_checkout_image">Stripe Checkout Image</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_stripe_stripe_checkout_image" id="woocommerce_stripe_stripe_checkout_image" style="" value="" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_request_payment_api">Payment Request API</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_stripe_request_payment_api">
                    <input class="" type="checkbox" name="woocommerce_stripe_request_payment_api" id="woocommerce_stripe_request_payment_api" style=""  <?php if($stripe_checkout == "yes"):?>checked="checked"<?endif;?>> Enable Payment Request API</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_apple_pay">Apple Pay</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_stripe_apple_pay">
                    <input class="" type="checkbox" name="woocommerce_stripe_apple_pay" id="woocommerce_stripe_apple_pay" style=""  <?php if($apple_pay == "yes"):?>checked="checked"<?endif;?>> Enable Apple Pay. <br>By using Apple Pay, you agree to <a href="https://stripe.com/apple-pay/legal" target="_blank">Stripe</a> and <a href="https://developer.apple.com/apple-pay/acceptable-use-guidelines-for-websites/" target="_blank">Apple</a>'s terms of service.</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_apple_pay_button">Apple Pay Button Style</label>
        </th>
        <td class="forminp">
            <fieldset>

                <select class="select " name="woocommerce_stripe_apple_pay_button" id="woocommerce_stripe_apple_pay_button" style="">
                    <option value="black" <?php if($apple_pay_button == "black"):?>selected="selected"<?php endif;?>>Black</option>
                    <option value="white" <?php if($apple_pay_button == "white"):?>selected="selected"<?php endif;?>>White</option>
                </select>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_stripe_apple_pay_button_lang"><?php echo $ss;?></label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_stripe_apple_pay_button_lang" id="woocommerce_stripe_apple_pay_button_lang" style="" value="en" placeholder="">
                <p class="description">Enter the 2 letter ISO code for the language you would like your Apple Pay Button to display in. Reference available ISO codes here <code>http://www.w3schools.com/tags/ref_language_codes.asp</code></p>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_saved_cards">Saved Cards</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_stripe_saved_cards">
                    <input class="" type="checkbox" name="woocommerce_stripe_saved_cards" id="woocommerce_stripe_saved_cards" style="" > Enable Payment via Saved Cards</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_stripe_logging">Logging</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_stripe_logging">
                    <input class="" type="checkbox" name="woocommerce_stripe_logging" id="woocommerce_stripe_logging" style=""  <?php if($logging == "yes"):?>checked="checked"<?endif;?>> Log debug messages</label><br>
            </fieldset>
        </td>
    </tr>

    </tbody></table>
<input class="strip_payment dfsf" style="float: left;" type="submit">
<img class="strip_preloader" style="display: none;float: left;width:39px;margin-left: 13px;" src="<?php echo site_url();?>/wp-content/themes/elearning/images/preloader.gif">
<script>
    jQuery(document).on("click",".strip_payment",function(){
        var data = {
            serialization_key        :   "woocommerce_stripe_settings",
            func                     :   "fn_update_payment_settings_custom",
            enabled                  :   (jQuery("input[name=woocommerce_stripe_enabled]:checked").val()=="on")?"yes":"no",
            title                    :   jQuery("input[name=woocommerce_stripe_title]").val(),
            description              :   jQuery("input[name=woocommerce_stripe_description]").val(),
            testmode                 :   (jQuery("input[name=woocommerce_stripe_testmode]:checked").val()=="on")?"yes":"no",
            test_secret_key          :   jQuery("input[name=woocommerce_stripe_test_secret_key]").val(),
            test_publishable_key     :   jQuery("input[name=woocommerce_stripe_test_publishable_key]").val(),
            secret_key               :   jQuery("input[name=woocommerce_stripe_secret_key]").val(),
            publishable_key          :   jQuery("input[name=woocommerce_stripe_publishable_key]").val(),
            statement_descriptor     :   jQuery("input[name=woocommerce_stripe_statement_descriptor]").val(),
            capture                  :   jQuery("input[name=woocommerce_stripe_capture]:checked").val(),
            stripe_checkout          :   (jQuery("input[name=woocommerce_stripe_stripe_checkout]:checked").val()=="on")?"yes":"no",
            stripe_checkout_locale   :   jQuery("input[name=woocommerce_stripe_stripe_checkout_locale]").val(),
            stripe_bitcoin           :   jQuery("input[name=woocommerce_stripe_stripe_bitcoin]").val(),
            stripe_checkout_image    :   jQuery("input[name=woocommerce_stripe_stripe_checkout_image]").val(),
            request_payment_api      :   (jQuery("input[name=woocommerce_stripe_request_payment_api]:checked").val()=="on")?"yes":"no",
            apple_pay                :   (jQuery("input[name=woocommerce_stripe_apple_pay]:checked").val()=="on")?"yes":"no",
            apple_pay_button         :   jQuery("select[name=woocommerce_stripe_apple_pay_button]").val(),
            apple_pay_button_lang    :   jQuery("input[name=woocommerce_stripe_apple_pay_button_lang]").val(),
            saved_cards              :   (jQuery("input[name=woocommerce_stripe_saved_cards]:checked").val()=="on")?"yes":"no",
            logging                  :   (jQuery("input[name=woocommerce_stripe_logging]:checked").val()=="on")?"yes":"no"

        }
        console.log(data)
        jQuery(".strip_preloader").css("display","block")
        jQuery.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : "<?php echo site_url();?>/wp-content/themes/elearning/members/single/settings-integrations.php", // the url where we want to POST
            data        : data, // our data object
            dataType    : 'json' // what type of data do we expect back from the server

        }).success(function(data) {
            console.log(data)
//            //var resp = jQuery.parseJSON(data)
//            jQuery("#"+data.block +" .plugin_settings_block").html(data.data)
//            console.log(data.block)
//            console.log(data.data)
//            jQuery("#"+data.block).dialog({ width: 760 })
            //jQuery(".ui-dialog-titlebar-close").trigger("click")
            jQuery(".strip_preloader").css("display","none")
        })
    })
</script>
