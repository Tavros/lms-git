<h2>PayPal </h2>
<p>PayPal Standard sends customers to PayPal to enter their payment information. PayPal IPN requires fsockopen/cURL support to update order statuses after payment. Check the <a href="<?php echo site_url();?>/wp-admin/admin.php?page=wc-status">system status</a> page for more details.</p>
<table class="form-table">		<tbody><tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypal_enabled">Enable/Disable</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_paypal_enabled">
                    <input class="" type="checkbox" name="woocommerce_paypal_enabled" id="woocommerce_paypal_enabled" style=""  <?php if($enabled == "yes"):?>checked<?endif;?>> Enable PayPal Standard</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_title">Title</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypal_title" id="woocommerce_paypal_title" style="" value="<?php echo $title?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_description">Description</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypal_description" id="woocommerce_paypal_description" style="" value="<?php echo $description?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_email">PayPal email</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="email" name="woocommerce_paypal_email" id="woocommerce_paypal_email" style="" value="<?php echo $email;?>" placeholder="you@youremail.com">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypal_testmode">PayPal sandbox</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_paypal_testmode">
                    <input class="" type="checkbox" name="woocommerce_paypal_testmode" id="woocommerce_paypal_testmode" style=""  <?php if($testmode == "yes"):?>checked<?endif;?>> Enable PayPal sandbox</label><br>
                <p class="description">PayPal sandbox can be used to test payments. Sign up for a <a href="https://developer.paypal.com/">developer account</a>.</p>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypal_debug">Debug log</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_paypal_debug">
                    <input class="" type="checkbox" name="woocommerce_paypal_debug" id="woocommerce_paypal_debug" style=""  <?php if($debug == "yes"):?>checked<?endif;?>> Enable logging</label><br>
                <p class="description">Log PayPal events, such as IPN requests, inside <code>/home/first/presentation.first.am/wp-content/uploads/wc-logs/paypal-e833b0b1b17bbd0a6a5d2c27455acf5a.log</code></p>
            </fieldset>
        </td>
    </tr>
    </tbody></table>
<h3 class="wc-settings-sub-title " id="woocommerce_paypal_advanced">Advanced options</h3>
<table class="form-table">
    <tbody><tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_receiver_email">Receiver email</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="email" name="woocommerce_paypal_receiver_email" id="woocommerce_paypal_receiver_email" style="" value="<?php echo $receiver_email;?>" placeholder="you@youremail.com">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_identity_token">PayPal identity token</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypal_identity_token" id="woocommerce_paypal_identity_token" style="" value="<?php echo $identity_token;?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_invoice_prefix">Invoice prefix</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypal_invoice_prefix" id="woocommerce_paypal_invoice_prefix" style="" value="<?php echo $invoice_prefix;?>" placeholder="">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypal_send_shipping">Shipping details</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_paypal_send_shipping">
                    <input class="" type="checkbox" name="woocommerce_paypal_send_shipping" id="woocommerce_paypal_send_shipping" style=""  <?php if($send_shipping == "yes"):?>checked<?endif;?>> Send shipping details to PayPal instead of billing.</label><br>
                <p class="description">PayPal allows us to send one address. If you are using PayPal for shipping labels you may prefer to send the shipping address rather than billing.</p>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypal_address_override">Address override</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_paypal_address_override">
                    <input class="" type="checkbox" name="woocommerce_paypal_address_override" id="woocommerce_paypal_address_override" style=""  <?php if($address_override == "yes"):?>checked<?endif;?>> Enable "address_override" to prevent address information from being changed.</label><br>
                <p class="description">PayPal verifies addresses therefore this setting can cause errors (we recommend keeping it disabled).</p>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_paymentaction">Payment action</label>
        </th>
        <td class="forminp">
            <fieldset>

                <select class="select wc-enhanced-select select2-hidden-accessible enhanced" name="woocommerce_paypal_paymentaction" id="woocommerce_paypal_paymentaction" style="" tabindex="-1" aria-hidden="true">
                    <option value="sale" selected="selected">Capture</option>
                    <option value="authorization">Authorize</option>
                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 88px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-woocommerce_paypal_paymentaction-container"><span class="select2-selection__rendered" id="select2-woocommerce_paypal_paymentaction-container" title="Capture">Capture</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_page_style">Page style</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypal_page_style" id="woocommerce_paypal_page_style" style="" value="<?php echo $page_style?>" placeholder="Optional">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_image_url">Image url</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypal_image_url" id="woocommerce_paypal_image_url" style="" value="<?php echo $image_url;?>" placeholder="Optional">
            </fieldset>
        </td>
    </tr>
    </tbody></table>
<h3 class="wc-settings-sub-title " id="woocommerce_paypal_api_details">API credentials</h3>
<p>Enter your PayPal API credentials to process refunds via PayPal. Learn how to access your <a href="https://developer.paypal.com/webapps/developer/docs/classic/api/apiCredentials/#creating-an-api-signature">PayPal API Credentials</a>.</p>
<table class="form-table">
    <tbody><tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_api_username">API username</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypal_api_username" id="woocommerce_paypal_api_username" style="" value="<?php echo $api_username;?>" placeholder="Optional">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_api_password">API password</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="password" name="woocommerce_paypal_api_password" id="woocommerce_paypal_api_password" style="" value="<?php echo $api_password;?>" placeholder="Optional">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <span class="woocommerce-help-tip"></span>				<label for="woocommerce_paypal_api_signature">API signature</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypal_api_signature" id="woocommerce_paypal_api_signature" style="" value="<?php echo $api_signature;?>" placeholder="Optional">
            </fieldset>
        </td>
    </tr>
    </tbody></table>
<input class="paypal_payment_custom_submit " style="float: left;" type="submit" value="Save">

<img class="strip_preloader" style="display: none;float: left;width:39px;margin-left: 13px;" src="<?php echo site_url();?>/wp-content/themes/elearning/images/preloader.gif">
<script>
    jQuery(document).ready(function () {
        jQuery(".paypal_payment_custom_submit").click(function(){
            var data = {
                serialization_key        :   "woocommerce_paypal_settings",
                func                     :   "fn_update_payment_settings_custom",
                enabled                  :   (jQuery("input[name=woocommerce_paypal_enabled]:checked").val()=="on")?"yes":"no",
                title                    :   jQuery("input[name=woocommerce_paypal_title]").val(),
                description              :   jQuery("input[name=woocommerce_paypal_description]").val(),
                email                    :   jQuery("input[name=woocommerce_paypal_email]").val(),
                testmode                 :   (jQuery("input[name=woocommerce_paypal_testmode]:checked").val()=="on")?"yes":"no",
                debug                    :   (jQuery("input[name=woocommerce_paypal_debug]:checked").val()=="on")?"yes":"no",
                receiver_email           :   jQuery("input[name=woocommerce_paypal_receiver_email]").val(),
                identity_token           :   jQuery("input[name=woocommerce_paypal_identity_token]").val(),
                invoice_prefix           :   jQuery("input[name=woocommerce_paypal_invoice_prefix]").val(),
                send_shipping            :   (jQuery("input[name=woocommerce_paypal_send_shipping]:checked").val()=="on")?"yes":"no",
                address_override         :   (jQuery("input[name=woocommerce_paypal_address_override]:checked").val()=="on")?"yes":"no",
                paymentaction            :   (jQuery("input[name=woocommerce_paypal_paymentaction]:checked").val()=="on")?"yes":"no",
                page_style               :   jQuery("input[name=woocommerce_paypal_page_style]").val(),
                image_url                :   jQuery("input[name=woocommerce_paypal_image_url]").val(),
                api_username             :   jQuery("input[name=woocommerce_paypal_api_username]").val(),
                api_password             :   jQuery("input[name=woocommerce_paypal_api_password]").val(),
                api_signature            :   jQuery("input[name=woocommerce_paypal_api_signature]").val(),
                close_dialog             :   (jQuery(this).hasClass("close-dialog"))?"yes":"no"

            }
            console.log(data)
            jQuery(".strip_preloader").css("display","block")
            jQuery.ajax({
                type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url         : "<?php echo site_url();?>/wp-content/themes/elearning/members/single/settings-integrations.php", // the url where we want to POST
                data        : data // our data object
               // what type of data do we expect back from the server

            }).success(function(data) {
                console.log(data)
                var resp = jQuery.parseJSON(data)
                console.log(resp)
                //var resp = jQuery.parseJSON(data)
//            jQuery("#"+data.block +" .plugin_settings_block").html(data.data)
//            console.log(data.block)
//            console.log(data.data)
                if(data.close_dialog == "yes")
                {
                    //jQuery("#"+data.dialog).hide()

                }
                //jQuery(".ui-dialog-titlebar-close").trigger("click")
                jQuery(".strip_preloader").css("display","none")
            })
        })
    })

</script>