<h3>PayPal Pro</h3>
<p>Allows Credit Card Payments via the PayPal Pro gateway.</p>
<table class="form-table">
    <tbody><tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypalpro_enabled">Enable/Disable</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_paypalpro_enabled">
                    <input class="" type="checkbox" name="woocommerce_paypalpro_enabled" id="woocommerce_paypalpro_enabled" style=""  <?php if($enabled == "yes"):?>checked<?endif;?>> Enable PayPal Pro Gateway</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypalpro_debug">Sandbox Mode</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_paypalpro_debug">
                    <input class="" type="checkbox" name="woocommerce_paypalpro_debug" id="woocommerce_paypalpro_debug" style=""  <?php if($debug == "yes"):?>checked<?endif;?>> Enable Sandbox Mode</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypalpro_title">Title</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypalpro_title" id="woocommerce_paypalpro_title" style="" value="<?php $title;?>" placeholder="">
                <p class="description">The title for this checkout option.</p>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypalpro_securitycodehint">Show CVV Hint</label>
        </th>
        <td class="forminp">
            <fieldset>

                <label for="woocommerce_paypalpro_securitycodehint">
                    <input class="" type="checkbox" name="woocommerce_paypalpro_securitycodehint" id="woocommerce_paypalpro_securitycodehint" style=""  <?php if($securitycodehint == "yes"):?>checked<?endif;?>> Enable this option if you want to show a hint for the CVV field on the credit card checkout form</label><br>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypalpro_paypalapiusername">PayPal Pro API Username</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypalpro_paypalapiusername" id="woocommerce_paypalpro_paypalapiusername" style="" value="<?php echo $paypalapiusername;?>" placeholder="">
                <p class="description">Your PayPal payments pro API username.</p>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypalpro_paypalapipassword">PayPal Pro API Password</label>
        </th>
        <td class="forminp">
            <fieldset>

                <input class="input-text regular-input " type="text" name="woocommerce_paypalpro_paypalapipassword" id="woocommerce_paypalpro_paypalapipassword" style="" value="<?php echo $paypalapipassword;?>" placeholder="">
                <p class="description">Your PayPal payments pro API password.</p>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="woocommerce_paypalpro_paypalapisigniture">PayPal Pro API Signature</label>
        </th>
        <td class="forminp">
            <fieldset>

                <textarea rows="3" cols="20" class="input-text wide-input " type="textarea" name="woocommerce_paypalpro_paypalapisigniture" id="woocommerce_paypalpro_paypalapisigniture" style="" placeholder=""><?php echo $paypalapisigniture;?></textarea>
                <p class="description">Your PayPal payments pro API signature.</p>
            </fieldset>
        </td>
    </tr>
    </tbody></table>
<input class="paypalpro_payment_custom_submit dfsf" style="float: left;" type="submit">
<img class="strip_preloader" style="display: none;float: left;width:39px;margin-left: 13px;" src="<?php echo site_url();?>/wp-content/themes/elearning/images/preloader.gif">
<script>
    jQuery(document).on("click",".paypalpro_payment_custom_submit",function(){
        var data = {
            serialization_key        :   "woocommerce_paypalpro_settings",
            func                     :   "fn_update_payment_settings_custom",
            enabled                  :   (jQuery("input[name=woocommerce_paypalpro_enabled]:checked").val() == "on")?"yes":"no",
            debug                    :   (jQuery("input[name=woocommerce_paypalpro_debug]:checked").val() == "on")?"yes":"no",
            title                    :   jQuery("input[name=woocommerce_paypalpro_title]").val(),
            securitycodehint         :   (jQuery("input[name=woocommerce_paypalpro_securitycodehint]:checked").val() == "on")?"yes":"no",
            paypalapiusername        :   jQuery("input[name=woocommerce_paypalpro_paypalapiusername]").val(),
            paypalapipassword        :   jQuery("input[name=woocommerce_paypalpro_paypalapipassword]").val(),
            paypalapisigniture       :   jQuery("textarea[name=woocommerce_paypalpro_paypalapisigniture]").val()

        }
        console.log(data)
        jQuery(".strip_preloader").css("display","block")
        jQuery.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : "<?php echo site_url();?>/wp-content/themes/elearning/members/single/settings-integrations.php", // the url where we want to POST
            data        : data, // our data object
            dataType    : 'json' // what type of data do we expect back from the server

        }).success(function(data) {

            //var resp = jQuery.parseJSON(data)
//            jQuery("#"+data.block +" .plugin_settings_block").html(data.data)
//            console.log(data.block)
//            console.log(data.data)
//            jQuery("#"+data.block).dialog({ width: 760 })
            //jQuery(".ui-dialog-titlebar-close").trigger("click")
            jQuery(".strip_preloader").css("display","none")
        })
    })
</script>