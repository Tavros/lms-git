<?php $current_user_login = wp_get_current_user()->user_login; ?>

<div class="eLearning-website row hs-elearning">
  <div class="container-fluid website-menu">
    <ul class="nav navbar-nav list-group">
      <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/website"?>">PAGES</a></li>
      <li class="list-group-item current selected "><a href="<?php echo site_url()."/".$current_user_login."/registration-form"?>">REGISTRATION FORM</a> <span class="fm_carret_up" ></span></li>
      <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/analytics"?>">ANALYTICS</a></li>
    	<li class="list-group-item "><a href="<?php echo site_url()."/".$current_user_login."/site-settings"?>">SITE SETTINGS</a></li>
    </ul>
  </div><!--End container-fluid website-menu-->
  <div class="Form_Customization_menu">
    <ul class="Form_Customization_nav">
      <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/entries"?>">ENTRIES</a></li>
      <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_builder"?>">FORM BOILDER</a></li>
      <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form-messages"?>">MESSAGES</a></li>
      <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_responses"?>">RESPONSES</a></li>
      <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/Form_Customization"?>">COSTOMIZATION</a></li>
      <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/Analitics"?>">CONDITIONAL LOGIC</a></li>
   </ul>
  </div><!--End Form_Customization_menu-->
  <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12   right_side">
    <div class="container-fluid">
      <div class="row right_row">
        <div class="col-md-4 col-sm-2 col-xs-2"><h1>Registration </h1></div>
        <div class="col-md-8 col-sm-10 col-sm-10 button_part"><button class="btn btn-primary btn-lg pull-right top-button">CREATE PAGE</button></div>
      </div>                   
      <div class="row table_wrapper">
        <div class="col-md-12 col-sm-12 col-xs-12"></div>
      </div>
    </div><!--End container-fluid-->
  </div><!--End col-xs-12 col-md-12 col-sm-12 col-lg-12 right_side-->
</div><!--End eLearning-website row hs-elearning-->
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>
