<?php
/**
 * BuddyPress - Users Settings
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */
if (!defined('ABSPATH'))
    exit;
?>

<div class="item-list-tabs no-ajax" id="subnav" role="navigation">
    <ul>
        <?php bp_get_options_nav(); ?>
    </ul>
</div>

<?php
do_action('eLearning_after_single_item_list_tabs');
switch (bp_current_action()) :
    case 'site' :
        bp_get_template_part('members/single/admin-settings/site/site');
        break;
    case 'setting-commerce' :
        bp_get_template_part('members/single/admin-settings/commerce/commerce');
        break;
    case 'setting-email' :
        bp_get_template_part('members/single/admin-settings/email/email');
        break;
    case 'setting-learning' :
        bp_get_template_part('members/single/admin-settings/learning/learning');
        break;
    case 'account-privacy' :
        bp_get_template_part('members/single/admin-settings/privacy');
        break;
    case 'account-notifications' :
        bp_get_template_part('members/single/admin-settings/notifications');
        break;
    case 'account-billing' :
        bp_get_template_part('members/single/admin-settings/billing');
        break;
    case 'setting-account' :
        bp_get_template_part('members/single/admin-settings/setting-account');
        break;
    case 'setting-onstream' :
        bp_get_template_part('members/single/admin-settings/setting-store');
        break;   
    default:
        bp_get_template_part('members/single/admin-settings/site/page');
        break;
endswitch;
?>
