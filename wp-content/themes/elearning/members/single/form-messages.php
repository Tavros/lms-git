<?php $current_user_login = wp_get_current_user()->user_login; ?>
<div class="eLearning-website row hs-elearning">
    <div class="container-fluid website-menu">
        <ul class="nav navbar-nav list-group">
            <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/website"?>">PAGES</a></li>
            <li class="list-group-item current selected "><a href="<?php echo site_url()."/".$current_user_login."/registration-form"?>">REGISTRATION FORM</a> <span class="asadadada" ></span></li>
            <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/analytics"?>">ANALYTICS</a></li>
            <li class="list-group-item "><a href="<?php echo site_url()."/".$current_user_login."/site-settings"?>">SITE SETTINGS</a></li>
        </ul>
    </div><!--container-fluid website-menu-->
    <div class="Form_Customization_menu">
        <ul class="Form_Customization_nav">
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/entries"?>">ENTRIES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_builder"?>">FORM BOILDER</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form-messages"?>"><i class="fa fa-caret-up" aria-hidden="true"></i>MESSAGES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/form_responses"?>">RESPONSES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/Form_Customization"?>">COSTOMIZATION</a></li>
            <li class="Form_Customization_item"><a href="<?php echo site_url()."/".$current_user_login."/Analitics"?>">CONDITIONAL LOGIC</a></li>
        </ul>
    </div><!--Form_Customization_menu-->
    <div class="container">
        <div class="page-header">
            <h1>Messages</h1>
        </div>
        <p>The following fields, you can use thesemail-tags:<br/>  [name] [email]</p><br>
        <form>
            <div class="form-group row">
                <label  class="col-sm-2 col-form-label">To</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-2 col-form-label">From</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-2 col-form-label">Subject</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-2 col-form-label">Additional  Headers</label>
                <div class="col-sm-6">
                    <textarea type="text" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-2 col-form-label">Message Body</label>
                <div class="col-sm-6">
                    <textarea type="text" class="form-control"></textarea>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="">
                            Exclude lines with blank mail-tags from output
                        </label>
                    </div>
                    <div class="form-check disabled">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="">
                            Use HTML content type
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-2 col-form-label">File Attachments</label>
                <div class="col-sm-6">
                    <textarea type="text" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-check">
                <label class="form-check-label">

                   <b> Mail(2)</b>
                </label><br/>
                <input class="form-check-input" type="checkbox" value=""> Use Mail(2)<br>
                <span class="mail_2_info"> Mail (2) is an additional mail template often used as an autoresponder.</span>
            </div>
            <br>
            <div class="form-group row">
                <div class="offset-sm-2 col-sm-6 hs-elearning">
                    <button type="submit" class="btn btn-primary top-button">Save</button>
                </div>
            </div>
        </form>
    </div><!--container-->
</div><!--eLearning-website row hs-elearning-->
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>