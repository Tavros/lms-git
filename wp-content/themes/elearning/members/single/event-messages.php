<?php $current_user_login = wp_get_current_user()->user_login; ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<div class="eLearning-website row hs-elearning event-messages">
    <div class="head">
        <div class="row">
            <div class="col-md-8">
                <h3>Event Name</h3>
            </div>
            <div class="col-md-2">
                <a id="event_edit_save" class="btn btn-lg btn-primary pull-right top-button"  type="button" >SAVE</a>
            </div>
            <div class="col-md-2">
                <a id="event_edit_publish" class="btn btn-lg btn-primar pull-righty top-button" type="button" >PUBLISH</a>
            </div>
        </div>
    </div><!--End head-->
    <div class="event_menu_links">
        <ul class="Form_Customization_nav">
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/entries">GENERAL</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/event-messages"><i class="fa fa-caret-up" aria-hidden="true"></i>MESSAGES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/event-pricing">PRICING</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/event-website">WEBSITE</a></li>
        </ul>
    </div>
    <div class="col-xs-8 col-md-8 col-sm-8 col-lg-8 right_side">
        <div class="container-fluid">                             
            <div class="row table_wrapper">
                <div class="col-md-10 col-sm-10 col-xs-10 content ">
                <h6 class="h6color">Course instructions</h6>
                        <div class="top-buttons-left">
                           <button type="button" class="messages-button-media"><span class="wp-media-buttons-icon"></span> Add Media</button>
                           <button type="button" class="messages-button-zip"><span class="glyphicon glyphicon-upload"></span> Upload ZIP</button>
                        </div> 
                        <div class="top-buttons-right">
                           <button type="button" class="messages-button-visual">Visual</button>
                           <button type="button" class="messages-button-text">Text</button>
                        </div>
                               
                            <div class="course-instructions">
                                <button type="button" class="messages-btn"><?php _e( 'b', 'make-bold' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( '/', 'slash' ); ?></button>
                                <button type="button" class="messages-btn" style="text-decoration:underline;"><?php _e( 'link', 'add-link' ); ?> </button>
                                <button type="button" class="messages-btn"><?php _e( 'b-quote', 'add-quote' ); ?> </button>
                                <button type="button" class="messages-btn"><?php _e( 'del', 'delete' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'ins', 'ins-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'img', 'img-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'ul', 'ul-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'ol', 'ol-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'll', 'll-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'code', 'code' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'more', 'read-more' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'close tags', 'close-tag' ); ?></button>                
                            </div>
                            <div class="main-textarea">
                            <textarea></textarea>
                            </div>      
                   
                    <h6 class="h6color">COURSE COMPLETION MESSAGE</h6>
                        <div class="top-buttons-left">
                           <button type="button" class="messages-button-media"><span class="wp-media-buttons-icon"></span> Add Media</button>
                           <button type="button" class="messages-button-zip"><span class="glyphicon glyphicon-upload"></span> Upload ZIP</button>
                        </div> 
                        <div class="top-buttons-right">
                           <button type="button" class="messages-button-visual">Visual</button>
                           <button type="button" class="messages-button-text">Text</button>
                        </div>
                                    
                            <div class="course-instructions">
                                <button type="button" class="messages-btn"><?php _e( 'b', 'make-bold' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( '/', 'slash' ); ?></button>
                                <button type="button" class="messages-btn" style="text-decoration:underline;"><?php _e( 'link', 'add-link' ); ?> </button>
                                <button type="button" class="messages-btn"><?php _e( 'b-quote', 'add-quote' ); ?> </button>
                                <button type="button" class="messages-btn"><?php _e( 'del', 'delete' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'ins', 'ins-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'img', 'img-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'ul', 'ul-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'ol', 'ol-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'll', 'll-tag' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'code', 'code' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'more', 'read-more' ); ?></button>
                                <button type="button" class="messages-btn"><?php _e( 'close tags', 'close-tag' ); ?></button>                
                            </div>
                    
                    <div class="main-textarea">      
                    <textarea></textarea>
                    </div>
                    <button type="button" class="save-setting">SAVE SETTINGS +</button>
                </div><!--Close col-md-12 col-sm-12 col-xs-12-->
            </div><!--Close row table_wrapper-->
        </div><!--Close row table_wrapper-->
    </div><!--Close container-fluid-->
