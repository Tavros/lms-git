<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php
get_header('buddypress');
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
?>
<?php
$custom_args = array(
    'post_type' => 'page',
    'posts_per_page' => 5,
    'paged' => $paged
);
$custom_query = new WP_Query($custom_args);
$current_user_login = wp_get_current_user()->user_login;
?>
<style>#subnav{display:none}</style>
<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row heigh_correct">
			<div class="item-list-tabs no-ajax test" id="subnav">
				<ul>			 
					<?php bp_get_options_nav(); ?>
					<?php do_action( 'bp_member_plugin_options_nav' );  ?>
				</ul>
			</div><!-- .item-list-tabs -->
                <div class="col-md-2 col-sm-4 left_nav_menu">
                    <?php do_action('bp_before_member_plugin_template');  ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>
                                <?php getCustomBodypressMenu_child(); ?>
                                <?php do_action( 'bp_member_options_nav' ); ?>
                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div><!--End col-md-2 col-sm-4 left_nav_menu-->
                <div class="col-md-10 col-sm-8 fm_right_grid">
					<div class="container-fluid website-menu">
						<ul class="nav navbar-nav list-group">
							 <li class="list-group-item current selected"><a href="<?php echo site_url()."/".$current_user_login."/setting/site"?>">SITE</a> <span class="fm_carret_up" ></span></li>
							<!-- <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/registration-form"?>">FORMS</a></li>-->
							 <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/settings-integrations"?>">INTEGRATIONS</a></li>
							 <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/Analitics"?>">ACCOUNT</a></li>
						</ul>
					</div>

					<div class="fm_settings_site">
						<div class="settings_site_title">
                        <div id="DpSettings" style="color:#333333">
                        <table id="students">
                            <h3>Lesson Experience</h3>
                            <tr>
                                <td>Lesson locking</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>This is an example descriptions that comes with this setting</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit media lock</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Notes and Discussion styles</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Auto-mark unit complete when user proceeds to next unit</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Enable Unit Time as Dript Duration</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Enable Section Drip feed</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Show Unit Description in Course Curriculum (LMS only)</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Assignment lock</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Enable Unit/Quiz Start date time</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border:none">Unit Comments/Notes</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                        </table><!--#students1-->
                        <table id="students" style="border-bottom:none;">
                            <h3>Course Display</h3>
                            <tr>
                                <td>Course default avatar</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>This is an example descriptions that comes with this setting</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Hide course from directory</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Show course progress bar</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Disable instructor display in courses</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Hide Members section in Single Course page</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Show curriculum below Course description</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Course Timeline Accordion style</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Remove Finished Courses from directory</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Show related courses at the end of single course</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Skip course status page</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Fix Course on Menu Scroll</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Default order in course directory</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Default order in Members directory</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Default order in Members directory</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border:none">Default course block style</td>
                                <td style="border:none"></td>
                            </tr>            
                            <tr>
                                <td style="display:inline-block;border:none">
                                    <div class="of-radio-img-inner-container">
                                        <input type="radio" id="header-layout_side" class="of-radio-img-radio of-js-hider js-hider-show" data-js-target="header-layout-side-microwidgets, .header-layout-side-settings" value="side" name="the7dtchild[header-layout]" checked="checked" style="display: none;">
                                        <img src="/wp-content/themes/elearning/images/side-header.gif" alt="Side header" class="of-radio-img-img of-radio-img-selected" onclick="dtRadioImagesSetCheckbox('header-layout_side');" style="display: inline;">
                                        <div class="of-radio-img-label"></div>
                                    </div>
                                </td>
                                <td style="display:inline-block; border:none">
                                    <div class="of-radio-img-inner-container">
                                        <input type="radio" id="header-layout_side" class="of-radio-img-radio of-js-hider js-hider-show" data-js-target="header-layout-side-microwidgets, .header-layout-side-settings" value="side" name="the7dtchild[header-layout]" checked="checked" style="display: none;">
                                        <img src="/wp-content/themes/elearning/images/side-header.gif" alt="Side header" class="of-radio-img-img of-radio-img-selected" onclick="dtRadioImagesSetCheckbox('header-layout_side');" style="display: inline;">
                                        <div class="of-radio-img-label"></div>
                                    </div>
                                </td>
                                <td style="display:inline-block; border:none">
                                    <div class="of-radio-img-inner-container">
                                        <input type="radio" id="header-layout_side" class="of-radio-img-radio of-js-hider js-hider-show" data-js-target="header-layout-side-microwidgets, .header-layout-side-settings" value="side" name="the7dtchild[header-layout]" checked="checked" style="display: none;">
                                        <img src="/wp-content/themes/elearning/images/side-header.gif" alt="Side header" class="of-radio-img-img of-radio-img-selected" onclick="dtRadioImagesSetCheckbox('header-layout_side');" style="display: inline;">
                                        <div class="of-radio-img-label"></div>
                                    </div>
                                </td>
                                <td style="display:inline-block; border:none">
                                    <div class="of-radio-img-inner-container">
                                        <input type="radio" id="header-layout_side" class="of-radio-img-radio of-js-hider js-hider-show" data-js-target="header-layout-side-microwidgets, .header-layout-side-settings" value="side" name="the7dtchild[header-layout]" checked="checked" style="display: none;">
                                        <img src="/wp-content/themes/elearning/images/side-header.gif" alt="Side header" class="of-radio-img-img of-radio-img-selected" onclick="dtRadioImagesSetCheckbox('header-layout_side');" style="display: inline;">
                                        <div class="of-radio-img-label"></div>
                                    </div>
                                </td>
                                <td style="display:inline-block; border:none">
                                    <div class="of-radio-img-inner-container">
                                        <input type="radio" id="header-layout_side" class="of-radio-img-radio of-js-hider js-hider-show" data-js-target="header-layout-side-microwidgets, .header-layout-side-settings" value="side" name="the7dtchild[header-layout]" checked="checked" style="display: none;">
                                        <img src="/wp-content/themes/elearning/images/side-header.gif" alt="Side header" class="of-radio-img-img of-radio-img-selected" onclick="dtRadioImagesSetCheckbox('header-layout_side');" style="display: inline;">
                                        <div class="of-radio-img-label"></div>
                                    </div>
                                </td>
                            </tr>
                        </table><!--#students2-->
                        <table id="students" style="border-bottom:none;">
                            <h3>Course Permissions</h3>
                            <tr>
                                <h4 style="font-weight:bold; font-size:1rem;">STUDENTS</h4>
                                <td>Assign Free courses to students on account activation</td>
                                <td> <label class="switch"> <input type="checkbox"> <div class="slider round"></div></label></td>
                            </tr>
                            <tr>
                                <td>Free units should only be accessible to logged in members</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Disable Auto allocation of free courses</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Enable Course external link</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Enable Course Codes</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Coming soon courses</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Change Pre-Required Course Condition from submitted to Evaluated</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Enable Course forum privacy</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                        </table><!--#students3-->
                        <table id="instruc" style="border-bottom:none;">
                            <tr>
                                <h4 style="font-weight:bold; font-size:1rem; margin-top:50px;">INSTRUCTORS</h4>
                                <td>Instructor can extend subscriptions</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Instructors can assign/ remove certificates and badges</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>instructors can manage studen status</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Instructors can add students</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Force instructor content privacy</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Finished Course Access</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Admin approval for course</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Force Administrator Approval on every setting</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                        </table><!--#instruc-->
                        <table id="students" style="border-bottom:none;">
                            <h3>Course Sections</h3>
                            <tr>
                                <td>Levels</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Locations</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>News</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Members</td>
                                <td><select>
                                    <option value="volvo">EVERYONE</option>
                                    <option value="saab">MEMBERS</option>
                                    <option value="mercedes">COURSE STUDENTS</option>
                                    <option value="audi">INSTRUCTORS</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Activity Feed</td>
                                <td><select>
                                    <option value="volvo">MEMBERS</option>
                                    <option value="saab">EVERYONE</option>
                                    <option value="mercedes">COURSE STUDENTS</option>
                                    <option value="audi">INSTRUCTORS</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Curriculum</td>
                                <td><select>
                                    <option value="volvo">MEMBERS</option>
                                    <option value="saab">COURSE STUDENTS</option>
                                    <option value="mercedes">EVERYONE</option>
                                    <option value="audi">INSTRUCTORS</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Drive</td>
                                <td><select>
                                    <option value="volvo">COURSE STUDENTS</option>
                                    <option value="saab">MEMBERS</option>
                                    <option value="mercedes">EVERYONE</option>
                                    <option value="audi">INSTRUCTORS</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Events</td>
                                <td><select>
                                    <option value="volvo">EVERYONE</option>
                                    <option value="saab">MEMBERS</option>
                                    <option value="mercedes">COURSE STUDENTS</option>
                                    <option value="audi">INSTRUCTORS</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Leaderboard stats</td>
                                <td><select>
                                    <option value="volvo">INSTRUCTORS</option>
                                    <option value="saab">MEMBERS</option>
                                    <option value="mercedes">COURSE STUDENTS</option>
                                    <option value="audi">EVERYONE</option>
                                </select></td>
                            </tr>
                            <tr>
                                <td>Forums</td>
                                <td><select>
                                    <option value="volvo">COURSE STUDENTS</option>
                                    <option value="saab">MEMBERS</option>
                                </select></td>
                            </tr>
                        </table><!--#students4-->                    
                        <table id="students" style="border-bottom:none;">
                            <tr>
                                <h3>Quiz Settings</h3>
                            </tr>
                            <tr>
                                <td>Enable In-Course Quiz</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>SIn-Course Quize</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Enable passing score for Quiz</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Force Quiz availablity to Course Students</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Hide Correct answers</td>
                                    <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Enable negative marking</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Show Assignements in Course Curriculum</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Display Submission  time in Course/Quiz/Assignment submissions</td>
                                <td>
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round"></div>
                                    </label>
                                </td>
                            </tr>
                        </table><!--#students5-->
                        </div>
                    </div><!-- .fm_settings_site -->
                    
                    <?php do_action('bp_after_member_dashboard_template'); ?>
                </div> <!--.fm_right_grid-->
            </div><!-- #content -->
        </div><!--.container-->
    </div><!--#buddypress-->
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
