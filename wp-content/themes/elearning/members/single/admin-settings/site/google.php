<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php get_header('buddypress'); ?>

<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">

                    <?php do_action('bp_before_member_plugin_template'); ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>

                                <?php bp_get_displayed_user_nav(); ?>

                                <?php do_action('bp_member_options_nav'); ?>

                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <div class="bp_section_title col-sm-3"><?php _e('Settings', 'eLearning'); ?></div>
                        <ul>
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('setting');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <ul id="subList">
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('site');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="padder row">
                        <form action="<?php echo bp_displayed_user_domain() . ELEARNING_ACCOUNT_SLUG . '/' . ELEARNING_ACCOUNT_GENETRAL_SLUG; ?>" method="post" class="<?php echo bp_current_action(); ?>  standard-form col-sm-10" id="settings-form">

                            <?php do_action('bp_account_before_submit'); ?> 
                            <div class="row">
                                <div class="clearfix ">
                                    <label for="google_captcha" class="bp_checkbox_label col-sm-11"><?php _e('Google captcha on form', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="google_captcha" id="google_captcha" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="google_captcha_publick_key" class="bp_checkbox_label col-sm-11"><?php _e('Google captcha publick key', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="google_captcha_publick_key" id="google_captcha_publick_key" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="google_captcha_private_key" class="bp_checkbox_label col-sm-11"><?php _e('Google captcha private key', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="google_captcha_private_key" id="google_captcha_private_key" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="google_maps_api_key" class="bp_checkbox_label col-sm-11"><?php _e('Google maps API key', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="google_maps_api_key" id="google_maps_api_key" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="contact_page_coords "class="bp_checkbox_label col-sm-11"><?php _e('Contact page latitude and longitude values', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="contact_page_coords" id="contact_page_coords" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="google_analytics_code "class="bp_checkbox_label col-sm-11"><?php _e('Google analytics code', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="google_analytics_code" id="google_analytics_code" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="google_fonts_api_key "class="bp_checkbox_label col-sm-11"><?php _e('Enter google fonts API key', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="google_fonts_api_key" id="google_fonts_api_key" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="submit">
                                <input type="submit" name="bp_peyotto_form_subimitted" value="<?php _e('Save', 'eLearning'); ?>" id="submit" class="auto" />
                            </div>

                            <?php do_action('bp_account_after_submit'); ?>
                            <?php wp_nonce_field('bp_account_general'); ?>

                        </form>
                    </div><!-- .padder -->

                    <?php do_action('bp_after_member_dashboard_template'); ?>

                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
