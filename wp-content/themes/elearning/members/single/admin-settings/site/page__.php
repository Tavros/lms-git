<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php
get_header('buddypress');

$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
?>
<?php
$custom_args = array(
    'post_type' => 'page',
    'posts_per_page' => 5,
    'paged' => $paged
);

$custom_query = new WP_Query($custom_args);
?>

<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">

                    <?php do_action('bp_before_member_plugin_template'); ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>

                                <?php getCustomBodypressMenu(); ?>

                                <?php do_action('bp_member_options_nav'); ?>

                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <div class="bp_section_title col-sm-3"><?php _e('Settings', 'eLearning'); ?></div>
                        <ul>
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('setting');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <ul id="subList">
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('site');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="add_site_page">
                        <a href="#"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                    <div class="padder row">
                        <div class="bs-example front-end-table col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width: 60%;"><?php _e('Title', 'eLearning'); ?></th>
                                        <th style="width: 20%;"><?php _e('Last Update', 'eLearning'); ?></th>
                                        <th style="width: 20%;" class="actions-hed">  <?php
                                            if (function_exists(custom_pagination)) {
                                                custom_pagination($custom_query->max_num_pages, "", $paged);
                                            }
                                            ?></th>                                        
                                    </tr>
                                </thead>
                                <tbody>



                                    <?php if ($custom_query->have_posts()) : ?>

                                        <!-- the loop -->
                                        <?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>

                                            <tr >
                                                <td><a target="_blank" href="<?php echo get_permalink(get_the_ID()); ?>"><?php the_title(); ?></a></td>
                                                <td><?PHP echo get_the_date('d/m/y hh:i A', get_the_ID()); ?></td>
                                                <td class="actions">
                                                    <a class="view"  target="_blank" href="<?php echo get_permalink(get_the_ID()); ?>">View</a>
                                                    <a class="edit" target="_blank" href="/wp-admin/post.php?vc_action = vc_inline&post_id =<?php echo get_the_ID(); ?>&post_type = page">Edit</a>
                                                    <a class="delete" href="#">Delete</a>
                                                </td>

                                            </tr>

                                        <?php endwhile; ?>
                                        <!-- end of the loop -->

                                        <!-- pagination here -->


                                        <?php wp_reset_postdata(); ?>

                                    <?php else: ?>
                                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                                <?php endif; ?>

                                
                                </tbody>
                            </table>
                        </div>

                    </div><!-- .padder -->

                    <?php do_action('bp_after_member_dashboard_template'); ?>

                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
