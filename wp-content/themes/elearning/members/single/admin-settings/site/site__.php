<?php

global $bp;

if (!isset($bp->action_variables[0])) {
    bp_get_template_part('members/single/admin-settings/site/page');
} else {
    switch ($bp->action_variables[0]) :
        case 'site-general' :
            bp_get_template_part('members/single/admin-settings/site/general');
            break;
        case 'site-google' :
            bp_get_template_part('members/single/admin-settings/site/google');
            break;
        case 'site-header' :
            bp_get_template_part('members/single/admin-settings/site/header');
            break;
        case 'site-footer' :
            bp_get_template_part('members/single/admin-settings/site/footer');
            break;
        default:
            bp_get_template_part('members/single/admin-settings/site/page');
            break;
    endswitch;
}

