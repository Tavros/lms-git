<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php
get_header('buddypress');
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
?>
<?php
$custom_args = array(
    'post_type' => 'page',
    'posts_per_page' => 5,
    'paged' => $paged
);
$custom_query = new WP_Query($custom_args);
$current_user_login = wp_get_current_user()->user_login;
?>
<style>#subnav{display:none}</style>
<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row heigh_correct">
			<div class="item-list-tabs no-ajax test" id="subnav">
				<ul>			 
					<?php bp_get_options_nav(); ?>
					<?php do_action( 'bp_member_plugin_options_nav' );  ?>
				</ul>
			</div><!-- .item-list-tabs -->
                <div class="col-md-3 col-sm-4 left_nav_menu">
                    <?php do_action('bp_before_member_plugin_template');  ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
							<div class="create_event hs-elearning">
								<a href="<?php echo site_url().'/'.$current_user_login.'/';?>edit-event" class="btn btn-primary top-button">Create Event</a>
							</div>
                            <ul>
                                <?php getCustomBodypressMenu_child(); ?>
                                <?php do_action( 'bp_member_options_nav' ); ?>
                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div><!--End col-md-2 col-sm-4 left_nav_menu-->
                <div class="col-md-9 col-sm-8 fm_right_grid">
					<div class="container-fluid website-menu">
						<ul class="nav navbar-nav list-group">
							 <li class="list-group-item current selected"><a href="<?php echo site_url()."/".$current_user_login."/setting/site"?>">SITE</a> <span class="fm_carret_up" ></span></li>
							<!-- <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/registration-form"?>">FORMS</a></li>-->
							 <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/settings-integrations"?>">INTEGRATIONS</a></li>
							 <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/Analitics"?>">ACCOUNT</a></li>
						</ul>
					</div>
					<div class="fm_settings_site">
						<div class="settings_site_title">
							<div class="left">
								<h1>Page</h1>
							</div>
							<div class="right">
								<input type="button" value="PUBLISH">
								<p>Published on </p>
							</div>
							<span>TITLE</span>
							<input type="input" value="" class="title_input">
							<span>REQUIRE MEMBERSHIP</span>
							<form action="">
							  <input type="checkbox" value=""> Gold MEmbership<br>
							  <input type="checkbox" value=""> Another example Membership
							  <input class="site_designer"type="button" value="SITE DESIGNER">
							</form>
						</div><!--End settings_site_title-->	
					</div><!--End fm_settings_site-->
                </div><!--End col-md-10 col-sm-8 fm_right_grid-->
            </div><!--End row heigh_correct-->
        </div><!--End container -->
    </div><!--End buddypress -->
</section><!--End section content -->
<!-- Extra Global div in header -->
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>
