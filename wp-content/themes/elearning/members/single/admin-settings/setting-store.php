<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php get_header('buddypress'); ?>

<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">

                    <?php do_action('bp_before_member_plugin_template'); ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>

                                <?php bp_get_displayed_user_nav(); ?>

                                <?php do_action('bp_member_options_nav'); ?>

                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <div class="bp_section_title col-sm-3"><?php _e('Store settings', 'eLearning'); ?></div>
                        <ul>
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('setting');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <?php
                    do_action('bp_template_content');

                    $onstreamAdminAccess = getOnstreamAdminAPIAccess();                    
                    ?>
                    <div class="padder row">
                        <form action="<?php echo bp_displayed_user_domain() . ELEARNING_ACCOUNTSETTINGS_SLUG . '/' . ELEARNING_ACCOUNTSETTINGS_STORE_SLUG . '/'; ?>" method="post" class="<?php echo bp_current_action(); ?>  standard-form col-sm-10" id="settings-form">
                            <label for="ostream_username" class="bp_section_label"><?php _e('Onstream Username', 'eLearning'); ?></label>
                            <input  type="text" name="ostream_username" id="ostream_username" value="<?php echo $onstreamAdminAccess['username']; ?>" class="form-control bp_section_input" />
                            <label for="ostream_password" class="bp_section_label"><?php _e('Ostream password', 'eLearning'); ?></label>
                            <input  type="password" name="ostream_password" id="ostream_password" value="<?php echo $onstreamAdminAccess['password']; ?>" class="form-control bp_section_input" />

                            <div class="submit">
                                <input type="submit" name="bp_peyotto_form_subimitted" value="<?php _e('Save', 'eLearning'); ?>" id="submit" class="auto" />
                            </div>

                            <?php do_action('bp_account_after_submit'); ?>
                            <?php wp_nonce_field('bp_onstream_account_settings'); ?>

                        </form>
                    </div><!-- .padder -->

                    <?php do_action('bp_after_member_dashboard_template'); ?>

                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>