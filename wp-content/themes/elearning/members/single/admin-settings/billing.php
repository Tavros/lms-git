<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php
get_header('buddypress');


$current_user = get_currentuserinfo();
?>

<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">

                    <?php do_action('bp_before_member_plugin_template'); ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>

                                <?php bp_get_displayed_user_nav(); ?>

                                <?php do_action('bp_member_options_nav'); ?>

                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <div class="bp_section_title col-sm-3"><?php _e('Account', 'eLearning'); ?></div>
                        <ul>
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('account');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="padder row">
                        <form action="<?php echo bp_displayed_user_domain() . ELEARNING_ACCOUNT_SLUG . '/' . ELEARNING_ACCOUNT_GENETRAL_SLUG; ?>" method="post" class="<?php echo bp_current_action(); ?> standard-form col-sm-10" id="settings-form">
                          <div class="row">
                            <div class="form-group col-md-4">
                              <label for="firstname" class="bp_section_label"><?php _e('First name', 'eLearning'); ?></label>
                              <input  type="text" name="firstname" id="firstname" value="" class="form-control" />
                            </div>
                            <div class="form-group col-md-1">
                              <label for="middle" class="bp_section_label"><?php _e('Middle', 'eLearning'); ?></label>
                              <input  type="text" name="middle" id="middle" value="" class="form-control" />
                            </div>
                            <div class="form-group col-md-4">
                              <label for="lastname" class="bp_section_label"><?php _e('Last name', 'eLearning'); ?></label>
                              <input  type="text" name="lastname" id="lastname" value="" class="form-control" />
                            </div>
                            <div class="form-group col-md-3">
                              <label for="suffix" class="bp_section_label"><?php _e('Suffix', 'eLearning'); ?></label>
                              <input  type="text" name="suffix" id="suffix" value="" class="form-control" />
                            </div>
                          </div>
                          <div class="row">
                            <div class="form-group col-md-8">
                              <label for="street" class="bp_section_label"><?php _e('Street', 'eLearning'); ?></label>
                              <input  type="text" name="street" id="street" value="" class="form-control" />
                            </div>
                            <div class="form-group col-md-4">
                              <label for="apartment" class="bp_section_label"><?php _e('Apartment', 'eLearning'); ?></label>
                              <input  type="text" name="apartment" id="apartment" value="" class="form-control" />
                            </div>
                          </div>
                          <div class="row">
                            <div class="form-group col-md-12">
                              <label for="street-line-2" class="bp_section_label"><?php _e('Street line 2', 'eLearning'); ?></label>
                              <input  type="text" name="street-line-2" id="street-line-2" value="" class="form-control" />
                            </div>
                          </div>
                          <div class="row">
                            <div class="form-group col-md-5">
                              <label for="city" class="bp_section_label"><?php _e('City', 'eLearning'); ?></label>
                              <input  type="text" name="city" id="city" value="" class="form-control" />
                            </div>
                            <div class="form-group col-md-2">
                              <label for="state" class="bp_section_label"><?php _e('State', 'eLearning'); ?></label>
                              <input  type="text" name="state" id="state" value="" class="form-control" />
                            </div>
                            <div class="form-group col-md-2">
                              <label for="zip" class="bp_section_label"><?php _e('Zip', 'eLearning'); ?></label>
                              <input  type="text" name="zip" id="zip" value="" class="form-control" />
                            </div>
                            <div class="form-group col-md-3">
                              <label for="country" class="bp_section_label"><?php _e('Country', 'eLearning'); ?></label>
                              <input  type="text" name="country" id="country" value="" class="form-control" />
                            </div>
                          </div>
                          <div class="submit">
                              <input type="submit" name="bp_peyotto_form_subimitted" value="<?php _e('Save', 'eLearning'); ?>" id="submit" class="auto" />
                          </div>

                            <?php do_action('bp_account_after_submit'); ?>
                            <?php wp_nonce_field('bp_account_profile'); ?>

                        </form>
                    </div><!-- .padder -->

                    <?php do_action('bp_after_member_dashboard_template'); ?>

                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
