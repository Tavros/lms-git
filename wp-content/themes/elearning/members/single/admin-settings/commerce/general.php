<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php get_header('buddypress');?>

<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">

                    <?php do_action('bp_before_member_plugin_template'); ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>

                                <?php bp_get_displayed_user_nav(); ?>

                                <?php do_action('bp_member_options_nav'); ?>

                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <div class="bp_section_title col-sm-3"><?php _e('Commerce', 'eLearning'); ?></div>
                        <ul>
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('setting');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <ul id="subList">
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('setting-commerce');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="padder row">
                        <form action="<?php echo bp_displayed_user_domain() . ELEARNING_ACCOUNT_SLUG . '/' . ELEARNING_ACCOUNT_GENETRAL_SLUG; ?>" method="post" class="<?php echo bp_current_action(); ?>  standard-form col-sm-10" id="settings-form">

                            <?php do_action('bp_account_before_submit'); ?> 
                            <div class="row">
                                <div class="clearfix ">
                                    <label for="base_location" class="bp_checkbox_label col-sm-11"><?php _e('Baselocation', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="base_location" id="base_location" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="direct_checkout" class="bp_checkbox_label col-sm-11"><?php _e('Direct checkout', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="direct_checkout" id="direct_checkout" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="redirect_to_course_page_on_order" class="bp_checkbox_label col-sm-11"><?php _e('Redirect to course page on order completion', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="redirect_to_course_page_on_order" id="redirect_to_course_page_on_order" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="remove_extra_checkout_fields" class="bp_checkbox_label col-sm-11"><?php _e('Remove extra checkout fields', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="remove_extra_checkout_fields" id="remove_extra_checkout_fields" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                 <div class="clearfix ">
                                    <label for="currancy" class="bp_checkbox_label col-sm-11"><?php _e('Currancy', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="currancy" id="currancy" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="enable_rating_on_reviews" class="bp_checkbox_label col-sm-11"><?php _e('Enable rating on reviews', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="enable_rating_on_reviews" id="enable_rating_on_reviews" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                 <div class="clearfix ">
                                    <label for="show_veryfied_owner_tags" class="bp_checkbox_label col-sm-11"><?php _e('Show veryfied owner tags', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="show_veryfied_owner_tags" id="show_veryfied_owner_tags" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="show_only_comment_from_veryfied_owners" class="bp_checkbox_label col-sm-11"><?php _e('Show only comment from veryfied owners', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="show_only_comment_from_veryfied_owners" id="show_only_comment_from_veryfied_owners" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="submit">
                                <input type="submit" name="bp_peyotto_form_subimitted" value="<?php _e('Save', 'eLearning'); ?>" id="submit" class="auto" />
                            </div>

                            <?php do_action('bp_account_after_submit'); ?>
                            <?php wp_nonce_field('bp_account_general'); ?>

                        </form>
                    </div><!-- .padder -->

                    <?php do_action('bp_after_member_dashboard_template'); ?>

                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
