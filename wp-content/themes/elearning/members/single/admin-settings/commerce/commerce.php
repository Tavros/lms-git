<?php

global $bp;

if (!isset($bp->action_variables[0])) {
    bp_get_template_part('members/single/admin-settings/commerce/general');
} else {
    switch ($bp->action_variables[0]) :
        case 'commerce-paypal' :
            bp_get_template_part('members/single/admin-settings/commerce/paypal');
            break;
        case 'commerce-stripe' :
            bp_get_template_part('members/single/admin-settings/commerce/stripe');
            break;
        default:
            bp_get_template_part('members/single/admin-settings/commerce/general');
            break;
    endswitch;
}

