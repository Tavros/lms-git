<?php
// echo "email-test1";die();
global $bp;

if (!isset($bp->action_variables[0])) {
    bp_get_template_part('members/single/admin-settings/email/general');
} else {
    switch ($bp->action_variables[0]) :
        case 'email-paypal' :
            bp_get_template_part('members/single/admin-settings/email/paypal');
            break;
        case 'email-stripe' :
            bp_get_template_part('members/single/admin-settings/email/stripe');
            break;
        default:
            bp_get_template_part('members/single/admin-settings/email/general');
            break;
    endswitch;
}

