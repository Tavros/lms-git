<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php
get_header('buddypress');
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
?>
<?php
$custom_args = array(
    'post_type' => 'page',
    'posts_per_page' => 5,
    'paged' => $paged
);
$custom_query = new WP_Query($custom_args);

$current_user_login = wp_get_current_user()->user_login;
?>
<style>#subnav{display:none}</style>
<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row heigh_correct">
			<div class="item-list-tabs no-ajax test" id="subnav">
				<ul>			 
					<?php bp_get_options_nav(); ?>
					<?php do_action( 'bp_member_plugin_options_nav' );  ?>
				</ul>
			</div><!-- .item-list-tabs -->
                <div class="col-md-2 col-sm-4 left_nav_menu">
                    <?php do_action('bp_before_member_plugin_template');  ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>
                                <?php getCustomBodypressMenu_child(); ?>
                                <?php do_action( 'bp_member_options_nav' ); ?>
                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div><!--End col-md-2 col-sm-4 left_nav_menu-->
                <div class="col-md-10 col-sm-8 fm_right_grid">
					<div class="container-fluid website-menu">
						<ul class="nav navbar-nav list-group">
							 <li class="list-group-item current selected"><a href="<?php echo site_url()."/".$current_user_login."/setting/site"?>">SITE</a> <span class="fm_carret_up" ></span></li>
							<!-- <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/?page=registration-form"?>">FORMS</a></li>-->
							 <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/?page=settings-integrations"?>">INTEGRATIONS</a></li>
							 <li class="list-group-item"><a href="<?php echo site_url()."/".$current_user_login."/?page=Analitics"?>">ACCOUNT</a></li>
						</ul>
					</div>
					<div class="fm_settings_site">
						<div class="settings_site_title">
                        <div id="Dpemail">
                        <div class="container">  
                            <h3>General </h3>
                            <table id="first">
                                <tr>
                                    <td>Use Mandrill SMTP to send emails</td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Add user registration entries to MailChimp lists.</td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>From Name</td>
                                    <td><input style="width:100%" type="text"></td>
                                </tr>
                                <tr>
                                    <td>From Email</td>
                                    <td><input style="width:100%" type="text"></td>
                                </tr>
                                <tr>
                                    <td>Reply to Email</td>
                                    <td><input style="width:100%" type="text"></td>
                                </tr>
                                <tr>
                                    <td>Mailchimp API Key</td>
                                    <td><input style="width:100%" type="text"></td>
                                </tr>
                                <tr>
                                    <td>Mandrill API Key</td>
                                    <td><input style="width:100%" type="text"></td>
                                </tr>
                            </table><!--#first-->
                            <h3>Email Templates</h3>
                            <table id="second">
                                <tr>
                                    <td>NAME</td>
                                    <td>CREATED</td>
                                    <td>ACTIONS</td>
                                </tr>
                                <tr>
                                    <td>Standard Template for Site</td>
                                    <td>3/16/2017</td>
                                    <td>
                                        <i class="fa fa-eye fa-lg" aria-hidden="true"></i> 
                                        <i class="fa fa-pencil fa-lg" aria-hidden="true"></i>
                                        <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Standard Template for Site</td>
                                    <td>3/16/2017</td>
                                    <td>
                                        <i class="fa fa-eye fa-lg" aria-hidden="true"></i> 
                                        <i class="fa fa-pencil fa-lg" aria-hidden="true"></i>
                                        <i class="fa fa-times fa-lg" aria-hidden="true"></i>
                                    </td>
                                </tr>
                                <tr>   
                                    <th>Course Interactions</th>
                                    <td>STUDENT</td>
                                    <td>INSTRUCTOR</td>
                                </tr>
                                <tr>
                                    <td>Course Subscribed</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>User Starts a Course</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Course Certificate</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td> Course Badge</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>                                    
                                <tr>
                                    <td>Course Retake by User</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Course Submit</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Course Reviews</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Unsubscribe Course</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Unit marked complete by User</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Unit comment added by User</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Quiz Start by User</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Quiz Submitted by User</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Quiz Retake by User</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Assignment Start by User</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Assignment Submitted by User</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Student applied for Course</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <th>Instructor Updates</th>
                                    <td>STUDENT</td>
                                    <td>INSTRUCTOR</td>
                                </tr>
                                <tr>
                                    <td>Announcements</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>User added to Course</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Course Reset by Instructor</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Unit marked complete by instructor for Student</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Unit marked incomplete by instructor for Student</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>News</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Quiz Reset by Instructor</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Quiz Evalution</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Assignment Evalution</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Assignment Reset by Instructor</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Instructor approves/reject user application</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <td>Instructor Publishes a Course or Sends for Approval</td>
                                    <td><input type="checkbox"></td>
                                    <td><input type="checkbox"></td>
                                </tr>
                                <tr>
                                    <th>Affiliate</th>
                                    <td>ACTIVATED</td>
                                    <td>TEMPLATE</td>
                                </tr>
                                <tr>
                                    <td>Affiliate signup</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Affiliate application accepted</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Affiliate referral notification message</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <th>Membership</th>
                                    <td>ACTIVATED</td>
                                    <td>TEMPLATE</td>
                                </tr>
                                <tr>
                                    <td>Account activation</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                    <tr>
                                    <td>Forgot password</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Account deactivation</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>                           
                                <tr>
                                    <th>Commerce</th>
                                    <td>ACTIVATED</td>
                                    <td>TEMPLATE</td>
                                </tr>
                                <tr>
                                    <td>New order</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Cancelled order</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Failed order</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Order on hold</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Processing order</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>                                   
                                <tr>
                                    <td>Completed order</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Refunded order</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Customer Invoice</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Customer note</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <th>Webinar</th>
                                    <td>ACTIVATED</td>
                                    <td>TEMPLATE</td>
                                </tr>
                                <tr>
                                    <td>Attendee registered</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Attendee unregistered</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Event finished</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                <tr>
                                    <td>Recording ready</td>
                                    <td><input type="checkbox"></td>
                                    <td>The name of the selected template<i style="margin-left:20px;" class="fa fa-pencil fa-lg" aria-hidden="true"></i></td>
                                </tr>
                            </table><!--#second-->
                        </div><!--.container-->
                        <?php do_action('bp_after_member_dashboard_template'); ?>
                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
