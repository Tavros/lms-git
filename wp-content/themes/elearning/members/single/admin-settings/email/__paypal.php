<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php get_header('buddypress'); ?>

<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">

                    <?php do_action('bp_before_member_plugin_template'); ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>

                                <?php bp_get_displayed_user_nav(); ?>

                                <?php do_action('bp_member_options_nav'); ?>

                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <div class="bp_section_title col-sm-3"><?php _e('Commerce', 'eLearning'); ?></div>
                        <ul>
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('setting');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <ul id="subList">
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('setting-commerce');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="padder row">
                        <form action="" method="post" class="<?php echo bp_current_action(); ?>  standard-form col-sm-10" id="settings-form">

                            <?php do_action('bp_account_before_submit'); ?> 
                            <div class="row">
                                <div class="clearfix ">
                                    <label for="enable_paypal" class="bp_checkbox_label col-sm-11"><?php _e('Enable paypal', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="enable_paypal" id="enable_paypal" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="api_username" class="bp_checkbox_label col-sm-9"><?php _e('API username', 'eLearning'); ?></label>
                                    <input class="col-sm-3" type="text" name="api_username" id="api_username" value="">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="api_password" class="bp_checkbox_label col-sm-9"><?php _e('API password', 'eLearning'); ?></label>
                                    <input class="col-sm-3" type="text" name="api_password" id="api_password" value="">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>

                                <div class="clearfix ">
                                    <label for="api_signature" class="bp_checkbox_label col-sm-9"><?php _e('API signature', 'eLearning'); ?></label>
                                    <input class="col-sm-3" type="text" name="api_signature" id="api_signature" value="">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>


                                <br>
                                <br>
                                <div class="clearfix ">
                                    <label for="enable_paypal_advanced" class="bp_checkbox_label col-sm-11"><?php _e('Enable paypal advanced', 'eLearning'); ?></label>
                                    <input class="col-sm-1 checkbox" type="checkbox" name="enable_paypal_advanced" id="enable_paypal_advanced" value="1">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>                          
                                <div class="clearfix ">
                                    <label for="marchent_login" class="bp_checkbox_label col-sm-9"><?php _e('Marchent login', 'eLearning'); ?></label>
                                    <input class="col-sm-3" type="text" name="marchent_login" id="marchent_login" value="">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="partner" class="bp_checkbox_label col-sm-9"><?php _e('Partner', 'eLearning'); ?></label>
                                    <input class="col-sm-3" type="text" name="partner" id="partner" value="">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="user" class="bp_checkbox_label col-sm-9"><?php _e('User', 'eLearning'); ?></label>
                                    <input class="col-sm-3" type="text" name="user" id="user" value="">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="password" class="bp_checkbox_label col-sm-9"><?php _e('Password', 'eLearning'); ?></label>
                                    <input class="col-sm-3" type="text" name="password" id="password" value="">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>
                                <div class="clearfix ">
                                    <label for="reciever_email" class="bp_checkbox_label col-sm-9"><?php _e('Reciever email', 'eLearning'); ?></label>
                                    <input class="col-sm-3" type="text" name="reciever_email" id="reciever_email" value="">
                                    <div class="col-md-12">
                                        <div class="border-bottom-gray"></div>
                                    </div>
                                </div>

                            </div>

                            <div class="submit">
                                <input type="submit" name="bp_peyotto_form_subimitted" value="<?php _e('Save', 'eLearning'); ?>" id="submit" class="auto" />
                            </div>

                            <?php do_action('bp_account_after_submit'); ?>
                            <?php wp_nonce_field('bp_account_general'); ?>

                        </form>
                    </div><!-- .padder -->

                    <?php do_action('bp_after_member_dashboard_template'); ?>

                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
