<?php $current_user_login = wp_get_current_user()->user_login; ?>
<div class="eLearning-website row hs-elearning event-pricing">
    <div class="head">
        <div class="row">
            <div class="col-md-8">
                <h3>Event Name</h3>
            </div>
            <div class="col-md-2">
                <a id="event_edit_save" class="btn btn-lg btn-primary pull-right top-button"  type="button" >SAVE</a>
            </div>
            <div class="col-md-2">
                <a id="event_edit_publish" class="btn btn-lg btn-primar pull-righty top-button" type="button" >PUBLISH</a>
            </div>
        </div>
    </div><!--End head-->
    <div class="event_menu_links">
        <ul class="Form_Customization_nav">
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/entries">GENERAL</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/event-messages">MESSAGES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/event-pricing"><i class="fa fa-caret-up" aria-hidden="true"></i>PRICING</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/event-website">WEBSITE</a></li>
        </ul>
    </div>

             
            <div class="col-lg-10 elearning3Price">
            <div class="free-event"> 
                <h6><b>Event pricing</b></h6>
            </div>     
            <hr/>
            <div class="applyForCourse ">
                <div class="col-lg-6 step2Left">
                    <p><small>Free</small><img class ="imgQues" src="/wp-content/themes/elearning/images/ques.png"></p>
                    
                        <h6><b>Free Event</b></h6>
                    
                </div>
                <div class="col-lg-6 step2Left">                    
                        <div class="switch">
                            <input type="radio" class="switch-input" name="pricing1" value="no-pricing" id="no-pricing" checked>
                            <label for="no-pricing" class="switch-label switch-label-off">No</label>
                            <input type="radio" class="switch-input" name="pricing1" value="yes-pricing" id="yes-pricing">
                            <label for="yes-pricing" class="switch-label switch-label-on">Yes</label>
                            <span class="switch-selection"></span>
                        </div>                   
                </div>        
                 <hr>             
            </div>
              
              <div class="inputs">
                <div class= "col-lg-6 step2Left">
                    <p><small>PRICE</small><img class ="imgQues" src="/wp-content/themes/elearning/images/ques.png"></p>
                    <p><b>Price</b>GBP</p>
                    <input class="step2input" name="eLearning_event_price" type="text">
                </div>
                <div class="col-lg-6 step2Left">
                    <p><small>Sale</small><img class ="imgQues" src="/wp-content/themes/elearning/images/ques.png"></p>
                    <p><b>Sale Price</b>GBP</p>
                    <input class="step2input step2inputright" name="eLearning_event_price_sale" type="text">
                </div>
                </div>
               <hr> 
            <!--End col-lg-12 elearning3Price-->
            
            
            <div class="applyForCourse">
                <div class="col-lg-6 step2Left">
                    <p><small>APPLY FOR COURSE</small><img class ="imgQues" src="/wp-content/themes/elearning/images/ques.png"></p>
                    <h6><b>Invite Student Applications For Course</b></h6>
                </div>
                <div class="col-lg-6 ">
                     <div class="switch">
                        <input type="radio" class="switch-input" name="pricing" value="hidden-pricing" id="hidden-pricing" checked>
                        <label for="hidden-pricing" class="switch-label switch-label-off">Hidden</label>
                        <input type="radio" class="switch-input" name="pricing" value="show-pricing" id="show-pricing">
                        <label for="show-pricing" class="switch-label switch-label-on">Show</label>
                        <span class="switch-selection"></span>
                    </div>  
                </div>          
                <hr>
                </br>
                <hr>
                <button class="savePricing cursor" type="button">SAVE PRICING +</div>
                <hr>
            </div><!--End col-lg-12 applyForCourse-->
            </div><!--End col-lg-10-->
        </div><!--End col-lg-8-->
        <div class="col-lg-2"></div>
    </div><!--End elearning3Step2-->
</div>
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>