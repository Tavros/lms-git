<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php
get_header('buddypress');


$current_user = get_currentuserinfo();
?>

<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">

                    <?php do_action('bp_before_member_plugin_template'); ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>

                                <?php bp_get_displayed_user_nav(); ?>

                                <?php do_action('bp_member_options_nav'); ?>

                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <div class="bp_section_title col-sm-3"><?php _e('Account', 'eLearning'); ?></div>
                        <ul>
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('account');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="padder row">
                        <form action="<?php echo bp_displayed_user_domain() . ELEARNING_ACCOUNT_SLUG . '/' . ELEARNING_ACCOUNT_GENETRAL_SLUG; ?>" method="post" class="<?php echo bp_current_action(); ?> standard-form col-sm-10" id="settings-form">
                            <label class="bp_checkbox_label col-sm-10"><strong><?php _e('Notify me for:', 'eLearning'); ?></strong></label>
                            <label for="announcments" class="bp_checkbox_label col-sm-10"><?php _e('Announcments', 'eLearning'); ?></label><input class="col-sm-2 checkbox" type="checkbox" name="announcments" id="announcments" value="1">
                            <label for="new_messages" class="bp_checkbox_label col-sm-10"><?php _e('New messages', 'eLearning'); ?></label><input class="col-sm-2 checkbox" type="checkbox" name="new_messages" id="new_messages" value="1">
                            <label for="coutse_progress_update" class="bp_checkbox_label col-sm-10"><?php _e('Coutse progress update', 'eLearning'); ?></label><input class="col-sm-2 checkbox" type="checkbox" name="coutse_progress_update" id="coutse_progress_update" value="1">
                            <label for="promotions" class="bp_checkbox_label col-sm-10"><?php _e('Promotions', 'eLearning'); ?></label><input class="col-sm-2 checkbox" type="checkbox" name="promotions" id="promotions" value="1">
                            <div class="clearfix"></div>
                            <br>
                            <label class="bp_checkbox_label col-sm-10"><strong><?php _e('By', 'eLearning'); ?></strong></label>
                            <label for="by_email" class="bp_checkbox_label col-sm-10"><?php _e('Email', 'eLearning'); ?></label><input class="col-sm-2 checkbox" type="checkbox" name="by_email" id="by_email" value="1">
                            <label for="by_push_notification" class="bp_checkbox_label col-sm-10"><?php _e('Push notification', 'eLearning'); ?></label><input class="col-sm-2 checkbox" type="checkbox" name="by_push_notification" id="by_push_notification" value="1">
                            <div class="submit col-sm-12">
                                <input type="submit" name="bp_peyotto_form_subimitted" value="<?php _e('Save', 'eLearning'); ?>" id="submit" class="auto" />
                            </div>

                            <?php do_action('bp_account_after_submit'); ?>
                            <?php wp_nonce_field('bp_account_profile'); ?>

                        </form>
                    </div><!-- .padder -->

                    <?php do_action('bp_after_member_dashboard_template'); ?>

                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
