<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php get_header('buddypress'); ?>

<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">

                    <?php do_action('bp_before_member_plugin_template'); ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>

                                <?php bp_get_displayed_user_nav(); ?>

                                <?php do_action('bp_member_options_nav'); ?>

                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <div class="bp_section_title col-sm-3"><?php _e('Account', 'eLearning'); ?></div>
                        <ul>
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('account');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="padder row">
                        <form action="<?php echo bp_displayed_user_domain() . ELEARNING_ACCOUNT_SLUG . '/' . ELEARNING_ACCOUNT_GENETRAL_SLUG; ?>" method="post" class="<?php echo bp_current_action(); ?>  standard-form col-sm-10" id="settings-form">
                            <!-- <?php if (!is_super_admin()) : ?>
                                <label for="pwd"><?php _e('Current Password <span>(required to update email or change current password)</span>', 'eLearning'); ?></label>
                                <input type="password" name="pwd" id="pwd" size="16" value="" class="settings-input small" /> &nbsp;<a href="<?php echo wp_lostpassword_url(); ?>" title="<?php _e('Password Lost and Found', 'eLearning'); ?>"><?php _e('Lost your password?', 'eLearning'); ?></a>
                            <?php endif; ?> -->


                            <!-- <label for="pass1"><?php _e('Change Password <span>(leave blank for no change)</span>', 'eLearning'); ?></label>
                            &nbsp;<?php _e('New Password', 'eLearning'); ?><br />
                            <input type="password" name="pass1" id="pass1" size="16" value="" class="bp_section_input" />
                            &nbsp;<?php _e('Repeat New Password', 'eLearning'); ?>
                            <input type="password" name="pass2" id="pass2" size="16" value="" class="bp_section_input" />

                            <?php do_action('bp_account_before_submit'); ?> -->
                            <label for="email" class="bp_section_label"><?php _e('Email', 'eLearning'); ?></label>
                            <input  type="text" name="email" id="email" value="<?php echo bp_get_displayed_user_email(); ?>" class="form-control bp_section_input" />
                            <div class="input-group">
                              <label for="password" class="bp_section_label"><?php _e('Reset password', 'eLearning'); ?></label>
                              <input type="password" name="password" id="password" value="" class="form-control bp_section_input" />
                              <span class="input-group-btn">
                                <button class="btn_change btn btn-default" type="button"><?php _e('change', 'eLearning'); ?></button>
                              </span>
                            </div>

                            <label for="facebook_url" class="bp_section_label"><?php _e('Facebook Url', 'eLearning'); ?></label>
                            <input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_user_meta(get_current_user_id(), 'facebook_url', true); ?>" class="form-control bp_section_input" />
                            <label for="linkedin_url" class="bp_section_label"><?php _e('LinkedIn Url', 'eLearning'); ?></label>
                            <input type="text" name="linkedin_url" id="linkedin_url" value="<?php echo get_user_meta(get_current_user_id(), 'linkedin_url', true); ?>" class="form-control bp_section_input" />

                            <div class="submit">
                                <input type="submit" name="bp_peyotto_form_subimitted" value="<?php _e('Save', 'eLearning'); ?>" id="submit" class="auto" />
                            </div>

                            <?php do_action('bp_account_after_submit'); ?>
                            <?php wp_nonce_field('bp_account_general'); ?>

                        </form>
                    </div><!-- .padder -->

                    <?php do_action('bp_after_member_dashboard_template'); ?>

                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
