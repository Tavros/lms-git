<?php
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
?>
<?php
get_header('buddypress');


$current_user = get_currentuserinfo();
?>

<section id="content">
    <div id="buddypress">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">

                    <?php do_action('bp_before_member_plugin_template'); ?>
                    <div class="pagetitle">
                        <div id="item-header">
                            <?php locate_template(array('members/single/member-header.php'), true); ?>
                        </div><!-- #item-header -->
                    </div>
                    <div id="item-nav">
                        <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                            <ul>

                                <?php bp_get_displayed_user_nav(); ?>

                                <?php do_action('bp_member_options_nav'); ?>

                            </ul>
                        </div>
                    </div><!-- #item-nav -->
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="item-list-tabs no-ajax <?php if (!bp_is_my_profile()) echo 'notmyprofile'; ?>" id="subnav" role="navigation">
                        <div class="bp_section_title col-sm-3"><?php _e('Account', 'eLearning'); ?></div>
                        <ul>
                            <?php
                            if (bp_is_my_profile()) {
                                bp_get_options_nav('account');
                            }
                            do_action('bp_course_get_options_sub_nav');
                            ?>
                        </ul>
                    </div><!-- .item-list-tabs -->
                    <div class="padder row">
                        <form action="<?php echo bp_displayed_user_domain() . ELEARNING_ACCOUNT_SLUG . '/' . ELEARNING_ACCOUNT_GENETRAL_SLUG; ?>" method="post" class="<?php echo bp_current_action(); ?> standard-form col-sm-10" id="settings-form">
                            <div class="bp_profile_img col-sm-3">
                              <?php bp_loggedin_user_avatar( 'type=full' ); ?>
                              <a href="#">
                                <div class="edit-avatar">
                                  <?php _e('edit', 'eLearning'); ?>
                                </div>
                              </a>
                            </div>
                            <label for="name" class="bp_section_label"><?php _e('Name', 'eLearning'); ?></label>
                            <input type="text" name="name" id="name" value="<?php bp_loggedin_user_fullname(); ?>" class="bp_profile_name bp_section_input col-sm-9" />

                            <label for="title" class="bp_section_label"><?php _e('Title', 'eLearning'); ?></label>
                            <input type="text" name="title" id="title" value="<?php echo get_user_meta($current_user->ID, 'title', true); ?>" class="col-sm-9 bp_profile_name bp_section_input" />

                            <label for="bio" class="col-sm-12 bp_section_label"><?php _e('BIO', 'eLearning'); ?></label>
                            <textarea rows="10" cols="45" name="bio" id="bio" class="form-control bp_section_textarea"><?php echo get_user_meta(get_current_user_id(), 'linkedin_url', true); ?></textarea>



                            <label for="website" class="bp_section_label"><?php _e('Website', 'eLearning'); ?></label>
                            <input type="text" name="website" id="website" value="<?php echo get_user_meta(get_current_user_id(), 'website', true); ?>" class="form-control bp_section_input" />

                            <label for="location" class="bp_section_label"><?php _e('Location', 'eLearning'); ?></label>
                            <input type="text" name="location" id="location" value="<?php echo get_user_meta(get_current_user_id(), 'location', true); ?>" class="form-control bp_section_input" />

                            <label for="language" class="bp_section_label"><?php _e('Language', 'eLearning'); ?></label>
                            <input type="text" name="language" id="language" value="<?php echo get_user_meta(get_current_user_id(), 'language', true); ?>" class="form-control bp_section_input" />
                            <div class="submit">
                                <input type="submit" name="bp_peyotto_form_subimitted" value="<?php _e('Save', 'eLearning'); ?>" id="submit" class="auto" />
                            </div>

                            <?php do_action('bp_account_after_submit'); ?>
                            <?php wp_nonce_field('bp_account_profile'); ?>

                        </form>
                    </div><!-- .padder -->

                    <?php do_action('bp_after_member_dashboard_template'); ?>

                </div>
            </div><!-- #content -->
        </div>
    </div>
</section>
</div> <!-- Extra Global div in header -->
<?php get_footer('buddypress'); ?>
