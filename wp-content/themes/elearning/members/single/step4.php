<?php


email_customization_shortcode();




            $meta = get_post_meta( $post->ID, '_template_meta', true );

            $default = array(
                'txt_color_default'            => '#000000',
                'txt_color'                    => '#000000',
                'bg_color_default'             => '#F5F5F5',
                'bg_color'                     => '#F5F5F5',
                'base_color_default'           => '#2470FF',
                'base_color'                   => '#2470FF',
                'body_color_default'           => '#FFFFFF',
                'body_color'                   => '#FFFFFF',
                'logo_url'                     => '',
                'custom_logo_url'              => get_option( 'yith-wcet-custom-default-header-logo' ),
                'page_width'                   => '800',
                // - - - - -      P R E M I U M      - - - - -
                'logo_height'                  => '100',
                'header_padding'               => array( 36, 48, 36, 48 ),
                'premium_mail_style'           => 'default',
                'footer_logo_url'              => '',
                'header_position'              => 'center',
                'header_color'                 => 'transparent',
                'header_color_default'         => 'transparent',
                'footer_text_color'            => '#555555',
                'footer_text_color_default'    => '#555555',
                'page_border_radius'           => '3',
                'h1_size'                      => '30',
                'h2_size'                      => '18',
                'h3_size'                      => '16',
                'body_size'                    => '14',
                'body_line_height'             => '20',
                'table_border_width'           => '1',
                'table_border_color'           => '#555555',
                'table_border_color_default'   => '#555555',
                'table_bg_color'               => '#FFFFFF',
                'table_bg_color_default'       => '#FFFFFF',
                'price_title_bg_color'         => '#FFFFFF',
                'price_title_bg_color_default' => '#FFFFFF',
                'show_prod_thumb'              => '',
                'footer_text'                  => '',
                'socials_on_header'            => 0,
                'socials_on_footer'            => 0,
                'socials_color'                => 'black',
                'custom_links'                 => array(),
            );

            $args = wp_parse_args( $meta, $default );
            
           
            $args = apply_filters( 'yith_wcet_metabox_options_content_args', $args );

            yith_wcet_metabox_options_content_premium( $args );

    //     <script type='text/javascript'> 
    // /* <![CDATA[ */
    // var ajax_object = {"assets_url":"http:\/\/lms01.first.am\/wp-content\/plugins\/yith-woocommerce-email-templates-premium\/assets","wp_ajax_url":"http:\/\/lms01.first.am\/wp-admin\/admin-ajax.php"};
    // /* ]]> */
    // </script>
            ?>


