<?php
/**
 * BuddyPress - Users Profile
 *
 * @package BuddyPress
 * @subpackage bp-default
 */
//
//error_reporting(E_ALL);
//$event_id = @$_REQUEST['id'];
$url = $_SERVER['REQUEST_URI'];
$current_user_login = wp_get_current_user()->user_login;

if(isset($url) && $url != '' ) {

    $file_name_array = explode('/', $url);

    $event_id = (int)$file_name_array[3];
}

/*check if exists id then call edit action*/
if( !empty($event_id) && is_numeric ($event_id))
{
    //Get All Students
    $args = array(
        'meta_key'  => $event_id
    );
    $studentsData = get_users($args);

    $current_event = get_post($event_id);
    $current_event_custom_fields = get_post_meta($event_id);
    if(isset($current_event_custom_fields['eLearning_pmpro_membership'][0]) && !empty($current_event_custom_fields['eLearning_pmpro_membership'][0])){
        $currentPostMemberLevel = $current_event_custom_fields['eLearning_pmpro_membership'][0];
    }
    else{
        $currentPostMemberLevel = '';
    }
    if(isset($current_event_custom_fields['eLearning_instructor'][0]) && !empty($current_event_custom_fields['eLearning_instructor'][0])){
        $currentEventInstructor = $current_event_custom_fields['eLearning_instructor'][0];
    }
    else{
        $currentEventInstructor = '';
    }

    $event_img_url = wp_get_attachment_url( get_post_thumbnail_id($event_id), 'thumbnail' );

    $currentPostUnitId = unserialize($current_event_custom_fields['eLearning_course_curriculum'][0])[0];

    $units = get_post_meta( $currentPostUnitId );

    $unitType = $units['eLearning_type'][0];

}
else{
    $current_user = wp_get_current_user();
    $create_new_event = array(
        'post_title' => 'No name',
        'post_type'  => 'course',
        'author'     => $current_user->data->ID
    );
    wp_insert_post($create_new_event);

    $args = array(
        'posts_per_page'   => 1,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'course',
        'post_status'      => 'draft',
        'author'           =>  $current_user->data->ID
    );
    $addedEvent = get_posts( $args );


}

$args = array(
    'post_type'   => 'page'
);
$redirectPages = get_posts( $args );

$levels = $wpdb->get_results("SELECT id, name FROM $wpdb->pmpro_membership_levels ORDER BY name");

$unit_types = apply_filters('eLearning_unit_types', array(
    array('label' => __('Video', 'eLearning-customtypes'), 'value' => 'play'),
    array('label' => __('Audio', 'eLearning-customtypes'), 'value' => 'music-file-1'),
    array('label' => __('Podcast', 'eLearning-customtypes'), 'value' => 'podcast'),
    array('label' => __('General', 'eLearning-customtypes'), 'value' => 'text-document'),
    array('label' => __('Webinar', 'eLearning-customtypes'), 'value' => 'webinar'),
));
//Get All Instructors
$args = array(
    'role'         => 'instructor'
);
$instructorsData = get_users($args);

?>

    <div class="eLearning-website row hs-elearning edit_event">
    <div class="head">
        <div class="row">
            <div class="col-md-8">
                <h3>Event Name</h3>
            </div>
            <div class="col-md-2">
                <a id="event_edit_save" class="btn btn-lg btn-primary pull-right top-button"  type="button" >SAVE</a>
            </div>
            <div class="col-md-2">
                <a id="event_edit_publish" class="btn btn-lg btn-primar pull-righty top-button" type="button" >PUBLISH</a>
            </div>
        </div>
    </div><!--End head-->
     
     
     <div class="edit_events_ando">
  <div class="row">
    <p class="col-md-12 font_h1"><span>STEP 5:</span> DESIGN YOUR EVENT PAGES</p>
    <div class="col-md-12">
      <div class="col-md-3">
        <img class="elearning3Step1" id="change_img" src="/wp-content/themes/elearning/images/Rounded-Rectangle-28.png">
      </div>
      <div class="col-md-9">
        <p class="font_h2">SALES / REGISTRATION PAGE</p>
        <p>Create a custom landing page for your event. The landing page is a public page that can be used to advertise your event, capture leads, and sell tickets.</p>
        <div class="switch_connect">
          <span class=" col-md-6">ACTIVATE SALES PAGE</span>
          <label class="switch col-md-6  ">
            <input type="checkbox">
            <div class="slider round"></div>
          </label>
        </div>
      </div>
      <div class="col-md-12">
        <div class="switch_w">
          <span>TITLE</span>
          <input type="text" name="">
          <button>EDIT <i class="fa fa-pencil" aria-hidden="true"></i>
          </button>
        </div>
      </div>
      <p class="text_left">ONLY SHOW TITLE IF THANK YOU PAGE IS ACTIVATED</p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-3">
        <img class="elearning3Step1" id="change_img" src="/wp-content/themes/elearning/images/Rounded-Rectangle-30.png">
      </div>
      <div class="col-md-9">
        <p class="font_h2">EVENT PAGE</p>
        <p>By default, webinars and streaming media are opened in a new window. If you would like to create a page and embed the webinar, check here.</p>
        <div class="switch_connect">
          <span class=" col-md-6">OPEN WEBINAR OR WEBCAST IN NEW WINDOW</span>
          <label class="switch col-md-6  ">
            <input type="checkbox">
            <div class="slider round"></div>
          </label>
        </div>
      </div>
      <div class="col-md-12">
        <div class="switch_w">
          <button>EDIT <i class="fa fa-pencil" aria-hidden="true"></i>
          </button>
        </div>
      </div>

    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-3">
        <img class="elearning3Step1" id="change_img" src="/wp-content/themes/elearning/images/Rounded-Rectangle-31.png">
      </div>
      <div class="col-md-9">
        <p class="font_h2">THANK YOU / INSTRUCTION PAGE</p>
        <p>Upon successful registration, direct your users to a custom thank you page instead of the event page. This could be very useful when pre-registering users.</p>
        <div class="switch_connect">
          <span class=" col-md-6">SHOW THANK YOU PAGE AFTER REGISTRATION</span>
          <label class="switch col-md-6  ">
            <input type="checkbox">
            <div class="slider round"></div>
          </label>
        </div>
      </div>
      <div class="col-md-12">
        <div class="switch_w">
          <span>TITLE</span>
          <input type="text" name="">
          <button>EDIT <i class="fa fa-pencil" aria-hidden="true"></i>
          </button>
        </div>
      </div>
      <p class="text_left">ONLY SHOW TITLE IF THANK YOU PAGE IS ACTIVATED</p>
    </div>
  </div>
</div>

    
    <div class="event_menu_links">
        <ul class="Form_Customization_nav">
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login; ?>/entries"><i class="fa fa-caret-up" aria-hidden="true"></i>GENERAL</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login; ?>/event-messages">MESSAGES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login; ?>/event-pricing">PRICING</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login; ?>/event-website">WEBSITE</a></li>
        </ul>
    </div>

    <div class="container event_edit_content">
        <form method="POST" id="edit_event_form" enctype="multipart/form-data">
            <input class="checker" type="hidden" name="updated" value="0">
            <div id="elearning3Step1" >
                <!--  <div class="evantTitle col-lg-1"></div> -->
                <div class="evantTitle col-lg-8">
                    <h6 class="h6color">EVENT TITLE</h6>
                    <input class="step1Iput editEvent" type="text" name="post_title" value="<?php  if($event_id)echo $current_event->post_title;?>"/>
                    <div class="col-lg-6 left">
                        <div class="startDate">
                            <h6 class="h6color" >START DATE</h6>
                            <input id="datepicker" class="eventDesc" type="text" name="eLearning_start_date" value="<?php if($event_id)echo $current_event_custom_fields['eLearning_start_date'][0]?>">
                        </div>
                        <div class="expDuration">
                            <h6 class="h6color">COURSE DURATION</h6>
                            <input class="eventDesc elearning3Step1" type="text" name="eLearning_duration" value="<?php if($event_id) echo $current_event_custom_fields['eLearning_duration'][0]?>" />
                        </div>
                        <div>
                            <h6 class="h6color">REGULAR PRICE</h6>
                            <?php $regularPrice = ''; if(isset($current_event_custom_fields['_regular_price'][0]) && !empty($current_event_custom_fields['_regular_price'][0])){$regularPrice = $current_event_custom_fields['_regular_price'][0];} ?>
                            <input class="maxcap" type="text" value="<?php echo $regularPrice; ?>" name="_regular_price" >
                        </div>
                    </div><!--End col-lg-6 left-->
                    <div class="col-lg-6 right">
                        <div class="startTime elearning3Step1">
                            <h6 class="h6color">START TIME</h6>
                            <input class="eventDesc" type="text" name="eLearning_start_time" value="<?php if($event_id) echo $current_event_custom_fields['eLearning_start_time'][0]?>">
                        </div>
                        <!--div class="WebinarID">
                    <h6 class="h6color">WEBINAR ID</h6>
                    <input class="eventDesc" type="text" name="eLearning_webinar_id" value="<?php // echo $current_event_custom_fields['eLearning_webinar_id'][0] ?>">
                </div-->
                        <div class="max-cap">
                            <h6 class="h6color">MAXIMUM CAPACITY</h6>
                            <?php if(isset($current_event_custom_fields['eLearning_maximum_students'][0]) && !empty($current_event_custom_fields['eLearning_maximum_students'][0])){$maxStudents = $current_event_custom_fields['eLearning_maximum_students'][0];} ?>
                            <input class="maxcap" type="text" value="<?php echo $maxStudents; ?>" name="eLearning_maximum_students" >
                        </div>
                        <span class="leave-blann">Leave blann for no limit to the number of users that can attend this event.</span>
                        <div>
                            <h6 class="h6color">SALE PRICE</h6>
                            <?php $salePrice = ''; if(isset($current_event_custom_fields['_sale_price'][0]) && !empty($current_event_custom_fields['_sale_price'][0])){$salePrice = $current_event_custom_fields['_sale_price'][0];} ?>
                            <input class="maxcap" type="text" value="<?php echo $salePrice; ?>" name="_sale_price" >
                        </div>
                    </div><!--End col-lg-6 right-->
                    <div class="col-lg-2"></div>
                    <div class="col-lg-12 select">
                        <div class="col-lg-6 elearning3Step1 select">
                            <h6 class="h6color">INSTRUCTOR</h6>
                            <select class="step1Select" id="eLearning_instructor" name="eLearning_instructor">
                                <option value="default">Select a Instructor</option>
                                <?php foreach ($instructorsData as $instructorData): ?>
                                    <option value="<?php echo $instructorData->data->ID; ?>" <?php if($currentEventInstructor == $instructorData->data->ID){echo 'selected="selected"';} ?>><?php echo $instructorData->data->user_nicename; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <h6 class="h6color"> EVENT STATUS</h6>
                            <div>
                                <div class="switch">
                                    <input type="radio" id="eventStatisSwitchL" class="switch-input" name="eLearning_event_status" value="Inactive" <?php if($event_id)if($current_event_custom_fields['elearning_events_status'][0] == 'Inactive'):?> checked<?php endif;?>/>
                                    <label for="eventStatisSwitchL" class="switch-label switch-label-off">INACTIVE</label>
                                    <input type="radio" id="eventStatisSwitchR" class="switch-input" name="eLearning_event_status" value="Active" <?php if($event_id)if($current_event_custom_fields['elearning_events_status'][0] == 'Active'):?> checked<?php endif;?> />
                                    <label for="eventStatisSwitchR" class="switch-label switch-label-on">ACTIVE</label><span class="switch-selection"></span>
                                </div>
                            </div>
                            <h6 class="h6color">EVENT LEVEL</h6>
                            <select class="step1Select" id="eLearning_pmpro_membership" name="eLearning_pmpro_membership">
                                <option value="default">Select a Event Level</option>
                                <?php foreach ($levels as $level): ?>
                                    <?php /*
                                    echo"<pre>"; print_r($level);
                                    echo "</br>";
                                    echo "-------------------------------------";
                                    echo "</br>"; */

                                    ?>
                                    <option value="<?php echo $level->id ?>" <?php if($currentPostMemberLevel == $level->id){echo 'selected="selected"';} ?>><?php echo $level->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <input type="hidden" name="eLearning_course_curriculum" value="<?php echo serialize($level); ?>" />
                            <h6 class="h6color">EVENT TYPE</h6>
                            <select class="step1Select" id="eLearning_type" name="eLearning_type">
                                <option value="default">Select a Event Type</option>
                                <?php foreach ($unit_types as $unit_type): ?>
                                    <option value="<?php echo $unit_type['value'] ?>" <?php if($unitType == $unit_type['value']) { echo'selected="selected"';} ?> ><?php echo $unit_type['label']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="webinarId_webinarRecorder" style="display: none;">
                                <label for="eLearning_webinar_id">Webinar Id</label>
                                <input type="text" id="eLearning_webinar_id" name="eLearning_webinar_id" value="<?php echo $current_event_custom_fields['eLearning_webinar_id'][0] ?>">
                                </br>
                                <label for="eLearning_webinar_recorder">Webinar Recorder</label>
                                <input type="text" id="eLearning_webinar_recorder" name="eLearning_webinar_recorder" value="<?php echo $current_event_custom_fields['eLearning_webinar_recorder'][0] ?>">
                            </div>
                            <h6 class="h6color"> EVENT IS ALREDY RECORDED </h6>
                            <div>
                                <div class="switch">

                                    <input type="radio" id="switch_leftt" class="switch-input" <?php if($current_event_custom_fields['event_recorded_switch'][0] == 'n'){echo "checked";} ?> name="event_recorded_switch" value="n"/>
                                    <label for="switch_leftt" class="switch-label switch-label-off">No</label>

                                    <input type="radio" class="switch-input" id="switch_rightt" <?php if($current_event_custom_fields['event_recorded_switch'][0] == 'y'){echo "checked";} ?> name="event_recorded_switch" value="y"/>
                                    <label for="switch_rightt" class="switch-label switch-label-on">Yes</label>
                                    <span class="switch-selection"></span>
                                </div>
                            </div>
                            <!--                        Get and Set categories for event-->
                            <?php
                            $get_all_categories = get_terms( array(
                                'taxonomy' => 'course-cat',
                                'hide_empty' => false,
                            ) );
                            if(isset($event_id) && !empty($event_id)){
                                $get_the_post_category = wp_get_object_terms($event_id,'course-cat');
                            }
                            else {
                                $get_the_post_category = '';

                            }

                            isset($get_the_post_category[0]) && !empty($get_the_post_category[0])? $currentPostCategory = $get_the_post_category[0] : $currentPostCategory = '';
                            ?>
                            <h6 class="h6color">EVENT CATEGORY</h6>
                            <select class="step1Select" name="eLearning_event_category">

                                <?php foreach ($get_all_categories as $cat):?>
                                    <?php if($get_the_post_category[0]->term_id ==  $cat->term_id) { ?>
                                        <option value="<?php  echo $get_the_post_category[0]->term_id; ?>" selected="selected"> <?php  echo $get_the_post_category[0]->name;?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo $cat->term_id; ?>"> <?php  echo $cat->name;?></option>
                                    <?} ?>
                                <?php endforeach;?>

                            </select>

                            <h6 class="h6color">REDIRECT URL</h6>
                            <select class="step1Select" name="eLearning_redirect_url">
                                <option value="">Choose redirect page</option>
                                <?php foreach ($redirectPages as $redirectPage):?>
                                    <option <?php if($current_event_custom_fields['eLearning_redirect_url'][0] == $redirectPage->guid){ echo "selected='selected'"; } ?> value="<?php echo $redirectPage->guid; ?>"> <?php  echo $redirectPage->post_title;?></option>
                                <?php endforeach;?>
                            </select>

                        </div><!--End col-lg-6 elearning3Step1 select-->
                        <div class="col-lg-6"></div>
                        <div class="col-lg-12 description">
                            <div class="col-lg-3 watch">
                                <h6 class="h6color">EVENT IMAGE</h6>
                                <?php if(!empty($event_img_url)): ?>
                                    <img class="elearning3Step1" id="change_img" src="<?php if($event_id)echo $event_img_url;?>">
                                <?php else: ?>
                                    <img class="elearning3Step1" id="change_img" src="/wp-content/themes/elearning/images/book-11433-300x300.jpg">
                                <?php endif; ?>
                            </div>

                            <div class="col-lg-9 elearning3Step1">
                                <h6 class="h6color">EVENT SHORT DESCRIPTION </h6>
                                <textarea  rows="9" cols="50" name="eLearning_event_sort_description"><?php if($event_id)echo $current_event_custom_fields['eLearning_course_instructions'][0];?></textarea>
                                <h6 class="add h6color"> ADD FULL DESCRIPTION </h6>
                            </div>
                        </div><!--End col-lg-12 description-->
                    </div><!--End col-lg-12 select-->

                    <input type="submit" id="edit_event_form_submit" class="btn" value="SAVE EVENT +" />

                </div><!--End evantTitle col-lg-10-->
            </div><!--End elearning3Step1-->

        </form>
    </div>
    <?php
    eLearning_include_template( "profile/bottom.php" );
    get_footer( eLearning_get_footer() );
    ?>
    <script>
        jQuery(document).ready(function(){
            jQuery("#back").click(function(){
                jQuery("#elearning3Step2").addClass("elearning3Step2");
                jQuery("#elearning3Step1").removeClass("elearning3Step");
            });

            jQuery("#imgButton").click(function(){
                jQuery("#elearning3Step1").addClass("elearning3Step");
                jQuery("#elearning3Step2").removeClass("elearning3Step2");

            });
            jQuery( "#datepicker" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });

            var unitType = jQuery('#eLearning_type option:selected').val();
            if(unitType == 'webinar'){
                jQuery('.webinarId_webinarRecorder').css('display', 'block');
            }
            jQuery('#eLearning_type').change( function(){
                if(jQuery(this).val() == 'webinar'){
                    jQuery('.webinarId_webinarRecorder').css('display', 'block');
                }
                else{
                    jQuery('.webinarId_webinarRecorder').css('display', 'none');
                }
            });

        });

        jQuery(document).ready(function() {
            jQuery("#event_edit_save").click(function() {
                jQuery('.checker').val(1);
                jQuery("#edit_event_form").submit();


            });
            jQuery('#edit_event_form_submit').click(function (){
                jQuery('.checker').val(1);
            });

//        jQuery("#edit_event_form").submit(function(){
//            var test = jQuery('#edit_event_form').serialize();
//            console.log(test);
//            jQuery.post({url: window.location.href, data: test, success: function(result){
//                location.reload();
//            }});
//        });
        });

    </script>
    <script>
        /*    var mediaUploader;

         jQuery('#change_img').click(function(e) {
         e.preventDefault();
         // If the uploader object has already been created, reopen the dialog
         if (mediaUploader) {
         mediaUploader.open();
         return;
         }
         // Extend the wp.media object
         mediaUploader = wp.media.frames.file_frame = wp.media({
         title: 'Choose Image',
         button: {
         text: 'Choose Image'
         }, multiple: false });

         // When a file is selected, grab the URL and set it as the text field's value
         mediaUploader.on('select', function() {
         var attachment = mediaUploader.state().get('selection').first().toJSON();
         jQuery('#image-url').val(attachment.url);
         });
         // Open the uploader dialog
         mediaUploader.open();
         }); */



        // Uploading files
        var media_uploader;
        jQuery('#change_img').on('click', function (event) {

            var button = jQuery(this);
            if (media_uploader) {
                media_uploader.open();
                return;
            }
            // Create the media uploader.
            media_uploader = wp.media.frames.media_uploader = wp.media({
                title: button.data('uploader-title'),
                // Tell the modal to show only images.
                library: {
                    type: 'image',
                    query: false
                },
                button: {
                    text: button.data('uploader-button-text'),
                },
                multiple: button.data('uploader-allow-multiple')
            });

            // Create a callback when the uploader is called
            media_uploader.on('select', function () {
                var selection = media_uploader.state().get('selection'),
                    input_name = "change_event_image";
                selection.map(function (attachment) {
                    attachment = attachment.toJSON();
                    console.log(attachment);
                    var url_image = '';
                    if (attachment.sizes) {
                        if (attachment.sizes.thumbnail !== undefined)
                            url_image = attachment.sizes.thumbnail.url;
                        else if (attachment.sizes.medium !== undefined)
                            url_image = attachment.sizes.medium.url;
                        else
                            url_image = attachment.sizes.full.url;
                    }

                    button.html('<img src="' + url_image + '" class="submission_thumb thumbnail" /><input id="' + input_name + '" class="post_field" data-type="featured_image" data-id="' + input_name + '" name="' + input_name + '" type="hidden" value="' + attachment.id + '" />');
                });

            });
            // Open the uploader
            media_uploader.open();
        });

    </script>

<?php
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------UPDATE EVENT---------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------*/
isset($_POST['post_title']) && !empty($_POST['post_title']) ? $postTitle = $_POST['post_title'] : $postTitle = '';
$atts_event_post = array(
    'ID'=> $event_id,
    'post_title' => $postTitle,

);


if(isset($_REQUEST['updated']) && $_REQUEST['updated'] == 1 && $event_id){

    isset($_POST['post_title']) && !empty($_POST['post_title']) ? $postTitle = $_POST['post_title'] : $postTitle = '';
    if(isset($_POST['eLearning_event_category']) && !empty($_POST['eLearning_event_category'])){
        $i = trim($_POST['eLearning_event_category']);
        wp_set_object_terms($event_id,array('tag_ID' => (int)$i), 'course-cat');
    }
    wp_update_post($atts_event_post);
    /* Update Custom fields*/

    if(isset($_POST['change_event_image']) && $_POST['change_event_image'] != '' ) {
        update_post_meta($event_id, '_thumbnail_id', $_POST['change_event_image']);
    }

    update_post_meta($event_id,'eLearning_start_date',$_POST['eLearning_start_date']);
    update_post_meta($event_id,'eLearning_start_time',$_POST['eLearning_start_time']);
    update_post_meta($event_id,'eLearning_duration',$_POST['eLearning_duration']);
    update_post_meta($event_id,'elearning_events_status',$_POST['eLearning_event_status']);
    update_post_meta($event_id,'elearning_events_status',$_POST['eLearning_event_status']);
    update_post_meta($event_id,'eLearning_course_instructions',$_POST['eLearning_event_sort_description']);
    update_post_meta($event_id,'eLearning_course_instructions',$_POST['eLearning_event_sort_description']);
    update_post_meta($event_id,'eLearning_pmpro_membership',serialize($_POST['eLearning_pmpro_membership']));
    update_post_meta($event_id,'eLearning_maximum_students',$_POST['eLearning_maximum_students']);
    update_post_meta($event_id,'eLearning_webinar_recorder',$_POST['eLearning_webinar_recorder']);
    update_post_meta($event_id,'eLearning_course_curriculum',serialize($_POST['eLearning_course_curriculum']));
    update_post_meta($currentPostUnitId,'eLearning_type',$_POST['eLearning_type']);
    update_post_meta($event_id,'_regular_price',$_POST['_regular_price']);
    update_post_meta($event_id,'_sale_price',$_POST['_sale_price']);
    if(($_POST['_regular_price'] == 0 || empty($_POST['_regular_price'])) && ($_POST['_sale_price'] == 0) || empty($_POST['_sale_price'])){
        update_post_meta($event_id,'eLearning_course_free','S');
    }
    update_post_meta($event_id,'eLearning_instructor',$_POST['eLearning_instructor']);


    //Check If webinar id is present, then go and Create new Webinar in Onstream
    if(isset($_POST['eLearning_type']) && !empty($_POST['eLearning_type'])){
        require_once( $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/elearning/eLearning-addons/onstreamAPI/functions.php' );

        $post_settings = array();
        $post_metas = '';
        $webinarId = '';
        $instructorId = '';
        $isWebinarChecker = false;
        //Check If event type is webinar or not
        if(isset($_POST['eLearning_webinar_id']) && !empty($_POST['eLearning_webinar_id'])){
            $webinarId = $_POST['eLearning_webinar_id'];
            $isWebinarChecker = true;
        }
        //Check If event instructor isset
        if(isset($_POST['eLearning_instructor']) && !empty($_POST['eLearning_instructor'])){
            $instructorId = $_POST['eLearning_instructor'];
        }

        $post_settings['post_title'] = $postTitle;
        $post_settings['eLearning_expected_duration'] = $_POST['eLearning_duration'];
        $post_settings['eLearning_start_date'] = $_POST['eLearning_start_time'];
        //Check if event type webinar and webinar id isset or not
        if ($isWebinarChecker == true){
            //Set Webinar too tegistered user
            foreach ($studentsData as $studentData){
                SetStudentAPI( $webinarId, $studentData->data->ID);
                getUsersVideoLinks($webinarId, $studentData->data->ID);
            }
        }
        else{
            //Create webinar and Set Webinar too tegistered user
            $data = createNewWebinarInOnstream($post_settings, $post_metas, $instructorId, $webinarId);
            $webinarId = $data['webinarId'];
            $addedInstructorId = $data['addedInstructorId'];
            add_post_meta($event_id,'eLearning_webinar_id',$webinarId);
            foreach ($studentsData as $studentData){
                SetStudentAPI( $webinarId, $studentData->data->ID);
                getUsersVideoLinks($webinarId, $studentData->data->ID);
            }
        }

    }
    if (isset($_POST['event_recorded_switch']) && !empty($_POST['event_recorded_switch'])){
        if(!get_post_meta($event_id, 'eLearning_start_time')){
            add_post_meta($event_id,'event_recorded_switch',$_POST['event_recorded_switch']);
        }
        else{
            update_post_meta($event_id,'event_recorded_switch',$_POST['event_recorded_switch']);
        }
    }
    if(!get_post_meta($event_id, 'eLearning_redirect_url')){
        add_post_meta($event_id,'eLearning_redirect_url',$_POST['eLearning_redirect_url']);
    }
    else{
        update_post_meta($event_id,'eLearning_redirect_url',$_POST['eLearning_redirect_url']);
    }

    if(isset($_POST['eLearning_redirect_url']) && !empty($_POST['eLearning_redirect_url'])){
        header("Location: ".$_POST['eLearning_redirect_url']."");
    }
    else{
        header("Refresh: 1");
    }


}
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------CREATE EVENT---------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------*/
if(isset($addedEvent)){

    if(isset($_REQUEST['updated']) && $_REQUEST['updated'] == 1 && $addedEvent){

        //Get New Added Post Id
        $newEventId = $addedEvent[0]->ID;

        //Add Event
        wp_publish_post($newEventId);

        //Update Added Event default data
        $update_event_post = array(
            'ID'            => $newEventId,
            'post_title'    => $postTitle
        );
        wp_update_post($update_event_post);

        //Update Added Event Post Metas
        add_post_meta($newEventId,'eLearning_maximum_students',$_POST['eLearning_maximum_students']);
        add_post_meta($newEventId,'eLearning_start_date',$_POST['eLearning_start_date']);
        add_post_meta($newEventId,'eLearning_start_time',$_POST['eLearning_start_time']);
        add_post_meta($newEventId,'eLearning_duration',$_POST['eLearning_duration']);
        add_post_meta($newEventId,'elearning_events_status',$_POST['eLearning_event_status']);
        add_post_meta($newEventId,'elearning_events_status',$_POST['eLearning_event_status']);
        add_post_meta($newEventId,'eLearning_course_instructions',$_POST['eLearning_event_sort_description']);
        add_post_meta($newEventId,'eLearning_course_instructions',$_POST['eLearning_event_sort_description']);
        add_post_meta($newEventId,'eLearning_redirect_url',$_POST['eLearning_redirect_url']);
        add_post_meta($newEventId,'eLearning_pmpro_membership',serialize($_POST['eLearning_pmpro_membership']));
        add_post_meta($newEventId,'eLearning_webinar_recorder',$_POST['eLearning_webinar_recorder']);
        add_post_meta($newEventId,'event_recorded_switch',$_POST['event_recorded_switch']);
        add_post_meta($newEventId, '_thumbnail_id', $_POST['change_event_image']);
        add_post_meta($newEventId,'eLearning_course_curriculum',serialize($_POST['eLearning_course_curriculum']));
        $category = trim($_POST['eLearning_event_category']);
        wp_set_object_terms($newEventId,array('tag_ID' => (int)$category), 'course-cat');
        add_post_meta($newEventId,'_regular_price',$_POST['_regular_price']);
        add_post_meta($newEventId,'_sale_price',$_POST['_sale_price']);
        if(($_POST['_regular_price'] == 0 || empty($_POST['_regular_price'])) && ($_POST['_sale_price'] == 0) || empty($_POST['_sale_price'])){
            update_post_meta($newEventId,'eLearning_course_free','S');
        }
        add_post_meta($newEventId,'is_event','S');
        add_post_meta($newEventId,'eLearning_instructor',$_POST['eLearning_instructor']);

        //Check If webinar id is present, then go and Create new Webinar in Onstream
        if(isset($_POST['eLearning_type']) && !empty($_POST['eLearning_type']) && $_POST['eLearning_type'] == 'webinar'){
            require_once( $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/elearning/eLearning-addons/onstreamAPI/functions.php' );

            $post_settings = array();
            $post_metas = '';
            $webinarId = '';
            $instructorId = '';
            if(isset($_POST['eLearning_instructor']) && !empty($_POST['eLearning_instructor'])){
                $instructorId = $_POST['eLearning_instructor'];
            }
            $post_settings['post_title'] = $postTitle;
            $post_settings['eLearning_expected_duration'] = $_POST['eLearning_duration'];
            $post_settings['eLearning_start_date'] = $_POST['eLearning_start_time'];
            $data = createNewWebinarInOnstream($post_settings, $post_metas, $webinarId, $instructorId);

            $webinarId = $data['webinarId'];
            $addedInstructorId = $data['addedInstructorId'];
            add_post_meta($newEventId,'eLearning_webinar_id',$webinarId);

            /*  if (!empty($addedInstructorId)){
                    foreach ($studentsData as $studentData){
                        SetStudentAPI( $webinarId, $studentData->data->ID);
                    }
               } */
        }

        //Get New Created And Updated Event Data
        $current_created_event = get_post($newEventId);
        //Check if event unit type is isset, then create new unit and set him unit type same event
        if(isset($_POST['eLearning_type']) && !empty($_POST['eLearning_type'])){
            //Create New Unit
            $create_new_unit = array(
                'post_title'    => $current_created_event->post_title,
                'post_type'     => 'unit',
                'post_status'   => 'publish',
                'author'        => $current_created_event->post_author
            );
            wp_insert_post($create_new_unit);
            $args = array(
                'posts_per_page'   => 1,
                'orderby'          => 'date',
                'order'            => 'DESC',
                'post_type'        => 'unit',
                'post_status'      => 'publish',
                'author'           =>  $current_created_event->post_author
            );
            $addedUnit = get_posts( $args );
            //Set Unit Type To Created Unit
            add_post_meta($addedUnit[0]->ID,'eLearning_type',$_POST['eLearning_type']);
        }
        //Create new product, wich connect to this event
        $create_new_product = array(
            'post_title'    => $current_created_event->post_title,
            'post_type'     => 'product',
            'post_status'   => 'publish',
            'author'        => $current_created_event->post_author
        );
        wp_insert_post($create_new_product);
        $args = array(
            'posts_per_page'   => 1,
            'orderby'          => 'date',
            'order'            => 'DESC',
            'post_type'        => 'product',
            'post_status'      => 'publish',
            'author'           =>  $current_created_event->post_author
        );
        //Add post metas (product price, sale price and add post meta wich connect this product to this event)
        $addedProduct = get_posts( $args );
        add_post_meta($newEventId,'eLearning_product',$addedProduct[0]->ID);
        add_post_meta($addedProduct[0]->ID,'_regular_price',$_POST['_regular_price']);
        add_post_meta($addedProduct[0]->ID,'_sale_price',$_POST['_sale_price']);

        if(isset($_POST['eLearning_redirect_url']) && !empty($_POST['eLearning_redirect_url'])){
            header("Location: ".$_POST['eLearning_redirect_url']."");
        }
        else{
            $url = get_site_url().'/'.$current_user_login."/events";
            header("Location: $url");
        }
    }
}

?>