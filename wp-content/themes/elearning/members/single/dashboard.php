<?php
$url = ABSPATH;
/**
 * ELEARNING- DASHBOARD TEMPLATE
 */
if ( !defined('ABSPATH') )
    exit;
$current_user_login = wp_get_current_user()->user_login;
if ( !is_user_logged_in() ) {
    wp_redirect( home_url(), '302' );
}
get_header( eLearning_get_header() );

$profile_layout = eLearning_get_customizer( 'profile_layout' );
eLearning_include_template( "profile/top$profile_layout.php" );

?>
<div class="eLearning-dashboard row  fm_vg">
    <?php do_action('bp_before_dashboard_body'); ?>


    <div class="custom_dashboard"><?php //do_shortcode( '[elreport-dashboard-report]' ); ?>
    	<span class="custom_dashboard_text">
    		Welcome to your Admin Dashboard
    	</span>
        <span class="custom_dashboard_line"></span>
        <div class="dashboard-place" id="custom_dashboard_html">
            <div class="col-lg-12 col-md-12 row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#"><img src="/wp-content/themes/elearning/images/custom_dashboard_create_course.png" alt=""></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#"><img src="/wp-content/themes/elearning/images/custom_dashboard_createEvent.png" alt=""></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#"><img src="/wp-content/themes/elearning/images/custom_dashboard_createPromotion.png" alt=""></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#"><img src="/wp-content/themes/elearning/images/custom_dashboard_inviteStudents.png" alt=""></a>
                </div> 
            </div> 
        <!--  <div class="col-lg-12 col-md-12  row courses_events_students_earnings">-->
         <?php  echo do_shortcode('[elreport-dashboard-report]');
              // echo  do_shortcode('[email_customization_shortcode]');   
          ?>
              <!--  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="<?php echo site_url()?>/all-courses/"><img class="position fooImg" src="/wp-content/themes/elearning/images/custom_dashboard_courses.png" alt=""></a>
                    <p class="numCoord">
                        <?php  $temp = $wpdb->get_results( "SELECT COUNT(*) as count FROM wp_posts WHERE `post_type` =  'product' AND  post_status =  'publish'" );
                        echo $count = $temp[0]->count;?>
                     </p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="<?php echo site_url()?>/<?php echo $current_user_login; ?>/events/"><img class="position fooImg" src="/wp-content/themes/elearning/images/custom_dashboard_events.png" alt=""></a>
                    <p class="numCoord"> 12 </p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#"><img class="position fooImg" src="/wp-content/themes/elearning/images/custom_dashboard_students.png" alt=""></a>
                    <p class="numCoord">
                        <?php  $temp = $wpdb->get_results( "SELECT COUNT(*) as count FROM `wp_cf7dbplugin_submits` WHERE  `field_name` =  'your-name'" );
                        echo $count = $temp[0]->count;?>
                    </p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="#"><img class="position fooImg" src="/wp-content/themes/elearning/images/custom_dashboard_earnings.png" alt=""></a>
                    <p class="numCoord">&#36; 2,353 </p>
                </div>-->
           <!-- </div>-->
        </div>
    </div>
    <?php do_action('bp_after_dashboard_body');?>
</div>
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>
                             
                            






