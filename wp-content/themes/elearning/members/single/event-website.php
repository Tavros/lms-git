<?php $current_user_login = wp_get_current_user()->user_login; ?>
<div class="eLearning-website row hs-elearning event-website">
    <div class="head">
        <div class="row">
            <div class="col-md-8">
                <h3>Event Name</h3>
            </div>
            <div class="col-md-2">
                <a id="event_edit_save" class="btn btn-lg btn-primary pull-right top-button"  type="button" >SAVE</a>
            </div>
            <div class="col-md-2">
                <a id="event_edit_publish" class="btn btn-lg btn-primar pull-righty top-button" type="button" >PUBLISH</a>
            </div>
        </div>
    </div><!--End head-->
    <div class="event_menu_links">
        <ul class="Form_Customization_nav">
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/entries">GENERAL</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/event-messages">MESSAGES</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/event-pricing">PRICING</a></li>
            <li class="Form_Customization_item"><a href="<?php echo  site_url(); ?>/<?php echo $current_user_login;?>/event-website"><i class="fa fa-caret-up" aria-hidden="true"></i>WEBSITE</a></li>
        </ul>
    </div>
	<div class="col-lg-10 step2Left">
		<h6 class="h6color">USE LMS STYLE EVENT PAGE</h6>
	    <div class="switch">
		    <input type="radio" class="switch-input" name="website-name" value="no" id="no" checked>
		    <label for="no" class="switch-label switch-label-off">No</label>
		    <input type="radio" class="switch-input" name="website-name" value="yes" id="yes">
		    <label for="yes" class="switch-label switch-label-on">Yes</label>
		    <span class="switch-selection"></span>
	  	</div>
	    <p>Settings this to "NO" will create a blankc fully editable page for you to edit</p>
	</div> 
	<div class="col-lg-10 step2Left">
		<h6 class="h6color">USE SPARATE LANDING PAGE</h6>
		<div class="switch">
		    <input type="radio" class="switch-input" name="view" value="no1" id="no1" checked>
		    <label for="no1" class="switch-label switch-label-off">No</label>
		    <input type="radio" class="switch-input" name="view" value="yes1" id="yes1">
		    <label for="yes1" class="switch-label switch-label-on">Yes</label>
		    <span class="switch-selection"></span>
	  	</div>
 

 

<p>When locking down your content, you can create a separate public page to sell aour advertise the event.</p>
	</div> 
	<div class="col-lg-10 edit-landing-page">
		<button>EDIT LANDING PAGE</button>
	</div>
</div>
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>