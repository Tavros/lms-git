<?php
/**
 * BuddyPress - Users Settings
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */
if (!defined('ABSPATH'))
    exit;

?>

<div class="item-list-tabs no-ajax" id="subnav" role="navigation">
    <ul>
       <?php bp_get_options_nav(); ?>
    </ul>
</div>

<?php
do_action('eLearning_after_single_item_list_tabs');
switch (bp_current_action()) :
   case 'account-general' :
      bp_get_template_part('members/single/account/general');
      break;
   case 'account-profile' :
      bp_get_template_part('members/single/account/profile');
      break;
    case 'account-privacy' :
       bp_get_template_part('members/single/account/privacy');
       break;
    case 'account-notifications' :
      bp_get_template_part('members/single/account/notifications');
      break;
    case 'account-billing' :
      bp_get_template_part('members/single/account/billing');
      break;
    default:
        bp_get_template_part('members/single/account/general');
        break;
endswitch;
?>
