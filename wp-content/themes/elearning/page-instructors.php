<?php
/**
 * Template Name: Instructors Access Only
 */

if(!is_user_logged_in() || !current_user_can('edit_posts'))
    wp_die('<h2>'.__('This Page is only accessible to Instructors','eLearning').'</h2>'.'<p>'.__('The page is only accessible to site Users with Instructor or above capabilites.','eLearning').'</p>',__('Instructors only page','eLearning'),array('back_link'=>true));

get_header(eLearning_get_header());

if ( have_posts() ) : while ( have_posts() ) : the_post();

$title=get_post_meta(get_the_ID(),'eLearning_title',true);
if(eLearning_validate($title) || empty($title)){
?>
<section id="title">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="pagetitle">
                    <?php
                    $breadcrumbs=get_post_meta(get_the_ID(),'eLearning_breadcrumbs',true);
                    if(eLearning_validate($breadcrumbs) || empty($breadcrumbs))
                        eLearning_breadcrumbs(); 
                    ?>
                    <h1><?php the_title(); ?></h1>
                    <?php the_sub_title(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
}

    $v_add_content = get_post_meta( $post->ID, '_add_content', true );
 
?>
<section id="content">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-12">

                <div class="<?php echo $v_add_content;?> content">
                    <?php
                        the_content();
                     ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
endwhile;
endif;
?>

<?php
get_footer(eLearning_get_footer());
?>