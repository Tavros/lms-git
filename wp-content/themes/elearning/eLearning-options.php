<?php

if(!class_exists('ELEARNING_Options')){
	require_once( dirname( __FILE__ ) . '/options/options.php' );
}

/*
 * 
 * Custom function for filtering the sections array given by theme, good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 *
 * NOTE: the defined constansts for urls, and dir will NOT be available at this point in a child theme, so you must use
 * get_template_directory_uri() if you want to use any of the built in icons
 *
 */
function add_another_section($sections){
	
	//$sections = array();
	$sections[] = array(
				'title' => __('A Section added by hook', 'eLearning'),
				'desc' => '<p class="description">'.__('This is a section created by adding a filter to the sections array, great to allow child themes, to add/remove sections from the options.', 'eLearning').'</p>',
				//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
				//You dont have to though, leave it blank for default.
				'icon' => trailingslashit(get_template_directory_uri()).'options/img/glyphicons/glyphicons_062_attach.png',
				//Lets leave this as a blank section, no options just some intro text set above.
				'fields' => array()
				);
	
	return $sections;
	
}//function


/*
 * 
 * Custom function for filtering the args array given by theme, good for child themes to override or add to the args array.
 *
 */
function change_framework_args($args){
	
	//$args['dev_mode'] = false;
	
	return $args;
	
}

/*
 * This is the meat of creating the optons page
 *
 * Override some of the default values, uncomment the args and change the values
 * - no $args are required, but there there to be over ridden if needed.
 *
 *
 */

function setup_framework_options(){
$args = array();
global $eLearning_options;

      $eLearning_options = get_option(THEME_SHORT_NAME);  //Initialize ELearningoptions
//Set it to dev mode to view the class settings/info in the form - default is false
$args['dev_mode'] = false;

//google api key MUST BE DEFINED IF YOU WANT TO USE GOOGLE WEBFONTS
//$args['google_api_key'] = '***';

//Remove the default stylesheet? make sure you enqueue another one all the page will look whack!
//$args['stylesheet_override'] = true;

//Add HTML before the form
$args['intro_text'] = '';

//Setup custom links in the footer for share icons
$args['share_icons']['twitter'] = array(
										'link' => 'http://twitter.com/eLearningthemes',
										'title' => __('Folow me on Twitter','eLearning'), 
										'img' => ELEARNING_OPTIONS_URL.'img/ico-twitter.png'
										);
$args['share_icons']['facebook'] = array(
										'link' => 'http://facebook.com/eLearningthemes',
										'title' => __('Be our Fan on Facebook','eLearning'), 
										'img' => ELEARNING_OPTIONS_URL.'img/ico-facebook.png'
										);
$args['share_icons']['gplus'] = array(
										'link' => 'https://plus.google.com/107421230631579548079',
										'title' => __('Follow us on Google Plus','eLearning'), 
										'img' => ELEARNING_OPTIONS_URL.'img/ico-g+.png'
										);
$args['share_icons']['rss'] = array(
										'link' => 'feed://themeforest.net/feeds/users/ELearningThemes',
										'title' => __('Latest News from ELearningThemes','eLearning'), 
										'img' => ELEARNING_OPTIONS_URL.'img/ico-rss.png'
										);

//Choose to disable the import/export feature
//$args['show_import_export'] = false;

//Choose a custom option name for your theme options, the default is the theme name in lowercase with spaces replaced by underscores
$args['opt_name'] = THEME_SHORT_NAME;

//Custom menu icon
//$args['menu_icon'] = '';

//Custom menu title for options page - default is "Options"
$args['menu_title'] = __(THEME_FULL_NAME, 'eLearning');

//Custom Page Title for options page - default is "Options"
$args['page_title'] = __('ELearning Options Panel v 2.0', 'eLearning');

//Custom page slug for options page (wp-admin/themes.php?page=***) - default is "eLearning_theme_options"
$args['page_slug'] = THEME_SHORT_NAME.'_options';

//Custom page capability - default is set to "manage_options"
$args['page_cap'] = 'manage_options';

//page type - "menu" (adds a top menu section) or "submenu" (adds a submenu) - default is set to "menu"
//$args['page_type'] = 'submenu';
//$args['page_parent'] = 'themes.php';
$social_links=array();
if(function_exists('social_sharing_links')){
$social_links= social_sharing_links();
foreach($social_links as $link => $value){
    $social_links[$link]=$link;
 }
}


//custom page location - default 100 - must be unique or will override other items
$args['page_position'] = 62;

$args['help_tabs'][] = array(
							'id' => 'eLearning-opts-1',
							'title' => __('Support', 'eLearning'),
							'content' => '<p>'.__('We provide support via three mediums (in priority)','eLearning').':
                                                            <ul><li><a href="http://eLearningthemes.com/documentation/eLearning" target="_blank">'.THEME_FULL_NAME.' ELearningThemes Forums</a></li><li>'.__('Support Email: ELearningThemes@gmail.com', 'eLearning').'</li><li>'.__('ThemeForest Item Comments','eLearning').'</li></ul>
                                                            </p>',
							);
$args['help_tabs'][] = array(
							'id' => 'eLearning-opts-2',
							'title' => __('Documentation & Links', 'eLearning'),
							'content' => '<ul><li><a href="http://eLearningthemes.com/documentation/eLearning/forums/" target="_blank">'.THEME_FULL_NAME.' Support Panel</a></li>
	                                          <li><a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/one-click-setup-eLearning-demo1/" target="_blank">'.THEME_FULL_NAME.' Theme Setup</a></li>
	                                          <li><a href="http://eLearningthemes.com/documentation/eLearning/article-categories/faqs/" target="_blank">'.THEME_FULL_NAME.' Common FAQs/Issues</a></li>  
	                                          <li><a href="http://eLearningthemes.com/documentation/eLearning/article-categories/tips-tricks/" target="_blank">'.THEME_FULL_NAME.' Tips and Tricks</a></li>
	                                          <li><a href="http://eLearningthemes.com/documentation/eLearning/forums/forum/general/feature-request/" target="_blank">'.THEME_FULL_NAME.' Feature Requests</a></li>    
	                                          <li><a href="http://eLearningthemes.com/documentation/eLearning/article-categories/update-log/" target="_blank">'.THEME_FULL_NAME.' Update Log</a></li>    
	                                          <li><a href="https://www.youtube.com/watch?v=A0RsQvmDuSM&list=PL8n4TGA_rwD_5jqsgXIxXOk1H6ar-SVCV" target="_blank">'.THEME_FULL_NAME.' Video Guide</a></li>
	                                      </ul>
                                                            ',
							);


//Set the Help Sidebar for the options page - no sidebar by default										
$args['help_sidebar'] = '<p>For Support/Help and Docuementation open <strong><a href="http://eLearningthemes.com/documentation/eLearning/">'.THEME_FULL_NAME.' forums</a></strong>'.__('Or email us at','eLearning').' <a href="mailto:eLearningthemes@gmail.com">eLearningthemes@gmail.com</a>. </p>';



$sections = array();

$sections[] = array(
				'title' => __('Getting Started', 'eLearning'),
				'desc' => '<p class="description">'.__('Welcome to '.THEME_FULL_NAME.' Theme Options panel. ','eLearning').'</p>
                                    <ol>
                                        <li>'.__('See Theme documentation : ','eLearning').'<a href="http://eLearningthemes.com/envato/eLearning/documentation/" class="button">Official ELEARNING Documentation</a></li> 
                                        <li>'.__('Setup in One Click ','eLearning').' <a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/eLearning-one-click-setup/"> '.__('Setup ELEARNING','eLearning').'</a><small>'.'</small></li> 
                                        <li>'.__('Setup in One Click with New demos','eLearning').' <a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/one-click-setup-eLearning-demo1/" class="button button-primary"> '.__('Setup ELEARNING','eLearning').'</a><small>'.'</small></li> 
                                        <li>'.__('Facing issues? Create a support thread. ','eLearning').' <a href="http://eLearningthemes.com/documentation/eLearning/forums/" class="button" target="_blank">'.__('Full Theme Guide','eLearning').'</a></li> 
                                        <li>'.__('How to Update? Facing Issues while updating?','eLearning').' <a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/auto-updating-eLearning/">Auto updating ELEARNING.</a></li>     
                                    </ol>
                                    
                                    </p>',
				//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
				//You dont have to though, leave it blank for default.
				'icon' => 'menu',
                                'fields' => array(
                                    array(
						'id' => 'notice',
						'type' => 'divide',
                        'desc' => __('Details required for Auto-Update','eLearning')
						),
                                    array(
						'id' => 'username',
						'type' => 'text',
						'title' => __('Enter Your Themeforest Username', 'eLearning'), 
						'sub_desc' => __('Required for Automatic Upgrades.', 'eLearning'),
                                                'std' => ''
						),
                                    array(
						'id' => 'apikey',
						'type' => 'password',
						'title' => __('Enter Your Themeforest API KEY', 'eLearning'), 
						'sub_desc' => __('Please Enter your API Key.Required for Automatic Upgrades.', 'eLearning'),
                                                'desc' => __('Whats an API KEY? Where can I find one?','eLearning').' : <a href="http://themeforest.net/help/api" target="_blank">Get all your Anwers here</a> or use our Support Forums',
                                                'std' => ''
						),
                                    )
                                );


$sections[] = array(
				'icon' => 'admin-generic',
				'title' => __('Header', 'eLearning'),
				'desc' => '<p class="description">'.__('Header settings','eLearning').'..</p>',
				'fields' => array(
                    
                       array(
						'id' => 'logo',
						'type' => 'upload',
						'title' => __('Upload Logo', 'eLearning'), 
						'sub_desc' => __('Upload your logo', 'eLearning'),
						'desc' => sprintf(__('This Logo is shown in header. NOT ABLE TO UPLOAD ? %s REFER TIP %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/logo-and-favicon-not-uploading/" target="_blank">','</a>'),
                        'std' => ELEARNING_URL.'/assets/images/logo.png'
						),
                       array(
						'id' => 'mobile_logo',
						'type' => 'upload',
						'title' => __('Upload Logo for Mobile', 'eLearning'), 
						'sub_desc' => __('Upload a logo for mobile viewport', 'eLearning'),
						'desc' => sprintf(__('This Logo is shown in header on Mobile devices, less than 768px wide. NOT ABLE TO UPLOAD ? %s REFER TIP %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/logo-and-favicon-not-uploading/" target="_blank">','</a>'),
                        'std' => ELEARNING_URL.'/assets/images/logo.png'
						),
                       array(
						'id' => 'alt_logo',
						'type' => 'upload',
						'title' => __('Upload Alternate Logo', 'eLearning'), 
						'sub_desc' => __('Alternate logo', 'eLearning'),
						'desc' => sprintf(__('This Logo is shown in header when it becomes fixed. Or in the Header top area. Defaults to logo.NOT ABLE TO UPLOAD ? %s REFER TIP %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/logo-and-favicon-not-uploading/" target="_blank">','</a>'),
                        'std' => ELEARNING_URL.'/assets/images/logo.png'
						),
                        array(
						'id' => 'favicon',
						'type' => 'upload',
						'title' => __('Upload Favicon', 'eLearning'), 
						'sub_desc' => __('Upload 16x16px Favicon', 'eLearning'),
						'desc' => sprintf(__('Upload 16x16px Favicon.NOT ABLE TO UPLOAD ? %s REFER TIP %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/logo-and-favicon-not-uploading/" target="_blank">','</a>'),
                        'std' => ELEARNING_URL.'/assets/images/favicon.png'
						),
                         array(
							'id' => 'header_fix',
							'type' => 'button_set',
							'title' => __('Fix Top Header on Scroll', 'eLearning'), 
							'sub_desc' => __('Fix header on top of screen' , 'eLearning'),
							'desc' => __('header is fixed to top as user scrolls down.', 'eLearning'),
							'options' => array('0' => __('Static','eLearning'),'1' => __('Fixed on Scroll','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),  
						array(
							'id' => 'course_search',
	                        'title' => __('Navigation Search as Course Search', 'eLearning'),
	                        'sub_desc' => __('Force the header search to search only in courses', 'eLearning'),
	                        'type' => 'button_set',
							'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),//Must provide key => value pairs for radio options
							'std' => '0'
						),   
						array(
							'id' => 'headertop_content',
							'type' => 'editor',
							'title' => __('Header Top Content (Optional)', 'eLearning'), 
							'sub_desc' => __('* Standard headers', 'eLearning'),
							'desc' => __('Optional content, only required in Header variations', 'eLearning'),
	                        'std' => 'Call us Today ! 11223344 / 45678 | info@yourdomain.com'
						),
						array(
							'id' => 'header_content',
							'type' => 'editor',
							'title' => __('Header Content (Optional)', 'eLearning'), 
							'sub_desc' => __('Required in "some" header variations', 'eLearning'),
							'desc' => __('Optional content, only required in Header variations', 'eLearning'),
	                        'std' => ''
						),
					)
				);

$sections[] = array(
				'icon' => 'feedback',
				'title' => __('Sidebar Manager', 'eLearning'),
				'desc' => '<p class="description">'.__('Generate more sidebars dynamically and use them in various layouts','eLearning').'..</p>',
				'fields' => array(
					 array(
						'id' => 'sidebars',
						'type' => 'multi_text',
                        'title' => __('Create New sidebars ', 'eLearning'),
                        'sub_desc' => __('Dynamically generate sidebars', 'eLearning'),
                        'desc' => __('Use these sidebars in various layouts. DO NOT ADD ANY SPECIAL CHARACTERS in Sidebar name', 'eLearning')
						),	
					array(
							'id' => 'events_sidebar',
							'type' => 'sidebarselect',
							'title' => __('All Events page Sidebar', 'eLearning'), 
							'sub_desc' => __('Select All events page sidebar', 'eLearning'),
							'desc' => __('Select a sidebar for all events page', 'eLearning'),
							'std' => 'mainsidebar'
						),	
					 array(
						'id' => 'sidebars_widgets',
						'type' => 'import_export',
                        'title' => __('Import/Export Sidebar settings ', 'eLearning'),
                        'sub_desc' => __('Import/Export sidebars settings', 'eLearning'),
                        'desc' => __('Use import/export functionality to import/export your Sidebar settings like Widgets included in sidebars.', 'eLearning')
						),
					array(
						'id' => 'widgets_settings',
						'type' => 'widgets_import_export',
                        'title' => __('Import/Export Widget settings ', 'eLearning'),
                        'sub_desc' => __('Import/Export widgets settings', 'eLearning'),
                        'desc' => __('Use import/export functionality to import/export your widget settings.', 'eLearning')
						)		
					)
				);

$sections[] = array(
				'icon' => 'groups',
				'title' => __('Buddypress', 'eLearning'),
				'desc' => '<p class="description">'.__('BuddyPress settings and Variables','eLearning').'..</p>',
				'fields' => array(
					array(
						'id' => 'default_avatar',
						'type' => 'upload',
						'title' => __('Upload BuddyPress default member avatar', 'eLearning'), 
						'sub_desc' => __('BuddyPress default members avatar', 'eLearning'),
						'desc' => sprintf(__('This avatar is shown for members who have not uploaded any custom avatar.NOT ABLE TO UPLOAD ? %s REFER TIP %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/logo-and-favicon-not-uploading/" target="_blank">','</a>'),
                        'std' => ELEARNING_URL.'/assets/images/avatar.jpg'
						),

					array(
						'id' => 'hide_wp_admin_bar',
                        'title' => __('Hide WP Admin bar for', 'eLearning'),
                        'sub_desc' => __('Hide the top WP admin bar', 'eLearning'),
                        'desc' => sprintf(__('WP Admin bar is hidden for user types %s Tutorial %s', 'eLearning'),'<a href="https://www.youtube.com/watch?v=I_NkIlf7cUY" target="_blank">','</a>'),
                        'type' => 'button_set',
						'options' => array('' => __('Students only','eLearning'),'1'=>__('Students & Instructors','eLearning'),'2'=>__('Everyone','eLearning')),
						'std' => ''
						),
					array(
						'id' => 'wp_admin_access',
                        'title' => __('WP Admin area access', 'eLearning'),
                        'sub_desc' => __('Restrict WP Admin area access', 'eLearning'),
                        'desc' => __('WP Admin area is restricted for', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('' => __('All','eLearning'),'1'=>__('Instructors & Administrators only','eLearning'),'2'=>__('Administrators only','eLearning')),
						'std' => ''
						),
					array(
						'id' => 'loop_number',
                        'title' => __('Buddypress Items Per Page', 'eLearning'),
                        'sub_desc' => __('number of items shown per page', 'eLearning'),
                        'desc' => __('Number of Buddypress items (Courses,Members,Groups,Forums,Blogs etc..)', 'eLearning'),
                        'type' => 'text',
						'std' => '5'
						),
					array(
						'id' => 'members_view',
                        'title' => __('All Members directory View', 'eLearning'),
                        'sub_desc' => __('All members pages can be viewed by:', 'eLearning'),
                        'desc' => __('Profile viewability : All {Non-Loggedin}, Members{Loggedin Members},Teachers {Teachers, Admins,Editors}', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('All','eLearning'),'1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),
					array(
						'id' => 'single_member_view',
                        'title' => __('Single Member Profile View', 'eLearning'),
                        'sub_desc' => __('Single members profile can be viewed by:', 'eLearning'),
                        'desc' => __('Profile viewability : All {Non-Loggedin}, Members{Loggedin Members},Teachers {Teachers, Admins,Editors}', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('All','eLearning'),'1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),
					array(
						'id' => 'single_member_controls',
                        'title' => __('Single Member Profile Tabs visibility', 'eLearning'),
                        'sub_desc' => __('Hide single member profile menu for member types', 'eLearning'),
                        'desc' => __('Member Menu tabs like "Profile", "Activity","Groups" .. can be hidden', 'eLearning'),
                        'type' => 'button_set',
						'options' => array(''=>__('Everyone','eLearning'),'1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),
					array(
						'id' => 'members_redirect',
						'type' => 'pages_select',
                        'title' => __('All Members no-access redirect Page', 'eLearning'),
                        'sub_desc' => __('User is redirected to this page on error.', 'eLearning'),
                        'desc' => __('In case Members view access is denied to the user, user is redirected to this page.','eLearning')
						),
					array(
						'id' => 'activity_view',
                        'title' => __('Activity directory View', 'eLearning'),
                        'sub_desc' => __('Activity can be viewed by :', 'eLearning'),
                        'desc' => __('Activity viewability : All {Non-Loggedin}, Members{Loggedin Members},Teachers {Teachers, Admins,Editors}', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('All','eLearning'),'1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),
					array(
						'id' => 'student_activity',
                        'title' => __('Restrict Student activity view', 'eLearning'),
                        'sub_desc' => __('Restrict student view of activity', 'eLearning'),
                        'desc' => __('Student can view only her activity', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('' => __('All Activity','eLearning'),'1'=>__('Student Activity','eLearning')),//Must provide key => value pairs for radio options
						'std' => ''
						),		
					array(
						'id' => 'activity_redirect',
						'type' => 'pages_select',
                        'title' => __('Activity no-access redirect Page', 'eLearning'),
                        'sub_desc' => __('User is redirected to this page on error.', 'eLearning'),
                        'desc' => __('In case Activity view access is denied to the user, user is redirected to this page.','eLearning')
						),
					array(
						'id' => 'group_view',
                        'title' => __('Group directory View', 'eLearning'),
                        'sub_desc' => __('All Groups can be viewed by :', 'eLearning'),
                        'desc' => __('Group directory viewability : All {Non-Loggedin}, Members{Loggedin Members},Teachers {Teachers, Admins,Editors}', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('All','eLearning'),'1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),	
					array(
						'id' => 'group_redirect',
						'type' => 'pages_select',
                        'title' => __('Group directory no-access redirect Page', 'eLearning'),
                        'sub_desc' => __('User is redirected to this page on error.', 'eLearning'),
                        'desc' => __('In case Groups view access is denied to the user, user is redirected to this page.','eLearning')
						),
					array(
						'id' => 'group_create',
                        'title' => __('Create Groups', 'eLearning'),
                        'sub_desc' => __('Groups can be created by :', 'eLearning'),
                        'desc' => __('Group creation : Members{Loggedin Members},Teachers {Teachers, Admins,Editors}', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '1'
						),	
					array(
						'id' => 'blog_create',
                        'title' => __('Create Blog (multisite)', 'eLearning'),
                        'sub_desc' => __('Blogs can be created by :', 'eLearning'),
                        'desc' => sprintf(__('Blog creation : Members{Loggedin Members},Teachers {Teachers, Admins,Editors}, %s screenshot %s', 'eLearning'),'<a href="http://prntscr.com/cldri1" target="_blank">','</a>'),
                        'type' => 'button_set',
						'options' => array('1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '1'
						),	
					array(
						'id' => 'members_activity',
                        'title' => __('Show Members Meta info', 'eLearning'),
                        'sub_desc' => __('Members meta-info is shown below the name', 'eLearning'),
                        'desc' => __('Members activity, Friendship , Message button is shown in Single & members directory.', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),
					array(
						'id' => 'activity_tab',
                        'title' => __('Profile Activity Tab', 'eLearning'),
                        'sub_desc' => __('Single Profile activity can be viewed by :', 'eLearning'),
                        'desc' => __('Activity viewability : All {Non-Loggedin}, Members{Loggedin Members},Teachers {Teachers, Admins,Editors}', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('All','eLearning'),'1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),	

					array(
						'id' => 'groups_tab',
                        'title' => __('Profile Group View', 'eLearning'),
                        'sub_desc' => __('Single Profile Groups can be viewed by :', 'eLearning'),
                        'desc' => __('Group viewability : All {Non-Loggedin}, Members{Loggedin Members},Teachers {Teachers, Admins,Editors}', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('All','eLearning'),'1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),	
					array(
						'id' => 'forums_tab',
                        'title' => __('Profile Forums View', 'eLearning'),
                        'sub_desc' => __('Single Profile Forums can be viewed by :', 'eLearning'),
                        'desc' => __('Group viewability : All {Non-Loggedin}, Members{Loggedin Members},Teachers {Teachers, Admins,Editors}', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('All','eLearning'),'1'=>__('Members only','eLearning'),'2' => __('Teachers only','eLearning'),'3' => __('Admins only','eLearning')),//Must provide key => value pairs for radio options
						'std' => '0'
						),
					array(
						'id' => 'activation_redirect',
						'type' => 'pages_select',
						'options'=> array('dashboard'=>__('Dashboard','eLearning'),'profile'=>__('Profile','eLearning'),'mycourses'=>__('My Courses','eLearning')),
                        'title' => __('Redirect Page on User Activation', 'eLearning'),
                        'sub_desc' => __('User is redirected to this page on activating her account.', 'eLearning'),
                        'desc' => __('After registering and activating the account the user is redirected to this page.','eLearning')
						),
					array(
						'id' => 'enable_groups_join_button',
						'type' => 'button_set',
                        'title' => __('Enable Join Group/Request Membership button', 'eLearning'),
                        'sub_desc' => __('Button is shown in Groups directory', 'eLearning'),
                        'desc' => __('Join Group for Public groups and Request Membership button for private groups','eLearning'),
                        'options' => array(0 => __('Disable','eLearning'),1=>__('Enable','eLearning'))
						),
					
					array(
						'id' => 'student_field',
						'type' => 'text',
                        'title' => __('Student Field', 'eLearning'),
                        'sub_desc' => __('Enter the name of the Student Field to show below the name.', 'eLearning'),
                        'std'=>'Location'
						),
					array(
						'id' => 'instructor_field_group',
						'type' => 'text',
                        'title' => __('Instructor Field Group', 'eLearning'),
                        'sub_desc' => __('Enter the name of the Instructor Field Group you want to hide from Students from viewing in Profile -> edit (* No Required fields & Case senstitive).', 'eLearning'),
                        'std'=>'Instructor'
						),
                    array(
						'id' => 'instructor_field',
						'type' => 'text',
                        'title' => __('Instructor Field', 'eLearning'),
                        'sub_desc' => __('Enter the name of the Instructor Field to show below the name.', 'eLearning'),
                        'std'=>'Speciality'
						),
                    array(
						'id' => 'social_field_group',
						'type' => 'text',
                        'title' => __('Social Profiles Field Group', 'eLearning'),
                        'sub_desc' => sprintf(__('Add social profiles in user profiles. %s refer tutorial %s', 'eLearning'),'<a href=" http://eLearningthemes.com/documentation/eLearning/knowledge-base/social-profile-field-group/ ">','</a>'),
                        'std'=>'Social Profiles'
						),
					array(
						'id' => 'instructor_paypal_field',
						'type' => 'text',
                        'title' => __('Instructor Paypal Field', 'eLearning'),
                        'sub_desc' => __('Enter "Field Name" for Instructor PayPal ID', 'eLearning'),
                        'desc' => sprintf(__('Set a custom profile field for Commission payouts, %s tutorial %s','eLearning'),'<a href="https://www.youtube.com/watch?v=TeVJs0dw-Os" target="_blank">','</a>'),
                        'std'=>''
						),
                    array(
						'id' => 'instructor_about',
						'type' => 'text',
                        'title' => __('Instructor Description Field', 'eLearning'),
                        'sub_desc' => __('Instructor Description is picked from this field in the Instructor Widget', 'eLearning'),
                        'std'=>'About'
						),	
                    array(
						'id' => 'enable_ajax_registration_login',
						'type' => 'button_set',
                        'title' => __('Enable ajax registration & login', 'eLearning'),
                        'sub_desc' => __('Registration & Forgot password forms work inside the popup form. ', 'eLearning'),
                        'desc' => __('Registration & Forgot password forms open inside login form.','eLearning').'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/ajax-login-and-registration-in-eLearning">more</a>',
                        'options' => array('' => __('Disable','eLearning'),1=>__('Enable Login only','eLearning'),2=>__('Enable Login and registration','eLearning'))
						),
					array(
						'id' => 'custom_registration_page',
						'type' => 'pages_select',
                        'title' => __('Custom Registration page', 'eLearning'),
                        'sub_desc' => __('Overrides the default registration page from buddypress', 'eLearning'),
						),								
					)
				);


$sections[] = array(
				'icon' => 'welcome-learn-more',
				'title' => __('Course Manager', 'eLearning'),
				'desc' => '<p class="description">'.__('Manage Course Options from here.','eLearning').'..</p>',
				'fields' => array(
					 	array(
						'id' => 'take_course_page',
						'type' => 'pages_select',
                        'title' => __('Take This Course Page', 'eLearning'),
                        'sub_desc' => __('A Page with Start Course Page Template', 'eLearning'),
						),
						array(
						'id' => 'create_course',
						'type' => 'pages_select',
                        'title' => __('Connect Edit Course Page', 'eLearning'),
                        'sub_desc' => __('A Page with "Create Content" Page Template OR a page with shortcode [edit_course]', 'eLearning'),
						),
						array(
						'id' => 'unit_comments',
						'type' => 'pages_select',
                        'title' => __('Notes & Discussion Page', 'eLearning'),
                        'sub_desc' => __('A Page with "Notes & Discussion" Page Template', 'eLearning'),
						),
						array(
						'id' => 'sync_student_count',
                        'title' => __('Maintain accurate Student Count', 'eLearning'),
                        'sub_desc' => __('Maintains accurate student count for course', 'eLearning'),
                        'desc' => __('The Number of Student in Course count gets verified everytime user visits the Course - admin section', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'tab_style_course_layout',
                        'title' => __('Tab style course layout', 'eLearning'),
                        'sub_desc' => __('Changes the course tab style', 'eLearning'),
                        'desc' => __('Changes course tab style like that of udemy', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array( 
						'id' => 'course_status_template',
                        'title' => __('Course status templates', 'eLearning'),
                        'sub_desc' => __('Change course status page templates', 'eLearning'),
                        'desc' => sprintf(__('Course status page templates, %s tutorial %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/course-status-templates/" target="_blank">','</a>'),
                        'type' => 'button_set',
						'options' => array('default'=>__('Default','eLearning'),'fullscreen' => __('Full Screen','eLearning')),
						'std' => ''
						),
						/*array( // To be added in 2.5
						'id' => 'course_status_controls',
                        'title' => __('Show ControlPanel course status page', 'eLearning'),
                        'sub_desc' => __('Display controls on course status page', 'eLearning'),
                        'desc' => sprintf(__('Course status controls, like hide/show, minimize, fullscreen,chat etc.. %s refer tutorial %s', 'eLearning'),'<a href="" target="_blank">','</a>'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),*/
						array(
						'id' => 'new_course_status',
                        'title' => __('Admin Approval for Course', 'eLearning'),
                        'sub_desc' => __('Force instructors for Admin approval for new course', 'eLearning'),
                        'desc' => __('Force Courses created by Instructors are first sent to Administrator for Approval.', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('pending' => __('Yes, require approval','eLearning'),'publish'=>__('No, allow publish','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'nextunit_access',
                        'title' => __('Unit Locking', 'eLearning'),
                        'sub_desc' => __('Set access to next units based on previous unit status', 'eLearning'),
                        'desc' => __('Force users to complete previous units and quiz evalution before viewing the next units', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('Free Access','eLearning'),'1'=>__('Force prev Unit/Quiz Complete','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'unit_media_lock',
                        'title' => __('Unit Media Lock', 'eLearning'),
                        'sub_desc' => __('Hide unit completion button unless the Media(Audio/Video) is complete', 'eLearning'),
                        'desc' => __('Force users to view/listen the Media, Audio/Video in units before marking the unit as complete', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'assignment_locking',
                        'title' => __('Assignment Locking', 'eLearning'),
                        'sub_desc' => __('Force users to finish assignments to complete units', 'eLearning'),
                        'desc' => __('Mark complete button for a unit will appear only after student has finished the unit assignment', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('Do not lock','eLearning'),'1'=>__('Lock Unit Completion','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'course_progressbar',
                        'title' => __('Show Course Progressbar (always active)', 'eLearning'),
                        'sub_desc' => __('Show course progress bar above course timeline', 'eLearning'),
                        'desc' => __('Course Progress bar is shown in Course curriculum page', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'instructor_add_students',
                        'title' => __('Instructors can Add Students', 'eLearning'),
                        'sub_desc' => __('Enable Instructors to be able to add students', 'eLearning'),
                        'desc' => __('A Bulk Action is added in Course -> Admin -> Members which enables Instructos to add students to the course', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'instructor_change_status',
                        'title' => __('Instructors can Manage Student Course status ', 'eLearning'),
                        'sub_desc' => __('Enable Instructors to be change Students course status', 'eLearning'),
                        'desc' => __('A Bulk Action is added in Course -> Admin -> Members which enables Instructos to manage students course status in the course', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'instructor_assign_badges',
                        'title' => __('Instructors can Assign/Remove Certificates & Badges', 'eLearning'),
                        'sub_desc' => __('Enable Instructors to be able to assign Certificates & Badges to Students', 'eLearning'),
                        'desc' => __('A Bulk Action is added in Course -> Admin -> Members which enables Instructos to assign Certificates & Badges to Students for the course', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'instructor_extend_subscription',
                        'title' => __('Instructors can Extend subscription', 'eLearning'),
                        'sub_desc' => __('Enable Instructors to extend subscriptions of Students', 'eLearning'),
                        'desc' => __('A Bulk Action is added in Course -> Admin -> Members which enables Instructos to extend subscriptions of Students for the course', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'instructor_content_privacy',
                        'title' => __('Force Instructor Content Privacy', 'eLearning'),
                        'sub_desc' => __('Select boxes show only instructor units/quizzes/questions', 'eLearning'),
                        'desc' => __('Instructors can view titles but can not open content', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'stats_visibility',
                        'title' => __('Leaderboard/Stats Visibility', 'eLearning'),
                        'sub_desc' => __('Stats & Leaderboard visible to', 'eLearning'),
                        'desc' => __('Stats displaying average, maxmium, minimum and top 10 students for a module', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('All','eLearning'),'1'=>__('Students','eLearning'),'2'=>__('All Instructors','eLearning'),'3'=>__('Module Instructor & Administrators','eLearning')),
						'std' => '0'
						),
                        array(
						'id' => 'teacher_form',
						'type' => 'pages_select',
                        'title' => __('Become an Instructor Page', 'eLearning'),
                        'sub_desc' => __('A Page with become a teacher form.', 'eLearning'),
						),
                        array(
						'id' => 'certificate_page',
						'type' => 'pages_select',
                        'title' => __('Fallback Certificate Page', 'eLearning'),
                        'sub_desc' => __('A Page with certificate page template, Fallback to courses with no Certificate Templates.', 'eLearning'),
						),
						array(
						'id' => 'default_course_avatar',
						'type' => 'upload',
						'title' => __('Course default avatar', 'eLearning'), 
						'sub_desc' => __('Default avatar for courses', 'eLearning'),
						'desc' => sprintf(__('This avatar is shown for courses which do not have any avatar. NOT ABLE TO UPLOAD ? %s REFER TIP %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/logo-and-favicon-not-uploading/" target="_blank">','</a>'),
                        'std' => ''
						),
						array(
						'id' => 'hide_courses',
						'type' => 'posts_multi_select',
                        'title' => __('Hide Courses from Directory', 'eLearning'),
                        'sub_desc' => __('Hide courses from directory & pages, only accessible via direct link', 'eLearning'),
                        'args' => 'post_type=course',
                        'class' => 'chosen',
                        'std'=>''
						),
						array(
						'id' => 'course_duration_display_parameter',
						'type' => 'select',
                        'title' => __('Course Duration parameter', 'eLearning'),
                        'sub_desc' => __('Set course duration parameter', 'eLearning'),
                        'desc' => __('Course duration parameter for display purpose','eLearning'),
                        'options' => array(
                        	0 => __('Automatic','eLearning'),
                        	1 => __('Seconds','eLearning'),
                        	60 => __('Minutes','eLearning'),
                        	3600 => __('Hours','eLearning'),
                        	86400 => __('Days','eLearning'),
                        	604800 => __('Weeks','eLearning'),
                        	2592000 => __('Months','eLearning'),
                        	31536000 => __('Years','eLearning'),
                        	),
                        'std' => 0
						),
						array(
						'id' => 'finished_course_access',
                        'title' => __('Finished Course Access', 'eLearning'),
                        'sub_desc' => __('Allow students to view Finished courses', 'eLearning'),
                        'desc' => __('Make finished courses secure area viewable to students', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => ''
						),
						array(
						'id' => 'notes_style',
                        'title' => __('Notes and Discussion styles', 'eLearning'),
                        'sub_desc' => __('Set style for Notes & Discussion template', 'eLearning'),
                        'desc' => __('Display notes & Discussions', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('' => __('Per paragraph in Unit','eLearning'),'1'=>__('Per Unit','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'show_news',
                        'title' => __('Display News', 'eLearning'),
                        'sub_desc' => __('Display News section in courses', 'eLearning'),
                        'desc' => __('Display News section in courses, *requires ELEARNING Dashboard plugin', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'level',
                        'title' => __('Enable Levels', 'eLearning'),
                        'sub_desc' => __('Enables Level taxonomy', 'eLearning'),
                        'desc' => __('Enables Level taxonomy in Courses and search page ', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => ''
						),
						array(
						'id' => 'location',
                        'title' => __('Enable Course Location', 'eLearning'),
                        'sub_desc' => __('Location taxonomy for Courses', 'eLearning'),
                        'desc' => sprintf(__('Location taxonomy for Courses in Course filters and Search %s Tutorial %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/course-location-taxonomy/">','</a>'),
                        'type' => 'button_set',
						'options' => array('' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => ''
						),
						array(
						'id' => 'linkage',
                        'title' => __('Enable Linkage', 'eLearning'),
                        'sub_desc' => __('Connect Course, Units, Quiz, Questions with linkage taxonomy', 'eLearning'),
                        'desc' => __('Shorten the list of units, quizzes, courses shown in dropdowns', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
						array(
						'id' => 'redirect_course_cat_directory',
                        'title' => __('Course Category/Level/Location redirect', 'eLearning'),
                        'sub_desc' => __('Redirect to Course directory', 'eLearning'),
                        'desc' => sprintf(__('User redirects to Directory with category activated when she selects course category %s refer tutorial %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/course-category-redirect-to-course-directory/" target="_blank" class="link">','</a>'),
                        'type' => 'button_set',
						'options' => array('' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => ''
						),

						array(
						'id' => 'default_course_block_style',
                        'title' => __('Default course block style', 'eLearning'),
                        'sub_desc' => __('Set a default block style for courses', 'eLearning'),
                        'desc' => __('default course block style', 'eLearning'),
                        'type' => 'radio_img',
						'options' => array(             
	                                        'course' => array('Default' => __('Four Columns','eLearning'), 'img' => get_template_directory_uri() .'/eLearning-addons/customtypes/metaboxes/library/images/thumb_2.png'),
											'course2' => array('Default' => __('Four Columns','eLearning'), 'img' => get_template_directory_uri() .'/eLearning-addons/customtypes/metaboxes/library/images/thumb_8.png'),
											'course3' => array('Default' => __('Four Columns','eLearning'), 'img' => get_template_directory_uri() .'/eLearning-addons/customtypes/metaboxes/library/images/thumb_8.jpg'),
											'course4' => array('Default' => __('Four Columns','eLearning'), 'img' =>  get_template_directory_uri() .'/eLearning-addons/customtypes/metaboxes/library/images/thumb_9.jpg'),
											'course5' => array('Default' => __('Four Columns','eLearning'), 'img' => get_template_directory_uri() .'/eLearning-addons/customtypes/metaboxes/library/images/thumb_10.jpg'),
	                            ),//Must provide key => value(array:title|img) pairs for radio options
						'std' => '4'
						),
						array(
						'id' => 'related_courses',
                        'title' => __('Show related courses at the end of single course', 'eLearning'),
                        'sub_desc' => __('related courses by category, instructor & tags', 'eLearning'),
                        'desc' => __('Related courses are displayed based on category & instructor', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('0' => __('No','eLearning'),'1'=>__('Yes','eLearning')),
						'std' => '0'
						),
					)
				);

$sections[] = array(
				'icon' => 'editor-spellcheck',
				'title' => __('Fonts Manager', 'eLearning'),
				'desc' => '<p class="description">'.__('Manage Fonts to be used in the Site. Fonts selected here will be available in Theme customizer font family select options.','eLearning').'..</p>',
				'fields' => array(
					array(
						'id' => 'google_fonts_api_key',
						'type' => 'text',
                        'title' => __('Enter Google fonts API key', 'eLearning'),
                        'sub_desc' => __('Required to fetch a list of Fonts from Google', 'eLearning').'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/google-api-key-for-font-manager-and-usage/">Learn how to get Google fonts API</a>'
						),
					 array(
						'id' => 'google_fonts',
						'type' => 'google_webfonts_multi_select',
                        'title' => __('Select Fonts for Live Theme Editor ', 'eLearning'),
                        'sub_desc' => __('Select Fonts and setup fonts in Live Editor', 'eLearning'),
                        'desc' => __('Use these sample layouts in PageBuilder.', 'eLearning')
						),
                        array(
						'id' => 'custom_fonts',
						'type' => 'multi_text',
                        'title' => __('Custom Fonts (Enter CSS Font Family name)', 'eLearning'),
                        'sub_desc' => __(' Custom Fonts are added to Theme Customizer Font List.. ', 'eLearning').'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/add-custom-fonts-in-eLearning/">Learn how to add custom fonts</a>'
						)
					 				
					)
				);


$sections[] = array(
				'icon' => 'visibility',
				'title' => __('Customizer', 'eLearning'),
				'desc' => '<p class="description">'.__('Import/Export customizer settings. Customize your theme using ','eLearning').' <a href="'.get_admin_url().'customize.php" class="button">'.__('WP Theme Customizer','eLearning').'</a></p>',
				'fields' => array(
                     array(
						'id' => 'eLearning_customizer',
						'type' => 'import_export',
                        'title' => __('Import/Export Customizer settings ', 'eLearning'),
                        'sub_desc' => __('Import/Export customizer settings', 'eLearning'),
                        'desc' => __('Use import/export functionality to import/export your customizer settings.', 'eLearning')
						)			
					)
				);

$sections[] = array(
				'icon' => 'editor-kitchensink',
				'title' => __('PageBuilder Manager', 'eLearning'),
				'desc' => '<p class="description">'.__('Manage PageBuilder saved layouts and Import/Export pagebuilder Saved layouts','eLearning').'</p>',
				'fields' => array(
					array(
						'id' => 'sample_layouts',
						'type' => 'pagebuilder_layouts',
                        'title' => __('Manage Sample Layouts ', 'eLearning'),
                        'sub_desc' => __('Delete Sample Layouts', 'eLearning'),
                        'desc' => __('Use these sample layouts in PageBuilder.', 'eLearning')
						),
                    array(
						'id' => 'eLearning_builder_sample_layouts',
						'type' => 'import_export',
                        'title' => __('Import/Export Sample Layouts ', 'eLearning'),
                        'sub_desc' => __('Import/Export existing Layouts', 'eLearning'),
                        'desc' => __('Use import/export functionality to save your layouts.', 'eLearning')
						)
					 				
					)
				);

$sections[] = array(
				'icon' => 'editor-insertmore',
				'title' => __('Footer ', 'eLearning'),
				'desc' => '<p class="description">'.__('Setup footer settings','eLearning').'..</p>',
				'fields' => array( 
						
					 	array(
							'id' => 'top_footer_columns',
							'type' => 'radio_img',
							'title' => __('Top Footer Sidebar Columns', 'eLearning'), 
							'sub_desc' => __('Footer Columns', 'eLearning'),
							'options' => array(             
	                                        'col-md-3 col-sm-6' => array('title' => __('Four Columns','eLearning'), 'img' => ELEARNING_OPTIONS_URL.'img/footer-1.png'),
											'col-md-4 col-sm-4' => array('title' => __('Three Columns','eLearning'), 'img' => ELEARNING_OPTIONS_URL.'img/footer-2.png'),    
											'col-md-6 col-sm-6' => array('title' => __('Two Columns','eLearning'), 'img' => ELEARNING_OPTIONS_URL.'img/footer-3.png'),
	                                        'col-md-12' => array('title' => __('One Columns','eLearning'), 'img' => ELEARNING_OPTIONS_URL.'img/footer-4.png'),
	                            ),//Must provide key => value(array:title|img) pairs for radio options
							'std' => '4'
						),
                        array(
							'id' => 'bottom_footer_columns',
							'type' => 'radio_img',
							'title' => __('Bottom Footer Sidebar Columns', 'eLearning'), 
							'sub_desc' => __('Footer Columns', 'eLearning'),
							'options' => array(             
	                                        'col-md-3 col-sm-6' => array('title' => __('Four Columns','eLearning'), 'img' => ELEARNING_OPTIONS_URL.'img/footer-1.png'),
											'col-md-4 col-sm-4' => array('title' => __('Three Columns','eLearning'), 'img' => ELEARNING_OPTIONS_URL.'img/footer-2.png'),    
											'col-md-6 col-sm-6' => array('title' => __('Two Columns','eLearning'), 'img' => ELEARNING_OPTIONS_URL.'img/footer-3.png'),
	                                        'col-md-12' => array('title' => __('One Columns','eLearning'), 'img' => ELEARNING_OPTIONS_URL.'img/footer-4.png'),
	                            ),//Must provide key => value(array:title|img) pairs for radio options
							'std' => '4'
						),  
                        array(
						'id' => 'footer_logo',
						'type' => 'upload',
						'title' => __('Upload Footer Logo', 'eLearning'), 
						'sub_desc' => __('Displayed in footer', 'eLearning'),
						'desc' => sprintf(__('This Logo is shown in footer bottom area. Defaults to logo. NOT ABLE TO UPLOAD ? %s REFER TIP %s', 'eLearning'),'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/logo-and-favicon-not-uploading/" target="_blank">','</a>'),
                        'std' => ELEARNING_URL.'/assets/images/logo.png'
						),
                        array(
							'id' => 'copyright',
							'type' => 'editor',
							'title' => __('Copyright Text', 'eLearning'), 
							'sub_desc' => __('Enter copyrighted text', 'eLearning'),
							'desc' => __('Also supports shotcodes.', 'eLearning'),
	                        'std' => 'Template Design © <a href="http://www.eLearningthemes.com" title="ELearningCom">ELearningThemes</a>. All rights reserved.'
						),
                        array(
						'id' => 'footerbottom_right',
                        'title' => __('Footer Bottom', 'eLearning'),
                        'sub_desc' => __('Select an option for FooterBottom', 'eLearning'),
                        'desc' => __('Select Footer Bottom style, set Social icon links in Social tab', 'eLearning'),
                        'type' => 'button_set',
						'options' => array('' => __('Show Footer Menu','eLearning'),'1'=>__('Show Social Icons','eLearning')),
						'std' => ''
						),             
                        array(
							'id' => 'google_analytics',
							'type' => 'textarea',
							'title' => __('Google Analytics Code', 'eLearning'), 
							'sub_desc' => __('Google Analytics account', 'eLearning'),
							'desc' => __('Please enter full code with javascript tags.', 'eLearning'),
						),

					 				
					)
				);
$sections[] = array(
				'icon' => 'twitter',
				'title' => __('Social Information', 'eLearning'),
				'desc' => '<p class="description">'.__('All Social media settings','eLearning').'..</p>',
				'fields' => array(
					   
                        array(
						'id' => 'social_icons',
						'type' => 'multi_social',
                        'title' => __('Add Social Media Icons ', 'eLearning'),
                        'sub_desc' => __('Dynamically add social media icons', 'eLearning'),
                        'desc' => __('Add your Full URL in social media.', 'eLearning')
						),
                        array(
						'id' => 'social_icons_type',
						'type' => 'button_set',
						'title' => __('Social Icons Type', 'eLearning'), 
						'sub_desc' => __('Social Icons Theme', 'eLearning'),
						'options' => array('' => __('Minimal','eLearning'),'round' => __('Round','eLearning'),'square' => __('Square','eLearning'),'round color' => __('Round Colored','eLearning'),'square color' => __('Square Colored','eLearning')),
						'std' => ''
						),
                        array(
						'id' => 'show_social_tooltip',
						'type' => 'button_set',
						'title' => __('Show Tooltip on Social Icons', 'eLearning'), 
						'options' => array(1 => __('Yes','eLearning'),0 => __('No','eLearning')),
						'std' => 1
						),     
                        array(
						'id' => 'social_share',
						'type' => 'multi_select',
                        'title' => __('Social Sharing buttons', 'eLearning'),
                        'sub_desc' => __('Show in-built sharing buttons in the theme', 'eLearning'),
                        'desc' => __('Adds Social media sharing buttons in single Courses etc.', 'eLearning'),
                        'options' => $social_links
						),
					)
				);
$sections[] = array(
				'icon' => 'welcome-view-site',
				'title' => __('TinCan/xAPI', 'eLearning'),
				'desc' =>'<p class="description">'. __('TinCan and LRS settings for ELEARNING', 'eLearning').'</p>',
				'fields' => array(
                        array(
						'id' => 'tincan',
						'type' => 'button_set',
						'title' => __('Enable TinCan recording', 'eLearning'), 
						'sub_desc' => __('Record TinCan/XAPI statements in External LRS', 'eLearning'),
						'desc' => __('Store all activity in an External LRS, which other XAPI compatible LMSes can read.', 'eLearning'),
						'options' => array(0 => __('Disable','eLearning'),1 => __('Enable','eLearning')),
						'std' => 0
						),	
						array(
						'id' => 'tincan_endpoint',
                        'title' => __('TinCan API EndPoint', 'eLearning'),
                        'sub_desc' => __('Add a TinCan API endpoint', 'eLearning'),
                        'desc' => __('Add Endpoint to track details in external LRS.', 'eLearning'),
                        'type' => 'text',
						'std' => '0'
						),
						array(
						'id' => 'tincan_user',
                        'title' => __('LRS User name', 'eLearning'),
                        'sub_desc' => __('TinCan compatible LRS authentication', 'eLearning'),
                        'desc' => __('Enter Username for external LRS authentication.', 'eLearning'),
                        'type' => 'text',
						'std' => '0'
						),
						array(
						'id' => 'tincan_pass',
                        'title' => __('LRS Password', 'eLearning'),
                        'sub_desc' => __('TinCan compatible LRS authentication', 'eLearning'),
                        'desc' => __('Enter Password for external LRS authentication.', 'eLearning'),
                        'type' => 'password',
						'std' => '0'
						),
					)
				);
$sections[] = array(
				'icon' => 'location',
				'title' => __('Miscellaneous', 'eLearning'),
				'desc' =>'<p class="description">'. __('Miscellaneous settings used in the theme.', 'eLearning').'</p>',
				'fields' => array(
                        array(
						'id' => 'page_loader',
						'type' => 'button_set',
						'title' => __('Page Loader', 'eLearning'), 
						'sub_desc' => __('Add a Page loader on site, everything is hidden till the page loads', 'eLearning'),
						'desc' => sprintf(__('A loading icon will be displayed to the user, till the whole page loads. %s', 'eLearning'),'<a href="https://www.youtube.com/watch?v=pwxYL080oFQ" target="_blank">tutorial</a>'),
						'options' => array('' => __('Disable','eLearning'),'pageloader1' => __('Enable, Style 1','eLearning'),'pageloader2' => __('Enable, Style 2','eLearning')),
						'std' => ''
						),                 
						array(
						'id' => 'site_lock',
						'type' => 'button_set',
						'title' => __('Site Lock', 'eLearning'), 
						'sub_desc' => __('Lock entire site, only logged in users can view the site', 'eLearning'),
						'desc' => sprintf(__('All the links require login. Only home page is accessible to non-logged in users. %s', 'eLearning'),'<a href="https://www.youtube.com/watch?v=pwxYL080oFQ" target="_blank">tutorial</a>'),
						'options' => array('' => __('No','eLearning'),'1' => __('Yes','eLearning')),
						'std' => ''
						), 
						array(
						'id' => 'site_lock_home_page_url',
						'type' => 'pages_select',
						'title' => __(' Site lock Logo link page', 'eLearning'), 
						'sub_desc' => __('Url on header logo and footer logo when site lock is enabled', 'eLearning'),
						'desc' => sprintf(__('Select page which you want to redirect users to when a user clicks on the site logo.','eLearning')),
						),
						array(
						'id' => 'security_key',
						'type' => 'text',
						'title' => __('Unique Security Key', 'eLearning'), 
						'sub_desc' => __('Security key for every site. Longer keys are good', 'eLearning'),
						'desc' => __('Unique key to avoid (logged in) users from bypassing the system.', 'eLearning'),
						'std' => wp_generate_password()
						),
						array(
						'id' => 'default_archive',
						'type' => 'button_set',
						'title' => __('Default Archive style', 'eLearning'), 
						'sub_desc' => __('Set the default archive style from default, blog1, blog 2, blog 3', 'eLearning'),
						'options' => array('' => __('Default','eLearning'),'blog1' => __('Blog 1','eLearning'),'blog2' => __('Blog 2','eLearning'),'blog3' => __('Blog 3','eLearning')),
						'std' => ''
						), 
						array(
						'id' => 'excerpt_length',
						'type' => 'text',
						'title' => __('Default Excerpt Length', 'eLearning'), 
						'sub_desc' => __('Excerpt length in number of Words.', 'eLearning'),
						'std' => '20'
						),
						array(
						'id' => 'page_comments',
						'type' => 'button_set',
						'title' => __('Comments in pages', 'eLearning'), 
						'sub_desc' => __('Enable comments in Pages (Disabled by default)','eLearning'),
						'desc' => __('Users can post comments in pages.', 'eLearning'),
						'options' => array('' => __('Disable','eLearning'),1 => __('Enable','eLearning')),
						'std' => 0
						),
						array(
						'id' => 'instructor_commission',
						'type' => 'number',
						'title' => __('Default Instructor Commission', 'eLearning'), 
						'sub_desc' => __('Insructor commission per sale of course/product (enter 0 to disable)', 'eLearning'),
						'std' => '70'
						),
						array(
						'id' => 'direct_checkout',
						'type' => 'button_set',
						'title' => __('Direct Checkout', 'eLearning'), 
						'sub_desc' => __('Requires WooCommerce installed','eLearning'),
						'desc' => __('User is redirected to the checkout page.', 'eLearning'),
						'options' => array(2 => __('Skip Product & Cart page','eLearning'),3 => __('Skip Product page','eLearning'),1 => __('Skip Cart','eLearning'),0 => __('Disable','eLearning')),
						'std' => 0
						),
                        array(
						'id' => 'thankyou_redirect',
						'type' => 'button_set',
						'title' => __('Redirect to Course Page on Order completion', 'eLearning'), 
						'sub_desc' => __('Only if the order contains one product with one course', 'eLearning'),
						'desc' => __('If you\'re forcing the direct checkout, and your products have one course per product then switching this on would send users directly to the course page.', 'eLearning'),
						'options' => array(0 => __('Disable','eLearning'),1 => __('Enable','eLearning')),
						'std' => 0
						),   
						array(
						'id' => 'remove_woo_fields',
						'type' => 'button_set',
						'title' => __('Remove Extra Checkout Fields', 'eLearning'), 
						'sub_desc' => __('Recommended if you\'re only selling courses/virtual products','eLearning'),
						'desc' => __('Removes following fields in WooCommerce Checkout : Billing Company/Address,State/Town, Pincode, Phone etc.', 'eLearning'),
						'options' => array(0 => __('No','eLearning'),1 => __('Yes','eLearning')),
						'std' => 0
						),   
						array(
							'id' => 'cache_duration',
							'type' => 'number',
							'title' => __('Cache Duration', 'eLearning'), 
							'sub_desc' => __('in seconds (0 to disable)', 'eLearning'),
							'desc' => __('Small cache duration could impact adversely. High for stable websites.', 'eLearning'),
	                        'std' => '0'
						),	
						array(
						'id' => 'google_captcha_public_key',
						'type' => 'text',
						'title' => __('Google Captcha Public Key', 'eLearning'), 
						'sub_desc' => __('Enter Google captcha public key', 'eLearning').'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/google-captcha-public-and-private-keys/">'.__('How to get an Public Key ?','eLearning').'</a>',
						'std' => ''
						),
						array(
						'id' => 'google_captcha_private_key',
						'type' => 'text',
						'title' => __('Google Captcha Private Key', 'eLearning'), 
						'sub_desc' => __('Enter Google captcha private key', 'eLearning').'<a href="hhttp://eLearningthemes.com/documentation/eLearning/knowledge-base/google-captcha-public-and-private-keys/">'.__('How to get an Private Key ?','eLearning').'</a>',
						'std' => ''
						),
						array(
						'id' => 'google_apikey_contact',
						'type' => 'text',
						'title' => __('Google Maps API Key', 'eLearning'), 
						'sub_desc' => __('Google Maps require an API key to function', 'eLearning').'<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/how-to-get-google-api-key-for-google-maps/">'.__('How to get an API Key ?','eLearning').'</a>',
						'std' => ''
						),
                       array(
						'id' => 'contact_ll',
						'type' => 'text',
						'title' => __('Contact Page Latitude and Longitude values', 'eLearning'), 
						'sub_desc' => __('Grab the latitude and Longitude values from .', 'eLearning').'<a href="http://itouchmap.com/latlong.html">'.__('Link','eLearning').'</a>',
						'std' => '43.730325,7.422155'
						),
                       array(
						'id' => 'contact_style',
						'type' => 'button_set',
						'title' => __('Contact Page Map Style', 'eLearning'), 
						'sub_desc' => __('Select the map style on contact page.', 'eLearning'),
						'desc' => __('Content area is the container in which all content is located.', 'eLearning'),
						'options' => array('SATELLITE' => __('Satellite View','eLearning'),'ROADMAP' => __('Road map','eLearning')),
						'std' => 'SATELLITE'
						),
						array(
							'id' => 'map_zoom',
							'type' => 'text',
							'title' => __('Google Map Zoom Level', 'eLearning'), 
							'sub_desc' => __('Enter the zoom level in Google maps', 'eLearning'),
							'desc' => __('Zoom Levels 0 - 19', 'eLearning'),
							'std' => 17
						), 
                        array(
							'id' => 'error404',
							'type' => 'pages_select',
							'title' => __('Select 404 Page', 'eLearning'), 
							'sub_desc' => __('This page is shown when page not found on your site.', 'eLearning'),
							'desc' => __('User redirected to this page when page not found.', 'eLearning'),
						), 
						array(
							'id' => 'xmlrpc',
							'type' => 'button_set',
							'title' => __('Disable XMLRPC/RSD/WLWManifest', 'eLearning'), 
							'sub_desc' => __('Remove security vulnerabilities', 'eLearning'),
							'desc' => __('Removes vulnerabilities at expense of ability to login via remote apps.', 'eLearning'),
							'options' => array('' => __('No','eLearning'),1 => __('Yes','eLearning')),
							'std' => ''
						),
						array(
							'id' => 'disable_versioning',
							'type' => 'button_set',
							'title' => __('Disable Versions in Scripts/Styles', 'eLearning'), 
							'sub_desc' => __('Helps in caching of resources', 'eLearning'),
							'desc' => __('Improves page speed score', 'eLearning'),
							'options' => array('' => __('No','eLearning'),1 => __('Yes','eLearning')),
							'std' => ''
						),
						array(
							'id' => 'wp_login_screen',
							'type' => 'textarea',
							'title' => __('Custom CSS for WP Login Screen', 'eLearning'), 
							'sub_desc' => __('Add custom CSS', 'eLearning'),
							'desc' => __('Custom CSS for WP Login screen.', 'eLearning'),
						),
						array(
							'id' => 'credits',
							'type' => 'text',
							'title' => __('Author & Credits', 'eLearning'), 
							'sub_desc' => __('Credits and Author of the Website', 'eLearning'),
							'desc' => __('Changes the reference to Author {ELearningThemes}', 'eLearning'),
	                        'std' => 'ELearningThemes'
						),
                      )
                    );      
	$tabs = array();
	
			
	if (function_exists('wp_get_theme')){
		$theme_data = wp_get_theme();
		$theme_uri = $theme_data->get('ThemeURI');
		$description = $theme_data->get('Description');
		$author = $theme_data->get('Author');
		$version = $theme_data->get('Version');
		$tags = $theme_data->get('Tags');
	}else{
		$theme_data = get_theme_data(trailingslashit(get_stylesheet_directory()).'style.css');
		$theme_uri = $theme_data['URI'];
		$description = $theme_data['Description'];
		$author = $theme_data['Author'];
		$version = $theme_data['Version'];
		$tags = $theme_data['Tags'];
	}	

	$theme_info = '<div class="eLearning-opts-section-desc">';
	$theme_info .= '<p class="eLearning-opts-theme-data description theme-uri"><strong>Theme URL:</strong> <a href="'.$theme_uri.'" target="_blank">'.$theme_uri.'</a></p>';
	$theme_info .= '<p class="eLearning-opts-theme-data description theme-author"><strong>Author:</strong>'.$author.'</p>';
	$theme_info .= '<p class="eLearning-opts-theme-data description theme-version"><strong>Version:</strong> '.$version.'</p>';
	$theme_info .= '<p class="eLearning-opts-theme-data description theme-description">'.$description.'</p>';
	$theme_info .= '<p class="eLearning-opts-theme-data description theme-tags"><strong>Tags:</strong> '.implode(', ', $tags).'</p>';
	$theme_info .= '</div>';



	$tabs['theme_info'] = array(
					'icon' => 'info-sign',
					'title' => __('Theme Information', 'eLearning'),
					'content' => $theme_info
					);
	/*
	if(file_exists(trailingslashit(get_stylesheet_directory()).'README.html')){
		$tabs['theme_docs'] = array(
						'icon' => 'book',
						'title' => __('Documentation', 'eLearning'),
						'content' => nl2br(file_get_contents(trailingslashit(get_stylesheet_directory()).'README.html'))
						);
	}*///if

	global $ELEARNING_Options;
	$sections = apply_filters('eLearning_option_custom_sections',$sections);
	$ELEARNING_Options = new ELEARNING_Options($sections, $args, $tabs);
	wp_cache_delete('eLearning_option','settings');
	
}//function
add_action('init', 'setup_framework_options', 0);

/*
 * 
 * Custom function for the callback referenced above
 *
 */
function my_custom_field($field, $value){
	print_r($field);
	print_r($value);

}//function

/*
 * 
 * Custom function for the callback validation referenced above
 *
 */
function validate_callback_function($field, $value, $existing_value){
	
	$error = false;
	$value =  'just testing';
	
	$return['value'] = $value;
	if($error == true){
		$return['error'] = $field;
	}
	return $return;
	
}//function
?>