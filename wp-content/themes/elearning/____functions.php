<?php

if (!defined('ABSPATH'))
    exit;

// Essentials
include_once 'includes/config.php';
include_once 'includes/init.php';

// Register & Functions
include_once 'includes/register.php';
include_once 'includes/actions.php';
include_once 'includes/filters.php';
include_once 'includes/func.php';
include_once 'includes/ratings.php';
// Customizer
include_once 'includes/customizer/customizer.php';
include_once 'includes/customizer/css.php';
include_once 'includes/eLearning-menu.php';
include_once 'includes/notes-discussions.php';
include_once 'includes/eLearning-woocommerce-checkout.php';

include_once 'eLearning-addons/onstreamAPI/functions.php';
include_once 'eLearning-addons/eLearning-dashboard/eLearning-dashboard.php';

if (function_exists('bp_is_active')) {


// eLearning Dashboard
    include_once 'eLearning-addons/account_dashboar/dashboard.php';
    //  include_once 'eLearning-addons/messagecenter/messagecenter.php';

    include_once 'eLearning-addons/shortcodes/eLearning-shortcodes.php';
    include_once 'eLearning-addons/events/events.php';
    include_once 'eLearning-addons/front-end/eLearning-front-end.php';
    include_once 'eLearning-addons/assignments/eLearning-assignments.php';
    include_once 'eLearning-addons/course-module/loader.php';
    include_once 'eLearning-addons/customtypes/eLearning-customtypes.php';
    include_once 'eLearning-addons/customlinks/customlink.php';

    if (checkUserRole('Student') || checkUserRole('instructor')) {
        include_once 'eLearning-addons/account/account.php';
        include_once 'eLearning-addons/profile/profile.php';
        include_once 'eLearning-addons/myfiles/myfiles.php';
        include_once 'eLearning-addons/mynetwork/mynetwork.php';
        include_once 'eLearning-addons/referrals/referrals.php';
        //include_once 'eLearning-addons/sessions/sessions.php';
    }

    if (is_super_admin()) {
        include_once 'eLearning-addons/sales/sales.php';
        include_once 'eLearning-addons/marketing/marketing.php';
        include_once 'eLearning-addons/users/users.php';
        include_once 'eLearning-addons/admin-settings/admin-settings.php';
    }
}


if (function_exists('bp_get_signup_allowed')) {
    include_once 'includes/bp-custom.php';
}

include_once '_inc/ajax.php';
include_once 'includes/buddydrive.php';
//Widgets
include_once('includes/widgets/custom_widgets.php');
if (function_exists('bp_get_signup_allowed')) {
    include_once('includes/widgets/custom_bp_widgets.php');
}
if (function_exists('pmpro_hasMembershipLevel')) {
    include_once('includes/pmpro-connect.php');
}
include_once('includes/widgets/advanced_woocommerce_widgets.php');
include_once('includes/widgets/twitter.php');
include_once('includes/widgets/flickr.php');

//Misc
include_once 'includes/extras.php';
include_once 'includes/tincan.php';
include_once 'setup/eLearning-install.php';

include_once 'setup/installer/envato_setup.php';

// Options Panel
get_template_part('eLearning', 'options');

function rt_login_redirect($redirect_to, $set_for, $user) {
    $redirect_to = bp_core_get_user_domain($user->ID);
    return $redirect_to;
}

add_filter("login_redirect", 'rt_login_redirect', 20, 3);

function checkUserRole($role) {
    $user = wp_get_current_user();
    if (in_array($role, (array) $user->roles)) {
        return true;
    }
    return false;
}

add_filter('bp_core_enable_root_profiles', '__return_true');


//if(! function_exists('getCustomBodypressMenu')) {

/* Functions from elearning  child   START*/


//Add Action wp_enqueue_scripts
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

//Declare theme_enqueue_styles function (include parent and css files)
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',get_stylesheet_directory_uri() . '/style.css', array('parent-style'));
}//End theme_enqueue_styles function


function enqueue_media_uploader()
{
    //this function enqueues all scripts required for media uploader to work
    wp_enqueue_media();
}

add_action("wp_enqueue_scripts", "enqueue_media_uploader");

//Add Action wp_enqueue_scripts
add_action( 'wp_enqueue_scripts', 'fm_theme_enqueue_styles' );

//Declare fm_theme_enqueue_styles function (include child theme css files)
function fm_theme_enqueue_styles() {

    wp_enqueue_script( 'custom_js', get_template_directory_uri() . '/custom_js.js');
    wp_enqueue_script( 'dataTablesJS', get_template_directory_uri() . '/elearning_js/jquery.dataTables.min.js');
    wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/elearning_js/jquery-ui.min.js');
    wp_enqueue_script( 'buttonsDatTable', get_template_directory_uri() . '/elearning_js/buttons.min.js');
    wp_enqueue_script( 'printDatTable', get_template_directory_uri() . '/elearning_js/buttons.html5.min.js');


}//End fm_theme_enqueue_styles function

/*
Declare getCustomBodypressMenu function

*/
function getCustomBodypressMenu_child() {
    $bp = buddypress();
//
//            echo "<pre>";
//        print_r($bp->members->nav->get_primary());die;

    foreach ( $bp->members->nav->get_primary() as $user_nav_item ) {

        $delete_menus = array('marketing', 'sales', 'courses', 'course', 'edit-course',);

        if ( in_array($user_nav_item->slug, $delete_menus) ) {
            continue;
        }

        if ( empty( $user_nav_item->show_for_displayed_user ) && !bp_is_my_profile() ) {
            continue;
        }

        $selected = '';
        if ( bp_is_current_component( $user_nav_item->slug ) ) {
            $selected = ' class="current selected"';
        }

        if ( bp_loggedin_user_domain() ) {
            $link = str_replace( bp_loggedin_user_domain(), bp_displayed_user_domain(), $user_nav_item->link );
        } else {
            $link = trailingslashit( bp_displayed_user_domain() . $user_nav_item->link );
        }

        /**
         * Filters the navigation markup for the displayed user.
         *
         * This is a dynamic filter that is dependent on the navigation tab component being rendered.
         *
         * @since 1.1.0
         *
         * @param string $value Markup for the tab list item including link.
         * @param array $user_nav_item Array holding parts used to construct tab list item.
         * Passed by reference.
         */

        $current_user_login = wp_get_current_user()->user_login;
//            echo "<pre>";
//        if ( $user_nav_item->slug == 'profile' ) {
////        print_r($user_nav_item->slug);die;
//
//        }
//        else
        if ( $user_nav_item->slug == 'buddydrive' ) {

            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_' . $user_nav_item->css_id, array( '<li id="' . $user_nav_item->css_id . '-personal-li" ' . $selected . '><a id="user-' . $user_nav_item->css_id . '" href="' . $link . '">' . __( 'File', 'elearning' ) . '</a></li>', &$user_nav_item ) );

        } else {

            if ( $user_nav_item->name == "Users" ) {

                $user_nav_item->name = "Registrants";
            }

            if ( $user_nav_item->name == "Settings" ) {

//                $link = "/'.$current_user_login.'/?page=settings-integrations";
            }
//            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="' . $user_nav_item->css_id . '-personal-li" ' . $selected . '><a id="user-' . $user_nav_item->css_id . '" href="' . $link . '"><img src="http://' . $_SERVER['HTTP_HOST'] . '/wp-content/themes/elearning/images/icons/' . $user_nav_item->css_id . '/' . $user_nav_item->css_id . '.png" class="cus_icon">' . $user_nav_item->name . '</a></li>', &$user_nav_item));

        }

    }
//

    if ( $_SERVER['REQUEST_URI']== "/".$current_user_login."/account-dashboard/" ) {
        echo apply_filters_ref_array( 'bp_get_displayed_user_nav_accountSettings', array( '<li id="website-personal-li" class="current selected"><a id="user-website" href="/'.$current_user_login.'/account-dashboard/"><img src="/wp-content/themes/elearning/images/icons/account-dashboard/account-dashboard.png" class="cus_icon">DASHBOARD</a></li>' ) );
    } else {
        echo apply_filters_ref_array( 'bp_get_displayed_user_nav_accountSettings', array( '<li id="website-personal-li" ><a id="user-website" href="/'.$current_user_login.'/account-dashboard/"><img src="/wp-content/themes/elearning/images/icons/account-dashboard/account-dashboard.png" class="cus_icon">DASHBOARD</a></li>'));
    }
    if ( $_SERVER['REQUEST_URI'] == "/".$current_user_login."/events/" ) {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li" class="current selected"><a id="user-website" href="/'.$current_user_login.'/events/"><img src="/wp-content/themes/elearning/images/icons/events/events.png" class="cus_icon">EVENTS</a></li>'));
    } else {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li" ><a id="user-website" href="/'.$current_user_login.'/events/"><img src="/wp-content/themes/elearning/images/icons/events/events.png" class="cus_icon">EVENTS</a></li>'));
    }
    if ( $_SERVER['REQUEST_URI'] == "/".$current_user_login."/users/" ) {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li" class="current selected"><a id="user-website" href="/'.$current_user_login.'/users/"><img src="/wp-content/themes/elearning/images/icons/users/users.png" class="cus_icon">REGISTRANTS</a></li>'));
    } else {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li" ><a id="user-website" href="/'.$current_user_login.'/users/"><img src="/wp-content/themes/elearning/images/icons/users/users.png" class="cus_icon">REGISTRANTS</a></li>'));
    }
    isset($_REQUEST['page']) && !empty($_REQUEST['page']) ? $postType = $_REQUEST['page'] : $postType = '';
    if ( $postType == "website" ) {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li" class="current selected"><a id="user-website" href="/'.$current_user_login.'/website"><img src="/wp-content/themes/elearning/images/icons/accountSettings/website.png" class="cus_icon">WEBSITE</a></li>'));
    } else {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li" ><a id="user-website" href="/'.$current_user_login.'/website"><img src="/wp-content/themes/elearning/images/icons/accountSettings/website.png" class="cus_icon">WEBSITE</a></li>'));
    }

    if ( $postType == "profile" ) {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="profile-personal-li" class="current selected"><a id="user-profile" href="/'.$current_user_login.'/profile"><img src="/wp-content/themes/elearning/images/icons/accountSettings/profile.png" class="cus_icon">PROFILE</a></li>'));
    } else {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="profile-personal-li"><a id="user-profile" href="/'.$current_user_login.'/profile"><img src="/wp-content/themes/elearning/images/icons/accountSettings/profile.png" class="cus_icon">PROFILE</a></li>'));
    }

    if ( $postType == "settings-integrations" ) {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li" class="current selected"><a id="user-website" href="/'.$current_user_login.'/settings-integrations"><img src="/wp-content/themes/elearning/images/icons/accountSettings/accountSettings.png" class="cus_icon">SETTINGS</a></li>'));
    } else {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li" ><a id="user-website" href="/'.$current_user_login.'/settings-integrations"><img src="/wp-content/themes/elearning/images/icons/accountSettings/accountSettings.png" class="cus_icon">SETTINGS</a></li>'));
    }

}

/*
 * Make website url's friendly
 *
 *
 *
 * **/

function members_custom_url() {
    $url = $_SERVER['REQUEST_URI'];
    $current_user_login = wp_get_current_user()->user_login;
    if(isset($url) && $url != '' ) {
        $file_name_array = explode('/', $url);
        $before_filename = $file_name_array[1];
        $file_name = $file_name_array[2];

    }
    if($file_name != 'setting' && is_string($file_name) && $before_filename == $current_user_login) {
        do_action('eLearning_before_member_profile');
        get_header(eLearning_get_header());
        $profile_layout = eLearning_get_customizer('profile_layout');

        eLearning_include_template("profile/top$profile_layout.php");

        ?>
        <div id="item-body">
            <?php do_action('template_notices'); ?>
            <?php do_action('bp_before_member_body');
            locate_template(array('members/single/' . $file_name . '.php'), true);
            ?>
        </div>
        <?php
        do_action('bp_after_member_home_content');

//eLearning_include_template( "profile/bottom.php" );
//
//get_footer( eLearning_get_footer() );

        do_action('eLearning_after_member_profile');

    }
}
//members_custom_url();

//add_action( 'bb_init', 'members_custom_url' );

add_action( 'bp_actions', 'members_custom_url' );


//Check If this Post is event

if($_SERVER['HTTP_REFERER'] == get_site_url().'/wp-admin/edit.php?post_type=course'){
    add_action( 'edit_form_after_editor', 'is_event' );
}

function is_event()
{
    $post = $_REQUEST['post'];
    $value = get_post_meta($post, 'is_event', true);
    echo
        '
        <div id="is_event_settings" class="postbox" style="margin-top: 40px;">
            <button type="button" class="handlediv button-link" aria-expanded="true">
                <span class="screen-reader-text">Toggle panel: Course Settings</span>
                <span class="toggle-indicator" aria-hidden="true"></span>
            </button>
            <h2 class="hndle ui-sortable-handle">
                <span>Event Settings</span>
            </h2>
            <div class="is_event_container inside">
                <h2>Is Event</h2>
                <div class="select_button yesno">
                    <span>No</span>
                    <span>Yes</span>
                </div>
                <select name="is_event" id="is_event" class="select_val">
                    <option ' . (empty($value) || $value == "H" ? ' selected="selected" ' : null) . ' value="H">No</option>
                    <option ' . (!empty($value) && $value == "S" ? ' selected="selected" ' : null) . ' value="S">Yes</option>
                </select>
            </div>
        </div>
        ';
}

add_action( 'save_post', 'save_postdata');

function save_postdata()
{

    $postid = @$_POST['post_ID'];

    if ( !current_user_can( 'edit_page', $postid ) ) return false;
    if(empty($postid)) return false;

    if($_POST['action'] == 'editpost'){
        delete_post_meta($postid, 'is_event');
    }

    add_post_meta($postid, 'is_event', $_POST['is_event']);
}


function bp_get_options_event_nav($parent_slug = '') {
    $bp = buddypress();

    // If we are looking at a member profile, then the we can use the current
    // component as an index. Otherwise we need to use the component's root_slug.
    $component_index = !empty($bp->displayed_user) ? bp_current_component() : bp_get_root_slug(bp_current_component());
    $selected_item = bp_current_action();
  //  var_dump($selected_item);
    // Default to the Members nav.
    if (!bp_is_single_item()) {
        // Set the parent slug, if not provided.
        if (empty($parent_slug)) {
            $parent_slug = $component_index;
        }

        $secondary_nav_items = $bp->members->nav->get_secondary(array('parent_slug' => $parent_slug));

        if (!$secondary_nav_items) {
            return false;
        }

        // For a single item, try to use the component's nav.
    } else {
        $current_item = bp_current_item();
        $single_item_component = bp_current_component();

        // Adjust the selected nav item for the current single item if needed.
        if (!empty($parent_slug)) {
            $current_item = $parent_slug;
            $selected_item = bp_action_variable(0);
        }

        // If the nav is not defined by the parent component, look in the Members nav.
        if (!isset($bp->{$single_item_component}->nav)) {
            $secondary_nav_items = $bp->members->nav->get_secondary(array('parent_slug' => $current_item));
        } else {
            $secondary_nav_items = $bp->{$single_item_component}->nav->get_secondary(array('parent_slug' => $current_item));
        }

        if (!$secondary_nav_items) {
            return false;
        }
    }

    // Loop through each navigation item.
    foreach ($secondary_nav_items as $subnav_item) {
        if (empty($subnav_item->user_has_access)) {
            continue;
        }

        // If the current action or an action variable matches the nav item id, then add a highlight CSS class.
        //  var_dump($subnav_item->slug);

        if ($subnav_item->slug === $selected_item) {
            $selected = ' class="current selected"';
        } else {
            $selected = '';
        }

        // List type depends on our current component.
        $list_type = bp_is_group() ? 'groups' : 'personal';

        /**
         * Filters the "options nav", the secondary-level single item navigation menu.
         *
         * This is a dynamic filter that is dependent on the provided css_id value.
         *
         * @since 1.1.0
         *
         * @param string $value         HTML list item for the submenu item.
         * @param array  $subnav_item   Submenu array item being displayed.
         * @param string $selected_item Current action.
         */
        echo apply_filters('bp_get_options_nav_' . $subnav_item->css_id, '<li id="' . esc_attr($subnav_item->css_id . '-' . $list_type . '-li') . '" ' . $selected . '><a id="' . esc_attr($subnav_item->css_id) . '" href="' . esc_url($subnav_item->link) . '">' . $subnav_item->name . '</a></li>', $subnav_item, $selected_item);
    }
}

define('COURSE_EDIT_PAGE', 24);
define('EVENT_EDIT_PAGE', 623);

function createEventWebinar($event_id, $post_settings, $setting) {
    $unit = array();
    $post_metas = array();
    foreach ($post_settings as $setting) {

        switch ($setting->id) {
            case 'eLearning_start_date':
                $originalDate = $setting->value;
                $post_metas['webinar_start_date'] = date("d-m-Y", strtotime($originalDate));
                break;
            case 'eLearning_start_time':
                $originalDate = $setting->value;
                $post_metas['webinar_start_time'] = date("hh:ii:ss", strtotime($originalDate));
                break;
            case 'eLearning_expected_duration':
                $post_metas['webinar_expected_duration'] = $setting->value;
                break;
            case 'eLearning_event_is_recorded':
                $post_metas['webinar_is_recorded'] = $setting->value;
                break;
        }
    }
    $post_settings['post_type'] = 'unit';
    $post_settings['post_status'] = 'publish';

    $webinarId = wp_insert_post($post_settings);
    if ($webinarId) {
        $getOnstreamResults = createNewWebinarInOnstream($post_settings, $post_metas, $webinarId);

        if (isset($getOnstreamResults->id)) {
            $post_metas['onstreamSessionId'] = (int) $getOnstreamResults->id;
        } elseif (!empty($getOnstreamResults->error)) {
            echo $getOnstreamResults->error;
        }

        foreach ($post_metas as $key => $value) {
            update_post_meta($webinarId, $key, $value);
        }
    }
    return $webinarId;
}

function custom_pagination($numpages = '', $pagerange = '', $paged = '') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     * 
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if (!$numpages) {
            $numpages = 1;
        }
    }

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function. 
     */
    $pagination_args = array(
        'base' => get_pagenum_link(1) . '%_%',
        'format' => 'page/%#%',
        'total' => $numpages,
        'current' => $paged,
        'show_all' => false,
        'end_size' => 1,
        'mid_size' => $pagerange,
        'prev_next' => true,
        'prev_text' => __('<'),
        'next_text' => __('>'),
        'type' => 'array',
        'add_args' => false,
        'add_fragment' => ''
    );

    $paginate_links = paginate_links($pagination_args);



    if ($paginate_links) {
        echo "<nav class='custom-pagination'>";
        foreach ($paginate_links as $key => $pg) {
            if ($key === 0) {
                echo str_replace(1, "<", $pg).'  ';
                echo "Page " . $paged . " of " . $numpages;
            }
            if (sizeof($paginate_links) === $key + 1) {
                echo '  '.$pg;
            }
        }

//        echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
//        echo $paginate_links;
//        echo "</nav>";
    }
}

?>