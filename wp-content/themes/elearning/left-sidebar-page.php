<?php
/**
 * Template Name: Left Sidebar Page
 */

get_header(eLearning_get_header());
if ( have_posts() ) : while ( have_posts() ) : the_post();

$title=get_post_meta(get_the_ID(),'eLearning_title',true);
if(eLearning_validate($title) || empty($title)){
?>
<section id="title">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="pagetitle">
                    <?php
                        $breadcrumbs=get_post_meta(get_the_ID(),'eLearning_breadcrumbs',true);
                        if(eLearning_validate($breadcrumbs) || empty($breadcrumbs))
                            eLearning_breadcrumbs(); 
                    ?>
                    <h1><?php the_title(); ?></h1>
                    <?php the_sub_title(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
}
?>
<section id="content">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-9 col-sm-8 col-md-push-3 col-sm-push-3">
                <div class="content">
                    <?php
                        the_content();
                        $page_comments = eLearning_get_option('page_comments');
                        if(!empty($page_comments))
                            comments_template();
                     ?>
                </div>
                <?php
                
                endwhile;
                endif;
                ?>
            </div>
            <div class="col-md-3 col-sm-4 col-md-pull-9 col-sm-pull-8">
                <div class="sidebar">
                    <?php
                    $sidebar = apply_filters('eLearning_sidebar','mainsidebar',get_the_ID());
                    if ( !function_exists('dynamic_sidebar')|| !dynamic_sidebar($sidebar) ) : ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer(eLearning_get_footer());
?>