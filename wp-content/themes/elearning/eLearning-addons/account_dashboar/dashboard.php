<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_ACCOUNT_DASHBOARD_URL')) {
    define('ELEARNING_ACCOUNT_DASHBOARD_URL', get_template_directory_uri() . '/eLearning-addons/account-dashboar/');
}

include_once 'includes/functions.php';
include_once 'includes/dashboard.php';


?>