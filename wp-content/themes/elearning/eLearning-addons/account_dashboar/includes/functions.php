<?php

function eLearning_account_dashboard_settings_template() {
    if (!is_user_logged_in())
        wp_redirect(site_url());

    $template = 'templates/dashboard';
    global $bp;

 
    $located_template = apply_filters('bp_located_template', locate_template($template, false), $template);
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/dashboard'));
}

function get_admin_account_dashboard_content() {
    // var_dump('get_admin_sales_content');
}
