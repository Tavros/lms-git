<?php

class ELEARNING_USER_DASHBOARD {

    function __construct() {

        $this->init();
        add_action('bp_setup_nav', array($this, 'setup_nav'));
       // add_action('bp_after_sales_body', array($this, 'add_security_parameter'));
      //  add_filter('eLearning_logged_in_top_menu', array($this, 'add_sales_in_menu'));

       // add_filter('eLearning_course_nav_menu', array($this, 'eLearning_course_news_menu'));
    }

    function init() {
        if (!defined('ELEARNING_ACCOUNT_DASHBOARD_SLUG'))
            define('ELEARNING_ACCOUNT_DASHBOARD_SLUG', 'account-dashboard');
    }


    function setup_nav() {
        global $bp;
        $access = 0;
        if (function_exists('bp_is_my_profile'))
            $access = true;

        bp_core_new_nav_item(array(
            'name' => __('Dashboard', 'eLearning-sales'),
            'slug' => ELEARNING_ACCOUNT_DASHBOARD_SLUG,
            'position' => 1,
            'screen_function' => 'eLearning_account_dashboard_settings_template',
            'show_for_displayed_user' => true
        ));
    }

//    function add_security_parameter() {
//        wp_nonce_field('eLearning_security', 'security');
//    }

//    function add_sales_in_menu($menu) {
//        $dash_menu['sales'] = array(
//            'icon' => 'icon-meter',
//            'label' => __('Sales', 'eLearning-sales'),
//            'position' => 2,
//            'link' => bp_loggedin_user_domain() . ELEARNING_SALES_SLUG
//        );
//        foreach ($menu as $key => $item) {
//            $dash_menu[$key] = $item;
//        }
//        return $dash_menu;
//    }
}

new ELEARNING_USER_DASHBOARD();
?>
