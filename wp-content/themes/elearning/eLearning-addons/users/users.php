<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_USERS_URL')) {
    define('ELEARNING_USERS_URL', get_template_directory_uri() . '/eLearning-addons/users/');
}

include_once 'includes/functions.php';
include_once 'includes/users.php';
add_action('after_setup_theme', 'load_users_textdomain');

function load_users_textdomain() {
    load_textdomain('eLearning-users', dirname(__FILE__) . '/languages/');
}

?>