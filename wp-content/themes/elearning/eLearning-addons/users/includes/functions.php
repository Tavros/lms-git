<?php

function eLearning_users_template() {

    if (!is_user_logged_in())
        wp_redirect(site_url());

    $template = 'templates/users';
global $bp;


    if ($bp->current_component == 'users') {

        $translation_array = array(
            'earnings' => __('Earnings', 'eLearning-users'),
            'payout' => __('Payout', 'eLearning-users'),
            'students' => __('# Students', 'eLearning-users'),
            'saved' => __('SAVED', 'eLearning-users'),
            'saving' => __('SAVING ...', 'eLearning-users'),
            'select_recipients' => __('Select recipients...', 'eLearning-users'),
            'stats_calculated' => __('Stats Calculated, reloading page ...', 'eLearning-users')
        );
        wp_localize_script('eLearning-users-js', 'eLearning_users_strings', $translation_array);
    }


    $located_template = apply_filters('bp_located_template', locate_template($template, false), $template);

    


        bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/users'));

}


function get_admin_users_content(){
    var_dump('get_admin_users_content');
}
