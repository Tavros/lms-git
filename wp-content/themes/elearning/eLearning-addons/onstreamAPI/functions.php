<?php
//error_reporting(E_ALL);

//debug mode
$debug_mode_manual = (isset($_REQUEST['dbg']));

//profile short code settings
$shortCode_method = 'lmsViewWebinarLink';//'lmsViewRecordingLink'
$shortCode_name = 'lms_view_recording';//'lmsViewRecordingLink'



//admin api access
function getAdminOnstreamAccess() {
    $onstreamAPI = get_site_option('onstreamAPI');
    return $onstreamAPI['defaultAdminAccess'];
}

//api settings
$api_url = 'https://join.onstreammedia.com/api/2/';
//get credential data
$access = getOnstreamAdminAPIAccess();
//if($debug_mode_manual){echo "mode 1<pre>";print_r($access);}

//get new webinar id
function OnstreamAPI_get_new_webinar_id($settings) {
    $OnstreamAPIAccess = getAdminOnstreamAccess();
}

//get onstream admin api access
function getOnstreamAdminAPIAccess() {
    $ostreamAPIData = get_option('onstreamAPI');
    /* if ($ostreamAPIData['defaultAdminAccess']) {
         return $ostreamAPIData['defaultAdminAccess'];
     } */
    return array('username' => '67455335', 'password' => '69950618');
}

//update onstream api admin username, password
function updateOnstreamAdminData($userName, $password) {
    $onstreamAPIData = get_option('onstreamAPI');
    $onstreamAPIData['defaultAdminAccess'] = array('username' => $userName, 'password' => $password);
    update_option('onstreamAPI', $onstreamAPIData);

}

function createNewWebinarInOnstream($post_settings, $post_metas, $webinarId = '',$instructorId = '') {
    global $api_url,$access;
    if (empty($webinarId) && !empty($instructorId)) {
        $username = $access['username'];
        $password = $access['password'];
        // PUT session
        $parameters = array(
            'topic' => $post_settings['post_title'],
            'duration' => $post_settings['eLearning_expected_duration'],
            'start_time' => date("d-m-Y", strtotime($post_settings['eLearning_start_date'])) + ' ' + date("hh:mm:ss", strtotime($post_settings['eLearning_start_time'])),
            'invited_participants' => array()
        );
        // encode as JSON
        $json = json_encode($parameters);
        $postArgs = 'input_type=json&rest_data=' . $json;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $api_url . $username . '/session/format/json');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postArgs);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        $result = curl_exec($curl);
        $result = json_decode($result);
        curl_close($curl);
        $webinarId = $result->id;

    }
    $result = SetModeratorAPI($instructorId, $webinarId);
    return $result;
}

function SetModeratorAPI($instructorId, $webinarId) {
    global $api_url;
    $access = getOnstreamAdminAPIAccess();
    $current_user = get_userdata($instructorId);
    $username = $access['username'];
    $password = $access['password'];

    $parameters = array(
        'session_id' => $webinarId,
        'email' => $current_user->data->user_email,
        'first_name' => $current_user->data->user_nicename,
        'last_name' => $current_user->data->user_nicename,
        'role' => 1
    );
// encode as JSON
    $json = json_encode($parameters);
    $postArgs = 'input_type=json&rest_data=' . $json;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $api_url . $username . '/invitee/format/json');
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postArgs);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
    $result = curl_exec($curl);
    $result = json_decode($result);
    curl_close($curl);
    update_user_meta($current_user->data->ID, 'webinar_' . $webinarId, array('webinar_id' => $webinarId, 'onstream_user_id' => $result->id));
    return array('webinarId' => $webinarId, 'addedInstructorId' => $result->id );
}

function SetStudentAPI($webinarId, $userId) {
    global $api_url,$access;

    $user_info = get_userdata($userId);
    $userFirstName = get_user_meta($userId, 'first_name');
    $userLastName = get_user_meta($userId, 'last_name');
    $username = $access['username'];
    $password = $access['password'];
    $parameters = array(
        'session_id' => $webinarId,
        'email' => $user_info->data->user_email,
        'first_name' => $userFirstName[0],
        'last_name' => $userLastName[0],
        'role' => 2
    );
// encode as JSON
    $json = json_encode($parameters);
    $postArgs = 'input_type=json&rest_data=' . $json;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $api_url . $username . '/invitee/format/json');
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postArgs);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
    $result = curl_exec($curl);
    $result = json_decode($result);
    curl_close($curl);
    if (!isset($result->error)){

        if (empty(get_user_meta($userId,'webinar_' . $webinarId))){

            add_user_meta($userId, 'webinar_' . $webinarId, array('session_id' => $webinarId, 'onstream_user_id' => $result->id));
        }
        else{

            update_user_meta($userId, 'webinar_' . $webinarId, array('session_id' => $webinarId, 'onstream_user_id' => $result->id));
        }


    }

    return $result->id;
}
/*
 * Function for getting current user webinar video link by onstream registered id
 *
 */
function getUsersVideoLinks( $onstream_user_id , $userId, $webinarId ) {
    global $debug_mode_manual,$api_url,$access;
    //Get user role
    $userRole = get_userdata($userId)->roles[0];
    $username = $access['username'];
    $password = $access['password'];
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $api_url . $username . '/invitees/id/'.$webinarId.'/format/json');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
    $result = curl_exec($curl);
    curl_close($curl);
    $result = json_decode($result, true);
    if($debug_mode_manual){echo "<pre>";print_r($result);}
    $result = ($result && count($result) > 0)
        ? ((isset($result['participants']))
            ? array_merge($result['moderators'],$result['participants'])
            : $result['moderators'])
    : false;
    if($debug_mode_manual){echo "mode 2<pre>";print_r($result);}
    if($debug_mode_manual){echo "mode 3<pre>";print_r($result[array_search($onstream_user_id,$result)]);}

    return $result[array_search($onstream_user_id,$result)]['personal_session_link'];

}
/*
show webinar video link button by event_id

int event_id

*/

//webinar current user video link json data
function lmsViewWebinarLink( $atts ){

    global $wpdb,$debug_mode_manual;
    $event_id = '';

    /*check if not exists then break */
    if( !isset(  $atts['event_id']) || empty($atts['event_id']) ) return ;

    $event_id = $atts['event_id'];
    if($debug_mode_manual){echo "event id: <pre>";print_r($event_id."\n");}
    /*get webinar id of event*/
    $webinarId = get_post_meta($event_id,'eLearning_webinar_id')[0];

    if($debug_mode_manual){echo "<pre>get_post_meta: ";print_r(get_post_meta($event_id));}
    /* get login user*/
    $cur_user_id = get_current_user_id();

    $query_invited_user_id = "SELECT * FROM `wp_usermeta` WHERE `user_id` = {$cur_user_id} and `meta_key` = 'webinar_{$webinarId}'";

    /*Get current user onstream_user_id*/
    $invited_user_id_data = $wpdb->get_results($query_invited_user_id  );
    if($debug_mode_manual){echo "mode webinar query<pre>";print_r($query_invited_user_id);}
    if($debug_mode_manual){echo "mode webinar query<pre>";print_r($invited_user_id_data);}
    if(isset($invited_user_id_data[0]))
    {
        $data = $invited_user_id_data[0]->meta_value;
        $data = unserialize($data);

        if( isset($data['onstream_user_id']))
        {
            $onstream_user_id = $data['onstream_user_id'];

            $webinarUrl = getUsersVideoLinks( $onstream_user_id , $cur_user_id, $webinarId );
            $eventData = get_post($event_id);
            $eventData_meta = get_post_meta($event_id);
            if($debug_mode_manual){echo "mode 4<pre>";print_r($eventData);}
            if($debug_mode_manual){echo "mode 5<pre>";print_r($eventData_meta);}
        }
    }
    echo '<table id="events_data" class="table table-hover info_table" border="0">
            <thead>
                <tr>                        
                    <th>Event Name</th>
                    <th>Duration</th>
                    <th>Start Date</th>
                    <th>Webinar Link</th>
                </tr>
            </thead>
            <tbody>                                 
            ';
    if(count($eventData) > 0 && count($eventData_meta) > 0) {
        echo '<tr>
                    <td class="col-md-3">' . $eventData->post_title . '</td>
                    <td class="col-md-1">'. $eventData_meta['eLearning_duration'][0] .  '</td>
                    <td class="col-md-1 hs-active">'. $eventData_meta['eLearning_start_date'][0] .' '.$eventData_meta['eLearning_start_time'][0].'</td>
                    <td class="col-md-1"><a  href="'.$webinarUrl.'" target="_blank">See Webinar</a></td>
                </tr>';

    }else{
        echo '<tr>
                    <td class="col-md-3">-</td>
                    <td class="col-md-1">-</td>
                    <td class="col-md-1 hs-active">-</td>
                    <td class="col-md-1">-</td>
                </tr>';
    }
    echo '
            </tbody>
         </table>';die;
}

//add_shortcode('lms_view_webinar', 'lmsViewWebinarLink');

//get recordings
function showOnstreamRecordings($cur_user_id, $webinarId){
    global $api_url,$access;
    //Get user role
//    $userRole = get_userdata($cur_user_id)->roles[0];
    $userRole = 'administrator';
    $username = $access['username'];
    $password = $access['password'];
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL,
        $api_url.$username.'/recordings/format/json');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
    $result = curl_exec($curl);
    curl_close($curl);
    $array  = json_decode($result,true);
    //die( var_dump($array));
    return $array;

}

/*
show webinar video link button by event_id

int event_id

*/
function lmsViewRecordingLink( $atts ){

    global $wpdb;
    $event_id = '';

    /*check if not exists then break */
    if( !isset(  $atts['event_id']) || empty($atts['event_id']) ) return ;

    $event_id = $atts['event_id'];

    /*get webinar id of event*/
//    $webinarId = get_post_meta($event_id,'eLearning_webinar_id')[0];
    $webinarId = 59494;

    /* get login user*/
    $cur_user_id = get_current_user_id();

    $query_invited_user_id = "SELECT * FROM `wp_usermeta` WHERE `user_id` = '{$cur_user_id}' and `meta_key` = 'webinar_{$webinarId}'";

    /*Get current user onstream_user_id*/
    $invited_user_id_data = $wpdb->get_results($query_invited_user_id  );

    //if on-stream registered
    if(isset($invited_user_id_data[0]))
    {
        //on-stream user_id serialized
        $data = $invited_user_id_data[0]->meta_value;
        //un-serialize data
        $data = unserialize($data);

        //check for on-stream user_id
        if( isset($data['onstream_user_id']))
        {
         //   $onstream_user_id = $data['onstream_user_id'];
            $onstream_user_id = 1027332;

            //?$recordingEmbed = showOnstreamRecordings( $cur_user_id, $webinarId );

            //return webinar user video link
            $getWebinarData = getUsersVideoLinks( $onstream_user_id , $cur_user_id, $webinarId );

            //combine and unique onstream webinar users
            //$getWebinarData = array_merge($getWebinarData['moderators'],$getWebinarData['participants']);

            //get more users and merge
            /*foreach($recordingEmbed as $item) {
                if (!empty($item['viewers'])){
                    array_merge($getWebinarData,$item['viewers']);
                }
            }*/
            //make array unique
            //$getWebinarData = array_unique($getWebinarData);
            $temp_rec = [];
            /*
            if(count($recordingEmbed) > 0){
                foreach($recordingEmbed as $item) {
                    foreach ($item['viewers'] as $user) {
                        if ($user['id'] == $onstream_user_id) {
                            $temp_rec[] = ['title'=>$item['title'],'duration'=>$item['duration'],'creation_date'=>$item['creation_date'],'personal_link'=>$user['personal_link']];
                        }
                    }
                }
            }*/

            echo '<table id="events_data" class="table table-hover info_table" border="0">
            <thead>
                <tr>                        
                    <th>Event</th>
                    <th>Duration</th>
                    <th>Created Date</th>
                    <th>Link</th>
                </tr>
            </thead>
            <tbody>                                 
            ';
            if(count($temp_rec) > 0) {
                foreach ($temp_rec as $item) {
                    if(strpos('play',$item['personal_link'])){
                        $videoLink = "<embed src='".$item['personal_link']."' width='600' height='500'>";
                    }
                    else{
                        $videoLink = "<a href='".$item['personal_link']."'><Video</a>";
                    }

                    echo '<tr>
                    <td class="col-md-3">' . $item['title'] . '</td>
                    <td class="col-md-1">' . $item['duration'] . '</td>
                    <td class="col-md-1 hs-active">' . $item['creation_date'] . '</td>
                    <td class="col-md-1">'.$videoLink.'</td>
                </tr>';
                }
            }else{
                echo '<tr>
                    <td class="col-md-3">-</td>
                    <td class="col-md-1">-</td>
                    <td class="col-md-1 hs-active">-</td>
                    <td class="col-md-1">-</td>
                </tr>';
            }
            echo '
            </tbody>
         </table>';
        }
    }


}

add_shortcode($shortCode_name, $shortCode_method);



function onstream_save_access_action() {


// Bail if not a POST action.
    if ('POST' !== strtoupper($_SERVER['REQUEST_METHOD']))
        return;

// Bail if no submit action.
    if (!isset($_POST['bp_peyotto_form_subimitted']))
        return;


// Bail if not in settings.
    if (!bp_is_current_action('setting-onstream'))
        return;

    $bp = buddypress(); // The instance
    $error = false;        // invalid|blocked|taken|empty|nochange
    $feedback_type = 'error';      // success|error
    $feedback = array();      // array of strings for feedback.
    check_admin_referer('bp_onstream_account_settings');

// Validate the user again for the current password when making a big change.
    if (is_super_admin()) {
        $userName = esc_html(trim($_POST['ostream_username']));
        $password = esc_html(trim($_POST['ostream_password']));
        if (!$password || !$userName) {
            $error = 'input_filds';
        } else {
            updateOnstreamAdminData($userName, $password);
        }
    } else {
        $error = 'access_denied';
    }

// Email feedback.
    switch ($error) {
        case 'input_filds' :
            $feedback['input_filds'] = __('Invalid Onstream API Username or Password', 'buddypress');
            break;
        case 'access_denied' :
            $feedback['access_denied'] = __('You cannot modify this information', 'buddypress');
            break;
        case 'error' :
            $feedback['email_taken'] = __('That email address is already taken.', 'buddypress');
            break;
        case false :
// No change.
            break;
    }



// No errors so show a simple success message.
    if (false === $error) {
        $feedback[] = __('Your Onstrem settings have been saved.', 'buddypress');
        $feedback_type = 'success';
    }

// Set the feedback.
    bp_core_add_message(implode("\n", $feedback), $feedback_type);

    /**
     * Fires after the general settings have been saved, and before redirect.
     *
     * @since 1.5.0
     */
    do_action('bp_core_general_settings_after_save');

// Redirect to prevent issues with browser back button.
    bp_core_redirect(trailingslashit(bp_displayed_user_domain() . 'setting/setting-onstream/'));
}

add_action('bp_actions', 'onstream_save_access_action');


