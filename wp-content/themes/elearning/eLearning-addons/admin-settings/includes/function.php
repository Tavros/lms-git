<?php

function save_peyotto_form_data() {
    // var_dump(bp_current_component());
    // var_dump(bp_current_action());
    if (!empty($_POST['bp_peyotto_form_subimitted'])) {
        switch (bp_current_component()) :
            case 'accountSettings' :
                save_accountSettings_form();
                break;
            default:
                break;
        endswitch;
    }
}

function save_accountSettings_form() {
    switch (bp_current_action()) :
        case 'accountSettings' :
        case 'accountSettings-general' :
            peyotto_general_form_save();
            break;
        default:
            break;
    endswitch;
}
