<?php

if (!defined('ABSPATH'))
    exit;
if (!defined('ELEARNING_ADMIN_SETTINGS_SLUG')) {
    define('ELEARNING_ADMIN_SETTINGS_SLUG', 'admin_settings');
    define('ELEARNING_ADMIN_SETTINGS_GENETRAL_SLUG', 'admin_settings-general');
    define('ELEARNING_ADMIN_SETTINGS_PROFILE_SLUG', 'admin_settings-profile');
    define('ELEARNING_ADMIN_SETTINGS_PRIVACY_SLUG', 'admin_settings-privacy');
    define('ELEARNING_ADMIN_SETTINGS_NOTIFICATIONS_SLUG', 'admin_settings-notifications');
    define('ELEARNING_ADMIN_SETTINGS_BILLING_SLUG', 'admin_settings-billing');
}


include_once 'includes/function.php';
include_once 'includes/general.php';

function bp_admin_settings_nav_item() {
    global $bp;
    $admin_settings_link = trailingslashit(bp_loggedin_user_domain() . ELEARNING_ADMIN_SETTINGS_SLUG);
    $user_access = bp_is_my_profile();
    $args = array(
        'name' => __('Settings', 'eLearning'),
        'slug' => 'admin_settings',
        'default_subnav_slug' => ELEARNING_ADMIN_SETTINGS_SLUG,
        'position' => 200,
        'show_for_displayed_user' => false,
        'screen_function' => 'bp_admin_settings_user_nav_item_screen',
        'item_css_id' => 'admin_settings'
    );


    bp_core_new_nav_item($args);
    bp_core_new_subnav_item(array(
        'name' => __('General', 'eLearning'),
        'slug' => ELEARNING_ADMIN_SETTINGS_GENETRAL_SLUG,
        'parent_slug' => ELEARNING_ADMIN_SETTINGS_SLUG,
        'parent_url' => $admin_settings_link,
        'screen_function' => 'bp_admin_settings_results',
        'position' => 30,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Profile', 'eLearning'),
        'slug' => ELEARNING_ADMIN_SETTINGS_PROFILE_SLUG,
        'parent_slug' => ELEARNING_ADMIN_SETTINGS_SLUG,
        'parent_url' => $admin_settings_link,
        'screen_function' => 'bp_admin_settings_results',
        'position' => 40,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Privacy', 'eLearning'),
        'slug' => ELEARNING_ADMIN_SETTINGS_PRIVACY_SLUG,
        'parent_slug' => ELEARNING_ADMIN_SETTINGS_SLUG,
        'parent_url' => $admin_settings_link,
        'screen_function' => 'bp_admin_settings_results',
        'position' => 50,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Notifications', 'eLearning'),
        'slug' => ELEARNING_ADMIN_SETTINGS_NOTIFICATIONS_SLUG,
        'parent_slug' => ELEARNING_ADMIN_SETTINGS_SLUG,
        'parent_url' => $admin_settings_link,
        'screen_function' => 'bp_admin_settings_results',
        'position' => 60,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Billing', 'eLearning'),
        'slug' => ELEARNING_ADMIN_SETTINGS_BILLING_SLUG,
        'parent_slug' => ELEARNING_ADMIN_SETTINGS_SLUG,
        'parent_url' => $admin_settings_link,
        'screen_function' => 'bp_admin_settings_results',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
}

function eLearning_logged_bp_admin_settings_nav_item($menu) {
    $dash_menu['settings'] = array(
        'icon' => 'icon-meter',
        'label' => __('Settings', 'eLearning-customlinks'),
        'position' => 2,
        'link' => '/lead-capture'
    );
    foreach ($menu as $key => $item) {
        $dash_menu[$key] = $item;
    }
    return $dash_menu;
}

add_action('bp_setup_nav', 'bp_admin_settings_nav_item', 99);
add_filter('eLearning_logged_in_top_menu', 'eLearning_logged_bp_admin_settings_nav_item', 99);

function bp_admin_settings_user_nav_item_screen() {
    add_action('bp_template_content', 'bp_custom_screen_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/admin_settings'));
}

function bp_admin_settings_results() {
    add_action('bp_template_content', 'bp_custom_screen_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/admin_settings'));
}

function bp_custom_screen_content() {

//    echo 'the custom content.
//    You can put a post loop here,
//    pass $user_id with bp_displayed_user_id()';
}

add_action('bp_ready', 'save_peyotto_form_data');
?>
