<?php

if (!defined('ABSPATH'))
    exit;

define('ELEARNING_ACCOUNTSETTINGS_SLUG', 'setting');
define('ELEARNING_ACCOUNTSETTINGS_SITE_SLUG', 'site');
define('ELEARNING_ACCOUNTSETTINGS_PROFILE_SLUG', 'setting-account');
define('ELEARNING_ACCOUNTSETTINGS_LEARNING_SLUG', 'setting-learning');
define('ELEARNING_ACCOUNTSETTINGS_MARKETING_SLUG', 'setting-marketing');
define('ELEARNING_ACCOUNTSETTINGS_MEMBERSHIP_SLUG', 'setting-membership');
define('ELEARNING_ACCOUNTSETTINGS_COMMERCE_SLUG', 'setting-commerce');
define('ELEARNING_ACCOUNTSETTINGS_STORE_SLUG', 'setting-onstream');
define('ELEARNING_ACCOUNTSETTINGS_EMAIL_SLUG', 'setting-email');

define('ELEARNING_ACCOUNTSETTINGS_SITE_PAGE_SLUG', 'site-page');
define('ELEARNING_ACCOUNTSETTINGS_SITE_GENERAL_SLUG', 'site-general');
define('ELEARNING_ACCOUNTSETTINGS_SITE_GOOGLE_SLUG', 'site-google');
define('ELEARNING_ACCOUNTSETTINGS_SITE_HEADER_SLUG', 'site-header');
define('ELEARNING_ACCOUNTSETTINGS_SITE_FOOTER_SLUG', 'site-footer');


define('ELEARNING_ACCOUNTSETTINGS_COMMERCE_GENERAL_SLUG', 'commerce-general');
define('ELEARNING_ACCOUNTSETTINGS_COMMERCE_PAYPAL_SLUG', 'commerce-paypal');
define('ELEARNING_ACCOUNTSETTINGS_COMMERCE_STRIPE_SLUG', 'commerce-stripe');



include_once 'includes/function.php';
include_once 'includes/general.php';

function bp_accountSettings_nav_item() {
    global $bp;
    $accountSettings_link = trailingslashit(bp_loggedin_user_domain() . ELEARNING_ACCOUNTSETTINGS_SLUG);
    $user_access = bp_is_my_profile();
    bp_core_new_nav_item(array(
        'name' => __('Settings', 'eLearning'),
        'slug' => 'setting',
        'default_subnav_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'position' => 200,
        'show_for_displayed_user' => false,
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'item_css_id' => 'accountSettings'
    ));
    bp_core_new_nav_item(array(
        'name' => __('Add Course', 'eLearning'),
        'slug' => 'edit-course',
        'default_subnav_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'position' => 190,
        'show_for_displayed_user' => false,
        'screen_function' => 'redirect_to_add_course',
        'item_css_id' => 'accountSettings'
    ));

    bp_core_new_subnav_item(array(
        'name' => __('Site', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_SITE_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'parent_url' => $accountSettings_link,
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 30,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));


    bp_core_new_subnav_item(array(
        'name' => __('Page', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_SITE_PAGE_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SITE_SLUG,
        'parent_url' => $accountSettings_link . ELEARNING_ACCOUNTSETTINGS_SITE_SLUG . '/',
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));

    bp_core_new_subnav_item(array(
        'name' => __('General', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_SITE_GENERAL_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SITE_SLUG,
        'parent_url' => $accountSettings_link . ELEARNING_ACCOUNTSETTINGS_SITE_SLUG . '/',
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));

    bp_core_new_subnav_item(array(
        'name' => __('Google', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_SITE_GOOGLE_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SITE_SLUG,
        'parent_url' => $accountSettings_link . ELEARNING_ACCOUNTSETTINGS_SITE_SLUG . '/',
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));

    bp_core_new_subnav_item(array(
        'name' => __('Header', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_SITE_HEADER_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SITE_SLUG,
        'parent_url' => $accountSettings_link . ELEARNING_ACCOUNTSETTINGS_SITE_SLUG . '/',
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));

    bp_core_new_subnav_item(array(
        'name' => __('Footer', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_SITE_FOOTER_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SITE_SLUG,
        'parent_url' => $accountSettings_link . ELEARNING_ACCOUNTSETTINGS_SITE_SLUG . '/',
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));



    bp_core_new_subnav_item(array(
        'name' => __('Account', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_PROFILE_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'parent_url' => $accountSettings_link,
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 40,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));

    bp_core_new_subnav_item(array(
        'name' => __('Learning', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_LEARNING_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'parent_url' => $accountSettings_link,
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 50,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Marketing', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_MARKETING_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'parent_url' => $accountSettings_link,
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 60,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Membership', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_MEMBERSHIP_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'parent_url' => $accountSettings_link,
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));






    bp_core_new_subnav_item(array(
        'name' => __('Commerce', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_COMMERCE_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'parent_url' => $accountSettings_link,
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));

    bp_core_new_subnav_item(array(
        'name' => __('General', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_COMMERCE_GENERAL_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_COMMERCE_SLUG,
        'parent_url' => $accountSettings_link . ELEARNING_ACCOUNTSETTINGS_COMMERCE_SLUG . '/',
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));


    bp_core_new_subnav_item(array(
        'name' => __('Paypal', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_COMMERCE_PAYPAL_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_COMMERCE_SLUG,
        'parent_url' => $accountSettings_link . ELEARNING_ACCOUNTSETTINGS_COMMERCE_SLUG . '/',
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));

    bp_core_new_subnav_item(array(
        'name' => __('Stripe', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_COMMERCE_STRIPE_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_COMMERCE_SLUG,
        'parent_url' => $accountSettings_link . ELEARNING_ACCOUNTSETTINGS_COMMERCE_SLUG . '/',
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));


    bp_core_new_subnav_item(array(
        'name' => __('Email', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_EMAIL_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'parent_url' => $accountSettings_link,
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));

    bp_core_new_subnav_item(array(
        'name' => __('Onstream Settings', 'eLearning'),
        'slug' => ELEARNING_ACCOUNTSETTINGS_STORE_SLUG,
        'parent_slug' => ELEARNING_ACCOUNTSETTINGS_SLUG,
        'parent_url' => $accountSettings_link,
        'screen_function' => 'bp_accountSettings_user_nav_item_screen',
        'position' => 80,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
}

add_action('bp_setup_nav', 'bp_accountSettings_nav_item', 99);

function bp_accountSettings_user_nav_item_screen() {
    add_action('bp_template_content', 'bp_custom_screen_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/admin-settings'));
}

function redirect_to_add_course() {
    wp_redirect(home_url().'/edit-course', '302');
}

//function eLearning_logged_bp_admin_settings_nav_item($menu) {
////    $dash_menu['settings'] = array( 
////        'icon' => 'icon-meter',
////        'label' => __('Settings1111', 'eLearning-customlinks'),
////        'position' => 1000,
////        'link' => 'http://lead-capture.com'
////    );
////    foreach ($menu as $key => $item) {      
////        $dash_menu[$key] = $item;
////    }
//    //var_dump($dash_menu);
//    //return $dash_menu;
//}
//
//add_filter('eLearning_logged_in_top_menu', 'eLearning_logged_bp_admin_settings_nav_item', 99);










function bp_custom_screen_content() {

//    echo 'the custom content.
//    You can put a post loop here,
//    pass $user_id with bp_displayed_user_id()';
}

add_action('bp_ready', 'save_peyotto_form_data');
?>
