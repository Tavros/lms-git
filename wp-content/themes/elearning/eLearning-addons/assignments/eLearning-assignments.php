<?php

if (!defined('ABSPATH'))
    exit;
include_once 'includes/assignments_functions.php';
include_once 'includes/assignments.php';


define('ELEARNING_ASSIGNMENTS_CPT', 'eLearning-assignment');
define('ELEARNING_ASSIGNMENTS_SLUG', 'assignment');

function initialize_assignments() {
    ELEARNING_Assignments::init();
}

if (class_exists('ELEARNING_Assignments')) {
    add_action('plugins_loaded', 'initialize_assignments', 100);
}




add_action('wp_enqueue_scripts', 'eLearning_assignments_enqueue_scripts');

function eLearning_assignments_enqueue_scripts() {
    if ((function_exists('bp_current_action') && is_singular('course') && bp_current_action() == 'submissions') || (isset($_GET['action']) && $_GET['action'] == 'admin' && isset($_GET['submissions']))) {
        wp_enqueue_script('plupload');
    }
    if (is_singular('eLearning-assignment') || (isset($_GET['action']) && $_GET['action'] == 'admin' && isset($_GET['submissions'])) || (function_exists('bp_current_action') && bp_current_action() == 'submissions')) {
         
        wp_enqueue_style('eLearning-assignments-css', get_template_directory_uri() .'/eLearning-addons/assignments/css/eLearning-assignments.css');
        wp_enqueue_script('eLearning-assignments-js',get_template_directory_uri() .'/eLearning-addons/assignments/js/eLearning-assignments.js');
        $translation_array = array(
            'assignment_reset' => __('This step is irreversible. All Assignment submissions would be reset for this user. Are you sure you want to Reset the Assignment for this User? ', 'eLearning-assignments'),
            'assignment_reset_button' => __('Confirm, Assignment reset for this User', 'eLearning-assignments'),
            'marks_saved' => __('Marks Saved', 'eLearning-assignments'),
            'assignment_marks_saved' => __('Assignment Marks Saved', 'eLearning-assignments'),
            'cancel' => __('Cancel', 'eLearning-assignments'),
            'incorrect_file_format' => __('Incorrect file format ', 'eLearning-assignments'),
            'duplicate_file' => __('File already selected ', 'eLearning-assignments'),
            'remove_attachment' => _x('Are you sure you want to remove this attachment ?', 'Notification when user removes the attachment from assignment', 'eLearning-assignments'),
        );
        wp_localize_script('eLearning-assignments-js', 'eLearning_assignment_messages', $translation_array);
    }
}

add_action('after_setup_theme', 'load_assignments_textdomain');
function load_assignments_textdomain() {
    load_textdomain('eLearning-assignments', dirname(__FILE__) . '/languages/');
}
