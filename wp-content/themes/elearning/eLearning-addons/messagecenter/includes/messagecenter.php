<?php

class ELEARNING_messagecenter {

    function __construct() {

        $this->init();
        add_action('bp_setup_nav', array($this, 'setup_nav'));
        add_action('bp_after_messagecenter_body', array($this, 'add_security_parameter'));
        add_filter('eLearning_logged_in_top_menu', array($this, 'add_messagecenter_in_menu'));

       // add_filter('eLearning_course_nav_menu', array($this, 'eLearning_course_news_menu'));
    }

    function init() {
        if (!defined('ELEARNING_MESSAGECENTER_SLUG'))
            define('ELEARNING_MESSAGECENTER_SLUG', 'messagecenter');
    }


    function setup_nav() {
        global $bp;
        $access = 0;
        if (function_exists('bp_is_my_profile'))
            $access = apply_filters('eLearning_student_messagecenter_access', bp_is_my_profile());

        bp_core_new_nav_item(array(
            'name' => __('Message center', 'eLearning-messagecenter'),
            'slug' => ELEARNING_MESSAGECENTER_SLUG,
            'position' => 4,
            'screen_function' => 'eLearning_messagecenter_template',
            'show_for_displayed_user' => $access
        ));
    }

    function add_security_parameter() {
        wp_nonce_field('eLearning_security', 'security');
    }

    function add_messagecenter_in_menu($menu) {
        $dash_menu['messagecenter'] = array(
            'icon' => 'icon-meter',
            'label' => __('Message center', 'eLearning-messagecenter'),
            'position' => 2,
            'link' => bp_loggedin_user_domain() . ELEARNING_MESSAGECENTER_SLUG
        );
        foreach ($menu as $key => $item) {
            $dash_menu[$key] = $item;
        }
        return $dash_menu;
    }

    function eLearning_course_news_menu($links) {
//
//        if (function_exists('eLearning_get_option')) {
//            $show_news = eLearning_get_option('show_news');
//            if (isset($show_news) && $show_news) {
//                $links['news'] = array(
//                    'id' => 'news',
//                    'label' => __('News', 'eLearning-messagecenter'),
//                    'action' => 'news',
//                    'link' => bp_get_course_permalink(),
//                );
//            }
//        }
//        return $links;
    }



}

new ELEARNING_Messagecenter();
?>
