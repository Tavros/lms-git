<?php

function eLearning_messagecenter_template() {

    if (!is_user_logged_in())
        wp_redirect(site_url());

    $template = 'templates/messagecenter';
global $bp;


    if ($bp->current_component == 'messagecenter') {

        $translation_array = array(
            'earnings' => __('Earnings', 'eLearning-messagecenter'),
            'payout' => __('Payout', 'eLearning-messagecenter'),
            'students' => __('# Students', 'eLearning-messagecenter'),
            'saved' => __('SAVED', 'eLearning-messagecenter'),
            'saving' => __('SAVING ...', 'eLearning-messagecenter'),
            'select_recipients' => __('Select recipients...', 'eLearning-messagecenter'),
            'stats_calculated' => __('Stats Calculated, reloading page ...', 'eLearning-messagecenter')
        );
        wp_localize_script('eLearning-messagecenter-js', 'eLearning_messagecenter_strings', $translation_array);
    }


    $located_template = apply_filters('bp_located_template', locate_template($template, false), $template);

  



        bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/messagecenter'));

}


function get_admin_messagecenter_content(){
    var_dump('get_admin_messagecenter_content');
}
