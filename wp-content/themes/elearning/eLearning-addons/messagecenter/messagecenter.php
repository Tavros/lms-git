<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_MESSAGECENTER_URL')) {
    define('ELEARNING_MESSAGECENTER_URL', get_template_directory_uri() . '/eLearning-addons/messagecenter/');
}

include_once 'includes/functions.php';
include_once 'includes/messagecenter.php';
add_action('after_setup_theme', 'load_messagecenter_textdomain');

function load_messagecenter_textdomain() {
    load_textdomain('eLearning-messagecenter', dirname(__FILE__) . '/languages/');
}

?>