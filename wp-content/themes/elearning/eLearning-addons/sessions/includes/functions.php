<?php

function eLearning_sessions_template() {

    if (!is_user_logged_in())
        wp_redirect(site_url());

    $template = 'templates/sessions';
global $bp;


    if ($bp->current_component == 'sessions') {

        $translation_array = array(
            'earnings' => __('Earnings', 'eLearning-sessions'),
            'payout' => __('Payout', 'eLearning-sessions'),
            'students' => __('# Students', 'eLearning-sessions'),
            'saved' => __('SAVED', 'eLearning-sessions'),
            'saving' => __('SAVING ...', 'eLearning-sessions'),
            'select_recipients' => __('Select recipients...', 'eLearning-sessions'),
            'stats_calculated' => __('Stats Calculated, reloading page ...', 'eLearning-sessions')
        );
        wp_localize_script('eLearning-sessions-js', 'eLearning_sessions_strings', $translation_array);
    }


    $located_template = apply_filters('bp_located_template', locate_template($template, false), $template);

     var_dump('errrrrrrrrrrrrrr',$located_template);



        bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/sessions'));

}


function get_admin_sessions_content(){
    var_dump('get_admin_sessions_content');
}
