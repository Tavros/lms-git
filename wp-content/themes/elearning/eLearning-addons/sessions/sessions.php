<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_SESSIONS_URL')) {
    define('ELEARNING_SESSIONS_URL', get_template_directory_uri() . '/eLearning-addons/sessions/');
}

include_once 'includes/functions.php';
include_once 'includes/sessions.php';
add_action('after_setup_theme', 'load_sessions_textdomain');

function load_sessions_textdomain() {
    load_textdomain('eLearning-sessions', dirname(__FILE__) . '/languages/');
}

?>