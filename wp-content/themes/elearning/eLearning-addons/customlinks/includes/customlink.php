<?php

class ELEARNING_customlinks {

    function __construct() {

        $this->init();
        add_action('bp_setup_nav', array($this, 'setup_nav'));
        add_action('bp_after_customlinks_body', array($this, 'add_security_parameter'));
        add_filter('eLearning_logged_in_top_menu', array($this, 'add_customlinks_in_menu'));

        // add_filter('eLearning_course_nav_menu', array($this, 'eLearning_course_news_menu'));
    }

    function init() {
        
    }

    function setup_nav() {
        global $bp;
        $access = 0;
        if (function_exists('bp_is_my_profile'))
            $access = apply_filters('eLearning_student_customlinks_access', bp_is_my_profile());

        bp_core_new_nav_item(array(
            'name' => __('Lead Capture', 'eLearning-customlinks'),
            'slug' => 'link',
            'position' => 150,
            'screen_function' => 'eLearning_locadCapture',
            'show_for_displayed_user' => $access
        ));
        bp_core_new_nav_item(array(
            'name' => __('Lead Capture', 'eLearning-customlinks'),
            'slug' => 'link',
            'position' => 156,
            'screen_function' => 'eLearning_locadCapture',
            'show_for_displayed_user' => $access
        ));
    }

    function add_security_parameter() {
        wp_nonce_field('eLearning_security', 'security');
    }

    function add_customlinks_in_menu($menu) {
        $dash_menu['customlinks'] = array(
            'icon' => 'icon-meter',
            'label' => __('Lead Capture', 'eLearning-customlinks'),
            'position' => 2,
            'link' => '/lead-capture'
        );
        foreach ($menu as $key => $item) {
            $dash_menu[$key] = $item;
        }
        return $dash_menu;
    }

}

new ELEARNING_customlinks();
?>
