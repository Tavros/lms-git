<?php

class ELEARNING_marketing {

    function __construct() {

        $this->init();
        add_action('bp_setup_nav', array($this, 'setup_nav'));
        add_action('bp_after_marketing_body', array($this, 'add_security_parameter'));
        add_filter('eLearning_logged_in_top_menu', array($this, 'add_marketing_in_menu'));

       // add_filter('eLearning_course_nav_menu', array($this, 'eLearning_course_news_menu'));
    }

    function init() {
        if (!defined('ELEARNING_MARKETING_SLUG'))
            define('ELEARNING_MARKETING_SLUG', 'marketing');
    }


    function setup_nav() {
        global $bp;
        $access = 0;
        if (function_exists('bp_is_my_profile'))
            $access = apply_filters('eLearning_student_marketing_access', bp_is_my_profile());

        bp_core_new_nav_item(array(
            'name' => __('Marketing', 'eLearning-marketing'),
            'slug' => ELEARNING_MARKETING_SLUG,
            'position' => 2,
            'screen_function' => 'eLearning_marketing_template',
            'show_for_displayed_user' => $access
        ));
    }

    function add_security_parameter() {
        wp_nonce_field('eLearning_security', 'security');
    }

    function add_marketing_in_menu($menu) {
        $dash_menu['marketing'] = array(
            'icon' => 'icon-meter',
            'label' => __('Marketing', 'eLearning-marketing'),
            'position' => 2,
            'link' => bp_loggedin_user_domain() . ELEARNING_MARKETING_SLUG
        );
        foreach ($menu as $key => $item) {
            $dash_menu[$key] = $item;
        }
        return $dash_menu;
    }




}

new ELEARNING_marketing();
?>
