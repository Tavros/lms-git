<?php

function eLearning_marketing_template() {

    if (!is_user_logged_in())
        wp_redirect(site_url());

    $template = 'templates/marketing';
global $bp;


    if ($bp->current_component == 'marketing') {

        $translation_array = array(
            'earnings' => __('Earnings', 'eLearning-marketing'),
            'payout' => __('Payout', 'eLearning-marketing'),
            'students' => __('# Students', 'eLearning-marketing'),
            'saved' => __('SAVED', 'eLearning-marketing'),
            'saving' => __('SAVING ...', 'eLearning-marketing'),
            'select_recipients' => __('Select recipients...', 'eLearning-marketing'),
            'stats_calculated' => __('Stats Calculated, reloading page ...', 'eLearning-marketing')
        );
        wp_localize_script('eLearning-marketing-js', 'eLearning_marketing_strings', $translation_array);
    }


    $located_template = apply_filters('bp_located_template', locate_template($template, false), $template);

    


        bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/marketing'));

}


function get_admin_marketing_content(){
    var_dump('get_admin_marketing_content');
}
