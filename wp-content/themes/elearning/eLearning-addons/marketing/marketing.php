<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_MARKETING_URL')) {
    define('ELEARNING_MARKETING_URL', get_template_directory_uri() . '/eLearning-addons/marketing/');
}

include_once 'includes/functions.php';
include_once 'includes/marketing.php';
add_action('after_setup_theme', 'load_marketing_textdomain');

function load_marketing_textdomain() {
    load_textdomain('eLearning-marketing', dirname(__FILE__) . '/languages/');
}

?>