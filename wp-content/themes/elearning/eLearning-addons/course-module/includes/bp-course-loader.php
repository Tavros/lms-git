<?php

// Exit if accessed directly
// It's a good idea to include this in each of your plugin files, for increased security on
// improperly configured servers
if (!defined('ABSPATH'))
    exit;

/*
 * If you want the users of your component to be able to change the values of your other custom constants,
 * you can use this code to allow them to add new definitions to the wp-config.php file and set the value there.
 *
 *
 * 	if ( !defined( 'BP_course_CONSTANT' ) )
 * 		define ( 'BP_course_CONSTANT', 'some value' // or some value without quotes if integer );
 */

/**
 * You should try hard to support translation in your component. It's actually very easy.
 * Make sure you wrap any rendered text in __() or _e() and it will then be translatable.
 *
 * You must also provide a text domain, so translation files know which bits of text to translate.
 * Throughout this course the text domain used is 'bp-course', you can use whatever you want.
 * Put the text domain as the second parameter:
 *
 * __( 'This text will be translatable', 'eLearning-course-module' ); // Returns the first parameter value
 * _e( 'This text will be translatable', 'eLearning-course-module' ); // Echos the first parameter value
 */

/**
 * Implementation of BP_Component
 *
 * BP_Component is the base class that all BuddyPress components use to set up their basic
 * structure, including global data, navigation elements, and admin bar information. If there's
 * a particular aspect of this class that is not relevant to your plugin, just leave it out.
 *
 * @package BuddyPress_Course_Component
 * @since 1.6
 */
class BP_Course_Component extends BP_Component {

    function __construct() {
        global $bp;
        parent::start(
                BP_COURSE_SLUG, __('Course', 'eLearning-course-module'), BP_COURSE_MOD_PLUGIN_DIR
        );

        if (!defined('BP_COURSE_RESULTS_SLUG'))
            define('BP_COURSE_RESULTS_SLUG', 'course-results');

        if (!defined('BP_COURSE_STATS_SLUG '))
            define('BP_COURSE_STATS_SLUG', 'course-stats');

        if (!defined('BP_EVENT_RESULTS_SLUG'))
            define('BP_EVENT_RESULTS_SLUG', 'event-results');

        if (!defined('BP_EVENT_STATS_SLUG '))
            define('BP_EVENT_STATS_SLUG', 'event-stats');
       if (!defined('BP_EVENT_INSTRUCTOR_SLUG '))
            define('BP_EVENT_INSTRUCTOR_SLUG', 'instructor-events');
        
        

        /**
         * BuddyPress-dependent plugins are loaded too late to depend on BP_Component's
         * hooks, so we must call the function directly.
         */
        $this->includes();

        /**
         * Put your component into the active components array, so that
         *   bp_is_active( 'course' );
         * returns true when appropriate. We have to do this manually, because non-core
         * components are not saved as active components in the database.
         */
        $bp->active_components[$this->id] = '1';
    }

    function includes($includes = array()) {

        // Files to include
        $includes = array(
            'includes/bp-course-actions.php',
            'includes/bp-course-screens.php',
            'includes/bp-course-filters.php',
            'includes/bp-event-filters.php',
            'includes/bp-course-classes.php',
            'includes/bp-course-activity.php',
            'includes/bp-course-template.php',
            'includes/bp-course-functions.php',
            'includes/bp-course-notifications.php',
            'includes/bp-course-widgets.php',
            'includes/bp-course-cssjs.php',
            'includes/bp-course-ajax.php',
            'includes/bp-course-offline.php',
            'includes/bp-course-mailer.php',
            'includes/bp-course-scheduler.php'
        );

        parent::includes($includes);

        // As an course of how you might do it manually, let's include the functions used
        // on the WordPress Dashboard conditionally:
        if (is_admin() || is_network_admin()) {
            include( BP_COURSE_MOD_PLUGIN_DIR . '/includes/bp-course-admin.php' );
        }
    }

    public function setup_globals($args = array()) {
        global $bp;

        // Defining the slug in this way makes it possible for site admins to override it
        if (!defined('BP_COURSE_SLUG'))
            define('BP_COURSE_SLUG', $this->id);

        if (!defined('BP_EVENT_SLUG'))
            define('BP_EVENT_SLUG', 'events');

        if (!defined('BP_COURSE_INSTRUCTOR_SLUG'))
            define('BP_COURSE_INSTRUCTOR_SLUG', 'instructor-courses');


        $globals = array(
            'slug' => BP_COURSE_SLUG,
            'root_slug' => isset($bp->pages->{$this->id}->slug) ? $bp->pages->{$this->id}->slug : BP_COURSE_SLUG,
            'has_directory' => true, // Set to false if not required
            'directory_title' => _x('Course Directory', 'Course directory title', 'eLearning-course-module'),
            'notification_callback' => 'bp_course_format_notifications',
            'search_string' => __('Search ...', 'eLearning-course-module'),
                //'global_tables'         => $global_tables
        );

        parent::setup_globals($globals);
    }

    function setup_nav($main_nav = array(), $sub_nav = array()) {

        $show_for_displayed_user = apply_filters('eLearning_user_profile_courses', false);
        $main_nav = array(
            'name' => sprintf(__('Courses <span>%s</span>', 'eLearning-course-module'), bp_course_get_total_course_count_for_user()),
            'slug' => BP_COURSE_SLUG,
            'position' => 5,
            'screen_function' => 'bp_course_my_courses',
            'show_for_displayed_user' => $show_for_displayed_user, //Change for admin
            'default_subnav_slug' => BP_COURSE_SLUG,
        );
        if (function_exists('eLearning_get_option')) {
            $course_view = eLearning_get_option('course_view');
            if (isset($course_view) && $course_view) {
                $main_nav['show_for_displayed_user'] = $show_for_displayed_user; //Change for admin
            }
        }

        $course_link = trailingslashit(bp_loggedin_user_domain() . BP_COURSE_SLUG);
        $events_link = trailingslashit(bp_loggedin_user_domain() . BP_EVENT_SLUG);


        // Determine user to use
        if (bp_displayed_user_domain()) {
            $user_domain = bp_displayed_user_domain();
        } elseif (bp_loggedin_user_domain()) {
            $user_domain = bp_loggedin_user_domain();
        } else {
            $user_domain = false;
        }


        if (!empty($user_domain)) {
            $user_access = bp_is_my_profile();
            $user_access = apply_filters('eLearning_user_profile_courses', $user_access);
            $sub_nav[] = array(
                'name' => __('My Courses', 'eLearning-course-module'),
                'slug' => BP_COURSE_SLUG,
                'parent_url' => $course_link,
                'parent_slug' => BP_COURSE_SLUG,
                'screen_function' => 'bp_course_my_courses',
                'user_has_access' => $user_access,
                'position' => 10
            );

            $sub_nav[] = array(
                'name' => __('My Events', 'eLearning-course-module'),
                'slug' => BP_EVENT_SLUG,
                'parent_url' => bp_loggedin_user_domain(),
                'parent_slug' => BP_EVENT_SLUG,
                'screen_function' => 'bp_events_my_events',
                'user_has_access' => $user_access,
                'position' => 10
            );

            bp_core_new_subnav_item(array(
                'name' => __('State', 'eLearning-course-module'),
                'slug' => BP_EVENT_STATS_SLUG,
                'parent_slug' => BP_EVENT_SLUG,
                'parent_url' => $events_link,
                'screen_function' => 'bp_events_my_events',
                'position' => 30,
                'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
            ));

            bp_core_new_subnav_item(array(
                'name' => __('Result', 'eLearning-course-module'),
                'slug' => BP_EVENT_RESULTS_SLUG,
                'parent_slug' => BP_EVENT_SLUG,
                'parent_url' => $events_link,
                'screen_function' => 'bp_events_my_events',
                'position' => 30,
                'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
            ));


            bp_core_new_subnav_item(array(
                'name' => __('Results', 'eLearning-course-module'),
                'slug' => BP_COURSE_RESULTS_SLUG,
                'parent_slug' => BP_COURSE_SLUG,
                'parent_url' => $course_link,
                'screen_function' => 'bp_course_my_results',
                'position' => 30,
                'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
            ));

            bp_core_new_subnav_item(array(
                'name' => __('Stats', 'eLearning-course-module'),
                'slug' => BP_COURSE_STATS_SLUG,
                'parent_slug' => BP_COURSE_SLUG,
                'parent_url' => $course_link,
                'screen_function' => 'bp_course_stats',
                'position' => 40,
                'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
            ));

            $sub_nav[] = array(
                'name' => __('Instructing Courses', 'eLearning-course-module'),
                'slug' => BP_COURSE_INSTRUCTOR_SLUG,
                'parent_slug' => BP_COURSE_SLUG,
                'parent_url' => $course_link,
                'screen_function' => 'bp_course_instructor_courses',
                'position' => 50,
                'user_has_access' => bp_is_my_profile_intructor(),
            );

            $sub_nav[] = array(
                'name' => __('Instructing Events', 'eLearning-course-module'),
                'slug' => BP_EVENT_INSTRUCTOR_SLUG,
                'parent_slug' => BP_EVENT_SLUG,
                'parent_url' => $events_link,
                'screen_function' => 'bp_events_my_events',
                'position' => 50,
                'user_has_access' => bp_is_my_profile_intructor(),
            );

            parent::setup_nav($main_nav, $sub_nav);
        }




        // If your component needs additional navigation menus that are not handled by

        if (bp_is_course_component() && bp_is_single_item()) {


            // Reset sub nav
            $sub_nav = array();

            // Add 'courses' to the main navigation
            $main_nav = array(
                'name' => __('Home', 'eLearning-course-module'),
                'slug' => get_current_course_slug(),
                'position' => -1, // Do not show in BuddyBar
                'screen_function' => 'bp_screen_course_home',
                'default_subnav_slug' => $this->default_extension,
                'item_css_id' => $this->id
            );


            parent::setup_nav($main_nav, $sub_nav);
        }

        if (isset($this->current_course->user_has_access)) {
            do_action('courses_setup_nav', $this->current_course->user_has_access);
        } else {
            do_action('courses_setup_nav');
        }
    }

}

global $bp;
$bp->course = new BP_Course_Component;


add_action('admin_notices', 'eLearning_check_plugin_notice');

function eLearning_check_plugin_notice() {

    if (!defined('THEME_SHORT_NAME'))
        return;

    $eLearning = wp_get_theme();

    $value = $eLearning->get('Version');

    $parent = $eLearning->parent();

    if (!empty($parent) && defined('THEME_SHORT_NAME') && $eLearning->name != 'ELEARNING Blank ChildTheme') {

        if (in_array($eLearning->name, array('ELEARNING Modern', 'ELEARNING Instructor', 'ELEARNING one Page', 'ELEARNING ChildTheme I'))) {
            $eLearning_parent = wp_get_theme(THEME_SHORT_NAME);
            $val = $eLearning_parent->get('Version');
            if (version_compare($val, "2.6") < 0) {
                echo '<div class="error">
				    <p>' . sprintf(__('Please Update the %s theme to latest version %s', 'eLearning-course-module'), $eLearning_parent->name, '2.6') . '</p>
				  </div>';
            }

            if (version_compare($value, "2.6") < 0) {
                echo '<div class="error">
				    <p>' . sprintf(__('Please Update the %s theme to latest version %s', 'eLearning-course-module'), $eLearning->name, '2.6') . '</p>
				  </div>';
            }
        }
    } else {
        $eLearning = wp_get_theme(THEME_SHORT_NAME);
        $value = $eLearning->get('Version');
        if (version_compare($value, "2.6") < 0) {
            echo '<div class="error notice is-dismissible">
			    <p>' . sprintf(__('Please Update the %s theme to latest version %s', 'eLearning-course-module'), $eLearning->name, '2.6') . '</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
			  </div>';
        }
    }
}
