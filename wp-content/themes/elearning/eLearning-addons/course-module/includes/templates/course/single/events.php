<?php
/**
 * The template for displaying Course events
 *
 * Override this template by copying it to yourtheme/course/single/events.php
 *
 * @author 		ELearningThemes
 * @package 	eLearning-course-module/templates
 * @version     2.1
 */


global $post;
if(class_exists('ELEARNING_Events_Interface')){
?>

<div class="course_title">
	<h1><?php the_title(); _e(' Events','eLearning-course-module')  ?></h1>
</div>
<?php
	    $events_interface = new ELEARNING_Events_Interface;
		$events_interface->eLearning_event_calendar(get_the_ID());
		
 }
?>