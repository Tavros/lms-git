<?php


// Checks if Course Module is Installed
define( 'BP_COURSE_MOD_INSTALLED', 1 );

// Checks the Course Module Version and necessary changes are hooked to this component
define( 'BP_COURSE_MOD_VERSION', '2.6' );

// FILE PATHS of Course Module
define( 'BP_COURSE_MOD_PLUGIN_DIR', dirname( __FILE__ ) );

/* Database Version for Course Module */
define ( 'BP_COURSE_DB_VERSION', '2.6' );

define ( 'BP_COURSE_CPT', 'course' );

if ( ! defined( 'BP_COURSE_SLUG' ) ){
    define ( 'BP_COURSE_SLUG','course' );
}

require('includes/bp-course-loader.php' );

function bp_course_version(){
    return '2.6';
}

add_action('after_setup_theme', 'load_eLearning_course_module_textdomain');

function load_eLearning_course_module_textdomain() {
    load_textdomain('eLearning-course-module', dirname(__FILE__) . '/languages/');
}
