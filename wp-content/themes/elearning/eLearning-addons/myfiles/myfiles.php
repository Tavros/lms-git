<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_MYFILES_URL')) {
    define('ELEARNING_MYFILES_URL', get_template_directory_uri() . '/eLearning-addons/myfiles/');
}

include_once 'includes/functions.php';
include_once 'includes/myfiles.php';
add_action('after_setup_theme', 'load_myfiles_textdomain');

function load_myfiles_textdomain() {
    load_textdomain('eLearning-myfiles', dirname(__FILE__) . '/languages/');
}

?>
