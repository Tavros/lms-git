<?php

if (!defined('ABSPATH'))
    exit;
if (!defined('ELEARNING_ACCOUNT_SLUG')) {
    define('ELEARNING_ACCOUNT_SLUG', 'account');
    define('ELEARNING_ACCOUNT_GENETRAL_SLUG', 'account-general');
    define('ELEARNING_ACCOUNT_PROFILE_SLUG', 'account-profile');
    define('ELEARNING_ACCOUNT_PRIVACY_SLUG', 'account-privacy');
    define('ELEARNING_ACCOUNT_NOTIFICATIONS_SLUG', 'account-notifications');
    define('ELEARNING_ACCOUNT_BILLING_SLUG', 'account-billing');
}


include_once 'includes/function.php';
include_once 'includes/general.php';


function bp_account_nav_item() {
    global $bp;
    $account_link = trailingslashit(bp_loggedin_user_domain() . ELEARNING_ACCOUNT_SLUG);
    $user_access = bp_is_my_profile();
    $args = array(
        'name' => __('Account', 'eLearning'),
        'slug' => 'account',
        'default_subnav_slug' => ELEARNING_ACCOUNT_SLUG,
        'position' => 200,
        'show_for_displayed_user' => false,
        'screen_function' => 'bp_account_user_nav_item_screen',
        'item_css_id' => 'account'
    );


    bp_core_new_nav_item($args);
    bp_core_new_subnav_item(array(
        'name' => __('General', 'eLearning'),
        'slug' => ELEARNING_ACCOUNT_GENETRAL_SLUG,
        'parent_slug' => ELEARNING_ACCOUNT_SLUG,
        'parent_url' => $account_link,
        'screen_function' => 'bp_account_results',
        'position' => 30,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Profile', 'eLearning'),
        'slug' => ELEARNING_ACCOUNT_PROFILE_SLUG,
        'parent_slug' => ELEARNING_ACCOUNT_SLUG,
        'parent_url' => $account_link,
        'screen_function' => 'bp_account_results',
        'position' => 40,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Privacy', 'eLearning'),
        'slug' => ELEARNING_ACCOUNT_PRIVACY_SLUG,
        'parent_slug' => ELEARNING_ACCOUNT_SLUG,
        'parent_url' => $account_link,
        'screen_function' => 'bp_account_results',
        'position' => 50,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Notifications', 'eLearning'),
        'slug' => ELEARNING_ACCOUNT_NOTIFICATIONS_SLUG,
        'parent_slug' => ELEARNING_ACCOUNT_SLUG,
        'parent_url' => $account_link,
        'screen_function' => 'bp_account_results',
        'position' => 60,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
    bp_core_new_subnav_item(array(
        'name' => __('Billing', 'eLearning'),
        'slug' => ELEARNING_ACCOUNT_BILLING_SLUG,
        'parent_slug' => ELEARNING_ACCOUNT_SLUG,
        'parent_url' => $account_link,
        'screen_function' => 'bp_account_results',
        'position' => 70,
        'user_has_access' => $user_access // Only the logged in user can access this on his/her profile
    ));
}

add_action('bp_setup_nav', 'bp_account_nav_item', 99);

function bp_account_user_nav_item_screen() {
    add_action('bp_template_content', 'bp_custom_screen_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/account'));
}

function bp_account_results() {
    add_action('bp_template_content', 'bp_custom_screen_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/account'));
}

function bp_custom_screen_content() {

//    echo 'the custom content.
//    You can put a post loop here,
//    pass $user_id with bp_displayed_user_id()';
}

add_action('bp_ready', 'save_peyotto_form_data');
?>
