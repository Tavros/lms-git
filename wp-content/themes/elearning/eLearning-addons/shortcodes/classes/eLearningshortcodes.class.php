<?php

class ELearningShortcodes {

    function __construct() {

        define('ELEARNING_TINYMCE_URI', ELEARNING_PLUGIN_URL . '/shortcodes/tinymce');

        $enable_shortcodes = apply_filters('wplms_eLearning_shortcodes', 1);

        if ($enable_shortcodes) {
            if (function_exists('eLearning_get_option')) {
                $this->create_course = eLearning_get_option('create_course');
                if (!empty($this->create_course)) {
                    add_action('wp_enqueue_scripts', array($this, 'shortcodes_front_end'));
                }
            }


            add_action('init', array($this, 'init'));
            add_action('wp_enqueue_scripts', array($this, 'frontend'));
        }

        add_action('admin_enqueue_scripts', array($this, 'admin_icons'), 10, 1);
        add_action('admin_enqueue_scripts', array($this, 'admin_init'), 10, 1);
    }

    /**
     * Registers TinyMCE rich editor buttons
     *
     * @return	void
     */
    function init() {
        if (!current_user_can('edit_posts') && !current_user_can('edit_pages'))
            return;

        if (get_user_option('rich_editing') == 'true') {
            add_filter('mce_external_plugins', array($this, 'add_rich_plugins'));
            add_filter('mce_buttons', array($this, 'register_rich_buttons'));
        }
    }

    function frontend() {

        wp_enqueue_script('shortcodes-js', ELEARNING_PLUGIN_URL . '/shortcodes/js/shortcodes.js', array('jquery', 'mediaelement', 'thickbox'), '2.6', true);
        $translation_array = array(
            'sending_mail' => __('Sending mail', 'eLearning-shortcodes'),
            'error_string' => __('Error :', 'eLearning-shortcodes'),
            'invalid_string' => __('Invalid ', 'eLearning-shortcodes'),
            'captcha_mismatch' => __('Captcha Mismatch', 'eLearning-shortcodes'),
        );
        wp_localize_script('shortcodes-js', 'eLearning_shortcode_strings', $translation_array);
    }

    // --------------------------------------------------------------------------

    /**
     * Defins TinyMCE rich editor js plugin
     *
     * @return	void
     */
    function add_rich_plugins($plugin_array) {
//		if ( floatval(get_bloginfo('version')) >= 3.9){
        $plugin_array['eLearningShortcodes'] = ELEARNING_TINYMCE_URI . '/plugin.js';
//		}else{
//			$plugin_array['eLearningShortcodes'] = ELEARNING_TINYMCE_URI . '/plugin.old.js'; // For old versions of WP
//		}

        return $plugin_array;
    }

    // --------------------------------------------------------------------------

    /**
     * Adds TinyMCE rich editor buttons
     *
     * @return	void
     */
    function register_rich_buttons($buttons) {
        array_push($buttons, "|", 'eLearning_button');
        return $buttons;
    }

    /**
     * Enqueue Scripts and Styles
     *
     * @return	void
     */
    function admin_init($hook) {
        if ((is_admin() && in_array($hook, array('post-new.php', 'post.php', 'toplevel_page_wplms_options')))) {
            wp_enqueue_script('jquery-ui-sortable');
            wp_enqueue_script('jquery-livequery', ELEARNING_TINYMCE_URI . '/js/jquery.livequery.js', false, '1.1.1', false);
            wp_enqueue_script('jquery-appendo', ELEARNING_TINYMCE_URI . '/js/jquery.appendo.js', false, '1.0', false);
            wp_enqueue_script('base64', ELEARNING_TINYMCE_URI . '/js/base64.js', false, '1.0', false);
            wp_localize_script('jquery', 'ELearningShortcodes', array('shortcodes_folder' => ELEARNING_PLUGIN_URL . '/shortcodes'));
            if (floatval(get_bloginfo('version')) >= 3.9) {
                wp_enqueue_script('eLearning-popup', ELEARNING_TINYMCE_URI . '/js/popup.js', array('jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-mouse', 'jquery-ui-draggable', 'jquery-ui-slider', 'iris'), '1.0', false);
            } else {
                wp_enqueue_script('eLearning-popup', ELEARNING_TINYMCE_URI . '/js/popup.old.js', array('jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-mouse', 'jquery-ui-draggable', 'jquery-ui-slider', 'iris'), '1.0', false);
            }
        }
        if (is_admin()) {
            wp_enqueue_style('eLearning-popup-css', ELEARNING_TINYMCE_URI . '/css/popup.css', false, '1.0', 'all');
        }

        $this->localise_shortcode_dropdown();
    }

    function admin_icons($hook) {

        if (is_admin() && ( in_array($hook, array('post-new.php', 'post.php')) || ($hook == 'lms_page_lms-settings' && $_GET['page'] == 'lms-settings' && $_GET['tab'] == 'general' && $_GET['sub'] == 'profile_menu_dropdown_settings') )) {
            wp_enqueue_style('icons-css', ELEARNING_PLUGIN_URL . '/shortcodes/css/fonticons.css');
        }
    }

    function shortcodes_front_end() {
        if (!current_user_can('edit_posts'))
            return;

        wp_enqueue_script('jquery-ui-draggable');
        wp_enqueue_script('jquery-ui-slider');
        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('iris', admin_url('js/iris.min.js'), array('jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch'), false, 1);
        wp_enqueue_script('jquery-livequery', ELEARNING_TINYMCE_URI . '/js/jquery.livequery.js', false, '1.1.1', true);
        wp_enqueue_script('jquery-appendo', ELEARNING_TINYMCE_URI . '/js/jquery.appendo.js', false, '1.0', true);
        wp_enqueue_script('base64', ELEARNING_TINYMCE_URI . '/js/base64.js', false, '1.0', true);


        if (floatval(get_bloginfo('version')) >= 3.9) {
            wp_enqueue_script('eLearning-popup', ELEARNING_TINYMCE_URI . '/js/popup.js', array(), '1.0', true);
        } else {
            wp_enqueue_script('eLearning-popup', ELEARNING_TINYMCE_URI . '/js/popup.old.js', array('jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-mouse', 'jquery-ui-draggable', 'jquery-ui-slider', 'iris'), '1.0', true);
        }

        wp_localize_script('jquery', 'ELearningShortcodes', array('shortcodes_folder' => ELEARNING_PLUGIN_URL . '/shortcodes'));
        wp_enqueue_style('eLearning-popup-css', ELEARNING_TINYMCE_URI . '/css/popup.css', false, '1.0', 'all');

        $this->localise_shortcode_dropdown();
    }

    function localise_shortcode_dropdown() {
        $translation_array = array(
            'accordion' => __('Accordion', 'eLearning-shortcodes'),
            'buttons' => __('Buttons', 'eLearning-shortcodes'),
            'columns' => __('Columns', 'eLearning-shortcodes'),
            'counter' => __('Counter', 'eLearning-shortcodes'),
            'course' => __('Course', 'eLearning-shortcodes'),
            'divider' => __('Divider', 'eLearning-shortcodes'),
            'forms' => __('Forms', 'eLearning-shortcodes'),
            'gallery' => __('Gallery', 'eLearning-shortcodes'),
            'google_maps' => __('Google Maps', 'eLearning-shortcodes'),
            'heading' => __('Heading', 'eLearning-shortcodes'),
            'icons' => __('Icons', 'eLearning-shortcodes'),
            'iframe' => __('Iframe', 'eLearning-shortcodes'),
            'note' => __('Note', 'eLearning-shortcodes'),
            'popups' => __('Popups', 'eLearning-shortcodes'),
            'progress_bar' => __('Progress Bar', 'eLearning-shortcodes'),
            'pull_quote' => __('PullQuote', 'eLearning-shortcodes'),
            'round_progress' => __('Round Progress', 'eLearning-shortcodes'),
            'survey_result' => __('Survey Result', 'eLearning-shortcodes'),
            'tabs' => __('Tabs', 'eLearning-shortcodes'),
            'team' => __('Team', 'eLearning-shortcodes'),
            'testimonial' => __('Testimonial', 'eLearning-shortcodes'),
            'tooltips' => __('Tooltips', 'eLearning-shortcodes'),
            'video' => __('Video', 'eLearning-shortcodes'),
        );
        wp_localize_script('eLearning-popup', 'eLearning_shortcode_icon_strings', $translation_array);
    }

}

$eLearning_shortcodes = new ELearningShortcodes();
?>