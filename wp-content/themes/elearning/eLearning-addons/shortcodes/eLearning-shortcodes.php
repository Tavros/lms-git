<?php

/*
Plugin Name: ELearning ShortCodes
Plugin URI: http://www.eLearningthemes.com
Description: Create unlimited shortcodes
Author: ELearningThemes
Version: 2.6
Author URI: http://www.eLearningthemes.com
Text Domain: eLearning-shortcodes
Domain Path: /languages/
*/


 if ( ! defined( 'ABSPATH' ) ) exit;
if( !defined('ELEARNING_PLUGIN_URL')){
    define('ELEARNING_PLUGIN_URL',get_template_directory_uri().'/eLearning-addons');
}

/*====== BEGIN VSLIDER======*/

include_once('classes/eLearningshortcodes.class.php');
include_once('shortcodes.php');
include_once('ajaxcalls.php');
include_once('upload_handler.php');

