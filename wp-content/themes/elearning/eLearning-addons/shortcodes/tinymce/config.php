<?php

/*-----------------------------------------------------------------------------------*/
/*	Accordion Config
/*-----------------------------------------------------------------------------------*/
$r = rand(0,999);
$eLearning_shortcodes['accordion'] = array(
    'params' => array(),
    'no_preview' => true,
    'params' => array(
        'open_first' => array(
			'type' => 'select',
			'label' => __('Open first', 'eLearning-shortcodes'),
			'desc' => __('First accordion will be open by default', 'eLearning-shortcodes'),
			'options' => array(
				0 => __('No','eLearning-shortcodes'),
				1 => __('Yes','eLearning-shortcodes'),
			)
		),
    ),
    'shortcode' => '[agroup first="{{open_first}}" connect="'.$r.'"] {{child_shortcode}}  [/agroup]',
    'popup_title' => __('Insert Accordion Shortcode', 'eLearning-shortcodes'),
    'child_shortcode' => array(
        'params' => array(
            'title' => array(
			'type' => 'text',
			'label' => __('Accordion Title 1', 'eLearning-shortcodes'),
			'desc' => __('Add the title of the accordion', 'eLearning-shortcodes'),
			'std' => 'Title'
		),
		'content' => array(
			'std' => 'Content',
			'type' => 'textarea',
			'label' => __('Accordion Content', 'eLearning-shortcodes'),
			'desc' => __('Add the content. Accepts HTML & other Shortcodes.', 'eLearning-shortcodes'),
		),
              ),
        'shortcode' => '[accordion title="{{title}}" connect="'.$r.'"] {{content}} [/accordion]',
        'clone_button' => __('Add Accordion Toggle', 'eLearning-shortcodes')
    )
);

/*-----------------------------------------------------------------------------------*/
/*	Button Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['button'] = array(
	'no_preview' => false,
	'params' => array(
		'url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Link URL', 'eLearning-shortcodes'),
			'desc' => __('Add the button\'s url eg http://www.example.com', 'eLearning-shortcodes')
		),
        'class' => array(
			'std' => '',
			'type' => 'select_hide',
			'label' => __('Button Style', 'eLearning-shortcodes'),
			'desc' => __('Select button style', 'eLearning-shortcodes'),
                        'options' => array(
				'' => 'Base',
				'primary' => 'Primary',
				'blue' => 'Blue',
				'green' => 'Green',
                'other' => 'Custom',
			),
            'level' => 7
		),
		'bg' => array(
			'type' => 'color',
			'label' => __('Background color', 'eLearning-shortcodes'),
			'desc' => __('Select the button\'s size', 'eLearning-shortcodes')
		),
                'hover_bg' => array(
			'type' => 'color',
			'label' => __('Hover Bg color', 'eLearning-shortcodes'),
			'desc' => __('Select the button\'s on hover background color ', 'eLearning-shortcodes')
		),
                'color' => array(
			'type' => 'color',
			'label' => __('Text color', 'eLearning-shortcodes'),
			'desc' => __('Select the button\'s text color', 'eLearning-shortcodes')
		),
                'size' => array(
			'type' => 'slide',
			'label' => __('Font Size', 'eLearning-shortcodes'),
                        'min' => 0,
                        'max' => 100,
                        'std' => 0,
		),
		'width' => array(
			'type' => 'slide',
			'label' => __('Width', 'eLearning-shortcodes'),
                        'min' => 0,
                        'max' => 500,
                        'std' => 0,
		),
                'height' => array(
			'type' => 'slide',
			'label' => __('Height', 'eLearning-shortcodes'),
                        'min' => 0,
                        'max' => 100,
                        'std' => 0,
		),
		'radius' => array(
			'type' => 'slide',
			'label' => __('Border Radius', 'eLearning-shortcodes'),
                        'min' => 0,
                        'max' => 150,
                        'std' => 0
		),
		'target' => array(
			'type' => 'select',
			'label' => __('Button Target', 'eLearning-shortcodes'),
			'desc' => __('_self = open in same window. _blank = open in new window', 'eLearning-shortcodes'),
			'options' => array(
				'_self' => '_self',
				'_blank' => '_blank'
			)
		),
            'content' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button Anchor', 'eLearning-shortcodes'),
			'desc' => __('Replace button label with the text you enter.', 'eLearning-shortcodes'),
		)
	),
	'shortcode' => '[button url="{{url}}" class="{{class}}" bg="{{bg}}" hover_bg="{{hover_bg}}" size="{{size}}" color="{{color}}" radius="{{radius}}" width="{{width}}"  height="{{height}}"  target="{{target}}"] {{content}} [/button]',
	'popup_title' => __('Insert Button Shortcode', 'eLearning-shortcodes')
);


/*-----------------------------------------------------------------------------------*/
/*	Columns Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['columns'] = array(
	'params' => array(),
	'shortcode' => ' {{child_shortcode}} ', // as there is no wrapper shortcode
	'popup_title' => __('Insert Columns Shortcode', 'eLearning-shortcodes'),
	'no_preview' => true,
	
	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'column' => array(
				'type' => 'select',
				'label' => __('Column Type', 'eLearning-shortcodes'),
				'desc' => __('Select the type, ie width of the column.', 'eLearning-shortcodes'),
				'options' => array(
                    'one_fifth' => 'One Fifth',
                    'one_fourth' => 'One Fourth',
					'one_third' => 'One Third',
                    'two_fifth' => 'Two Fifth',
					'one_half' => 'One Half',
                    'three_fifth' => 'Three Fifth',
                    'two_third' => 'Two Thirds',
					'three_fourth' => 'Three Fourth',
                    'four_fifth' => 'Four Fifth',
				)
			),
                        'first' => array(
				'type' => 'select',
				'label' => __('Column Type', 'eLearning-shortcodes'),
				'desc' => __('Select the type, ie width of the column.', 'eLearning-shortcodes'),
				'options' => array(
                                        '' => 'Default',
                                        'first' => 'First in Row (from Left)',
				)
			),
			'content' => array(
				'std' => '',
				'type' => 'textarea',
				'label' => __('Column Content', 'eLearning-shortcodes'),
				'desc' => __('Add the column content.', 'eLearning-shortcodes'),
			)
		),
		'shortcode' => '[{{column}} first={{first}}] {{content}} [/{{column}}] ',
		'clone_button' => __('Add Column', 'eLearning-shortcodes')
	)
);

/*-----------------------------------------------------------------------------------*/
/*	Counter Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['counter'] = array(
	'no_preview' => true,
	'params' => array(
        'min' => array(
			'std' => 0,
			'type' => 'number',
			'label' => __('Start value of counter', 'eLearning-shortcodes'),
			'desc' => __('Add a starting number', 'eLearning-shortcodes'),
		),
		'max' => array(
			'std' => 100,
			'type' => 'number',
			'label' => __('Maximum value of counter', 'eLearning-shortcodes'),
			'desc' => __('Add the Tooltip text', 'eLearning-shortcodes'),
		),
		'delay' => array(
			'std' => 3,
			'type' => 'number',
			'label' => __('Total delay in finishing counter', 'eLearning-shortcodes'),
			'desc' => __('Add the total duration of counter increment', 'eLearning-shortcodes'),
		),
		'increment' => array(
			'std' => 1,
			'type' => 'number',
			'label' => __('Increment unit', 'eLearning-shortcodes'),
			'desc' => __('Increment the counter by this value', 'eLearning-shortcodes'),
		),
	),
	'shortcode' => '[number_counter min="{{min}}" max="{{max}}" delay="{{delay}}" increment="{{increment}}"]',
	'popup_title' => __('Insert Counter Shortcode', 'eLearning-shortcodes')
);


/*-----------------------------------------------------------------------------------*/
/*	Icon Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['icons'] = array(
	'no_preview' => true,
	'params' => array(
		'icon' => array(
					'type' => 'icon',
					'label' => __('Icon type', 'eLearning-shortcodes'),
					'desc' => __('Select Icon type', 'eLearning-shortcodes'),
					
                 ),
                 'size' => array(
					'type' => 'slide',
					'label' => __('Icon Size', 'eLearning-shortcodes'),
					'desc' => __('Icon Size', 'eLearning-shortcodes'),
					'min' => 0,
                    'max' => 100,
                    'std' => 0,
                 ),
                 
                 'class' => array(
					'std' => '',
					'type' => 'select_hide',
					'label' => __('Custom Style', 'eLearning-shortcodes'),
					'desc' => __('icon style', 'eLearning-shortcodes'),
                    'options' => array(
								'' => 'Text Style',
                                'other' => 'Custom',
					),
		            'level' => 6
				),
                 'color' => array(
					'type' => 'color',
					'label' => __('Icon Color', 'eLearning-shortcodes'),
					'desc' => __('Icon Color', 'eLearning-shortcodes')
                 )
                 ,
                 'bg' => array(
					'type' => 'color',
					'label' => __('Icon Bg Color', 'eLearning-shortcodes'),
					'desc' => __('Icon Background color', 'eLearning-shortcodes'),
                 ),
                 'hovercolor' => array(
					'type' => 'color',
					'label' => __('Icon Hover Color', 'eLearning-shortcodes'),
					'desc' => __('Icon Color', 'eLearning-shortcodes'),
                 )
                 ,
                 'hoverbg' => array(
					'type' => 'color',
					'label' => __('Icon Hover Bg Color', 'eLearning-shortcodes'),
					'desc' => __('Icon Background color', 'eLearning-shortcodes'),
                 ),
                 'padding' => array(
					'type' => 'slide',
					'label' => __('Icon padding', 'eLearning-shortcodes'),
					'desc' => __('Icon Background padding', 'eLearning-shortcodes'),
					'min' => 0,
                                        'max' => 100,
                                        'std' => 0,
                 ),
                 'radius' => array(
					'type' => 'slide',
					'label' => __('Icon Bg Radius', 'eLearning-shortcodes'),
					'desc' => __('Icon Background radius', 'eLearning-shortcodes'),
					'min' => 0,
                                        'max' => 100,
                                        'std' => 0,
                 ),
                 
		
	),
	'shortcode' => '[icon icon="{{icon}}" size="{{size}}" color="{{color}}" bg="{{bg}}" hovercolor="{{hovercolor}}" hoverbg="{{hoverbg}}" padding="{{padding}}" radius="{{radius}}"]',
	'popup_title' => __('Insert Icon Shortcode', 'eLearning-shortcodes')
);


/*-----------------------------------------------------------------------------------*/
/*	Alert Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['alert'] = array(
	'no_preview' => true,
	'params' => array(
		'style' => array(
			'type' => 'select_hide',
			'label' => __('Alert Style', 'eLearning-shortcodes'),
			'desc' => __('Select the alert\'s style, ie the alert colour', 'eLearning-shortcodes'),
			'options' => array(
				'block' => 'Orange',
				'info' => 'Blue',
				'error' => 'Red',
				'success' => 'Green',
                                'other' => 'Custom'
			),
                        'level' => 3
		),
            'bg' => array(
					'type' => 'color',
					'label' => __('Alert Bg Color', 'eLearning-shortcodes'),
					'desc' => __('Background color', 'eLearning-shortcodes'),
                 ),
            'border' => array(
					'type' => 'color',
					'label' => __('Alert Border Color', 'eLearning-shortcodes'),
					'desc' => __('Border color', 'eLearning-shortcodes'),
                 ),
            'color' => array(
					'type' => 'color',
					'label' => __('Text Color', 'eLearning-shortcodes'),
					'desc' => __('Alert Text color', 'eLearning-shortcodes'),
                 ),
		'content' => array(
			'std' => 'Your Alert/Information Message!',
			'type' => 'textarea',
			'label' => __('Alert Text', 'eLearning-shortcodes'),
			'desc' => __('Add the alert\'s text', 'eLearning-shortcodes'),
		)
		
	),
	'shortcode' => '[alert style="{{style}}" bg="{{bg}}" border="{{border}}" color="{{color}}"] {{content}} [/alert]',
	'popup_title' => __('Insert Alert Shortcode', 'eLearning-shortcodes')
);

/*-----------------------------------------------------------------------------------*/
/*	Tooltip Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['tooltip'] = array(
	'no_preview' => true,
	'params' => array(
        'tip' => array(
			'std' => 'Tip content!',
			'type' => 'textarea',
			'label' => __('Tooltip Text', 'eLearning-shortcodes'),
			'desc' => __('Add the Tooltip text', 'eLearning-shortcodes'),
		),
		'content' => array(
			'std' => 'Tooltip',
			'type' => 'text',
			'label' => __('Tooltip Anchor', 'eLearning-shortcodes'),
			'desc' => __('Add the Tooltip anchor', 'eLearning-shortcodes'),
		),
		
	),
	'shortcode' => '[tooltip tip="{{tip}}"] {{content}} [/tooltip]',
	'popup_title' => __('Insert Tooltip Shortcode', 'eLearning-shortcodes')
);



/*-----------------------------------------------------------------------------------*/
/*	RoundProgressBar
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['roundprogress'] = array(
	'no_preview' => true,
	'params' => array(
		'percentage' => array(
			'type' => 'text',
			'label' => __('Percentage Cover', 'eLearning-shortcodes'),
			'desc' => __('Only number eg:20', 'eLearning-shortcodes'),
			'std' => '20'
		),
                'style' => array(
			'type' => 'select',
			'label' => __('Style', 'eLearning-shortcodes'),
			'desc' => __('Tron or Custom', 'eLearning-shortcodes'),
			'options' => array(
				'' => 'Tron',
				'other' => 'Custom'
			)
		),
                'radius' => array(
			'std' => '200',
			'type' => 'text',
			'label' => __('Circle Diameter', 'eLearning-shortcodes'),
			'desc' => __('In pixels eg: 100', 'eLearning-shortcodes'),
		),
                'thickness' => array(
			'std' => '20',
			'type' => 'text',
			'label' => __('Circle Thickness', 'eLearning-shortcodes'),
			'desc' => __('In percentage', 'eLearning-shortcodes'),
		),
                 'color' => array(
					'type' => 'color',
					'label' => __('Progress  Text Color', 'eLearning-shortcodes'),
					'desc' => __('Progress  Text color', 'eLearning-shortcodes'),
                 ),
                 'bg_color' => array(
					'type' => 'color',
					'label' => __('Progress Circle Color', 'eLearning-shortcodes'),
					'desc' => __('Progress Circle color', 'eLearning-shortcodes'),
                 ),
		'content' => array(
			'std' => '20%',
			'type' => 'text',
			'label' => __('Some Content', 'eLearning-shortcodes'),
			'desc' => __('like : 20% Skill, shortcodes/html allowed', 'eLearning-shortcodes'),
		),
		
	),
	'shortcode' => '[roundprogress style="{{style}}" color="{{color}}" bg_color="{{bg_color}}" percentage="{{percentage}}" radius="{{radius}}" thickness="{{thickness}}"] {{content}} [/roundprogress]',
	'popup_title' => __('Insert Round Progress Shortcode', 'eLearning-shortcodes')
);



/*-----------------------------------------------------------------------------------*/
/*	ProgressBar
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['progressbar'] = array(
	'no_preview' => true,
	'params' => array(
		'percentage' => array(
			'type' => 'text',
			'label' => __('Percentage Cover', 'eLearning-shortcodes'),
			'desc' => __('Only number eg:20', 'eLearning-shortcodes'),
			'std' => '20'
		),
		'content' => array(
			'std' => '20%',
			'type' => 'text',
			'label' => __('Some Content', 'eLearning-shortcodes'),
			'desc' => __('like : 20% Skill, shortcodes/html allowed', 'eLearning-shortcodes'),
		),
		'color' => array(
			'type' => 'select_hide',
			'label' => __('Color', 'eLearning-shortcodes'),
			'desc' => __('Select progressbar color', 'eLearning-shortcodes'),
			'options' => array(
				'' => 'Default',
                'other' => 'Custom',
			),
                        'level' => 2
		),

        'bar_color' => array(
			'type' => 'color',
			'label' => __('Bar Color', 'eLearning-shortcodes'),
			'desc' => __('Bar color', 'eLearning-shortcodes'),
         ),
        'bg' => array(
			'type' => 'color',
			'label' => __('Bar Background Color', 'eLearning-shortcodes'),
			'desc' => __('Bar Background color', 'eLearning-shortcodes'),
         ),
	),
	'shortcode' => '[progressbar color="{{color}}" percentage="{{percentage}}" bg={{bg}} bar_color={{bar_color}}] {{content}} [/progressbar]',
	'popup_title' => __('Insert Progressbar Shortcode', 'eLearning-shortcodes')
);


/*-----------------------------------------------------------------------------------*/
/*	Tabs Config
/*-----------------------------------------------------------------------------------*/
$r = rand(0,999);
$eLearning_shortcodes['tabs'] = array(
    'params' => array(),
    'no_preview' => true,
    'params' => array(
            'style' => array(
                'std' => '',
                'type' => 'select',
                'label' => __('Tabs Style', 'eLearning-shortcodes'),
                'desc' => __('select a style', 'eLearning-shortcodes'),
                'options' => array(
                    '' => 'Top Horizontal',
                    'tabs-left' => 'Left Vertical',
                    'tabs-right' => 'Right Vertical'
                )
            ),
            'theme' => array(
                'std' => '',
                'type' => 'select',
                'label' => __('Tabs theme', 'eLearning-shortcodes'),
                'desc' => __('select a theme', 'eLearning-shortcodes'),
                'options' => array(
                    '' => 'Light',
                    'dark' => 'Dark'
                )
            ),
        ),
    'shortcode' => '[tabs style="{{style}}" theme={{theme}} connect="'.$r.'"] {{child_shortcode}}  [/tabs]',
    'popup_title' => __('Insert Tab Shortcode', 'eLearning-shortcodes'),
    
    'child_shortcode' => array(
        'params' => array(
            'title' => array(
                'std' => 'Title',
                'type' => 'text',
                'label' => __('Tab Title', 'eLearning-shortcodes'),
                'desc' => __('Title of the tab', 'eLearning-shortcodes'),
            ),  
            'icon' => array(
            			'type' => 'icon',
            			'label' => __('Title Icon', 'eLearning-shortcodes'),
            			'desc' => __('Select Icon type', 'eLearning-shortcodes'),
            			),   
            'content' => array(
                'std' => 'Tab Content',
                'type' => 'textarea',
                'label' => __('Tab Content', 'eLearning-shortcodes'),
                'desc' => __('Add the tabs content', 'eLearning-shortcodes')
            )
        ),
        'shortcode' => '[tab title="{{title}}" icon="{{icon}}" connect="'.$r.'"] {{content}} [/tab]',
        'clone_button' => __('Add Tab', 'eLearning-shortcodes')
    )
);


/*-----------------------------------------------------------------------------------*/
/*	Note Config
/*-----------------------------------------------------------------------------------*/


$eLearning_shortcodes['note'] = array(
	'no_preview' => true,
	'params' => array(
            
		'style' => array(
				'std' => 'default',
				'type' => 'select_hide',
				'label' => __('Background Color', 'eLearning-shortcodes'),
				'desc' => __('Background color & theme of note', 'eLearning-shortcodes'),
                                'options' => array(
					'' => 'Default',
                                        'other' => 'Custom'
				),
                                'level' => 3
			),
                'bg' => array(
                        'label' => 'Background Color',
                        'desc'  => 'Background color',
                        'type'  => 'color'
                ),
                'border' => array(
                        'label' => 'Border Color',
                        'desc'  => 'border color',
                        'type'  => 'color'
                ),
                'color' => array(
                        'label' => 'Text Color',
                        'desc'  => 'text color',
                        'type'  => 'color'
                ),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Content', 'eLearning-shortcodes'),
			'desc' => __('Note Content, supports HTML/Shortcodes', 'eLearning-shortcodes'),
		)
		
	),
	'shortcode' => '[note style="{{style}}" bg="{{bg}}" border="{{border}}" bordercolor="{{bordercolor}}" color="{{color}}"] {{content}} [/note]',
	'popup_title' => __('Insert Note Shortcode', 'eLearning-shortcodes')
);


/*-----------------------------------------------------------------------------------*/
/*	DIVIDER Config
/*-----------------------------------------------------------------------------------*/


$eLearning_shortcodes['divider'] = array(
	'no_preview' => true,
	'params' => array(
		'style' => array(
				'std' => 'clear',
				'type' => 'text',
				'label' => __('Divider Class', 'eLearning-shortcodes'),
				'desc' => __('clear : To begin form new line. Change Size using : one_third,one_fourth,one_fifth,two_third. Use multiple styles space saperated', 'eLearning-shortcodes'),
			)
		
	),
	'shortcode' => '[divider style="{{style}}"]',
	'popup_title' => __('Insert Divider Shortcode', 'eLearning-shortcodes')
);

/*-----------------------------------------------------------------------------------*/
/*	Tagline Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['tagline'] = array(
	'no_preview' => true,
	'params' => array(
		'style' => array(
			'type' => 'select_hide',
			'label' => __('Tagline Style', 'eLearning-shortcodes'),
			'desc' => __('Select the Tagline style', 'eLearning-shortcodes'),
			'options' => array(
				'boxed' => 'Boxed',
				'tagfullwidth' => 'Fullwidth',
                                'other' => 'Custom Boxed'
			),
                    'level' => 4
                    ),
                'bg' => array(
                        'label' => 'Background Color',
                        'desc'  => 'Background color',
                        'type'  => 'color'
                ),
                'border' => array(
                        'label' => 'Overall Border Color',
                        'desc'  => 'border color',
                        'type'  => 'color'
                ),
                'bordercolor' => array(
                        'label' => 'Left Border Color',
                        'desc'  => 'Default color : Theme Primary color',
                        'type'  => 'color'
                ),
                'color' => array(
                        'label' => 'Text Color',
                        'desc'  => 'Default color : Theme text color',
                        'type'  => 'color'
                ),
		'content' => array(
			'std' => 'Tagline Supports HTML',
			'type' => 'textarea',
			'label' => __('Tagline', 'eLearning-shortcodes'),
			'desc' => __('Supports HTML content', 'eLearning-shortcodes'),
		)
		
	),
	'shortcode' => '[tagline style="{{style}}" bg="{{bg}}" border="{{border}}" bordercolor="{{bordercolor}}" color="{{color}}"] {{content}} [/tagline]',
	'popup_title' => __('Insert Tagline Shortcode', 'eLearning-shortcodes')
);



/*-----------------------------------------------------------------------------------*/
/*	Popupss Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['popups'] = array(
	'no_preview' => true,
	'params' => array(
                'id' => array(
                'std' =>'',
				'type' => 'text',
				'label' => __('Enter Popup ID', 'eLearning-shortcodes'),
			),  
                'classes' => array(
                                'type' => 'select',
                                'label' => __('Anchor Style', 'eLearning-shortcodes'),
                                'options' => array(
				    'default' => 'Default',
		                    'btn' =>  'Button',
		                    'btn primary' =>  'Primary Button',
                                        )
                                    ),    
                    'content' => array(
                        'std' =>'',
			'type' => 'textarea',
			'label' => __('Popup/Modal Anchor', 'eLearning-shortcodes'),
			'desc' => __('Supports HTML & Shortcodes', 'eLearning-shortcodes')
			),
		    'auto' => array(
                        'std' =>'',
			'type' => 'select',
			'label' => __('Show Popup on Page-load', 'eLearning-shortcodes'),
                        'options' => array(1 => 'Yes',0 => 'No')
			), 
		
	),
	'shortcode' => '[popup id="{{id}}" auto="{{auto}}" classes="{{classes}}"] {{content}} [/popup] ',
	'popup_title' => __('Insert Popups Shortcode', 'eLearning-shortcodes')
);

/*-----------------------------------------------------------------------------------*/
/*	Testimonials Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['testimonial'] = array(
	'no_preview' => true,
	'params' => array(
                'id' => array(
                'std' =>'',
				'type' => 'text',
				'label' => __('Enter Testimonial ID', 'eLearning-shortcodes'),
			),
             	'length' => array(
                'std' =>'100',
				'type' => 'text',
				'label' => __('Number of Characters to show', 'eLearning-shortcodes'),
                'desc' => __('If number of characters entered above is less than Testimonial Post length, Read more link will appear', 'eLearning-shortcodes'), 
			),
	),
	'shortcode' => '[testimonial id="{{id}}" length={{length}}]',
	'popup_title' => __('Insert Testimonial Shortcode', 'eLearning-shortcodes')
);

/*-----------------------------------------------------------------------------------*/
/*	COURSE Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['course'] = array(
	'no_preview' => true,
	'params' => array(
                'id' => array(
                'std' =>'',
				'type' => 'text',
				'label' => __('Enter Course ID', 'eLearning-shortcodes'),
			),
	),
	'shortcode' => '[course id="{{id}}"]',
	'popup_title' => __('Insert Course Shortcode', 'eLearning-shortcodes')
);

/*-----------------------------------------------------------------------------------*/
/*	PULLQUOTE Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['pullquote'] = array(
	'no_preview' => true,
	'params' => array(
                'style' => array(
                        'std' =>'',
			'type' => 'select',
			'label' => __('Select Side', 'eLearning-shortcodes'),
                        'options' => array(
                            'left' => 'LEFT',
                            'right' => 'RIGHT'
                        )
			),
            'content' => array(
					'type' => 'textarea',
					'label' => __('Content', 'eLearning-shortcodes'),	
                    ),
	),
	'shortcode' => '[pullquote style="{{style}}"]{{content}}[/pullquote]',
	'popup_title' => __('Insert PullQuote Shortcode', 'eLearning-shortcodes')
);


/*-----------------------------------------------------------------------------------*/
/*	TEAM MEMBER Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['team_member'] = array(
	'no_preview' => true,
	'params' => array(
                'pic' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Member Image', 'eLearning-shortcodes'),
			'desc' => __('Image url of team member', 'eLearning-shortcodes'),
		),
		'name' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Member Name', 'eLearning-shortcodes'),
			'desc' => __('Name of team member (HTML allowed)', 'eLearning-shortcodes'),
		),
        'designation' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Designation', 'eLearning-shortcodes'),
			'desc' => __('Designation of Team Member (HTML allowed)', 'eLearning-shortcodes'),
		),
        ),
        'shortcode' => '[team_member pic="{{pic}}" name="{{name}}" designation="{{designation}}"] {{child_shortcode}}  [/team_member]',
        'popup_title' => __('Insert Team Member Shortcode', 'eLearning-shortcodes'),
        'child_shortcode' => array(
        'params' => array(
                'icon' => array(
					'type' => 'socialicon',
					'label' => __('Social Icon', 'eLearning-shortcodes'),	
                    ),
            'url' => array(
						'std' => 'http://www.eLearningthemes.com',
						'type' => 'text',
						'label' => __('Icon Link', 'eLearning-shortcodes'),
                    )
                ),
        'shortcode' => '[team_social url="{{url}}" icon="{{icon}}"]',
        'clone_button' => __('Add Social Information', 'eLearning-shortcodes')
                )
    );


/*-----------------------------------------------------------------------------------*/
/*	Google Maps Config
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['maps'] = array(
	'no_preview' => true,
	'params' => array(
		'map' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('End Map Iframe code', 'eLearning-shortcodes'),
			'desc' => __('Enter your map iframce code including iframe tags', 'eLearning-shortcodes'),
		)
		
	),
	'shortcode' => '[map]{{map}}[/map]',
	'popup_title' => __('Insert Google Maps Shortcode', 'eLearning-shortcodes')
);

/*-----------------------------------------------------------------------------------*/
/*	Gallery Config
/*-----------------------------------------------------------------------------------*/

                        
$eLearning_shortcodes['gallery'] = array(
	'no_preview' => true,
	'params' => array(
                
		'size' => array(
		                'std' =>'',
			'type' => 'select',
			'label' => __('Select Thumb Size', 'eLearning-shortcodes'),
			'desc' => __('Image size', 'eLearning-shortcodes'),
			'options' => array(
			                        '' => 'Select Size',
			                        'normal' => 'Normal',
			                        'small' => 'Small',
			                        'micro' => 'Very Small',
			                        'large' => 'Large'
			            )
		),
		
                'ids' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Attachment Ids', 'eLearning-shortcodes'),
			'desc' => __('Attachment Ids separated by comma', 'eLearning-shortcodes'),
		)
		
	),
	'shortcode' => '[gallery size="{{size}}" ids="{{ids}}"]',
	'popup_title' => __('Insert Gallery Shortcode', 'eLearning-shortcodes')
);

/*-----------------------------------------------------------------------------------*/
/*	Social Icons
/*-----------------------------------------------------------------------------------*/


$eLearning_shortcodes['socialicons'] = array(
	'no_preview' => true,
	'params' => array(
		'icon' => array(
					'type' => 'socialicon',
					'label' => __('Social Icon', 'eLearning-shortcodes'),
					'desc' => __('Select Elastic Social Icon, takes size/color of text it is inserted in:', 'eLearning-shortcodes'),
				),	
				'size' => array(
					'std' => '32',
					'type' => 'text',
					'label' => __('Size in pixels', 'eLearning-shortcodes'),
					'desc' => __('Enter Elastic font size in pixels ', 'eLearning-shortcodes'),
				),
				),
				
				        'shortcode' => '[socialicon icon="{{icon}}" size="{{size}}"]',
				        'popup_title' => __('Insert Social Icon Shortcode', 'eLearning-shortcodes')
			);
/*-----------------------------------------------------------------------------------*/
/*	Forms
/*-----------------------------------------------------------------------------------*/



$eLearning_shortcodes['forms'] = array(
	'no_preview' => true,
	'params' => array(
                    'to' => array(
					'std' => 'example@example.com',
					'type' => 'text',
					'label' => __('Enter email', 'eLearning-shortcodes'),
					'desc' => __('Email is sent to this email. Use comma for multiple entries', 'eLearning-shortcodes'),
				),
                    'subject' => array(
					'std' => 'Subject',
					'type' => 'text',
					'label' => __('Email Subject', 'eLearning-shortcodes'),
					'desc' => __('Subject of email', 'eLearning-shortcodes'),
				),             
		),
	'shortcode' => '[form to="{{to}}" subject="{{subject}}"] {{child_shortcode}}  [/form]',
    'popup_title' => __('Generate Contact Form Shortcode', 'eLearning-shortcodes'),
    'child_shortcode' => array(
        'params' => array(
                    'placeholder' => array(
			'std' => 'Name',
			'type' => 'text',
			'label' => __('Label Text', 'eLearning-shortcodes'),
			'desc' => __('Add the content. Accepts HTML & other Shortcodes.', 'eLearning-shortcodes'),
                    ),
                    'type' => array(
			'type' => 'select',
			'label' => __('Form Element', 'eLearning-shortcodes'),
			'desc' => __('select Form element type', 'eLearning-shortcodes'),
			'options' => array(
                            'text' => 'Single Line Text Box (Text)',
                            'textarea' => 'Multi Line Text Box (TextArea)',
                            'select' => 'Select from Options (Select)',
                            'captcha' => 'Captcha field',
                            'submit' => 'Submit Button'
                        )
                    ),
            'options' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Enter Select Options', 'eLearning-shortcodes'),
			'desc' => __('Comma seperated options.', 'eLearning-shortcodes'),
                    ),
            'validate' => array(
			'type' => 'select',
			'label' => __('Validation', 'eLearning-shortcodes'),
			'desc' => __('select Form element type', 'eLearning-shortcodes'),
			'options' => array(
                            '' => 'None',
                            'required' => 'Required',
                            'email' => 'Email',
                            'numeric' => 'Numeric',
                            'phone' => 'Phone Number'
                        )
                    ),
                    
              ),
        'shortcode' => '[form_element type="{{type}}" validate="{{validate}}" options="{{options}}" placeholder="{{placeholder}}"]',
        'clone_button' => __('Add Form Element', 'eLearning-shortcodes')
    )
);	


/*-----------------------------------------------------------------------------------*/
/*	HEADING
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['heading'] = array(
	'no_preview' => true,
	'params' => array(
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Enter Heading', 'eLearning-shortcodes'),
			'desc' => __('Enter heading.', 'eLearning-shortcodes')
                    )
		),
	'shortcode' => '[heading] {{content}} [/heading]',
	'popup_title' => __('Insert Heading Shortcode', 'eLearning-shortcodes')
);					

/*-----------------------------------------------------------------------------------*/
/*	VIDEO
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['iframevideo'] = array(
	'no_preview' => true,
	'params' => array(
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Enter Video iframe Code', 'eLearning-shortcodes'),
			'desc' => __('For Responsive iframe videos form Youtube, Vimeo,bliptv etc...', 'eLearning-shortcodes')
                    )
		),
	'shortcode' => '[iframevideo] {{content}} [/iframevideo]',
	'popup_title' => __('Insert iFrame Video Shortcode', 'eLearning-shortcodes')
);					

/*-----------------------------------------------------------------------------------*/
/*	IFRAME
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['iframe'] = array(
	'no_preview' => true,
	'params' => array(
		'height' => array(
					'std' => '600',
					'type' => 'text',
					'label' => __('Enter Iframe Height', 'eLearning-shortcodes'),
					'desc' => __('Set iframe height', 'eLearning-shortcodes'),
				),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Enter iframe URL', 'eLearning-shortcodes'),
			'desc' => __('For Responsive iframe based content, like Articulate storyline, iSpring content etc...', 'eLearning-shortcodes')
                    )
		),
	'shortcode' => '[iframe height={{height}}] {{content}} [/iframe]',
	'popup_title' => __('Insert iFrame Shortcode', 'eLearning-shortcodes')
);	

/*-----------------------------------------------------------------------------------*/
/*	SURVEY RESULT
/*-----------------------------------------------------------------------------------*/

$eLearning_shortcodes['survey_result'] = array(
	'no_preview' => true,
	'params' => array(
		'quiz_id' => array(
					'std' => '',
					'type' => 'number',
					'label' => __('Enter Quiz id (optional)', 'eLearning-shortcodes'),
					'desc' => __('Quiz id for which Survey results are to be displayed.', 'eLearning-shortcodes'),
				),
		'user_id' => array(
					'std' => '',
					'type' => 'number',
					'label' => __('Enter User id (optional)', 'eLearning-shortcodes'),
					'desc' => __('User id for which Survey results are to be displayed.', 'eLearning-shortcodes'),
				),
		'lessthan' => array(
					'std' => '',
					'type' => 'number',
					'label' => __('Enter result Upper limit ', 'eLearning-shortcodes'),
					'desc' => __('Message to be displayed if survey score is less than this value', 'eLearning-shortcodes'),
				),
		'greaterthan' => array(
					'std' => '',
					'type' => 'number',
					'label' => __('Enter result Lower limit ', 'eLearning-shortcodes'),
					'desc' => __('Message to be displayed if survey score is more than this value', 'eLearning-shortcodes'),
				),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Enter Survey result message', 'eLearning-shortcodes'),
			'desc' => __('Enter message for result', 'eLearning-shortcodes')
                    )
		),
	'shortcode' => '[survey_result user_id={{user_id}} quiz_id={{quiz_id}} lessthan={{lessthan}} greaterthan={{greaterthan}}] {{content}} [/survey_result]',
	'popup_title' => __('Insert Survey Result Shortcode in Quiz completion message', 'eLearning-shortcodes')
);	

?>