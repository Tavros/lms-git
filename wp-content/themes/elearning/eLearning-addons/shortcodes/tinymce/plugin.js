(function($) {
"use strict";

 			//Shortcodes
           tinymce.PluginManager.add( 'eLearningShortcodes', function( editor, url ) {

				editor.addCommand("eLearningPopup", function ( a, params ){
					var popup = params.identifier;
					tb_show("Insert Shortcode", url + "/popup.php?popup=" + popup + "&width=" + 800);
				});
     
                editor.addButton( 'eLearning_button', {
                    type: 'splitbutton',
                    icon: 'icon eLearning-icon',
					title:  'ELearning Shortcodes',
					onclick : function(e) {},
					menu: [
					{text: eLearning_shortcode_icon_strings.accordion,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.accordion,identifier: 'accordion'})
					}},
					{text: eLearning_shortcode_icon_strings.buttons,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.buttons,identifier: 'button'})
					}},
					{text: eLearning_shortcode_icon_strings.columns,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.columns,identifier: 'columns'})
					}},
					{text: eLearning_shortcode_icon_strings.counter,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.counter,identifier: 'counter'})
					}},
					{text: eLearning_shortcode_icon_strings.course,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.course,identifier: 'course'})
					}},
					{text: eLearning_shortcode_icon_strings.divider,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.divider,identifier: 'divider'})
					}},
					{text: eLearning_shortcode_icon_strings.forms,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.forms,identifier: 'forms'})
					}},
					{text: eLearning_shortcode_icon_strings.gallery,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.gallery,identifier: 'gallery'})
					}},
					{text: eLearning_shortcode_icon_strings.google_maps,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.google_maps,identifier: 'maps'})
					}},
					{text: eLearning_shortcode_icon_strings.heading,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.heading,identifier: 'heading'})
					}},
					{text: eLearning_shortcode_icon_strings.icons,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.icons,identifier: 'icons'})
					}},
					{text: eLearning_shortcode_icon_strings.iframe,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.iframe,identifier: 'iframe'})
					}},
					{text: eLearning_shortcode_icon_strings.note,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.note,identifier: 'note'})
					}},
					{text: eLearning_shortcode_icon_strings.popups,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.popups,identifier: 'popups'})
					}},
					{text: eLearning_shortcode_icon_strings.progress_bar,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.progress_bar,identifier: 'progressbar'})
					}},
					{text: eLearning_shortcode_icon_strings.pull_quote,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.pull_quote,identifier: 'pullquote'})
					}},
					{text: eLearning_shortcode_icon_strings.round_progress,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.round_progress,identifier: 'roundprogress'})
					}},
					{text: eLearning_shortcode_icon_strings.survey_result,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.survey_result,identifier: 'survey_result'})
					}},
					{text: eLearning_shortcode_icon_strings.tabs,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.tabs,identifier: 'tabs'})
					}},
					{text: eLearning_shortcode_icon_strings.team,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.team,identifier: 'team_member'})
					}},
					{text: eLearning_shortcode_icon_strings.testimonial,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.testimonial,identifier: 'testimonial'})
					}},
					{text: eLearning_shortcode_icon_strings.tooltips,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.tooltips,identifier: 'tooltip'})
					}},
					{text: eLearning_shortcode_icon_strings.video,onclick:function(){
						editor.execCommand("eLearningPopup", false, {title: eLearning_shortcode_icon_strings.video,identifier: 'iframevideo'})
					}},
					]                
        	  });
         
          });  
 
})(jQuery);
