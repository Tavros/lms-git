jQuery(document).ready(function($) {

	$(".eLearning-tabs").tabs({ fx: { opacity: 'show' } });
	
	$(".eLearning-toggle").each( function () {
		if($(this).attr('data-id') == 'closed') {
			$(this).accordion({ header: '.eLearning-toggle-title', collapsible: true, active: false  });
		} else {
			$(this).accordion({ header: '.eLearning-toggle-title', collapsible: true});
		}
	});
	
	
});