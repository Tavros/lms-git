<?php

function eLearning_events_template() {

    if (!is_user_logged_in())
        wp_redirect(site_url());

    $template = 'templates/events';
global $bp;


    if ($bp->current_component == 'events') {

        $translation_array = array(
            'earnings' => __('Earnings', 'eLearning-events'),
            'payout' => __('Payout', 'eLearning-events'),
            'students' => __('# Students', 'eLearning-events'),
            'saved' => __('SAVED', 'eLearning-events'),
            'saving' => __('SAVING ...', 'eLearning-events'),
            'select_recipients' => __('Select recipients...', 'eLearning-events'),
            'stats_calculated' => __('Stats Calculated, reloading page ...', 'eLearning-events')
        );
        wp_localize_script('eLearning-events-js', 'eLearning_events_strings', $translation_array);
    }


    $located_template = apply_filters('bp_located_template', locate_template($template, false), $template);

           bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/events'));

}


function get_admin_events_content(){
    var_dump('get_admin_events_content');
}
