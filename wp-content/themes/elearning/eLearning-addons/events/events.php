<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_EVENTS_URL')) {
    define('ELEARNING_EVENTS_URL', get_template_directory_uri() . '/eLearning-addons/events/');
}

include_once 'includes/functions.php';
include_once 'includes/events.php';
add_action('after_setup_theme', 'load_events_textdomain');

function load_events_textdomain() {
    load_textdomain('eLearning-events', dirname(__FILE__) . '/languages/');
}

?>