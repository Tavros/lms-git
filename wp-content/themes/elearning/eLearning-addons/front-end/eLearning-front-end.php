<?php

if (!defined('ABSPATH'))
    exit;



if (!class_exists('eLearning_Front_End')) {
    require_once('includes/class_eLearning_front_end.php' );
    require_once('includes/class.generate_fields.php' );
    require_once('includes/class.process_fields.php' );
    require_once('includes/class_eLearning_front_end.php' );
    eLearning_Front_End::instance();
}

add_action('wp_enqueue_scripts', 'eLearning_front_end_enqueue_scripts');

function eLearning_front_end_enqueue_scripts() {
    if (function_exists('eLearning_get_option')) {
        $edit_course = eLearning_get_option('create_course');

        if (function_exists('icl_object_id'))
            $edit_course = icl_object_id($edit_course, 'page', true);

        if (is_numeric($edit_course) && (is_page($edit_course))) { //adhoc fix
        } else {
            global $wp_query;
            if ((!isset($_GET['edit']) && !isset($wp_query->query_vars['edit'])) || !current_user_can('edit_posts'))
                return;
        }
    }
    eLearning_front_end_loadscripts();
}

function eLearning_front_end_loadscripts() {
    $itemName = 'course';
    if (get_the_ID() !== COURSE_EDIT_PAGE) {
        $itemName = 'event';
    }
    wp_enqueue_media();
    wp_enqueue_style('eLearning-front-end-css', get_template_directory_uri() . '/eLearning-addons/front-end/assets/css/eLearning_front_end.min.css', array(), '2.6');
    wp_enqueue_script('eLearning-front-end-js', get_template_directory_uri() . '/eLearning-addons/front-end/assets/js/eLearning_front_end.min.js?1', array('bp-course-js', 'jquery-ui-core', 'jquery-ui-sortable', 'jquery-ui-slider', 'jquery-ui-datepicker', 'bp-confirm'), '2.6');

    $translation_array = array(
        'course_title' => __('Please change the ' . $itemName . ' title', 'eLearning-front-end'),
        'create_course_confirm' => __('This will create a new ' . $itemName . ' in the site, do you want to continue ?', 'eLearning-front-end'),
        'create_course_confirm_button' => __('Yes, create a new ' . $itemName . '', 'eLearning-front-end'),
        'save_course_confirm' => __('This will overwrite the previous ' . $itemName . ' settings, do you want to continue ?', 'eLearning-front-end'),
        'save_course_confirm_button' => __('Save ' . $itemName . '', 'eLearning-front-end'),
        'create_unit_confirm' => __('This will ' . $itemName . ' a new unit in the site, do you want to continue ?', 'eLearning-front-end'),
        'create_unit_confirm_button' => __('Yes, ' . $itemName . ' a new unit', 'eLearning-front-end'),
        'save_unit_confirm' => __('This will overwrite the existing unit settings, do you want to continue ?', 'eLearning-front-end'),
        'save_unit_confirm_button' => __('Yes, save unit settings', 'eLearning-front-end'),
        'create_question_confirm' => __('This will create a new question in the site, do you want to continue ?', 'eLearning-front-end'),
        'create_question_confirm_button' => __('Yes, create a new question', 'eLearning-front-end'),
        'create_quiz_confirm' => __('This will create a new quiz in the site, do you want to continue ?', 'eLearning-front-end'),
        'create_quiz_confirm_button' => __('Yes, create a new quiz', 'eLearning-front-end'),
        'save_quiz_confirm' => __('This will overwrite the existing quiz settings, do you want to continue ?', 'eLearning-front-end'),
        'save_quiz_confirm_button' => __('Yes, save quiz settings', 'eLearning-front-end'),
        'delete_confirm' => __('This will delete the element from your site, do you want to continue ?', 'eLearning-front-end'),
        'delete_confirm_button' => __('Continue', 'eLearning-front-end'),
        'save_confirm' => __('This will overwrite the previous settings, do you want to continue ?', 'eLearning-front-end'),
        'save_confirm_button' => __('Save', 'eLearning-front-end'),
        'create_assignment_confirm' => __('This will create a new assignment in the site, do you want to continue ?', 'eLearning-front-end'),
        'create_assignment_confirm_button' => __('Yes, create a new assignment', 'eLearning-front-end'),
        'course_offline' => __('Are you sure you want to take ' . $itemName . ' offline, this will remove the ' . $itemName . ' from ' . $itemName . ' directory and it will not be visible to your students', 'eLearning-front-end'),
        'delete_course_confirm' => __('Are you sure you want to delete this ' . $itemName . ' ?', 'eLearning-front-end'),
        'delete_button' => __('Delete ' . $itemName . '', 'eLearning-front-end'),
        'create_group_confirm' => __('This will create a new group in the site, do you want to continue ?', 'eLearning-front-end'),
        'create_forum_confirm' => __('This will create a new forum in the site, do you want to continue ?', 'eLearning-front-end'),
    );
    wp_localize_script('eLearning-front-end-js', 'eLearning_front_end_messages', $translation_array);
}

add_action('after_setup_theme', 'load_eLearning_front_end_textdomain');

function load_eLearning_front_end_textdomain() {
    load_textdomain('eLearning-front-end', dirname(__FILE__) . '/languages/');
}

?>
