<?php

class eLearning_Front_End {

    const VERSION = '2.0';

    private static $instance;

    private function __construct() {

        add_action('eLearning_be_instructor_button', array($this, 'create_course_button'), 5);

        add_action('template_redirect', array($this, 'eLearning_front_end_validate_action'));
        add_action('eLearning_before_create_course_header', array($this, 'eLearning_before_create_course_page'), 1);
        add_action('bp_course_options_nav', array($this, 'eLearning_edit_course_menu_link'));

        /* == OLD FRONT END AJAX CALLS === */
        add_action('wp_ajax_create_course', array($this, 'create_course'));
        add_action('wp_ajax_save_course', array($this, 'save_course'));
        add_action('wp_ajax_save_course_settings', array($this, 'save_course_settings'));
        add_action('wp_ajax_create_unit', array($this, 'create_unit'));
        add_action('wp_ajax_create_quiz', array($this, 'create_quiz'));
        add_action('wp_ajax_delete_curriculum', array($this, 'delete_curriculum'));
        add_action('wp_ajax_save_curriculum', array($this, 'save_curriculum'));
        add_action('wp_ajax_save_pricing', array($this, 'save_pricing'));
        add_action('wp_ajax_save_membership', array($this, 'save_membership'));
        add_filter('eLearning_create_course_settings', array($this, 'eLearning_create_course_settings'));
        add_filter('eLearning_frontend_create_course_pricing', array($this, 'eLearning_frontend_create_course_pricing'));


        /* == Current Front end Editor compatible calls == */
        add_filter('eLearning_front_end_quiz_settings', array($this, 'eLearning_front_end_quiz_settings'));
        add_action('eLearning_front_end_unit_controls', array($this, 'eLearning_front_end_unit_controls'));
        add_filter('eLearning_front_end_unit_settings', array($this, 'eLearning_front_end_unit_settings'));
        add_action('wp_ajax_save_unit_settings', array($this, 'save_unit_settings'));
        add_action('eLearning_front_end_quiz_controls', array($this, 'eLearning_front_end_quiz_controls'));
        add_action('eLearning_front_end_quiz_meta_controls', array($this, 'eLearning_front_end_quiz_meta_controls'));
        add_action('wp_ajax_create_question', array($this, 'create_question'));
        add_action('wp_ajax_save_quiz_settings', array($this, 'save_quiz_settings'));
        add_action('wp_ajax_delete_question', array($this, 'delete_question'));
        add_action('eLearning_front_end_question_controls', array($this, 'eLearning_front_end_question_controls'));
        add_filter('eLearning_front_end_question_settings', array($this, 'eLearning_front_end_question_settings'));
        add_action('wp_ajax_save_question', array($this, 'save_question'));
        add_action('wp_ajax_create_assignment', array($this, 'create_assignment'));
        add_action('eLearning_front_end_assignment_controls', array($this, 'eLearning_front_end_assignment_controls'));
        add_filter('eLearning_front_end_assignment_settings', array($this, 'eLearning_front_end_assignment_settings'));
        add_action('wp_ajax_save_assignment_settings', array($this, 'save_assignment_settings'));
        add_action('eLearning_unit_end_front_end_controls', array($this, 'eLearning_unit_upload_zip_controls'));
        add_filter('eLearning_front_end_pricing', array($this, 'show_pricing'));


        /* == Ajax calls used in Current Front end Editor == */
        add_action('wp_ajax_publish_course', array($this, 'publish_course'));
        add_action('wp_ajax_offline_course', array($this, 'offline_course'));
        add_action('wp_ajax_delete_course', array($this, 'delete_course'));
    }

    public static function instance() {
        if (!self::$instance)
            self::$instance = new self;

        return self::$instance;
    }

    function create_course_button() {
        if (function_exists('eLearning_get_option')) {
            $pageid = eLearning_get_option('create_course');

            if (isset($pageid) && $pageid && current_user_can('edit_posts')) {
                if (class_exists('ELEARNING_Actions')) {
                    $actions = ELEARNING_Actions::init();
                    remove_action('eLearning_be_instructor_button', array($actions, 'eLearning_be_instructor_button'));
                }
                echo '<a href="' . get_permalink($pageid) . '" class="button create-group-button full">' . __('Create a Course', 'eLearning-front-end') . '</a>';
            }
        }
    }

    function eLearning_before_create_course_page() {
        if (!current_user_can('edit_posts')) {
            wp_die(__('COURSE CREATION ONLY ALLOWED FOR INSTRUCTORS & ADMINISTRATORS', 'eLearning-front-end'));
        }
    }

    function eLearning_front_end_validate_action() {
        $create_course = eLearning_get_option('create_course');
        if (isset($_GET['action']) && is_page($create_course)) {
            if (is_numeric($_GET['action']) && (get_post_type($_GET['action']) == 'course')) {
                if (current_user_can('edit_post', $_GET['action'])) {
                    
                } else {
                    wp_die(__('Unable to edit Course, please contact Site Administrator', 'eLearning-front-end'));
                }
            } else {
                wp_die(__('Incorrect Action, please contact Site Administrator', 'eLearning-front-end'));
            }
        }
    }

    function eLearning_create_course_settings($course_settings) {
        if (isset($_GET['action']) && is_numeric($_GET['action'])) {
            $course_id = $_GET['action'];
            foreach ($course_settings as $key => $value) {
                $db = get_post_meta($course_id, $key, true);
                if (isset($db) && $db != '')
                    $course_settings[$key] = $db;
            }
            //Anamoly
            if ($course_settings['eLearning_course_badge'] == '' || $course_settings['eLearning_course_badge'] == ' ') {
                $course_settings['eLearning_badge'] = 'H';
                $course_settings['eLearning_course_badge'] = '';
            }
        }
        return $course_settings;
    }

    function eLearning_edit_course_menu_link($nav_menu) {
        $pageid = COURSE_EDIT_PAGE;
        if (get_post_meta(get_the_ID(), 'course_type', true) == 'event') {
            $pageid = EVENT_EDIT_PAGE;
        }


        if (function_exists('icl_object_id'))
            $pageid = icl_object_id($pageid, 'page', true);

        $user_id = get_current_user_id();
        global $post;
        if (isset($pageid) && $pageid && ( $post->post_author == $user_id || current_user_can('manage_options'))) {
            $slh = 'Course';
            if (get_post_meta($post->ID, 'course_type', true) === 'event') {
                $slh = 'Event';
            }
            echo '<li id="edit"><a href="' . get_permalink($pageid) . '?action=' . $post->ID . '">' . __('Edit ' . $slh, 'eLearning-front-end') . '</a></li>';
        }
    }

    function create_course() {
        $user_id = get_current_user_id();
        $title = $_POST['title'];
        $category = $_POST['category'];
        $newcategory = $_POST['newcategory'];
        $thumbnail_id = $_POST['thumbnail'];
        $description = $_POST['description'];
        $courselinkage = $_POST['courselinkage'];
        $newcourselinkage = $_POST['newcourselinkage'];


        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id) || !current_user_can('edit_posts')) {
            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
            die();
        }

        $course_post = array(
            'post_status' => 'draft',
            'post_type' => 'course',
            'post_title' => $title,
            'post_excerpt' => $description,
            'post_content' => $description,
            'comment_status' => 'open'
        );

        if (is_numeric($category)) {
            $course_post['tax_input'] = array('course-cat' => $category);
        } else if ($category == 'new') {
            $term = term_exists($newcategory, 'course-cat');
            if ($term !== 0 && $term !== null) {
                $course_post['tax_input'] = array('course-cat' => $term['term_id']);
            } else {
                $new_term = wp_insert_term($newcategory, 'course-cat');
                if (is_array($new_term)) {
                    $course_post['tax_input'] = array('course-cat' => $new_term['term_id']);
                } else {
                    _e('Unable to create a new Course Category. Contact Admin !', 'eLearning-front-end');
                    die();
                }
            }
        }


        $post_id = wp_insert_post($course_post);

        if (is_numeric($post_id)) {
            if (isset($thumbnail_id) && is_numeric($thumbnail_id))
                set_post_thumbnail($post_id, $thumbnail_id);

            //Linkage
            if (isset($courselinkage) && $courselinkage) {
                $course_linkage = array($courselinakge);
                wp_set_post_terms($post_id, $course_linkage, 'linkage');
            }

            if ($courselinkage == 'add_new') {
                $new_term = wp_insert_term($newcourselinkage, 'linkage');
                if (is_array($new_term)) {
                    $course_linkage = array($newcourselinkage);
                    $check = wp_set_post_terms($post_id, $course_linkage, 'linkage');
                }
            }

            echo $post_id;
        } else {
            _e('Unable to create course, contact admin !', 'eLearning-front-end');
        }

        die();
    }

    function save_course() {
        $user_id = get_current_user_id();
        $course_id = $_POST['ID'];
        $title = $_POST['title'];
        $status = $_POST['status'];
        $category = $_POST['category'];
        $newcategory = $_POST['newcategory'];
        $thumbnail_id = $_POST['thumbnail'];
        $description = $_POST['description'];
        $courselinkage = $_POST['courselinkage'];
        $newcourselinkage = $_POST['newcourselinkage'];

        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id) || !current_user_can('edit_posts')) {
            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
            die();
        }


        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }

        $the_post = get_post($course_id);
        if ($the_post->post_author != $user_id && !current_user_can('manage_options')) { // Instructor and Admin check
            _e('Invalid Course Instructor', 'eLearning-front-end');
            die();
        }

        $course_post = array(
            'ID' => $course_id,
            'post_status' => $status,
            'post_title' => $title,
            'post_excerpt' => $description
        );

        $post_id = wp_update_post($course_post);
        echo $post_id;

        if (is_numeric($category)) {
            wp_set_post_terms($course_id, $category, 'course-cat');
        } else if ($category == 'new') {
            $term = term_exists($newcategory, 'course-cat');
            if ($term !== 0 && $term !== null) {
                wp_set_post_terms($course_id, $term['term_id'], 'course-cat');
            } else {
                $new_term = wp_insert_term($newcategory, 'course-cat');
                if (is_array($new_term)) {
                    wp_set_post_terms($course_id, $new_term['term_id'], 'course-cat');
                } else {
                    _e('Unable to create a new Course Category. Contact Admin !', 'eLearning-front-end');
                    die();
                }
            }
        }



        if (is_numeric($post_id) && $post_id) {
            if (isset($thumbnail_id) && is_numeric($thumbnail_id))
                set_post_thumbnail($post_id, $thumbnail_id);

            //Linkage
            if (isset($courselinkage) && $courselinkage) {
                $course_linkage = array($courselinkage);
                wp_set_post_terms($post_id, $course_linkage, 'linkage');
            }

            if ($courselinkage == 'add_new') {
                $new_term = wp_insert_term($newcourselinkage, 'linkage');
                if (is_array($new_term)) {
                    $course_linkage = array($newcourselinkage);
                    wp_set_post_terms($post_id, $course_linkage, 'linkage');
                }
            }

            echo $post_id;
        } else {
            _e('Unable to create course, contact admin !', 'eLearning-front-end');
        }

        die();
    }

    function save_course_settings() {

        $user_id = get_current_user_id();
        $course_id = $_POST['course_id'];
        $course_setting['eLearning_course_auto_eval'] = $_POST['eLearning_course_auto_eval'];
        $course_setting['eLearning_duration'] = $_POST['eLearning_duration'];
        $course_setting['eLearning_pre_course'] = $_POST['eLearning_pre_course'];
        $course_setting['eLearning_course_drip'] = $_POST['eLearning_course_drip'];
        $course_setting['eLearning_course_drip_duration'] = $_POST['eLearning_course_drip_duration'];
        $course_setting['eLearning_course_certificate'] = $_POST['eLearning_certificate'];
        $course_setting['eLearning_course_passing_percentage'] = $_POST['eLearning_course_passing_percentage'];
        $course_setting['eLearning_certificate_template'] = $_POST['eLearning_certificate_template'];
        $course_setting['eLearning_badge'] = $_POST['eLearning_badge'];
        $course_setting['eLearning_course_badge_percentage'] = $_POST['eLearning_course_badge_percentage'];
        $course_setting['eLearning_course_badge_title'] = $_POST['eLearning_course_badge_title'];
        $course_setting['eLearning_course_badge'] = $_POST['eLearning_course_badge'];
        $course_setting['eLearning_max_students'] = $_POST['eLearning_max_students'];
        $course_setting['eLearning_start_date'] = $_POST['eLearning_start_date'];
        $course_setting['eLearning_course_retakes'] = $_POST['eLearning_course_retakes'];
        $course_setting['eLearning_group'] = $_POST['eLearning_group'];
        $course_setting['eLearning_forum'] = $_POST['eLearning_forum'];
        $course_setting['eLearning_course_instructions'] = $_POST['eLearning_course_instructions'];
        $course_setting['eLearning_course_message'] = $_POST['eLearning_course_message'];

        $flag = 0; //Error Flag
        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id) || !current_user_can('edit_posts')) {
            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
            die();
        }

        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }

        $the_post = get_post($course_id);
        if ($the_post->post_author != $user_id && !current_user_can('manage_options')) {
            _e('Invalid Course Instructor', 'eLearning-front-end');
            die();
        }

        if ($course_setting['eLearning_badge'] == 'H') {
            $course_setting['eLearning_course_badge'] = '';
        }
        foreach ($course_setting as $key => $value) {
            $prev_val = get_post_meta($course_id, $key, true);
            if ($prev_val != $value) {
                update_post_meta($course_id, $key, $value);
            }
        }

        if ($course_setting['eLearning_group'] == 'add_new' && !$flag) {
            $the_course = get_post($course_id);
            $t = wp_get_attachment_image_src(get_post_thumbnail_id($course_id, 'thumbnail'));
            $f = wp_get_attachment_image_src(get_post_thumbnail_id($course_id, 'full'));

            $group_slug = $the_course->post_name; //groups_check_slug( sanitize_title( esc_attr( $the_course->post_name ) ) );
            $group_settings = array(
                'creator_id' => $user_id,
                'name' => $the_course->post_title,
                'slug' => $group_slug,
                'description' => $the_course->post_excerpt,
                'status' => 'private',
                'date_created' => current_time('mysql')
            );

            $group_settings = apply_filters('eLearning_front_end_group_vars', $group_settings);
            if ($course_setting['eLearning_forum'] == 'add_group_forum') {
                $group_settings['enable_forum'] = 1;
            }


            global $bp;

            $new_group_id = groups_create_group($group_settings);

            bp_core_avatar_handle_crop(array('object' => 'group', 'avatar_dir' => 'group-avatars', 'item_id' => $new_group_id, 'original_file' => $f[0], 'crop_x' => 0, 'crop_y' => 0, 'crop_w' => $f[1], 'crop_h' => $f[2]));

            groups_update_groupmeta($new_group_id, 'total_member_count', 1);
            groups_update_groupmeta($new_group_id, 'last_activity', gmdate("Y-m-d H:i:s"));
            update_post_meta($course_id, 'eLearning_group', $new_group_id);


            if ($course_setting['eLearning_forum'] == 'add_group_forum') {

                $forum_settings = array(
                    'post_title' => stripslashes($the_course->post_title),
                    'post_content' => stripslashes($the_course->post_excerpt),
                    'post_name' => $the_course->post_name,
                    'post_status' => 'private',
                    'post_type' => 'forum',
                );
                $forum_settings = apply_filters('eLearning_front_end_forum_vars', $forum_settings);
                $new_forum_id = wp_insert_post($forum_settings);

                //Linkage 
                $linkage = eLearning_get_option('linkage');
                if (isset($linkage) && $linkage) {
                    $course_linkage = wp_get_post_terms($course_id, 'linkage', array("fields" => "names"));
                    if (isset($course_linkage) && is_array($course_linkage))
                        wp_set_post_terms($new_forum_id, $course_linkage, 'linkage');
                }
                groups_update_groupmeta($new_group_id, 'forum_id', array($new_forum_id));
                update_post_meta($course_id, 'eLearning_forum', $new_forum_id);
            }
        }

        if ($course_setting['eLearning_forum'] == 'add_new' && !$flag) {
            $forum_settings = array(
                'post_title' => stripslashes($the_post->post_title),
                'post_content' => stripslashes($the_post->post_excerpt),
                'post_name' => $the_post->post_name,
                'post_status' => 'private',
                'post_type' => 'forum',
            );

            $forum_settings = apply_filters('eLearning_front_end_forum_vars', $forum_settings);
            $new_forum_id = wp_insert_post($forum_settings);
            update_post_meta($course_id, 'eLearning_forum', $new_forum_id);
        }

        if (isset($_POST['level']) && $_POST['level']) {
            $level = $_POST['level'];
            if (is_numeric($level)) {
                wp_set_post_terms($course_id, $level, 'level');
            }
        }

        if ($flag) {
            echo $message;
        } else {
            echo $course_id;
            do_action('eLearning_course_settings_updated', $course_id);
        }

        die();
    }

    function create_unit() {
        $user_id = get_current_user_id();
        $course_id = $_POST['course_id'];
        $unit_title = stripslashes($_POST['unit_title']);

        if (!isset($unit_title) || count($unit_title) < 2 && $unit_title == '') {
            _e('Can not have a blank Unit ', 'eLearning-front-end');
            die();
        }

        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id) || !current_user_can('edit_posts')) {
            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
            die();
        }

        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }

        $the_post = get_post($course_id);
        if ($the_post->post_author != $user_id && !current_user_can('manage_options')) {
            _e('Invalid Course Instructor', 'eLearning-front-end');
            die();
        }

        $unit_settings = array(
            'post_title' => $unit_title,
            'post_content' => $unit_title,
            'post_status' => 'publish',
            'post_type' => 'unit',
        );
        $unit_settings = apply_filters('eLearning_front_end_unit_vars', $unit_settings);
        $unit_id = wp_insert_post($unit_settings);



        echo '<h3 class="title" data-id="' . $unit_id . '"><i class="icon-file"></i> ' . $unit_title . '</h3>
                <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="' . get_permalink($unit_id) . 'edit/?id=' . $course_id . '" target="_blank">' . __('Edit Unit', 'eLearning-front-end') . '</a></li>
                    <li><a href="' . get_permalink($unit_id) . '" target="_blank">' . __('Preview Unit', 'eLearning-front-end') . '</a></li>
                    <li><a class="remove">' . __('Remove', 'eLearning-front-end') . '</a></li>
                    <li><a class="delete">' . __('Delete', 'eLearning-front-end') . '</a></li>
                </ul>
                </div>
            ';

        //Linkage 
        $linkage = eLearning_get_option('linkage');
        if (isset($linkage) && $linkage) {
            $course_linkage = wp_get_post_terms($course_id, 'linkage', array("fields" => "names"));
            if (isset($course_linkage) && is_array($course_linkage))
                wp_set_post_terms($unit_id, $course_linkage, 'linkage');
        }
        die();
    }

    function create_quiz() {
        $user_id = get_current_user_id();
        $course_id = $_POST['course_id'];
        $quiz_title = stripslashes($_POST['quiz_title']);

        if (!isset($quiz_title) || count($quiz_title) < 2 && $quiz_title == '') {
            _e('Can not have a Blank Quiz', 'eLearning-front-end');
            die();
        }

        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id) || !current_user_can('edit_posts')) {
            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
            die();
        }

        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }

        $the_post = get_post($course_id);
        if ($the_post->post_author != $user_id && !current_user_can('manage_options')) {
            _e('Invalid Course Instructor', 'eLearning-front-end');
            die();
        }

        $quiz_settings = array(
            'post_title' => $quiz_title,
            'post_content' => $quiz_title,
            'post_status' => 'publish',
            'post_type' => 'quiz',
        );
        $quiz_settings = apply_filters('eLearning_front_end_quiz_vars', $quiz_settings);
        $quiz_id = wp_insert_post($quiz_settings);

        echo '<h3 class="title" data-id="' . $quiz_id . '"><i class="icon-task"></i> ' . $quiz_title . '</h3>
                <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="' . get_permalink($quiz_id) . 'edit/?id=' . $course_id . '" target="_blank" class="edit_quiz">' . __('Edit Quiz', 'eLearning-front-end') . '</a></li>
                    <li><a class="remove">' . __('Remove', 'eLearning-front-end') . '</a></li>
                    <li><a class="delete">' . __('Delete', 'eLearning-front-end') . '</a></li>
                </ul>
                </div>
            ';

        //Linkage 
        $linkage = eLearning_get_option('linkage');
        if (isset($linkage) && $linkage) {
            $course_linkage = wp_get_post_terms($course_id, 'linkage', array("fields" => "names"));
            if (isset($course_linkage) && is_array($course_linkage))
                wp_set_post_terms($quiz_id, $course_linkage, 'linkage');
        }
        die();
    }

    function delete_curriculum() {
        $user_id = get_current_user_id();
        $course_id = $_POST['course_id'];
        $id = stripslashes($_POST['id']);

        if (!isset($id) || !is_numeric($id) || $id == '') {
            _e('Can not delete', 'eLearning-front-end') . $id;
            die();
        }

        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id) || !current_user_can('edit_posts')) {
            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
            die();
        }

        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }

        $the_post = get_post($id);
        if ($the_post->post_author != $user_id && !current_user_can('manage_options')) {
            _e('Instructor can not delete this unit/quiz', 'eLearning-front-end');
            die();
        }

        $status = wp_trash_post($id);
        if ($status) {
            echo 1;
        } else {
            _e('Unable to delete', 'eLearning-front-end');
        }
        die();
    }

    function save_curriculum() {
        $user_id = get_current_user_id();
        $course_id = $_POST['course_id'];

        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id) || !current_user_can('edit_posts')) {
            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
            die();
        }

        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }

        $the_post = get_post($course_id);
        if ($the_post->post_author != $user_id && !current_user_can('manage_options')) {
            _e('Invalid Course Instructor', 'eLearning-front-end');
            die();
        }

        $objcurriculum = json_decode(stripslashes($_POST['curriculum']));
        if (is_array($objcurriculum) && isset($objcurriculum))
            foreach ($objcurriculum as $c) {
                $curriculum[] = $c->id;
            }

        // $curriculum=array(serialize($curriculum)); // Backend Compatiblity
        update_post_meta($course_id, 'eLearning_course_curriculum', $curriculum);
        echo $course_id;
        do_action('eLearning_course_curriculum_updated', $course_id, $curriculum);

        die();
    }

    function save_pricing() {
        $user_id = get_current_user_id();
        $course_id = $_POST['course_id'];

        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id) || !current_user_can('edit_posts')) {
            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
            die();
        }

        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }

        $the_post = get_post($course_id);
        if ($the_post->post_author != $user_id && !current_user_can('manage_options')) {
            _e('Invalid Course Instructor', 'eLearning-front-end');
            die();
        }

        $pricing = json_decode(stripslashes($_POST['pricing']));

        $eLearning_course_free = $pricing->eLearning_course_free;

        if ($eLearning_course_free == 'S' || $eLearning_course_free === 'S') {
            update_post_meta($course_id, 'eLearning_course_free', $eLearning_course_free);
            echo $course_id;
            die();
        } else if ($eLearning_course_free == 'H') {
            update_post_meta($course_id, 'eLearning_course_free', $eLearning_course_free);
            echo 1;
        }

        if (isset($pricing->eLearning_coming_soon) && $pricing->eLearning_coming_soon == 'S') {
            update_post_meta($course_id, 'eLearning_coming_soon', 'S');
            echo $course_id;
            die();
        } else {
            update_post_meta($course_id, 'eLearning_coming_soon', 'H');
        }

        do_action('eLearning_course_pricing_save', $course_id, $pricing);

        if (isset($pricing->eLearning_product)) {
            $eLearning_product = $pricing->eLearning_product;
            $eLearning_product_price = $pricing->eLearning_product_price;

            if (isset($pricing->eLearning_subscription)) {
                $eLearning_subscription = $pricing->eLearning_subscription;
                $eLearning_duration = $pricing->eLearning_duration;
            }
        }

        if (property_exists($pricing, 'eLearning_pmpro_membership')) {

            $eLearning_pmpro_membership = $pricing->eLearning_pmpro_membership;
            if (!count($eLearning_pmpro_membership))
                $eLearning_pmpro_membership = array();

            update_post_meta($course_id, 'eLearning_pmpro_membership', $eLearning_pmpro_membership);
            do_action('eLearning_course_pricing_membership_updated', $course_id, $eLearning_pmpro_membership);
        }

        if (isset($eLearning_product) && is_numeric($eLearning_product)) {
            update_post_meta($course_id, 'eLearning_product', $eLearning_product);

            $products_meta = eLearning_sanitize(get_post_meta($eLearning_product, 'eLearning_courses', false));
            if (!isset($products_meta) || $products_meta == '' || count($products_meta) < 1)
                $products_meta = array();

            if (!in_array($course_id, $products_meta)) {
                array_push($products_meta, $course_id);
            }
            update_post_meta($eLearning_product, 'eLearning_courses', $products_meta);
            echo $eLearning_product;
            do_action('eLearning_course_pricing_product_updated', $course_id, $eLearning_product);
        }

        if (isset($eLearning_product) && $eLearning_product == 'none') {
            $pid = get_post_meta($course_id, 'eLearning_product', true);
            if (isset($pid) && is_numeric($pid)) {
                delete_post_meta($course_id, 'eLearning_product');
                echo $pid;
                do_action('eLearning_course_pricing_product_removed', $course_id, $pid);
            }
        }
        if (isset($eLearning_product) && $eLearning_product == 'add_new') {

            if (!is_numeric($eLearning_product_price) || (!is_numeric($eLearning_duration) && $eLearning_subscription == 'S')) {
                _e('Invalid Product specs', 'eLearning-front-end');
                die();
            }
            $the_course = get_post($course_id);

            $product_settings = array(
                'post_status' => 'publish',
                'post_type' => 'product',
                'post_title' => $the_course->post_title,
                'post_excerpt' => $the_course->post_excerpt,
                'post_content' => $the_course->post_content,
                'comment_status' => 'open'
            );



            $product_settings = apply_filters('eLearning_frontend_new_product', $product_settings);
            $product_id = wp_insert_post($product_settings);
            if (isset($product_id) && $product_id) {

                $attach_id = get_post_meta($course_id, "_thumbnail_id", true);
                add_post_meta($post_id, '_thumbnail_id', $attach_id);
                wp_set_object_terms($product_id, 'simple', 'product_type');
                update_post_meta($product_id, '_price', $eLearning_product_price);
                update_post_meta($product_id, '_regular_price', $eLearning_product_price);
                update_post_meta($product_id, '_visibility', 'visible');
                update_post_meta($product_id, '_virtual', 'yes');
                update_post_meta($product_id, '_downloadable', 'yes');
                update_post_meta($product_id, '_sold_individually', 'yes');

                $courses = array($course_id);
                update_post_meta($product_id, 'eLearning_courses', $courses);
                update_post_meta($course_id, 'eLearning_product', $product_id);

                $thumbnail_id = get_post_thumbnail_id($course_id);
                set_post_thumbnail($product_id, $thumbnail_id);

                if ($eLearning_subscription == 'S') {
                    update_post_meta($product_id, 'eLearning_subscription', 'S');
                    update_post_meta($product_id, 'eLearning_duration', $eLearning_duration);
                }
                echo $product_id;
                do_action('eLearning_course_pricing_product_added', $course_id, $product_id);
                //Linkage 
                $linkage = eLearning_get_option('linkage');
                if (isset($linkage) && $linkage) {
                    $course_linkage = wp_get_post_terms($course_id, 'linkage', array("fields" => "names"));
                    if (isset($course_linkage) && is_array($course_linkage))
                        wp_set_post_terms($product_id, $course_linkage, 'linkage');
                }
                die();
            }
            _e('Unable to create product and pricing for course', 'eLearning-front-end');
        }

        do_action('eLearning_front_end_save_course_pricing', $course_id);

        die();
    }

    function publish_course() {
        $user_id = get_current_user_id();
        $course_id = $_POST['course_id'];

        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id)) {
            if (!wp_verify_nonce($_POST['security'], 'security') || !current_user_can('edit_posts')) {
                _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                die();
            }
        }

        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }

        $the_post = get_post($course_id, 'ARRAY_A'); // For futher use
        if ($the_post['post_author'] != $user_id && !current_user_can('manage_options')) {
            _e('Invalid Course Instructor', 'eLearning-front-end');
            die();
        }

        $new_course_status = eLearning_get_option('new_course_status');
        if (isset($new_course_status) && $new_course_status == 'publish') {
            $the_post['post_status'] = 'publish';
        } else {
            $the_post['post_status'] = 'pending';
        }
        $the_post = apply_filters('eLearning_frontend_course_update', $the_post);
        if (wp_update_post($the_post)) {
            echo '<div id="message" class="success"><p>' . __('Course successfully updated.', 'eLearning-front-end') . '</p></div>';
            echo '<a href="' . get_permalink($course_id) . '" class="button full">' . __('View Course', 'eLearning-front-end') . '</a>';
            do_action('eLearning_course_go_live', $course_id, $the_post);
        } else {
            echo '<div id="message"><p>' . __('Unable to update Course, contact Site admin', 'eLearning-front-end') . '</p></div>';
        }

        die();
    }

    function offline_course() {
        $user_id = get_current_user_id();
        $course_id = $_POST['course_id'];

        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id)) {
            if (!wp_verify_nonce($_POST['security'], 'security') || !current_user_can('edit_posts')) {
                _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                die();
            }
        }

        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }

        $the_post = get_post($course_id, 'ARRAY_A');
        $the_post['post_status'] = 'draft';
        if (wp_update_post($the_post)) {
            echo '<div id="message" class="success"><p>' . __('Course Offline.', 'eLearning-front-end') . '</p></div>';
        } else {
            echo '<div id="message"><p>' . __('Unable to update Course, contact Site admin', 'eLearning-front-end') . '</p></div>';
        }
        die();
    }

    function delete_course() {
        global $wpdb;
        $user_id = get_current_user_id();
        $course_id = $_POST['course_id'];

        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_course' . $user_id)) {
            if (!wp_verify_nonce($_POST['security'], 'security') || !current_user_can('edit_posts')) {
                _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                die();
            }
        }

        if (!is_numeric($course_id) || get_post_type($course_id) != 'course') {
            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
            die();
        }
        $delete_flag = apply_filters('eLearning_front_end_course_delete', 0);
        if (!$delete_flag) {
            _e('Course deletion not allowed', 'eLearning-front-end');
            die();
        }

        $fields = json_decode(stripslashes($_POST['fields']));

        if (is_array($fields) && isset($fields)) {
            foreach ($fields as $key => $c) {
                if ($c->post_meta == 'eLearning_course_curriculum') {
                    $curriculum = eLearning_sanitize(get_post_meta($course_id, $c->post_meta, false));
                    if (!is_array($curriculum) || !count($curriculum)) {
                        echo '<div id="message"><p>' . __('Unable to delete curriclum', 'eLearning-front-end') . '</p></div>';
                        die();
                    }

                    foreach ($curriculum as $key => $uid) {
                        if (is_numeric($uid)) {
                            $post_type = get_post_type($uid);
                            if ($post_type == $c->post_type) {
                                wp_trash_post($uid);
                            }
                        }
                    }
                }
                if ($c->post_meta == 'eLearning_assignment_course') {
                    $results = $wpdb->get_results($wpdb->prepare("SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = %s and meta_value = %d", 'eLearning_assignment_course', $course_id));
                    if (is_array($results) && count($results)) {
                        foreach ($results as $result) {
                            if (isset($result->post_id) && is_numeric($result->post_id)) {
                                wp_trash_post($result->post_id);
                            }
                        }
                    }
                }
                if ($c->post_meta == 'eLearning_product') {
                    $product_id = get_post_meta($course_id, 'eLearning_product', true);
                    if (!empty($product_id)) {
                        wp_trash_post($product_id);
                    }
                }
            }
        }
        if (wp_trash_post($course_id)) {

            echo '<div id="message" class="success"><p>' . __('Course Deleted.', 'eLearning-front-end') . '</p></div>';
            if (defined('BP_COURSE_SLUG') && defined('BP_COURSE_INSTRUCTOR_SLUG')) {
                ?>
                <script>
                    window.location.replace("<?php echo bp_loggedin_user_domain() . BP_COURSE_SLUG . '/' . BP_COURSE_INSTRUCTOR_SLUG; ?>");
                </script>
                <?php
            }
        } else {
            echo '<div id="message"><p>' . __('Unable to update Course, contact Site admin', 'eLearning-front-end') . '</p></div>';
        }
        die();
    }

    function eLearning_frontend_create_course_pricing($course_pricing) {

        if (isset($_GET['action']) && is_numeric($_GET['action'])) {
            $course_id = $_GET['action'];
            $course_pricing['eLearning_course_free'] = get_post_meta($course_id, 'eLearning_course_free', true);
            $course_pricing['eLearning_product'] = get_post_meta($course_id, 'eLearning_product', true);
            if (isset($course_pricing['eLearning_product']) && is_numeric($course_pricing['eLearning_product'])) {
                $course_pricing['eLearning_subscription'] = get_post_meta($course_pricing['eLearning_product'], 'eLearning_subscription', true);
                $course_pricing['eLearning_duration'] = get_post_meta($course_pricing['eLearning_product'], 'eLearning_duration', true);
            }
            $course_pricing['eLearning_pmpro_membership'] = eLearning_sanitize(get_post_meta($course_id, 'eLearning_pmpro_membership', false));
        }
        return $course_pricing;
    }

    function eLearning_front_end_unit_controls() {
        $unit_id = get_the_ID();

        if (!current_user_can('edit_posts'))
            return;

        $user_id = get_current_user_id();
        $unit_settings = array(
            'eLearning_type' => 'text-document',
            'eLearning_free' => 'H',
            'eLearning_duration' => 2,
            'eLearning_assignment' => array(),
            'eLearning_forum' => ''
        );
        $unit_settings = apply_filters('eLearning_front_end_unit_settings', $unit_settings);

        echo '<div class="eLearning_front_end_wrapper">
              <h3 class="heading">' . __('Unit Settings', 'eLearning-front-end') . '</h3>';
        ?>
        <ul class="unit_settings">
            <li><label><?php _e('Unit Type', 'eLearning-front-end'); ?></label>
                <h3><?php _e('Select Unit Type', 'eLearning-front-end'); ?><span>
                        <select id="eLearning_type">
                            <?php
                            $unit_types = apply_filters('eLearning_unit_types', array(
                                array('label' => __('Video', 'eLearning-front-end'), 'value' => 'play'),
                                array('label' => __('Audio', 'eLearning-front-end'), 'value' => 'music-file-1'),
                                array('label' => __('Podcast', 'eLearning-front-end'), 'value' => 'podcast'),
                                array('label' => __('General', 'eLearning-front-end'), 'value' => 'text-document'),
                                array('label' => __('Webinar', 'eLearning-front-end'), 'value' => 'webinar'),
                            ));
                            foreach ($unit_types as $unit) {
                                echo '<option value="' . $unit['value'] . '" ' . selected($unit_settings['eLearning_type'], $unit['value'], false) . '>' . $unit['label'] . '</option>';
                            }
                            ?>
                        </select>    
                    </span></h3></li>
            <li><label><?php _e('Free Unit', 'eLearning-front-end'); ?></label>
                <h3><?php _e('Make Unit Free', 'eLearning-front-end'); ?><span>
                        <div class="switch">
                            <input type="radio" class="switch-input eLearning_free" name="eLearning_free" value="H" id="free_no" <?php checked($unit_settings['eLearning_free'], 'H'); ?>>
                            <label for="free_no" class="switch-label switch-label-off"><?php _e('NO', 'eLearning-front-end'); ?></label>
                            <input type="radio" class="switch-input eLearning_free" name="eLearning_free" value="S" id="free_yes" <?php checked($unit_settings['eLearning_free'], 'S'); ?>>
                            <label for="free_yes" class="switch-label switch-label-on"><?php _e('YES', 'eLearning-front-end'); ?></label>
                            <span class="switch-selection"></span>
                        </div>
                    </span>
                </h3>
            </li>
            <li><label><?php _e('Unit Duration', 'eLearning-front-end'); ?></label>
                <h3><?php _e('Duration of Unit', 'eLearning-front-end'); ?><span>
                        <input type="number" class="small_box" id="eLearning_duration" value="<?php echo $unit_settings['eLearning_duration']; ?>" /> <?php
                        $unit_duration_parameter = apply_filters('eLearning_unit_duration_parameter', 60);
                        echo calculate_duration_time($unit_duration_parameter);
                        ?>
                        </li>
                        <?php
                        do_action('eLearning_front_end_unit_settings_form', $unit_settings);
                        if (in_array('eLearning-assignments/eLearning-assignments.php', apply_filters('active_plugins', get_option('active_plugins')))) {
                            ?>
                            <li><label><?php _e('Unit assignments', 'eLearning-front-end'); ?></label>
                                <h3><?php _e('Connect Assignments', 'eLearning-front-end'); ?></h3>
                                <ul id="assignments_list">
                                    <?php
                                    $args = array(
                                        'post_type' => 'eLearning-assignment',
                                        'numberposts' => -1
                                    );
                                    $args = apply_filters('eLearning_frontend_cpt_query', $args);
                                    $kposts = get_posts($args);

                                    foreach ($unit_settings['eLearning_assignment'] as $assignment) {
                                        echo '<li data-id="' . $assignment . '"> 
                                <strong><i class="icon-text-document"></i>' . get_the_title($assignment) . '</strong>';
                                        echo '
                            <div class="btn-group">
                                <button type="button" class="btn btn-course dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="' . get_permalink($assignment) . 'edit/" target="_blank" class="edit_unit">' . __('Edit Assignment', 'eLearning-front-end') . '</a></li>
                                    <li><a href="' . get_permalink($assignment) . '" target="_blank" class="edit_unit">' . __('Preview Assignment', 'eLearning-front-end') . '</a></li>
                                    <li><a class="remove">' . __('Remove', 'eLearning-front-end') . '</a></li>
                                    <li><a class="delete">' . __('Delete', 'eLearning-front-end') . '</a></li>
                                </ul>
                            </div>
                        </li>';
                                    }

                                    echo '<li class="hide"> 
                                <strong><select id="eLearning_assignment">
                                    <option value="">' . __('None', 'eLearning-front-end') . '</option>';
                                    if (is_Array($kposts))
                                        foreach ($kposts as $kpost) {
                                            echo '<option value="' . $kpost->ID . '">' . $kpost->post_title . '</option>';
                                        }
                                    echo '</select></strong>
                            <div class="btn-group">
                                <button type="button" class="btn btn-course dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="" target="_blank" class="edit_unit">' . __('Edit Assignment', 'eLearning-front-end') . '</a></li>
                                    <li><a href="" target="_blank" class="preview_unit">' . __('Preview Assignment', 'eLearning-front-end') . '</a></li>
                                    <li><a class="remove">' . __('Remove', 'eLearning-front-end') . '</a></li>
                                    <li><a class="delete">' . __('Delete', 'eLearning-front-end') . '</a></li>
                                </ul>
                            </div>
                        </li>';
                                    ?>    
                                </ul>
                                <hr />
                                <ul class="new_assignment_actions">
                                    <li><a class="link add_existing_assignment"><?php _e('ADD EXISTING ASSIGNMENT', 'eLearning-front-end'); ?></a></li>
                                    <li><a class="link add_new_assignment"><?php _e('ADD NEW ASSIGNMENT', 'eLearning-front-end'); ?></a></li>
                                    <li><input type="text" name="new_assignment_title" class="new_assignment_title mid_box left" placeholder="<?php _e('Add the Assignment title', 'eLearning-front-end'); ?>"/>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-course dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a class="publish"><?php _e('Publish', 'eLearning-front-end'); ?></a></li>
                                                <li><a class="new_remove"><?php _e('Remove', 'eLearning-front-end'); ?></a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                                <hr class="clear" />
                            </li>
                            <?php
                        }
                        if (isset($_GET['id'])) {
                            ?>
                            <li><label><?php _e('Unit Forum', 'eLearning-front-end'); ?></label>
                                <h3><?php _e('Connect a Forum', 'eLearning-front-end'); ?><span>
                                        <select id="eLearning_forum" class="chosen">
                                            <option value=""><?php _e('None', 'eLearning-front-end'); ?></option>
                                            <option value="add_group_child_forum"><?php _e('Add new child forum in Course forum', 'eLearning-front-end'); ?></option>
                                            <option value="add_new"><?php _e('Add new forum', 'eLearning-front-end'); ?></option>
                                            <?php
                                            $args = array(
                                                'post_type' => 'forum',
                                                'numberposts' => -1
                                            );
                                            $args = apply_filters('eLearning_frontend_cpt_query', $args);
                                            $kposts = get_posts($args);
                                            if (is_array($kposts))
                                                foreach ($kposts as $kpost) {
                                                    echo '<option value="' . $kpost->ID . '" ' . selected($unit_settings['eLearning_forum'], $kpost->ID) . '>' . $kpost->post_title . '</option>';
                                                }
                                            ?>
                                        </select></span>
                                </h3>                    
                            </li>
                            <?php
                        }
                        ?>
                        <li>
                            <a id="save_unit_settings" class="course_button button full" data-id="<?php echo get_the_ID(); ?>" data-course="<?php echo $_GET['id']; ?>"><?php _e('SAVE UNIT SETTINGS', 'eLearning-front-end'); ?></a>
                        </li>
                        </ul> 
                        </div>   
                        <?php
                        wp_nonce_field('save_unit' . $user_id, 'security');
                    }

                    function eLearning_front_end_unit_settings($unit_settings) {
                        global $post;
                        $unit_id = $post->ID;
                        foreach ($unit_settings as $key => $setting) {

                            if ($key == 'eLearning_assignment') {
                                $setting = eLearning_sanitize(get_post_meta($unit_id, $key, false));
                            } else {
                                $setting = get_post_meta($unit_id, $key, true);
                            }

                            if (isset($setting) && $setting)
                                $unit_settings[$key] = $setting;
                        }

                        return $unit_settings;
                    }

                    function save_unit_settings() {
                        $user_id = get_current_user_id();
                        $course_id = $_POST['course_id'];
                        $unit_id = $_POST['unit_id'];
                        $eLearning_type = $_POST['eLearning_type'];
                        $eLearning_free = $_POST['eLearning_free'];
                        $eLearning_duration = $_POST['eLearning_duration'];

                        if (isset($_POST['eLearning_assignment'])) {
                            $eLearning_assignment = $_POST['eLearning_assignment'];
                        }

                        if (isset($_POST['eLearning_forum']))
                            $eLearning_forum = $_POST['eLearning_forum'];

                        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'save_unit' . $user_id) || !current_user_can('edit_posts')) {
                            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                            die();
                        }

                        if ((isset($_POST['eLearning_forum']) && $_POST['eLearning_forum']) && (!is_numeric($course_id) || get_post_type($course_id) != 'course')) {
                            _e('Invalid Course id, please edit a course', 'eLearning-front-end');
                            die();
                        }

                        if (!is_numeric($unit_id) || get_post_type($unit_id) != 'unit') {
                            _e('Invalid Unit id, please edit a course', 'eLearning-front-end');
                            die();
                        }

                        $unit_post = get_post($unit_id, ARRAY_A);
                        if ($unit_post['post_author'] != $user_id && !current_user_can('manage_options')) {
                            _e('Invalid Unit Instructor', 'eLearning-front-end');
                            die();
                        }

                        $flag = 1;

                        update_post_meta($unit_id, 'eLearning_type', $eLearning_type);
                        update_post_meta($unit_id, 'eLearning_free', $eLearning_free);
                        update_post_meta($unit_id, 'eLearning_duration', $eLearning_duration);

                        if (isset($eLearning_assignment) && $flag) {
                            $eLearning_assignment = json_decode(stripslashes($eLearning_assignment));
                            $assignments = array();
                            if (is_array($eLearning_assignment)) {
                                foreach ($eLearning_assignment as $c) {
                                    $assignments[] = $c->id;
                                }
                            }


                            if (is_array($assignments) && isset($assignments)) {
                                update_post_meta($unit_id, 'eLearning_assignment', $assignments);
                            }
                        }

                        if (isset($eLearning_forum) && $flag)
                            if (is_numeric($eLearning_forum)) {
                                update_post_meta($unit_id, 'eLearning_forum', $eLearning_forum);
                            }


                        if ($eLearning_forum == 'add_group_child_forum' && $flag) {

                            $group_id = get_post_meta($course_id, 'eLearning_group', true);
                            if (isset($group_id) && is_numeric($groupd_id)) {
                                $forum_id = groups_get_groupmeta($group_id, 'forum_id');
                                if (is_array($forum_id))
                                    $forum_id = $forum_id[0];
                            }else {
                                $forum_id = get_post_meta($course_id, 'eLearning_forum', true);
                            }
                            if (isset($forum_id) && is_numeric($forum_id)) {
                                $forum_settings = array(
                                    'post_title' => stripslashes($unit_post['post_title']),
                                    'post_content' => stripslashes($unit_post['post_excerpt']),
                                    'post_name' => $unit_post['post_name'],
                                    'post_parent' => $forum_id,
                                    'post_status' => 'publish',
                                    'post_type' => 'forum',
                                    'comment_status' => 'closed'
                                );
                                $forum_settings = apply_filters('eLearning_front_end_forum_vars', $forum_settings);
                                if (isset($forum_id) && is_numeric($forum_id))
                                    $new_forum_id = wp_insert_post($forum_settings);
                                if (!update_post_meta($unit_id, 'eLearning_forum', $new_forum_id))
                                    $flag = 0;
                            }
                        }

                        if ($eLearning_forum == 'add_new' && $flag) {
                            $forum_settings = array(
                                'post_title' => stripslashes($unit_post['post_title']),
                                'post_content' => stripslashes($unit_post['post_excerpt']),
                                'post_name' => $unit_post['post_name'],
                                'post_status' => 'publish',
                                'post_type' => 'forum',
                                'comment_status' => 'closed'
                            );
                            $forum_settings = apply_filters('eLearning_front_end_forum_vars', $forum_settings);
                            $new_forum_id = wp_insert_post($forum_settings);
                            if (!update_post_meta($unit_post->ID, 'eLearning_forum', $new_forum_id))
                                $flag = 0;
                        }

                        do_action('eLearning_front_end_save_unit_settings_extras', $unit_id, $flag);
                        if ($flag)
                            _e('Settings Saved', 'eLearning-front-end');
                        else
                            _e('Unable to save settings', 'eLearning-front-end');

                        die();
                    }

                    function eLearning_front_end_quiz_controls() {
                        global $wp_query, $post;
                        if ((!isset($_GET['edit']) && !isset($wp_query->query_vars['edit'])) || !current_user_can('edit_posts'))
                            return;

                        $user_id = get_current_user_id();
                        wp_nonce_field('create_quiz' . $user_id, 'security');
                        $course_id = get_post_meta($post->ID, 'eLearning_quiz_course', true);
                        if (isset($course_id) && is_numeric($course_id)) {
                            
                        } else {
                            if (!isset($_GET['id']) || !is_numeric($_GET['id']))
                                return;
                            $course_id = $_GET['id'];
                        }
                        $quiz_dynamic = get_post_meta($post->ID, 'eLearning_quiz_dynamic', true);
                        if (isset($quiz_dynamic) && $quiz_dynamic == 'S')
                            return;
                        ?>
                        <div id="quiz_question_controls">
                            <h3 class="heading"><?php _e('Manage Quiz Questions', 'eLearning-front-end'); ?></h3>


                            <a id="add_question" class="button primary small"><?php _e('ADD QUESTION', 'eLearning-front-end'); ?></a>
                            <ul id="questions">
                                <?php
                                $quiz_questions = eLearning_sanitize(get_post_meta(get_the_ID(), 'eLearning_quiz_questions', false));
                                if (isset($quiz_questions['ques']))
                                    $questions = $quiz_questions['ques'];
                                if (isset($quiz_questions['marks']))
                                    $marks = $quiz_questions['marks'];

                                if (isset($questions))
                                    foreach ($questions as $key => $question) {
                                        echo '<li><strong>' . __('Question ', 'eLearning-front-end') . ($key + 1) . ' : ' . get_the_title($question) . '</strong>
                        <div class="btn-group">
                            <button type="button" class="btn btn-course dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="' . get_permalink($question) . 'edit/" target="_blank" class="edit_unit">' . __('Edit Question', 'eLearning-front-end') . '</a></li>
                                <li><a href="' . get_permalink($question) . '" target="_blank" class="edit_unit">' . __('Preview Question', 'eLearning-front-end') . '</a></li>
                                <li><a class="remove">' . __('Remove', 'eLearning-front-end') . '</a></li>
                                <li><a class="delete">' . __('Delete', 'eLearning-front-end') . '</a></li>
                            </ul>
                        </div>
                        <span>' . __('MARKS : ', 'eLearning-front-end') . $marks[$key] . '<input type="hidden" class="question_marks" value="' . $marks[$key] . '" /></span>
                        <input type="hidden" class="question" value="' . $question . '" />
                    </li>';
                                    }
                                ?>
                            </ul>
                            <ul id="hidden">
                                <li class="new_question">
                                    <select class="question">
                                        <option value="none"><?php _e('None', 'eLearning-front-end'); ?></option>
                                        <option value="add_new"><?php _e('ADD A NEW QUESTION', 'eLearning-front-end'); ?></option>
                                        <?php
                                        $args = array(
                                            'post_type' => 'question',
                                            'numberposts' => -1
                                        );
                                        $args = apply_filters('eLearning_frontend_cpt_query', $args);
                                        $kposts = get_posts($args);
                                        if (is_array($kposts))
                                            foreach ($kposts as $kpost) {
                                                echo '<option value="' . $kpost->ID . '">' . $kpost->post_title . '</option>';
                                            }
                                        ?>
                                    </select>
                                    <a class="rem right"><i class="icon-x"></i></a>
                                    <span>
                                        <?php _e('Marks : ', 'eLearning-front-end'); ?><input type="number" class="small_box question_marks" value="0" />
                                    </span>
                                    <h3 class="new_q"><?php _e('Question Title : ', 'eLearning-front-end'); ?><span>
                                            <input type="text" class="question_title large_box" placeholder="<?php _e('Enter title for reference', 'eLearning-front-end'); ?>"  />
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-course dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a class="publish"><?php _e('Publish Question', 'eLearning-front-end'); ?></a></li>
                                                    <li><a class="remove"><?php _e('Remove', 'eLearning-front-end'); ?></a></li>
                                                </ul>
                                            </div>
                                        </span></h3>
                                </li>
                            </ul>
                            <a data-quiz="<?php echo get_the_ID(); ?>" class="save_quiz_settings button hero"><?php _e('SAVE QUIZ SETTINGS', 'eLearning-front-end'); ?></a>
                        </div>
                        <?php
                    }

                    function eLearning_front_end_quiz_meta_controls() {
                        global $wp_query, $post;
                        if ((!isset($_GET['edit']) && !isset($wp_query->query_vars['edit'])) || !current_user_can('edit_posts'))
                            return;
                        $user_id = get_current_user_id();

                        $course_id = get_post_meta($post->ID, 'eLearning_quiz_course', true);
                        if (isset($course_id) && is_numeric($course_id)) {
                            
                        } else {
                            if (!isset($_GET['id']) || !is_numeric($_GET['id']))
                                return;

                            $course_id = $_GET['id'];
                        }


                        $quiz_settings = array(
                            'eLearning_subtitle' => __('Enter a Quiz sub-title', 'eLearning-front-end'),
                            'eLearning_quiz_course' => $course_id,
                            'eLearning_duration' => 10,
                            'eLearning_quiz_auto_evaluate' => 'H',
                            'eLearning_quiz_dynamic' => 'H',
                            'eLearning_quiz_tags' => array(),
                            'eLearning_quiz_number_questions' => 0,
                            'eLearning_quiz_marks_per_question' => 0,
                            'eLearning_quiz_retakes' => 0,
                            'eLearning_quiz_random' => 'H',
                            'eLearning_quiz_message' => __('Enter a Quiz Completion message', 'eLearning-front-end')
                        );

                        $quiz_settings = apply_filters('eLearning_front_end_quiz_settings', $quiz_settings);
                        ?>
                        <div class="eLearning_front_end_wrapper">
                            <h3 class="heading"><?php _e('Quiz Settings', 'eLearning-front-end'); ?></h3>
                            <article class="live-edit" data-model="article" data-id="1" data-url="/articles">
                                <ul class="settings_quiz">
                                    <li>
                                        <label><?php _e('QUIZ SUB-TITLE', 'eLearning-front-end'); ?></label>
                                        <div id="eLearning_subtitle" data-editable="true" data-name="content" data-max-length="250" data-text-options="true">
                                            <p><?php
                                                echo strip_tags($quiz_settings['eLearning_subtitle'], '<br><br/>');
                                                ;
                                                ?></p>
                                        </div>
                                    </li><li>    
                                        <label><?php _e('CONNECTED COURSE', 'eLearning-front-end'); ?></label>
                                        <select class="chosen" id="eLearning_quiz_course">
                                            <?php
                                            $args = array(
                                                'post_type' => 'course',
                                                'numberposts' => -1
                                            );
                                            $args = apply_filters('eLearning_frontend_cpt_query', $args);
                                            $kposts = get_posts($args);
                                            if (is_array($kposts))
                                                foreach ($kposts as $kpost) {
                                                    echo '<option value="' . $kpost->ID . '" ' . selected($quiz_settings['eLearning_quiz_course'], $kpost->ID) . '>' . $kpost->post_title . '</option>';
                                                }
                                            ?>
                                        </select>
                                    </li><li><label><?php _e('QUIZ DURATION', 'eLearning-front-end'); ?></label>
                                        <input type="number" class="small_box" id="eLearning_duration" value="<?php echo $quiz_settings['eLearning_duration']; ?>" /> <?php
                                        $quiz_duration_parameter = apply_filters('eLearning_quiz_duration_parameter', 60);
                                        echo calculate_duration_time($quiz_duration_parameter);
                                        ?>
                                    </li>
                                    <li><label><?php _e('QUIZ EVALUATION', 'eLearning-front-end'); ?></label>
                                        <div class="switch">
                                            <input type="radio" class="switch-input eLearning_quiz_auto_evaluate" name="eLearning_quiz_auto_evaluate" value="S" id="quiz_auto_evaluate_on" <?php checked($quiz_settings['eLearning_quiz_auto_evaluate'], 'S'); ?>>
                                            <label for="quiz_auto_evaluate_on" class="switch-label switch-label-off"><?php _e('AUTO', 'eLearning-front-end'); ?></label>
                                            <input type="radio" class="switch-input eLearning_quiz_auto_evaluate" name="eLearning_quiz_auto_evaluate" value="H" id="quiz_auto_evaluate_off" <?php checked($quiz_settings['eLearning_quiz_auto_evaluate'], 'H'); ?>>
                                            <label for="quiz_auto_evaluate_off" class="switch-label switch-label-on"><?php _e('MANUAL', 'eLearning-front-end'); ?></label>
                                            <span class="switch-selection"></span>
                                        </div>
                                    </li>
                                    <li><label><?php _e('QUIZ QUESTIONS TYPE', 'eLearning-front-end'); ?></label>
                                        <div class="switch">
                                            <input type="radio" class="switch-input eLearning_quiz_dynamic" name="eLearning_quiz_dynamic" value="S" id="quiz_dynamic_on" <?php checked($quiz_settings['eLearning_quiz_dynamic'], 'S'); ?>>
                                            <label for="quiz_dynamic_on" class="switch-label switch-label-off"><?php _e('DYNAMIC', 'eLearning-front-end'); ?></label>
                                            <input type="radio" class="switch-input eLearning_quiz_dynamic" name="eLearning_quiz_dynamic" value="H" id="quiz_dynamic_off" <?php checked($quiz_settings['eLearning_quiz_dynamic'], 'H'); ?>>
                                            <label for="quiz_dynamic_off" class="switch-label switch-label-on"><?php _e('STATIC', 'eLearning-front-end'); ?></label>
                                            <span class="switch-selection"></span>
                                        </div>
                                    </li> 
                                    <li class="dynamic <?php echo (($quiz_settings['eLearning_quiz_dynamic'] == 'S') ? '' : 'hide_it'); ?>"><label><?php _e('SELECT QUESTION TAGS', 'eLearning-front-end'); ?></label>
                                        <select id="eLearning_quiz_tags" class="chosen" multiple>
                                            <?php
                                            $terms = get_terms('question-tag', array('fields' => 'id=>name'));

                                            if (isset($terms) && is_array($terms))
                                                foreach ($terms as $key => $term)
                                                    echo '<option value="' . $key . '" ' . (in_array($key, $quiz_settings['eLearning_quiz_tags']) ? 'SELECTED' : '') . '>' . $term . '</option>';
                                            ?>
                                        </select>
                                    </li>
                                    <li class="dynamic <?php echo (($quiz_settings['eLearning_quiz_dynamic'] == 'S') ? '' : 'hide_it'); ?>"><label><?php _e('NUMER OF QUESTIONS', 'eLearning-front-end'); ?></label>
                                        <input type="number" class="small_box" id="eLearning_quiz_number_questions" value="<?php echo $quiz_settings['eLearning_quiz_number_questions']; ?>" />
                                    </li>
                                    <li class="dynamic <?php echo (($quiz_settings['eLearning_quiz_dynamic'] == 'S') ? '' : 'hide_it'); ?>"><label><?php _e('MARKS PER QUESTION', 'eLearning-front-end'); ?></label>
                                        <input type="number" class="small_box" id="eLearning_quiz_marks_per_question" value="<?php echo $quiz_settings['eLearning_quiz_marks_per_question']; ?>" />
                                    </li>
                                    <li><label><?php _e('QUIZ RETAKES', 'eLearning-front-end'); ?></label>
                                        <input type="number" id="eLearning_quiz_retakes" class="small_box" value="<?php echo $quiz_settings['eLearning_quiz_retakes']; ?>" /><?php _e(' Retakes', 'eLearning-front-end'); ?>
                                    </li>
                                    <li><label><?php _e('QUESTIONS ORDER', 'eLearning-front-end'); ?></label>
                                        <div class="switch">
                                            <input type="radio" class="switch-input eLearning_quiz_random" name="eLearning_quiz_random" value="S" id="quiz_random_on" <?php checked($quiz_settings['eLearning_quiz_random'], 'S'); ?>>
                                            <label for="quiz_random_on" class="switch-label switch-label-off"><?php _e('RANDOM', 'eLearning-front-end'); ?></label>
                                            <input type="radio" class="switch-input eLearning_quiz_random" name="eLearning_quiz_random" value="H" id="quiz_random_off" <?php checked($quiz_settings['eLearning_quiz_random'], 'H'); ?>>
                                            <label for="quiz_random_off" class="switch-label switch-label-on"><?php _e('SEQUENTIAL', 'eLearning-front-end'); ?></label>
                                            <span class="switch-selection"></span>
                                        </div>
                                    </li> 
                                    <li><label><?php _e('QUIZ COMPLETION MESSAGE', 'eLearning-front-end'); ?></label>
                                        <div id="eLearning_quiz_message" data-editable="true" data-name="content" data-max-length="500" data-text-options="true">
                                            <p><?php echo $quiz_settings['eLearning_quiz_message']; ?></p>
                                        </div>
                                    </li>
                                    <?php
                                    do_action('eLearning_front_end_quiz_settings_action', $quiz_settings);
                                    ?>
                                </ul>
                            </article>
                            <a data-quiz="<?php echo get_the_ID(); ?>" class="save_quiz_settings button hero"><?php _e('SAVE QUIZ SETTINGS', 'eLearning-front-end'); ?></a>
                        </div>
                        <?php
                    }

                    function create_question() {
                        $user_id = get_current_user_id();
                        $question_title = stripcslashes($_POST['title']);
                        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_quiz' . $user_id) || !current_user_can('edit_posts')) {
                            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                            die();
                        }

                        $question_settings = array(
                            'post_title' => $question_title,
                            'post_content' => sprintf(__('Add Content for %s', 'eLearning-front-end'), $question_title),
                            'post_status' => 'publish',
                            'post_type' => 'question',
                        );
                        $question_settings = apply_filters('eLearning_front_end_question_vars', $question_settings);
                        $question_id = wp_insert_post($question_settings);

                        echo '<strong>' . __('Question ', 'eLearning-front-end') . ' : ' . $question_title . '</strong>
                        <div class="btn-group right">
                            <button type="button" class="btn btn-course dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="' . get_permalink($question_id) . 'edit/" target="_blank" class="edit_unit">' . __('Edit Question', 'eLearning-front-end') . '</a></li>
                                <li><a class="remove">' . __('Remove', 'eLearning-front-end') . '</a></li>
                                <li><a class="delete">' . __('Delete', 'eLearning-front-end') . '</a></li>
                            </ul>
                        </div>
                        <span>' . __('MARKS : ', 'eLearning-front-end') . '<input type="number" class="question_marks small_box" value="0" /></span>
                        <input type="hidden" class="question" value="' . $question_id . '" />
                    ';

                        //Linkage 
                        $linkage = eLearning_get_option('linkage');
                        if (isset($linkage) && $linkage) {
                            $quiz_id = $_POST['quiz_id'];
                            $quiz_linkage = wp_get_post_terms($quiz_id, 'linkage', array("fields" => "names"));
                            if (isset($quiz_linkage) && is_array($quiz_linkage))
                                wp_set_post_terms($question_id, $quiz_linkage, 'linkage');
                        }

                        die();
                    }

                    function save_quiz_settings() {
                        $user_id = get_current_user_id();
                        $quiz_id = $_POST['quiz_id'];

                        $eLearning_subtitle = $_POST['eLearning_subtitle'];
                        $eLearning_quiz_course = $_POST['eLearning_quiz_course'];
                        $eLearning_duration = $_POST['eLearning_duration'];
                        $eLearning_quiz_auto_evaluate = $_POST['eLearning_quiz_auto_evaluate'];
                        $eLearning_quiz_retakes = $_POST['eLearning_quiz_retakes'];
                        $eLearning_quiz_message = $_POST['eLearning_quiz_message'];
                        $eLearning_quiz_dynamic = $_POST['eLearning_quiz_dynamic'];
                        $eLearning_quiz_tags = $_POST['eLearning_quiz_tags'];
                        $eLearning_quiz_number_questions = $_POST['eLearning_quiz_number_questions'];
                        $eLearning_quiz_random = $_POST['eLearning_quiz_random'];
                        $eLearning_quiz_marks_per_question = $_POST['eLearning_quiz_marks_per_question'];

                        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_quiz' . $user_id) || !current_user_can('edit_posts')) {
                            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                            die();
                        }

                        if (!is_numeric($quiz_id)) {
                            _e('Invalid Quiz', 'eLearning-front-end');
                            die();
                        }
                        update_post_meta($quiz_id, 'eLearning_subtitle', $eLearning_subtitle);
                        update_post_meta($quiz_id, 'eLearning_quiz_course', $eLearning_quiz_course);
                        update_post_meta($quiz_id, 'eLearning_duration', $eLearning_duration);
                        update_post_meta($quiz_id, 'eLearning_quiz_auto_evaluate', $eLearning_quiz_auto_evaluate);
                        update_post_meta($quiz_id, 'eLearning_quiz_retakes', $eLearning_quiz_retakes);
                        update_post_meta($quiz_id, 'eLearning_quiz_message', $eLearning_quiz_message);

                        update_post_meta($quiz_id, 'eLearning_quiz_dynamic', $eLearning_quiz_dynamic);
                        update_post_meta($quiz_id, 'eLearning_quiz_tags', $eLearning_quiz_tags);
                        update_post_meta($quiz_id, 'eLearning_quiz_number_questions', $eLearning_quiz_number_questions);
                        update_post_meta($quiz_id, 'eLearning_quiz_marks_per_question', $eLearning_quiz_marks_per_question);
                        update_post_meta($quiz_id, 'eLearning_quiz_random', $eLearning_quiz_random);

                        $objquestions = json_decode(stripslashes($_POST['questions']));
                        $questions = array();
                        if (is_array($objquestions) && isset($objquestions))
                            foreach ($objquestions as $c) {
                                $questions['ques'][] = $c->ques;
                                $questions['marks'][] = $c->marks;
                            }

                        update_post_meta($quiz_id, 'eLearning_quiz_questions', $questions);

                        if (isset($_POST['extras'])) {
                            $obj = json_decode(stripslashes($_POST['extras']));
                            $marks = array();
                            if (is_array($obj) && isset($obj)) {
                                foreach ($obj as $extra) {
                                    update_post_meta($quiz_id, $extra->element, $extra->value);
                                }
                            }
                        }

                        do_action('eLearning_front_end_save_quiz_settings_extras', $quiz_id);

                        _e('Quiz Settings saved', 'eLearning-front-end');

                        die();
                    }

                    function eLearning_front_end_quiz_settings($quiz_settings) {
                        $quiz_id = get_the_ID();
                        foreach ($quiz_settings as $key => $value) {
                            if ($key == 'eLearning_quiz_tags') {
                                $value = eLearning_sanitize(get_post_meta($quiz_id, $key, false));
                            } else
                                $value = get_post_meta($quiz_id, $key, true);

                            if (isset($value) && $value)
                                $quiz_settings[$key] = $value;
                        }
                        return $quiz_settings;
                    }

                    function delete_question() {
                        $user_id = get_current_user_id();
                        $id = stripslashes($_POST['id']);

                        if (!isset($id) || !is_numeric($id) && $id == '') {
                            _e('Can not delete', 'eLearning-front-end');
                            die();
                        }

                        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'create_quiz' . $user_id) || !current_user_can('edit_posts')) {
                            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                            die();
                        }

                        $the_post = get_post($id);
                        if ($the_post->post_author != $user_id && !current_user_can('manage_options')) {
                            _e('Instructor can not delete this unit/quiz', 'eLearning-front-end');
                            die();
                        }

                        $status = wp_trash_post($id);
                        if ($status) {
                            echo 1;
                        } else {
                            _e('Unable to delete', 'eLearning-front-end');
                        }
                        die();
                    }

                    function eLearning_front_end_question_controls() {
                        global $wp_query;
                        $user_id = get_current_user_id();
                        if ((!isset($_GET['edit']) && !isset($wp_query->query_vars['edit'])) || !current_user_can('edit_posts'))
                            return;

                        $question_settings = array(
                            'eLearning_question_type' => 'single',
                            'eLearning_question_options' => '',
                            'eLearning_question_answer' => 0,
                            'eLearning_question_hint' => '',
                            'eLearning_question_explaination' => ''
                        );
                        $question_settings = apply_filters('eLearning_front_end_question_settings', $question_settings);

                        $question_types = apply_filters('eLearning_question_types', array(
                            array('label' => __('True or False', 'eLearning-front-end'), 'value' => 'truefalse'),
                            array('label' => __('Multiple Choice', 'eLearning-front-end'), 'value' => 'single'),
                            array('label' => __('Multiple Correct', 'eLearning-front-end'), 'value' => 'multiple'),
                            array('label' => __('Sort Answers', 'eLearning-front-end'), 'value' => 'sort'),
                            array('label' => __('Match Answers', 'eLearning-front-end'), 'value' => 'match'),
                            array('label' => __('Fill in the Blank', 'eLearning-front-end'), 'value' => 'fillblank'),
                            array('label' => __('Dropdown Select', 'eLearning-front-end'), 'value' => 'select'),
                            array('label' => __('Small Text', 'eLearning-front-end'), 'value' => 'smalltext'),
                            array('label' => __('Large Text', 'eLearning-front-end'), 'value' => 'largetext')
                        ));
                        ?>
                        <div class="eLearning_front_end_wrapper">
                            <h3 class="heading"><?php _e('QUESTION SETTINGS', 'eLearning-front-end'); ?></h3>
                            <ul class="question_settings">
                                <li><h3><?php _e('Question Type', 'eLearning-front-end'); ?><span>
                                            <select id="eLearning_question_type" class="chosen">
                                                <?php
                                                foreach ($question_types as $question_type) {
                                                    echo '<option value="' . $question_type['value'] . '" ' . selected($question_settings['eLearning_question_type'], $question_type['value']) . '>' . $question_type['label'] . '</option>';
                                                }
                                                ?>
                                            </select></span>
                                    </h3>
                                </li>
                                <li class="optionli">
                                    <a id="add_option" class="button small primary"><?php _e('ADD OPTION', 'eLearning-front-end'); ?></a>
                                    <ul class="eLearning_question_options <?php echo $question_settings['eLearning_question_type']; ?>">
                                        <?php
                                        if (isset($question_settings['eLearning_question_answer'])) {
                                            $answer = explode(',', $question_settings['eLearning_question_answer']);
                                        }

                                        if (isset($question_settings['eLearning_question_options']) && is_array($question_settings['eLearning_question_options'])) {
                                            foreach ($question_settings['eLearning_question_options'] as $key => $option) {
                                                echo '<li ' . (in_array(($key + 1), $answer) ? 'class="selected"' : '') . '><span class="tip" title="' . __('Click to select as Correct answer', 'eLearning-front-end') . '">' . ($key + 1) . '</span><input type="text" class="option very_large_box" value="' . $option . '" /><a class="rem"><i class="icon-x"></i></a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                    <ul class="hidden">
                                        <li><span></span><input type="text" class="option very_large_box" /><a class="rem"><i class="icon-x"></i></a></li>
                                    </ul>
                                </li>
                                <li><h3><?php _e('Correct Answer', 'eLearning-front-end'); ?></h3><input type="text" id="eLearning_question_answer" class="very_large_box" value="<?php echo $question_settings['eLearning_question_answer']; ?>" /></li>
                                <li><h3><?php _e('Answer Hint', 'eLearning-front-end'); ?></h3><input type="text" id="eLearning_question_hint" class="very_large_box" value="<?php echo $question_settings['eLearning_question_hint']; ?>" /></li>
                                <li><h3><?php _e('Answer Explanation', 'eLearning-front-end'); ?></h3>
                                    <article class="live-edit" data-model="article" data-id="1" data-url="/articles">
                                        <div id="eLearning_question_explaination" data-editable="true" data-name="content" data-max-length="350" data-text-options="true">
                                            <?php echo $question_settings['eLearning_question_explaination']; ?>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                            <a id="save_question_settings" class="button hero"><?php _e('SAVE QUESTION SETTINGS', 'eLearning-front-end'); ?></a>
                            <input type="hidden" value="<?php echo get_the_ID(); ?>" id="question_id" />
                            <?php
                            wp_nonce_field('save_question' . $user_id, 'security');
                            ?>
                        </div>
                        <?php
                    }

                    function eLearning_front_end_question_settings($question_settings) {

                        foreach ($question_settings as $key => $value) {
                            if ($key == 'eLearning_question_options')
                                $question_settings[$key] = eLearning_sanitize(get_post_meta(get_the_ID(), $key, false));
                            else
                                $question_settings[$key] = get_post_meta(get_the_ID(), $key, true);
                        }

                        return $question_settings;
                    }

                    function save_question() {
                        $qid = $_POST['id'];

                        $question_settings = array(
                            'eLearning_question_type' => 'single',
                            'eLearning_question_options' => '',
                            'eLearning_question_answer' => 0,
                            'eLearning_question_hint' => '',
                            'eLearning_question_explaination' => ''
                        );
                        $question_settings = apply_filters('eLearning_front_end_question_settings', $question_settings);

                        $eLearning_question_options = array();
                        $user_id = get_current_user_id();
                        if (!isset($qid) || !is_numeric($qid) && $qid == '') {
                            _e('Unable to save, incorrect question', 'eLearning-front-end');
                            die();
                        }

                        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'save_question' . $user_id) || !current_user_can('edit_posts')) {
                            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                            die();
                        }

                        foreach ($question_settings as $key => $value) { //print_r($key.' = '.$value.' != '.$_POST[$key]);
                            if ($value !== $_POST[$key] && $_POST[$key] != '') {
                                if ($key != 'eLearning_question_options') {
                                    update_post_meta($qid, $key, $_POST[$key]);
                                }
                            }
                        }

                        $objcurriculum = json_decode(stripslashes($_POST['eLearning_question_options']));
                        if (is_array($objcurriculum) && isset($objcurriculum))
                            foreach ($objcurriculum as $c) {
                                $eLearning_question_options[] = $c->option;
                            }

                        update_post_meta($qid, 'eLearning_question_options', $eLearning_question_options);

                        _e('Question settings successfully saved', 'eLearning-front-end');
                        die();
                    }

                    function create_assignment() {
                        $user_id = get_current_user_id();
                        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'save_unit' . $user_id) || !current_user_can('edit_posts')) {
                            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                            die();
                        }
                        $unit_id = $_POST['unit_id'];
                        $title = $_POST['title'];

                        if (!isset($unit_id) || !is_numeric($unit_id) || $unit_id == '' || $title == '' || strlen($title) < 5) {
                            _e('Unable to save, incorrect unit', 'eLearning-front-end');
                            die();
                        }

                        $assignment_settings = array(
                            'post_title' => $title,
                            'post_content' => $title,
                            'post_status' => 'publish',
                            'post_type' => 'eLearning-assignment',
                        );
                        $assignment_settings = apply_filters('eLearning_front_end_assignment_vars', $assignment_settings);
                        $assignment_id = wp_insert_post($assignment_settings);

                        $assignment_array = array(
                            'id' => $assignment_id,
                            'title' => $title,
                            'link' => get_permalink($assignment_id)
                        );
                        echo json_encode($assignment_array);
                        die();
                    }

                    function eLearning_front_end_assignment_controls() {
                        $user_id = get_current_user_id();

                        global $wp_query;

                        if ((!isset($_GET['edit']) && !isset($wp_query->query_vars['edit'])) || !current_user_can('edit_posts'))
                            return;

                        $assignment_settings = array(
                            'eLearning_subtitle' => 'Enter a Subtitle',
                            'eLearning_assignment_marks' => 0,
                            'eLearning_assignment_duration' => 0,
                            'eLearning_assignment_evaluation' => 'H',
                            'eLearning_assignment_course' => '',
                            'eLearning_assignment_submission_type' => 'upload',
                            'eLearning_attachment_type' => '',
                            'eLearning_attachment_size' => ''
                        );
                        $assignment_settings = apply_filters('eLearning_front_end_assignment_settings', $assignment_settings);
                        ?>
                        <div class="eLearning_front_end_wrapper">
                            <h3 class="heading"><?php _e('ASSIGNMENT SETTINGS', 'eLearning-front-end'); ?></h3>
                            <article class="live-edit" data-model="article" data-id="1" data-url="/articles">
                                <ul class="assignment_settings">
                                    <li>
                                        <label><?php _e('ASSIGNMENT SUB-TITLE', 'eLearning-front-end'); ?></label>
                                        <div id="eLearning_subtitle" data-editable="true" data-name="content" data-max-length="250" data-text-options="true">
                                            <p><?php echo $assignment_settings['eLearning_subtitle']; ?></p>
                                        </div>
                                    </li>
                                    <li>
                                        <label><?php _e('ASSIGNMENT MARKS', 'eLearning-front-end'); ?></label>
                                        <h3><?php _e('Assignment Maximum Marks', 'eLearning-front-end'); ?></h3><input type="number" class="small_box" id="eLearning_assignment_marks" value="<?php echo $assignment_settings['eLearning_assignment_marks']; ?>" /> <?php _e('MARKS', 'eLearning-front-end'); ?>
                                    </li>
                                    <li>
                                        <label><?php _e('ASSIGNMENT DURATION', 'eLearning-front-end'); ?></label>
                                        <h3><?php _e('Enter Assignment Duration', 'eLearning-front-end'); ?></h3><input type="number" class="small_box" id="eLearning_assignment_duration" value="<?php echo $assignment_settings['eLearning_assignment_duration']; ?>" /> <?php
                                        $assignment_duration_parameter = apply_filters('eLearning_assignment_duration_parameter', 86400);
                                        echo calculate_duration_time($assignment_duration_parameter);
                                        ?>
                                    </li>
                                    <li><label><?php _e('ASSIGNMENT EVALUATION', 'eLearning-front-end'); ?></label>
                                        <h3><?php _e('Include in Course Evaluation', 'eLearning-front-end'); ?></h3>
                                        <div class="switch">
                                            <input type="radio" class="switch-input eLearning_assignment_evaluation" name="eLearning_assignment_evaluation" value="S" id="eLearning_assignment_evaluate_on" <?php checked($assignment_settings['eLearning_assignment_evaluation'], 'S'); ?>>
                                            <label for="eLearning_assignment_evaluate_on" class="switch-label switch-label-off"><?php _e('YES', 'eLearning-front-end'); ?></label>
                                            <input type="radio" class="switch-input eLearning_assignment_evaluation" name="eLearning_assignment_evaluation" value="H" id="eLearning_assignment_evaluate_off" <?php checked($assignment_settings['eLearning_assignment_evaluation'], 'H'); ?>>
                                            <label for="eLearning_assignment_evaluate_off" class="switch-label switch-label-on"><?php _e('NO', 'eLearning-front-end'); ?></label>
                                            <span class="switch-selection"></span>
                                        </div>
                                    </li>
                                    <li id="assignment_course" <?php echo (($assignment_settings['eLearning_assignment_evaluation'] == 'H') ? 'class="hide"' : ''); ?>>
                                        <label><?php _e('ASSIGNMENT COURSE', 'eLearning-front-end'); ?></label>
                                        <h3><?php _e('Select Assignment Course', 'eLearning-front-end'); ?></h3>
                                        <select id="eLearning_assignment_course" class="chosen">
                                            <option value=""><?php _e('None', 'eLearning-front-end'); ?></option>
                                            <?php
                                            $args = array(
                                                'post_type' => 'course',
                                                'numberposts' => -1
                                            );
                                            $args = apply_filters('eLearning_frontend_cpt_query', $args);
                                            $kposts = get_posts($args);
                                            if (is_Array($kposts))
                                                foreach ($kposts as $kpost) {
                                                    echo '<option value="' . $kpost->ID . '" ' . selected($assignment_settings['eLearning_assignment_course'], $kpost->ID) . '>' . $kpost->post_title . '</option>';
                                                }
                                            ?>
                                        </select>
                                    </li>
                                    <li><h3><?php _e('ASSIGNMENT SUBMISISON TYPE', 'eLearning-front-end'); ?></h3>
                                        <select id="eLearning_assignment_submission_type" class="chosen">
                                            <option value="upload" <?php selected($assignment_settings['eLearning_assignment_submission_type'], 'upload'); ?>><?php _e('UPLOAD', 'eLearning-front-end'); ?></option>
                                            <option value="textarea" <?php selected($assignment_settings['eLearning_assignment_submission_type'], 'textarea'); ?>><?php _e('TEXTAREA', 'eLearning-front-end'); ?></option>
                                        </select>
                                    </li>
                                    <li id="attachment_type"><h3><?php _e('ATTACHMENT TYPE', 'eLearning-front-end'); ?></h3>
                                        <select id="eLearning_attachment_type" class="chosen" multiple>
                                            <?php
                                            $attachment_types = array(
                                                array('value' => 'JPG', 'label' => 'JPG'),
                                                array('value' => 'GIF', 'label' => 'GIF'),
                                                array('value' => 'PNG', 'label' => 'PNG'),
                                                array('value' => 'PDF', 'label' => 'PDF'),
                                                array('value' => 'DOC', 'label' => 'DOC'),
                                                array('value' => 'DOCX', 'label' => 'DOCX'),
                                                array('value' => 'PPT', 'label' => 'PPT'),
                                                array('value' => 'PPTX', 'label' => 'PPTX'),
                                                array('value' => 'PPS', 'label' => 'PPS'),
                                                array('value' => 'PPSX', 'label' => 'PPSX'),
                                                array('value' => 'ODT', 'label' => 'ODT'),
                                                array('value' => 'XLS', 'label' => 'XLS'),
                                                array('value' => 'XLSX', 'label' => 'XLSX'),
                                                array('value' => 'MP3', 'label' => 'MP3'),
                                                array('value' => 'M4A', 'label' => 'M4A'),
                                                array('value' => 'OGG', 'label' => 'OGG'),
                                                array('value' => 'WAV', 'label' => 'WAV'),
                                                array('value' => 'WMA', 'label' => 'WMA'),
                                                array('value' => 'MP4', 'label' => 'MP4'),
                                                array('value' => 'M4V', 'label' => 'M4V'),
                                                array('value' => 'MOV', 'label' => 'MOV'),
                                                array('value' => 'WMV', 'label' => 'WMV'),
                                                array('value' => 'AVI', 'label' => 'AVI'),
                                                array('value' => 'MPG', 'label' => 'MPG'),
                                                array('value' => 'OGV', 'label' => 'OGV'),
                                                array('value' => '3GP', 'label' => '3GP'),
                                                array('value' => '3G2', 'label' => '3G2'),
                                                array('value' => 'FLV', 'label' => 'FLV'),
                                                array('value' => 'WEBM', 'label' => 'WEBM'),
                                                array('value' => 'APK', 'label' => 'APK '),
                                                array('value' => 'RAR', 'label' => 'RAR'),
                                                array('value' => 'ZIP', 'label' => 'ZIP')
                                            );

                                            foreach ($attachment_types as $attachment_type) {
                                                echo '<option value="' . $attachment_type['value'] . '" ' . ((is_array($assignment_settings['eLearning_attachment_type']) && in_array($attachment_type['value'], $assignment_settings['eLearning_attachment_type'])) ? 'selected' : '') . '>' . $attachment_type['label'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </li>
                                    <li>
                                        <label><?php _e('Attachment Size', 'eLearning-front-end'); ?></label>
                                        <h3><?php _e('Maximum attachment size', 'eLearning-front-end'); ?></h3><input type="number" class="small_box" id="eLearning_attachment_size" value="<?php echo $assignment_settings['eLearning_attachment_size']; ?>" /> <?php _e(' MBs', 'eLearning-front-end'); ?>
                                    </li>
                                </ul></article>
                            <a id="save_assignment_settings" class="course_button button full"><?php _e('SAVE SETTINGS', 'eLearning-front-end'); ?></a>
                            <input type="hidden" value="<?php echo get_the_ID(); ?>" id="assignment_id" />
                            <?php
                            wp_nonce_field('save-assignment-settings' . $user_id, 'assignment_security');
                            ?>
                        </div>
                        <?php
                    }

                    function eLearning_front_end_assignment_settings($assignment_settings) {
                        $id = get_the_ID();
                        if (is_array($assignment_settings))
                            foreach ($assignment_settings as $key => $settings) {
                                if ($key == 'eLearning_attachment_type') {
                                    $val = get_post_meta($id, $key, false);
                                    if (isset($val) && is_array($val))
                                        $assignment_settings[$key] = eLearning_sanitize($val);
                                }else {
                                    $val = get_post_meta($id, $key, true);
                                    if (isset($val) && $val) {
                                        $assignment_settings[$key] = $val;
                                    }
                                }
                            }
                        return $assignment_settings;
                    }

                    function save_assignment_settings() {
                        $user_id = get_current_user_id();

                        $assignment_id = $_POST['assignment_id'];
                        $assignment_settings['eLearning_subtitle'] = $_POST['eLearning_subtitle'];
                        $assignment_settings['eLearning_assignment_marks'] = $_POST['eLearning_assignment_marks'];
                        $assignment_settings['eLearning_assignment_duration'] = $_POST['eLearning_assignment_duration'];
                        $assignment_settings['eLearning_assignment_evaluation'] = $_POST['eLearning_assignment_evaluation'];
                        $assignment_settings['eLearning_assignment_course'] = $_POST['eLearning_assignment_course'];
                        $assignment_settings['eLearning_assignment_submission_type'] = $_POST['eLearning_assignment_submission_type'];
                        $assignment_settings['eLearning_attachment_type'] = json_decode(stripslashes($_POST['eLearning_attachment_type']));
                        $assignment_settings['eLearning_attachment_size'] = $_POST['eLearning_attachment_size'];


                        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'save-assignment-settings' . $user_id) || !current_user_can('edit_posts')) {
                            _e('Security check Failed. Contact Administrator.', 'eLearning-front-end');
                            die();
                        }

                        if (!is_numeric($assignment_id)) {
                            _e('Invalid details', 'eLearning-front-end');
                            die();
                        }

                        $the_post = get_post($assignment_id);
                        if ($the_post->post_author != $user_id && !current_user_can('manage_options')) { // Instructor and Admin check
                            _e('Invalid Assignment author', 'eLearning-front-end');
                            die();
                        }


                        foreach ($assignment_settings as $key => $setting) {
                            update_post_meta($assignment_id, $key, $setting);
                        }

                        _e('SETTINGS SAVED', 'eLearning-front-end');

                        die();
                    }

                    function eLearning_unit_upload_zip_controls() {
                        global $wp_query;
                        if (!isset($_GET['edit']) && !isset($wp_query->query_vars['edit']))
                            return;

                        if (current_user_can('edit_posts'))
                            echo '<a href="#" id="upload_zip_button" class="button primary small" data-admin-url="' . admin_url() . '">' . __('ADD PACKAGE', 'eLearning-front-end') . '</a>';
                    }

                    function calculate_duration_time($seconds) {
                        switch ($seconds) {
                            case 1: $return = __('Seconds', 'eLearning-front-end');
                                break;
                            case 60: $return = __('Minutes', 'eLearning-front-end');
                                break;
                            case 3600: $return = __('Hours', 'eLearning-front-end');
                                break;
                            case 86400: $return = __('Days', 'eLearning-front-end');
                                break;
                            case 604800: $return = __('Weeks', 'eLearning-front-end');
                                break;
                            case 2592000: $return = __('Months', 'eLearning-front-end');
                                break;
                            case 31104000: $return = __('Years', 'eLearning-front-end');
                                break;
                            default:
                                $return = apply_filters('eLearning_calculation_duration_default', $return, $seconds);
                                break;
                        }
                        return $return;
                    }

                    function show_pricing($true) {
                        if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) || (function_exists('is_plugin_active_for_network') && is_plugin_active_for_network('woocommerce/woocommerce.php')))
                            return 1;
                        if (in_array('paid-memberships-pro/paid-memberships-pro.php', apply_filters('active_plugins', get_option('active_plugins'))) || (function_exists('is_plugin_active_for_network') && is_plugin_active_for_network('paid-memberships-pro/paid-memberships-pro.php')))
                            return 1;
                        if (in_array('mycred/mycred.php', apply_filters('active_plugins', get_option('active_plugins'))) || (function_exists('is_plugin_active_for_network') && is_plugin_active_for_network('mycred/mycred.php')))
                            return 1;
                        return $true;
                    }

                }

                eLearning_Front_End::instance();


                