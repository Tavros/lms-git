<?php

class ELEARNING_Content_Templates {


	public static $instance;
    
    public static function init(){
        if ( is_null( self::$instance ) )
            self::$instance = new ELEARNING_Content_Templates;
        return self::$instance;
    }

	private function __construct(){

	}

	function get_template($post_type,$type){
		switch($post_type){
			case 'question':
				$this->settings = array(
					'post_content'=>'',
					'meta_fields'=> array(
						'eLearning_question_type'=>'',
						'eLearning_question_options'=>'',
						'eLearning_question_answer'=>'',
						'eLearning_question_hint'=>'',
						'eLearning_question_explaination'=>''
						)
					);
				self::get_question_templates($type);
			break;
		}

		return $this->settings;
	}

	function get_question_templates($type){

		switch($type){
			case 'single':
				$this->settings['post_content'] = __('Question Statement : Which is the largest continent in the World ?','eLearning-front-end');
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_options'=>array(__('Asia','eLearning-front-end'),__('America','eLearning-front-end'),__('Europe','eLearning-front-end'),__('Australia','eLearning-front-end')),
						'eLearning_question_answer'=>'1',
						'eLearning_question_hint'=> __('Continent with Russia','eLearning-front-end'),
						'eLearning_question_explaination'=>__('A continent is one of several very large landmasses on Earth. They are generally identified by convention rather than any strict criteria, with up to seven regions commonly regarded as continents.','eLearning-front-end')
					);
			break;
			case 'multiple':
				$this->settings['post_content'] = __('Question Statement : This question can have multiple answers.','eLearning-front-end');
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_options'=>array(__('Option 1','eLearning-front-end'),__('Option 2','eLearning-front-end'),__('Option 3','eLearning-front-end'),__('Option 4','eLearning-front-end')),
						'eLearning_question_answer'=>'2,3',
						'eLearning_question_hint'=> __(' The answer to this quesiton is 2,3','eLearning-front-end'),
						'eLearning_question_explaination'=>__('Some explaination to this question.','eLearning-front-end')
					);
			break;
			case 'truefalse':
				$this->settings['post_content'] = __('Question Statement : True and False question type.','eLearning-front-end');
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_answer'=>'1',
						'eLearning_question_hint'=> __(' True','eLearning-front-end'),
						'eLearning_question_explaination'=>__('Some explaination to this question.','eLearning-front-end')
					);
			break;
			case 'select':
				$this->settings['post_content'] = __('Question Statement : Select correct answer out of the following','eLearning-front-end').' [select options="1,2,3"] and another [select options="4,5,6"]';
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_options'=>array(__('Option 1','eLearning-front-end'),__('Option 2','eLearning-front-end'),__('Option 3','eLearning-front-end'),__('Option 4','eLearning-front-end'),__('Option 5','eLearning-front-end'),__('Option 6','eLearning-front-end')),
						'eLearning_question_answer'=>'1|4',
						'eLearning_question_hint'=> __(' Option 1 and Option 4','eLearning-front-end'),
						'eLearning_question_explaination'=>__('Some explaination to this question.','eLearning-front-end')
					);
			break;
			case 'sort':
				$this->settings['post_content'] = __('Question Statement : Arrange the below options in following order: 4,3,2,1','eLearning-front-end');
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_options'=>array(__('Option 1','eLearning-front-end'),__('Option 2','eLearning-front-end'),__('Option 3','eLearning-front-end'),__('Option 4','eLearning-front-end')),
						'eLearning_question_answer'=>'4,3,2,1',
						'eLearning_question_hint'=> '4,3,2,1',
						'eLearning_question_explaination'=>__('Some explaination to this question.','eLearning-front-end')
					);
			break;
			case 'match':
				$this->settings['post_content'] = __('Question Statement : Arrange the below options in following order: 4,3,2,1','eLearning-front-end').'<br />[match]<ul><li>'.__('First Order','eLearning-front-end').'</li><li>'.__('Second Order','eLearning-front-end').'</li><li>'.__('Third order','eLearning-front-end').'</li><li>'.__('Fourth Order','eLearning-front-end').'</li></ul>[/match]';
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_options'=>array(__('Option 1','eLearning-front-end'),__('Option 2','eLearning-front-end'),__('Option 3','eLearning-front-end'),__('Option 4','eLearning-front-end')),
						'eLearning_question_answer'=>'4,3,2,1',
						'eLearning_question_hint'=> '4,3,2,1',
						'eLearning_question_explaination'=>__('Some explaination to this question.','eLearning-front-end')
					);
			break;
			case 'fillblank':
				$this->settings['post_content'] = __('Question Statement : Fill in the blank','eLearning-front-end').' [fillblank] and another [fillblank]';
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_options'=>array(),
						'eLearning_question_answer'=>'somevalue|anothervalue',
						'eLearning_question_hint'=> __('some value','eLearning-front-end'),
						'eLearning_question_explaination'=>__('Some explaination to this question.','eLearning-front-end')
					);
			break;
			case 'smalltext':
				$this->settings['post_content'] = __('Question Statement : Enter the answer in below text box','eLearning-front-end');
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_options'=>'',
						'eLearning_question_answer'=>'some answer',
						'eLearning_question_hint'=> __('some hint','eLearning-front-end'),
						'eLearning_question_explaination'=>__('Some explaination to this question.','eLearning-front-end')
					);
			break;
			case 'survey':
				$this->settings['post_content'] = __('Survey Question : How likely are you to going to pick the number 7 out of 0 to 10 ?','eLearning-front-end');
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_options'=>array('1','2','3','4','5'),
						'eLearning_question_answer'=>'4',
						'eLearning_question_hint'=> __('Help text for survey question. Each choice has marks used to end result','eLearning-front-end'),
						'eLearning_question_explaination'=>__('Scores are connected with the marked answer','eLearning-front-end')
					);
			break;
			case 'largetext':
				$this->settings['post_content'] = __('Question Statement :Enter the answer in below text area','eLearning-front-end');
				$this->settings['meta_fields'] = array(
						'eLearning_question_type'=> $type,
						'eLearning_question_options'=>'',
						'eLearning_question_answer'=>'some answer',
						'eLearning_question_hint'=> 'some hint',
						'eLearning_question_explaination'=>__('Some explaination to this question.','eLearning-front-end')
					);
			break;
		}
	}
}