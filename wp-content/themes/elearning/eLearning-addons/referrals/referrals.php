<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_REFERRALS_URL')) {
    define('ELEARNING_REFERRALS_URL', get_template_directory_uri() . '/eLearning-addons/referrals/');
}

include_once 'includes/functions.php';
include_once 'includes/referrals.php';
add_action('after_setup_theme', 'load_referrals_textdomain');

function load_referrals_textdomain() {
    load_textdomain('eLearning-referrals', dirname(__FILE__) . '/languages/');
}

?>