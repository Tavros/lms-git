<?php

/*
  Plugin Name: eLearning Dashboard
  Plugin URI: http://www.eLearningthemes.com
  Description: Student/Instructor Dashboard for eLearning theme
  Version: 2.6
  Author: eLearningThemes
  Author URI: http://www.eLearningthemes.com
  License: as Per Themeforest GuideLines
 */
/*
  Copyright 2014  eLearningThemes  (email : eLearningthemes@gmail.com)
  eLearning Dashboard is a plugin made for eLearning Theme. This plugin is only meant to work with eLearning and can only be used with eLearning. eLearning Dashboard program is not a free software; you can not redistribute it and/or modify
  Please consult eLearningThemes.com or email us at eLearningthemes@gmail.com for more.
 */

if (!defined('ABSPATH'))
    exit;

if (!defined('eLearning_DASHBOARD_URL')) {
    define('eLearning_DASHBOARD_URL', get_template_directory_uri() . '/eLearning-dashboard');
}

include_once 'includes/functions.php';
include_once 'includes/dashboard.php';

include_once 'includes/widgets/student/activity_widget.php';
include_once 'includes/widgets/student/course_progress.php';
include_once 'includes/widgets/student/contact_users.php';
include_once 'includes/widgets/student/text_widget.php';
include_once 'includes/widgets/student/todo_task.php';
include_once 'includes/widgets/student/student_stats.php';
include_once 'includes/widgets/student/dash_stats.php';
include_once 'includes/widgets/student/notes_discussions.php';
include_once 'includes/widgets/student/my_modules.php';
include_once 'includes/widgets/student/news.php';
include_once 'includes/widgets/instructor/break.php';
include_once 'includes/widgets/instructor/dash_instructor_stats.php';
include_once 'includes/widgets/instructor/instructor_stats.php';
include_once 'includes/widgets/instructor/instructor_commissions.php';
include_once 'includes/widgets/instructor/announcements.php';
include_once 'includes/widgets/instructor/instructing_modules.php';
?>