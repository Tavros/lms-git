<?php

class eLearning_Dashboard{

	function __construct(){
		register_activation_hook(__FILE__, array($this,'activate'));
		register_deactivation_hook(__FILE__,array($this,'deactivate'));
		$this->init();
		//$this->setup_sidebars();
		add_action( 'plugins_loaded', array($this,'init_language' ));
		add_action( 'bp_setup_nav', array($this,'setup_nav' ));
		add_action('bp_after_dashboard_body',array($this,'add_security_parameter'));
		add_filter('eLearning_logged_in_top_menu',array($this,'add_dashboard_in_menu'));
		add_post_type_support( 'news', 'front-end-editor' );
		add_filter('eLearning_course_nav_menu',array($this,'eLearning_course_news_menu'));
		add_filter('eLearning_course_locate_template',array($this,'eLearning_course_news_template'),10,2);
		add_action('eLearning_load_templates',array($this,'eLearning_course_show_news'));
	}

	function init(){ 
		if ( !defined( 'eLearning_DASHBOARD_SLUG' ) )
		define ( 'eLearning_DASHBOARD_SLUG', 'dashboard' );
	}

	function init_language(){
		
	}

	function activate(){

	}

	function deactivate(){
		
	}

	function setup_nav(){
		global $bp;
		$access= 0;
		if(function_exists('bp_is_my_profile'))
			$access = apply_filters('eLearning_student_dashboard_access',bp_is_my_profile());

        bp_core_new_nav_item( array( 
            'name' => __('Dashboard', 'eLearning' ), 
            'slug' => eLearning_DASHBOARD_SLUG, 
            'position' => 4,
            'screen_function' => 'eLearning_dashboard_template', 
            'show_for_displayed_user' => $access
      	) );
	}
	
	function add_security_parameter(){
		wp_nonce_field( 'eLearning_security', 'security');
	}
	function add_dashboard_in_menu($menu){
		$dash_menu['dashboard']=array(
                          'icon' => 'icon-meter',
                          'label' => __('Dashboard','eLearning'),
                          'link' => bp_loggedin_user_domain().eLearning_DASHBOARD_SLUG
                          );
		foreach($menu as $key=>$item){
			$dash_menu[$key]=$item;
		}
		return $dash_menu;
	}
	function eLearning_course_news_menu($links){

		if(function_exists('eLearning_get_option')){
			$show_news = eLearning_get_option('show_news');
			if(isset($show_news) && $show_news){	
				$links['news'] = array(
                        'id' => 'news',
                        'label'=>__('News','eLearning'),
                        'action' => 'news',
                        'link'=>bp_get_course_permalink(),
                    );
      
			}
		}
		return $links;
	}
	function eLearning_course_news_template($template,$action){
      if($action == 'news'){ 
          $template= array(get_template_directory('course/single/plugins.php'));
      }
      return $template;
    }
    function eLearning_course_show_news(){
    	$course_id=get_the_ID();
      	if(!isset($_GET['action']) || ($_GET['action'] != 'news'))
        return;

    	require_once('news_template.php');
    }
}

new eLearning_Dashboard();


?>