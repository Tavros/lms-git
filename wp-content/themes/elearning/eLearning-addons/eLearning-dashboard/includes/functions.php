<?php

function eLearning_get_random_color($i = NULL) {
    $color_array = array(
        '#7266ba',
        '#23b7e5',
        '#f05050',
        '#fad733',
        '#27c24c',
        '#fa7252'
    );
    if (isset($i)) {
        if (isset($color_array[$i]))
            return $color_array[$i];
    }
    $k = array_rand($color_array);
    return $color_array[$k];
}

function eLearning_dashboard_template() {

    if (!is_user_logged_in())
        wp_redirect(site_url());

    $template = 'templates/dashboard';
    global $bp;
    if ($bp->current_component == 'dashboard') {
        wp_enqueue_style('eLearning-dashboard-css', eLearning_DASHBOARD_URL . '/css/eLearning-dashboard.css', array(), '1.0');
        wp_enqueue_script('eLearning-dashboard-js', eLearning_DASHBOARD_URL . '/js/eLearning-dashboard.js', array('jquery', 'jquery-ui-sortable'), '1.0');
        if (is_active_widget(false, false, 'eLearning_instructor_dash_stats', true) || is_active_widget(false, false, 'eLearning_dash_stats', true)) {
            wp_enqueue_script('eLearning-sparkline', eLearning_DASHBOARD_URL . '/js/jquery.sparkline.min.js', array('jquery'), true);
        }
        if (is_active_widget(false, false, 'eLearning_instructor_stats', true) || is_active_widget(false, false, 'eLearning_instructor_commission_stats', true) || is_active_widget(false, false, 'eLearning_student_stats', true)) {
            wp_enqueue_script('eLearning-raphael', eLearning_DASHBOARD_URL . '/js/raphael-min.js', array('jquery'), true);
            wp_enqueue_script('eLearning-morris', eLearning_DASHBOARD_URL . '/js/morris.min.js', array('jquery'), true);
        }
        $translation_array = array(
            'earnings' => __('Earnings', 'eLearning'),
            'payout' => __('Payout', 'eLearning'),
            'students' => __('# Students', 'eLearning'),
            'saved' => __('SAVED', 'eLearning'),
            'saving' => __('SAVING ...', 'eLearning'),
            'select_recipients' => __('Select recipients...', 'eLearning'),
            'stats_calculated' => __('Stats Calculated, reloading page ...', 'eLearning')
        );
        wp_localize_script('eLearning-dashboard-js', 'eLearning_dashboard_strings', $translation_array);
    }


    $located_template = apply_filters('bp_located_template', locate_template($template, false), $template);
    if ($located_template && $located_template != '') {
        bp_get_template_part(apply_filters('bp_load_template', $located_template));
    } else {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/dashboard'));
    }
}

add_action('widgets_init', 'eLearning_dashboard_setup_sidebars');

function eLearning_dashboard_setup_sidebars() {
    if (function_exists('register_sidebar')) {
        register_sidebar(array(
            'name' => __('Student Sidebar', 'eLearning'),
            'id' => 'student_sidebar',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="dash_widget_title">',
            'after_title' => '</h4>',
            'description' => __('This is the dashboard sidebar for Students', 'eLearning')
        ));
        register_sidebar(array(
            'name' => __('Instructor Sidebar', 'eLearning'),
            'id' => 'instructor_sidebar',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="dash_widget_title">',
            'after_title' => '</h4>',
            'description' => __('This is the dashboard sidebar for Instructors', 'eLearning')
        ));
    }
}

add_action('init', 'eLearning_register_news');

function eLearning_register_news() {

    if (function_exists('eLearning_get_option')) {

        $show_news = eLearning_get_option('show_news');
        if (empty($show_news))
            return;

        add_action('init', 'eLearning_register_course_news', 20);
    }
}

function eLearning_register_course_news() {

    if (!defined('eLearning_NEWS_SLUG')) {
        define('eLearning_NEWS_SLUG', 'news');
    }

    register_post_type('news', array(
        'labels' => array(
            'name' => __('Course News', 'eLearning'),
            'menu_name' => __('Course News', 'eLearning'),
            'singular_name' => __('News', 'eLearning'),
            'add_new_item' => __('Add News', 'eLearning'),
            'all_items' => __('Course News', 'eLearning')
        ),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'capapbility_type' => 'post',
        'has_archive' => true,
        'show_in_admin_bar' => true,
        'show_in_menu' => 'lms',
        'show_in_nav_menus' => true,
        'taxonomies' => array('news-tag'),
        'supports' => array('title', 'editor', 'thumbnail', 'author', 'post-formats', 'comments', 'excerpt', 'revisions', 'custom-fields'),
        'hierarchical' => true,
        'rewrite' => array('slug' => eLearning_NEWS_SLUG, 'hierarchical' => true, 'with_front' => false)
            )
    );

    register_taxonomy('news-tag', array('news'), array(
        'labels' => array(
            'name' => __('News Tag', 'eLearning'),
            'menu_name' => __('News Tag', 'eLearning'),
            'singular_name' => __('News Tag', 'eLearning'),
            'add_new_item' => __('Add New Tag', 'eLearning'),
            'all_items' => __('All News Tags', 'eLearning')
        ),
        'public' => true,
        'hierarchical' => false,
        'show_ui' => true,
        'show_admin_column' => 'true',
        'show_in_nav_menus' => true,
        'rewrite' => array('slug' => 'news-tag', 'hierarchical' => true, 'with_front' => false),
            )
    );
    $prefix = 'eLearning_';
    $news_metabox = array(
        array(// Text Input
            'label' => __('Share with students in Course', 'eLearning'), // <label>
            'desc' => __('Student having access to this courses will get the news', 'eLearning'), // description
            'id' => $prefix . 'news_course', // field id and name
            'type' => 'selectcpt', // type of field
            'post_type' => 'course'
        ),
        array(// Single checkbox
            'label' => __('Post Sub-Title', 'eLearning'), // <label>
            'desc' => __('Post Sub- Title.', 'eLearning'), // description
            'id' => $prefix . 'subtitle', // field id and name
            'type' => 'textarea', // type of field
            'std' => ''
        ),
    );
    if (class_exists('custom_add_meta_box'))
        $news_box = new custom_add_meta_box('page-settings', __('News Settings', 'eLearning'), $news_metabox, 'news', true);
}
