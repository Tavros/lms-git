<?php

add_action( 'widgets_init', 'eLearning_notes_discussion_widget' );

function eLearning_notes_discussion_widget() {
    register_widget('eLearning_notes_discussion');
}

class eLearning_notes_discussion extends WP_Widget {

    /** constructor -- name this the same as the class above */
    function __construct() {
    $widget_ops = array( 'classname' => 'eLearning_notes_discussion', 'description' => __('Notes & Discussion Widget for Dashboard', 'eLearning') );
    $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'eLearning_notes_discussion' );
    parent::__construct( 'eLearning_notes_discussion', __(' DASHBOARD : Notes & Discussion', 'eLearning'), $widget_ops, $control_ops );
  }
        
 
    /** @see WP_Widget::widget -- do not rename this */
    function widget( $args, $instance ) {
    extract( $args );

    //Our variables from the widget settings.
    $title = apply_filters('widget_title', $instance['title'] );
    $width =  $instance['width'];
    $number = $instance['number'];
    echo '<div class="'.$width.'">
            <div class="dash-widget">'.$before_widget;

    // Display the widget title 
    if ( $title )
        echo $before_title . $title . $after_title;
        
        $unit_comments = eLearning_get_option('unit_comments');
        if(isset($unit_comments) && is_numeric($unit_comments)){
            $link = get_permalink($unit_comments);
        }else
            $link = '#';

        echo '<div id="eLearning-tabs-notes_discussion" class="tabs tabbable">   
             <a href="'.$link.'" class="view_all_notes">'.__('SEE ALL','eLearning').'</a>
              <ul class="nav nav-tabs clearfix">
                <li><a href="#tab-notes" data-toggle="tab">'.__('My Notes','eLearning').'</a></li>
                <li><a href="#tab-discussion" data-toggle="tab">'.__('My Discussions','eLearning').'</a></li>
            </ul><div class="tab-content">';
            echo '<div id="tab-notes" class="tab-pane">';
            $user_id =get_current_user_id();
            $args = apply_filters('eLearning_notes_dicussion_dashboard_args',array(
                'number'              => $number,
                'post_status'         => 'publish',
                'post_type'           => 'unit',
                'status'              => 'approve',
                'type'                => 'note',
                'user_id'             => $user_id
            ));
            echo '<div id="notes_query">'.json_encode($args).'</div>
                    <div id="notes_discussions">';
                    $comments_query = new WP_Comment_Query;
                    $comments = $comments_query->query( $args );
                    // Comment Loop
                    $eLearning_notes_discussions= new eLearning_notes_discussions();
                    $eLearning_notes_discussions->comments_loop($comments);
            echo '</div></div>';
            echo '<div id="tab-discussion" class="tab-pane">';
            $args = apply_filters('eLearning_notes_dicussion_dashboard_args',array(
                'number'              => $number,
                'post_status'         => 'publish',
                'post_type'           => 'unit',
                'status'              => 'approve',
                'type'        => 'public',
                'user_id'             => $user_id
            ));
            echo '<div id="notes_query">'.json_encode($args).'</div>
                    <div id="notes_discussions">';
                    $comments_query = new WP_Comment_Query;
                    $comments = $comments_query->query( $args );
                    // Comment Loop
                    $eLearning_notes_discussions= new eLearning_notes_discussions();
                    $eLearning_notes_discussions->comments_loop($comments);
            echo '</div></div>';

            echo '</div></div>';
        echo $after_widget.'
        </div>
        </div>';
                
    }
 
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {   
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = $new_instance['number'];
        $instance['width'] = $new_instance['width'];
        return $instance;
    }
 
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {  
        $defaults = array( 
                        'title'  => __('Notes& Discussion','eLearning'),
                        'content' => '',
                        'width' => 'col-md-6 col-sm-12'
                    );
          $instance = wp_parse_args( (array) $instance, $defaults );
        $title  = esc_attr($instance['title']);
        $number = esc_attr($instance['number']);
        $width = esc_attr($instance['width']);
        ?>
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','eLearning'); ?></label> 
          <input class="regular_text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
         <p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of Notes/Dicussions','eLearning'); ?></label> 
          <input class="regular_text" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('Select Width','eLearning'); ?></label> 
          <select id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>">
            <option value="col-md-3 col-sm-6" <?php selected('col-md-3 col-sm-6',$width); ?>><?php _e('One Fourth','eLearning'); ?></option>
            <option value="col-md-4 col-sm-6" <?php selected('col-md-4 col-sm-6',$width); ?>><?php _e('One Third','eLearning'); ?></option>
            <option value="col-md-6 col-sm-12" <?php selected('col-md-6 col-sm-12',$width); ?>><?php _e('One Half','eLearning'); ?></option>
            <option value="col-md-8 col-sm-12" <?php selected('col-md-8 col-sm-12',$width); ?>><?php _e('Two Third','eLearning'); ?></option>
             <option value="col-md-8 col-sm-12" <?php selected('col-md-9 col-sm-12',$width); ?>><?php _e('Three Fourth','eLearning'); ?></option>
            <option value="col-md-12" <?php selected('col-md-12',$width); ?>><?php _e('Full','eLearning'); ?></option>
          </select>
        </p>
        <?php 
    }
} 

?>