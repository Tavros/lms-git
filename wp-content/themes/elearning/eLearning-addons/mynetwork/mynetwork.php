<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_MYNETWORK_URL')) {
    define('ELEARNING_MYNETWORK_URL', get_template_directory_uri() . '/eLearning-addons/mynetwork/');
}

include_once 'includes/functions.php';
include_once 'includes/mynetwork.php';
add_action('after_setup_theme', 'load_mynetwork_textdomain');

function load_mynetwork_textdomain() {
    load_textdomain('eLearning-mynetwork', dirname(__FILE__) . '/languages/');
}

?>
