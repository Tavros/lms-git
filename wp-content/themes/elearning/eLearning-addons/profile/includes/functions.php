<?php

function eLearning_profile_template() {

    if (!is_user_logged_in())
        wp_redirect(site_url());

    $template = 'templates/profile';
global $bp;


    if ($bp->current_component == 'profile') {

        $translation_array = array(
            'earnings' => __('Earnings', 'eLearning-profile'),
            'payout' => __('Payout', 'eLearning-profile'),
            'students' => __('# Students', 'eLearning-profile'),
            'saved' => __('SAVED', 'eLearning-profile'),
            'saving' => __('SAVING ...', 'eLearning-profile'),
            'select_recipients' => __('Select recipients...', 'eLearning-profile'),
            'stats_calculated' => __('Stats Calculated, reloading page ...', 'eLearning-profile')
        );
        wp_localize_script('eLearning-profile-js', 'eLearning_profile_strings', $translation_array);
    }


    $located_template = apply_filters('bp_located_template', locate_template($template, false), $template);

     var_dump('wwwwwwwwwwwwwwwwwwwww',$located_template);



        bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/profile'));

}


function get_admin_profile_content(){
    var_dump('get_admin_profile_content');
}
