<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_PROFILE_URL')) {
    define('ELEARNING_PROFILE_URL', get_template_directory_uri() . '/eLearning-addons/profile/');
}

include_once 'includes/functions.php';
include_once 'includes/profile.php';
add_action('after_setup_theme', 'load_profile_textdomain');

function load_profile_textdomain() {
    load_textdomain('eLearning-profile', dirname(__FILE__) . '/languages/');
}

?>