<?php

if (!defined('ABSPATH'))
    exit;

if (!defined('ELEARNING_SALES_URL')) {
    define('ELEARNING_SALES_URL', get_template_directory_uri() . '/eLearning-addons/sales/');
}

include_once 'includes/functions.php';
include_once 'includes/sales.php';
add_action('after_setup_theme', 'load_sales_textdomain');

function load_sales_textdomain() {
    load_textdomain('eLearning-sales', dirname(__FILE__) . '/languages/');
}

?>