<?php

function eLearning_sales_template() {

    if (!is_user_logged_in())
        wp_redirect(site_url());

    $template = 'templates/sales';
global $bp;


    if ($bp->current_component == 'sales') {

        $translation_array = array(
            'earnings' => __('Earnings', 'eLearning-sales'),
            'payout' => __('Payout', 'eLearning-sales'),
            'students' => __('# Students', 'eLearning-sales'),
            'saved' => __('SAVED', 'eLearning-sales'),
            'saving' => __('SAVING ...', 'eLearning-sales'),
            'select_recipients' => __('Select recipients...', 'eLearning-sales'),
            'stats_calculated' => __('Stats Calculated, reloading page ...', 'eLearning-sales')
        );
        wp_localize_script('eLearning-sales-js', 'eLearning_sales_strings', $translation_array);
    }


    $located_template = apply_filters('bp_located_template', locate_template($template, false), $template);

    


        bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/sales'));

}


function get_admin_sales_content(){
    var_dump('get_admin_sales_content');
}
