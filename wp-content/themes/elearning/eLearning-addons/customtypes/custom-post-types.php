<?php
/**
 * FILE: custom-post-types.php 
 * Created on Feb 18, 2013 at 7:47:20 PM 
 * Author: Mr.ELearning 
 * Credits: www.ELearningThemes.com
 * Project: ELEARNING
 * License: GPLv2
 */
 
if ( !defined( 'ABSPATH' ) ) exit;


add_action( 'admin_menu', 'register_lms_menu_page' );

function register_lms_menu_page(){
	$settings = lms_settings::init();
    add_menu_page( __('Learning Management System','eLearning-customtypes'), 'LMS', 'edit_posts', 'lms', 'eLearning_lms_dashboard','dashicons-welcome-learn-more',7 );
    add_submenu_page( 'lms', __('Statistics','eLearning-customtypes'), __('Statistics','eLearning-customtypes'),  'edit_posts', 'lms-stats', 'eLearning_lms_stats' );
    add_submenu_page( 'lms', __('Settings','eLearning-customtypes'), __('Settings','eLearning-customtypes'),  'manage_options', 'lms-settings', array($settings,'eLearning_lms_settings'));
    add_submenu_page( 'lms', __('Lms Tree','eLearning-customtypes'), __('Lms Tree','eLearning-customtypes'),  'manage_options', 'lms-tree', array($settings,'eLearning_lms_tree'));
    //admin.php?page=lms
   // add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function )
}

/*== PORTFOLIO == */
if(!function_exists('register_lms')){

function register_lms() {
	if ( ! defined( 'ELEARNING_COURSE_SLUG' ) )
		define( 'ELEARNING_COURSE_SLUG', 'course' );

	if ( ! defined( 'BP_COURSE_SLUG' ) )
		define( 'BP_COURSE_SLUG', 'course' );

	if ( ! defined( 'ELEARNING_COURSE_CATEGORY_SLUG' ) )
		define( 'ELEARNING_COURSE_CATEGORY_SLUG', 'course-cat' );

	if ( ! defined( 'ELEARNING_UNIT_SLUG' ) )
		define( 'ELEARNING_UNIT_SLUG', 'unit' );

	if ( ! defined( 'ELEARNING_QUIZ_SLUG' ) )
		define( 'ELEARNING_QUIZ_SLUG', 'quiz' );

	if ( ! defined( 'ELEARNING_QUESTION_SLUG' ) )
		define( 'ELEARNING_QUESTION_SLUG', 'question' );

	if ( ! defined( 'ELEARNING_EVENT_SLUG' ) )
		define( 'ELEARNING_EVENT_SLUG', 'event' );

	if ( ! defined( 'ELEARNING_ASSIGNMENT_SLUG' ) )
		define( 'ELEARNING_ASSIGNMENT_SLUG', 'assignment' );

	if ( ! defined( 'ELEARNING_TESTIMONIAL_SLUG' ) )
		define( 'ELEARNING_TESTIMONIAL_SLUG', _x('testimonial','Testimonial slug in permalink','eLearning-customtypes'));

	$permalinks = get_option( 'eLearning_course_permalinks' );
	$course_permalink = empty( $permalinks['course_base'] ) ? ELEARNING_COURSE_SLUG : $permalinks['course_base'];
	$quiz_permalink = empty( $permalinks['quiz_base'] ) ? ELEARNING_QUIZ_SLUG : $permalinks['quiz_base'];
	$unit_permalink = empty( $permalinks['unit_base'] ) ? ELEARNING_UNIT_SLUG : $permalinks['unit_base'];

	$bp_pages = get_option('bp-pages');
	if(isset($bp_pages) && is_array($bp_pages) && isset($bp_pages['course'])){
		 $projects_page_id = $bp_pages['course'];
		if(get_post_type( $projects_page_id ) == 'page'){
			$uri = get_page_uri( $projects_page_id );
		}
	} 
	
	register_taxonomy( 'course-cat', array( 'course'),
		array(
			'labels' => array(
				'name' => __('Course Category','eLearning-customtypes'),
				'menu_name' => __('Category','eLearning-customtypes'),
				'singular_name' => __('Category','eLearning-customtypes'),
				'add_new_item' => __('Add New Course Category','eLearning-customtypes'),
				'all_items' => __('All Categories','eLearning-customtypes')
			),
			'public' => true,
			'hierarchical' => true,
			'show_ui' => true,
			'show_in_menu' => 'lms',
			'show_admin_column' => true,
	        'query_var' => 'course-cat',           
			'show_in_nav_menus' => true,
			'rewrite' => array( 
				'slug' => empty( $permalinks['course_category_base'] ) ? ELEARNING_COURSE_CATEGORY_SLUG : $permalinks['course_category_base'],
				'hierarchical' => true, 
				'with_front' => false ),
		)
	);

         
	register_post_type( 'course',
		array(
			'labels' => array(
				'name' => __('Courses','eLearning-customtypes'),
				'menu_name' => __('Courses','eLearning-customtypes'),
				'singular_name' => __('Course','eLearning-customtypes'),
				'add_new_item' => __('Add New Course','eLearning-customtypes'),
				'all_items' => __('All Courses','eLearning-customtypes')
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'capapbility_type' => 'post',
            'has_archive' => empty($uri)?true:$uri,
			'show_in_menu' => 'lms',
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'taxonomies' => array( 'course-cat'),
			'supports' => array( 'title','editor','thumbnail','author','comments','excerpt','revisions','custom-fields', 'page-attributes'),
			'hierarchical' => true,
			'rewrite' => array( 'slug' => $course_permalink, 'hierarchical' => true, 'with_front' => false )
		)
	);


    register_post_type( 'unit',
		array(
			'labels' => array(
				'name' => __('Units','eLearning-customtypes'),
				'menu_name' => __('Units','eLearning-customtypes'),
				'singular_name' => __('Unit','eLearning-customtypes'),
				'add_new_item' => __('Add New Unit','eLearning-customtypes'),
				'all_items' => __('All Units','eLearning-customtypes')
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
            'has_archive' => false,
			'show_in_menu' => 'lms',
			'show_in_admin_bar' => true,
			'exclude_from_search' => true, 
			'show_in_nav_menus' => true,
			'supports' => array( 'title', 'editor', 'thumbnail','author','comments', 'post-formats', 'revisions','custom-fields' ),
			'hierarchical' => true,
			'rewrite' => array( 'slug' => $unit_permalink, 'hierarchical' => true, 'with_front' => false )
		)
	 );   
    

     register_taxonomy( 'module-tag', array( 'unit'),
		array(
			'labels' => array(
				'name' => __('Tag','eLearning-customtypes'),
				'menu_name' => __('Tag','eLearning-customtypes'),
				'singular_name' => __('Tag','eLearning-customtypes'),
				'add_new_item' => __('Add New Tag','eLearning-customtypes'),
				'all_items' => __('All Tags','eLearning-customtypes')
			),
			'public' => true,
			'hierarchical' => false,
			'show_in_menu' => 'lms',
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'rewrite' => array( 'slug' => 'module-tag', 'hierarchical' => true, 'with_front' => true ),
		)
	);

	 register_post_type( 'quiz',
		array(
			'labels' => array(
				'name' => __('Quizes','eLearning-customtypes'),
				'menu_name' => __('Quizes','eLearning-customtypes'),
				'singular_name' => __('Quiz','eLearning-customtypes'),
				'all_items' => __('All Quizes','eLearning-customtypes')
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
            'has_archive' => false,
			'show_in_menu' => 'lms',
			'exclude_from_search' => true, 
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'supports' => array( 'title','author','thumbnail','editor', 'revisions','custom-fields' ),
			'hierarchical' => true,
			'rewrite' => array( 'slug' => $quiz_permalink,'hierarchical' => true, 'with_front' => false )
		)
	 );  

	 register_taxonomy( 'quiz-type', array( 'quiz'),
			array(
				'labels' => array(
					'name' => __('Quiz type','eLearning-customtypes'),
					'menu_name' => __('Quiz type','eLearning-customtypes'),
					'singular_name' => __('Quiz type','eLearning-customtypes'),
					'add_new_item' => __('Add New Quiz type','eLearning-customtypes'),
					'all_items' => __('All Quiz types','eLearning-customtypes')
				),
				'public' => true,
				'hierarchical' => true,
				'show_ui' => true,
				'show_in_menu' => 'lms',
				'show_admin_column' => true,
				'show_in_admin_bar' => true,
				'show_in_nav_menus' => true,
				'rewrite' => array( 'slug' => 'quiz-type', 'hierarchical' => true, 'with_front' => false ),
			)
		);
    	 
	 register_post_type( 'question',
		array(
			'labels' => array(
				'name' => __('Question Bank','eLearning-customtypes'),
				'menu_name' => __('Question Bank','eLearning-customtypes'),
				'singular_name' => __('Question','eLearning-customtypes'),
				'all_items' => __('All Questions','eLearning-customtypes')
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
            'has_archive' => false,
			'show_in_menu' => 'lms',
			'exclude_from_search' => true, 
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'supports' => array( 'title','author','editor', 'comments','revisions' ,'custom-fields'),
			'hierarchical' => true,
			'rewrite' => array( 'slug' => ELEARNING_QUESTION_SLUG,'hierarchical' => true, 'with_front' => false )
		)
	 ); 
	 
    	 
	 register_taxonomy( 'question-tag', array( 'question'),
		array(
			'labels' => array(
				'name' => __('Question Tag','eLearning-customtypes'),
				'menu_name' => __('Tag','eLearning-customtypes'),
				'singular_name' => __('Tag','eLearning-customtypes'),
				'add_new_item' => __('Add New Tag','eLearning-customtypes'),
				'all_items' => __('All Tags','eLearning-customtypes')
			),
			'public' => true,
			'hierarchical' => false,
			'show_ui' => true,
			'show_admin_column' => 'true',
			'show_in_nav_menus' => true,
			'rewrite' => array( 'slug' => 'question-tag', 'hierarchical' => false, 'with_front' => false ),
		)
	); 

	add_post_type_support('question','comments');

	/*====== Version 1.4 EVENTS =====*/

	if ( in_array( 'eLearning-events/eLearning-events.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {     

	register_post_type( 'eLearning-event',
			array(
				'labels' => array(
					'name' => __('Events','eLearning-customtypes'),
					'menu_name' => __('Events','eLearning-customtypes'),
					'singular_name' => __('Event','eLearning-customtypes'),
					'add_new_item' => __('Add New Events','eLearning-customtypes'),
					'all_items' => __('All Events','eLearning-customtypes')
				),
				'public' => true,
				'taxonomies' => array( 'event-type'),
				'publicly_queryable' => true,
				'show_ui' => true,
	            'has_archive' => true,
				'show_in_menu' => 'lms',
				'show_in_admin_bar' => true,
				'show_in_nav_menus' => true,
				'supports' => array( 'title', 'editor', 'thumbnail','author', 'post-formats', 'revisions','custom-fields' ),
				'hierarchical' => true,
				'rewrite' => array( 'slug' => ELEARNING_EVENT_SLUG, 'hierarchical' => true, 'with_front' => true )
			)
		 );   
	    

	 register_taxonomy( 'event-type', array( 'eLearning-event'),
			array(
				'labels' => array(
					'name' => __('Event type','eLearning-customtypes'),
					'menu_name' => __('Event type','eLearning-customtypes'),
					'singular_name' => __('Event type','eLearning-customtypes'),
					'add_new_item' => __('Add New Event type','eLearning-customtypes'),
					'all_items' => __('All Event types','eLearning-customtypes')
				),
				'public' => true,
				'hierarchical' => true,
				'show_ui' => true,
				'show_admin_column' => true,
				'show_in_admin_bar' => true,
				'show_in_nav_menus' => true,
				'rewrite' => array( 'slug' => 'event-type', 'hierarchical' => true, 'with_front' => false ),
			)
		);
	 add_post_type_support('eLearning-event','comments');
	}

	/*====== Version 1.5 ASSIGNMENTS =====*/

	if ( in_array( 'eLearning-assignments/eLearning-assignments.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {     

	register_post_type( 'eLearning-assignment',
			array(
				'labels' => array(
					'name' => __('Assignments','eLearning-customtypes'),
					'menu_name' => __('Assignments','eLearning-customtypes'),
					'singular_name' => __('Assignment','eLearning-customtypes'),
					'add_new_item' => __('Add New Assignment','eLearning-customtypes'),
					'all_items' => __('All Assignments','eLearning-customtypes')
				),
				'public' => true,
				'taxonomies' => array( 'assignment-type'),
				'publicly_queryable' => true,
				'show_ui' => true,
	            'has_archive' => true,
				'show_in_menu' => 'lms',
				'exclude_from_search' => true, 
				'show_in_admin_bar' => true,
				'show_in_nav_menus' => true,
				'supports' => array( 'title', 'editor','author', 'post-formats', 'revisions','custom-fields' ),
				'hierarchical' => true,
				'rewrite' => array( 'slug' => ELEARNING_ASSIGNMENT_SLUG, 'hierarchical' => true, 'with_front' => true )
			)
		 );   
	    

	 register_taxonomy( 'assignment-type', array( 'eLearning-assignment'),
			array(
				'labels' => array(
					'name' => __('Assignment type','eLearning-customtypes'),
					'menu_name' => __('Assignment type','eLearning-customtypes'),
					'singular_name' => __('Assignment type','eLearning-customtypes'),
					'add_new_item' => __('Add New Assignment type','eLearning-customtypes'),
					'all_items' => __('All Assignment types','eLearning-customtypes')
				),
				'public' => true,
				'hierarchical' => true,
				'show_ui' => true,
				'show_admin_column' => true,
				'show_in_admin_bar' => true,
				'show_in_nav_menus' => true,
				'rewrite' => array( 'slug' => 'Assignment-type', 'hierarchical' => true, 'with_front' => false ),
			)
		);
	 add_post_type_support('eLearning-assignment','comments');
	}

/*====== Version 1.3 RECORD PAYMENTS =====*/
	register_post_type( 'payments',
		array(
			'labels' => array(
				'name' => __('Payments','eLearning-customtypes'),
				'menu_name' => __('Payments','eLearning-customtypes'),
				'singular_name' => __('Payment','eLearning-customtypes'),
				'add_new_item' => __('Add New Payment','eLearning-customtypes'),
				'all_items' => __('Payouts','eLearning-customtypes')
			),
			'publicly_queryable' => true,
			'show_ui' => true,
			'exclude_from_search' => true,
            'has_archive' => false,
            'query_var'   => false,
			'show_in_menu' => (current_user_can('manage_options')?'lms':false),
			'show_in_nav_menus' => false,
			'supports' => array( 'title'),
			'hierarchical' => false,
			'rewrite' => array( 'slug' => 'payments', 'hierarchical' => false, 'with_front' => false )
		)
	 );   
   
	/*====== Version 1.3.2 CERTIFICATE TEMPLATES =====*/
	register_post_type( 'certificate',
		array(
			'labels' => array(
				'name' => __('Certificate Template','eLearning-customtypes'),
				'menu_name' => __('Certificates Template','eLearning-customtypes'),
				'singular_name' => __('Certificate Template','eLearning-customtypes'),
				'add_new_item' => __('Add New Certificate','eLearning-customtypes'),
				'all_items' => __('Certificate Templates','eLearning-customtypes')
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
            'has_archive' => false,
			'show_in_menu' => 'lms',
			'show_in_nav_menus' => false,
			'supports' => array( 'title','editor'),
			'hierarchical' => false,
			'rewrite' => array( 'slug' => 'certificates', 'hierarchical' => false, 'with_front' => false )
		)
	 );
	/*====== Version 1.6.2 LINKAGE =====*/
	$linkage = eLearning_get_option('linkage');
	if(isset($linkage) && $linkage){
		register_taxonomy( 'linkage', array( 'course','unit','quiz','question','certificate','eLearning-assignment','eLearning-event','forum','product'),
			array(
				'labels' => array(
					'name' => __('Linkage','eLearning-customtypes'),
					'menu_name' => __('Linkage','eLearning-customtypes'),
					'singular_name' => __('Linkage','eLearning-customtypes'),
					'add_new_item' => __('Add New Link','eLearning-customtypes'),
					'all_items' => __('All Links','eLearning-customtypes')
				),
				'public' => true,
				'hierarchical' => false,
				'show_ui' => true,
				'show_in_menu' => 'lms',
				'show_admin_column' => true,
				'show_in_nav_menus' => true,
				'rewrite' => array( 'slug' => 'linkage', 'hierarchical' => true, 'with_front' => false ),
			)
		);
	}
	/*====== Version 1.6.5 LEVEL =====*/	
	$level = eLearning_get_option('level');
	if(isset($level) && $level){
		register_taxonomy( 'level', array( 'course'),
			array(
				'labels' => array(
					'name' => __('Level','eLearning-customtypes'),
					'menu_name' => __('Level','eLearning-customtypes'),
					'singular_name' => __('Level','eLearning-customtypes'),
					'add_new_item' => __('Add New Level','eLearning-customtypes'),
					'all_items' => __('All Levels','eLearning-customtypes')
				),
				'public' => true,
				'hierarchical' => true,
				'show_ui' => true,
				'show_in_menu' => 'lms',
				'show_admin_column' => true,
	            'query_var' => 'level',           
				'show_in_nav_menus' => true,
				'rewrite' => array( 'slug' => ELEARNING_LEVEL_SLUG, 'hierarchical' => true, 'with_front' => false ),
			)
		);
	}	
	/*====== Version 1.6.5 LEVEL =====*/	
	$location = eLearning_get_option('location');
	if(isset($location) && $location){
		register_taxonomy( 'location', array( 'course'),
			array(
				'labels' => array(
					'name' => __('Location','eLearning-customtypes'),
					'menu_name' => __('Location','eLearning-customtypes'),
					'singular_name' => __('Location','eLearning-customtypes'),
					'add_new_item' => __('Add New Location','eLearning-customtypes'),
					'all_items' => __('All Locations','eLearning-customtypes')
				),
				'public' => true,
				'hierarchical' => true,
				'show_ui' => true,
				'show_in_menu' => 'lms',
				'show_admin_column' => true,
	            'query_var' => 'location',           
				'show_in_nav_menus' => true,
				'rewrite' => array( 'slug' => ELEARNING_LOCATION_SLUG, 'hierarchical' => true, 'with_front' => false ),
			)
		);
	}
	}// End REgister LMS
}


/*== Testimonials == */
if(!function_exists('register_testimonials')){
function register_testimonials() {
	register_post_type( 'testimonials',
		array(
			'labels' => array(
				'name' => __('Testimonials','eLearning-customtypes'),
				'menu_name' => __('Testimonials','eLearning-customtypes'),
				'singular_name' => __('Testimonial','eLearning-customtypes'),
				'all_items' => __('All Testimonials','eLearning-customtypes')
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'supports' => array( 'title', 'editor','excerpt', 'thumbnail'),
			'hierarchical' => false,
			'has_archive' => true,
            'menu_position' => 10,
			'rewrite' => array( 'slug' => ELEARNING_TESTIMONIAL_SLUG, 'hierarchical' => true, 'with_front' => false )
		)
	);
        
   
}
}
/*== Popups == */
if(!function_exists('register_popups')){
function register_popups() {
	register_post_type( 'popups',
		array(
			'labels' => array(
				'name' => __('Popups','eLearning-customtypes'),
				'menu_name' => __('Popups','eLearning-customtypes'),
				'singular_name' => __('Popup','eLearning-customtypes'),
				'all_items' => __('All Popups','eLearning-customtypes')
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => false,
			'supports' => array( 'title', 'editor','excerpt' ),
			'hierarchical' => false,
			'has_archive' => false,
            'menu_position' => 8,
			'rewrite' => array( 'slug' => 'popup', 'hierarchical' => true, 'with_front' => false )
		)
	);
     
}

}


add_action( 'init', 'register_lms',5 );
add_action( 'init', 'register_testimonials' );
add_action( 'init', 'register_popups' );

if(!function_exists('eLearning_get_option')){ // Defining GET OPTION function
	function eLearning_get_option($field,$compare = NULL){
		if(defined('THEME_SHORT_NAME')){
			$option=get_option(THEME_SHORT_NAME);
		}else{
			$option=get_option('eLearning');
		}
	    
	    $return = isset($option[$field])?$option[$field]:NULL;
	    if(isset($return)){
	        if(isset($compare)){
	        if($compare === $return){
	            return true;
	        }else
	            return false;
	    }
	        return $return;
	    }else
	    	return NULL;
   }   
}
 
add_filter( 'post_row_actions', 'remove_payments_row_actions', 10, 1 );
function remove_payments_row_actions( $actions )
{
    if( get_post_type() === 'payments' )
        unset( $actions['view'] );
        unset( $actions['inline hide-if-no-js'] );
    return $actions;
}
