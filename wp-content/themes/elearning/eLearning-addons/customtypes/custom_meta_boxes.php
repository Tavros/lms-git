<?php

if (!defined('ABSPATH'))
    exit;

function eLearning_meta_box_arrays($metabox) { // References added to Pick labels for Import/Export
    $prefix = 'eLearning_';
    $sidebars = $GLOBALS['wp_registered_sidebars'];
    $sidebararray = array();
    foreach ($sidebars as $sidebar) {
        if (!in_array($sidebar['id'], array('student_sidebar', 'instructor_sidebar')))
            $sidebararray[] = array('label' => $sidebar['name'], 'value' => $sidebar['id']);
    }
    $id = '';
    global $post;
    if (is_object($post)) {
        $id = $post->ID;
    }
    $course_duration_parameter = apply_filters('eLearning_course_duration_parameter', 86400, $id);
    $drip_duration_parameter = apply_filters('eLearning_drip_duration_parameter', 86400, $id);
    $unit_duration_parameter = apply_filters('eLearning_unit_duration_parameter', 60, $id);
    $unit_start_date = apply_filters('eLearning_unit_start_date', 60, $id);
    $unit_start_time = apply_filters('eLearning_unit_start_time', 60, $id);
    $quiz_duration_parameter = apply_filters('eLearning_quiz_duration_parameter', 60, $id);
    $product_duration_parameter = apply_filters('eLearning_product_duration_parameter', 86400, $id);
    $assignment_duration_parameter = apply_filters('eLearning_assignment_duration_parameter', 86400, $id);

    switch ($metabox) {
        case 'post':
            $metabox_settings = array(
                $prefix . 'subtitle' => array(// Single checkbox
                    'label' => __('Post Sub-Title', 'eLearning-customtypes'), // <label>
                    'desc' => __('Post Sub- Title.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'subtitle', // field id and name
                    'type' => 'textarea', // type of field
                    'std' => ''
                ),
                $prefix . 'template' => array(// Single checkbox
                    'label' => __('Post Template', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select a post template for showing content.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'template', // field id and name
                    'type' => 'select', // type of field
                    'options' => array(
                        1 => array('label' => __('Content on Left', 'eLearning-customtypes'), 'value' => ''),
                        2 => array('label' => __('Content on Right', 'eLearning-customtypes'), 'value' => 'right'),
                        3 => array('label' => __('Full Width', 'eLearning-customtypes'), 'value' => 'full'),
                    ),
                    'std' => ''
                ),
                $prefix . 'sidebar' => array(// Single checkbox
                    'label' => __('Sidebar', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select a Sidebar | Default : mainsidebar', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'sidebar', // field id and name
                    'type' => 'select',
                    'options' => $sidebararray
                ),
                $prefix . 'title' => array(// Single checkbox
                    'label' => __('Show Page Title', 'eLearning-customtypes'), // <label>
                    'desc' => __('Show Page/Post Title.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'title', // field id and name
                    'type' => 'showhide', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'S'
                ),
                $prefix . 'author' => array(// Single checkbox
                    'label' => __('Show Author Information', 'eLearning-customtypes'), // <label>
                    'desc' => __('Author information below post content.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'author', // field id and name
                    'type' => 'showhide', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'breadcrumbs' => array(// Single checkbox
                    'label' => __('Show Breadcrumbs', 'eLearning-customtypes'), // <label>
                    'desc' => __('Show breadcrumbs.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'breadcrumbs', // field id and name
                    'type' => 'showhide', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'S'
                ),
                $prefix . 'prev_next' => array(// Single checkbox
                    'label' => __('Show Prev/Next Arrows', 'eLearning-customtypes'), // <label>
                    'desc' => __('Show previous/next links on top below the Subheader.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'prev_next', // field id and name
                    'type' => 'showhide', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
            );
            break;
        case 'page':
            $metabox_settings = array(
                $prefix . 'title' => array(// Single checkbox
                    'label' => __('Show Page Title', 'eLearning-customtypes'), // <label>
                    'desc' => __('Show Page/Post Title.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'title', // field id and name
                    'type' => 'showhide', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'S'
                ),
                $prefix . 'subtitle' => array(// Single checkbox
                    'label' => __('Page Sub-Title', 'eLearning-customtypes'), // <label>
                    'desc' => __('Page Sub- Title.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'subtitle', // field id and name
                    'type' => 'textarea', // type of field
                    'std' => ''
                ),
                $prefix . 'breadcrumbs' => array(// Single checkbox
                    'label' => __('Show Breadcrumbs', 'eLearning-customtypes'), // <label>
                    'desc' => __('Show breadcrumbs.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'breadcrumbs', // field id and name
                    'type' => 'showhide', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'S'
                ),
                $prefix . 'sidebar' => array(// Single checkbox
                    'label' => __('Sidebar', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select Sidebar | Sidebar : mainsidebar', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'sidebar', // field id and name
                    'type' => 'select',
                    'options' => $sidebararray
                ),
            );
            break;
        case 'course':
            $metabox_settings = array(
                $prefix . 'sidebar' => array(// Single checkbox
                    'label' => __('Sidebar', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select a Sidebar | Default : mainsidebar', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'sidebar', // field id and name
                    'type' => 'select',
                    'options' => $sidebararray,
                    'std' => 'coursesidebar'
                ),
                $prefix . 'duration' => array(// Text Input
                    'label' => __('Total Duration of Course', 'eLearning-customtypes'), // <label>
                    'desc' => sprintf(__('Duration of Course (in %s)', 'eLearning-customtypes'), calculate_duration_time($course_duration_parameter)), // description
                    'id' => $prefix . 'duration', // field id and name
                    'type' => 'number', // type of field
                    'std' => 10,
                ),
                $prefix . 'course_duration_parameter' => array(// Text Input
                    'label' => __('Course Duration parameter', 'eLearning-customtypes'), // <label>
                    'desc' => __('Duration parameter', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_duration_parameter', // field id and name
                    'type' => 'duration', // type of field
                    'std' => $course_duration_parameter
                ),
                $prefix . 'students' => array(// Text Input
                    'label' => __('Total number of Students in Course', 'eLearning-customtypes'), // <label>
                    'desc' => __('Total number of Students who have taken this Course.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'students', // field id and name
                    'type' => 'number', // type of field
                    'std' => 0,
                ),
                $prefix . 'course_prev_unit_quiz_lock' => array(// Text Input
                    'label' => __('Unit Completion Lock', 'eLearning-customtypes'), // <label>
                    'desc' => __('Previous Units/Quiz must be Complete before next unit/quiz access', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_prev_unit_quiz_lock', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_offline' => array(// Text Input
                    'label' => __('Offline Course', 'eLearning-customtypes'), // <label>
                    'desc' => __('Make this an Offline Course', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_offline', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_unit_content' => array(// Text Input
                    'label' => __('Show Unit content in Curriculum', 'eLearning-customtypes'), // <label>
                    'desc' => __('Display units content in Course Curriculum, unit content visible in curriculum. ( Recommended for Offline Courses )', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_unit_content', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_button' => array(// Text Input
                    'label' => __('Hide Course Button after subscription', 'eLearning-customtypes'), // <label>
                    'desc' => __('Hide Start Course/Continue Course button after Course is subscribed by user. ( Recommended for Offline Courses )', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_button', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_progress' => array(// Text Input
                    'label' => __('Display Course Progress on Course home', 'eLearning-customtypes'), // <label>
                    'desc' => __('Display User Course progress on Course page. ( Recommended for Offline Courses )', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_progress', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_auto_progress' => array(// Text Input
                    'label' => __('Time based Course Progress ', 'eLearning-customtypes'), // <label>
                    'desc' => __('Automatically generate course progress based on duration (number of months/weeks/days/hours) passed in course.( Recommended for Offline Courses )', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_auto_progress', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_review' => array(// Text Input
                    'label' => __('Post Course Reviews from Course Home', 'eLearning-customtypes'), // <label>
                    'desc' => __('Allow subscribed users to post Course reviews from Course home page. ( Recommended for Offline Courses )', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_review', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_auto_eval' => array(// Text Input
                    'label' => __('Auto Evaluation', 'eLearning-customtypes'), // <label>
                    'desc' => __('Evalute Courses based on Quizzes scores available in Course (* Requires at least 1 Quiz in course)', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_auto_eval', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'start_date' => array(// Text Input
                    'label' => __('Course Start Date', 'eLearning-customtypes'), // <label>
                    'desc' => __('Date from which Course Begins', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'start_date', // field id and name
                    'type' => 'date', // type of field
                ),
                $prefix . 'max_students' => array(// Text Input
                    'label' => __('Maximum Students in Course', 'eLearning-customtypes'), // <label>
                    'desc' => __('Maximum number of students who can pursue the course at a time.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'max_students', // field id and name
                    'type' => 'number', // type of field
                ),
                $prefix . 'course_badge' => array(// Text Input
                    'label' => __('Excellence Badge', 'eLearning-customtypes'), // <label>
                    'desc' => __('Upload badge image which Students receive upon course completion', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_badge', // field id and name
                    'type' => 'image' // type of field
                ),
                $prefix . 'course_badge_percentage' => array(// Text Input
                    'label' => __('Badge Percentage', 'eLearning-customtypes'), // <label>
                    'desc' => __('Badge is given to people passing above percentage (out of 100)', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_badge_percentage', // field id and name
                    'type' => 'number' // type of field
                ),
                $prefix . 'course_badge_title' => array(// Text Input
                    'label' => __('Badge Title', 'eLearning-customtypes'), // <label>
                    'desc' => __('Title is shown on hovering the badge.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_badge_title', // field id and name
                    'type' => 'text' // type of field
                ),
                $prefix . 'course_certificate' => array(// Text Input
                    'label' => __('Completion Certificate', 'eLearning-customtypes'), // <label>
                    'desc' => __('Enable Certificate image which Students receive upon course completion (out of 100)', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_certificate', // field id and name
                    'type' => 'showhide', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'certificate_template' => array(// Text Input
                    'label' => __('Certificate Template', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select a Certificate Template', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'certificate_template', // field id and name
                    'type' => 'selectcpt', // type of field
                    'post_type' => 'certificate'
                ),
                $prefix . 'course_passing_percentage' => array(// Text Input
                    'label' => __('Passing Percentage', 'eLearning-customtypes'), // <label>
                    'desc' => __('Course passing percentage, for completion certificate', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_passing_percentage', // field id and name
                    'type' => 'number' // type of field
                ),
                $prefix . 'course_drip' => array(// Text Input
                    'label' => __('Drip Feed', 'eLearning-customtypes'), // <label>
                    'desc' => __('Enable Drip Feed course', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_drip', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_drip_origin' => array(// Text Input
                    'label' => __('Course Starting Time as Drip Feed Origin', 'eLearning-customtypes'), // <label>
                    'desc' => sprintf(__('Drip feed time calculation from Course starting date/time vs previous unit access date/time (default), %s tutorial %s', 'eLearning-customtypes'), '<a href="http://eLearningthemes.com/documentation/eLearning/knowledge-base/course-drip-origin/ " target="_blank">', '</a>'), // description
                    'id' => $prefix . 'course_drip_origin', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_section_drip' => array(// Text Input
                    'label' => __('Section Drip Feed', 'eLearning-customtypes'), // <label>
                    'desc' => __('Enable Section Drip Feed (default ) course', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_section_drip', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_drip_duration_type' => array(// Text Input
                    'label' => __('Drip Duration as Unit Duration', 'eLearning-customtypes'), // <label>
                    'desc' => __('Assume Drip duration same as Unit Duration. Duration between consecutive units.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_drip_duration_type', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_drip_duration' => array(// Text Input
                    'label' => __('Drip Feed Duration (Static)', 'eLearning-customtypes'), // <label>
                    'desc' => __('Static duration, if Drip duration not equals Unit duration. This is the duration between consecutive Drip feed units (in ', 'eLearning-customtypes') . calculate_duration_time($drip_duration_parameter) . ' )', // description
                    'id' => $prefix . 'course_drip_duration', // field id and name
                    'type' => 'number', // type of field
                ),
                $prefix . 'drip_duration_parameter' => array(// Text Input
                    'label' => __('Drip Duration parameter', 'eLearning-customtypes'), // <label>
                    'desc' => __('Duration parameter', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'drip_duration_parameter', // field id and name
                    'type' => 'duration', // type of field
                    'std' => $drip_duration_parameter
                ),
                $prefix . 'course_curriculum' => array(// Text Input
                    'label' => __('Course Curriculum', 'eLearning-customtypes'), // <label>
                    'desc' => __('Set Course Curriculum, prepare units and quizzes before setting up curriculum', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_curriculum', // field id and name
                    'post_type1' => 'unit',
                    'post_type2' => 'quiz',
                    'type' => 'curriculum' // type of field
                ),
                $prefix . 'pre_course' => array(// Text Input
                    'label' => __('Prerequisite Course', 'eLearning-customtypes'), // <label>
                    'desc' => __('Prerequisite courses for this course', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'pre_course', // field id and name
                    'type' => 'selectmulticpt', // type of field
                    'post_type' => 'course'
                ),
                $prefix . 'course_retakes' => array(// Text Input
                    'label' => __('Course Retakes', 'eLearning-customtypes'), // <label>
                    'desc' => __('Set number of times a student can re-take the course (0 to disable)', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_retakes', // field id and name
                    'type' => 'number',
                    'std' => 0 // type of field
                ),
                $prefix . 'forum' => array(// Text Input
                    'label' => __('Course Forum', 'eLearning-customtypes'), // <label>
                    'desc' => __('Connect Forum with Course.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'forum', // field id and name
                    'type' => 'selectcpt', // type of field
                    'post_type' => 'forum',
                    'std' => 0,
                ),
                $prefix . 'group' => array(// Text Input
                    'label' => __('Course Group', 'eLearning-customtypes'), // <label>
                    'desc' => __('Connect a Group with Course.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'group', // field id and name
                    'type' => 'groups', // type of field
                ),
                $prefix . 'course_instructions' => array(// Text Input
                    'label' => __('Course specific instructions', 'eLearning-customtypes'), // <label>
                    'desc' => __('Course specific instructions which would be shown in the Start course/Course status page', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_instructions', // field id and name
                    'type' => 'editor', // type of field
                    'std' => ''
                ),
                $prefix . 'course_message' => array(// Text Input
                    'label' => __('Course Completion Message', 'eLearning-customtypes'), // <label>
                    'desc' => __('This message is shown to users when they Finish submit the course', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_message', // field id and name
                    'type' => 'editor', // type of field
                    'std' => __('This message is shown to the user when she finishes the course.', 'eLearning-customtypes')
                ),
            );
            break;
        case 'course_product':
            $metabox_settings = array(
                $prefix . 'course_free' => array(// Text Input
                    'label' => __('Free Course', 'eLearning-customtypes'), // <label>
                    'desc' => __('Set Course free for all Members', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_free', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'course_apply' => array(// Text Input
                    'label' => __('Apply for Course', 'eLearning-customtypes'), // <label>
                    'text' => __('Invite Student applications for Course', 'eLearning-customtypes'),
                    'desc' => __('Students are required to Apply for course and instructor would manually approve them to course. Do not enable "Free" course with this setting.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'course_apply', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
            );

            if ((in_array('paid-memberships-pro/paid-memberships-pro.php', apply_filters('active_plugins', get_option('active_plugins'))) || (function_exists('is_plugin_active') && is_plugin_active('paid-memberships-pro/paid-memberships-pro.php')) ) && function_exists('pmpro_getAllLevels')) {
                $level_array = array();
                $levels = pmpro_getAllLevels();
                foreach ($levels as $level) {
                    $level_array[] = array('value' => $level->id, 'label' => $level->name);
                }
                $metabox_settings[$prefix . 'pmpro_membership'] = array(
                    'label' => __('Membership', 'eLearning-customtypes'), // <label>
                    'desc' => __('Allow access to these membership levels.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'pmpro_membership', // field id and name
                    'type' => 'multiselect', // type of field
                    'options' => $level_array,
                );
            }
            if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) || (function_exists('is_plugin_active') && is_plugin_active('woocommerce/woocommerce.php'))) {
                $instructor_privacy = eLearning_get_option('instructor_content_privacy');
                $flag = 1;
                if (isset($instructor_privacy) && $instructor_privacy && !current_user_can('manage_options')) {
                    $flag = 0;
                }
                if ($flag) {
                    $metabox_settings[$prefix . 'product'] = array(
                        'label' => __('Associated Product', 'eLearning-customtypes'), // <label>
                        'desc' => __('Associated Product with the Course.', 'eLearning-customtypes'), // description
                        'id' => $prefix . 'product', // field id and name
                        'type' => 'selectcpt', // type of field
                        'post_type' => 'product',
                        'std' => ''
                    );
                }
            }
            break;
        case 'unit':
            $unit_types = apply_filters('eLearning_unit_types', array(
                array('label' => __('Video', 'eLearning-customtypes'), 'value' => 'play'),
                array('label' => __('Audio', 'eLearning-customtypes'), 'value' => 'music-file-1'),
                array('label' => __('Podcast', 'eLearning-customtypes'), 'value' => 'podcast'),
                array('label' => __('General', 'eLearning-customtypes'), 'value' => 'text-document'),
                array('label' => __('Webinar', 'eLearning-customtypes'), 'value' => 'webinar'),
            ));
            $metabox_settings = array(
                $prefix . 'subtitle' => array(// Single checkbox
                    'label' => __('Unit Description', 'eLearning-customtypes'), // <label>
                    'desc' => __('Small Description.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'subtitle', // field id and name
                    'type' => 'textarea', // type of field
                    'std' => ''
                ),
                $prefix . 'type' => array(// Text Input
                    'label' => __('Unit Type', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select Unit type from Video , Audio , Podcast, General , ', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'type', // field id and name
                    'type' => 'select', // type of field
                    'options' => $unit_types,
                    'std' => 'text-document'
                ),
                $prefix . 'free' => array(// Text Input
                    'label' => __('Free Unit', 'eLearning-customtypes'), // <label>
                    'desc' => __('Set Free unit, viewable to all', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'free', // field id and name
                    'type' => 'showhide', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'duration' => array(// Text Input
                    'label' => __('Unit Duration', 'eLearning-customtypes'), // <label>
                    'desc' => __('Duration in ', 'eLearning-customtypes') . calculate_duration_time($unit_duration_parameter), // description
                    'id' => $prefix . 'duration', // field id and name
                    'type' => 'number' // type of field
                ),
                $prefix . 'unit_duration_parameter' => array(// Text Input
                    'label' => __('Unit Duration parameter', 'eLearning-customtypes'), // <label>
                    'desc' => __('Unit Duration parameter', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'unit_duration_parameter', // field id and name
                    'type' => 'duration', // type of field
                    'std' => $unit_duration_parameter
                ),
                $prefix . 'unit_start_date' => array(// Text Input
                    'label' => __('Unit Start Date', 'eLearning-customtypes'), // <label>
                    'desc' => __('Unit Start Date', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'unit_start_date', // field id and name
                    'type' => 'date', // type of field
                    'std' => $unit_start_date
                ),
                $prefix . 'unit_start_time' => array(// Text Input
                    'label' => __('Unit Start Time', 'eLearning-customtypes'), // <label>
                    'desc' => __('Unit Start Time', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'unit_start_time', // field id and name
                    'type' => 'time', // type of field
                    'std' => $unit_start_time
                ),
                $prefix . 'forum' => array(// Text Input
                    'label' => __('Unit Forum', 'eLearning-customtypes'), // <label>
                    'desc' => __('Connect Forum with Unit.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'forum', // field id and name
                    'type' => 'selectcpt', // type of field
                    'post_type' => 'forum',
                    'std' => 0,
                ),
                $prefix . 'assignment' => array(// Text Input
                    'label' => __('Connect Assignments', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select an Assignment which you can connect with this Unit', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'assignment', // field id and name
                    'type' => 'selectmulticpt', // type of field
                    'post_type' => 'eLearning-assignment'
                ),
            );

            break;
        case 'question':
            $question_types = apply_filters('eLearning_question_types', array(
                array('label' => __('True or False', 'eLearning-customtypes'), 'value' => 'truefalse'),
                array('label' => __('Multiple Choice', 'eLearning-customtypes'), 'value' => 'single'),
                array('label' => __('Multiple Correct', 'eLearning-customtypes'), 'value' => 'multiple'),
                array('label' => __('Sort Answers', 'eLearning-customtypes'), 'value' => 'sort'),
                array('label' => __('Match Answers', 'eLearning-customtypes'), 'value' => 'match'),
                array('label' => __('Fill in the Blank', 'eLearning-customtypes'), 'value' => 'fillblank'),
                array('label' => __('Dropdown Select', 'eLearning-customtypes'), 'value' => 'select'),
                array('label' => __('Small Text', 'eLearning-customtypes'), 'value' => 'smalltext'),
                array('label' => __('Large Text', 'eLearning-customtypes'), 'value' => 'largetext'),
                array('label' => __('Survey type', 'eLearning-customtypes'), 'value' => 'survey')
            ));
            $metabox_settings = array(
                $prefix . 'question_type' => array(// Text Input
                    'label' => __('Question Type', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select Question type, ', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'question_type', // field id and name
                    'type' => 'select', // type of field
                    'options' => $question_types,
                    'std' => 'single'
                ),
                $prefix . 'question_options' => array(// Text Input
                    'label' => __('Question Options (For Single/Multiple/Sort/Match Question types)', 'eLearning-customtypes'), // <label>
                    'desc' => __('Single/Mutiple Choice question options', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'question_options', // field id and name
                    'type' => 'repeatable_count' // type of field
                ),
                $prefix . 'question_answer' => array(// Text Input
                    'label' => __('Correct Answer', 'eLearning-customtypes'), // <label>
                    'desc' => __('Enter (1 = True, 0 = false ) or Choice Number (1,2..) or comma saperated Choice numbers (1,2..) or Correct Answer for small text (All possible answers comma saperated) | 0 for No Answer or Manual Check', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'question_answer', // field id and name
                    'type' => 'text', // type of field
                    'std' => 0
                ),
                $prefix . 'question_hint' => array(// Text Input
                    'label' => __('Answer Hint', 'eLearning-customtypes'), // <label>
                    'desc' => __('Add a Hint/clue for the answer to show to student', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'question_hint', // field id and name
                    'type' => 'textarea', // type of field
                    'std' => ''
                ),
                $prefix . 'question_explaination' => array(// Text Input
                    'label' => __('Answer Explanation', 'eLearning-customtypes'), // <label>
                    'desc' => __('Add Answer explanation', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'question_explaination', // field id and name
                    'type' => 'editor', // type of field
                    'std' => ''
                ),
            );
            break;
        case 'quiz':
            $metabox_settings = array(
                $prefix . 'subtitle' => array(// Text Input
                    'label' => __('Quiz Subtitle', 'eLearning-customtypes'), // <label>
                    'desc' => __('Quiz Subtitle.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'subtitle', // field id and name
                    'type' => 'text', // type of field
                    'std' => ''
                ),
                $prefix . 'quiz_course' => array(// Text Input
                    'label' => __('Connected Course', 'eLearning-customtypes'), // <label>
                    'id' => $prefix . 'quiz_course', // field id and name
                    'type' => 'selectcpt', // type of field
                    'post_type' => 'course',
                    'post_status' => array('publish', 'draft'),
                    'desc' => __('Connecting a quiz with a course would force the quiz to be available to users who have taken the course.', 'eLearning-customtypes'),
                ),
                $prefix . 'duration' => array(// Text Input
                    'label' => __('Quiz Duration', 'eLearning-customtypes'), // <label>
                    'desc' => __('Quiz duration in ', 'eLearning-customtypes') . calculate_duration_time($quiz_duration_parameter) . __(' Enables Timer & auto submits on expire. 9999 to disable.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'duration', // field id and name
                    'type' => 'number', // type of field
                    'std' => 0
                ),
                $prefix . 'quiz_duration_parameter' => array(// Text Input
                    'label' => __('Quiz Duration parameter', 'eLearning-customtypes'), // <label>
                    'desc' => __('Duration parameter', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'quiz_duration_parameter', // field id and name
                    'type' => 'duration', // type of field
                    'std' => $quiz_duration_parameter
                ),
                $prefix . 'quiz_auto_evaluate' => array(// Text Input
                    'label' => __('Auto Evaluate Results', 'eLearning-customtypes'), // <label>
                    'desc' => __('Evaluate results as soon as quiz is complete. (* No Large text questions ), Diable for manual evaluate', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'quiz_auto_evaluate', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'quiz_retakes' => array(// Text Input
                    'label' => __('Number of Extra Quiz Retakes', 'eLearning-customtypes'), // <label>
                    'desc' => __('Student can reset and start the quiz all over again. Number of Extra retakes a student can take.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'quiz_retakes', // field id and name
                    'type' => 'number', // type of field
                    'std' => 0
                ),
                $prefix . 'quiz_message' => array(// Text Input
                    'label' => __('Post Quiz Message', 'eLearning-customtypes'), // <label>
                    'desc' => __('This message is shown to users when they submit the quiz', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'quiz_message', // field id and name
                    'type' => 'editor', // type of field
                    'std' => 'Thank you for Submitting the Quiz. Check Results in your Profile.'
                ),
                $prefix . 'quiz_check_answer' => array(// Text Input
                    'label' => __('Add Check Answer Switch', 'eLearning-customtypes'), // <label>
                    'desc' => __('Instantly check answer answer when question is marked', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'quiz_check_answer', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'quiz_dynamic' => array(// Text Input
                    'label' => __('Dynamic Quiz', 'eLearning-customtypes'), // <label>
                    'desc' => __('Dynamic quiz automatically selects questions.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'quiz_dynamic', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'quiz_tags' => array(// Text Input
                    'label' => __('Dynamic Quiz Question tags', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select Question tags from where questions will be selected for the quiz.(required if dynamic enabled)', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'quiz_tags', // field id and name
                    'type' => 'dynamic_quiz_questions', // type of field
                    'taxonomy' => 'question-tag',
                    'std' => 0
                ),
                /* $prefix.'quiz_number_questions'=>array( // Text Input
                  'label'	=> __('Number of Questions in Dynamic Quiz','eLearning-customtypes'), // <label>
                  'desc'	=> __('Enter the number of Questions in the dynamic quiz. (required if dynamic enabled).','eLearning-customtypes'), // description
                  'id'	=> $prefix.'quiz_number_questions', // field id and name
                  'type'	=> 'number', // type of field
                  'std'   => 0
                  ),
                  $prefix.'quiz_marks_per_question'=>array( // Text Input
                  'label'	=> __('Marks per Question in Dynamic Quiz','eLearning-customtypes'), // <label>
                  'desc'	=> __('Enter the number of marks per Questions in the dynamic quiz. (required if dynamic enabled).','eLearning-customtypes'), // description
                  'id'	=> $prefix.'quiz_marks_per_question', // field id and name
                  'type'	=> 'number', // type of field
                  'std'   => 0
                  ), */
                $prefix . 'quiz_random' => array(// Text Input
                    'label' => __('Randomize Quiz Questions', 'eLearning-customtypes'), // <label>
                    'desc' => __('Random Question sequence for every quiz', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'quiz_random', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                $prefix . 'quiz_questions' => array(// Text Input
                    'label' => __('Quiz Questions', 'eLearning-customtypes'), // <label>
                    'desc' => __('Quiz questions for Static Quiz only', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'quiz_questions', // field id and name
                    'type' => 'repeatable_selectcpt', // type of field
                    'post_type' => 'question',
                    'std' => 0
                ),
            );
            break;
        case 'testimonial':
            $metabox_settings = array(
                array(// Text Input
                    'label' => __('Author Name', 'eLearning-customtypes'), // <label>
                    'desc' => __('Enter the name of the testimonial author.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'testimonial_author_name', // field id and name
                    'type' => 'text' // type of field
                ),
                array(// Text Input
                    'label' => __('Designation', 'eLearning-customtypes'), // <label>
                    'desc' => __('Enter the testimonial author\'s designation.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'testimonial_author_designation', // field id and name
                    'type' => 'text' // type of field
                ),
            );
            break;
        case 'product': //WooCommerce uses Old select2
            global $wpdb;
            $courses = array();
            $course_array = $wpdb->get_results("SELECT ID,post_title FROM {$wpdb->posts} WHERE post_type = 'course' AND post_status = 'publish' LIMIT 0,9999");
            if (!empty($course_array)) {
                foreach ($course_array as $course) {
                    $courses[] = array('label' => $course->post_title, 'value' => $course->ID);
                }
            }
            $metabox_settings = array(
                array(// Text Input
                    'label' => __('Associated Courses', 'eLearning-customtypes'), // <label>
                    'desc' => __('Associated Courses with this product. Enables access to the course.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'courses', // field id and name
                    'type' => 'multiselect', // type of field
                    'options' => $courses
                ),
                array(// Text Input
                    'label' => __('Subscription ', 'eLearning-customtypes'), // <label>
                    'desc' => __('Enable if Product is Subscription Type (Price per month)', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'subscription', // field id and name
                    'type' => 'showhide', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                array(// Text Input
                    'label' => __('Subscription Duration', 'eLearning-customtypes'), // <label>
                    'desc' => __('Duration for Subscription Products (in ', 'eLearning-customtypes') . calculate_duration_time($product_duration_parameter) . ')', // description
                    'id' => $prefix . 'duration', // field id and name
                    'type' => 'number' // type of field
                ),
                $prefix . 'product_duration_parameter' => array(// Text Input
                    'label' => __('Product Duration parameter', 'eLearning-customtypes'), // <label>
                    'desc' => __('Duration parameter', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'product_duration_parameter', // field id and name
                    'type' => 'duration', // type of field
                    'std' => $product_duration_parameter
                ),
            );
            break;
        case 'eLearning-event':
            $metabox_settings = array(
                array(// Single checkbox
                    'label' => __('Event Sub-Title', 'eLearning-customtypes'), // <label>
                    'desc' => __('Event Sub-Title.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'subtitle', // field id and name
                    'type' => 'textarea', // type of field
                    'std' => ''
                ),
                array(// Text Input
                    'label' => __('Course', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select Course for which the event is valid', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'event_course', // field id and name
                    'type' => 'selectcpt', // type of field
                    'post_type' => 'course'
                ),
                array(// Text Input
                    'label' => __('Connect an Assignment', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select an Assignment which you can connect with this Event', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'assignment', // field id and name
                    'type' => 'selectcpt', // type of field
                    'post_type' => 'eLearning-assignment'
                ),
                array(// Text Input
                    'label' => __('Event Icon', 'eLearning-customtypes'), // <label>
                    'desc' => __('Click on icon to  select an icon for the event', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'icon', // field id and name
                    'type' => 'icon', // type of field
                ),
                array(// Text Input
                    'label' => __('Event Color', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select color for Event', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'color', // field id and name
                    'type' => 'color', // type of field
                ),
                array(// Text Input
                    'label' => __('Start Date', 'eLearning-customtypes'), // <label>
                    'desc' => __('Date from which Event Begins', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'start_date', // field id and name
                    'type' => 'date', // type of field
                ),
                array(// Text Input
                    'label' => __('End Date', 'eLearning-customtypes'), // <label>
                    'desc' => __('Date on which Event ends.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'end_date', // field id and name
                    'type' => 'date', // type of field
                ),
                array(// Text Input
                    'label' => __('Start Time', 'eLearning-customtypes'), // <label>
                    'desc' => __('Date from which Event Begins', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'start_time', // field id and name
                    'type' => 'time', // type of field
                ),
                array(// Text Input
                    'label' => __('End Time', 'eLearning-customtypes'), // <label>
                    'desc' => __('Date on which Event ends.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'end_time', // field id and name
                    'type' => 'time', // type of field
                ),
                array(// Text Input
                    'label' => __('Show Location', 'eLearning-customtypes'), // <label>
                    'desc' => __('Show Location and Google map with the event', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'show_location', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                array(// Text Input
                    'label' => __('Location', 'eLearning-customtypes'), // <label>
                    'desc' => __('Location of event', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'location', // field id and name
                    'type' => 'gmap' // type of field
                ),
                array(// Text Input
                    'label' => __('Additional Information', 'eLearning-customtypes'), // <label>
                    'desc' => __('Point wise Additional Information regarding the event', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'additional_info', // field id and name
                    'type' => 'repeatable' // type of field
                ),
                array(// Text Input
                    'label' => __('More Information', 'eLearning-customtypes'), // <label>
                    'desc' => __('Supports HTML and shortcodes', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'more_info', // field id and name
                    'type' => 'editor' // type of field
                ),
                array(// Text Input
                    'label' => __('All Day', 'eLearning-customtypes'), // <label>
                    'desc' => __('An all Day event', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'all_day', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                array(// Text Input
                    'label' => __('Private Event', 'eLearning-customtypes'), // <label>
                    'desc' => __('Only Invited participants can see the Event', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'private_event', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
            );



            if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) || (function_exists('is_plugin_active') && is_plugin_active('woocommerce/woocommerce.php'))) {
                $metabox_settings[] = array(
                    'label' => __('Associated Product for Event Access', 'eLearning-customtypes'), // <label>
                    'desc' => __('Purchase of this product grants Event access to the member.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'product', // field id and name
                    'type' => 'selectcpt', // type of field
                    'post_type' => 'product',
                    'std' => ''
                );
            }
            break;
        case 'payments':
            $metabox_settings = array(
                array(// Text Input
                    'label' => __('From', 'eLearning-customtypes'), // <label>
                    'desc' => __('Date on which Payment was done.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'date_from', // field id and name
                    'type' => 'text', // type of field
                ),
                array(// Text Input
                    'label' => __('To', 'eLearning-customtypes'), // <label>
                    'desc' => __('Date on which Payment was done.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'date_to', // field id and name
                    'type' => 'text', // type of field
                ),
                array(// Text Input
                    'label' => __('Instructor and Commissions', 'eLearning-customtypes'), // <label>
                    'desc' => __('Instructor commissions', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'instructor_commissions', // field id and name
                    'type' => 'payments' // type of field
                ),
            );
            break;
        case 'certificate':
            $metabox_settings = array(
                array(// Text Input
                    'label' => __('Background Image/Pattern', 'eLearning-customtypes'), // <label>
                    'desc' => __('Add background image', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'background_image', // field id and name
                    'type' => 'image', // type of field
                ),
                array(// Text Input
                    'label' => __('Enable Print & PDF', 'eLearning-customtypes'), // <label>
                    'desc' => __('Displays a Print and Download as PDF Button on top right corner of certificate', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'print', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                array(// Text Input
                    'label' => __('Certificate Width', 'eLearning-customtypes'), // <label>
                    'desc' => __('Add certificate width', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'certificate_width', // field id and name
                    'type' => 'text', // type of field
                ),
                array(// Text Input
                    'label' => __('Certificate Height', 'eLearning-customtypes'), // <label>
                    'desc' => __('Add certificate height', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'certificate_height', // field id and name
                    'type' => 'text', // type of field
                ),
                array(// Text Input
                    'label' => __('Custom Class', 'eLearning-customtypes'), // <label>
                    'desc' => __('Add Custom Class over Certificate container.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'custom_class', // field id and name
                    'type' => 'text', // type of field
                ),
                array(// Text Input
                    'label' => __('Custom CSS', 'eLearning-customtypes'), // <label>
                    'desc' => __('Add Custom CSS for Certificate.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'custom_css', // field id and name
                    'type' => 'textarea', // type of field
                ),
                array(// Text Input
                    'label' => __('NOTE:', 'eLearning-customtypes'), // <label>
                    'desc' => __(' USE FOLLOWING SHORTCODES TO DISPLAY RELEVANT DATA : <br />1. <strong>[certificate_student_name]</strong> : Displays Students Name<br />2. <strong>[certificate_course]</strong> : Displays Course Name<br />3. <strong>[certificate_student_marks]</strong> : Displays Students Marks in Course<br />4. <strong>[certificate_student_date]</strong>: Displays date on which Certificate was awarded to the Student<br />5. <strong>[certificate_student_email]</strong>: Displays registered email of the Student<br />6. <strong>[certificate_code]</strong>: Generates unique code for Student which can be validated from Certificate page.<br />7. <strong>[course_completion_date]</strong>: Displays course completion date from course activity.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'note', // field id and name
                    'type' => 'note', // type of field
                ),
            );
            break;
        case 'eLearning-assignment':
            $max_upload = (int) (ini_get('upload_max_filesize'));
            $max_post = (int) (ini_get('post_max_size'));
            $memory_limit = (int) (ini_get('memory_limit'));
            $upload_mb = min($max_upload, $max_post, $memory_limit);
            $metabox_settings = array(
                array(// Single checkbox
                    'label' => __('Assignment Sub-Title', 'eLearning-customtypes'), // <label>
                    'desc' => __('Assignment Sub-Title.', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'subtitle', // field id and name
                    'type' => 'textarea', // type of field
                    'std' => ''
                ),
                array(// Single checkbox
                    'label' => __('Sidebar', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select a Sidebar | Default : mainsidebar', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'sidebar', // field id and name
                    'type' => 'select',
                    'options' => $sidebararray
                ),
                array(// Text Input
                    'label' => __('Assignment Maximum Marks', 'eLearning-customtypes'), // <label>
                    'desc' => __('Set Maximum marks for the assignment', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'assignment_marks', // field id and name
                    'type' => 'number', // type of field
                    'std' => '10'
                ),
                array(// Text Input
                    'label' => sprintf(__('Assignment Maximum Time limit %s', 'eLearning-customtypes'), '( ' . calculate_duration_time($assignment_duration_parameter) . ' )'), // <label>
                    'desc' => __('Set Maximum Time limit for Assignment ( in ', 'eLearning-customtypes') . calculate_duration_time($assignment_duration_parameter) . ' )', // description
                    'id' => $prefix . 'assignment_duration', // field id and name
                    'type' => 'number', // type of field
                    'std' => '10'
                ),
                $prefix . 'assignment_duration_parameter' => array(// Text Input
                    'label' => __('Assignment Duration parameter', 'eLearning-customtypes'), // <label>
                    'desc' => __('Duration parameter', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'assignment_duration_parameter', // field id and name
                    'type' => 'duration', // type of field
                    'std' => $assignment_duration_parameter
                ),
                array(// Text Input
                    'label' => __('Include in Course Evaluation', 'eLearning-customtypes'), // <label>
                    'desc' => __('Include assignment marks in Course Evaluation', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'assignment_evaluation', // field id and name
                    'type' => 'yesno', // type of field
                    'options' => array(
                        array('value' => 'H',
                            'label' => __('Hide', 'eLearning-customtypes')),
                        array('value' => 'S',
                            'label' => __('Show', 'eLearning-customtypes')),
                    ),
                    'std' => 'H'
                ),
                array(// Text Input
                    'label' => __('Include in Course', 'eLearning-customtypes'), // <label>
                    'desc' => __('Assignments marks will be shown/used in course evaluation', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'assignment_course', // field id and name
                    'post_status' => array('publish', 'draft'),
                    'type' => 'selectcpt', // type of field
                    'post_type' => 'course'
                ),
                array(// Single checkbox
                    'label' => __('Assignment Submissions', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select type of assignment submissions', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'assignment_submission_type', // field id and name
                    'type' => 'select', // type of field
                    'options' => array(
                        1 => array('label' => 'Upload file', 'value' => 'upload'),
                        2 => array('label' => 'Text Area', 'value' => 'textarea'),
                    ),
                    'std' => ''
                ),
                array(// Text Input
                    'label' => __('Attachment Type', 'eLearning-customtypes'), // <label>
                    'desc' => __('Select valid attachment types ', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'attachment_type', // field id and name
                    'type' => 'multiselect', // type of field
                    'options' => array(
                        array('value' => 'JPG', 'label' => 'JPG'),
                        array('value' => 'GIF', 'label' => 'GIF'),
                        array('value' => 'PNG', 'label' => 'PNG'),
                        array('value' => 'PDF', 'label' => 'PDF'),
                        array('value' => 'PSD', 'label' => 'PSD'),
                        array('value' => 'DOC', 'label' => 'DOC'),
                        array('value' => 'DOCX', 'label' => 'DOCX'),
                        array('value' => 'PPT', 'label' => 'PPT'),
                        array('value' => 'PPTX', 'label' => 'PPTX'),
                        array('value' => 'PPS', 'label' => 'PPS'),
                        array('value' => 'PPSX', 'label' => 'PPSX'),
                        array('value' => 'ODT', 'label' => 'ODT'),
                        array('value' => 'XLS', 'label' => 'XLS'),
                        array('value' => 'XLSX', 'label' => 'XLSX'),
                        array('value' => 'MP3', 'label' => 'MP3'),
                        array('value' => 'M4A', 'label' => 'M4A'),
                        array('value' => 'OGG', 'label' => 'OGG'),
                        array('value' => 'WAV', 'label' => 'WAV'),
                        array('value' => 'WMA', 'label' => 'WMA'),
                        array('value' => 'MP4', 'label' => 'MP4'),
                        array('value' => 'M4V', 'label' => 'M4V'),
                        array('value' => 'MOV', 'label' => 'MOV'),
                        array('value' => 'WMV', 'label' => 'WMV'),
                        array('value' => 'AVI', 'label' => 'AVI'),
                        array('value' => 'MPG', 'label' => 'MPG'),
                        array('value' => 'OGV', 'label' => 'OGV'),
                        array('value' => '3GP', 'label' => '3GP'),
                        array('value' => '3G2', 'label' => '3G2'),
                        array('value' => 'FLV', 'label' => 'FLV'),
                        array('value' => 'WEBM', 'label' => 'WEBM'),
                        array('value' => 'APK', 'label' => 'APK '),
                        array('value' => 'RAR', 'label' => 'RAR'),
                        array('value' => 'ZIP', 'label' => 'ZIP'),
                    ),
                    'std' => 'single'
                ),
                array(// Text Input
                    'label' => __('Attachment Size (in MB)', 'eLearning-customtypes'), // <label>
                    'desc' => __('Set Maximum Attachment size for upload ( set less than ', 'eLearning-customtypes') . $upload_mb . ' MB)', // description
                    'id' => $prefix . 'attachment_size', // field id and name
                    'type' => 'number', // type of field
                    'std' => '2'
                ),
            );
            break;
        case 'popup':
            $metabox_settings = array(
                array(// Text Input
                    'label' => __('Width (in px)', 'eLearning-customtypes'), // <label>
                    'desc' => __('Set Maximum width of popup', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'popup_width', // field id and name
                    'type' => 'number', // type of field
                    'std' => '480'
                ),
                array(// Text Input
                    'label' => __('Height (in px)', 'eLearning-customtypes'), // <label>
                    'desc' => __('Set Maximum height of popup ', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'popup_height', // field id and name
                    'type' => 'number', // type of field
                    'std' => '600'
                ),
                array(// Text Input
                    'label' => __('Custom Class', 'eLearning-customtypes'), // <label>
                    'desc' => __('Add custom class to popup ', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'popup_class', // field id and name
                    'type' => 'text', // type of field
                    'std' => ''
                ),
                array(// Single checkbox
                    'label' => __('Add Custom CSS', 'eLearning-customtypes'), // <label>
                    'desc' => __('Custom CSS for Popup', 'eLearning-customtypes'), // description
                    'id' => $prefix . 'custom_css', // field id and name
                    'type' => 'textarea', // type of field
                    'std' => ''
                ),
            );
            break;
    }
    return apply_filters('eLearning_' . $metabox . '_metabox', $metabox_settings);
}

function add_eLearning_metaboxes() {

    $prefix = 'eLearning_';
    $post_metabox = eLearning_meta_box_arrays('post');
    $page_metabox = eLearning_meta_box_arrays('page');
    $course_metabox = eLearning_meta_box_arrays('course');
    $course_product_metabox = eLearning_meta_box_arrays('course_product');
    $unit_metabox = eLearning_meta_box_arrays('unit');
    $question_metabox = eLearning_meta_box_arrays('question');
    $quiz_metabox = eLearning_meta_box_arrays('quiz');
    $testimonial_metabox = eLearning_meta_box_arrays('testimonial');
    $product_metabox = eLearning_meta_box_arrays('product');
    $eLearning_events_metabox = eLearning_meta_box_arrays('eLearning-event');
    $payments_metabox = eLearning_meta_box_arrays('payments');
    $certificate_metabox = eLearning_meta_box_arrays('certificate');
    $eLearning_assignments_metabox = eLearning_meta_box_arrays('eLearning-assignment');
    $eLearning_popup_metabox = eLearning_meta_box_arrays('popup');

    $dwqna_custom_metabox = array(
        array(// Text Input
            'label' => __('Connected Course', 'eLearning-customtypes'), // <label>
            'desc' => __('Connect this question to a course', 'eLearning-customtypes'), // description
            'id' => $prefix . 'question_course', // field id and name
            'type' => 'selectcpt', // type of field
            'post_type' => 'course'
        ),
        array(// Text Input
            'label' => __('Connected Unit', 'eLearning-customtypes'), // <label>
            'desc' => __('Connect this question to a Unit', 'eLearning-customtypes'), // description
            'id' => $prefix . 'question_unit', // field id and name
            'type' => 'selectcpt', // type of field
            'post_type' => 'unit'
        ),
    );


    $post_metabox = new custom_add_meta_box('post-settings', __('Post Settings', 'eLearning-customtypes'), $post_metabox, 'post', true);
    $page_metabox = new custom_add_meta_box('page-settings', __('Page Settings', 'eLearning-customtypes'), $page_metabox, 'page', true);

    $course_box = new custom_add_meta_box('page-settings', __('Course Settings', 'eLearning-customtypes'), $course_metabox, 'course', true);

    $course_product = __('Course Product', 'eLearning-customtypes');
    if (function_exists('pmpro_getAllLevels')) {
        $course_product = __('Course Membership', 'eLearning-customtypes');
    }
    $course_product_box = new custom_add_meta_box('post-settings', $course_product, $course_product_metabox, 'course', true);
    $unit_box = new custom_add_meta_box('page-settings', __('Unit Settings', 'eLearning-customtypes'), $unit_metabox, 'unit', true);

    $question_box = new custom_add_meta_box('page-settings', __('Question Settings', 'eLearning-customtypes'), $question_metabox, 'question', true);
    $quiz_box = new custom_add_meta_box('page-settings', __('Quiz Settings', 'eLearning-customtypes'), $quiz_metabox, 'quiz', true);

    if (post_type_exists('dwqa-question'))
        $dwqna_custom_box = new custom_add_meta_box('page-settings', __('Settings', 'eLearning-customtypes'), $dwqna_custom_metabox, 'dwqa-question', false);

    $testimonial_box = new custom_add_meta_box('testimonial-info', __('Testimonial Author Information', 'eLearning-customtypes'), $testimonial_metabox, 'testimonials', true);
    $payments_metabox = new custom_add_meta_box('page-settings', __('Payments Settings', 'eLearning-customtypes'), $payments_metabox, 'payments', true);
    $certificates_metabox = new custom_add_meta_box('page-settings', __('Certificate Template Settings', 'eLearning-customtypes'), $certificate_metabox, 'certificate', true);
    $popup_metabox = new custom_add_meta_box('page-settings', __('Popup Settings', 'eLearning-customtypes'), $eLearning_popup_metabox, 'popups', true);


    if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) || (function_exists('is_plugin_active') && is_plugin_active('woocommerce/woocommerce.php'))) {
        $flag = apply_filters('eLearning_woocommerce_enable_pricing', 1);
        if ($flag) {
            $product_box = new custom_add_meta_box('page-settings', __('Product Course Settings', 'eLearning-customtypes'), $product_metabox, 'product', true);
        }
    }

    if (in_array('eLearning-events/eLearning-events.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        $events_metabox = new custom_add_meta_box('page-settings', __('ELEARNING Events Settings', 'eLearning-customtypes'), $eLearning_events_metabox, 'eLearning-event', true);
    }


    if (in_array('eLearning-assignments/eLearning-assignments.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        $eassignments_metabox = new custom_add_meta_box('page-settings', __('ELEARNING Assignments Settings', 'eLearning-customtypes'), $eLearning_assignments_metabox, 'eLearning-assignment', true);
    }
}

add_action('init', 'add_eLearning_metaboxes');


add_action('add_meta_boxes', 'add_eLearning_editor');
if (!function_exists('add_eLearning_editor')) {

    function add_eLearning_editor() {
        $page_builder = ELEARNING_Page_Builder::init();
        add_meta_box('eLearning-editor', __('Page Builder', 'eLearning-customtypes'), array($page_builder, 'eLearning_layout_editor'), 'page', 'normal', 'high');
    }

}

function attachment_getMaximumUploadFileSize() {
    $maxUpload = (int) (ini_get('upload_max_filesize'));
    $maxPost = (int) (ini_get('post_max_size'));
    $memoryLimit = (int) (ini_get('memory_limit'));
    return min($maxUpload, $maxPost, $memoryLimit);
}
