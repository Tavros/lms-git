<?php

if ( !defined( 'ABSPATH' ) ) exit;
/*  Copyright 2013 ELearningThemes  (email: pr@peyotto.com) */

if( !defined('ELEARNING_CUSTOMTYPE_URL')){
    define('ELEARNING_CUSTOMTYPE_URL',get_template_directory_uri().'/eLearning-addons');
}

/*====== BEGIN INCLUDING FILES ======*/


include_once 'custom-post-types.php'; 
include_once('includes/errorhandle.php');
include_once('includes/featured.php');
include_once('includes/statistics.php');
include_once('includes/musettings.php');
include_once('includes/course_settings.php');
include_once('includes/profile_menu_dropdown_settings.php');
include_once('includes/permalinks.php');
include_once('includes/caching.php');
include_once('includes/tips.php');
include_once('metaboxes/meta_box.php');
include_once('metaboxes/library/eLearning-editor.php');
include_once('custom_meta_boxes.php');

/*====== INSTALLATION HOOKs ======*/        

//register_activation_hook(__FILE__,'register_lms');
//register_activation_hook(__FILE__,'register_popups', 11);
//register_activation_hook(__FILE__,'register_testimonials', 12);
//register_activation_hook(__FILE__,'flush_rewrite_rules', 20);

if(!function_exists('animation_effects')){
    function animation_effects(){
        $animate=array(
                        ''=>'none',
                        'animate cssanim flash'=> 'Flash',
                        'animate zoom' => 'Zoom',
                        'animate scale' => 'Scale',
                        'animate slide' => 'Slide (Height)', 
                        'animate expand' => 'Expand (Width)',
                        'animate cssanim shake'=> 'Shake',
                        'animate cssanim bounce'=> 'Bounce',
                        'animate cssanim tada'=> 'Tada',
                        'animate cssanim swing'=> 'Swing',
                        'animate cssanim wobble'=> 'Flash',
                        'animate cssanim wiggle'=> 'Flash',
                        'animate cssanim pulse'=> 'Flash',
                        'animate cssanim flip'=> 'Flash',
                        'animate cssanim flipInX'=> 'Flip Left',
                        'animate cssanim flipInY'=> 'Flip Top',
                        'animate cssanim fadeIn'=> 'Fade',
                        'animate cssanim fadeInUp'=> 'Fade Up',
                        'animate cssanim fadeInDown'=> 'Fade Down',
                        'animate cssanim fadeInLeft'=> 'Fade Left',
                        'animate cssanim fadeInRight'=> 'Fade Right',
                        'animate cssanim fadeInUptBig'=> 'Fade Big Up',
                        'animate cssanim fadeInDownBig'=> 'Fade Big Down',
                        'animate cssanim fadeInLeftBig'=> 'Fade Big Left',
                        'animate cssanim fadeInRightBig'=> 'Fade Big Right',
                        'animate cssanim bounceInUp'=> 'Bounce Up',
                        'animate cssanim bounceInDown'=> 'Bounce Down',
                        'animate cssanim bounceInLeft'=> 'Bounce Left',
                        'animate cssanim bounceInRight'=> 'Bounce Right',
                        'animate cssanim rotateIn'=> 'Rotate',
                        'animate cssanim rotateInUpLeft'=> 'Rotate Up Left',
                        'animate cssanim rotateInUpRight'=> 'Rotate Up Right',
                        'animate cssanim rotateInDownLeft'=> 'Rotate Down Left',
                        'animate cssanim rotateInDownRight'=> 'Rotate Down Right',
                        'animate cssanim speedIn'=> 'Speed In',
                        'animate cssanim rollIn'=> 'Roll In',
                        'animate ltr'=> 'Left To Right',
                        'animate rtl' => 'Right to Left', 
                        'animate btt' => 'Bottom to Top',
                        'animate ttb'=>'Top to Bottom',
                        'animate smallspin'=> 'Small Spin',
                        'animate spin'=> 'Infinite Spin'
                        );
    return $animate;
    }
}

add_action('after_setup_theme', 'load_eLearning_customtypes_textdomain');

function load_eLearning_customtypes_textdomain() {
    load_textdomain('eLearning-customtypes', dirname(__FILE__) . '/languages/');
}