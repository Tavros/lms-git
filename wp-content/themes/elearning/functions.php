<?php
$view_errors = (isset($_REQUEST['verror']));
if($view_errors){
    error_reporting(E_ALL);
}

function fm_print_die($variable){
    echo "<pre>";
    print_r($variable);die;
}
function fm_print_r($variable){
    echo "<pre>";
    print_r($variable);
}

if (!defined('ABSPATH'))
    exit;

// Essentials
include_once 'includes/config.php';
include_once 'includes/init.php';

// Register & Functions
include_once 'includes/register.php';
include_once 'includes/actions.php';
include_once 'includes/filters.php';
include_once 'includes/func.php';
include_once 'includes/ratings.php';
// Customizer
include_once 'includes/customizer/customizer.php';
include_once 'includes/customizer/css.php';
include_once 'includes/eLearning-menu.php';
include_once 'includes/notes-discussions.php';
include_once 'includes/eLearning-woocommerce-checkout.php';

include_once 'eLearning-addons/onstreamAPI/functions.php';
include_once 'eLearning-addons/eLearning-dashboard/eLearning-dashboard.php';

if (function_exists('bp_is_active')) {


// eLearning Dashboard
    include_once 'eLearning-addons/account_dashboar/dashboard.php';
    //  include_once 'eLearning-addons/messagecenter/messagecenter.php';

    include_once 'eLearning-addons/shortcodes/eLearning-shortcodes.php';
    include_once 'eLearning-addons/events/events.php';
    include_once 'eLearning-addons/front-end/eLearning-front-end.php';
    include_once 'eLearning-addons/assignments/eLearning-assignments.php';
    include_once 'eLearning-addons/course-module/loader.php';
    include_once 'eLearning-addons/customtypes/eLearning-customtypes.php';
    include_once 'eLearning-addons/customlinks/customlink.php';

    if (checkUserRole('Student') || checkUserRole('instructor')) {
        include_once 'eLearning-addons/account/account.php';
        include_once 'eLearning-addons/profile/profile.php';
        include_once 'eLearning-addons/myfiles/myfiles.php';
        include_once 'eLearning-addons/mynetwork/mynetwork.php';
        include_once 'eLearning-addons/referrals/referrals.php';
        //include_once 'eLearning-addons/sessions/sessions.php';
    }

    if (is_super_admin()) {
        include_once 'eLearning-addons/sales/sales.php';
        include_once 'eLearning-addons/marketing/marketing.php';
        include_once 'eLearning-addons/users/users.php';
        include_once 'eLearning-addons/admin-settings/admin-settings.php';
    }
}


if (function_exists('bp_get_signup_allowed')) {
    include_once 'includes/bp-custom.php';
}

include_once '_inc/ajax.php';
include_once 'includes/buddydrive.php';
//Widgets
include_once('includes/widgets/custom_widgets.php');
if (function_exists('bp_get_signup_allowed')) {
    include_once('includes/widgets/custom_bp_widgets.php');
}
if (function_exists('pmpro_hasMembershipLevel')) {
    include_once('includes/pmpro-connect.php');
}
include_once('includes/widgets/advanced_woocommerce_widgets.php');
include_once('includes/widgets/twitter.php');
include_once('includes/widgets/flickr.php');

//Misc
include_once 'includes/extras.php';
include_once 'includes/tincan.php';
include_once 'setup/eLearning-install.php';

include_once 'setup/installer/envato_setup.php';

// Options Panel
get_template_part('eLearning', 'options');

function rt_login_redirect($redirect_to, $set_for, $user) {



    $redirect_to = bp_core_get_user_domain($user->ID);
    if(checkUserRole_log_in("administrator",$user)){
        $redirect_to .="/account-dashboard";
    }else if(checkUserRole_log_in("student",$user) == 1){
         $redirect_to =site_url()."/events";
     }

//    echo "<pre>"; print_r($user);die;
    return $redirect_to;
}

add_filter("login_redirect", 'rt_login_redirect', 20, 3);

function checkUserRole($role) {
    $user = wp_get_current_user();
    if (in_array($role, (array) $user->roles)) {
        return true;
    }
    return false;
}
function checkUserRole_log_in($role,$user) {
    //$user = wp_get_current_user();
    if (in_array($role, (array) $user->roles)) {
        return true;
    }
    return false;
}
add_filter('bp_core_enable_root_profiles', '__return_true');


//if(! function_exists('getCustomBodypressMenu')) {

/* Functions from elearning  child   START*/


//Add Action wp_enqueue_scripts
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

//Declare theme_enqueue_styles function (include parent and css files)
function theme_enqueue_styles() {
    wp_enqueue_style( 'popup1', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', '' , 555 );
    wp_enqueue_style( 'popup2', 'https://jqueryui.com/resources/demos/style.css', '' , 555 );
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'Ando', get_template_directory_uri() . '/Ando.css' );
    wp_enqueue_style( 'child-style',get_stylesheet_directory_uri() . '/style.css', array('parent-style'));

    wp_enqueue_style( 'yit-plugin-panel-sidebar', get_template_directory_uri() .'/assets/css/yit-plugin-panel-sidebar.css' );
    wp_enqueue_style( 'yit-chosen', get_template_directory_uri() . '/assets/css/chosen.css');
    wp_enqueue_style( 'yit-licence', get_template_directory_uri() . '/assets/css/yit-licence.css');
    wp_enqueue_style( 'yit-admin', get_template_directory_uri() .'/assets/css/admin.css' );
}//End theme_enqueue_styles function


function enqueue_media_uploader()
{
    //this function enqueues all scripts required for media uploader to work
    wp_enqueue_media();
}

add_action("wp_enqueue_scripts", "enqueue_media_uploader");

//Add Action wp_enqueue_scripts
add_action( 'wp_enqueue_scripts', 'fm_theme_enqueue_styles' );

//Declare fm_theme_enqueue_styles function (include child theme css files)
function fm_theme_enqueue_styles() {  

    wp_enqueue_script( 'custom_js', get_template_directory_uri() . '/custom_js.js');
    wp_enqueue_script( 'dataTablesJS', get_template_directory_uri() . '/elearning_js/jquery.dataTables.min.js');
    wp_enqueue_script( 'jquery-steps', get_template_directory_uri() . '/elearning_js/jquery.steps.min.js');
    wp_enqueue_script( 'timezons', get_template_directory_uri() . '/elearning_js/timezones.full.js');
    wp_enqueue_script( 'moment', get_template_directory_uri() . '/elearning_js/moment.js'); 
    wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/elearning_js/jquery-ui.min.js');
    wp_enqueue_script( 'buttonsDatTable', get_template_directory_uri() . '/elearning_js/buttons.min.js');
    wp_enqueue_script( 'printDatTable', get_template_directory_uri() . '/elearning_js/buttons.html5.min.js');
    wp_enqueue_style( "contact_form_styles","/wp-content/plugins/contact-form-7/admin/css/styles.css" );
    wp_enqueue_style( "contact_form_styles_rtl","/wp-content/plugins/contact-form-7/admin/css/styles-rtl.css" );
    wp_enqueue_script( "contact_form_scripts",get_template_directory_uri() ."/form_builder_js/scripts.js" );
    wp_enqueue_script( "contact_form_tag-generator",get_template_directory_uri() ."/form_builder_js/tag-generator.js" );
    wp_enqueue_script( 'popup', get_template_directory_uri() . '/elearning_js/dialog-jquery-ui.js');
}//End fm_theme_enqueue_styles function

function fm_theme_enqueue_styles_color() { 
    wp_enqueue_script( 'iris_min', get_site_url() . '/wp-admin/js/iris.min.js');
    wp_enqueue_script( 'jquery_blockUI', get_site_url() . '/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js');
    wp_enqueue_script( 'yit-licence_min', get_site_url() . '/wp-content/plugins/yith-woocommerce-email-templates-premium/plugin-fw/licence/assets/js/yit-licence.min.js');
     
    wp_enqueue_script( 'chosen_jquery', get_site_url() . '/wp-content/plugins/yith-woocommerce-email-templates-premium/plugin-fw/assets/js/chosen/chosen.jquery.js');
    wp_enqueue_script( 'settings', get_site_url() . '/wp-content/plugins/woocommerce/assets/js/admin/settings.min.js');
    wp_enqueue_script( 'color-picker', get_template_directory_uri() . '/elearning_js/color-picker.min.js');  
    wp_enqueue_script( 'wp_color_picker', get_template_directory_uri() . '/elearning_js/wp-color-picker.js');
    // wp_enqueue_script( 'yit-plugin-panel-sidebar', get_site_url() . '/wp-content/plugins/yith-woocommerce-email-templates-premium/plugin-fw/assets/js/yit-plugin-panel-sidebar.min.js');
    wp_enqueue_script( 'metabox_options_premium', get_site_url() . '/wp-content/plugins/yith-woocommerce-email-templates-premium/assets/js/metabox_options_premium.js');
    
}//End fm_theme_enqueue_styles_color function
add_action( 'wp_enqueue_scripts', 'fm_theme_enqueue_styles_color');

function wpa82718_scripts() { 
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 
		'iris',
		admin_url( 'js/iris.min.js' ),
		array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ),
		false,
		1
	);
	wp_enqueue_script(
		'wp-color-picker',
		admin_url( 'js/color-picker.min.js' ),
		array( 'iris' ),
		false,
		1
	); 
	$colorpicker_l10n = array(
		'clear' => __( 'Clear' ),
		'defaultString' => __( 'Default' ),
		'pick' => __( 'Select Color' ),
		'current' => __( 'Current Color' )
	);
	wp_localize_script( 'wp-color-picker', 'wpColorPickerL10n', $colorpicker_l10n ); 

}
add_action( 'wp_enqueue_scripts', 'wpa82718_scripts',100 );

/*
Declare getCustomBodypressMenu function

*/
function getCustomBodypressMenu_child() {
    $bp = buddypress();
//
//            echo "<pre>"; 
//        print_r($bp->members->nav->get_primary());die;

    foreach ( $bp->members->nav->get_primary() as $user_nav_item ) {

        $delete_menus = array('marketing', 'sales', 'courses', 'course', 'edit-course',);

        if ( in_array($user_nav_item->slug, $delete_menus) ) {
            continue;
        }

        if ( empty( $user_nav_item->show_for_displayed_user ) && !bp_is_my_profile() ) {
            continue;
        }

        $selected = '';
        if ( bp_is_current_component( $user_nav_item->slug ) ) {
            $selected = ' class="current selected"';
        }

        if ( bp_loggedin_user_domain() ) {
            $link = str_replace( bp_loggedin_user_domain(), bp_displayed_user_domain(), $user_nav_item->link );
        } else {
            $link = trailingslashit( bp_displayed_user_domain() . $user_nav_item->link );
        }

        /**
         * Filters the navigation markup for the displayed user.
         *
         * This is a dynamic filter that is dependent on the navigation tab component being rendered.
         *
         * @since 1.1.0
         *
         * @param string $value Markup for the tab list item including link.
         * @param array $user_nav_item Array holding parts used to construct tab list item.
         * Passed by reference.
         */

        $current_user_login = wp_get_current_user()->user_login;
//            echo "<pre>";
//        if ( $user_nav_item->slug == 'profile' ) {
////        print_r($user_nav_item->slug);die;
//
//        }
//        else
        if ( $user_nav_item->slug == 'buddydrive' ) {

            echo apply_filters_ref_array( 'bp_get_displayed_user_nav_' . $user_nav_item->css_id, array( '<li id="' . $user_nav_item->css_id . '-personal-li" ' . $selected . '><a id="user-' . $user_nav_item->css_id . '" href="' . $link . '">' . __( 'File', 'elearning' ) . '</a></li>', &$user_nav_item ) );

        } else {

            if ( $user_nav_item->name == "Users" ) {

                $user_nav_item->name = "Registrants";
            }

            if ( $user_nav_item->name == "Settings" ) {

//                $link = "/'.$current_user_login.'/?page=settings-integrations";
            }
//            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="' . $user_nav_item->css_id . '-personal-li" ' . $selected . '><a id="user-' . $user_nav_item->css_id . '" href="' . $link . '"><img src="http://' . $_SERVER['HTTP_HOST'] . '/wp-content/themes/elearning/images/icons/' . $user_nav_item->css_id . '/' . $user_nav_item->css_id . '.png" class="cus_icon">' . $user_nav_item->name . '</a></li>', &$user_nav_item));

        }

    }
    $userTypeChekcer = false;
    $currentUserType = wp_get_current_user()->roles[0];
    if($currentUserType == 'administrator'){
        $userTypeChekcer = true;
    }
    elseif($currentUserType == 'instructor'){
        $userTypeChekcer = true;
    }
    isset($_REQUEST['page']) && !empty($_REQUEST['page']) ? $postType = $_REQUEST['page'] : $postType = '';
    if ( $_SERVER['REQUEST_URI']== "/".$current_user_login."/account-dashboard/" ) {
        echo apply_filters_ref_array( 'bp_get_displayed_user_nav_accountSettings', array( '<li id="website-personal-li_dashboard" class="current selected"><a id="user-website" href="/'.$current_user_login.'/account-dashboard/"><img src="/wp-content/themes/elearning/images/icons/account-dashboard/account-dashboard.png" class="cus_icon">DASHBOARD</a></li>' ) );
    } else {
        echo apply_filters_ref_array( 'bp_get_displayed_user_nav_accountSettings', array( '<li id="website-personal-li_dashboard" ><a id="user-website" href="/'.$current_user_login.'/account-dashboard/"><img src="/wp-content/themes/elearning/images/icons/account-dashboard/account-dashboard.png" class="cus_icon">DASHBOARD</a></li>'));
    }

//        if ( $_SERVER['REQUEST_URI'] == "/".$current_user_login."/blog/" ) {
//            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="profile-personal-li" class="current selected"><a id="members_level" href="/'.$current_user_login.'/blog/"><img src="/wp-content/themes/elearning/images/icons/accountSettings/profile.png" class="cus_icon">BLOG</a></li>'));
//        } else {
//            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="profile-personal-li"><a id="members_level" href="/'.$current_user_login.'/blog/"><img src="/wp-content/themes/elearning/images/icons/accountSettings/profile.png" class="cus_icon">BLOG</a></li>'));
//        }
    if($userTypeChekcer == true) {
        if ($_SERVER['REQUEST_URI'] == "/" . $current_user_login . "/events/") {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_events" class="current selected"><a id="user-website" href="/' . $current_user_login . '/events/"><img src="/wp-content/themes/elearning/images/icons/events/events.png" class="cus_icon">EVENTS</a></li>'));
        } else {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_events" ><a id="user-website" href="/' . $current_user_login . '/events/"><img src="/wp-content/themes/elearning/images/icons/events/events.png" class="cus_icon">EVENTS</a></li>'));
        }
    }
    if($userTypeChekcer == true){
        if ( $_REQUEST['page'] == "blog-page" || $_SERVER['REQUEST_URI'] == '/blog/frontend-publishing/edit/1/' ) {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_blog-page" class="current selected"><a id="user-website" href="/'.$current_user_login.'?page=blog-page"><img src="/wp-content/themes/elearning/images/icons/users/blog.png" class="cus_icon">blog</a></li>'));
        } else {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_blog-page" ><a id="user-website" href="/'.$current_user_login.'?page=blog-page"><img src="/wp-content/themes/elearning/images/icons/users/blog.png" class="cus_icon">blog</a></li>'));
        }
    }
    if($userTypeChekcer == true) {
        if ($_SERVER['REQUEST_URI'] == "/" . $current_user_login . "/users/") {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_users" class="current selected"><a id="user-website" href="/' . $current_user_login . '/users/"><img src="/wp-content/themes/elearning/images/icons/users/users.png" class="cus_icon">REGISTRANTS</a></li>'));
        } else {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_users" ><a id="user-website" href="/' . $current_user_login . '/users/"><img src="/wp-content/themes/elearning/images/icons/users/users.png" class="cus_icon">REGISTRANTS</a></li>'));
        }
    }

    if ( $_SERVER['REQUEST_URI'] == "/".$current_user_login."/website/"  ) {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_website" class="current selected"><a id="user-website" href="/'.$current_user_login.'/website"><img src="/wp-content/themes/elearning/images/icons/accountSettings/website.png" class="cus_icon">WEBSITE</a></li>'));
    } else {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_website" ><a id="user-website" href="/'.$current_user_login.'/website"><img src="/wp-content/themes/elearning/images/icons/accountSettings/website.png" class="cus_icon">WEBSITE</a></li>'));
    }
 
  /*  if ( $postType == "profile" ) {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="profile-personal-li_profile" class="current selected"><a id="user-profile" href="/'.$current_user_login.'/profile"><img src="/wp-content/themes/elearning/images/icons/accountSettings/profile.png" class="cus_icon">PROFILE</a></li>'));
    } else {
        echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="profile-personal-li_profile"><a id="user-profile" href="/'.$current_user_login.'/profile"><img src="/wp-content/themes/elearning/images/icons/accountSettings/profile.png" class="cus_icon">PROFILE</a></li>'));
     }*/
    if($userTypeChekcer == true) {
        if ($_SERVER['REQUEST_URI'] == "/" . $current_user_login . "/membership-level/") {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="profile-personal-li_membership-level" class="current selected"><a id="members_level" href="/' . $current_user_login . '/membership-level"><img src="/wp-content/themes/elearning/images/icons/users/mem_lev.png" class="cus_icon">MEMBERSHIP</a></li>'));
        } else {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="profile-personal-li_membership-level"><a id="members_level" href="/' . $current_user_login . '/membership-level"><img src="/wp-content/themes/elearning/images/icons/users/mem_lev.png" class="cus_icon">MEMBERSHIP</a></li>'));
        }

        if (  $_SERVER['REQUEST_URI'] == "/".$current_user_login."/settings-integrations/"  ) {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_settings-integrations" class="current selected"><a id="user-website" href="/'.$current_user_login.'/settings-integrations"><img src="/wp-content/themes/elearning/images/icons/accountSettings/accountSettings.png" class="cus_icon">SETTINGS</a></li>'));
        } else {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_accountSettings', array('<li id="website-personal-li_settings-integrations" ><a id="user-website" href="/'.$current_user_login.'/settings-integrations"><img src="/wp-content/themes/elearning/images/icons/accountSettings/accountSettings.png" class="cus_icon">SETTINGS</a></li>'));
        }
    }
}

/*
 * Make website url's friendly
 *
 *
 *
 * **/

function members_custom_url() {
    $url = $_SERVER['REQUEST_URI'];
    $current_user_login = wp_get_current_user()->user_login;
    if(isset($url) && $url != '' ) {
        $file_name_array = explode('/', $url);
        $before_filename = $file_name_array[1];
        $file_name = $file_name_array[2];

    }
    if($file_name != 'setting' && is_string($file_name) && $before_filename == $current_user_login) {
        do_action('eLearning_before_member_profile');
        get_header(eLearning_get_header());
        $profile_layout = eLearning_get_customizer('profile_layout');

        eLearning_include_template("profile/top$profile_layout.php");

        ?>
        <div id="item-body">
            <?php do_action('template_notices'); ?>
            <?php do_action('bp_before_member_body');

            locate_template(array('members/single/' . $file_name . '.php'), true);
            ?>
        </div>
        <?php
        do_action('bp_after_member_home_content');

//eLearning_include_template( "profile/bottom.php" );
//
//get_footer( eLearning_get_footer() );

        do_action('eLearning_after_member_profile');

    }
}
//members_custom_url();

//add_action( 'bb_init', 'members_custom_url' );

add_action( 'bp_actions', 'members_custom_url' );


//Check If this Post is event
if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){
    if($_SERVER['HTTP_REFERER'] == get_site_url().'/wp-admin/edit.php?post_type=course'){
        add_action( 'edit_form_after_editor', 'is_event' );
    }
}


function is_event()
{
    $post = $_REQUEST['post'];
    $value = get_post_meta($post, 'is_event', true);
    echo
        '
        <div id="is_event_settings" class="postbox" style="margin-top: 40px;">
            <button type="button" class="handlediv button-link" aria-expanded="true">
                <span class="screen-reader-text">Toggle panel: Course Settings</span>
                <span class="toggle-indicator" aria-hidden="true"></span>
            </button>
            <h2 class="hndle ui-sortable-handle">
                <span>Event Settings</span>
            </h2>
            <div class="is_event_container inside">
                <h2>Is Event</h2>
                <div class="select_button yesno">
                    <span>No</span>
                    <span>Yes</span>
                </div>
                <select name="is_event" id="is_event" class="select_val">
                    <option ' . (empty($value) || $value == "H" ? ' selected="selected" ' : null) . ' value="H">No</option>
                    <option ' . (!empty($value) && $value == "S" ? ' selected="selected" ' : null) . ' value="S">Yes</option>
                </select>
            </div>
        </div>
        ';
}

add_action( 'save_post', 'save_postdata');

function save_postdata()
{

    $postid = @$_POST['post_ID'];

    if ( !current_user_can( 'edit_page', $postid ) ) return false;
    if(empty($postid)) return false;

    if($_POST['action'] == 'editpost'){
        delete_post_meta($postid, 'is_event');
    }

    add_post_meta($postid, 'is_event', $_POST['is_event']);
}


function bp_get_options_event_nav($parent_slug = '') {
    $bp = buddypress();

    // If we are looking at a member profile, then the we can use the current
    // component as an index. Otherwise we need to use the component's root_slug.
    $component_index = !empty($bp->displayed_user) ? bp_current_component() : bp_get_root_slug(bp_current_component());
    $selected_item = bp_current_action();
  //  var_dump($selected_item);
    // Default to the Members nav.
    if (!bp_is_single_item()) {
        // Set the parent slug, if not provided.
        if (empty($parent_slug)) {
            $parent_slug = $component_index;
        }

        $secondary_nav_items = $bp->members->nav->get_secondary(array('parent_slug' => $parent_slug));

        if (!$secondary_nav_items) {
            return false;
        }

        // For a single item, try to use the component's nav.
    } else {
        $current_item = bp_current_item();
        $single_item_component = bp_current_component();

        // Adjust the selected nav item for the current single item if needed.
        if (!empty($parent_slug)) {
            $current_item = $parent_slug;
            $selected_item = bp_action_variable(0);
        }

        // If the nav is not defined by the parent component, look in the Members nav.
        if (!isset($bp->{$single_item_component}->nav)) {
            $secondary_nav_items = $bp->members->nav->get_secondary(array('parent_slug' => $current_item));
        } else {
            $secondary_nav_items = $bp->{$single_item_component}->nav->get_secondary(array('parent_slug' => $current_item));
        }

        if (!$secondary_nav_items) {
            return false;
        }
    }

    // Loop through each navigation item.
    foreach ($secondary_nav_items as $subnav_item) {
        if (empty($subnav_item->user_has_access)) {
            continue;
        }

        // If the current action or an action variable matches the nav item id, then add a highlight CSS class.
        //  var_dump($subnav_item->slug);

        if ($subnav_item->slug === $selected_item) {
            $selected = ' class="current selected"';
        } else {
            $selected = '';
        }

        // List type depends on our current component.
        $list_type = bp_is_group() ? 'groups' : 'personal';

        /**
         * Filters the "options nav", the secondary-level single item navigation menu.
         *
         * This is a dynamic filter that is dependent on the provided css_id value.
         *
         * @since 1.1.0
         *
         * @param string $value         HTML list item for the submenu item.
         * @param array  $subnav_item   Submenu array item being displayed.
         * @param string $selected_item Current action.
         */
        echo apply_filters('bp_get_options_nav_' . $subnav_item->css_id, '<li id="' . esc_attr($subnav_item->css_id . '-' . $list_type . '-li') . '" ' . $selected . '><a id="' . esc_attr($subnav_item->css_id) . '" href="' . esc_url($subnav_item->link) . '">' . $subnav_item->name . '</a></li>', $subnav_item, $selected_item);
    }
}

define('COURSE_EDIT_PAGE', 24);
define('EVENT_EDIT_PAGE', 623);

function createEventWebinar($event_id, $post_settings, $setting) {
    $unit = array();
    $post_metas = array();
    foreach ($post_settings as $setting) {

        switch ($setting->id) {
            case 'eLearning_start_date':
                $originalDate = $setting->value;
                $post_metas['webinar_start_date'] = date("d-m-Y", strtotime($originalDate));
                break;
            case 'eLearning_start_time':
                $originalDate = $setting->value;
                $post_metas['webinar_start_time'] = date("hh:ii:ss", strtotime($originalDate));
                break;
            case 'eLearning_expected_duration':
                $post_metas['webinar_expected_duration'] = $setting->value;
                break;
            case 'eLearning_event_is_recorded':
                $post_metas['webinar_is_recorded'] = $setting->value;
                break;
        }
    }
    $post_settings['post_type'] = 'unit';
    $post_settings['post_status'] = 'publish';

    $webinarId = wp_insert_post($post_settings);
    if ($webinarId) {
        $getOnstreamResults = createNewWebinarInOnstream($post_settings, $post_metas, $webinarId);

        if (isset($getOnstreamResults->id)) {
            $post_metas['onstreamSessionId'] = (int) $getOnstreamResults->id;
        } elseif (!empty($getOnstreamResults->error)) {
            echo $getOnstreamResults->error;
        }

        foreach ($post_metas as $key => $value) {
            update_post_meta($webinarId, $key, $value);
        }
    }
    return $webinarId;
}

function custom_pagination($numpages = '', $pagerange = '', $paged = '') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     * 
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if (!$numpages) {
            $numpages = 1;
        }
    }

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function. 
     */
    $pagination_args = array(
        'base' => get_pagenum_link(1) . '%_%',
        'format' => 'page/%#%',
        'total' => $numpages,
        'current' => $paged,
        'show_all' => false,
        'end_size' => 1,
        'mid_size' => $pagerange,
        'prev_next' => true,
        'prev_text' => __('<'),
        'next_text' => __('>'),
        'type' => 'array',
        'add_args' => false,
        'add_fragment' => ''
    );

    $paginate_links = paginate_links($pagination_args);



    if ($paginate_links) {
        echo "<nav class='custom-pagination'>";
        foreach ($paginate_links as $key => $pg) {
            if ($key === 0) {
                echo str_replace(1, "<", $pg).'  ';
                echo "Page " . $paged . " of " . $numpages;
            }
            if (sizeof($paginate_links) === $key + 1) {
                echo '  '.$pg;
            }
        }

//        echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
//        echo $paginate_links;
//        echo "</nav>";
    }
}


/*
 * Function name set_woocommerce_default_parameters
 *
 * Description : Set woocomerce default parameters on woocomerce activating
 *
 * **/


function set_woocommerce_default_parameters() {
    //Setting the currency
    $option_currency = "woocommerce_currency";

    $new_currency = "USD";

    update_option( $option_currency, $new_currency );

    //Setting the country
    $option_country = "woocommerce_default_country";

    $new_country = "US:CA";

    update_option( $option_country, $new_country );

    //Setting Allowed Countries

    $option_allowed_country = "woocommerce_allowed_countries";

    $new_allowed_country = "all";

    update_option( $option_allowed_country, $new_allowed_country );

    // Setting shipping parameters

    $option_allowed_country = "woocommerce_ship_to_countries";

    $new_allowed_country = "disabled";

    update_option( $option_allowed_country, $new_allowed_country );

    // Setting default customer location

    $option_default_customer_address = "woocommerce_default_customer_address";

    $new_customer_location = "geolocation";

    update_option( $option_default_customer_address, $new_customer_location );

    $user = wp_get_current_user();
    $guid = "https://".$_SERVER['HTTP_HOST']."checkout/";

    $args = array(
        'post_author' => 1,
        'post_content' => '[woocommerce_checkout]',
        'post_content_filtered' => '',
        'post_title' => 'Checkout',
        'post_excerpt' => '',
        'post_status' => 'publish',
        'post_type' => 'page',
        'comment_status' => 'closed',
        'ping_status' => 'closed',
        'post_password' => '',
        'to_ping' =>  '',
        'pinged' => '',
        'post_parent' => 0,
        'menu_order' => 0,
        'guid' => $guid,

    );
    wp_insert_post($args);


}
add_action('woocommerce_loaded', 'set_woocommerce_default_parameters');




/*
 * Function name create_free_membership_level_after_install
 *
 * Description : Create a new free membership level when theme is installed and PMPRO plugin activated
 *
 * **/


function create_free_membership_level_after_install() {

    global $wpdb ;

    $table_name = $wpdb->prefix."pmpro_membership_levels";

    $data = array(
        'name'              => 'Student Free Membership Level',
        'description'       => '',
        'confirmation'      => '',
        'initial_payment'   => '0.00',
        'billing_amount'    => '0.00',
        'cycle_number'      => '0',
        'cycle_period'      => '',
        'billing_limit'     => '0',
        'trial_amount'      => '0.00',
        'trial_limit'       => '0',
        'allow_signups'     => '1',
        'expiration_number' => '0',
        'expiration_period' => '',
    );

    $wpdb->insert($table_name,$data);


}

add_action('pmpro_activation', 'create_free_membership_level_after_install');


/*
 *  Create required pages after installing theme
 *  Home page
 *  Student Login
 *  Admin Login
 *
 * **/

function create_required_pages_after_install_theme() {

    $guid_home = "https://".$_SERVER['HTTP_HOST']."/home/";

    $args_for_home = array(
        'post_author' => 1,
        'post_content' => '',
        'post_content_filtered' => '',
        'post_title' => 'Home Page',
        'post_excerpt' => '',
        'post_status' => 'publish',
        'post_type' => 'page',
        'comment_status' => 'closed',
        'ping_status' => 'closed',
        'post_password' => '',
        'to_ping' =>  '',
        'pinged' => '',
        'post_parent' => 0,
        'menu_order' => 0,
        'guid' => $guid_home,

    );
    wp_insert_post($args_for_home);

    $guid_login = "https://".$_SERVER['HTTP_HOST']."/login/";
    $args_for_login = array(
        'post_author' => 1,
        'post_content' => '[email_login_student]',
        'post_content_filtered' => '',
        'post_title' => 'Login',
        'post_excerpt' => '',
        'post_status' => 'publish',
        'post_type' => 'page',
        'comment_status' => 'closed',
        'ping_status' => 'closed',
        'post_password' => '',
        'to_ping' =>  '',
        'pinged' => '',
        'post_parent' => 0,
        'menu_order' => 0,
        'guid' => $guid_login,

    );
    wp_insert_post($args_for_login);


    $guid_adminlogin = "https://".$_SERVER['HTTP_HOST']."/adminlogin/";
    $args_for_adminlogin = array(
        'post_author' => 1,
        'post_content' => '[email_login_admin]',
        'post_content_filtered' => '',
        'post_title' => 'Admin Login',
        'post_excerpt' => '',
        'post_status' => 'publish',
        'post_type' => 'page',
        'comment_status' => 'closed',
        'ping_status' => 'closed',
        'post_password' => '',
        'to_ping' =>  '',
        'pinged' => '',
        'post_parent' => 0,
        'menu_order' => 0,
        'guid' => $guid_adminlogin,

    );
    wp_insert_post($args_for_adminlogin);


    global $wpdb;

    $table_name_page  = $wpdb->prefix."posts";

    $sql = "SELECT ID FROM ".$table_name_page." WHERE post_name = 'home-page' AND post_type='page'  AND post_status = 'publish'";

    $page_id = $wpdb->get_row($sql);

    update_option('page_on_front',$page_id->ID );
}



add_action( 'after_switch_theme', 'create_required_pages_after_install_theme' );

//Set auto approve to registered users
function setApprove()
{

    $args = array(
        'orderby' => 'registered',
        'order' => 'DESC',
        'number' => 5
    );

    $lastTenUsers = get_users( $args );

    foreach ($lastTenUsers as $lastUser) {
        $user_id = $lastUser->data->ID;



        update_user_meta($user_id, 'pw_user_status', 'approved');
    }

}

add_action( 'user_register', 'setApprove');


/*
 *
 * Function name: add_tag_to_post
 *
 *
 * Adding default tag elearninglms to a page
 *
 *
 * **/



function add_tag_to_post() {

    global $wpdb;

    if(isset($_POST['post_type']) && !empty($_POST['post_type']) && $_POST['post_type'] == 'page' && isset($_POST['auto_draft']) && !empty($_POST['auto_draft']) && $_POST['auto_draft'] == 1 ){
        if(isset($_POST['post_ID']) && !empty($_POST['post_ID']) && is_numeric($_POST['post_ID'])) {
            $post_id = $_POST['post_ID'];

            $get_elearninglms_tag_id = "SELECT term_id FROM wp_terms WHERE name = 'elearninglms' AND slug = 'elearninglms'";

            $elearninglms_tag_id = $wpdb->get_row($get_elearninglms_tag_id);

            $elearninglms_tag_id = $elearninglms_tag_id->term_id;

            $check_page_id_exist_in_db_sql = "SELECT object_id FROM wp_term_relationships  WHERE term_taxonomy_id = " . $elearninglms_tag_id;

            $page_id_exist_in_db = $wpdb->get_results($check_page_id_exist_in_db_sql);

            $checkarr = array();
            foreach ($page_id_exist_in_db as $obj_id) {
                $checkarr[] = $obj_id->object_id;
            }

            if (!in_array($post_id, $checkarr)) {

                $data = array(
                    'object_id' => $post_id,
                    'term_taxonomy_id' => $elearninglms_tag_id,
                    'term_order' => 0,
                );
                $wpdb->insert('wp_term_relationships', $data);
            }
        }
    }
}



add_action('publish_page','add_tag_to_post');

/* email custamization  shortcode*/

function email_customization_shortcode (){
echo __FILE__ . __LINE__;
/**
 * New Post Administration Screen.
 *
 * @package WordPress
 * @subpackage Administration
 */

/** Load WordPress Administration Bootstrap */
//require_once( dirname( __FILE__ ) . '/admin.php' );
echo __FILE__ . __LINE__;
/**
 * @global string  $post_type
 * @global object  $post_type_object
 * @global WP_Post $post
 */
global $post_type, $post_type_object, $post;

if ( ! isset( $_GET['post_type'] ) ) {
	$post_type = 'post';
} elseif ( in_array( $_GET['post_type'], get_post_types( array('show_ui' => true ) ) ) ) {
	$post_type = $_GET['post_type'];
} else {
	wp_die( __( 'Invalid post type.' ) );
}
$post_type_object = get_post_type_object( $post_type );

if ( 'post' == $post_type ) {
	$parent_file = 'edit.php';
	$submenu_file = 'post-new.php';
} elseif ( 'attachment' == $post_type ) {
	if ( wp_redirect( admin_url( 'media-new.php' ) ) )
		exit;
} else {
	$submenu_file = "post-new.php?post_type=$post_type";
	if ( isset( $post_type_object ) && $post_type_object->show_in_menu && $post_type_object->show_in_menu !== true ) {
		$parent_file = $post_type_object->show_in_menu;
		// What if there isn't a post-new.php item for this post type?
		if ( ! isset( $_registered_pages[ get_plugin_page_hookname( "post-new.php?post_type=$post_type", $post_type_object->show_in_menu ) ] ) ) {
			if (	isset( $_registered_pages[ get_plugin_page_hookname( "edit.php?post_type=$post_type", $post_type_object->show_in_menu ) ] ) ) {
				// Fall back to edit.php for that post type, if it exists
				$submenu_file = "edit.php?post_type=$post_type";
			} else {
				// Otherwise, give up and highlight the parent
				$submenu_file = $parent_file;
			}
		}
	} else {
		$parent_file = "edit.php?post_type=$post_type";
	}
}

$title = $post_type_object->labels->add_new_item;

$editing = true;

if ( ! current_user_can( $post_type_object->cap->edit_posts ) || ! current_user_can( $post_type_object->cap->create_posts ) ) {
	wp_die(
		'<h1>' . __( 'Cheatin&#8217; uh?' ) . '</h1>' .
		'<p>' . __( 'Sorry, you are not allowed to create posts as this user.' ) . '</p>',
		403
	);
}

// Schedule auto-draft cleanup
if ( ! wp_next_scheduled( 'wp_scheduled_auto_draft_delete' ) )
	wp_schedule_event( time(), 'daily', 'wp_scheduled_auto_draft_delete' );

wp_enqueue_script( 'autosave' );

if ( is_multisite() ) {
	add_action( 'admin_footer', '_admin_notice_post_locked' );
} else {
	$check_users = get_users( array( 'fields' => 'ID', 'number' => 2 ) );

	if ( count( $check_users ) > 1 )
		add_action( 'admin_footer', '_admin_notice_post_locked' );

	unset( $check_users );
}

// Show post form.
//$post = get_default_post_to_edit_custom( $post_type, true );
$post_ID = $post->ID;

    $post_ID = isset($post_ID) ? (int) $post_ID : 0;
    $user_ID = isset($user_ID) ? (int) $user_ID : 0; 


    $url = ABSPATH;
    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    //require_once( $url.'/wp-includes/script-loader.php' );

    ?>
    <!--<form name="post" action="" method="post" id="post">
        <div id="yith-wcet-metabox" class="postbox ">-->
    <?php
    $post_ID = (int) $post->ID;
    //print_r('!!!!!!'.$post_ID);
    ?>
    <input type="text"   id="title"                name="post_title" size="30" value=""  spellcheck="true" autocomplete="off">
    <input type="hidden" id="hiddenaction"         name="action"               value="editpost">
    <input type="hidden" id="originalaction"       name="originalaction"       value="editpost">
    <input type="hidden" id="post_type"            name="post_type"            value="yith-wcet-etemplate">
    <input type="hidden" id="original_post_status" name="original_post_status" value="publish">
    <input type="hidden" id="post_ID"              name="post_ID"              value="<?php echo $post_ID; ?>">
    <input type="hidden" id="user-id"              name="user_ID"              value="<?php //echo (int) $user_ID ?>1">
    <input type="hidden" id="post_author"          name="post_author"          value="<?php echo esc_attr( $post->post_author ); ?>">


    <?php

    $meta = get_post_meta( $post->ID, '_template_meta', true );
    //print_r("!!!!". $post->ID);
                $default = array(
                    'txt_color_default'            => '#000000',
                    'txt_color'                    => '#000000',
                    'bg_color_default'             => '#F5F5F5',
                    'bg_color'                     => '#F5F5F5',
                    'base_color_default'           => '#2470FF',
                    'base_color'                   => '#2470FF',
                    'body_color_default'           => '#FFFFFF',
                    'body_color'                   => '#FFFFFF',
                    'logo_url'                     => '',
                    'custom_logo_url'              => get_option( 'yith-wcet-custom-default-header-logo' ),
                    'page_width'                   => '800',
                    // - - - - -      P R E M I U M      - - - - -
                    'logo_height'                  => '100',
                    'header_padding'               => array( 36, 48, 36, 48 ),
                    'premium_mail_style'           => 'default',
                    'footer_logo_url'              => '',
                    'header_position'              => 'center',
                    'header_color'                 => 'transparent',
                    'header_color_default'         => 'transparent',
                    'footer_text_color'            => '#555555',
                    'footer_text_color_default'    => '#555555',
                    'page_border_radius'           => '3',
                    'h1_size'                      => '30',
                    'h2_size'                      => '18',
                    'h3_size'                      => '16',
                    'body_size'                    => '14',
                    'body_line_height'             => '20',
                    'table_border_width'           => '1',
                    'table_border_color'           => '#555555',
                    'table_border_color_default'   => '#555555',
                    'table_bg_color'               => '#FFFFFF',
                    'table_bg_color_default'       => '#FFFFFF',
                    'price_title_bg_color'         => '#FFFFFF',
                    'price_title_bg_color_default' => '#FFFFFF',
                    'show_prod_thumb'              => '',
                    'footer_text'                  => '',
                    'socials_on_header'            => 0,
                    'socials_on_footer'            => 0,
                    'socials_color'                => 'black',
                    'custom_links'                 => array(),
                );

                $args = wp_parse_args( $meta, $default );


                $args = apply_filters( 'yith_wcet_metabox_options_content_args', $args );

                yith_wcet_metabox_options_content_premium( $args );
                ?> 
                </div>
            <div id="save-action">
            <input type="submit" name="save" id="save-post" value="Save" class="button">
            <span class="spinner"></span>
        </div>
    <!--</form>-->
    <?php

    $post_id = $post->ID;
    //metabox_save( $post_id );

    ?> <script type='text/javascript'>
    /* <![CDATA[ */
    //var ajax_object = {"assets_url":"http:\/\/finallms.first.am\/wp-content\/plugins\/yith-woocommerce-email-templates-premium\/assets","wp_ajax_url":"http:\/\/finallms.first.am\/wp-admin\/admin-ajax.php"};
    /* ]]> */
    </script>

    <?php

}
function get_default_post_to_edit_custom( $post_type = 'post', $create_in_db = false ) {
	$post_title = '';
	if ( !empty( $_REQUEST['post_title'] ) )
		$post_title = esc_html( wp_unslash( $_REQUEST['post_title'] ));

	$post_content = '';
	if ( !empty( $_REQUEST['content'] ) )
		$post_content = esc_html( wp_unslash( $_REQUEST['content'] ));

	$post_excerpt = '';
	if ( !empty( $_REQUEST['excerpt'] ) )
		$post_excerpt = esc_html( wp_unslash( $_REQUEST['excerpt'] ));

	if ( $create_in_db ) {
		$post_id = wp_insert_post( array( 'post_title' => __( 'Auto Draft' ), 'post_type' => $post_type, 'post_status' => 'auto-draft' ) );
		$post = get_post( $post_id );
		if ( current_theme_supports( 'post-formats' ) && post_type_supports( $post->post_type, 'post-formats' ) && get_option( 'default_post_format' ) )
			set_post_format( $post, get_option( 'default_post_format' ) );
	} else {
		$post = new stdClass;
		$post->ID = 0;
		$post->post_author = '';
		$post->post_date = '';
		$post->post_date_gmt = '';
		$post->post_password = '';
		$post->post_name = '';
		$post->post_type = $post_type;
		$post->post_status = 'draft';
		$post->to_ping = '';
		$post->pinged = '';
		$post->comment_status = get_default_comment_status( $post_type );
		$post->ping_status = get_default_comment_status( $post_type, 'pingback' );
		$post->post_pingback = get_option( 'default_pingback_flag' );
		$post->post_category = get_option( 'default_category' );
		$post->page_template = 'default';
		$post->post_parent = 0;
		$post->menu_order = 0;
		$post = new WP_Post( $post );
	}

	/**
	 * Filters the default post content initially used in the "Write Post" form.
	 *
	 * @since 1.5.0
	 *
	 * @param string  $post_content Default post content.
	 * @param WP_Post $post         Post object.
	 */
	$post->post_content = apply_filters( 'default_content', $post_content, $post );

	/**
	 * Filters the default post title initially used in the "Write Post" form.
	 *
	 * @since 1.5.0
	 *
	 * @param string  $post_title Default post title.
	 * @param WP_Post $post       Post object.
	 */
	$post->post_title = apply_filters( 'default_title', $post_title, $post );

	/**
	 * Filters the default post excerpt initially used in the "Write Post" form.
	 *
	 * @since 1.5.0
	 *
	 * @param string  $post_excerpt Default post excerpt.
	 * @param WP_Post $post         Post object.
	 */
	$post->post_excerpt = apply_filters( 'default_excerpt', $post_excerpt, $post );

	return $post;
}


function metabox_save( $post_id ) {
            if ( !empty( $_POST[ '_template_meta' ] ) ) {
                $meta = $_POST[ '_template_meta' ];
                //checkbox
                $meta[ 'show_prod_thumb' ]   = ( !isset( $_POST[ '_template_meta' ][ 'show_prod_thumb' ] ) ) ? 0 : 1;
                $meta[ 'socials_on_header' ] = ( !isset( $_POST[ '_template_meta' ][ 'socials_on_header' ] ) ) ? 0 : 1;
                $meta[ 'socials_on_footer' ] = ( !isset( $_POST[ '_template_meta' ][ 'socials_on_footer' ] ) ) ? 0 : 1;

                //custom links
                $meta[ 'custom_links' ] = ( !empty( $_POST[ '_template_meta' ][ 'custom_links' ] ) ) ? $_POST[ '_template_meta' ][ 'custom_links' ] : array();

                $empty_custom_links = 1;
                foreach ( $meta[ 'custom_links' ] as $key => $m ) {
                    if ( ( $m[ 'text' ] == '' ) && ( $m[ 'url' ] == '' ) ) {
                        unset( $meta[ 'custom_links' ][ $key ] );
                    } else {
                        $empty_custom_links = 0;
                    }
                }
                if ( $empty_custom_links ) {
                    $meta[ 'custom_links' ] = array();
                }

                update_post_meta( $post_id, '_template_meta', $meta );
            }
        }

//add_shortcode( 'email_customization_shortcode', 'email_customization_shortcode' );

/*end email custamization  shortcode*/










/*
 *
 * Create page on front page in website create page button
 *
 *
 *
 * **/
function create_page_from_front() {
    $user_id = get_current_user_id();
    $args_for_new_page = array(
        'post_author' => $user_id,
        'post_content' => '',
        'post_content_filtered' => '',
        'post_excerpt' => '',
        'post_type' => 'page',
        'comment_status' => 'closed',
        'ping_status' => 'closed',
        'post_password' => '',
        'to_ping' =>  '',
        'pinged' => '',
        'post_parent' => 0,
        'menu_order' => 0,
     );


    $page_id = wp_insert_post($args_for_new_page);

        echo '<a href="http://lms01.first.am/wp-admin/post.php?vc_action=vc_inline&post_id='.$page_id.'&post_type=page" target="_blank" class="btn btn-primary btn-lg pull-right top-button">CREATE PAGE</a>';
}

add_shortcode('create_page_from_front','create_page_from_front');


/*
 * Manual time sheduller for cron
 * **/

/*
function my_cron_schedules($schedules){
    if(!isset($schedules["2min"])){
        $schedules["2min"] = array(
            'interval' => 2*60,
            'display' => __('Once every 2 minutes'));
    }
    if(!isset($schedules["30min"])){
        $schedules["30min"] = array(
            'interval' => 30*60,
            'display' => __('Once every 30 minutes'));
    }
    return $schedules;
}
add_filter('cron_schedules','my_cron_schedules');



function my_schedule_hook($args) {

    if(file_exists('cron.log')){
        $data = file_get_contents('cron.log');
        file_put_contents('cron.log',$data.'\nCron run:'.gmdate("Y-m-d H:i:s"));
    }else{
        file_put_contents('cron.log','\nCron run:'.gmdate("Y-m-d H:i:s"));
    }


}

function schedule_my_hook() {

    wp_schedule_event(time(), '2min', 'my_schedule_hook', $args);

}
if(!wp_get_schedule('my_schedule_hook')){

    add_action('init', 'schedule_my_hook',10);

}


add_action('wp_loaded', 'my_cron_function');

function my_cron_function() {
    if (! wp_next_scheduled ( 'my_hourly_event' )) {
        wp_schedule_event(time(), '2min', 'my_hourly_event');
    }
}

add_action('my_hourly_event', 'do_this_hourly');

function do_this_hourly() {
    // do something every hour
    file_put_contents('cron.log','\nCron run:'.gmdate("Y-m-d H:i:s"));
}
*/
?>