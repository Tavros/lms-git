jQuery(document).ready(function(){
	
   if(jQuery('#last_tab').val() == ''){

		jQuery('.eLearning-opts-group-tab:first').slideDown('fast');
		jQuery('#eLearning-opts-group-menu li:first').addClass('active');
	
	}else{
		
		tabid = jQuery('#last_tab').val();
		jQuery('#'+tabid+'_section_group').slideDown('fast');
		jQuery('#'+tabid+'_section_group_li').addClass('active');
		
	}
	
	
	jQuery('input[name="'+eLearning_opts.opt_name+'[defaults]"]').click(function(){
		if(!confirm(eLearning_opts.reset_confirm)){
			return false;
		}
	});
	
	jQuery('.eLearning-opts-group-tab-link-a').click(function(){
		relid = jQuery(this).attr('data-rel');
		
		jQuery('#last_tab').val(relid);
		
		jQuery('.eLearning-opts-group-tab').each(function(){
			if(jQuery(this).attr('id') == relid+'_section_group'){
				jQuery(this).delay(400).fadeIn(1200);
			}else{
				jQuery(this).fadeOut('fast');
			}
			
		});
		
		jQuery('.eLearning-opts-group-tab-link-li').each(function(){
				if(jQuery(this).attr('id') != relid+'_section_group_li' && jQuery(this).hasClass('active')){
					jQuery(this).removeClass('active');
				}
				if(jQuery(this).attr('id') == relid+'_section_group_li'){
					jQuery(this).addClass('active');
				}
		});
	});
	

	
	
	
	if(jQuery('#eLearning-opts-save').is(':visible')){
		jQuery('#eLearning-opts-save').delay(4000).slideUp('slow');
	}
	
	if(jQuery('#eLearning-opts-imported').is(':visible')){
		jQuery('#eLearning-opts-imported').delay(4000).slideUp('slow');
	}	
	
	jQuery('input, textarea, select').change(function(){
		jQuery('#eLearning-opts-save-warn').slideDown('slow');
	});
	
	
	jQuery('#eLearning-opts-import-code-button').click(function(){
		if(jQuery('#eLearning-opts-import-link-wrapper').is(':visible')){
			jQuery('#eLearning-opts-import-link-wrapper').fadeOut('fast');
			jQuery('#import-link-value').val('');
		}
		jQuery('#eLearning-opts-import-code-wrapper').fadeIn('slow');
	});
	
	jQuery('#eLearning-opts-import-link-button').click(function(){
		if(jQuery('#eLearning-opts-import-code-wrapper').is(':visible')){
			jQuery('#eLearning-opts-import-code-wrapper').fadeOut('fast');
			jQuery('#import-code-value').val('');
		}
		jQuery('#eLearning-opts-import-link-wrapper').fadeIn('slow');
	});
	
	
	
	
	jQuery('#eLearning-opts-export-code-copy').click(function(){
		if(jQuery('#eLearning-opts-export-link-value').is(':visible')){jQuery('#eLearning-opts-export-link-value').fadeOut('slow');}
		jQuery('#eLearning-opts-export-code').toggle('fade');
	});
	
	jQuery('#eLearning-opts-export-link').click(function(){
		if(jQuery('#eLearning-opts-export-code').is(':visible')){jQuery('#eLearning-opts-export-code').fadeOut('slow');}
		jQuery('#eLearning-opts-export-link-value').toggle('fade');
	});
});
jQuery(document).ready(function(){
	jQuery('#sampleinstall').click(function(){ 
            jQuery(this).addClass('disabled');
            var file =jQuery(this).attr('rel-file');
                         jQuery.ajax({
                                    type: "POST",
                                    url: ajaxurl,
                                    data:
                                    {
					action : 'import_sample_data',
					file : file
                                    },
                                    error: function( xhr, ajaxOptions, thrownError ){
                                        jQuery(this).after('<span class="error">Some error occured !<span>');
					console.log('error occurred' +ajaxOptions);
                                    },
                                    success: function( data ){ 
                                        jQuery('#sampleinstall').after('<span class="success">'+data+'<span>');
                                        setTimeout(function(){
                                            jQuery(this).parent().find('.success').fadeOut(300);},3000);
                                            jQuery('#sampleinstall').removeClass('disabled');
                                        }
                         });
        });
});