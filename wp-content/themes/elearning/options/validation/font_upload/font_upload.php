<?php
class ELEARNING_Validation_font_upload extends ELEARNING_Options{	
	
	/**
	 * Field Constructor.
	 *
	 * Required - must call the parent constructor, then assign field and value to vars, and obviously call the render field function
	 *
	 * @since ELEARNING_Options 1.0
	*/
	function __construct($field, $value, $current){
		
		parent::__construct();
		$this->field = $field;
		$this->field['msg'] = (isset($this->field['msg']))?$this->field['msg']:__('Unable to upload font.', 'eLearning');
		$this->value = $value;
		$this->current = $current;
		$this->validate();
		
	}//function
	
	
	
	/**
	 * Field Render Function.
	 *
	 * Takes the vars and validates them
	 *
	 * @since ELEARNING_Options 1.0
	*/
	function validate(){
		
		if (!isset($_POST['name'])) {
			  
		}else{
			}
				
	}//function
	
}//class
?>