<?php

if ( !defined( 'ABSPATH' ) ) exit;
do_action('eLearning_before_single_group');

get_header( eLearning_get_header() ); 

$group_layout = eLearning_get_customizer('group_layout');
if ( bp_has_groups() ) : while ( bp_groups() ) : bp_the_group();

eLearning_include_template("groups/top$group_layout.php"); 
?>
					

					<div id="item-body">

						<?php do_action( 'bp_before_group_body' ); ?>

						<?php do_action( 'bp_template_content' ); ?>

						<?php do_action( 'bp_after_group_body' ); ?>
					</div><!-- #item-body -->

					<?php do_action( 'bp_after_group_plugin_template' ); ?>

					<?php endwhile; endif; ?>

<?php
				
eLearning_include_template("groups/bottom$group_layout.php","groups/bottom.php");  			
get_footer( eLearning_get_footer() );  