<?php
/**
 * Template Name: No Title
 */
get_header(eLearning_get_header());
?>
<section id="content">
    <div class="<?php echo eLearning_get_container(); ?>">
            <?php
            if ( have_posts() ) : while ( have_posts() ) : the_post();
                the_content();
            endwhile;
            endif;
            ?>
    </div>
</section>
<?php
get_footer(eLearning_get_footer());
?>