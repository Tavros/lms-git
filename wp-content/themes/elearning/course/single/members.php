<?php
/**
 * The template for displaying Course members
 *
 * Override this template by copying it to yourtheme/course/single/members.php
 *
 * @author 		ELearningThemes
 * @package 	eLearning-course-module/templates
 * @version     1.8.1
 */
if ( !defined( 'ABSPATH' ) ) exit;
global $post;
$students=get_post_meta(get_the_ID(),'eLearning_students',true);

$course_layout = eLearning_get_customizer('course_layout');


if(empty($course_layout)){
?>
<div class="course_title">
	<h1><?php the_title(); ?></h1>
</div>
<?php
}
?>

<?php

$students_undertaking= bp_course_get_students_undertaking(); 

if(count($students_undertaking) > 0 ){
	?>
	<h4 class="total_students"><?php _e('Total number of Students in course','eLearning'); ?><span><?php echo $students; ?></span></h4>
	<?php
	echo "<table id='registered_members_list' class='display nowrap' cellspacing='0' width='100%'>";
	echo "  <thead>
                <tr>                        
                    <th>ID</th>
                    <th>Email</th>
                    <th>Event</th> 
                    <th>Name</th>
                    <th>Register Date</th>                      
                    <th>Avatar</th>                      
                </tr>
            </thead>
            <tbody>";
	foreach($students_undertaking as $student){

		$currentStudentData = get_userdata($student);
		if (function_exists('bp_get_profile_field_data')) {
		    $bp_name = bp_core_get_userlink( $student );

		    $sfield=eLearning_get_option('student_field');

		    if(!isset($sfield) || $sfield =='') 
		    	$sfield = 'Location';

		    $bp_location ='';
		    if(bp_is_active('xprofile'))
		    $bp_location = bp_get_profile_field_data('field='.$sfield.'&user_id='.$student);
		    
		    if ($bp_name) {
		    	echo "<tr>";
		    		echo '<td class="col-md-1">';
		    			echo $currentStudentData->data->ID; 
			    	echo '</td>';

			    	echo '<td>';
		    			echo $currentStudentData->data->user_email;
			    	echo '</td>';

			    	echo '<td>';
		    			echo the_title();
			    	echo '</td>';

			    	echo '<td>';
		    			echo $currentStudentData->data->display_name;
			    	echo '</td>';

			    	echo '<td>';
		    			echo $currentStudentData->data->user_registered;
			    	echo '</td>';

		    		echo '<td>';
		    		echo get_avatar($student);
				    // if ($bp_location) {
				    // 	echo '<span>'. $bp_location . '</span>';
				    // }
			    // echo '<div class="action">';
			    // $check_meta = eLearning_get_option('members_activity');

			    // if(bp_is_active('friends') && $check_meta){
			    // 	if(function_exists('bp_add_friend_button')){
				   //  	bp_add_friend_button( $student );
				   //  }
			    // }
			    echo '</td></tr>';
		    }
		    
		}
	}
	echo "</tbody>";
	echo "</table>";
	echo bp_course_paginate_students_undertaking();
}else{
	echo '<div class="message">'._x('No members found in this course.','No members notification in course - members','eLearning').'</div>';
}

?>
<script type="text/javascript">
	jQuery(document).ready(function(){
	    jQuery('#registered_members_list').DataTable({
            searching: false,
            select: true,
            lengthChange: false,
            paging: false,
            bInfo : false,
	        dom: 'Bfrtip',
	        buttons: [
	            'csv'
	        ]
        });
    })
</script>