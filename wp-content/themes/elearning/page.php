<?php

get_header(eLearning_get_header());

if ( have_posts() ) : while ( have_posts() ) : the_post();



?>
<section id="title">
    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="pagetitle">
                    <?php
                        $breadcrumbs=get_post_meta(get_the_ID(),'eLearning_breadcrumbs',true);
                        if(eLearning_validate($breadcrumbs) || empty($breadcrumbs))
                            eLearning_breadcrumbs(); 

                        $title=get_post_meta(get_the_ID(),'eLearning_title',true);
                        if(eLearning_validate($title) || empty($title)){
                    ?>
                    <h1><?php the_title(); ?></h1>
                    <?php the_sub_title(); }?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

    $v_add_content = get_post_meta( $post->ID, '_add_content', true );
 
?>





<section id="content">

<?php if($_SERVER['REQUEST_URI'] == '/blog/frontend-publishing/edit/1/' || $_SERVER['REQUEST_URI'] == '/blog/frontend-publishing/edit/' || $_SERVER['REQUEST_URI'] == '/blog/frontend-publishing/status/publish/' || $_SERVER['REQUEST_URI'] == '/blog/frontend-publishing/status/pending/' || $_SERVER['REQUEST_URI'] == '/blog/frontend-publishing/status/draft/'):?>
    <div id="buddypress"><!--Open buddypress div-->
        <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row heigh_correct">
        <div class="col-md-3 col-sm-4 left_nav_menu"><!--Open left nav menu div-->
        <?php do_action( 'bp_before_member_home_content' ); ?><!--Include home bp_before_member_home_content-->

                <div id="item-nav" class="">
                    <div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
                        <!--div class="create_event hs-elearning">
                                        <a href="<?php echo site_url().'/'.$current_user_login.'/';?>edit-event" class="btn btn-primary top-button">Create Event</a>
                                    </div-->
                        <ul>
                            <?php getCustomBodypressMenu_child(); ?>
                            <?php do_action( 'bp_member_options_nav' ); ?>
                        </ul>
                    </div><!-- #object-nav -->
                </div><!-- #item-nav -->
        </div><!--Close left nav menu div-->
        <div class="col-md-9 col-sm-8">

            <div >
                <?php
                the_content();
                $page_comments = eLearning_get_option('page_comments');
                if(!empty($page_comments))
                comments_template();
                ?>
            </div>
        </div>
        </div>
        </div>
<?php
eLearning_include_template( "profile/bottom.php" );
get_footer( eLearning_get_footer() );
?>

<?php else:?>

    <div class="<?php echo eLearning_get_container(); ?>">
        <div class="row">
            <div class="col-md-12">

                <div class="<?php echo $v_add_content;?> content">
                    <?php
                        the_content();
                        $page_comments = eLearning_get_option('page_comments');
                        if(!empty($page_comments))
                            comments_template();
                     ?>
                </div>
            </div>
        </div>
    </div>
</section>


<?php endif;?>

<?php
endwhile;
endif; 
?>
<?php
get_footer( eLearning_get_footer() );