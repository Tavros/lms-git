<?php
get_header(eLearning_get_header());
if ( have_posts() ) : while ( have_posts() ) : the_post();

$class = get_post_meta(get_the_ID(),'eLearning_popup_class',true);
$width = get_post_meta(get_the_ID(),'eLearning_popup_width',true);
$height = get_post_meta(get_the_ID(),'eLearning_popup_height',true);
?>
<section id="content">
    <div class="container center">
        <div class="popup-content <?php echo $class; ?>" style="display:inline-block;width:<?php echo $width; ?>px;max-height:<?php echo $height; ?>px;">
        	<style>
        		<?php
        			echo get_post_meta(get_the_ID(),'eLearning_custom_css',true);
        		?>
        	</style>
            <?php
            the_content();
            endwhile;
            endif;
             ?>
        </div>
    </div>
</section>

<?php
get_footer(eLearning_get_footer());
?>