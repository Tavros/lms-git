<?php

if ( ! defined( 'ABSPATH' ) ) exit;

//Define CONSTANTS
define('THEME_DOMAIN','eLearning'); 
define('THEME_SHORT_NAME','eLearning');
define('THEME_FULL_NAME','ELEARNING');
define('ELEARNING_PATH',get_theme_root().'/eLearning');
if ( !defined( 'ELEARNING_URL' ) )
	define('ELEARNING_URL',get_template_directory_uri());
define('ELEARNING_VERSION','2.6');


if ( !defined( 'BP_AVATAR_THUMB_WIDTH' ) )
define( 'BP_AVATAR_THUMB_WIDTH', 150 ); //change this with your desired thumb width

if ( !defined( 'BP_AVATAR_THUMB_HEIGHT' ) )
define( 'BP_AVATAR_THUMB_HEIGHT', 150 ); //change this with your desired thumb height

if ( !defined( 'BP_AVATAR_FULL_WIDTH' ) )
define( 'BP_AVATAR_FULL_WIDTH', 460 ); //change this with your desired full size,weel I changed it to 260 :) 

if ( !defined( 'BP_AVATAR_FULL_HEIGHT' ) )
define( 'BP_AVATAR_FULL_HEIGHT', 460 ); //change this to default height for full avatar

if ( ! defined( 'BP_DEFAULT_COMPONENT' ) )
define( 'BP_DEFAULT_COMPONENT', 'profile' );

if(function_exists('eLearning_get_option')){
	$username = eLearning_get_option('username');
	$apikey  = eLearning_get_option('apikey');
	// Auto Update
	if(isset($username) && isset($apikey)){ 
	  require_once(ELEARNING_PATH."/options/validation/theme-update/class-theme-update.php");
	  ELearningThemeUpdate::init($username,$apikey);
	}
}

add_action('after_setup_theme','eLearning_define_constants',10);
function eLearning_define_constants(){

	if ( ! defined( 'ELEARNING_COURSE_SLUG' ) )
		define( 'ELEARNING_COURSE_SLUG', 'course' );

	if ( ! defined( 'BP_COURSE_SLUG' ) )
		define( 'BP_COURSE_SLUG', 'course' );

	if ( ! defined( 'ELEARNING_COURSE_CATEGORY_SLUG' ) )
		define( 'ELEARNING_COURSE_CATEGORY_SLUG', 'course-cat' );

	if ( ! defined( 'ELEARNING_UNIT_SLUG' ) )
		define( 'ELEARNING_UNIT_SLUG', 'unit' );

	if ( ! defined( 'ELEARNING_QUIZ_SLUG' ) )
		define( 'ELEARNING_QUIZ_SLUG', 'quiz' );

	if ( ! defined( 'ELEARNING_QUESTION_SLUG' ) )
		define( 'ELEARNING_QUESTION_SLUG', 'question' );

	if ( ! defined( 'ELEARNING_EVENT_SLUG' ) )
		define( 'ELEARNING_EVENT_SLUG', 'event' );

	if ( ! defined( 'ELEARNING_ASSIGNMENT_SLUG' ) )
		define( 'ELEARNING_ASSIGNMENT_SLUG', 'assignment' );

	if ( ! defined( 'ELEARNING_LEVEL_SLUG' ) )
		define( 'ELEARNING_LEVEL_SLUG', 'level' );

	if ( ! defined( 'ELEARNING_LOCATION_SLUG' ) )
		define( 'ELEARNING_LOCATION_SLUG', 'location' );

	if ( ! defined( 'BP_COURSE_RESULTS_SLUG' ) )
		define( 'BP_COURSE_RESULTS_SLUG', 'course-results' );

	if ( !defined( 'BP_COURSE_STATS_SLUG' ) )
 		define( 'BP_COURSE_STATS_SLUG', 'course-stats' );

}
?>