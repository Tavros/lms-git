<?php

if (!defined('ABSPATH'))
    exit;

// Essentials
include_once 'includes/config.php';
include_once 'includes/init.php';

// Register & Functions
include_once 'includes/register.php';
include_once 'includes/actions.php';
include_once 'includes/filters.php';
include_once 'includes/func.php';
include_once 'includes/ratings.php';
// Customizer
include_once 'includes/customizer/customizer.php';
include_once 'includes/customizer/css.php';
include_once 'includes/eLearning-menu.php';
include_once 'includes/notes-discussions.php';
include_once 'includes/eLearning-woocommerce-checkout.php';

include_once 'eLearning-addons/onstreamAPI/functions.php';

if (function_exists('bp_is_active')) {


// eLearning Dashboard
    include_once 'eLearning-addons/account_dashboar/dashboard.php';
    //  include_once 'eLearning-addons/messagecenter/messagecenter.php';

    include_once 'eLearning-addons/shortcodes/eLearning-shortcodes.php';
    include_once 'eLearning-addons/events/events.php';
    include_once 'eLearning-addons/front-end/eLearning-front-end.php';
    include_once 'eLearning-addons/assignments/eLearning-assignments.php';
    include_once 'eLearning-addons/course-module/loader.php';
    include_once 'eLearning-addons/customtypes/eLearning-customtypes.php';
    include_once 'eLearning-addons/customlinks/customlink.php';

    if (checkUserRole('Student') || checkUserRole('instructor')) {
        include_once 'eLearning-addons/account/account.php';
        include_once 'eLearning-addons/profile/profile.php';
        include_once 'eLearning-addons/myfiles/myfiles.php';
        include_once 'eLearning-addons/mynetwork/mynetwork.php';
        include_once 'eLearning-addons/referrals/referrals.php';
        //include_once 'eLearning-addons/sessions/sessions.php';
    }

    if (is_super_admin()) {
        include_once 'eLearning-addons/sales/sales.php';
        include_once 'eLearning-addons/marketing/marketing.php';
        include_once 'eLearning-addons/users/users.php';
        include_once 'eLearning-addons/admin-settings/admin-settings.php';
    }
}


if (function_exists('bp_get_signup_allowed')) {
    include_once 'includes/bp-custom.php';
}

include_once '_inc/ajax.php';
include_once 'includes/buddydrive.php';
//Widgets
include_once('includes/widgets/custom_widgets.php');
if (function_exists('bp_get_signup_allowed')) {
    include_once('includes/widgets/custom_bp_widgets.php');
}
if (function_exists('pmpro_hasMembershipLevel')) {
    include_once('includes/pmpro-connect.php');
}
include_once('includes/widgets/advanced_woocommerce_widgets.php');
include_once('includes/widgets/twitter.php');
include_once('includes/widgets/flickr.php');

//Misc
include_once 'includes/extras.php';
include_once 'includes/tincan.php';
include_once 'setup/eLearning-install.php';

include_once 'setup/installer/envato_setup.php';

// Options Panel
get_template_part('eLearning', 'options');

function rt_login_redirect($redirect_to, $set_for, $user) {
    $redirect_to = bp_core_get_user_domain($user->ID);
    return $redirect_to;
}

add_filter("login_redirect", 'rt_login_redirect', 20, 3);

function checkUserRole($role) {
    $user = wp_get_current_user();
    if (in_array($role, (array) $user->roles)) {
        return true;
    }
    return false;
}

add_filter('bp_core_enable_root_profiles', '__return_true');

function getCustomBodypressMenu() {

    $bp = buddypress();
    foreach ($bp->members->nav->get_primary() as $user_nav_item) {
        if (empty($user_nav_item->show_for_displayed_user) && !bp_is_my_profile()) {
            continue;
        }

        $selected = '';
        if (bp_is_current_component($user_nav_item->slug)) {
            $selected = ' class="current selected"';
        }

        if (bp_loggedin_user_domain()) {
            $link = str_replace(bp_loggedin_user_domain(), bp_displayed_user_domain(), $user_nav_item->link);
        } else {
            $link = trailingslashit(bp_displayed_user_domain() . $user_nav_item->link);
        }

        /**
         * Filters the navigation markup for the displayed user.
         *
         * This is a dynamic filter that is dependent on the navigation tab component being rendered.
         *
         * @since 1.1.0
         *
         * @param string $value         Markup for the tab list item including link.
         * @param array  $user_nav_item Array holding parts used to construct tab list item.
         *                              Passed by reference.
         */
        if ($user_nav_item->slug == 'profile') {
            
        } elseif ($user_nav_item->slug == 'buddydrive') {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_' . $user_nav_item->css_id, array('<li id="' . $user_nav_item->css_id . '-personal-li" ' . $selected . '><a id="user-' . $user_nav_item->css_id . '" href="' . $link . '">' . __('File', 'elearning') . '</a></li>', &$user_nav_item));
        } else {
            echo apply_filters_ref_array('bp_get_displayed_user_nav_' . $user_nav_item->css_id, array('<li id="' . $user_nav_item->css_id . '-personal-li" ' . $selected . '><a id="user-' . $user_nav_item->css_id . '" href="' . $link . '">' . $user_nav_item->name . '</a></li>', &$user_nav_item));
        }
    }
}

function bp_get_options_event_nav($parent_slug = '') {
    $bp = buddypress();

    // If we are looking at a member profile, then the we can use the current
    // component as an index. Otherwise we need to use the component's root_slug.
    $component_index = !empty($bp->displayed_user) ? bp_current_component() : bp_get_root_slug(bp_current_component());
    $selected_item = bp_current_action();
  //  var_dump($selected_item);
    // Default to the Members nav.
    if (!bp_is_single_item()) {
        // Set the parent slug, if not provided.
        if (empty($parent_slug)) {
            $parent_slug = $component_index;
        }

        $secondary_nav_items = $bp->members->nav->get_secondary(array('parent_slug' => $parent_slug));

        if (!$secondary_nav_items) {
            return false;
        }

        // For a single item, try to use the component's nav.
    } else {
        $current_item = bp_current_item();
        $single_item_component = bp_current_component();

        // Adjust the selected nav item for the current single item if needed.
        if (!empty($parent_slug)) {
            $current_item = $parent_slug;
            $selected_item = bp_action_variable(0);
        }

        // If the nav is not defined by the parent component, look in the Members nav.
        if (!isset($bp->{$single_item_component}->nav)) {
            $secondary_nav_items = $bp->members->nav->get_secondary(array('parent_slug' => $current_item));
        } else {
            $secondary_nav_items = $bp->{$single_item_component}->nav->get_secondary(array('parent_slug' => $current_item));
        }

        if (!$secondary_nav_items) {
            return false;
        }
    }

    // Loop through each navigation item.
    foreach ($secondary_nav_items as $subnav_item) {
        if (empty($subnav_item->user_has_access)) {
            continue;
        }

        // If the current action or an action variable matches the nav item id, then add a highlight CSS class.
        //  var_dump($subnav_item->slug);

        if ($subnav_item->slug === $selected_item) {
            $selected = ' class="current selected"';
        } else {
            $selected = '';
        }

        // List type depends on our current component.
        $list_type = bp_is_group() ? 'groups' : 'personal';

        /**
         * Filters the "options nav", the secondary-level single item navigation menu.
         *
         * This is a dynamic filter that is dependent on the provided css_id value.
         *
         * @since 1.1.0
         *
         * @param string $value         HTML list item for the submenu item.
         * @param array  $subnav_item   Submenu array item being displayed.
         * @param string $selected_item Current action.
         */
        echo apply_filters('bp_get_options_nav_' . $subnav_item->css_id, '<li id="' . esc_attr($subnav_item->css_id . '-' . $list_type . '-li') . '" ' . $selected . '><a id="' . esc_attr($subnav_item->css_id) . '" href="' . esc_url($subnav_item->link) . '">' . $subnav_item->name . '</a></li>', $subnav_item, $selected_item);
    }
}

define('COURSE_EDIT_PAGE', 24);
define('EVENT_EDIT_PAGE', 623);

function createEventWebinar($event_id, $post_settings, $setting) {
    $unit = array();
    $post_metas = array();
    foreach ($post_settings as $setting) {

        switch ($setting->id) {
            case 'eLearning_start_date':
                $originalDate = $setting->value;
                $post_metas['webinar_start_date'] = date("d-m-Y", strtotime($originalDate));
                break;
            case 'eLearning_start_time':
                $originalDate = $setting->value;
                $post_metas['webinar_start_time'] = date("hh:ii:ss", strtotime($originalDate));
                break;
            case 'eLearning_expected_duration':
                $post_metas['webinar_expected_duration'] = $setting->value;
                break;
            case 'eLearning_event_is_recorded':
                $post_metas['webinar_is_recorded'] = $setting->value;
                break;
        }
    }
    $post_settings['post_type'] = 'unit';
    $post_settings['post_status'] = 'publish';

    $webinarId = wp_insert_post($post_settings);
    if ($webinarId) {
        $getOnstreamResults = createNewWebinarInOnstream($post_settings, $post_metas, $webinarId);

        if (isset($getOnstreamResults->id)) {
            $post_metas['onstreamSessionId'] = (int) $getOnstreamResults->id;
        } elseif (!empty($getOnstreamResults->error)) {
            echo $getOnstreamResults->error;
        }

        foreach ($post_metas as $key => $value) {
            update_post_meta($webinarId, $key, $value);
        }
    }
    return $webinarId;
}

function custom_pagination($numpages = '', $pagerange = '', $paged = '') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     * 
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if (!$numpages) {
            $numpages = 1;
        }
    }

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function. 
     */
    $pagination_args = array(
        'base' => get_pagenum_link(1) . '%_%',
        'format' => 'page/%#%',
        'total' => $numpages,
        'current' => $paged,
        'show_all' => false,
        'end_size' => 1,
        'mid_size' => $pagerange,
        'prev_next' => true,
        'prev_text' => __('<'),
        'next_text' => __('>'),
        'type' => 'array',
        'add_args' => false,
        'add_fragment' => ''
    );

    $paginate_links = paginate_links($pagination_args);



    if ($paginate_links) {
        echo "<nav class='custom-pagination'>";
        foreach ($paginate_links as $key => $pg) {
            if ($key === 0) {
                echo str_replace(1, "<", $pg).'  ';
                echo "Page " . $paged . " of " . $numpages;
            }
            if (sizeof($paginate_links) === $key + 1) {
                echo '  '.$pg;
            }
        }

//        echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
//        echo $paginate_links;
//        echo "</nav>";
    }
}

add_action( 'wp_enqueue_scripts', 'fm_theme_enqueue_styles' );
function fm_theme_enqueue_styles() {
    wp_enqueue_style( 'Hasmik', get_template_directory_uri() . '/Hasmik.css' );
    wp_enqueue_style( 'Ando', get_template_directory_uri() . '/Ando.css' );
    wp_enqueue_style( 'Vigen', get_template_directory_uri() . '/Vigen.css' );
    wp_enqueue_style( 'Vahan', get_template_directory_uri() . '/Vahan.css' );
    wp_enqueue_style( 'Hovo', get_template_directory_uri() . '/Hovo.css' );
    wp_enqueue_style( 'Rob', get_template_directory_uri() . '/Rob.css' );
/*wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/Hasmik.css' );*/

}
